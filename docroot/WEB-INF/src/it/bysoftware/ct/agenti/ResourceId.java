package it.bysoftware.ct.agenti;

/**
 * Enum to manage resource id.
 */
public enum ResourceId {
    /**
     * Save Order.
     */
    saveOrder,
    
    /**
     * Export Order.
     */
    exportOrder,
    
    /**
     * Print Order ID.
     */
    printOrder,
    
    /**
     * Send Order ID.
     */
    sendOrder,
    
    /**
     * Auto complete ID.
     */
    autoComplete, 
    
    /**
     * Load item info ID.
     */
    loadItemInfo,
    
    /**
     * Save note document.
     */
    saveNote
    
}
