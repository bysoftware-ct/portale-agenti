/***********************************************************************
 * Copyright (c) 2016:
 * BySoftware Soc. coop. a.r.l., Belpasso (CT) - Italy
 *
 * See http://www.bysoftware.ct.it for details on the copyright holder.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***********************************************************************/
package it.bysoftware.ct.agenti;

/**
 * Class that handles Response.
 * 
 * @author Mario Torrisi
 *
 */
public class Response {

    /**
     * Response Code.
     * 
     * @author Mario Torrisi
     */
    public enum Code {
        /**
         * Action completes correctly.
         */
        OK,
        /**
         * Duplicate key error.
         */
        GET_PRIMARY_KEY_ERROR,
        /**
         * Generic insert error.
         */
        INSERT_ERROR,
        /**
         * Parsing JSON error.
         */
        PARSING_JSON_ERROR,
        /**
         * Error occurred sending email.
         */
        SENDING_MAIL_ERROR,
        /**
         * Date format error.
         */
        DATE_FORMAT_ERROR,
        /**
         * Generic error.
         */
        GENERIC_ERROR,
        /**
         * Order acquired.
         */
        ACQUIRED,
        /**
         * Print order error.
         */
        PRINT_ERROR,
        /**
         * Save note error.
         */
        SAVE_NOTE_ERROR
    }

    /**
     * Response message.
     */
    private String message;

    /**
     * Response code attribute.
     */
    private int code;

    /**
     * Creates an empty response.
     */
    public Response() {
    }

    /**
     * Creates a response with given identifier and {@link Code}.
     * 
     * @param c
     *            response Code
     */
    public Response(final Code c) {
        this.code = c.ordinal();
    }

    /**
     * Creates a response with given identifier and {@link Code}.
     * 
     * @param c
     *            response Code
     * @param msg
     *            message of the new response
     */
    public Response(final Code c, final String msg) {
        this.code = c.ordinal();
        this.message = msg;
    }

    /**
     * Returns response message.
     * 
     * @return response message
     */
    public final String getMessage() {
        return message;
    }

    /**
     * Sets response identifier.
     * 
     * @param msg
     *            response identifier
     */
    public final void setMessage(final String msg) {
        this.message = msg;
    }

    /**
     * Returns response {@link Code}.
     * 
     * @return response Code
     */
    public final int getCode() {
        return code;
    }

    @Override
    public final String toString() {
        return "Response [message=" + this.message + ", code=" + this.code
                + "]";
    }
}
