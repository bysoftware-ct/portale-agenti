/***********************************************************************
 * Copyright (c) 2016:
 * BySoftware Soc. coop. a.r.l., Belpasso (CT) - Italy
 *
 * See http://www.bysoftware.ct.it for details on the copyright holder.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***********************************************************************/

package it.bysoftware.ct.agenti;

import it.bysoftware.ct.agenti.Response.Code;
import it.bysoftware.ct.model.Articoli;
import it.bysoftware.ct.model.DatiClientiFornitori;
import it.bysoftware.ct.model.ListiniPrezziArticoli;
import it.bysoftware.ct.model.ScontiArticoli;
import it.bysoftware.ct.model.VociIva;
import it.bysoftware.ct.model.WKOrdiniClienti;
import it.bysoftware.ct.model.WKRigheOrdiniClienti;
import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.service.DatiClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.ListiniPrezziArticoliLocalServiceUtil;
import it.bysoftware.ct.service.ScontiArticoliLocalServiceUtil;
import it.bysoftware.ct.service.VociIvaLocalServiceUtil;
import it.bysoftware.ct.service.WKOrdiniClientiLocalServiceUtil;
import it.bysoftware.ct.service.WKRigheOrdiniClientiLocalServiceUtil;
import it.bysoftware.ct.service.persistence.ScontiArticoliPK;
import it.bysoftware.ct.service.persistence.WKOrdiniClientiPK;
import it.bysoftware.ct.service.persistence.WKRigheOrdiniClientiPK;
import it.bysoftware.ct.utils.Constants;
import it.bysoftware.ct.utils.Report;
import it.bysoftware.ct.utils.Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;

import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.servlet.ServletResponseUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ClassNameLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.util.DLUtil;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import com.liferay.util.ContentUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Class the implements the portlet logic. It extends the {@link MVCPortlet}
 *
 * @author Mario Torrisi torrisi.mario@gmail.com
 */
public class AgentiPortlet extends MVCPortlet {

    /**
     * Description string constant.
     */
    private static final String DESCRIZIONE = "descrizione";

    /**
     * Year string constant.
     */
    private static final String ANNO = "anno";

    /**
     * Hundred constant.
     */
    private static final int HUNDRED = 100;
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(AgentiPortlet.class);
    
    @Override
    public final void serveResource(final ResourceRequest resourceRequest,
            final ResourceResponse resourceResponse) throws IOException,
            PortletException {

        String resourceID = resourceRequest.getResourceID();
        String codiceArticolo = ParamUtil.
                getString(resourceRequest, "itemCode");
        String codiceSoggetto = ParamUtil.getString(resourceRequest,
                "customerCode");
        PrintWriter out;
        Response response;
        switch (ResourceId.valueOf(resourceID)) {
            case saveOrder:
                this.logger.debug("Calling saveOrder()");
                response = this.saveOrder(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case printOrder:
                this.logger.debug("Calling printOrder()");
                response = this.printOrder(resourceRequest);
                File file = new File(response.getMessage());
                long fileEntryId = addToDL(file, resourceRequest);
                if (fileEntryId != -1) {
                    try {
                        WKOrdiniClienti o = WKOrdiniClientiLocalServiceUtil
                                .fetchWKOrdiniClienti(new WKOrdiniClientiPK(
                                        ParamUtil.getInteger(resourceRequest,
                                                ANNO, Utils.getYear()),
                                                Constants.TIPO_ORDINE,
                                        ParamUtil.getInteger(resourceRequest,
                                                "numeroOrdine")));
                        o.setNomePDF(String.valueOf(fileEntryId));
                        WKOrdiniClientiLocalServiceUtil.
                            updateWKOrdiniClienti(o);
                    } catch (SystemException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    InputStream in = new FileInputStream(file);
                    HttpServletResponse httpRes = PortalUtil
                            .getHttpServletResponse(resourceResponse);
                    HttpServletRequest httpReq = PortalUtil
                            .getHttpServletRequest(resourceRequest);
                    ServletResponseUtil.sendFile(httpReq, httpRes,
                            file.getName(), in, "application/pdf");
                    in.close();
                }
                break;
            case sendOrder:
                this.logger.debug("Calling sendOrder()");
                response = this.sendOrder(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            case autoComplete:
                this.logger.debug("Looking for items matching: "
                        + codiceArticolo);
                JSONArray usersJSONArray = JSONFactoryUtil.createJSONArray();
                if (codiceArticolo != null && !codiceArticolo.isEmpty()) {
                    usersJSONArray = this.searchArticoliByCodice(
                            codiceArticolo, ParamUtil.getBoolean(
                                    resourceRequest, "noObsolete", true));
                }
                out = resourceResponse.getWriter();
                out.println(usersJSONArray.toString());
                out.flush();
                out.close();
                break;
            case loadItemInfo:
                this.logger.debug("Loading item: " + codiceArticolo
                        + " info, for: " + codiceSoggetto + "...");
                JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
                if (codiceArticolo != null && !codiceArticolo.isEmpty()
                        && codiceSoggetto != null
                        && !codiceSoggetto.isEmpty()) {
                    jsonObject = loadItemInfo(codiceArticolo, codiceSoggetto);
                }
                out = resourceResponse.getWriter();
                out.println(jsonObject.toString());
                out.flush();
                out.close();
                break;
            case saveNote:
                this.logger.debug("Saving note ...");
                response = this.saveNote(resourceRequest);
                out = resourceResponse.getWriter();
                out.print(JSONFactoryUtil.looseSerialize(response));
                out.flush();
                out.close();
                break;
            default:
                break;
        }
    }

    /**
     * Loads item information.
     * @param codiceArticolo item code
     * @param codiceSoggetto customer code
     * @return a {@link JSONObject} represents the item information 
     */
    private JSONObject loadItemInfo(final String codiceArticolo,
            final String codiceSoggetto) {
        DatiClientiFornitori datiCliente = null;
        Articoli articolo = null;

        boolean scontoDaANAART = true;
        double prezzo = 0.0;
        double sconto1 = 0.0;
        double sconto2 = 0.0;
        double sconto3 = 0.0;
        double aliquotaIva = 0.0;
        String codiceIva = "";

        if (codiceSoggetto != null && codiceArticolo != null
                && !codiceSoggetto.isEmpty() && !codiceArticolo.isEmpty()) {
            datiCliente = DatiClientiFornitoriLocalServiceUtil.
                    getDatiCliente(codiceSoggetto);
            try {
                articolo = ArticoliLocalServiceUtil.getArticoli(codiceArticolo);
                if (datiCliente.getPrezzoDaProporre() == 0) {
                    ListiniPrezziArticoli listinoDedicato =
                            ListiniPrezziArticoliLocalServiceUtil.
                            getListinoDedicato(articolo.getCodiceArticolo(),
                                    "", codiceSoggetto);
                    ListiniPrezziArticoli listinoGenerico =
                            ListiniPrezziArticoliLocalServiceUtil.
                            getListinoGenerico(articolo.getCodiceArticolo(),
                                    "");
                    if (listinoDedicato != null) {
                        if (!listinoDedicato.getCodiceIVA().equals("")) {
                            VociIva iva = VociIvaLocalServiceUtil.
                                    fetchVociIva(listinoDedicato
                                            .getCodiceIVA());
                            prezzo = listinoDedicato.getPrezzoNettoIVA()
                                    / (1 + (iva.getAliquota() / HUNDRED));
                        } else {
                            prezzo = listinoDedicato.getPrezzoNettoIVA();
                        }
                        if (listinoDedicato.getUtilizzoSconti()) {
                            scontoDaANAART = false;
                            sconto1 = listinoDedicato.getScontoListino1();
                            sconto2 = listinoDedicato.getScontoListino2();
                            sconto3 = listinoDedicato.getScontoListino3();
                        }
                    } else if (listinoGenerico != null) {
                        if (!listinoGenerico.getCodiceIVA().equals("")) {
                            VociIva iva = VociIvaLocalServiceUtil.
                                    fetchVociIva(listinoGenerico.
                                            getCodiceIVA());
                            prezzo = listinoGenerico.getPrezzoNettoIVA()
                                    / (1 + (iva.getAliquota() / HUNDRED));
                        } else {
                            prezzo = listinoGenerico.getPrezzoNettoIVA();
                        }
                        if (listinoGenerico.getUtilizzoSconti()) {
                            scontoDaANAART = false;
                            sconto1 = listinoGenerico.getScontoListino1();
                            sconto2 = listinoGenerico.getScontoListino2();
                            sconto3 = listinoGenerico.getScontoListino3();
                        }
                    }
                } else if (datiCliente.getPrezzoDaProporre() == 1) {
                    if (!articolo.getCodiceIVAPrezzo1().equals("")) {
                        VociIva iva = VociIvaLocalServiceUtil.
                                fetchVociIva(articolo.getCodiceIVAPrezzo1());
                        prezzo = articolo.getPrezzo1()
                                / (1 + (iva.getAliquota() / HUNDRED));
                    } else {
                        prezzo = articolo.getPrezzo1();
                    }
                } else if (datiCliente.getPrezzoDaProporre() == 2) {
                    if (!articolo.getCodiceIVAPrezzo2().equals("")) {
                        VociIva iva = VociIvaLocalServiceUtil.
                                fetchVociIva(articolo.getCodiceIVAPrezzo2());
                        prezzo = articolo.getPrezzo2()
                                / (1 + (iva.getAliquota() / HUNDRED));
                    } else {
                        prezzo = articolo.getPrezzo2();
                    }
                } else {
                    if (!articolo.getCodiceIVAPrezzo3().equals("")) {
                        VociIva iva = VociIvaLocalServiceUtil.
                                fetchVociIva(articolo.getCodiceIVAPrezzo3());
                        prezzo = articolo.getPrezzo3()
                                / (1 + (iva.getAliquota() / HUNDRED));
                    } else {
                        prezzo = articolo.getPrezzo3();
                    }
                }
                if (scontoDaANAART) {
                    ScontiArticoli sc1 = ScontiArticoliLocalServiceUtil.
                            fetchScontiArticoli(new ScontiArticoliPK(
                                    datiCliente.getScontoCat1(), articolo.
                                            getCodiceSconto1()));
                    ScontiArticoli sc2 = ScontiArticoliLocalServiceUtil.
                            fetchScontiArticoli(new ScontiArticoliPK(
                                    datiCliente.getScontoCat2(), articolo.
                                            getCodiceSconto2()));
                    sconto1 = 0.0;
                    if (sc1 != null) {
                        sc1.getSconto();
                    }
                    sconto2 = 0.0;
                    if (sc2 != null) {
                        sc2.getSconto(); 
                    }
                }
                VociIva iva = VociIvaLocalServiceUtil.fetchVociIva(articolo.
                        getCodiceIVA());
                aliquotaIva = iva.getAliquota();
                codiceIva = iva.getCodiceIva();
            } catch (PortalException | SystemException e) {
                this.logger.error(e.getMessage());
                e.printStackTrace();
            }
        }

        JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
        jsonObject.put(DESCRIZIONE, articolo.getDescrizione());
        jsonObject.put("unitaMisura", articolo.getUnitaMisura());
        jsonObject.put("prezzo", prezzo);
        jsonObject.put("sconto1", sconto1);
        jsonObject.put("sconto2", sconto2);
        jsonObject.put("sconto3", sconto3);
        jsonObject.put("aliquotaIva", aliquotaIva);
        jsonObject.put("codiceIva", codiceIva);
        jsonObject.put("itemSheetId", articolo.getLiberoString5());

        return jsonObject;
    }

    /**
     * Returns a list of items that match with user imputed characters.
     * 
     * @param codiceArticolo
     *            item user is looking for
     * @param noObsolete
     *            true for non obsolete items, false otherwise 
     * @return the list of matching items
     */
    private JSONArray searchArticoliByCodice(final String codiceArticolo,
            final boolean noObsolete) {
        JSONArray usersJSONArray = JSONFactoryUtil.createJSONArray();
        try {
            List<Articoli> articoli = ArticoliLocalServiceUtil.
                    searchArticoliByCodice(codiceArticolo, "", "all",
                            noObsolete, true, 0, ArticoliLocalServiceUtil.
                            getArticolisCount(), null);
            JSONObject userJSON = null;
            this.logger.debug("Item size" + articoli.size());
            for (Articoli articolo : articoli) {
                userJSON = JSONFactoryUtil.createJSONObject();
                userJSON.put("codiceArticolo", articolo.getCodiceArticolo());
                userJSON.put(DESCRIZIONE, articolo.getDescrizione());
                usersJSONArray.put(userJSON);
            }
        } catch (SystemException e) {
            this.logger.error(e);
            e.printStackTrace();
        }
        return usersJSONArray;
    }

    /**
     * Adds the order to the Document Library.
     * 
     * @param file
     *            the order file
     * @param resourceRequest
     *            {@link ResourceRequest}
     * @return {@link FileEntry} id or -1 if any error occur. 
     */
    private long addToDL(final File file,
            final ResourceRequest resourceRequest) {
        long result = -1;
        try {
            int anno = ParamUtil.getInteger(resourceRequest, ANNO,
                    Utils.getYear());
            User agente = UserLocalServiceUtil.getUser(Long.
                    parseLong(resourceRequest.getRemoteUser()));
            ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
                    agente.getCompanyId(),
                    ClassNameLocalServiceUtil.getClassNameId(User.class),
                    "CUSTOM_FIELDS", Constants.CODICE_AZIENDA,
                    agente.getUserId());
            long codiceAzienda = Long.parseLong(value.getData());
            User azienda = UserLocalServiceUtil.getUser(codiceAzienda);

            ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.
                    getAttribute(WebKeys.THEME_DISPLAY);

            long groupId = themeDisplay.getLayout().getGroupId();
            long repositoryId = themeDisplay.getScopeGroupId();
            DLFolder aziendaFolder = Utils.getAziendaFolder(groupId, azienda);
            this.logger.debug("Azienda FOLDER: " + aziendaFolder);
            DLFolder agentFolder = Utils.getAgenteFolder(groupId,
                    aziendaFolder, agente);
            this.logger.debug("Agente FOLDER: " + agentFolder);
            DLFolder yearFolder = Utils.getAgenteYearFolder(groupId,
                    agentFolder, anno);

            FileEntry fileEntry = null;
            try {
                fileEntry = DLAppServiceUtil.getFileEntry(groupId,
                        yearFolder.getFolderId(), file.getName());
                this.logger.info("Entry found, the file will be replaced.");
            } catch (PortalException e) {
                this.logger.info("File Entry not found, a new file will be"
                        + " created." + file.getName());
            }

            if (fileEntry != null) {
                DLAppServiceUtil.deleteFileEntry(fileEntry.getFileEntryId());
            }
            fileEntry = DLAppServiceUtil.addFileEntry(repositoryId,
                    yearFolder.getFolderId(), file.getName(),
                    MimeTypesUtil.getContentType(file), file.getName(), "", "",
                    file, new ServiceContext());

            this.logger.info("Added: " + fileEntry.getTitle() + " to: /"
                    + aziendaFolder.getName() + "/" + agentFolder.getName()
                    + "/" + yearFolder.getName());
            DLUtil.getPreviewURL(fileEntry, fileEntry.getFileVersion(),
                    themeDisplay, file.getName());
            
            result = fileEntry.getFileEntryId();
        } catch (NumberFormatException | PortalException | SystemException e1) {
            e1.printStackTrace();
        }
        return result;
    }

    /**
     * Prints an order's PDF preview.
     * 
     * @param resourceRequest
     *            the {@link ResourceRequest}
     * @return {@link Response} that contains result information
     */
    private Response printOrder(final ResourceRequest resourceRequest) {

        int anno = ParamUtil.getInteger(resourceRequest, ANNO,
                Utils.getYear());
        int numero = ParamUtil.getInteger(resourceRequest, "numeroOrdine", -1);
        String codiceCliente = ParamUtil.getString(resourceRequest,
                "codiceCliente", "");

        try {
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("jdbc/AgentiPool");

            Connection conn = ds.getConnection();
            if (conn != null) {
                this.logger.debug(conn.getCatalog());
                Report report = new Report(conn);
                try {
                    User user = UserLocalServiceUtil.getUser(Long.
                            parseLong(resourceRequest.getRemoteUser()));
                    ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
                            user.getCompanyId(), ClassNameLocalServiceUtil.
                                    getClassNameId(User.class),
                            "CUSTOM_FIELDS", Constants.CODICE_AZIENDA, user
                                    .getUserId());
                    long codiceAzienda = Long.parseLong(value.getData());
                    this.logger.debug("CodiceAzienda: " + codiceAzienda);
                    User azienda = UserLocalServiceUtil.getUser(codiceAzienda);
                    String filePath = report.print(azienda.getUserId(),
                            azienda.getScreenName(), codiceCliente, anno,
                            Constants.TIPO_ORDINE, numero);
                    conn.close();
                    return new Response(Code.OK, filePath);
                } catch (ClassNotFoundException | JRException
                        | NumberFormatException | PortalException
                        | SystemException e) {
                    this.logger.error(e.getMessage());
                    conn.close();
                    return new Response(Code.PRINT_ERROR, e.getMessage());
                }
            }
        } catch (NamingException | SQLException e) {
            this.logger.error(e.getMessage());
            return new Response(Code.PRINT_ERROR, e.getMessage());
        }
        return null;
    }

    /**
     * Processes the delete order request.
     * 
     * @param actionRequest
     *            the request
     * @param actionResponse
     *            the response
     * @throws PortletException
     */
    @ProcessAction(name = "deleteOrder")
    public final void deleteOrder(final ActionRequest actionRequest,
            final ActionResponse actionResponse) {
        logger.debug("Called deleteOrder...");

        int anno = ParamUtil.getInteger(actionRequest, ANNO, -1);
        int tipoOrdine = ParamUtil.getInteger(actionRequest, "tipoOrdine", -1);
        int numeroOrdine = ParamUtil.getInteger(actionRequest, "numeroOrdine",
                -1);
        if (anno != -1 && tipoOrdine != -1 && numeroOrdine != -1) {
            WKOrdiniClienti ordine = null;
            try {
                ordine = WKOrdiniClientiLocalServiceUtil
                        .getWKOrdiniClienti(new WKOrdiniClientiPK(anno,
                                tipoOrdine, numeroOrdine));
                List<WKRigheOrdiniClienti> righeOrdine =
                        WKRigheOrdiniClientiLocalServiceUtil
                        .getByAnnoTipoNumeroOrdine(anno, tipoOrdine,
                                numeroOrdine);
                for (WKRigheOrdiniClienti rigo : righeOrdine) {
                    WKRigheOrdiniClientiLocalServiceUtil
                            .deleteWKRigheOrdiniClienti(rigo);
                }
                WKOrdiniClientiLocalServiceUtil.deleteWKOrdiniClienti(ordine);
                actionResponse.setRenderParameter("codiceSoggetto",
                        ordine.getCodiceCliente());
            } catch (PortalException | SystemException e) {
                this.logger.error(e.getMessage());
                SessionErrors.add(actionRequest, "error-deleting-order");
            } finally {
                actionResponse.setRenderParameter("jspPage",
                        "/jsps/agenti/orders.jsp");
            }
        }
    }

    /**
     * Store order in the WKOrdiniClienti.
     * 
     * @param resourceRequest
     *            request comes from the user
     * @return result of Order save process
     */
    private Response saveOrder(final ResourceRequest resourceRequest) {
        List<WKRigheOrdiniClienti> righiOrdine = null;
        try {
            User user = UserLocalServiceUtil.getUser(Long.
                    parseLong(resourceRequest.getRemoteUser()));
            ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
                    user.getCompanyId(),
                    ClassNameLocalServiceUtil.getClassNameId(User.class),
                    "CUSTOM_FIELDS", Constants.CODICE_AGENTE, user.getUserId());
            String codiceAgente = value.getData();
            this.logger.debug(codiceAgente);
            String codiceCliente = ParamUtil.getString(resourceRequest,
                    "codiceCliente", null);
            this.logger.debug(codiceCliente);
            String codicePagam = ParamUtil.getString(resourceRequest,
                    "codicePagam", null);
            this.logger.debug(codicePagam);
            String orderDate = ParamUtil.getString(resourceRequest,
                    "orderDate", null);
            this.logger.debug(orderDate);
            String deliveryDate = ParamUtil.getString(resourceRequest,
                    "deliveryDate", null);
            this.logger.debug(deliveryDate);
            int numeroOrdine = ParamUtil.getInteger(resourceRequest,
                    "numeroOrdine", -1);
            boolean editFlag = false;
            WKOrdiniClienti testataOrdine = null;
            if (numeroOrdine == -1) {
                this.logger.debug("Creating a new order...");
                numeroOrdine = WKOrdiniClientiLocalServiceUtil.getNumeroOrdine(
                        Utils.getYear(), Constants.TIPO_ORDINE);
                testataOrdine = WKOrdiniClientiLocalServiceUtil.
                        createWKOrdiniClienti(new WKOrdiniClientiPK(Utils.
                                getYear(), Constants.TIPO_ORDINE,
                                numeroOrdine + 1));
            } else {
                this.logger.debug("Updating an existing order...");
                editFlag = true;
                testataOrdine = WKOrdiniClientiLocalServiceUtil
                        .getWKOrdiniClienti(new WKOrdiniClientiPK(Utils.
                                getYear(), Constants.TIPO_ORDINE,
                                numeroOrdine));
                if (testataOrdine.getStatoOrdine()) {
                    return new Response(Code.ACQUIRED,
                            "L'ordine è già stato acquisito e non può più"
                                    + " essere modificato.");
                }
            }
            if (numeroOrdine >= 0) {
                testataOrdine.setCodiceCliente(codiceCliente);
                testataOrdine.setCodicePianoPag(codicePagam);
                testataOrdine.setCodiceAgente(codiceAgente);
                testataOrdine.setTipoDocumento(Constants.TIPO_DOCUMENTO);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                        "dd/MM/yyyy");
                testataOrdine.setDataDocumento(simpleDateFormat.
                        parse(orderDate));
                String rows = ParamUtil.
                        getString(resourceRequest, "rows", null);
                this.logger.debug(rows);
                if (editFlag) {
                    List<WKRigheOrdiniClienti> righeOrdine =
                            WKRigheOrdiniClientiLocalServiceUtil.
                            getByAnnoTipoNumeroOrdine(testataOrdine.getAnno(),
                                    testataOrdine.getTipoOrdine(),
                                    testataOrdine.getNumeroOrdine());
                    for (WKRigheOrdiniClienti rigo : righeOrdine) {
                        WKRigheOrdiniClientiLocalServiceUtil.
                                deleteWKRigheOrdiniClienti(rigo);
                    }
                }
                righiOrdine = new ArrayList<WKRigheOrdiniClienti>();
                JSONArray rowsJSON = JSONFactoryUtil.createJSONArray(rows);
                for (int i = 0; i < rowsJSON.length(); i++) {
                    JSONObject rowJSON = rowsJSON.getJSONObject(i);
                    this.logger.debug(rowJSON.toString());
                    WKRigheOrdiniClienti rigo =
                            WKRigheOrdiniClientiLocalServiceUtil.
                            createWKRigheOrdiniClienti(
                                    new WKRigheOrdiniClientiPK(
                                    testataOrdine.getAnno(), testataOrdine.
                                            getTipoOrdine(), testataOrdine.
                                            getNumeroOrdine(), i + 1));
                    rigo.setCodiceArticolo(rowJSON.getString("codiceArticolo"));
                    rigo.setDescrizione(rowJSON.getString(DESCRIZIONE));
                    rigo.setCodiceUnitMis(rowJSON.getString("unitaMisura"));
                    rigo.setQuantita(rowJSON.getDouble("quantita", 0.0F));
                    rigo.setPrezzo(rowJSON.getDouble("prezzo", 0.0F));
                    rigo.setSconto1(-1 * rowJSON.getDouble("sconto1", 0.0F));
                    rigo.setSconto2(-1 * rowJSON.getDouble("sconto2", 0.0F));
                    rigo.setSconto3(-1 * rowJSON.getDouble("sconto3", 0.0F));
                    rigo.setTipoRigo(rowJSON.getInt("tipoRigo", -1));
                    if (rigo.getTipoRigo() == 0) {
                        Articoli articolo = ArticoliLocalServiceUtil.
                                getArticoli(rigo.getCodiceArticolo());
                        rigo.setCodiceIVAFatturazione(articolo.getCodiceIVA());
                        rigo.setDataOridine(simpleDateFormat.
                                parse(orderDate));
                        rigo.setDataPrevistaConsegna(simpleDateFormat.
                                parse(deliveryDate));
                    }
                    try { // Adds or updates row
                        this.logger.debug("Inserting or updating row: " + rigo);
                        WKRigheOrdiniClientiLocalServiceUtil.
                                updateWKRigheOrdiniClienti(rigo);
                    } catch (SystemException e) { // If error roll back
                        this.logger.error(e.getMessage());
                        if (righiOrdine.size() > 0) {
                            for (WKRigheOrdiniClienti delRow : righiOrdine) {
                                WKRigheOrdiniClientiLocalServiceUtil.
                                        deleteWKRigheOrdiniClienti(delRow);
                            }
                        }
                        return new Response(Code.INSERT_ERROR, e.getMessage());
                    }
                    righiOrdine.add(rigo);
                }
                this.logger.debug("Inserting or updating: " + testataOrdine);
                WKOrdiniClientiLocalServiceUtil.
                        updateWKOrdiniClienti(testataOrdine);
                try {
                    sendMail(user, editFlag,
                            String.valueOf(testataOrdine.getNumeroOrdine()));
                } catch (AddressException e) {
                    this.logger.warn(e.getMessage());
                }
                return new Response(Response.Code.OK,
                        JSONFactoryUtil.looseSerialize(testataOrdine));
            } else {
                return new Response(Response.Code.INSERT_ERROR,
                        "Errore durante l'inserimento del documento.");
            }
        } catch (PortalException | SystemException | ParseException e) {
            this.logger.error(e.getMessage());
            if (righiOrdine != null && righiOrdine.size() > 0) {
                for (WKRigheOrdiniClienti delRow : righiOrdine) {
                    try {
                        WKRigheOrdiniClientiLocalServiceUtil.
                                deleteWKRigheOrdiniClienti(delRow);
                    } catch (SystemException ex) {
                        this.logger.error(ex.getMessage());
                        return new Response(Response.Code.GENERIC_ERROR,
                                e.getLocalizedMessage());
                    }
                }
            }
            return new Response(Response.Code.GENERIC_ERROR,
                    e.getLocalizedMessage());
        }
    }
    
    /**
     * Sends order preview to the customer.
     * 
     * @param resourceRequest
     *            the {@link ResourceRequest} object
     * @return {@link Response}
     */
    private Response sendOrder(final ResourceRequest resourceRequest) {
        String origEmail = ParamUtil.getString(resourceRequest, "origEmail");
        String email = ParamUtil.getString(resourceRequest, "email", "");
        User agent;
        try {
            agent = UserLocalServiceUtil.getUser(Long.parseLong(resourceRequest
                    .getRemoteUser()));
            String customer = ParamUtil.getString(resourceRequest, "customer");
            int numeroOrdine = ParamUtil.getInteger(resourceRequest,
                    "orderNumber");
            WKOrdiniClienti o = WKOrdiniClientiLocalServiceUtil.
                    getWKOrdiniClienti(new WKOrdiniClientiPK(
                            ParamUtil.getInteger(resourceRequest, ANNO,
                                    Utils.getYear()),
                            Constants.TIPO_ORDINE, numeroOrdine));
            boolean sendResult;
            if (!origEmail.isEmpty()) {
                email = origEmail;
            }
            sendResult = sendMail(agent, email, customer, numeroOrdine,
                    o.getNomePDF());
            if (sendResult) {
                return new Response(Response.Code.OK, "");
            } else {
                return new Response(Response.Code.GENERIC_ERROR,
                        "No valid order found!");
            }
        } catch (NumberFormatException | PortalException | SystemException
                | AddressException e) {
            e.printStackTrace();
            this.logger.error(e.getMessage());
            return new Response(Response.Code.SENDING_MAIL_ERROR,
                    e.getLocalizedMessage());
        }

    }
    
    /**
     * Actual sends the email to the customer.
     * 
     * @param agent
     *            connected
     * @param email
     *            customer's email address
     * @param customer
     *            customer name
     * @param orderNumber
     *            sending order number
     * @param fileEntryId
     *            {@link DLFileEntry} id of the order
     * @throws AddressException
     *             if to is not valid email address
     * @return send process result
     */
    private boolean sendMail(final User agent, final String email,
            final String customer, final int orderNumber,
            final String fileEntryId) throws AddressException {
        if (!fileEntryId.isEmpty()) {
            try {
                long id = Long.parseLong(fileEntryId);
                String body = ContentUtil
                        .get("/content/order-email.tmpl", true);
                body = StringUtil.replace(body,
                        new String[] { "[$CUSTOMER$]" },
                        new String[] { customer });

                MailMessage mailMessage = new MailMessage();
                mailMessage.setBody(body);

                mailMessage.setSubject("Copia Ordine N° " + orderNumber);
                mailMessage
                        .setFrom(new InternetAddress(agent.getEmailAddress()));
                mailMessage.setTo(new InternetAddress(email));
                DLFileEntry fileEntry = DLFileEntryLocalServiceUtil
                        .getDLFileEntry(id);
                File f = DLFileEntryLocalServiceUtil.getFile(agent.getUserId(),
                        id, "1.0", false);
                File fDest = new File(fileEntry.getTitle());
                FileUtil.copyFile(f, fDest);
                mailMessage.addFileAttachment(fDest);
                MailServiceUtil.sendEmail(mailMessage);

                this.logger.debug(("FROM: " + mailMessage.getFrom()
                        .getAddress()));
                this.logger
                        .debug(("TO: " + mailMessage.getTo()[0].getAddress()));
                return true;
            } catch (NumberFormatException | PortalException | SystemException
                    | IOException e) {
                this.logger.error(e.getMessage());
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Add note document for the customer.
     * 
     * @param resourceRequest
     *            {@link ResourceRequest}
     * @return {@link Response} 
     */
    private Response saveNote(final ResourceRequest resourceRequest) {
        String noteName = ParamUtil.getString(resourceRequest, "clientCode");
        Date date = new Date(new java.util.Date().getTime());

        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.applyLocalizedPattern("dd-MM-yyyy");
        noteName += "_" + sdf.format(date) + ".html";
        
        String note = ParamUtil.getString(resourceRequest, "descriptionEditor");
        this.logger.info(note);
        try {
            File tempFile = this.createFile(noteName, note);
            long fileEntryId = addToDL(tempFile, resourceRequest); 
            if (fileEntryId != -1) {
                User agent = UserLocalServiceUtil.getUser(Long.parseLong(
                        resourceRequest.getRemoteUser()));
                sendMail(agent, fileEntryId);
                return new Response(Response.Code.OK, "");
            } else {
                return new Response(Response.Code.GENERIC_ERROR, "Error code:"
                        + " 1");
            }
        } catch (IOException | NumberFormatException | PortalException
                | SystemException | AddressException e) {
            this.logger.error(e.getMessage());
            e.printStackTrace();
            return new Response(Response.Code.SAVE_NOTE_ERROR, e.getMessage());
        }
    }


    /**
     * Sends email to company when agent creates a new order.
     * 
     * @param agent
     *            name
     * @param editFlag
     *            true if agent is updating an existing order
     * @param numeroOrdine
     *            order number
     * @throws AddressException
     *             address exception
     */
    private void sendMail(final User agent, final boolean editFlag,
            final String numeroOrdine) throws AddressException {

        try {
            ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
                    agent.getCompanyId(),
                    ClassNameLocalServiceUtil.getClassNameId(User.class),
                    "CUSTOM_FIELDS", Constants.CODICE_AZIENDA,
                    agent.getUserId());
            long codiceAzienda = Long.parseLong(value.getData());
            User company = UserLocalServiceUtil.getUser(codiceAzienda);
            MailMessage mailMessage = new MailMessage();

            String action = "aggiornato: ";
            String subject = "Notifica modifica ordine.";

            if (!editFlag) {
                action = "creato: ";
                subject = "Notifica creazione documento.";
            }

            String body = ContentUtil.get("/content/email.tmpl", true);
            body = StringUtil.replace(
                    body,
                    new String[] { "[$AZIENDA$]", "[$AGENTE$]", "[$ACTION$]",
                            "[$ORDER_NUMBER$]" },
                    new String[] {
                            company.getFirstName() + " "
                                    + company.getLastName(),
                            agent.getFirstName() + " " + agent.getLastName(),
                            action, numeroOrdine });

            mailMessage.setBody(body);

            mailMessage.setSubject(subject);
            mailMessage.setFrom(new InternetAddress(agent.getEmailAddress()));
            mailMessage.setTo(new InternetAddress(company.getEmailAddress()));
            MailServiceUtil.sendEmail(mailMessage);

            this.logger.debug(("FROM: " + mailMessage.getFrom().getAddress()));
            this.logger.debug(("TO: " + mailMessage.getTo()[0].getAddress()));
        } catch (SystemException | PortalException e) {
            this.logger.warn("Could not send notification email!\n"
                    + e.getMessage());
        }

    }
    
    /**
     * Sends email to company when agent creates a new order.
     * 
     * @param agent
     *            name
     * @param fileEntryId
     *            note entry id
     * @throws AddressException
     *             address exception
     * @throws IOException 
     */
    private void sendMail(final User agent, final long fileEntryId)
            throws AddressException, IOException {
        
        try {
            ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
                    agent.getCompanyId(),
                    ClassNameLocalServiceUtil.getClassNameId(User.class),
                    "CUSTOM_FIELDS", Constants.CODICE_AZIENDA,
                    agent.getUserId());
            long codiceAzienda = Long.parseLong(value.getData());
            User company = UserLocalServiceUtil.getUser(codiceAzienda);
            MailMessage mailMessage = new MailMessage();

            String action = "creato: ";
            String subject = "Notifica creazione documento note.";

            String body = ContentUtil.get("/content/note-email.tmpl", true);
            body = StringUtil.replace(
                    body,
                    new String[] { "[$AZIENDA$]", "[$AGENTE$]", "[$ACTION$]"},
                    new String[] {
                            company.getFirstName() + " "
                                    + company.getLastName(),
                            agent.getFirstName() + " " + agent.getLastName(),
                            action });

            mailMessage.setBody(body);

            mailMessage.setSubject(subject);
            mailMessage.setFrom(new InternetAddress(agent.getEmailAddress()));
            mailMessage.setTo(new InternetAddress(company.getEmailAddress()));
            DLFileEntry fileEntry = DLFileEntryLocalServiceUtil
                    .getFileEntry(fileEntryId);
            File f = DLFileEntryLocalServiceUtil.getFile(agent.getUserId(),
                    fileEntryId, "1.0", true);
            File file = new File(System.getProperty("java.io.tmpdir")
                    + File.separator + fileEntry.getTitle());
            FileUtil.copyFile(f, file);
            mailMessage.addFileAttachment(file);
            MailServiceUtil.sendEmail(mailMessage);

            this.logger.debug(("FROM: " + mailMessage.getFrom().getAddress()));
            this.logger.debug(("TO: " + mailMessage.getTo()[0].getAddress()));
        } catch (SystemException | PortalException e) {
            this.logger.warn("Could not send notification email!\n"
                    + e.getMessage());
        }
        
    }
    
    /**
     * Creates a temporary that will be stored in Document library folder.
     * 
     * @param noteName note file name
     * @param content
     *            file content
     * @return return the temporary created file
     * @throws IOException
     *             exception
     */
    private File createFile(final String noteName, final String content)
            throws IOException {

        // Where should we store this file?
        File tempFile = new File(noteName);

        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            fw = new FileWriter(tempFile);
            bw = new BufferedWriter(fw);
            bw.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return tempFile;
    }
}
