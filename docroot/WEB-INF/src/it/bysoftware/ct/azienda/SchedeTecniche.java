package it.bysoftware.ct.azienda;

import it.bysoftware.ct.model.Articoli;
import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.utils.Constants;
import it.bysoftware.ct.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class SchedeTecniche.
 */
public class SchedeTecniche extends MVCPortlet {

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(SchedeTecniche.class);

    /**
     * Activates or Deactivates a user based on his current state.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     */
    public final void deleteSheet(final ActionRequest areq,
            final ActionResponse ares) {
        this.logger.info("deleteSheet() called");
    }

    /**
     * Activates or Deactivates a user based on his current state.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     */
    public final void upload(final ActionRequest areq,
            final ActionResponse ares) {
        this.logger.debug("upload() called");
        UploadPortletRequest uploadRequest = PortalUtil
                .getUploadPortletRequest(areq);

        File tmpFolder = new File(System.getProperty("java.io.tmpdir"));

        String itemCode = uploadRequest.getParameter("itemCode");
        String sourceFileName = uploadRequest.getFileName("fileupload");
        String origFileEntryId = uploadRequest.getParameter("origId");
        
        // Check minimum 1GB storage space to save new files...
        if (tmpFolder.getUsableSpace() < Constants.ONE_GB) {
            this.logger.warn("No enough space in: "
                    + tmpFolder.getAbsolutePath());
            SessionErrors.add(areq, "disk-space");
            ares.setRenderParameter("itemCode", itemCode);
            ares.setRenderParameter("itemSheetId", origFileEntryId);
        } else if (uploadRequest.getSize("fileupload") == 0) {
            this.logger.warn("Uploaded an empty file.");
            SessionErrors.add(areq, "empty-file");
            ares.setRenderParameter("itemCode", itemCode);
            ares.setRenderParameter("itemSheetId", origFileEntryId);
        } else {
            File file = uploadRequest.getFile("fileupload");
            try {
                File uploadedFile = this.uploadFile(file, tmpFolder);

                ThemeDisplay themeDisplay = (ThemeDisplay) uploadRequest
                        .getAttribute(WebKeys.THEME_DISPLAY);

                User company = themeDisplay.getUser();

                long groupId = themeDisplay.getLayout().getGroupId();
                
                DLFolder companyFolder = Utils.getAziendaFolder(groupId,
                        company);
                this.logger.debug("Company FOLDER: " + companyFolder);
                DLFolder itemSheetFolder = Utils.getgetItemSheetFolder(groupId,
                        companyFolder);
                this.logger.debug("Item Sheet FOLDER: " + itemSheetFolder);
                FileEntry fileEntry = null;
                try {
                    if (origFileEntryId != null && !origFileEntryId.isEmpty()
                            && !"-1".equals(origFileEntryId)) {
                        fileEntry = DLAppServiceUtil.getFileEntry(Long
                                .parseLong(origFileEntryId));
                    }
                    this.logger
                            .debug("Entry found, the file will be replaced.");
                } catch (PortalException e) {
                    this.logger
                            .debug("File Entry not found, a new file will be"
                                    + " created." + sourceFileName);
                }

                if (fileEntry != null) {
                    DLAppServiceUtil
                            .deleteFileEntry(fileEntry.getFileEntryId());
                }
                long repositoryId = themeDisplay.getScopeGroupId();
                fileEntry = DLAppServiceUtil.addFileEntry(repositoryId,
                        itemSheetFolder.getFolderId(), sourceFileName,
                        MimeTypesUtil.getContentType(uploadedFile), itemCode,
                        "", "", uploadedFile, new ServiceContext());

                Utils.setFilePermissions(fileEntry);
                this.logger.info("Added: " + fileEntry.getTitle());
                Articoli a = ArticoliLocalServiceUtil.getArticoli(itemCode);
                a.setLiberoString5(String.valueOf(fileEntry.getFileEntryId()));
                ArticoliLocalServiceUtil.updateArticoli(a);
                ares.setRenderParameter("itemSheetId", a.getLiberoString5());
            } catch (PortalException | SystemException | IOException e) {
                this.logger.error(e.getMessage());
                e.printStackTrace();
            }
        }
        PortalUtil.copyRequestParameters(areq, ares);
        ares.setRenderParameter("jspPage",
                "/jsps/schedetecniche/edit-item-sheet.jsp");
    }

    /**
     * Uploads file to server temporary folder.
     * 
     * @param file
     *            user uploaded {@link File}
     * @param tmpFolder
     *            system temporary folder
     * @return return the temporary uploaded file
     * @throws IOException
     *             exception
     */
    private File uploadFile(final File file, final File tmpFolder)
            throws IOException {

        // Where should we store this file?
        File tempFile = new File(UUID.randomUUID().toString());

        // This is our final file path.
        File filePath = new File(tmpFolder.getAbsolutePath() + File.separator
                + tempFile.getName());

        FileUtil.copyFile(file, filePath);
        return filePath;
    }
}
