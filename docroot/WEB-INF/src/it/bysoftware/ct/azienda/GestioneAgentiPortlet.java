/***********************************************************************
 * Copyright (c) 2016:
 * BySoftware Soc. coop. a.r.l., Belpasso (CT) - Italy
 *
 * See http://www.bysoftware.ct.it for details on the copyright holder.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***********************************************************************/

package it.bysoftware.ct.azienda;

import it.bysoftware.ct.utils.Constants;

import java.util.Calendar;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.RoleServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.model.DLFolderConstants;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
import com.liferay.portlet.expando.model.ExpandoColumn;
import com.liferay.portlet.expando.model.ExpandoTable;
import com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoTableLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Class that implements the portlet logic. It extends the {@link MVCPortlet}
 *
 * @author Mario Torrisi torrisi.mario@gmail.com
 */
public class GestioneAgentiPortlet extends MVCPortlet {

    /**
     * Default user birth year.
     */
    private static final int BIRTH_YEAR = 1970;

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(GestioneAgentiPortlet.class);

    /**
     * Activates or Deactivates a user based on his current state.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     */
    public final void toggleAgent(final ActionRequest areq,
            final ActionResponse ares) {

        ParamUtil.getLong(areq, "userId", -1);
        try {
            User agent = UserLocalServiceUtil.getUser(ParamUtil.getLong(areq,
                    "userId"));
            if (agent.isActive()) {
                this.logger.debug("Agent: " + agent.getUserId() + " status: "
                        + agent.isActive());
                UserLocalServiceUtil
                        .updateStatus(agent.getUserId(),
                                WorkflowConstants.STATUS_INACTIVE,
                                new ServiceContext());
                this.logger.debug("Agent: " + agent.getUserId() + " status: "
                        + agent.isActive());
            } else {
                this.logger.debug("Agent: " + agent.getUserId() + " status: "
                        + agent.isActive());
                UserLocalServiceUtil
                        .updateStatus(agent.getUserId(),
                                WorkflowConstants.STATUS_APPROVED,
                                new ServiceContext());
                this.logger.debug("Agent: " + agent.getUserId() + " status: "
                        + agent.isActive());
            }
        } catch (PortalException ex) {
            this.logger.error(ex.getMessage());
        } catch (SystemException ex) {
            this.logger.error(ex.getMessage());
        }
    }

    /**
     * Creates a new agent.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     */
    public final void saveAgent(final ActionRequest areq,
            final ActionResponse ares) {

        long userId = ParamUtil.getLong(areq, "id", -1);
        String agentCode = ParamUtil.getString(areq, "agentCode");
        String firstName = ParamUtil.getString(areq, "firstName");
        String lastName = ParamUtil.getString(areq, "lastName");
        String email = ParamUtil.getString(areq, "email");
        String password = ParamUtil.getString(areq, "password");
        String folder = ParamUtil.getString(areq, "folder");

        ThemeDisplay themeDisplay = (ThemeDisplay) areq
                .getAttribute(WebKeys.THEME_DISPLAY);

        this.logger.debug("userID: " + userId); 
        try {
            User creator = PortalUtil.getUser(areq);
            if (userId == -1) { // New Agent
                try {

                    Role role = RoleServiceUtil.getRole(creator.getCompanyId(),
                            "agente");

                    User liferayUser = this.addLiferayUser(firstName, lastName,
                            email, password, folder, creator, role.getRoleId());
                    this.logger.info("Inserted Liferay user: " + liferayUser);

                    DLFolder companyFolder = this.getCompanyFolder(creator,
                            themeDisplay);
                    DLFolder dlFolder = this.createAgentFolder(liferayUser,
                            companyFolder, folder, themeDisplay);
                    this.logger.info("Created folder: " + dlFolder);

                    ExpandoTable table = ExpandoTableLocalServiceUtil
                            .getDefaultTable(liferayUser.getCompanyId(),
                                    User.class.getName());
                    ExpandoColumn column = ExpandoColumnLocalServiceUtil
                            .getColumn(table.getTableId(),
                                    Constants.CODICE_AGENTE);
                    ExpandoValueLocalServiceUtil.addValue(
                            liferayUser.getCompanyId(), User.class.getName(),
                            table.getName(), column.getName(),
                            liferayUser.getUserId(), agentCode);
                    column = ExpandoColumnLocalServiceUtil.getColumn(
                            table.getTableId(), Constants.CODICE_AZIENDA);
                    ExpandoValueLocalServiceUtil.addValue(
                            liferayUser.getCompanyId(), User.class.getName(),
                            table.getName(), column.getName(),
                            liferayUser.getUserId(), creator.getUserId());

                } catch (SystemException | PortalException ex) {
                    ex.printStackTrace();
                    this.logger.error(ex.getMessage());
                    SessionErrors.add(areq, "no-registration");
                }

            } else { // Update Agent
                User liferayUser = UserLocalServiceUtil.getUser(userId);
                liferayUser.setFirstName(firstName);
                liferayUser.setFirstName(lastName);
                liferayUser.setPassword(password);
                ExpandoTable table = ExpandoTableLocalServiceUtil
                        .getDefaultTable(liferayUser.getCompanyId(),
                                User.class.getName());
                ExpandoColumn column = ExpandoColumnLocalServiceUtil.getColumn(
                        table.getTableId(), Constants.CODICE_AGENTE);
                ExpandoValueLocalServiceUtil.addValue(
                        liferayUser.getCompanyId(), User.class.getName(),
                        table.getName(), column.getName(),
                        liferayUser.getUserId(), agentCode);
                UserLocalServiceUtil.updateUser(liferayUser);
            }
        } catch (SystemException | PortalException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    /**
     * Updates an existing agent.
     * 
     * @param areq
     *            {@link ActionRequest}
     * @param ares
     *            {@link ActionResponse}
     */
    public final void updateAgent(final ActionRequest areq,
            final ActionResponse ares) {

    }

    /**
     * Actual adds a new user as portal user.
     * 
     * @param firstName
     *            user first name
     * @param lastName
     *            user first name
     * @param email
     *            user email
     * @param password
     *            user password
     * @param screenName
     *            user username
     * @param creator
     *            user creator
     * @param roleId
     *            user role
     * @return {@link User} the just created user
     * @throws PortalException
     *             if a {@link PortalException} occurs
     * @throws SystemException
     *             if any error occurs
     */
    private User addLiferayUser(final String firstName, final String lastName,
            final String email, final String password, final String screenName,
            final User creator, final long roleId) throws PortalException,
            SystemException {

        long[] emptyLong = {};
        long[] rolesId = { roleId };
        User user = UserLocalServiceUtil.addUser(creator.getUserId(),
                creator.getCompanyId(), false, password, password, false,
                screenName, email, 0, "", Locale.ITALIAN, firstName, "",
                lastName, 0, 0, true, 1, 1, BIRTH_YEAR, "", emptyLong,
                emptyLong, rolesId, emptyLong, false, null);

        return user;

    }

    /**
     * Returns the company folder where store all agent documents.
     * 
     * @param company
     *            {@link User} the company the own the portal
     * @param themeDisplay
     *            {@link ThemeDisplay} object
     * @return returns the just created {@link DLFolder}
     */
    private DLFolder getCompanyFolder(final User company,
            final ThemeDisplay themeDisplay) {
        long groupId = themeDisplay.getLayout().getGroupId();
        long repositoryId = themeDisplay.getScopeGroupId();
        DLFolder folder = null;
        try {
            this.logger.info("Loading Company folder");
            folder = DLFolderLocalServiceUtil.getFolder(groupId,
                    DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,
                    company.getScreenName());
        } catch (PortalException ex) {
            this.logger.warn("Cannot find OP root folder. Creating... ");
            try {

                folder = DLFolderLocalServiceUtil.addFolder(
                        company.getUserId(), groupId, repositoryId, false,
                        DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,
                        company.getScreenName(),
                        "Directory di" + company.getFirstName(), false,
                        new ServiceContext());
            } catch (PortalException e) {
                this.logger.error(e.getMessage());
            } catch (SystemException e) {
                this.logger.error(e.getMessage());
            }
        } catch (SystemException ex) {
            this.logger.error(ex.getMessage());
        }
        return folder;
    }

    /**
     * Creates a new {@link DLFolder} for the agent.
     * 
     * @param agent
     *            the agent owner of the folder
     * @param parentFolder
     *            the company folder
     * @param folder
     *            the agent folder name
     * @param themeDisplay
     *            {@link ThemeDisplay} object
     * @return the just created {@link DLFolder}
     */
    private DLFolder createAgentFolder(final User agent,
            final DLFolder parentFolder, final String folder,
            final ThemeDisplay themeDisplay) {
        long groupId = themeDisplay.getLayout().getGroupId();
        long repositoryId = themeDisplay.getScopeGroupId();
        DLFolder agentFolder = null;

        try {

            agentFolder = DLFolderLocalServiceUtil.addFolder(agent.getUserId(),
                    groupId, repositoryId, false, parentFolder.getFolderId(),
                    folder, "Directory di " + agent.getFirstName(), false,
                    new ServiceContext());

            Role agentRole = RoleLocalServiceUtil.getRole(agent.getCompanyId(),
                    "Azienda");
            String[] actionsRW = new String[] { ActionKeys.VIEW };

            ResourcePermissionLocalServiceUtil.setResourcePermissions(
                    agent.getCompanyId(),
                    "com.liferay.portlet.documentlibrary.model.DLFolder",
                    ResourceConstants.SCOPE_INDIVIDUAL,
                    "" + agentFolder.getFolderId(), agentRole.getRoleId(),
                    actionsRW);

            // Create document subfolders...

            DLFolder yearFolder = DLFolderLocalServiceUtil.addFolder(
                    agent.getUserId(), groupId, repositoryId, false,
                    agentFolder.getFolderId(),
                    String.valueOf(Calendar.getInstance().get(Calendar.YEAR)),
                    "Directory di " + agent.getFirstName(), false,
                    new ServiceContext());
            ResourcePermissionLocalServiceUtil.setResourcePermissions(
                    agent.getCompanyId(),
                    "com.liferay.portlet.documentlibrary.model.DLFolder",
                    ResourceConstants.SCOPE_INDIVIDUAL,
                    "" + yearFolder.getFolderId(), agentRole.getRoleId(),
                    actionsRW);
            this.logger.debug("Created yearFolder: " + yearFolder);

        } catch (PortalException ex) {
            ex.printStackTrace();
            this.logger.error(ex.getMessage());
        } catch (SystemException ex) {
            ex.printStackTrace();
            this.logger.error(ex.getMessage());
        }
        return agentFolder;
    }
}
