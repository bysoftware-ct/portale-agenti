/***********************************************************************
 * Copyright (c) 2016:
 * BySoftware Soc. coop. a.r.l., Belpasso (CT) - Italy
 *
 * See http://www.bysoftware.ct.it for details on the copyright holder.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***********************************************************************/
package it.bysoftware.ct.azienda;

import it.bysoftware.ct.agenti.ResourceId;
import it.bysoftware.ct.model.WKOrdiniClienti;
import it.bysoftware.ct.model.WKRigheOrdiniClienti;
import it.bysoftware.ct.service.WKOrdiniClientiLocalServiceUtil;
import it.bysoftware.ct.service.WKRigheOrdiniClientiLocalServiceUtil;
import it.bysoftware.ct.service.persistence.WKOrdiniClientiPK;
import it.bysoftware.ct.utils.Constants;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.ServletResponseUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class GestioneDocumenti {@link MVCPortlet}.
 * 
 * @author Mario Torrisi torrisi.mario@gmail.com
 */
public class GestioneDocumenti extends MVCPortlet {

    /**
     * Constant to take default value.
     */
    private static final String LONG_999999999 = "-999999999";

    /**
     * Constant to take default value.
     */
    private static final String SHORT_999 = "-999";

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private final Log logger = LogFactoryUtil.getLog(GestioneDocumenti.class);

    @Override
    public final void serveResource(final ResourceRequest resourceRequest,
            final ResourceResponse resourceResponse) {

        String resourceID = resourceRequest.getResourceID();

        switch (ResourceId.valueOf(resourceID)) {
            case exportOrder:
                this.logger.debug("Exporting order number: "
                        + ParamUtil.getInteger(resourceRequest,
                                "numeroOrdine"));
                String tracciato;
                try {
                    tracciato = this.creaFileTracciato(
                            ParamUtil.getInteger(resourceRequest, "anno"),
                            ParamUtil.getInteger(resourceRequest, "tipoOrdine"),
                            ParamUtil.getInteger(resourceRequest,
                                    "numeroOrdine"));
    
                    File file = new File(tracciato);
                    InputStream in = new FileInputStream(file);
                    HttpServletResponse httpRes = PortalUtil.
                            getHttpServletResponse(resourceResponse);
                    HttpServletRequest httpReq = PortalUtil.
                            getHttpServletRequest(resourceRequest);
                    ServletResponseUtil.sendFile(httpReq, httpRes,
                            file.getName(), in, "application/download");
    
                    in.close();
                } catch (IOException e) {
                    this.logger.error(e.getMessage());
                }
                break;
            default:
                break;
        }

    }

    /**
     * Creates file containing the order details.
     * 
     * @param anno
     *            order year
     * @param tipoOrdine
     *            order type
     * @param numeroOrdine
     *            order number
     * 
     * @return file path
     */
    private String creaFileTracciato(final int anno, final int tipoOrdine,
            final int numeroOrdine) {

        String fileName;
        try {
            WKOrdiniClienti ordine = WKOrdiniClientiLocalServiceUtil.
                    fetchWKOrdiniClienti(new WKOrdiniClientiPK(anno,
                            tipoOrdine, numeroOrdine));
            try {
                String filePath = "/tmp/" + ordine.getAnno() + "_"
                        + ordine.getCodiceCliente() + "_"
                        + ordine.getNumeroOrdine() + ".txt";
                File file = new File(filePath);
                if (!file.exists()) {
                    file.createNewFile();
                } else {
                    Path path = FileSystems.getDefault().getPath(filePath);
                    Files.deleteIfExists(path);
                    this.logger.info("Deleted : " + filePath);
                }
                FileWriter fw = new FileWriter(file.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);

                String stringTestata = this.creaRigoTestata(ordine);

                bw.write(stringTestata);
                List<WKRigheOrdiniClienti> righeOrdine =
                        WKRigheOrdiniClientiLocalServiceUtil.
                        getByAnnoTipoNumeroOrdine(ordine.getAnno(),
                                Constants.TIPO_ORDINE,
                                ordine.getNumeroOrdine());
                for (WKRigheOrdiniClienti rigoOrdine : righeOrdine) {
                    String rigo = "";
                    switch (rigoOrdine.getTipoRigo()) {
                        case 0:
                            rigo = this.createNormalRow(rigoOrdine);
                            break;
                        case 1:
                            break;
                        case 2:
                            rigo = this.createDescrptiveRow(rigoOrdine);
                            break;
                        default:
                            break;
                    }
                    bw.write(rigo);
                }
                bw.close();
                ordine.setStatoOrdine(true);
                ordine.setDataRegistrazione(new Date());
                WKOrdiniClientiLocalServiceUtil.updateWKOrdiniClienti(ordine);
                fileName = file.getAbsolutePath();
            } catch (IOException e) {
                this.logger.error(e.getMessage());
                fileName = null;
            }
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            fileName = null;
        }
        return fileName;
    }

    /**
     * Creates header row.
     * 
     * @param ordine
     *            order data
     * @return header row
     */
    private String creaRigoTestata(final WKOrdiniClienti ordine) {
        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.applyLocalizedPattern("dd/MM/yyyy");
        String dataDoc = sdf.format(ordine.getDataDocumento());
        String dataReg = "";
        if (ordine.getDataRegistrazione() != null) {
            dataReg = sdf.format(ordine.getDataRegistrazione());
        } else {
            dataReg = sdf.format(new Date());
        }
        return "\"Testata" + Constants.SEPARATOR + ordine.getTipoDocumento()
                + Constants.SEPARATOR + dataReg + Constants.SEPARATOR
                + ordine.getCodiceCliente() + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + dataDoc + Constants.SEPARATOR
                + Constants.STRING_DEFAULT + Constants.SEPARATOR + -1
                + Constants.SEPARATOR + Constants.STRING_DEFAULT
                + Constants.SEPARATOR + -1 + Constants.SEPARATOR
                + Constants.SEPARATOR + Constants.SEPARATOR
                + Constants.SEPARATOR + Constants.SEPARATOR
                + ordine.getCodicePianoPag() + Constants.SEPARATOR
                + Constants.STRING_DEFAULT + Constants.SEPARATOR
                + Constants.STRING_DEFAULT + Constants.SEPARATOR
                + Constants.STRING_DEFAULT + Constants.SEPARATOR
                + Constants.STRING_DEFAULT + Constants.SEPARATOR
                + ordine.getCodiceAgente() + Constants.SEPARATOR
                + ordine.getCodiceAgente() + "/" + ordine.getNumeroOrdine()
                + Constants.SEPARATOR + Constants.STRING_DEFAULT
                + Constants.SEPARATOR + Constants.STRING_DEFAULT
                + Constants.SEPARATOR + Constants.STRING_DEFAULT
                + Constants.SEPARATOR + Constants.STRING_DEFAULT
                + Constants.SEPARATOR + Constants.STRING_DEFAULT
                + Constants.SEPARATOR + Constants.STRING_DEFAULT
                + Constants.SEPARATOR + Constants.SEPARATOR
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR + SHORT_999
                + Constants.SEPARATOR + SHORT_999 + Constants.SEPARATOR
                + SHORT_999 + Constants.SEPARATOR + SHORT_999
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR
                + Constants.SEPARATOR + Constants.SEPARATOR
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + Constants.SEPARATOR
                + Constants.SEPARATOR + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR
                + Constants.SEPARATOR + Constants.STRING_DEFAULT
                + Constants.SEPARATOR + Constants.STRING_DEFAULT
                + Constants.SEPARATOR + Constants.STRING_DEFAULT
                + Constants.SEPARATOR + Constants.STRING_DEFAULT
                + Constants.SEPARATOR + Constants.SEPARATOR
                + Constants.SEPARATOR + "9999" + Constants.SEPARATOR
                + "9999999" + Constants.SEPARATOR + Constants.SEPARATOR
                + LONG_999999999 + Constants.SEPARATOR + LONG_999999999
                + Constants.SEPARATOR + LONG_999999999 + Constants.SEPARATOR
                + 0 + Constants.SEPARATOR + 0 + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + Constants.STRING_DEFAULT
                + Constants.SEPARATOR + "\"\r\n";
    }

    /**
     * Creates a normal row.
     * 
     * @param rigoOrdine
     *            the {@link WKRigheOrdiniClienti} to convert in ASCII
     * @return the converted {@link WKRigheOrdiniClienti}
     */
    private String createNormalRow(final WKRigheOrdiniClienti rigoOrdine) {
        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.applyLocalizedPattern("dd/MM/yyyy");
        String dataCon = "";
        if (rigoOrdine.getDataPrevistaConsegna() != null) {
            dataCon =  sdf.format(rigoOrdine.getDataPrevistaConsegna());
        } else {
            dataCon = "";
        }
        return "\"RigaNormale" + Constants.SEPARATOR
                + rigoOrdine.getCodiceArticolo() + Constants.SEPARATOR
                + rigoOrdine.getQuantita() + Constants.SEPARATOR
                + rigoOrdine.getCodiceVariante() + Constants.SEPARATOR
                + Constants.STRING_DEFAULT + Constants.SEPARATOR
                + Constants.SEPARATOR + Constants.STRING_DEFAULT
                + Constants.SEPARATOR + Constants.STRING_DEFAULT
                + Constants.SEPARATOR + Constants.STRING_DEFAULT
                + Constants.SEPARATOR + Constants.SEPARATOR
                + rigoOrdine.getDescrizione() + Constants.SEPARATOR
                + Constants.SEPARATOR + LONG_999999999 + Constants.SEPARATOR
                + LONG_999999999 + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR
                + rigoOrdine.getPrezzo() + Constants.SEPARATOR
                + rigoOrdine.getSconto1() + Constants.SEPARATOR
                + rigoOrdine.getSconto2() + Constants.SEPARATOR
                + rigoOrdine.getSconto3() + Constants.SEPARATOR
                + Constants.STRING_DEFAULT + Constants.SEPARATOR
                + Constants.STRING_DEFAULT + Constants.SEPARATOR
                + dataCon + Constants.SEPARATOR 
                + -1 + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + Constants.SEPARATOR
                + Constants.SEPARATOR + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR
                + Constants.SEPARATOR + Constants.SEPARATOR
                + Constants.SEPARATOR + Constants.SEPARATOR
                + Constants.SEPARATOR + Constants.SEPARATOR
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR
                + Constants.SEPARATOR + "\"\r\n";
    }

    /**
     * Creates a descriptive row.
     * 
     * @param rigoOrdine
     *            the {@link WKRigheOrdiniClienti} to convert in ASCII
     * @return the converted {@link WKRigheOrdiniClienti}
     */
    private String createDescrptiveRow(final WKRigheOrdiniClienti rigoOrdine) {
        return "\"RigaDescrittiva" + Constants.SEPARATOR + Constants.SEPARATOR
                + rigoOrdine.getDescrizione() + Constants.SEPARATOR
                + Constants.SEPARATOR + Constants.SEPARATOR
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + 0 + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + Constants.SEPARATOR
                + Constants.SEPARATOR + Constants.SEPARATOR + 0
                + Constants.SEPARATOR + Constants.SEPARATOR + "\"\r\n";
    }
}
