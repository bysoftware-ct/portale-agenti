/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.OrdiniClienti;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing OrdiniClienti in entity cache.
 *
 * @author Mario Torrisi
 * @see OrdiniClienti
 * @generated
 */
public class OrdiniClientiCacheModel implements CacheModel<OrdiniClienti>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(159);

		sb.append("{anno=");
		sb.append(anno);
		sb.append(", codiceAttivita=");
		sb.append(codiceAttivita);
		sb.append(", codiceCentro=");
		sb.append(codiceCentro);
		sb.append(", codiceDeposito=");
		sb.append(codiceDeposito);
		sb.append(", tipoOrdine=");
		sb.append(tipoOrdine);
		sb.append(", numeroOrdine=");
		sb.append(numeroOrdine);
		sb.append(", tipoDocumento=");
		sb.append(tipoDocumento);
		sb.append(", codiceContAnalitica=");
		sb.append(codiceContAnalitica);
		sb.append(", idTipoDocumento=");
		sb.append(idTipoDocumento);
		sb.append(", statoOrdine=");
		sb.append(statoOrdine);
		sb.append(", descrEstremiDoc=");
		sb.append(descrEstremiDoc);
		sb.append(", tipoSoggetto=");
		sb.append(tipoSoggetto);
		sb.append(", codiceCliente=");
		sb.append(codiceCliente);
		sb.append(", naturaTransazione=");
		sb.append(naturaTransazione);
		sb.append(", codiceEsenzione=");
		sb.append(codiceEsenzione);
		sb.append(", codiceDivisa=");
		sb.append(codiceDivisa);
		sb.append(", valoreCambio=");
		sb.append(valoreCambio);
		sb.append(", dataValoreCambio=");
		sb.append(dataValoreCambio);
		sb.append(", codicePianoPag=");
		sb.append(codicePianoPag);
		sb.append(", inizioCalcoloPag=");
		sb.append(inizioCalcoloPag);
		sb.append(", percentualeScontoMaggiorazione=");
		sb.append(percentualeScontoMaggiorazione);
		sb.append(", percentualeScontoProntaCassa=");
		sb.append(percentualeScontoProntaCassa);
		sb.append(", percentualeProvvChiusura=");
		sb.append(percentualeProvvChiusura);
		sb.append(", dataDocumento=");
		sb.append(dataDocumento);
		sb.append(", dataRegistrazione=");
		sb.append(dataRegistrazione);
		sb.append(", causaleEstrattoConto=");
		sb.append(causaleEstrattoConto);
		sb.append(", dataPrimaRata=");
		sb.append(dataPrimaRata);
		sb.append(", dataUltimaRata=");
		sb.append(dataUltimaRata);
		sb.append(", codiceBanca=");
		sb.append(codiceBanca);
		sb.append(", codiceAgenzia=");
		sb.append(codiceAgenzia);
		sb.append(", codiceAgente=");
		sb.append(codiceAgente);
		sb.append(", codiceGruppoAgenti=");
		sb.append(codiceGruppoAgenti);
		sb.append(", codiceZona=");
		sb.append(codiceZona);
		sb.append(", codiceSpedizione=");
		sb.append(codiceSpedizione);
		sb.append(", codicePorto=");
		sb.append(codicePorto);
		sb.append(", codiceDestinatario=");
		sb.append(codiceDestinatario);
		sb.append(", codiceListino=");
		sb.append(codiceListino);
		sb.append(", codiceLingua=");
		sb.append(codiceLingua);
		sb.append(", numeroDecPrezzo=");
		sb.append(numeroDecPrezzo);
		sb.append(", note=");
		sb.append(note);
		sb.append(", percentualeSpeseTrasp=");
		sb.append(percentualeSpeseTrasp);
		sb.append(", speseTrasporto=");
		sb.append(speseTrasporto);
		sb.append(", speseImballaggio=");
		sb.append(speseImballaggio);
		sb.append(", speseVarie=");
		sb.append(speseVarie);
		sb.append(", speseBanca=");
		sb.append(speseBanca);
		sb.append(", curaTrasporto=");
		sb.append(curaTrasporto);
		sb.append(", causaleTrasporto=");
		sb.append(causaleTrasporto);
		sb.append(", aspettoEstriore=");
		sb.append(aspettoEstriore);
		sb.append(", vettore1=");
		sb.append(vettore1);
		sb.append(", vettore2=");
		sb.append(vettore2);
		sb.append(", vettore3=");
		sb.append(vettore3);
		sb.append(", numeroColli=");
		sb.append(numeroColli);
		sb.append(", pesoLordo=");
		sb.append(pesoLordo);
		sb.append(", pesoNetto=");
		sb.append(pesoNetto);
		sb.append(", volume=");
		sb.append(volume);
		sb.append(", numeroCopie=");
		sb.append(numeroCopie);
		sb.append(", numeroCopieStampate=");
		sb.append(numeroCopieStampate);
		sb.append(", inviatoEmail=");
		sb.append(inviatoEmail);
		sb.append(", nomePDF=");
		sb.append(nomePDF);
		sb.append(", riferimentoOrdine=");
		sb.append(riferimentoOrdine);
		sb.append(", dataConferma=");
		sb.append(dataConferma);
		sb.append(", confermaStampata=");
		sb.append(confermaStampata);
		sb.append(", totaleOrdine=");
		sb.append(totaleOrdine);
		sb.append(", codiceIVATrasp=");
		sb.append(codiceIVATrasp);
		sb.append(", codiceIVAImballo=");
		sb.append(codiceIVAImballo);
		sb.append(", codiceIVAVarie=");
		sb.append(codiceIVAVarie);
		sb.append(", codiceIVABanca=");
		sb.append(codiceIVABanca);
		sb.append(", libStr1=");
		sb.append(libStr1);
		sb.append(", libStr2=");
		sb.append(libStr2);
		sb.append(", libStr3=");
		sb.append(libStr3);
		sb.append(", libDbl1=");
		sb.append(libDbl1);
		sb.append(", libDbl2=");
		sb.append(libDbl2);
		sb.append(", libDbl3=");
		sb.append(libDbl3);
		sb.append(", libDat1=");
		sb.append(libDat1);
		sb.append(", libDat2=");
		sb.append(libDat2);
		sb.append(", libDat3=");
		sb.append(libDat3);
		sb.append(", libLng1=");
		sb.append(libLng1);
		sb.append(", libLng2=");
		sb.append(libLng2);
		sb.append(", libLng3=");
		sb.append(libLng3);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public OrdiniClienti toEntityModel() {
		OrdiniClientiImpl ordiniClientiImpl = new OrdiniClientiImpl();

		ordiniClientiImpl.setAnno(anno);

		if (codiceAttivita == null) {
			ordiniClientiImpl.setCodiceAttivita(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceAttivita(codiceAttivita);
		}

		if (codiceCentro == null) {
			ordiniClientiImpl.setCodiceCentro(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceCentro(codiceCentro);
		}

		if (codiceDeposito == null) {
			ordiniClientiImpl.setCodiceDeposito(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceDeposito(codiceDeposito);
		}

		ordiniClientiImpl.setTipoOrdine(tipoOrdine);
		ordiniClientiImpl.setNumeroOrdine(numeroOrdine);

		if (tipoDocumento == null) {
			ordiniClientiImpl.setTipoDocumento(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setTipoDocumento(tipoDocumento);
		}

		if (codiceContAnalitica == null) {
			ordiniClientiImpl.setCodiceContAnalitica(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceContAnalitica(codiceContAnalitica);
		}

		ordiniClientiImpl.setIdTipoDocumento(idTipoDocumento);
		ordiniClientiImpl.setStatoOrdine(statoOrdine);

		if (descrEstremiDoc == null) {
			ordiniClientiImpl.setDescrEstremiDoc(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setDescrEstremiDoc(descrEstremiDoc);
		}

		ordiniClientiImpl.setTipoSoggetto(tipoSoggetto);

		if (codiceCliente == null) {
			ordiniClientiImpl.setCodiceCliente(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceCliente(codiceCliente);
		}

		if (naturaTransazione == null) {
			ordiniClientiImpl.setNaturaTransazione(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setNaturaTransazione(naturaTransazione);
		}

		if (codiceEsenzione == null) {
			ordiniClientiImpl.setCodiceEsenzione(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceEsenzione(codiceEsenzione);
		}

		if (codiceDivisa == null) {
			ordiniClientiImpl.setCodiceDivisa(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceDivisa(codiceDivisa);
		}

		ordiniClientiImpl.setValoreCambio(valoreCambio);

		if (dataValoreCambio == Long.MIN_VALUE) {
			ordiniClientiImpl.setDataValoreCambio(null);
		}
		else {
			ordiniClientiImpl.setDataValoreCambio(new Date(dataValoreCambio));
		}

		if (codicePianoPag == null) {
			ordiniClientiImpl.setCodicePianoPag(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodicePianoPag(codicePianoPag);
		}

		if (inizioCalcoloPag == Long.MIN_VALUE) {
			ordiniClientiImpl.setInizioCalcoloPag(null);
		}
		else {
			ordiniClientiImpl.setInizioCalcoloPag(new Date(inizioCalcoloPag));
		}

		ordiniClientiImpl.setPercentualeScontoMaggiorazione(percentualeScontoMaggiorazione);
		ordiniClientiImpl.setPercentualeScontoProntaCassa(percentualeScontoProntaCassa);
		ordiniClientiImpl.setPercentualeProvvChiusura(percentualeProvvChiusura);

		if (dataDocumento == Long.MIN_VALUE) {
			ordiniClientiImpl.setDataDocumento(null);
		}
		else {
			ordiniClientiImpl.setDataDocumento(new Date(dataDocumento));
		}

		if (dataRegistrazione == Long.MIN_VALUE) {
			ordiniClientiImpl.setDataRegistrazione(null);
		}
		else {
			ordiniClientiImpl.setDataRegistrazione(new Date(dataRegistrazione));
		}

		if (causaleEstrattoConto == null) {
			ordiniClientiImpl.setCausaleEstrattoConto(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCausaleEstrattoConto(causaleEstrattoConto);
		}

		if (dataPrimaRata == Long.MIN_VALUE) {
			ordiniClientiImpl.setDataPrimaRata(null);
		}
		else {
			ordiniClientiImpl.setDataPrimaRata(new Date(dataPrimaRata));
		}

		if (dataUltimaRata == Long.MIN_VALUE) {
			ordiniClientiImpl.setDataUltimaRata(null);
		}
		else {
			ordiniClientiImpl.setDataUltimaRata(new Date(dataUltimaRata));
		}

		if (codiceBanca == null) {
			ordiniClientiImpl.setCodiceBanca(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceBanca(codiceBanca);
		}

		if (codiceAgenzia == null) {
			ordiniClientiImpl.setCodiceAgenzia(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceAgenzia(codiceAgenzia);
		}

		if (codiceAgente == null) {
			ordiniClientiImpl.setCodiceAgente(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceAgente(codiceAgente);
		}

		if (codiceGruppoAgenti == null) {
			ordiniClientiImpl.setCodiceGruppoAgenti(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceGruppoAgenti(codiceGruppoAgenti);
		}

		if (codiceZona == null) {
			ordiniClientiImpl.setCodiceZona(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceZona(codiceZona);
		}

		if (codiceSpedizione == null) {
			ordiniClientiImpl.setCodiceSpedizione(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceSpedizione(codiceSpedizione);
		}

		if (codicePorto == null) {
			ordiniClientiImpl.setCodicePorto(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodicePorto(codicePorto);
		}

		if (codiceDestinatario == null) {
			ordiniClientiImpl.setCodiceDestinatario(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceDestinatario(codiceDestinatario);
		}

		if (codiceListino == null) {
			ordiniClientiImpl.setCodiceListino(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceListino(codiceListino);
		}

		if (codiceLingua == null) {
			ordiniClientiImpl.setCodiceLingua(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceLingua(codiceLingua);
		}

		ordiniClientiImpl.setNumeroDecPrezzo(numeroDecPrezzo);

		if (note == null) {
			ordiniClientiImpl.setNote(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setNote(note);
		}

		ordiniClientiImpl.setPercentualeSpeseTrasp(percentualeSpeseTrasp);
		ordiniClientiImpl.setSpeseTrasporto(speseTrasporto);
		ordiniClientiImpl.setSpeseImballaggio(speseImballaggio);
		ordiniClientiImpl.setSpeseVarie(speseVarie);
		ordiniClientiImpl.setSpeseBanca(speseBanca);

		if (curaTrasporto == null) {
			ordiniClientiImpl.setCuraTrasporto(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCuraTrasporto(curaTrasporto);
		}

		if (causaleTrasporto == null) {
			ordiniClientiImpl.setCausaleTrasporto(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCausaleTrasporto(causaleTrasporto);
		}

		if (aspettoEstriore == null) {
			ordiniClientiImpl.setAspettoEstriore(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setAspettoEstriore(aspettoEstriore);
		}

		if (vettore1 == null) {
			ordiniClientiImpl.setVettore1(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setVettore1(vettore1);
		}

		if (vettore2 == null) {
			ordiniClientiImpl.setVettore2(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setVettore2(vettore2);
		}

		if (vettore3 == null) {
			ordiniClientiImpl.setVettore3(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setVettore3(vettore3);
		}

		ordiniClientiImpl.setNumeroColli(numeroColli);
		ordiniClientiImpl.setPesoLordo(pesoLordo);
		ordiniClientiImpl.setPesoNetto(pesoNetto);
		ordiniClientiImpl.setVolume(volume);
		ordiniClientiImpl.setNumeroCopie(numeroCopie);
		ordiniClientiImpl.setNumeroCopieStampate(numeroCopieStampate);
		ordiniClientiImpl.setInviatoEmail(inviatoEmail);

		if (nomePDF == null) {
			ordiniClientiImpl.setNomePDF(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setNomePDF(nomePDF);
		}

		if (riferimentoOrdine == null) {
			ordiniClientiImpl.setRiferimentoOrdine(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setRiferimentoOrdine(riferimentoOrdine);
		}

		if (dataConferma == Long.MIN_VALUE) {
			ordiniClientiImpl.setDataConferma(null);
		}
		else {
			ordiniClientiImpl.setDataConferma(new Date(dataConferma));
		}

		ordiniClientiImpl.setConfermaStampata(confermaStampata);
		ordiniClientiImpl.setTotaleOrdine(totaleOrdine);

		if (codiceIVATrasp == null) {
			ordiniClientiImpl.setCodiceIVATrasp(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceIVATrasp(codiceIVATrasp);
		}

		if (codiceIVAImballo == null) {
			ordiniClientiImpl.setCodiceIVAImballo(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceIVAImballo(codiceIVAImballo);
		}

		if (codiceIVAVarie == null) {
			ordiniClientiImpl.setCodiceIVAVarie(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceIVAVarie(codiceIVAVarie);
		}

		if (codiceIVABanca == null) {
			ordiniClientiImpl.setCodiceIVABanca(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setCodiceIVABanca(codiceIVABanca);
		}

		if (libStr1 == null) {
			ordiniClientiImpl.setLibStr1(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setLibStr1(libStr1);
		}

		if (libStr2 == null) {
			ordiniClientiImpl.setLibStr2(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setLibStr2(libStr2);
		}

		if (libStr3 == null) {
			ordiniClientiImpl.setLibStr3(StringPool.BLANK);
		}
		else {
			ordiniClientiImpl.setLibStr3(libStr3);
		}

		ordiniClientiImpl.setLibDbl1(libDbl1);
		ordiniClientiImpl.setLibDbl2(libDbl2);
		ordiniClientiImpl.setLibDbl3(libDbl3);

		if (libDat1 == Long.MIN_VALUE) {
			ordiniClientiImpl.setLibDat1(null);
		}
		else {
			ordiniClientiImpl.setLibDat1(new Date(libDat1));
		}

		if (libDat2 == Long.MIN_VALUE) {
			ordiniClientiImpl.setLibDat2(null);
		}
		else {
			ordiniClientiImpl.setLibDat2(new Date(libDat2));
		}

		if (libDat3 == Long.MIN_VALUE) {
			ordiniClientiImpl.setLibDat3(null);
		}
		else {
			ordiniClientiImpl.setLibDat3(new Date(libDat3));
		}

		ordiniClientiImpl.setLibLng1(libLng1);
		ordiniClientiImpl.setLibLng2(libLng2);
		ordiniClientiImpl.setLibLng3(libLng3);

		ordiniClientiImpl.resetOriginalValues();

		return ordiniClientiImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		anno = objectInput.readInt();
		codiceAttivita = objectInput.readUTF();
		codiceCentro = objectInput.readUTF();
		codiceDeposito = objectInput.readUTF();
		tipoOrdine = objectInput.readInt();
		numeroOrdine = objectInput.readInt();
		tipoDocumento = objectInput.readUTF();
		codiceContAnalitica = objectInput.readUTF();
		idTipoDocumento = objectInput.readInt();
		statoOrdine = objectInput.readBoolean();
		descrEstremiDoc = objectInput.readUTF();
		tipoSoggetto = objectInput.readBoolean();
		codiceCliente = objectInput.readUTF();
		naturaTransazione = objectInput.readUTF();
		codiceEsenzione = objectInput.readUTF();
		codiceDivisa = objectInput.readUTF();
		valoreCambio = objectInput.readDouble();
		dataValoreCambio = objectInput.readLong();
		codicePianoPag = objectInput.readUTF();
		inizioCalcoloPag = objectInput.readLong();
		percentualeScontoMaggiorazione = objectInput.readDouble();
		percentualeScontoProntaCassa = objectInput.readDouble();
		percentualeProvvChiusura = objectInput.readDouble();
		dataDocumento = objectInput.readLong();
		dataRegistrazione = objectInput.readLong();
		causaleEstrattoConto = objectInput.readUTF();
		dataPrimaRata = objectInput.readLong();
		dataUltimaRata = objectInput.readLong();
		codiceBanca = objectInput.readUTF();
		codiceAgenzia = objectInput.readUTF();
		codiceAgente = objectInput.readUTF();
		codiceGruppoAgenti = objectInput.readUTF();
		codiceZona = objectInput.readUTF();
		codiceSpedizione = objectInput.readUTF();
		codicePorto = objectInput.readUTF();
		codiceDestinatario = objectInput.readUTF();
		codiceListino = objectInput.readUTF();
		codiceLingua = objectInput.readUTF();
		numeroDecPrezzo = objectInput.readInt();
		note = objectInput.readUTF();
		percentualeSpeseTrasp = objectInput.readDouble();
		speseTrasporto = objectInput.readDouble();
		speseImballaggio = objectInput.readDouble();
		speseVarie = objectInput.readDouble();
		speseBanca = objectInput.readDouble();
		curaTrasporto = objectInput.readUTF();
		causaleTrasporto = objectInput.readUTF();
		aspettoEstriore = objectInput.readUTF();
		vettore1 = objectInput.readUTF();
		vettore2 = objectInput.readUTF();
		vettore3 = objectInput.readUTF();
		numeroColli = objectInput.readInt();
		pesoLordo = objectInput.readDouble();
		pesoNetto = objectInput.readDouble();
		volume = objectInput.readDouble();
		numeroCopie = objectInput.readInt();
		numeroCopieStampate = objectInput.readInt();
		inviatoEmail = objectInput.readBoolean();
		nomePDF = objectInput.readUTF();
		riferimentoOrdine = objectInput.readUTF();
		dataConferma = objectInput.readLong();
		confermaStampata = objectInput.readBoolean();
		totaleOrdine = objectInput.readDouble();
		codiceIVATrasp = objectInput.readUTF();
		codiceIVAImballo = objectInput.readUTF();
		codiceIVAVarie = objectInput.readUTF();
		codiceIVABanca = objectInput.readUTF();
		libStr1 = objectInput.readUTF();
		libStr2 = objectInput.readUTF();
		libStr3 = objectInput.readUTF();
		libDbl1 = objectInput.readDouble();
		libDbl2 = objectInput.readDouble();
		libDbl3 = objectInput.readDouble();
		libDat1 = objectInput.readLong();
		libDat2 = objectInput.readLong();
		libDat3 = objectInput.readLong();
		libLng1 = objectInput.readLong();
		libLng2 = objectInput.readLong();
		libLng3 = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(anno);

		if (codiceAttivita == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceAttivita);
		}

		if (codiceCentro == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCentro);
		}

		if (codiceDeposito == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDeposito);
		}

		objectOutput.writeInt(tipoOrdine);
		objectOutput.writeInt(numeroOrdine);

		if (tipoDocumento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoDocumento);
		}

		if (codiceContAnalitica == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceContAnalitica);
		}

		objectOutput.writeInt(idTipoDocumento);
		objectOutput.writeBoolean(statoOrdine);

		if (descrEstremiDoc == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrEstremiDoc);
		}

		objectOutput.writeBoolean(tipoSoggetto);

		if (codiceCliente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCliente);
		}

		if (naturaTransazione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(naturaTransazione);
		}

		if (codiceEsenzione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceEsenzione);
		}

		if (codiceDivisa == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDivisa);
		}

		objectOutput.writeDouble(valoreCambio);
		objectOutput.writeLong(dataValoreCambio);

		if (codicePianoPag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codicePianoPag);
		}

		objectOutput.writeLong(inizioCalcoloPag);
		objectOutput.writeDouble(percentualeScontoMaggiorazione);
		objectOutput.writeDouble(percentualeScontoProntaCassa);
		objectOutput.writeDouble(percentualeProvvChiusura);
		objectOutput.writeLong(dataDocumento);
		objectOutput.writeLong(dataRegistrazione);

		if (causaleEstrattoConto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(causaleEstrattoConto);
		}

		objectOutput.writeLong(dataPrimaRata);
		objectOutput.writeLong(dataUltimaRata);

		if (codiceBanca == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceBanca);
		}

		if (codiceAgenzia == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceAgenzia);
		}

		if (codiceAgente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceAgente);
		}

		if (codiceGruppoAgenti == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceGruppoAgenti);
		}

		if (codiceZona == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceZona);
		}

		if (codiceSpedizione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceSpedizione);
		}

		if (codicePorto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codicePorto);
		}

		if (codiceDestinatario == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDestinatario);
		}

		if (codiceListino == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceListino);
		}

		if (codiceLingua == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceLingua);
		}

		objectOutput.writeInt(numeroDecPrezzo);

		if (note == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(note);
		}

		objectOutput.writeDouble(percentualeSpeseTrasp);
		objectOutput.writeDouble(speseTrasporto);
		objectOutput.writeDouble(speseImballaggio);
		objectOutput.writeDouble(speseVarie);
		objectOutput.writeDouble(speseBanca);

		if (curaTrasporto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(curaTrasporto);
		}

		if (causaleTrasporto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(causaleTrasporto);
		}

		if (aspettoEstriore == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(aspettoEstriore);
		}

		if (vettore1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(vettore1);
		}

		if (vettore2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(vettore2);
		}

		if (vettore3 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(vettore3);
		}

		objectOutput.writeInt(numeroColli);
		objectOutput.writeDouble(pesoLordo);
		objectOutput.writeDouble(pesoNetto);
		objectOutput.writeDouble(volume);
		objectOutput.writeInt(numeroCopie);
		objectOutput.writeInt(numeroCopieStampate);
		objectOutput.writeBoolean(inviatoEmail);

		if (nomePDF == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nomePDF);
		}

		if (riferimentoOrdine == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(riferimentoOrdine);
		}

		objectOutput.writeLong(dataConferma);
		objectOutput.writeBoolean(confermaStampata);
		objectOutput.writeDouble(totaleOrdine);

		if (codiceIVATrasp == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVATrasp);
		}

		if (codiceIVAImballo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVAImballo);
		}

		if (codiceIVAVarie == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVAVarie);
		}

		if (codiceIVABanca == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVABanca);
		}

		if (libStr1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr1);
		}

		if (libStr2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr2);
		}

		if (libStr3 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr3);
		}

		objectOutput.writeDouble(libDbl1);
		objectOutput.writeDouble(libDbl2);
		objectOutput.writeDouble(libDbl3);
		objectOutput.writeLong(libDat1);
		objectOutput.writeLong(libDat2);
		objectOutput.writeLong(libDat3);
		objectOutput.writeLong(libLng1);
		objectOutput.writeLong(libLng2);
		objectOutput.writeLong(libLng3);
	}

	public int anno;
	public String codiceAttivita;
	public String codiceCentro;
	public String codiceDeposito;
	public int tipoOrdine;
	public int numeroOrdine;
	public String tipoDocumento;
	public String codiceContAnalitica;
	public int idTipoDocumento;
	public boolean statoOrdine;
	public String descrEstremiDoc;
	public boolean tipoSoggetto;
	public String codiceCliente;
	public String naturaTransazione;
	public String codiceEsenzione;
	public String codiceDivisa;
	public double valoreCambio;
	public long dataValoreCambio;
	public String codicePianoPag;
	public long inizioCalcoloPag;
	public double percentualeScontoMaggiorazione;
	public double percentualeScontoProntaCassa;
	public double percentualeProvvChiusura;
	public long dataDocumento;
	public long dataRegistrazione;
	public String causaleEstrattoConto;
	public long dataPrimaRata;
	public long dataUltimaRata;
	public String codiceBanca;
	public String codiceAgenzia;
	public String codiceAgente;
	public String codiceGruppoAgenti;
	public String codiceZona;
	public String codiceSpedizione;
	public String codicePorto;
	public String codiceDestinatario;
	public String codiceListino;
	public String codiceLingua;
	public int numeroDecPrezzo;
	public String note;
	public double percentualeSpeseTrasp;
	public double speseTrasporto;
	public double speseImballaggio;
	public double speseVarie;
	public double speseBanca;
	public String curaTrasporto;
	public String causaleTrasporto;
	public String aspettoEstriore;
	public String vettore1;
	public String vettore2;
	public String vettore3;
	public int numeroColli;
	public double pesoLordo;
	public double pesoNetto;
	public double volume;
	public int numeroCopie;
	public int numeroCopieStampate;
	public boolean inviatoEmail;
	public String nomePDF;
	public String riferimentoOrdine;
	public long dataConferma;
	public boolean confermaStampata;
	public double totaleOrdine;
	public String codiceIVATrasp;
	public String codiceIVAImballo;
	public String codiceIVAVarie;
	public String codiceIVABanca;
	public String libStr1;
	public String libStr2;
	public String libStr3;
	public double libDbl1;
	public double libDbl2;
	public double libDbl3;
	public long libDat1;
	public long libDat2;
	public long libDat3;
	public long libLng1;
	public long libLng2;
	public long libLng3;
}