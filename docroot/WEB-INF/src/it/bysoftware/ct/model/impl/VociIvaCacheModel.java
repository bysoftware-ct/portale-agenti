/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.VociIva;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing VociIva in entity cache.
 *
 * @author Mario Torrisi
 * @see VociIva
 * @generated
 */
public class VociIvaCacheModel implements CacheModel<VociIva>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{codiceIva=");
		sb.append(codiceIva);
		sb.append(", descrizione=");
		sb.append(descrizione);
		sb.append(", aliquota=");
		sb.append(aliquota);
		sb.append(", aliquotaVandita=");
		sb.append(aliquotaVandita);
		sb.append(", aliquotaScorporo=");
		sb.append(aliquotaScorporo);
		sb.append(", aliquotaUlterioreDetr=");
		sb.append(aliquotaUlterioreDetr);
		sb.append(", tipoVoceIva=");
		sb.append(tipoVoceIva);
		sb.append(", gestioneOmaggio=");
		sb.append(gestioneOmaggio);
		sb.append(", codIvaSpeseOmaggio=");
		sb.append(codIvaSpeseOmaggio);
		sb.append(", descrizioneDocuemto=");
		sb.append(descrizioneDocuemto);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public VociIva toEntityModel() {
		VociIvaImpl vociIvaImpl = new VociIvaImpl();

		if (codiceIva == null) {
			vociIvaImpl.setCodiceIva(StringPool.BLANK);
		}
		else {
			vociIvaImpl.setCodiceIva(codiceIva);
		}

		if (descrizione == null) {
			vociIvaImpl.setDescrizione(StringPool.BLANK);
		}
		else {
			vociIvaImpl.setDescrizione(descrizione);
		}

		vociIvaImpl.setAliquota(aliquota);
		vociIvaImpl.setAliquotaVandita(aliquotaVandita);
		vociIvaImpl.setAliquotaScorporo(aliquotaScorporo);
		vociIvaImpl.setAliquotaUlterioreDetr(aliquotaUlterioreDetr);
		vociIvaImpl.setTipoVoceIva(tipoVoceIva);
		vociIvaImpl.setGestioneOmaggio(gestioneOmaggio);

		if (codIvaSpeseOmaggio == null) {
			vociIvaImpl.setCodIvaSpeseOmaggio(StringPool.BLANK);
		}
		else {
			vociIvaImpl.setCodIvaSpeseOmaggio(codIvaSpeseOmaggio);
		}

		if (descrizioneDocuemto == null) {
			vociIvaImpl.setDescrizioneDocuemto(StringPool.BLANK);
		}
		else {
			vociIvaImpl.setDescrizioneDocuemto(descrizioneDocuemto);
		}

		vociIvaImpl.resetOriginalValues();

		return vociIvaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		codiceIva = objectInput.readUTF();
		descrizione = objectInput.readUTF();
		aliquota = objectInput.readDouble();
		aliquotaVandita = objectInput.readDouble();
		aliquotaScorporo = objectInput.readDouble();
		aliquotaUlterioreDetr = objectInput.readDouble();
		tipoVoceIva = objectInput.readInt();
		gestioneOmaggio = objectInput.readInt();
		codIvaSpeseOmaggio = objectInput.readUTF();
		descrizioneDocuemto = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (codiceIva == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIva);
		}

		if (descrizione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizione);
		}

		objectOutput.writeDouble(aliquota);
		objectOutput.writeDouble(aliquotaVandita);
		objectOutput.writeDouble(aliquotaScorporo);
		objectOutput.writeDouble(aliquotaUlterioreDetr);
		objectOutput.writeInt(tipoVoceIva);
		objectOutput.writeInt(gestioneOmaggio);

		if (codIvaSpeseOmaggio == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codIvaSpeseOmaggio);
		}

		if (descrizioneDocuemto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizioneDocuemto);
		}
	}

	public String codiceIva;
	public String descrizione;
	public double aliquota;
	public double aliquotaVandita;
	public double aliquotaScorporo;
	public double aliquotaUlterioreDetr;
	public int tipoVoceIva;
	public int gestioneOmaggio;
	public String codIvaSpeseOmaggio;
	public String descrizioneDocuemto;
}