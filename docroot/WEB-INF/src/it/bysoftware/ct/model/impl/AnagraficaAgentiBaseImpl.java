/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.exception.SystemException;

import it.bysoftware.ct.model.AnagraficaAgenti;
import it.bysoftware.ct.service.AnagraficaAgentiLocalServiceUtil;

/**
 * The extended model base implementation for the AnagraficaAgenti service. Represents a row in the &quot;AGENTI&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link AnagraficaAgentiImpl}.
 * </p>
 *
 * @author Mario Torrisi
 * @see AnagraficaAgentiImpl
 * @see it.bysoftware.ct.model.AnagraficaAgenti
 * @generated
 */
public abstract class AnagraficaAgentiBaseImpl extends AnagraficaAgentiModelImpl
	implements AnagraficaAgenti {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a anagrafica agenti model instance should use the {@link AnagraficaAgenti} interface instead.
	 */
	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			AnagraficaAgentiLocalServiceUtil.addAnagraficaAgenti(this);
		}
		else {
			AnagraficaAgentiLocalServiceUtil.updateAnagraficaAgenti(this);
		}
	}
}