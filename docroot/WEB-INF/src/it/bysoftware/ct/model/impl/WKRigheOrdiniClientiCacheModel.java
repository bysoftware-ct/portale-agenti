/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.WKRigheOrdiniClienti;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing WKRigheOrdiniClienti in entity cache.
 *
 * @author Mario Torrisi
 * @see WKRigheOrdiniClienti
 * @generated
 */
public class WKRigheOrdiniClientiCacheModel implements CacheModel<WKRigheOrdiniClienti>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(91);

		sb.append("{anno=");
		sb.append(anno);
		sb.append(", tipoOrdine=");
		sb.append(tipoOrdine);
		sb.append(", numeroOrdine=");
		sb.append(numeroOrdine);
		sb.append(", numeroRigo=");
		sb.append(numeroRigo);
		sb.append(", statoRigo=");
		sb.append(statoRigo);
		sb.append(", tipoRigo=");
		sb.append(tipoRigo);
		sb.append(", codiceDepositoMov=");
		sb.append(codiceDepositoMov);
		sb.append(", codiceArticolo=");
		sb.append(codiceArticolo);
		sb.append(", codiceVariante=");
		sb.append(codiceVariante);
		sb.append(", descrizione=");
		sb.append(descrizione);
		sb.append(", codiceUnitMis=");
		sb.append(codiceUnitMis);
		sb.append(", decimaliQuant=");
		sb.append(decimaliQuant);
		sb.append(", quantita1=");
		sb.append(quantita1);
		sb.append(", quantita2=");
		sb.append(quantita2);
		sb.append(", quantita3=");
		sb.append(quantita3);
		sb.append(", quantita=");
		sb.append(quantita);
		sb.append(", codiceUnitMis2=");
		sb.append(codiceUnitMis2);
		sb.append(", quantitaUnitMis2=");
		sb.append(quantitaUnitMis2);
		sb.append(", decimaliPrezzo=");
		sb.append(decimaliPrezzo);
		sb.append(", prezzo=");
		sb.append(prezzo);
		sb.append(", importoLordo=");
		sb.append(importoLordo);
		sb.append(", sconto1=");
		sb.append(sconto1);
		sb.append(", sconto2=");
		sb.append(sconto2);
		sb.append(", sconto3=");
		sb.append(sconto3);
		sb.append(", importoNetto=");
		sb.append(importoNetto);
		sb.append(", importo=");
		sb.append(importo);
		sb.append(", codiceIVAFatturazione=");
		sb.append(codiceIVAFatturazione);
		sb.append(", codiceCliente=");
		sb.append(codiceCliente);
		sb.append(", riferimentoOrdineCliente=");
		sb.append(riferimentoOrdineCliente);
		sb.append(", dataOridine=");
		sb.append(dataOridine);
		sb.append(", statoEvasione=");
		sb.append(statoEvasione);
		sb.append(", dataPrevistaConsegna=");
		sb.append(dataPrevistaConsegna);
		sb.append(", dataRegistrazioneOrdine=");
		sb.append(dataRegistrazioneOrdine);
		sb.append(", libStr1=");
		sb.append(libStr1);
		sb.append(", libStr2=");
		sb.append(libStr2);
		sb.append(", libStr3=");
		sb.append(libStr3);
		sb.append(", libDbl1=");
		sb.append(libDbl1);
		sb.append(", libDbl2=");
		sb.append(libDbl2);
		sb.append(", libDbl3=");
		sb.append(libDbl3);
		sb.append(", libDat1=");
		sb.append(libDat1);
		sb.append(", libDat2=");
		sb.append(libDat2);
		sb.append(", libDat3=");
		sb.append(libDat3);
		sb.append(", libLng1=");
		sb.append(libLng1);
		sb.append(", libLng2=");
		sb.append(libLng2);
		sb.append(", libLng3=");
		sb.append(libLng3);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public WKRigheOrdiniClienti toEntityModel() {
		WKRigheOrdiniClientiImpl wkRigheOrdiniClientiImpl = new WKRigheOrdiniClientiImpl();

		wkRigheOrdiniClientiImpl.setAnno(anno);
		wkRigheOrdiniClientiImpl.setTipoOrdine(tipoOrdine);
		wkRigheOrdiniClientiImpl.setNumeroOrdine(numeroOrdine);
		wkRigheOrdiniClientiImpl.setNumeroRigo(numeroRigo);
		wkRigheOrdiniClientiImpl.setStatoRigo(statoRigo);
		wkRigheOrdiniClientiImpl.setTipoRigo(tipoRigo);

		if (codiceDepositoMov == null) {
			wkRigheOrdiniClientiImpl.setCodiceDepositoMov(StringPool.BLANK);
		}
		else {
			wkRigheOrdiniClientiImpl.setCodiceDepositoMov(codiceDepositoMov);
		}

		if (codiceArticolo == null) {
			wkRigheOrdiniClientiImpl.setCodiceArticolo(StringPool.BLANK);
		}
		else {
			wkRigheOrdiniClientiImpl.setCodiceArticolo(codiceArticolo);
		}

		if (codiceVariante == null) {
			wkRigheOrdiniClientiImpl.setCodiceVariante(StringPool.BLANK);
		}
		else {
			wkRigheOrdiniClientiImpl.setCodiceVariante(codiceVariante);
		}

		if (descrizione == null) {
			wkRigheOrdiniClientiImpl.setDescrizione(StringPool.BLANK);
		}
		else {
			wkRigheOrdiniClientiImpl.setDescrizione(descrizione);
		}

		if (codiceUnitMis == null) {
			wkRigheOrdiniClientiImpl.setCodiceUnitMis(StringPool.BLANK);
		}
		else {
			wkRigheOrdiniClientiImpl.setCodiceUnitMis(codiceUnitMis);
		}

		wkRigheOrdiniClientiImpl.setDecimaliQuant(decimaliQuant);
		wkRigheOrdiniClientiImpl.setQuantita1(quantita1);
		wkRigheOrdiniClientiImpl.setQuantita2(quantita2);
		wkRigheOrdiniClientiImpl.setQuantita3(quantita3);
		wkRigheOrdiniClientiImpl.setQuantita(quantita);

		if (codiceUnitMis2 == null) {
			wkRigheOrdiniClientiImpl.setCodiceUnitMis2(StringPool.BLANK);
		}
		else {
			wkRigheOrdiniClientiImpl.setCodiceUnitMis2(codiceUnitMis2);
		}

		wkRigheOrdiniClientiImpl.setQuantitaUnitMis2(quantitaUnitMis2);
		wkRigheOrdiniClientiImpl.setDecimaliPrezzo(decimaliPrezzo);
		wkRigheOrdiniClientiImpl.setPrezzo(prezzo);
		wkRigheOrdiniClientiImpl.setImportoLordo(importoLordo);
		wkRigheOrdiniClientiImpl.setSconto1(sconto1);
		wkRigheOrdiniClientiImpl.setSconto2(sconto2);
		wkRigheOrdiniClientiImpl.setSconto3(sconto3);
		wkRigheOrdiniClientiImpl.setImportoNetto(importoNetto);
		wkRigheOrdiniClientiImpl.setImporto(importo);

		if (codiceIVAFatturazione == null) {
			wkRigheOrdiniClientiImpl.setCodiceIVAFatturazione(StringPool.BLANK);
		}
		else {
			wkRigheOrdiniClientiImpl.setCodiceIVAFatturazione(codiceIVAFatturazione);
		}

		if (codiceCliente == null) {
			wkRigheOrdiniClientiImpl.setCodiceCliente(StringPool.BLANK);
		}
		else {
			wkRigheOrdiniClientiImpl.setCodiceCliente(codiceCliente);
		}

		if (riferimentoOrdineCliente == null) {
			wkRigheOrdiniClientiImpl.setRiferimentoOrdineCliente(StringPool.BLANK);
		}
		else {
			wkRigheOrdiniClientiImpl.setRiferimentoOrdineCliente(riferimentoOrdineCliente);
		}

		if (dataOridine == Long.MIN_VALUE) {
			wkRigheOrdiniClientiImpl.setDataOridine(null);
		}
		else {
			wkRigheOrdiniClientiImpl.setDataOridine(new Date(dataOridine));
		}

		wkRigheOrdiniClientiImpl.setStatoEvasione(statoEvasione);

		if (dataPrevistaConsegna == Long.MIN_VALUE) {
			wkRigheOrdiniClientiImpl.setDataPrevistaConsegna(null);
		}
		else {
			wkRigheOrdiniClientiImpl.setDataPrevistaConsegna(new Date(
					dataPrevistaConsegna));
		}

		if (dataRegistrazioneOrdine == Long.MIN_VALUE) {
			wkRigheOrdiniClientiImpl.setDataRegistrazioneOrdine(null);
		}
		else {
			wkRigheOrdiniClientiImpl.setDataRegistrazioneOrdine(new Date(
					dataRegistrazioneOrdine));
		}

		if (libStr1 == null) {
			wkRigheOrdiniClientiImpl.setLibStr1(StringPool.BLANK);
		}
		else {
			wkRigheOrdiniClientiImpl.setLibStr1(libStr1);
		}

		if (libStr2 == null) {
			wkRigheOrdiniClientiImpl.setLibStr2(StringPool.BLANK);
		}
		else {
			wkRigheOrdiniClientiImpl.setLibStr2(libStr2);
		}

		if (libStr3 == null) {
			wkRigheOrdiniClientiImpl.setLibStr3(StringPool.BLANK);
		}
		else {
			wkRigheOrdiniClientiImpl.setLibStr3(libStr3);
		}

		wkRigheOrdiniClientiImpl.setLibDbl1(libDbl1);
		wkRigheOrdiniClientiImpl.setLibDbl2(libDbl2);
		wkRigheOrdiniClientiImpl.setLibDbl3(libDbl3);

		if (libDat1 == Long.MIN_VALUE) {
			wkRigheOrdiniClientiImpl.setLibDat1(null);
		}
		else {
			wkRigheOrdiniClientiImpl.setLibDat1(new Date(libDat1));
		}

		if (libDat2 == Long.MIN_VALUE) {
			wkRigheOrdiniClientiImpl.setLibDat2(null);
		}
		else {
			wkRigheOrdiniClientiImpl.setLibDat2(new Date(libDat2));
		}

		if (libDat3 == Long.MIN_VALUE) {
			wkRigheOrdiniClientiImpl.setLibDat3(null);
		}
		else {
			wkRigheOrdiniClientiImpl.setLibDat3(new Date(libDat3));
		}

		wkRigheOrdiniClientiImpl.setLibLng1(libLng1);
		wkRigheOrdiniClientiImpl.setLibLng2(libLng2);
		wkRigheOrdiniClientiImpl.setLibLng3(libLng3);

		wkRigheOrdiniClientiImpl.resetOriginalValues();

		return wkRigheOrdiniClientiImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		anno = objectInput.readInt();
		tipoOrdine = objectInput.readInt();
		numeroOrdine = objectInput.readInt();
		numeroRigo = objectInput.readInt();
		statoRigo = objectInput.readBoolean();
		tipoRigo = objectInput.readInt();
		codiceDepositoMov = objectInput.readUTF();
		codiceArticolo = objectInput.readUTF();
		codiceVariante = objectInput.readUTF();
		descrizione = objectInput.readUTF();
		codiceUnitMis = objectInput.readUTF();
		decimaliQuant = objectInput.readInt();
		quantita1 = objectInput.readDouble();
		quantita2 = objectInput.readDouble();
		quantita3 = objectInput.readDouble();
		quantita = objectInput.readDouble();
		codiceUnitMis2 = objectInput.readUTF();
		quantitaUnitMis2 = objectInput.readDouble();
		decimaliPrezzo = objectInput.readInt();
		prezzo = objectInput.readDouble();
		importoLordo = objectInput.readDouble();
		sconto1 = objectInput.readDouble();
		sconto2 = objectInput.readDouble();
		sconto3 = objectInput.readDouble();
		importoNetto = objectInput.readDouble();
		importo = objectInput.readDouble();
		codiceIVAFatturazione = objectInput.readUTF();
		codiceCliente = objectInput.readUTF();
		riferimentoOrdineCliente = objectInput.readUTF();
		dataOridine = objectInput.readLong();
		statoEvasione = objectInput.readBoolean();
		dataPrevistaConsegna = objectInput.readLong();
		dataRegistrazioneOrdine = objectInput.readLong();
		libStr1 = objectInput.readUTF();
		libStr2 = objectInput.readUTF();
		libStr3 = objectInput.readUTF();
		libDbl1 = objectInput.readDouble();
		libDbl2 = objectInput.readDouble();
		libDbl3 = objectInput.readDouble();
		libDat1 = objectInput.readLong();
		libDat2 = objectInput.readLong();
		libDat3 = objectInput.readLong();
		libLng1 = objectInput.readLong();
		libLng2 = objectInput.readLong();
		libLng3 = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(anno);
		objectOutput.writeInt(tipoOrdine);
		objectOutput.writeInt(numeroOrdine);
		objectOutput.writeInt(numeroRigo);
		objectOutput.writeBoolean(statoRigo);
		objectOutput.writeInt(tipoRigo);

		if (codiceDepositoMov == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDepositoMov);
		}

		if (codiceArticolo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceArticolo);
		}

		if (codiceVariante == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceVariante);
		}

		if (descrizione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizione);
		}

		if (codiceUnitMis == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceUnitMis);
		}

		objectOutput.writeInt(decimaliQuant);
		objectOutput.writeDouble(quantita1);
		objectOutput.writeDouble(quantita2);
		objectOutput.writeDouble(quantita3);
		objectOutput.writeDouble(quantita);

		if (codiceUnitMis2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceUnitMis2);
		}

		objectOutput.writeDouble(quantitaUnitMis2);
		objectOutput.writeInt(decimaliPrezzo);
		objectOutput.writeDouble(prezzo);
		objectOutput.writeDouble(importoLordo);
		objectOutput.writeDouble(sconto1);
		objectOutput.writeDouble(sconto2);
		objectOutput.writeDouble(sconto3);
		objectOutput.writeDouble(importoNetto);
		objectOutput.writeDouble(importo);

		if (codiceIVAFatturazione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVAFatturazione);
		}

		if (codiceCliente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCliente);
		}

		if (riferimentoOrdineCliente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(riferimentoOrdineCliente);
		}

		objectOutput.writeLong(dataOridine);
		objectOutput.writeBoolean(statoEvasione);
		objectOutput.writeLong(dataPrevistaConsegna);
		objectOutput.writeLong(dataRegistrazioneOrdine);

		if (libStr1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr1);
		}

		if (libStr2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr2);
		}

		if (libStr3 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr3);
		}

		objectOutput.writeDouble(libDbl1);
		objectOutput.writeDouble(libDbl2);
		objectOutput.writeDouble(libDbl3);
		objectOutput.writeLong(libDat1);
		objectOutput.writeLong(libDat2);
		objectOutput.writeLong(libDat3);
		objectOutput.writeLong(libLng1);
		objectOutput.writeLong(libLng2);
		objectOutput.writeLong(libLng3);
	}

	public int anno;
	public int tipoOrdine;
	public int numeroOrdine;
	public int numeroRigo;
	public boolean statoRigo;
	public int tipoRigo;
	public String codiceDepositoMov;
	public String codiceArticolo;
	public String codiceVariante;
	public String descrizione;
	public String codiceUnitMis;
	public int decimaliQuant;
	public double quantita1;
	public double quantita2;
	public double quantita3;
	public double quantita;
	public String codiceUnitMis2;
	public double quantitaUnitMis2;
	public int decimaliPrezzo;
	public double prezzo;
	public double importoLordo;
	public double sconto1;
	public double sconto2;
	public double sconto3;
	public double importoNetto;
	public double importo;
	public String codiceIVAFatturazione;
	public String codiceCliente;
	public String riferimentoOrdineCliente;
	public long dataOridine;
	public boolean statoEvasione;
	public long dataPrevistaConsegna;
	public long dataRegistrazioneOrdine;
	public String libStr1;
	public String libStr2;
	public String libStr3;
	public double libDbl1;
	public double libDbl2;
	public double libDbl3;
	public long libDat1;
	public long libDat2;
	public long libDat3;
	public long libLng1;
	public long libLng2;
	public long libLng3;
}