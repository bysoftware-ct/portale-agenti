/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.DatiClientiFornitori;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing DatiClientiFornitori in entity cache.
 *
 * @author Mario Torrisi
 * @see DatiClientiFornitori
 * @generated
 */
public class DatiClientiFornitoriCacheModel implements CacheModel<DatiClientiFornitori>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(133);

		sb.append("{tipoSoggetto=");
		sb.append(tipoSoggetto);
		sb.append(", codiceSoggetto=");
		sb.append(codiceSoggetto);
		sb.append(", codiceBanca=");
		sb.append(codiceBanca);
		sb.append(", codiceAgenzia=");
		sb.append(codiceAgenzia);
		sb.append(", codiceIdBanca=");
		sb.append(codiceIdBanca);
		sb.append(", codicePaese=");
		sb.append(codicePaese);
		sb.append(", codiceCINInt=");
		sb.append(codiceCINInt);
		sb.append(", codiceCINNaz=");
		sb.append(codiceCINNaz);
		sb.append(", numeroContoCorrente=");
		sb.append(numeroContoCorrente);
		sb.append(", IBAN=");
		sb.append(IBAN);
		sb.append(", categoriaEconomica=");
		sb.append(categoriaEconomica);
		sb.append(", codiceEsenzioneIVA=");
		sb.append(codiceEsenzioneIVA);
		sb.append(", codiceDivisa=");
		sb.append(codiceDivisa);
		sb.append(", codicePagamento=");
		sb.append(codicePagamento);
		sb.append(", codiceZona=");
		sb.append(codiceZona);
		sb.append(", codiceAgente=");
		sb.append(codiceAgente);
		sb.append(", codiceGruppoAgente=");
		sb.append(codiceGruppoAgente);
		sb.append(", codiceCatProvv=");
		sb.append(codiceCatProvv);
		sb.append(", codiceSpedizione=");
		sb.append(codiceSpedizione);
		sb.append(", codicePorto=");
		sb.append(codicePorto);
		sb.append(", codiceVettore=");
		sb.append(codiceVettore);
		sb.append(", codiceDestDiversa=");
		sb.append(codiceDestDiversa);
		sb.append(", codiceListinoPrezzi=");
		sb.append(codiceListinoPrezzi);
		sb.append(", codiceScontoTotale=");
		sb.append(codiceScontoTotale);
		sb.append(", codiceScontoPreIVA=");
		sb.append(codiceScontoPreIVA);
		sb.append(", scontoCat1=");
		sb.append(scontoCat1);
		sb.append(", scontoCat2=");
		sb.append(scontoCat2);
		sb.append(", codiceLingua=");
		sb.append(codiceLingua);
		sb.append(", codiceLinguaEC=");
		sb.append(codiceLinguaEC);
		sb.append(", addebitoBolli=");
		sb.append(addebitoBolli);
		sb.append(", addebitoSpeseBanca=");
		sb.append(addebitoSpeseBanca);
		sb.append(", ragruppaBolle=");
		sb.append(ragruppaBolle);
		sb.append(", sospensioneIVA=");
		sb.append(sospensioneIVA);
		sb.append(", ragruppaOrdini=");
		sb.append(ragruppaOrdini);
		sb.append(", ragruppaXDestinazione=");
		sb.append(ragruppaXDestinazione);
		sb.append(", ragruppaXPorto=");
		sb.append(ragruppaXPorto);
		sb.append(", percSpeseTrasporto=");
		sb.append(percSpeseTrasporto);
		sb.append(", prezzoDaProporre=");
		sb.append(prezzoDaProporre);
		sb.append(", sogettoFatturazione=");
		sb.append(sogettoFatturazione);
		sb.append(", codiceRaggEffetti=");
		sb.append(codiceRaggEffetti);
		sb.append(", riportoRiferimenti=");
		sb.append(riportoRiferimenti);
		sb.append(", stampaPrezzo=");
		sb.append(stampaPrezzo);
		sb.append(", bloccato=");
		sb.append(bloccato);
		sb.append(", motivazione=");
		sb.append(motivazione);
		sb.append(", livelloBlocco=");
		sb.append(livelloBlocco);
		sb.append(", addebitoCONAI=");
		sb.append(addebitoCONAI);
		sb.append(", libStr1=");
		sb.append(libStr1);
		sb.append(", libStr2=");
		sb.append(libStr2);
		sb.append(", libStr3=");
		sb.append(libStr3);
		sb.append(", libStr4=");
		sb.append(libStr4);
		sb.append(", libStr5=");
		sb.append(libStr5);
		sb.append(", libDat1=");
		sb.append(libDat1);
		sb.append(", libDat2=");
		sb.append(libDat2);
		sb.append(", libDat3=");
		sb.append(libDat3);
		sb.append(", libDat4=");
		sb.append(libDat4);
		sb.append(", libDat5=");
		sb.append(libDat5);
		sb.append(", libLng1=");
		sb.append(libLng1);
		sb.append(", libLng2=");
		sb.append(libLng2);
		sb.append(", libLng3=");
		sb.append(libLng3);
		sb.append(", libLng4=");
		sb.append(libLng4);
		sb.append(", libLng5=");
		sb.append(libLng5);
		sb.append(", libDbl1=");
		sb.append(libDbl1);
		sb.append(", libDbl2=");
		sb.append(libDbl2);
		sb.append(", libDbl3=");
		sb.append(libDbl3);
		sb.append(", libDbl4=");
		sb.append(libDbl4);
		sb.append(", libDbl5=");
		sb.append(libDbl5);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public DatiClientiFornitori toEntityModel() {
		DatiClientiFornitoriImpl datiClientiFornitoriImpl = new DatiClientiFornitoriImpl();

		datiClientiFornitoriImpl.setTipoSoggetto(tipoSoggetto);

		if (codiceSoggetto == null) {
			datiClientiFornitoriImpl.setCodiceSoggetto(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceSoggetto(codiceSoggetto);
		}

		if (codiceBanca == null) {
			datiClientiFornitoriImpl.setCodiceBanca(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceBanca(codiceBanca);
		}

		if (codiceAgenzia == null) {
			datiClientiFornitoriImpl.setCodiceAgenzia(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceAgenzia(codiceAgenzia);
		}

		if (codiceIdBanca == null) {
			datiClientiFornitoriImpl.setCodiceIdBanca(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceIdBanca(codiceIdBanca);
		}

		if (codicePaese == null) {
			datiClientiFornitoriImpl.setCodicePaese(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodicePaese(codicePaese);
		}

		if (codiceCINInt == null) {
			datiClientiFornitoriImpl.setCodiceCINInt(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceCINInt(codiceCINInt);
		}

		if (codiceCINNaz == null) {
			datiClientiFornitoriImpl.setCodiceCINNaz(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceCINNaz(codiceCINNaz);
		}

		if (numeroContoCorrente == null) {
			datiClientiFornitoriImpl.setNumeroContoCorrente(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setNumeroContoCorrente(numeroContoCorrente);
		}

		if (IBAN == null) {
			datiClientiFornitoriImpl.setIBAN(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setIBAN(IBAN);
		}

		if (categoriaEconomica == null) {
			datiClientiFornitoriImpl.setCategoriaEconomica(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCategoriaEconomica(categoriaEconomica);
		}

		if (codiceEsenzioneIVA == null) {
			datiClientiFornitoriImpl.setCodiceEsenzioneIVA(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceEsenzioneIVA(codiceEsenzioneIVA);
		}

		if (codiceDivisa == null) {
			datiClientiFornitoriImpl.setCodiceDivisa(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceDivisa(codiceDivisa);
		}

		if (codicePagamento == null) {
			datiClientiFornitoriImpl.setCodicePagamento(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodicePagamento(codicePagamento);
		}

		if (codiceZona == null) {
			datiClientiFornitoriImpl.setCodiceZona(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceZona(codiceZona);
		}

		if (codiceAgente == null) {
			datiClientiFornitoriImpl.setCodiceAgente(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceAgente(codiceAgente);
		}

		if (codiceGruppoAgente == null) {
			datiClientiFornitoriImpl.setCodiceGruppoAgente(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceGruppoAgente(codiceGruppoAgente);
		}

		if (codiceCatProvv == null) {
			datiClientiFornitoriImpl.setCodiceCatProvv(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceCatProvv(codiceCatProvv);
		}

		if (codiceSpedizione == null) {
			datiClientiFornitoriImpl.setCodiceSpedizione(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceSpedizione(codiceSpedizione);
		}

		if (codicePorto == null) {
			datiClientiFornitoriImpl.setCodicePorto(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodicePorto(codicePorto);
		}

		if (codiceVettore == null) {
			datiClientiFornitoriImpl.setCodiceVettore(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceVettore(codiceVettore);
		}

		if (codiceDestDiversa == null) {
			datiClientiFornitoriImpl.setCodiceDestDiversa(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceDestDiversa(codiceDestDiversa);
		}

		if (codiceListinoPrezzi == null) {
			datiClientiFornitoriImpl.setCodiceListinoPrezzi(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceListinoPrezzi(codiceListinoPrezzi);
		}

		if (codiceScontoTotale == null) {
			datiClientiFornitoriImpl.setCodiceScontoTotale(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceScontoTotale(codiceScontoTotale);
		}

		if (codiceScontoPreIVA == null) {
			datiClientiFornitoriImpl.setCodiceScontoPreIVA(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceScontoPreIVA(codiceScontoPreIVA);
		}

		if (scontoCat1 == null) {
			datiClientiFornitoriImpl.setScontoCat1(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setScontoCat1(scontoCat1);
		}

		if (scontoCat2 == null) {
			datiClientiFornitoriImpl.setScontoCat2(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setScontoCat2(scontoCat2);
		}

		if (codiceLingua == null) {
			datiClientiFornitoriImpl.setCodiceLingua(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceLingua(codiceLingua);
		}

		if (codiceLinguaEC == null) {
			datiClientiFornitoriImpl.setCodiceLinguaEC(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceLinguaEC(codiceLinguaEC);
		}

		datiClientiFornitoriImpl.setAddebitoBolli(addebitoBolli);
		datiClientiFornitoriImpl.setAddebitoSpeseBanca(addebitoSpeseBanca);
		datiClientiFornitoriImpl.setRagruppaBolle(ragruppaBolle);
		datiClientiFornitoriImpl.setSospensioneIVA(sospensioneIVA);
		datiClientiFornitoriImpl.setRagruppaOrdini(ragruppaOrdini);
		datiClientiFornitoriImpl.setRagruppaXDestinazione(ragruppaXDestinazione);
		datiClientiFornitoriImpl.setRagruppaXPorto(ragruppaXPorto);
		datiClientiFornitoriImpl.setPercSpeseTrasporto(percSpeseTrasporto);
		datiClientiFornitoriImpl.setPrezzoDaProporre(prezzoDaProporre);

		if (sogettoFatturazione == null) {
			datiClientiFornitoriImpl.setSogettoFatturazione(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setSogettoFatturazione(sogettoFatturazione);
		}

		if (codiceRaggEffetti == null) {
			datiClientiFornitoriImpl.setCodiceRaggEffetti(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setCodiceRaggEffetti(codiceRaggEffetti);
		}

		datiClientiFornitoriImpl.setRiportoRiferimenti(riportoRiferimenti);
		datiClientiFornitoriImpl.setStampaPrezzo(stampaPrezzo);
		datiClientiFornitoriImpl.setBloccato(bloccato);

		if (motivazione == null) {
			datiClientiFornitoriImpl.setMotivazione(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setMotivazione(motivazione);
		}

		datiClientiFornitoriImpl.setLivelloBlocco(livelloBlocco);
		datiClientiFornitoriImpl.setAddebitoCONAI(addebitoCONAI);

		if (libStr1 == null) {
			datiClientiFornitoriImpl.setLibStr1(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setLibStr1(libStr1);
		}

		if (libStr2 == null) {
			datiClientiFornitoriImpl.setLibStr2(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setLibStr2(libStr2);
		}

		if (libStr3 == null) {
			datiClientiFornitoriImpl.setLibStr3(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setLibStr3(libStr3);
		}

		if (libStr4 == null) {
			datiClientiFornitoriImpl.setLibStr4(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setLibStr4(libStr4);
		}

		if (libStr5 == null) {
			datiClientiFornitoriImpl.setLibStr5(StringPool.BLANK);
		}
		else {
			datiClientiFornitoriImpl.setLibStr5(libStr5);
		}

		if (libDat1 == Long.MIN_VALUE) {
			datiClientiFornitoriImpl.setLibDat1(null);
		}
		else {
			datiClientiFornitoriImpl.setLibDat1(new Date(libDat1));
		}

		if (libDat2 == Long.MIN_VALUE) {
			datiClientiFornitoriImpl.setLibDat2(null);
		}
		else {
			datiClientiFornitoriImpl.setLibDat2(new Date(libDat2));
		}

		if (libDat3 == Long.MIN_VALUE) {
			datiClientiFornitoriImpl.setLibDat3(null);
		}
		else {
			datiClientiFornitoriImpl.setLibDat3(new Date(libDat3));
		}

		if (libDat4 == Long.MIN_VALUE) {
			datiClientiFornitoriImpl.setLibDat4(null);
		}
		else {
			datiClientiFornitoriImpl.setLibDat4(new Date(libDat4));
		}

		if (libDat5 == Long.MIN_VALUE) {
			datiClientiFornitoriImpl.setLibDat5(null);
		}
		else {
			datiClientiFornitoriImpl.setLibDat5(new Date(libDat5));
		}

		datiClientiFornitoriImpl.setLibLng1(libLng1);
		datiClientiFornitoriImpl.setLibLng2(libLng2);
		datiClientiFornitoriImpl.setLibLng3(libLng3);
		datiClientiFornitoriImpl.setLibLng4(libLng4);
		datiClientiFornitoriImpl.setLibLng5(libLng5);
		datiClientiFornitoriImpl.setLibDbl1(libDbl1);
		datiClientiFornitoriImpl.setLibDbl2(libDbl2);
		datiClientiFornitoriImpl.setLibDbl3(libDbl3);
		datiClientiFornitoriImpl.setLibDbl4(libDbl4);
		datiClientiFornitoriImpl.setLibDbl5(libDbl5);

		datiClientiFornitoriImpl.resetOriginalValues();

		return datiClientiFornitoriImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		tipoSoggetto = objectInput.readBoolean();
		codiceSoggetto = objectInput.readUTF();
		codiceBanca = objectInput.readUTF();
		codiceAgenzia = objectInput.readUTF();
		codiceIdBanca = objectInput.readUTF();
		codicePaese = objectInput.readUTF();
		codiceCINInt = objectInput.readUTF();
		codiceCINNaz = objectInput.readUTF();
		numeroContoCorrente = objectInput.readUTF();
		IBAN = objectInput.readUTF();
		categoriaEconomica = objectInput.readUTF();
		codiceEsenzioneIVA = objectInput.readUTF();
		codiceDivisa = objectInput.readUTF();
		codicePagamento = objectInput.readUTF();
		codiceZona = objectInput.readUTF();
		codiceAgente = objectInput.readUTF();
		codiceGruppoAgente = objectInput.readUTF();
		codiceCatProvv = objectInput.readUTF();
		codiceSpedizione = objectInput.readUTF();
		codicePorto = objectInput.readUTF();
		codiceVettore = objectInput.readUTF();
		codiceDestDiversa = objectInput.readUTF();
		codiceListinoPrezzi = objectInput.readUTF();
		codiceScontoTotale = objectInput.readUTF();
		codiceScontoPreIVA = objectInput.readUTF();
		scontoCat1 = objectInput.readUTF();
		scontoCat2 = objectInput.readUTF();
		codiceLingua = objectInput.readUTF();
		codiceLinguaEC = objectInput.readUTF();
		addebitoBolli = objectInput.readBoolean();
		addebitoSpeseBanca = objectInput.readBoolean();
		ragruppaBolle = objectInput.readBoolean();
		sospensioneIVA = objectInput.readBoolean();
		ragruppaOrdini = objectInput.readInt();
		ragruppaXDestinazione = objectInput.readBoolean();
		ragruppaXPorto = objectInput.readBoolean();
		percSpeseTrasporto = objectInput.readDouble();
		prezzoDaProporre = objectInput.readInt();
		sogettoFatturazione = objectInput.readUTF();
		codiceRaggEffetti = objectInput.readUTF();
		riportoRiferimenti = objectInput.readInt();
		stampaPrezzo = objectInput.readBoolean();
		bloccato = objectInput.readInt();
		motivazione = objectInput.readUTF();
		livelloBlocco = objectInput.readInt();
		addebitoCONAI = objectInput.readBoolean();
		libStr1 = objectInput.readUTF();
		libStr2 = objectInput.readUTF();
		libStr3 = objectInput.readUTF();
		libStr4 = objectInput.readUTF();
		libStr5 = objectInput.readUTF();
		libDat1 = objectInput.readLong();
		libDat2 = objectInput.readLong();
		libDat3 = objectInput.readLong();
		libDat4 = objectInput.readLong();
		libDat5 = objectInput.readLong();
		libLng1 = objectInput.readLong();
		libLng2 = objectInput.readLong();
		libLng3 = objectInput.readLong();
		libLng4 = objectInput.readLong();
		libLng5 = objectInput.readLong();
		libDbl1 = objectInput.readDouble();
		libDbl2 = objectInput.readDouble();
		libDbl3 = objectInput.readDouble();
		libDbl4 = objectInput.readDouble();
		libDbl5 = objectInput.readDouble();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeBoolean(tipoSoggetto);

		if (codiceSoggetto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceSoggetto);
		}

		if (codiceBanca == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceBanca);
		}

		if (codiceAgenzia == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceAgenzia);
		}

		if (codiceIdBanca == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIdBanca);
		}

		if (codicePaese == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codicePaese);
		}

		if (codiceCINInt == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCINInt);
		}

		if (codiceCINNaz == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCINNaz);
		}

		if (numeroContoCorrente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(numeroContoCorrente);
		}

		if (IBAN == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(IBAN);
		}

		if (categoriaEconomica == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(categoriaEconomica);
		}

		if (codiceEsenzioneIVA == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceEsenzioneIVA);
		}

		if (codiceDivisa == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDivisa);
		}

		if (codicePagamento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codicePagamento);
		}

		if (codiceZona == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceZona);
		}

		if (codiceAgente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceAgente);
		}

		if (codiceGruppoAgente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceGruppoAgente);
		}

		if (codiceCatProvv == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCatProvv);
		}

		if (codiceSpedizione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceSpedizione);
		}

		if (codicePorto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codicePorto);
		}

		if (codiceVettore == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceVettore);
		}

		if (codiceDestDiversa == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDestDiversa);
		}

		if (codiceListinoPrezzi == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceListinoPrezzi);
		}

		if (codiceScontoTotale == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceScontoTotale);
		}

		if (codiceScontoPreIVA == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceScontoPreIVA);
		}

		if (scontoCat1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(scontoCat1);
		}

		if (scontoCat2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(scontoCat2);
		}

		if (codiceLingua == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceLingua);
		}

		if (codiceLinguaEC == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceLinguaEC);
		}

		objectOutput.writeBoolean(addebitoBolli);
		objectOutput.writeBoolean(addebitoSpeseBanca);
		objectOutput.writeBoolean(ragruppaBolle);
		objectOutput.writeBoolean(sospensioneIVA);
		objectOutput.writeInt(ragruppaOrdini);
		objectOutput.writeBoolean(ragruppaXDestinazione);
		objectOutput.writeBoolean(ragruppaXPorto);
		objectOutput.writeDouble(percSpeseTrasporto);
		objectOutput.writeInt(prezzoDaProporre);

		if (sogettoFatturazione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(sogettoFatturazione);
		}

		if (codiceRaggEffetti == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceRaggEffetti);
		}

		objectOutput.writeInt(riportoRiferimenti);
		objectOutput.writeBoolean(stampaPrezzo);
		objectOutput.writeInt(bloccato);

		if (motivazione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(motivazione);
		}

		objectOutput.writeInt(livelloBlocco);
		objectOutput.writeBoolean(addebitoCONAI);

		if (libStr1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr1);
		}

		if (libStr2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr2);
		}

		if (libStr3 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr3);
		}

		if (libStr4 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr4);
		}

		if (libStr5 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr5);
		}

		objectOutput.writeLong(libDat1);
		objectOutput.writeLong(libDat2);
		objectOutput.writeLong(libDat3);
		objectOutput.writeLong(libDat4);
		objectOutput.writeLong(libDat5);
		objectOutput.writeLong(libLng1);
		objectOutput.writeLong(libLng2);
		objectOutput.writeLong(libLng3);
		objectOutput.writeLong(libLng4);
		objectOutput.writeLong(libLng5);
		objectOutput.writeDouble(libDbl1);
		objectOutput.writeDouble(libDbl2);
		objectOutput.writeDouble(libDbl3);
		objectOutput.writeDouble(libDbl4);
		objectOutput.writeDouble(libDbl5);
	}

	public boolean tipoSoggetto;
	public String codiceSoggetto;
	public String codiceBanca;
	public String codiceAgenzia;
	public String codiceIdBanca;
	public String codicePaese;
	public String codiceCINInt;
	public String codiceCINNaz;
	public String numeroContoCorrente;
	public String IBAN;
	public String categoriaEconomica;
	public String codiceEsenzioneIVA;
	public String codiceDivisa;
	public String codicePagamento;
	public String codiceZona;
	public String codiceAgente;
	public String codiceGruppoAgente;
	public String codiceCatProvv;
	public String codiceSpedizione;
	public String codicePorto;
	public String codiceVettore;
	public String codiceDestDiversa;
	public String codiceListinoPrezzi;
	public String codiceScontoTotale;
	public String codiceScontoPreIVA;
	public String scontoCat1;
	public String scontoCat2;
	public String codiceLingua;
	public String codiceLinguaEC;
	public boolean addebitoBolli;
	public boolean addebitoSpeseBanca;
	public boolean ragruppaBolle;
	public boolean sospensioneIVA;
	public int ragruppaOrdini;
	public boolean ragruppaXDestinazione;
	public boolean ragruppaXPorto;
	public double percSpeseTrasporto;
	public int prezzoDaProporre;
	public String sogettoFatturazione;
	public String codiceRaggEffetti;
	public int riportoRiferimenti;
	public boolean stampaPrezzo;
	public int bloccato;
	public String motivazione;
	public int livelloBlocco;
	public boolean addebitoCONAI;
	public String libStr1;
	public String libStr2;
	public String libStr3;
	public String libStr4;
	public String libStr5;
	public long libDat1;
	public long libDat2;
	public long libDat3;
	public long libDat4;
	public long libDat5;
	public long libLng1;
	public long libLng2;
	public long libLng3;
	public long libLng4;
	public long libLng5;
	public double libDbl1;
	public double libDbl2;
	public double libDbl3;
	public double libDbl4;
	public double libDbl5;
}