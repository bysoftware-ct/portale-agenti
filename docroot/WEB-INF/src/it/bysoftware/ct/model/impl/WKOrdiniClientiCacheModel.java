/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.WKOrdiniClienti;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing WKOrdiniClienti in entity cache.
 *
 * @author Mario Torrisi
 * @see WKOrdiniClienti
 * @generated
 */
public class WKOrdiniClientiCacheModel implements CacheModel<WKOrdiniClienti>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(89);

		sb.append("{anno=");
		sb.append(anno);
		sb.append(", tipoOrdine=");
		sb.append(tipoOrdine);
		sb.append(", numeroOrdine=");
		sb.append(numeroOrdine);
		sb.append(", tipoDocumento=");
		sb.append(tipoDocumento);
		sb.append(", idTipoDocumento=");
		sb.append(idTipoDocumento);
		sb.append(", statoOrdine=");
		sb.append(statoOrdine);
		sb.append(", tipoSoggetto=");
		sb.append(tipoSoggetto);
		sb.append(", codiceCliente=");
		sb.append(codiceCliente);
		sb.append(", codiceEsenzione=");
		sb.append(codiceEsenzione);
		sb.append(", codiceDivisa=");
		sb.append(codiceDivisa);
		sb.append(", valoreCambio=");
		sb.append(valoreCambio);
		sb.append(", dataValoreCambio=");
		sb.append(dataValoreCambio);
		sb.append(", codicePianoPag=");
		sb.append(codicePianoPag);
		sb.append(", inizioCalcoloPag=");
		sb.append(inizioCalcoloPag);
		sb.append(", percentualeScontoMaggiorazione=");
		sb.append(percentualeScontoMaggiorazione);
		sb.append(", percentualeScontoProntaCassa=");
		sb.append(percentualeScontoProntaCassa);
		sb.append(", dataDocumento=");
		sb.append(dataDocumento);
		sb.append(", dataRegistrazione=");
		sb.append(dataRegistrazione);
		sb.append(", causaleEstrattoConto=");
		sb.append(causaleEstrattoConto);
		sb.append(", codiceAgente=");
		sb.append(codiceAgente);
		sb.append(", codiceGruppoAgenti=");
		sb.append(codiceGruppoAgenti);
		sb.append(", codiceZona=");
		sb.append(codiceZona);
		sb.append(", codiceDestinatario=");
		sb.append(codiceDestinatario);
		sb.append(", codiceListino=");
		sb.append(codiceListino);
		sb.append(", numeroDecPrezzo=");
		sb.append(numeroDecPrezzo);
		sb.append(", note=");
		sb.append(note);
		sb.append(", inviatoEmail=");
		sb.append(inviatoEmail);
		sb.append(", nomePDF=");
		sb.append(nomePDF);
		sb.append(", riferimentoOrdine=");
		sb.append(riferimentoOrdine);
		sb.append(", dataConferma=");
		sb.append(dataConferma);
		sb.append(", confermaStampata=");
		sb.append(confermaStampata);
		sb.append(", totaleOrdine=");
		sb.append(totaleOrdine);
		sb.append(", libStr1=");
		sb.append(libStr1);
		sb.append(", libStr2=");
		sb.append(libStr2);
		sb.append(", libStr3=");
		sb.append(libStr3);
		sb.append(", libDbl1=");
		sb.append(libDbl1);
		sb.append(", libDbl2=");
		sb.append(libDbl2);
		sb.append(", libDbl3=");
		sb.append(libDbl3);
		sb.append(", libDat1=");
		sb.append(libDat1);
		sb.append(", libDat2=");
		sb.append(libDat2);
		sb.append(", libDat3=");
		sb.append(libDat3);
		sb.append(", libLng1=");
		sb.append(libLng1);
		sb.append(", libLng2=");
		sb.append(libLng2);
		sb.append(", libLng3=");
		sb.append(libLng3);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public WKOrdiniClienti toEntityModel() {
		WKOrdiniClientiImpl wkOrdiniClientiImpl = new WKOrdiniClientiImpl();

		wkOrdiniClientiImpl.setAnno(anno);
		wkOrdiniClientiImpl.setTipoOrdine(tipoOrdine);
		wkOrdiniClientiImpl.setNumeroOrdine(numeroOrdine);

		if (tipoDocumento == null) {
			wkOrdiniClientiImpl.setTipoDocumento(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setTipoDocumento(tipoDocumento);
		}

		wkOrdiniClientiImpl.setIdTipoDocumento(idTipoDocumento);
		wkOrdiniClientiImpl.setStatoOrdine(statoOrdine);
		wkOrdiniClientiImpl.setTipoSoggetto(tipoSoggetto);

		if (codiceCliente == null) {
			wkOrdiniClientiImpl.setCodiceCliente(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setCodiceCliente(codiceCliente);
		}

		if (codiceEsenzione == null) {
			wkOrdiniClientiImpl.setCodiceEsenzione(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setCodiceEsenzione(codiceEsenzione);
		}

		if (codiceDivisa == null) {
			wkOrdiniClientiImpl.setCodiceDivisa(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setCodiceDivisa(codiceDivisa);
		}

		wkOrdiniClientiImpl.setValoreCambio(valoreCambio);

		if (dataValoreCambio == Long.MIN_VALUE) {
			wkOrdiniClientiImpl.setDataValoreCambio(null);
		}
		else {
			wkOrdiniClientiImpl.setDataValoreCambio(new Date(dataValoreCambio));
		}

		if (codicePianoPag == null) {
			wkOrdiniClientiImpl.setCodicePianoPag(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setCodicePianoPag(codicePianoPag);
		}

		if (inizioCalcoloPag == Long.MIN_VALUE) {
			wkOrdiniClientiImpl.setInizioCalcoloPag(null);
		}
		else {
			wkOrdiniClientiImpl.setInizioCalcoloPag(new Date(inizioCalcoloPag));
		}

		wkOrdiniClientiImpl.setPercentualeScontoMaggiorazione(percentualeScontoMaggiorazione);
		wkOrdiniClientiImpl.setPercentualeScontoProntaCassa(percentualeScontoProntaCassa);

		if (dataDocumento == Long.MIN_VALUE) {
			wkOrdiniClientiImpl.setDataDocumento(null);
		}
		else {
			wkOrdiniClientiImpl.setDataDocumento(new Date(dataDocumento));
		}

		if (dataRegistrazione == Long.MIN_VALUE) {
			wkOrdiniClientiImpl.setDataRegistrazione(null);
		}
		else {
			wkOrdiniClientiImpl.setDataRegistrazione(new Date(dataRegistrazione));
		}

		if (causaleEstrattoConto == null) {
			wkOrdiniClientiImpl.setCausaleEstrattoConto(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setCausaleEstrattoConto(causaleEstrattoConto);
		}

		if (codiceAgente == null) {
			wkOrdiniClientiImpl.setCodiceAgente(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setCodiceAgente(codiceAgente);
		}

		if (codiceGruppoAgenti == null) {
			wkOrdiniClientiImpl.setCodiceGruppoAgenti(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setCodiceGruppoAgenti(codiceGruppoAgenti);
		}

		if (codiceZona == null) {
			wkOrdiniClientiImpl.setCodiceZona(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setCodiceZona(codiceZona);
		}

		if (codiceDestinatario == null) {
			wkOrdiniClientiImpl.setCodiceDestinatario(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setCodiceDestinatario(codiceDestinatario);
		}

		if (codiceListino == null) {
			wkOrdiniClientiImpl.setCodiceListino(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setCodiceListino(codiceListino);
		}

		wkOrdiniClientiImpl.setNumeroDecPrezzo(numeroDecPrezzo);

		if (note == null) {
			wkOrdiniClientiImpl.setNote(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setNote(note);
		}

		wkOrdiniClientiImpl.setInviatoEmail(inviatoEmail);

		if (nomePDF == null) {
			wkOrdiniClientiImpl.setNomePDF(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setNomePDF(nomePDF);
		}

		if (riferimentoOrdine == null) {
			wkOrdiniClientiImpl.setRiferimentoOrdine(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setRiferimentoOrdine(riferimentoOrdine);
		}

		if (dataConferma == Long.MIN_VALUE) {
			wkOrdiniClientiImpl.setDataConferma(null);
		}
		else {
			wkOrdiniClientiImpl.setDataConferma(new Date(dataConferma));
		}

		wkOrdiniClientiImpl.setConfermaStampata(confermaStampata);
		wkOrdiniClientiImpl.setTotaleOrdine(totaleOrdine);

		if (libStr1 == null) {
			wkOrdiniClientiImpl.setLibStr1(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setLibStr1(libStr1);
		}

		if (libStr2 == null) {
			wkOrdiniClientiImpl.setLibStr2(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setLibStr2(libStr2);
		}

		if (libStr3 == null) {
			wkOrdiniClientiImpl.setLibStr3(StringPool.BLANK);
		}
		else {
			wkOrdiniClientiImpl.setLibStr3(libStr3);
		}

		wkOrdiniClientiImpl.setLibDbl1(libDbl1);
		wkOrdiniClientiImpl.setLibDbl2(libDbl2);
		wkOrdiniClientiImpl.setLibDbl3(libDbl3);

		if (libDat1 == Long.MIN_VALUE) {
			wkOrdiniClientiImpl.setLibDat1(null);
		}
		else {
			wkOrdiniClientiImpl.setLibDat1(new Date(libDat1));
		}

		if (libDat2 == Long.MIN_VALUE) {
			wkOrdiniClientiImpl.setLibDat2(null);
		}
		else {
			wkOrdiniClientiImpl.setLibDat2(new Date(libDat2));
		}

		if (libDat3 == Long.MIN_VALUE) {
			wkOrdiniClientiImpl.setLibDat3(null);
		}
		else {
			wkOrdiniClientiImpl.setLibDat3(new Date(libDat3));
		}

		wkOrdiniClientiImpl.setLibLng1(libLng1);
		wkOrdiniClientiImpl.setLibLng2(libLng2);
		wkOrdiniClientiImpl.setLibLng3(libLng3);

		wkOrdiniClientiImpl.resetOriginalValues();

		return wkOrdiniClientiImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		anno = objectInput.readInt();
		tipoOrdine = objectInput.readInt();
		numeroOrdine = objectInput.readInt();
		tipoDocumento = objectInput.readUTF();
		idTipoDocumento = objectInput.readInt();
		statoOrdine = objectInput.readBoolean();
		tipoSoggetto = objectInput.readBoolean();
		codiceCliente = objectInput.readUTF();
		codiceEsenzione = objectInput.readUTF();
		codiceDivisa = objectInput.readUTF();
		valoreCambio = objectInput.readDouble();
		dataValoreCambio = objectInput.readLong();
		codicePianoPag = objectInput.readUTF();
		inizioCalcoloPag = objectInput.readLong();
		percentualeScontoMaggiorazione = objectInput.readDouble();
		percentualeScontoProntaCassa = objectInput.readDouble();
		dataDocumento = objectInput.readLong();
		dataRegistrazione = objectInput.readLong();
		causaleEstrattoConto = objectInput.readUTF();
		codiceAgente = objectInput.readUTF();
		codiceGruppoAgenti = objectInput.readUTF();
		codiceZona = objectInput.readUTF();
		codiceDestinatario = objectInput.readUTF();
		codiceListino = objectInput.readUTF();
		numeroDecPrezzo = objectInput.readInt();
		note = objectInput.readUTF();
		inviatoEmail = objectInput.readBoolean();
		nomePDF = objectInput.readUTF();
		riferimentoOrdine = objectInput.readUTF();
		dataConferma = objectInput.readLong();
		confermaStampata = objectInput.readBoolean();
		totaleOrdine = objectInput.readDouble();
		libStr1 = objectInput.readUTF();
		libStr2 = objectInput.readUTF();
		libStr3 = objectInput.readUTF();
		libDbl1 = objectInput.readDouble();
		libDbl2 = objectInput.readDouble();
		libDbl3 = objectInput.readDouble();
		libDat1 = objectInput.readLong();
		libDat2 = objectInput.readLong();
		libDat3 = objectInput.readLong();
		libLng1 = objectInput.readLong();
		libLng2 = objectInput.readLong();
		libLng3 = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(anno);
		objectOutput.writeInt(tipoOrdine);
		objectOutput.writeInt(numeroOrdine);

		if (tipoDocumento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoDocumento);
		}

		objectOutput.writeInt(idTipoDocumento);
		objectOutput.writeBoolean(statoOrdine);
		objectOutput.writeBoolean(tipoSoggetto);

		if (codiceCliente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCliente);
		}

		if (codiceEsenzione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceEsenzione);
		}

		if (codiceDivisa == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDivisa);
		}

		objectOutput.writeDouble(valoreCambio);
		objectOutput.writeLong(dataValoreCambio);

		if (codicePianoPag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codicePianoPag);
		}

		objectOutput.writeLong(inizioCalcoloPag);
		objectOutput.writeDouble(percentualeScontoMaggiorazione);
		objectOutput.writeDouble(percentualeScontoProntaCassa);
		objectOutput.writeLong(dataDocumento);
		objectOutput.writeLong(dataRegistrazione);

		if (causaleEstrattoConto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(causaleEstrattoConto);
		}

		if (codiceAgente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceAgente);
		}

		if (codiceGruppoAgenti == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceGruppoAgenti);
		}

		if (codiceZona == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceZona);
		}

		if (codiceDestinatario == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDestinatario);
		}

		if (codiceListino == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceListino);
		}

		objectOutput.writeInt(numeroDecPrezzo);

		if (note == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(note);
		}

		objectOutput.writeBoolean(inviatoEmail);

		if (nomePDF == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nomePDF);
		}

		if (riferimentoOrdine == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(riferimentoOrdine);
		}

		objectOutput.writeLong(dataConferma);
		objectOutput.writeBoolean(confermaStampata);
		objectOutput.writeDouble(totaleOrdine);

		if (libStr1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr1);
		}

		if (libStr2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr2);
		}

		if (libStr3 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr3);
		}

		objectOutput.writeDouble(libDbl1);
		objectOutput.writeDouble(libDbl2);
		objectOutput.writeDouble(libDbl3);
		objectOutput.writeLong(libDat1);
		objectOutput.writeLong(libDat2);
		objectOutput.writeLong(libDat3);
		objectOutput.writeLong(libLng1);
		objectOutput.writeLong(libLng2);
		objectOutput.writeLong(libLng3);
	}

	public int anno;
	public int tipoOrdine;
	public int numeroOrdine;
	public String tipoDocumento;
	public int idTipoDocumento;
	public boolean statoOrdine;
	public boolean tipoSoggetto;
	public String codiceCliente;
	public String codiceEsenzione;
	public String codiceDivisa;
	public double valoreCambio;
	public long dataValoreCambio;
	public String codicePianoPag;
	public long inizioCalcoloPag;
	public double percentualeScontoMaggiorazione;
	public double percentualeScontoProntaCassa;
	public long dataDocumento;
	public long dataRegistrazione;
	public String causaleEstrattoConto;
	public String codiceAgente;
	public String codiceGruppoAgenti;
	public String codiceZona;
	public String codiceDestinatario;
	public String codiceListino;
	public int numeroDecPrezzo;
	public String note;
	public boolean inviatoEmail;
	public String nomePDF;
	public String riferimentoOrdine;
	public long dataConferma;
	public boolean confermaStampata;
	public double totaleOrdine;
	public String libStr1;
	public String libStr2;
	public String libStr3;
	public double libDbl1;
	public double libDbl2;
	public double libDbl3;
	public long libDat1;
	public long libDat2;
	public long libDat3;
	public long libLng1;
	public long libLng2;
	public long libLng3;
}