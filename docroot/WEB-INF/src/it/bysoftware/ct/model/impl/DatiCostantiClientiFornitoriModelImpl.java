/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.model.DatiCostantiClientiFornitori;
import it.bysoftware.ct.model.DatiCostantiClientiFornitoriModel;
import it.bysoftware.ct.model.DatiCostantiClientiFornitoriSoap;
import it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK;

import java.io.Serializable;

import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The base model implementation for the DatiCostantiClientiFornitori service. Represents a row in the &quot;COSCLF&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link it.bysoftware.ct.model.DatiCostantiClientiFornitoriModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link DatiCostantiClientiFornitoriImpl}.
 * </p>
 *
 * @author Mario Torrisi
 * @see DatiCostantiClientiFornitoriImpl
 * @see it.bysoftware.ct.model.DatiCostantiClientiFornitori
 * @see it.bysoftware.ct.model.DatiCostantiClientiFornitoriModel
 * @generated
 */
@JSON(strict = true)
public class DatiCostantiClientiFornitoriModelImpl extends BaseModelImpl<DatiCostantiClientiFornitori>
	implements DatiCostantiClientiFornitoriModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a dati costanti clienti fornitori model instance should use the {@link it.bysoftware.ct.model.DatiCostantiClientiFornitori} interface instead.
	 */
	public static final String TABLE_NAME = "COSCLF";
	public static final Object[][] TABLE_COLUMNS = {
			{ "RcsTipo", Types.BOOLEAN },
			{ "RcsCodclf", Types.VARCHAR },
			{ "RcsCodcon", Types.VARCHAR },
			{ "RcsMaxsco", Types.DOUBLE },
			{ "RcsEstcon", Types.BOOLEAN },
			{ "RcsCodpag", Types.VARCHAR },
			{ "RcsCodbaa", Types.VARCHAR },
			{ "RcsCodccb", Types.VARCHAR },
			{ "RcsCpcori", Types.VARCHAR },
			{ "RcsTesint", Types.BOOLEAN }
		};
	public static final String TABLE_SQL_CREATE = "create table COSCLF (RcsTipo BOOLEAN not null,RcsCodclf VARCHAR(75) not null,RcsCodcon VARCHAR(75) null,RcsMaxsco DOUBLE,RcsEstcon BOOLEAN,RcsCodpag VARCHAR(75) null,RcsCodbaa VARCHAR(75) null,RcsCodccb VARCHAR(75) null,RcsCpcori VARCHAR(75) null,RcsTesint BOOLEAN,primary key (RcsTipo, RcsCodclf))";
	public static final String TABLE_SQL_DROP = "drop table COSCLF";
	public static final String ORDER_BY_JPQL = " ORDER BY datiCostantiClientiFornitori.id.tipoSoggetto ASC, datiCostantiClientiFornitori.id.codiceSoggetto ASC";
	public static final String ORDER_BY_SQL = " ORDER BY COSCLF.RcsTipo ASC, COSCLF.RcsCodclf ASC";
	public static final String DATA_SOURCE = "agentiDS";
	public static final String SESSION_FACTORY = "agentiSessionFactory";
	public static final String TX_MANAGER = "agentiTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.it.bysoftware.ct.model.DatiCostantiClientiFornitori"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.it.bysoftware.ct.model.DatiCostantiClientiFornitori"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = false;

	/**
	 * Converts the soap model instance into a normal model instance.
	 *
	 * @param soapModel the soap model instance to convert
	 * @return the normal model instance
	 */
	public static DatiCostantiClientiFornitori toModel(
		DatiCostantiClientiFornitoriSoap soapModel) {
		if (soapModel == null) {
			return null;
		}

		DatiCostantiClientiFornitori model = new DatiCostantiClientiFornitoriImpl();

		model.setTipoSoggetto(soapModel.getTipoSoggetto());
		model.setCodiceSoggetto(soapModel.getCodiceSoggetto());
		model.setCodiceSottoconto(soapModel.getCodiceSottoconto());
		model.setMassimoScoperto(soapModel.getMassimoScoperto());
		model.setGestioneEstrattoConto(soapModel.getGestioneEstrattoConto());
		model.setCodicePagamento(soapModel.getCodicePagamento());
		model.setCodiceBancaPagFor(soapModel.getCodiceBancaPagFor());
		model.setCodiceAgenziaPagFor(soapModel.getCodiceAgenziaPagFor());
		model.setCodiceContropartita(soapModel.getCodiceContropartita());
		model.setCalcoloIntMora(soapModel.getCalcoloIntMora());

		return model;
	}

	/**
	 * Converts the soap model instances into normal model instances.
	 *
	 * @param soapModels the soap model instances to convert
	 * @return the normal model instances
	 */
	public static List<DatiCostantiClientiFornitori> toModels(
		DatiCostantiClientiFornitoriSoap[] soapModels) {
		if (soapModels == null) {
			return null;
		}

		List<DatiCostantiClientiFornitori> models = new ArrayList<DatiCostantiClientiFornitori>(soapModels.length);

		for (DatiCostantiClientiFornitoriSoap soapModel : soapModels) {
			models.add(toModel(soapModel));
		}

		return models;
	}

	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.it.bysoftware.ct.model.DatiCostantiClientiFornitori"));

	public DatiCostantiClientiFornitoriModelImpl() {
	}

	@Override
	public DatiCostantiClientiFornitoriPK getPrimaryKey() {
		return new DatiCostantiClientiFornitoriPK(_tipoSoggetto, _codiceSoggetto);
	}

	@Override
	public void setPrimaryKey(DatiCostantiClientiFornitoriPK primaryKey) {
		setTipoSoggetto(primaryKey.tipoSoggetto);
		setCodiceSoggetto(primaryKey.codiceSoggetto);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new DatiCostantiClientiFornitoriPK(_tipoSoggetto, _codiceSoggetto);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((DatiCostantiClientiFornitoriPK)primaryKeyObj);
	}

	@Override
	public Class<?> getModelClass() {
		return DatiCostantiClientiFornitori.class;
	}

	@Override
	public String getModelClassName() {
		return DatiCostantiClientiFornitori.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceSoggetto", getCodiceSoggetto());
		attributes.put("codiceSottoconto", getCodiceSottoconto());
		attributes.put("massimoScoperto", getMassimoScoperto());
		attributes.put("gestioneEstrattoConto", getGestioneEstrattoConto());
		attributes.put("codicePagamento", getCodicePagamento());
		attributes.put("codiceBancaPagFor", getCodiceBancaPagFor());
		attributes.put("codiceAgenziaPagFor", getCodiceAgenziaPagFor());
		attributes.put("codiceContropartita", getCodiceContropartita());
		attributes.put("calcoloIntMora", getCalcoloIntMora());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceSoggetto = (String)attributes.get("codiceSoggetto");

		if (codiceSoggetto != null) {
			setCodiceSoggetto(codiceSoggetto);
		}

		String codiceSottoconto = (String)attributes.get("codiceSottoconto");

		if (codiceSottoconto != null) {
			setCodiceSottoconto(codiceSottoconto);
		}

		Double massimoScoperto = (Double)attributes.get("massimoScoperto");

		if (massimoScoperto != null) {
			setMassimoScoperto(massimoScoperto);
		}

		Boolean gestioneEstrattoConto = (Boolean)attributes.get(
				"gestioneEstrattoConto");

		if (gestioneEstrattoConto != null) {
			setGestioneEstrattoConto(gestioneEstrattoConto);
		}

		String codicePagamento = (String)attributes.get("codicePagamento");

		if (codicePagamento != null) {
			setCodicePagamento(codicePagamento);
		}

		String codiceBancaPagFor = (String)attributes.get("codiceBancaPagFor");

		if (codiceBancaPagFor != null) {
			setCodiceBancaPagFor(codiceBancaPagFor);
		}

		String codiceAgenziaPagFor = (String)attributes.get(
				"codiceAgenziaPagFor");

		if (codiceAgenziaPagFor != null) {
			setCodiceAgenziaPagFor(codiceAgenziaPagFor);
		}

		String codiceContropartita = (String)attributes.get(
				"codiceContropartita");

		if (codiceContropartita != null) {
			setCodiceContropartita(codiceContropartita);
		}

		Boolean calcoloIntMora = (Boolean)attributes.get("calcoloIntMora");

		if (calcoloIntMora != null) {
			setCalcoloIntMora(calcoloIntMora);
		}
	}

	@JSON
	@Override
	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;
	}

	@JSON
	@Override
	public String getCodiceSoggetto() {
		if (_codiceSoggetto == null) {
			return StringPool.BLANK;
		}
		else {
			return _codiceSoggetto;
		}
	}

	@Override
	public void setCodiceSoggetto(String codiceSoggetto) {
		_codiceSoggetto = codiceSoggetto;
	}

	@JSON
	@Override
	public String getCodiceSottoconto() {
		if (_codiceSottoconto == null) {
			return StringPool.BLANK;
		}
		else {
			return _codiceSottoconto;
		}
	}

	@Override
	public void setCodiceSottoconto(String codiceSottoconto) {
		_codiceSottoconto = codiceSottoconto;
	}

	@JSON
	@Override
	public double getMassimoScoperto() {
		return _massimoScoperto;
	}

	@Override
	public void setMassimoScoperto(double massimoScoperto) {
		_massimoScoperto = massimoScoperto;
	}

	@JSON
	@Override
	public boolean getGestioneEstrattoConto() {
		return _gestioneEstrattoConto;
	}

	@Override
	public boolean isGestioneEstrattoConto() {
		return _gestioneEstrattoConto;
	}

	@Override
	public void setGestioneEstrattoConto(boolean gestioneEstrattoConto) {
		_gestioneEstrattoConto = gestioneEstrattoConto;
	}

	@JSON
	@Override
	public String getCodicePagamento() {
		if (_codicePagamento == null) {
			return StringPool.BLANK;
		}
		else {
			return _codicePagamento;
		}
	}

	@Override
	public void setCodicePagamento(String codicePagamento) {
		_codicePagamento = codicePagamento;
	}

	@JSON
	@Override
	public String getCodiceBancaPagFor() {
		if (_codiceBancaPagFor == null) {
			return StringPool.BLANK;
		}
		else {
			return _codiceBancaPagFor;
		}
	}

	@Override
	public void setCodiceBancaPagFor(String codiceBancaPagFor) {
		_codiceBancaPagFor = codiceBancaPagFor;
	}

	@JSON
	@Override
	public String getCodiceAgenziaPagFor() {
		if (_codiceAgenziaPagFor == null) {
			return StringPool.BLANK;
		}
		else {
			return _codiceAgenziaPagFor;
		}
	}

	@Override
	public void setCodiceAgenziaPagFor(String codiceAgenziaPagFor) {
		_codiceAgenziaPagFor = codiceAgenziaPagFor;
	}

	@JSON
	@Override
	public String getCodiceContropartita() {
		if (_codiceContropartita == null) {
			return StringPool.BLANK;
		}
		else {
			return _codiceContropartita;
		}
	}

	@Override
	public void setCodiceContropartita(String codiceContropartita) {
		_codiceContropartita = codiceContropartita;
	}

	@JSON
	@Override
	public boolean getCalcoloIntMora() {
		return _calcoloIntMora;
	}

	@Override
	public boolean isCalcoloIntMora() {
		return _calcoloIntMora;
	}

	@Override
	public void setCalcoloIntMora(boolean calcoloIntMora) {
		_calcoloIntMora = calcoloIntMora;
	}

	@Override
	public DatiCostantiClientiFornitori toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (DatiCostantiClientiFornitori)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		DatiCostantiClientiFornitoriImpl datiCostantiClientiFornitoriImpl = new DatiCostantiClientiFornitoriImpl();

		datiCostantiClientiFornitoriImpl.setTipoSoggetto(getTipoSoggetto());
		datiCostantiClientiFornitoriImpl.setCodiceSoggetto(getCodiceSoggetto());
		datiCostantiClientiFornitoriImpl.setCodiceSottoconto(getCodiceSottoconto());
		datiCostantiClientiFornitoriImpl.setMassimoScoperto(getMassimoScoperto());
		datiCostantiClientiFornitoriImpl.setGestioneEstrattoConto(getGestioneEstrattoConto());
		datiCostantiClientiFornitoriImpl.setCodicePagamento(getCodicePagamento());
		datiCostantiClientiFornitoriImpl.setCodiceBancaPagFor(getCodiceBancaPagFor());
		datiCostantiClientiFornitoriImpl.setCodiceAgenziaPagFor(getCodiceAgenziaPagFor());
		datiCostantiClientiFornitoriImpl.setCodiceContropartita(getCodiceContropartita());
		datiCostantiClientiFornitoriImpl.setCalcoloIntMora(getCalcoloIntMora());

		datiCostantiClientiFornitoriImpl.resetOriginalValues();

		return datiCostantiClientiFornitoriImpl;
	}

	@Override
	public int compareTo(
		DatiCostantiClientiFornitori datiCostantiClientiFornitori) {
		DatiCostantiClientiFornitoriPK primaryKey = datiCostantiClientiFornitori.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DatiCostantiClientiFornitori)) {
			return false;
		}

		DatiCostantiClientiFornitori datiCostantiClientiFornitori = (DatiCostantiClientiFornitori)obj;

		DatiCostantiClientiFornitoriPK primaryKey = datiCostantiClientiFornitori.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public void resetOriginalValues() {
	}

	@Override
	public CacheModel<DatiCostantiClientiFornitori> toCacheModel() {
		DatiCostantiClientiFornitoriCacheModel datiCostantiClientiFornitoriCacheModel =
			new DatiCostantiClientiFornitoriCacheModel();

		datiCostantiClientiFornitoriCacheModel.tipoSoggetto = getTipoSoggetto();

		datiCostantiClientiFornitoriCacheModel.codiceSoggetto = getCodiceSoggetto();

		String codiceSoggetto = datiCostantiClientiFornitoriCacheModel.codiceSoggetto;

		if ((codiceSoggetto != null) && (codiceSoggetto.length() == 0)) {
			datiCostantiClientiFornitoriCacheModel.codiceSoggetto = null;
		}

		datiCostantiClientiFornitoriCacheModel.codiceSottoconto = getCodiceSottoconto();

		String codiceSottoconto = datiCostantiClientiFornitoriCacheModel.codiceSottoconto;

		if ((codiceSottoconto != null) && (codiceSottoconto.length() == 0)) {
			datiCostantiClientiFornitoriCacheModel.codiceSottoconto = null;
		}

		datiCostantiClientiFornitoriCacheModel.massimoScoperto = getMassimoScoperto();

		datiCostantiClientiFornitoriCacheModel.gestioneEstrattoConto = getGestioneEstrattoConto();

		datiCostantiClientiFornitoriCacheModel.codicePagamento = getCodicePagamento();

		String codicePagamento = datiCostantiClientiFornitoriCacheModel.codicePagamento;

		if ((codicePagamento != null) && (codicePagamento.length() == 0)) {
			datiCostantiClientiFornitoriCacheModel.codicePagamento = null;
		}

		datiCostantiClientiFornitoriCacheModel.codiceBancaPagFor = getCodiceBancaPagFor();

		String codiceBancaPagFor = datiCostantiClientiFornitoriCacheModel.codiceBancaPagFor;

		if ((codiceBancaPagFor != null) && (codiceBancaPagFor.length() == 0)) {
			datiCostantiClientiFornitoriCacheModel.codiceBancaPagFor = null;
		}

		datiCostantiClientiFornitoriCacheModel.codiceAgenziaPagFor = getCodiceAgenziaPagFor();

		String codiceAgenziaPagFor = datiCostantiClientiFornitoriCacheModel.codiceAgenziaPagFor;

		if ((codiceAgenziaPagFor != null) &&
				(codiceAgenziaPagFor.length() == 0)) {
			datiCostantiClientiFornitoriCacheModel.codiceAgenziaPagFor = null;
		}

		datiCostantiClientiFornitoriCacheModel.codiceContropartita = getCodiceContropartita();

		String codiceContropartita = datiCostantiClientiFornitoriCacheModel.codiceContropartita;

		if ((codiceContropartita != null) &&
				(codiceContropartita.length() == 0)) {
			datiCostantiClientiFornitoriCacheModel.codiceContropartita = null;
		}

		datiCostantiClientiFornitoriCacheModel.calcoloIntMora = getCalcoloIntMora();

		return datiCostantiClientiFornitoriCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{tipoSoggetto=");
		sb.append(getTipoSoggetto());
		sb.append(", codiceSoggetto=");
		sb.append(getCodiceSoggetto());
		sb.append(", codiceSottoconto=");
		sb.append(getCodiceSottoconto());
		sb.append(", massimoScoperto=");
		sb.append(getMassimoScoperto());
		sb.append(", gestioneEstrattoConto=");
		sb.append(getGestioneEstrattoConto());
		sb.append(", codicePagamento=");
		sb.append(getCodicePagamento());
		sb.append(", codiceBancaPagFor=");
		sb.append(getCodiceBancaPagFor());
		sb.append(", codiceAgenziaPagFor=");
		sb.append(getCodiceAgenziaPagFor());
		sb.append(", codiceContropartita=");
		sb.append(getCodiceContropartita());
		sb.append(", calcoloIntMora=");
		sb.append(getCalcoloIntMora());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(34);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.DatiCostantiClientiFornitori");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>tipoSoggetto</column-name><column-value><![CDATA[");
		sb.append(getTipoSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceSoggetto</column-name><column-value><![CDATA[");
		sb.append(getCodiceSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceSottoconto</column-name><column-value><![CDATA[");
		sb.append(getCodiceSottoconto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>massimoScoperto</column-name><column-value><![CDATA[");
		sb.append(getMassimoScoperto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>gestioneEstrattoConto</column-name><column-value><![CDATA[");
		sb.append(getGestioneEstrattoConto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codicePagamento</column-name><column-value><![CDATA[");
		sb.append(getCodicePagamento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceBancaPagFor</column-name><column-value><![CDATA[");
		sb.append(getCodiceBancaPagFor());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceAgenziaPagFor</column-name><column-value><![CDATA[");
		sb.append(getCodiceAgenziaPagFor());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceContropartita</column-name><column-value><![CDATA[");
		sb.append(getCodiceContropartita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>calcoloIntMora</column-name><column-value><![CDATA[");
		sb.append(getCalcoloIntMora());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = DatiCostantiClientiFornitori.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			DatiCostantiClientiFornitori.class
		};
	private boolean _tipoSoggetto;
	private String _codiceSoggetto;
	private String _codiceSottoconto;
	private double _massimoScoperto;
	private boolean _gestioneEstrattoConto;
	private String _codicePagamento;
	private String _codiceBancaPagFor;
	private String _codiceAgenziaPagFor;
	private String _codiceContropartita;
	private boolean _calcoloIntMora;
	private DatiCostantiClientiFornitori _escapedModel;
}