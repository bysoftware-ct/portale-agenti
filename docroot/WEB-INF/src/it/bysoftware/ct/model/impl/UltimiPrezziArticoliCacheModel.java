/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.UltimiPrezziArticoli;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing UltimiPrezziArticoli in entity cache.
 *
 * @author Mario Torrisi
 * @see UltimiPrezziArticoli
 * @generated
 */
public class UltimiPrezziArticoliCacheModel implements CacheModel<UltimiPrezziArticoli>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{tipoSoggetto=");
		sb.append(tipoSoggetto);
		sb.append(", codiceSoggetto=");
		sb.append(codiceSoggetto);
		sb.append(", codiceArticolo=");
		sb.append(codiceArticolo);
		sb.append(", codiceVariante=");
		sb.append(codiceVariante);
		sb.append(", dataDocumento=");
		sb.append(dataDocumento);
		sb.append(", codiceDivisa=");
		sb.append(codiceDivisa);
		sb.append(", prezzo=");
		sb.append(prezzo);
		sb.append(", sconto1=");
		sb.append(sconto1);
		sb.append(", sconto2=");
		sb.append(sconto2);
		sb.append(", sconto3=");
		sb.append(sconto3);
		sb.append(", scontoChiusura=");
		sb.append(scontoChiusura);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UltimiPrezziArticoli toEntityModel() {
		UltimiPrezziArticoliImpl ultimiPrezziArticoliImpl = new UltimiPrezziArticoliImpl();

		ultimiPrezziArticoliImpl.setTipoSoggetto(tipoSoggetto);

		if (codiceSoggetto == null) {
			ultimiPrezziArticoliImpl.setCodiceSoggetto(StringPool.BLANK);
		}
		else {
			ultimiPrezziArticoliImpl.setCodiceSoggetto(codiceSoggetto);
		}

		if (codiceArticolo == null) {
			ultimiPrezziArticoliImpl.setCodiceArticolo(StringPool.BLANK);
		}
		else {
			ultimiPrezziArticoliImpl.setCodiceArticolo(codiceArticolo);
		}

		if (codiceVariante == null) {
			ultimiPrezziArticoliImpl.setCodiceVariante(StringPool.BLANK);
		}
		else {
			ultimiPrezziArticoliImpl.setCodiceVariante(codiceVariante);
		}

		if (dataDocumento == Long.MIN_VALUE) {
			ultimiPrezziArticoliImpl.setDataDocumento(null);
		}
		else {
			ultimiPrezziArticoliImpl.setDataDocumento(new Date(dataDocumento));
		}

		if (codiceDivisa == null) {
			ultimiPrezziArticoliImpl.setCodiceDivisa(StringPool.BLANK);
		}
		else {
			ultimiPrezziArticoliImpl.setCodiceDivisa(codiceDivisa);
		}

		ultimiPrezziArticoliImpl.setPrezzo(prezzo);
		ultimiPrezziArticoliImpl.setSconto1(sconto1);
		ultimiPrezziArticoliImpl.setSconto2(sconto2);
		ultimiPrezziArticoliImpl.setSconto3(sconto3);
		ultimiPrezziArticoliImpl.setScontoChiusura(scontoChiusura);

		ultimiPrezziArticoliImpl.resetOriginalValues();

		return ultimiPrezziArticoliImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		tipoSoggetto = objectInput.readBoolean();
		codiceSoggetto = objectInput.readUTF();
		codiceArticolo = objectInput.readUTF();
		codiceVariante = objectInput.readUTF();
		dataDocumento = objectInput.readLong();
		codiceDivisa = objectInput.readUTF();
		prezzo = objectInput.readDouble();
		sconto1 = objectInput.readDouble();
		sconto2 = objectInput.readDouble();
		sconto3 = objectInput.readDouble();
		scontoChiusura = objectInput.readDouble();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeBoolean(tipoSoggetto);

		if (codiceSoggetto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceSoggetto);
		}

		if (codiceArticolo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceArticolo);
		}

		if (codiceVariante == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceVariante);
		}

		objectOutput.writeLong(dataDocumento);

		if (codiceDivisa == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDivisa);
		}

		objectOutput.writeDouble(prezzo);
		objectOutput.writeDouble(sconto1);
		objectOutput.writeDouble(sconto2);
		objectOutput.writeDouble(sconto3);
		objectOutput.writeDouble(scontoChiusura);
	}

	public boolean tipoSoggetto;
	public String codiceSoggetto;
	public String codiceArticolo;
	public String codiceVariante;
	public long dataDocumento;
	public String codiceDivisa;
	public double prezzo;
	public double sconto1;
	public double sconto2;
	public double sconto3;
	public double scontoChiusura;
}