/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.PianoPagamenti;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing PianoPagamenti in entity cache.
 *
 * @author Mario Torrisi
 * @see PianoPagamenti
 * @generated
 */
public class PianoPagamentiCacheModel implements CacheModel<PianoPagamenti>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{codicePianoPagamento=");
		sb.append(codicePianoPagamento);
		sb.append(", descrizione=");
		sb.append(descrizione);
		sb.append(", primoMeseEscluso=");
		sb.append(primoMeseEscluso);
		sb.append(", giornoPrimoMeseSucc=");
		sb.append(giornoPrimoMeseSucc);
		sb.append(", giornoSecondoMeseSucc=");
		sb.append(giornoSecondoMeseSucc);
		sb.append(", secondoMeseEscluso=");
		sb.append(secondoMeseEscluso);
		sb.append(", inizioFattDaBolla=");
		sb.append(inizioFattDaBolla);
		sb.append(", percScChiusura=");
		sb.append(percScChiusura);
		sb.append(", percScCassa=");
		sb.append(percScCassa);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public PianoPagamenti toEntityModel() {
		PianoPagamentiImpl pianoPagamentiImpl = new PianoPagamentiImpl();

		if (codicePianoPagamento == null) {
			pianoPagamentiImpl.setCodicePianoPagamento(StringPool.BLANK);
		}
		else {
			pianoPagamentiImpl.setCodicePianoPagamento(codicePianoPagamento);
		}

		if (descrizione == null) {
			pianoPagamentiImpl.setDescrizione(StringPool.BLANK);
		}
		else {
			pianoPagamentiImpl.setDescrizione(descrizione);
		}

		pianoPagamentiImpl.setPrimoMeseEscluso(primoMeseEscluso);
		pianoPagamentiImpl.setGiornoPrimoMeseSucc(giornoPrimoMeseSucc);
		pianoPagamentiImpl.setGiornoSecondoMeseSucc(giornoSecondoMeseSucc);
		pianoPagamentiImpl.setSecondoMeseEscluso(secondoMeseEscluso);
		pianoPagamentiImpl.setInizioFattDaBolla(inizioFattDaBolla);
		pianoPagamentiImpl.setPercScChiusura(percScChiusura);
		pianoPagamentiImpl.setPercScCassa(percScCassa);

		pianoPagamentiImpl.resetOriginalValues();

		return pianoPagamentiImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		codicePianoPagamento = objectInput.readUTF();
		descrizione = objectInput.readUTF();
		primoMeseEscluso = objectInput.readInt();
		giornoPrimoMeseSucc = objectInput.readInt();
		giornoSecondoMeseSucc = objectInput.readInt();
		secondoMeseEscluso = objectInput.readInt();
		inizioFattDaBolla = objectInput.readBoolean();
		percScChiusura = objectInput.readDouble();
		percScCassa = objectInput.readDouble();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (codicePianoPagamento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codicePianoPagamento);
		}

		if (descrizione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizione);
		}

		objectOutput.writeInt(primoMeseEscluso);
		objectOutput.writeInt(giornoPrimoMeseSucc);
		objectOutput.writeInt(giornoSecondoMeseSucc);
		objectOutput.writeInt(secondoMeseEscluso);
		objectOutput.writeBoolean(inizioFattDaBolla);
		objectOutput.writeDouble(percScChiusura);
		objectOutput.writeDouble(percScCassa);
	}

	public String codicePianoPagamento;
	public String descrizione;
	public int primoMeseEscluso;
	public int giornoPrimoMeseSucc;
	public int giornoSecondoMeseSucc;
	public int secondoMeseEscluso;
	public boolean inizioFattDaBolla;
	public double percScChiusura;
	public double percScCassa;
}