/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.ScontiArticoli;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ScontiArticoli in entity cache.
 *
 * @author Mario Torrisi
 * @see ScontiArticoli
 * @generated
 */
public class ScontiArticoliCacheModel implements CacheModel<ScontiArticoli>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{codScontoCliente=");
		sb.append(codScontoCliente);
		sb.append(", codScontoArticolo=");
		sb.append(codScontoArticolo);
		sb.append(", sconto=");
		sb.append(sconto);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ScontiArticoli toEntityModel() {
		ScontiArticoliImpl scontiArticoliImpl = new ScontiArticoliImpl();

		if (codScontoCliente == null) {
			scontiArticoliImpl.setCodScontoCliente(StringPool.BLANK);
		}
		else {
			scontiArticoliImpl.setCodScontoCliente(codScontoCliente);
		}

		if (codScontoArticolo == null) {
			scontiArticoliImpl.setCodScontoArticolo(StringPool.BLANK);
		}
		else {
			scontiArticoliImpl.setCodScontoArticolo(codScontoArticolo);
		}

		scontiArticoliImpl.setSconto(sconto);

		scontiArticoliImpl.resetOriginalValues();

		return scontiArticoliImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		codScontoCliente = objectInput.readUTF();
		codScontoArticolo = objectInput.readUTF();
		sconto = objectInput.readDouble();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (codScontoCliente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codScontoCliente);
		}

		if (codScontoArticolo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codScontoArticolo);
		}

		objectOutput.writeDouble(sconto);
	}

	public String codScontoCliente;
	public String codScontoArticolo;
	public double sconto;
}