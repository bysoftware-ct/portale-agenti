/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.AnagraficaClientiFornitori;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing AnagraficaClientiFornitori in entity cache.
 *
 * @author Mario Torrisi
 * @see AnagraficaClientiFornitori
 * @generated
 */
public class AnagraficaClientiFornitoriCacheModel implements CacheModel<AnagraficaClientiFornitori>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{codiceSoggetto=");
		sb.append(codiceSoggetto);
		sb.append(", ragioneSociale=");
		sb.append(ragioneSociale);
		sb.append(", ragioneSocialeAggiuntiva=");
		sb.append(ragioneSocialeAggiuntiva);
		sb.append(", indirizzo=");
		sb.append(indirizzo);
		sb.append(", comune=");
		sb.append(comune);
		sb.append(", CAP=");
		sb.append(CAP);
		sb.append(", siglaProvincia=");
		sb.append(siglaProvincia);
		sb.append(", siglaStato=");
		sb.append(siglaStato);
		sb.append(", partitaIVA=");
		sb.append(partitaIVA);
		sb.append(", codiceFiscale=");
		sb.append(codiceFiscale);
		sb.append(", note=");
		sb.append(note);
		sb.append(", tipoSoggetto=");
		sb.append(tipoSoggetto);
		sb.append(", codiceMnemonico=");
		sb.append(codiceMnemonico);
		sb.append(", tipoSollecito=");
		sb.append(tipoSollecito);
		sb.append(", attivoEC=");
		sb.append(attivoEC);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public AnagraficaClientiFornitori toEntityModel() {
		AnagraficaClientiFornitoriImpl anagraficaClientiFornitoriImpl = new AnagraficaClientiFornitoriImpl();

		if (codiceSoggetto == null) {
			anagraficaClientiFornitoriImpl.setCodiceSoggetto(StringPool.BLANK);
		}
		else {
			anagraficaClientiFornitoriImpl.setCodiceSoggetto(codiceSoggetto);
		}

		if (ragioneSociale == null) {
			anagraficaClientiFornitoriImpl.setRagioneSociale(StringPool.BLANK);
		}
		else {
			anagraficaClientiFornitoriImpl.setRagioneSociale(ragioneSociale);
		}

		if (ragioneSocialeAggiuntiva == null) {
			anagraficaClientiFornitoriImpl.setRagioneSocialeAggiuntiva(StringPool.BLANK);
		}
		else {
			anagraficaClientiFornitoriImpl.setRagioneSocialeAggiuntiva(ragioneSocialeAggiuntiva);
		}

		if (indirizzo == null) {
			anagraficaClientiFornitoriImpl.setIndirizzo(StringPool.BLANK);
		}
		else {
			anagraficaClientiFornitoriImpl.setIndirizzo(indirizzo);
		}

		if (comune == null) {
			anagraficaClientiFornitoriImpl.setComune(StringPool.BLANK);
		}
		else {
			anagraficaClientiFornitoriImpl.setComune(comune);
		}

		if (CAP == null) {
			anagraficaClientiFornitoriImpl.setCAP(StringPool.BLANK);
		}
		else {
			anagraficaClientiFornitoriImpl.setCAP(CAP);
		}

		if (siglaProvincia == null) {
			anagraficaClientiFornitoriImpl.setSiglaProvincia(StringPool.BLANK);
		}
		else {
			anagraficaClientiFornitoriImpl.setSiglaProvincia(siglaProvincia);
		}

		if (siglaStato == null) {
			anagraficaClientiFornitoriImpl.setSiglaStato(StringPool.BLANK);
		}
		else {
			anagraficaClientiFornitoriImpl.setSiglaStato(siglaStato);
		}

		if (partitaIVA == null) {
			anagraficaClientiFornitoriImpl.setPartitaIVA(StringPool.BLANK);
		}
		else {
			anagraficaClientiFornitoriImpl.setPartitaIVA(partitaIVA);
		}

		if (codiceFiscale == null) {
			anagraficaClientiFornitoriImpl.setCodiceFiscale(StringPool.BLANK);
		}
		else {
			anagraficaClientiFornitoriImpl.setCodiceFiscale(codiceFiscale);
		}

		if (note == null) {
			anagraficaClientiFornitoriImpl.setNote(StringPool.BLANK);
		}
		else {
			anagraficaClientiFornitoriImpl.setNote(note);
		}

		anagraficaClientiFornitoriImpl.setTipoSoggetto(tipoSoggetto);

		if (codiceMnemonico == null) {
			anagraficaClientiFornitoriImpl.setCodiceMnemonico(StringPool.BLANK);
		}
		else {
			anagraficaClientiFornitoriImpl.setCodiceMnemonico(codiceMnemonico);
		}

		anagraficaClientiFornitoriImpl.setTipoSollecito(tipoSollecito);
		anagraficaClientiFornitoriImpl.setAttivoEC(attivoEC);

		anagraficaClientiFornitoriImpl.resetOriginalValues();

		return anagraficaClientiFornitoriImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		codiceSoggetto = objectInput.readUTF();
		ragioneSociale = objectInput.readUTF();
		ragioneSocialeAggiuntiva = objectInput.readUTF();
		indirizzo = objectInput.readUTF();
		comune = objectInput.readUTF();
		CAP = objectInput.readUTF();
		siglaProvincia = objectInput.readUTF();
		siglaStato = objectInput.readUTF();
		partitaIVA = objectInput.readUTF();
		codiceFiscale = objectInput.readUTF();
		note = objectInput.readUTF();
		tipoSoggetto = objectInput.readBoolean();
		codiceMnemonico = objectInput.readUTF();
		tipoSollecito = objectInput.readInt();
		attivoEC = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (codiceSoggetto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceSoggetto);
		}

		if (ragioneSociale == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ragioneSociale);
		}

		if (ragioneSocialeAggiuntiva == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ragioneSocialeAggiuntiva);
		}

		if (indirizzo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(indirizzo);
		}

		if (comune == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(comune);
		}

		if (CAP == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(CAP);
		}

		if (siglaProvincia == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(siglaProvincia);
		}

		if (siglaStato == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(siglaStato);
		}

		if (partitaIVA == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(partitaIVA);
		}

		if (codiceFiscale == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceFiscale);
		}

		if (note == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(note);
		}

		objectOutput.writeBoolean(tipoSoggetto);

		if (codiceMnemonico == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceMnemonico);
		}

		objectOutput.writeInt(tipoSollecito);
		objectOutput.writeBoolean(attivoEC);
	}

	public String codiceSoggetto;
	public String ragioneSociale;
	public String ragioneSocialeAggiuntiva;
	public String indirizzo;
	public String comune;
	public String CAP;
	public String siglaProvincia;
	public String siglaStato;
	public String partitaIVA;
	public String codiceFiscale;
	public String note;
	public boolean tipoSoggetto;
	public String codiceMnemonico;
	public int tipoSollecito;
	public boolean attivoEC;
}