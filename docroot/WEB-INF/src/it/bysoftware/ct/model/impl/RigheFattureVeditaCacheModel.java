/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.RigheFattureVedita;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing RigheFattureVedita in entity cache.
 *
 * @author Mario Torrisi
 * @see RigheFattureVedita
 * @generated
 */
public class RigheFattureVeditaCacheModel implements CacheModel<RigheFattureVedita>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(139);

		sb.append("{anno=");
		sb.append(anno);
		sb.append(", codiceAttivita=");
		sb.append(codiceAttivita);
		sb.append(", codiceCentro=");
		sb.append(codiceCentro);
		sb.append(", numeroProtocollo=");
		sb.append(numeroProtocollo);
		sb.append(", numeroRigo=");
		sb.append(numeroRigo);
		sb.append(", statoRigo=");
		sb.append(statoRigo);
		sb.append(", annoBolla=");
		sb.append(annoBolla);
		sb.append(", tipoIdentificativoBolla=");
		sb.append(tipoIdentificativoBolla);
		sb.append(", tipoBolla=");
		sb.append(tipoBolla);
		sb.append(", codiceDeposito=");
		sb.append(codiceDeposito);
		sb.append(", numeroBollettario=");
		sb.append(numeroBollettario);
		sb.append(", numeroBolla=");
		sb.append(numeroBolla);
		sb.append(", dataBollaEvasa=");
		sb.append(dataBollaEvasa);
		sb.append(", rigoDDT=");
		sb.append(rigoDDT);
		sb.append(", tipoRigoDDT=");
		sb.append(tipoRigoDDT);
		sb.append(", codiceCausaleMagazzino=");
		sb.append(codiceCausaleMagazzino);
		sb.append(", codiceDepositoMagazzino=");
		sb.append(codiceDepositoMagazzino);
		sb.append(", codiceArticolo=");
		sb.append(codiceArticolo);
		sb.append(", codiceVariante=");
		sb.append(codiceVariante);
		sb.append(", codiceTestoDescrizioni=");
		sb.append(codiceTestoDescrizioni);
		sb.append(", descrizione=");
		sb.append(descrizione);
		sb.append(", unitaMisura=");
		sb.append(unitaMisura);
		sb.append(", numeroDecimalliQuant=");
		sb.append(numeroDecimalliQuant);
		sb.append(", quantita1=");
		sb.append(quantita1);
		sb.append(", quantita2=");
		sb.append(quantita2);
		sb.append(", quantita3=");
		sb.append(quantita3);
		sb.append(", quantita=");
		sb.append(quantita);
		sb.append(", unitaMisura2=");
		sb.append(unitaMisura2);
		sb.append(", quantitaUnitMisSec=");
		sb.append(quantitaUnitMisSec);
		sb.append(", numeroDecimalliPrezzo=");
		sb.append(numeroDecimalliPrezzo);
		sb.append(", prezzo=");
		sb.append(prezzo);
		sb.append(", prezzoIVA=");
		sb.append(prezzoIVA);
		sb.append(", importoLordo=");
		sb.append(importoLordo);
		sb.append(", importoLordoIVA=");
		sb.append(importoLordoIVA);
		sb.append(", sconto1=");
		sb.append(sconto1);
		sb.append(", sconto2=");
		sb.append(sconto2);
		sb.append(", sconto3=");
		sb.append(sconto3);
		sb.append(", importoNettoRigo=");
		sb.append(importoNettoRigo);
		sb.append(", importoNettoRigoIVA=");
		sb.append(importoNettoRigoIVA);
		sb.append(", importoNetto=");
		sb.append(importoNetto);
		sb.append(", importoNettoIVA=");
		sb.append(importoNettoIVA);
		sb.append(", importoCONAIAddebitatoInt=");
		sb.append(importoCONAIAddebitatoInt);
		sb.append(", importoCONAIAddebitatoOrig=");
		sb.append(importoCONAIAddebitatoOrig);
		sb.append(", codiceIVAFatturazione=");
		sb.append(codiceIVAFatturazione);
		sb.append(", nomenclaturaCombinata=");
		sb.append(nomenclaturaCombinata);
		sb.append(", testStampaDisBase=");
		sb.append(testStampaDisBase);
		sb.append(", tipoOrdineEvaso=");
		sb.append(tipoOrdineEvaso);
		sb.append(", annoOrdineEvaso=");
		sb.append(annoOrdineEvaso);
		sb.append(", numeroOrdineEvaso=");
		sb.append(numeroOrdineEvaso);
		sb.append(", numeroRigoOrdineEvaso=");
		sb.append(numeroRigoOrdineEvaso);
		sb.append(", tipoEvasione=");
		sb.append(tipoEvasione);
		sb.append(", descrizioneRiferimento=");
		sb.append(descrizioneRiferimento);
		sb.append(", numeroPrimaNotaMagazzino=");
		sb.append(numeroPrimaNotaMagazzino);
		sb.append(", numeroMovimentoMagazzino=");
		sb.append(numeroMovimentoMagazzino);
		sb.append(", tipoEsplosione=");
		sb.append(tipoEsplosione);
		sb.append(", livelloMassimoEsplosione=");
		sb.append(livelloMassimoEsplosione);
		sb.append(", libStr1=");
		sb.append(libStr1);
		sb.append(", libStr2=");
		sb.append(libStr2);
		sb.append(", libStr3=");
		sb.append(libStr3);
		sb.append(", libDbl1=");
		sb.append(libDbl1);
		sb.append(", libDbl2=");
		sb.append(libDbl2);
		sb.append(", libDbl3=");
		sb.append(libDbl3);
		sb.append(", libLng1=");
		sb.append(libLng1);
		sb.append(", libLng2=");
		sb.append(libLng2);
		sb.append(", libLng3=");
		sb.append(libLng3);
		sb.append(", libDat1=");
		sb.append(libDat1);
		sb.append(", libDat2=");
		sb.append(libDat2);
		sb.append(", libDat3=");
		sb.append(libDat3);
		sb.append(", testFatturazioneAuto=");
		sb.append(testFatturazioneAuto);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public RigheFattureVedita toEntityModel() {
		RigheFattureVeditaImpl righeFattureVeditaImpl = new RigheFattureVeditaImpl();

		righeFattureVeditaImpl.setAnno(anno);

		if (codiceAttivita == null) {
			righeFattureVeditaImpl.setCodiceAttivita(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setCodiceAttivita(codiceAttivita);
		}

		if (codiceCentro == null) {
			righeFattureVeditaImpl.setCodiceCentro(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setCodiceCentro(codiceCentro);
		}

		righeFattureVeditaImpl.setNumeroProtocollo(numeroProtocollo);
		righeFattureVeditaImpl.setNumeroRigo(numeroRigo);
		righeFattureVeditaImpl.setStatoRigo(statoRigo);
		righeFattureVeditaImpl.setAnnoBolla(annoBolla);
		righeFattureVeditaImpl.setTipoIdentificativoBolla(tipoIdentificativoBolla);

		if (tipoBolla == null) {
			righeFattureVeditaImpl.setTipoBolla(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setTipoBolla(tipoBolla);
		}

		if (codiceDeposito == null) {
			righeFattureVeditaImpl.setCodiceDeposito(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setCodiceDeposito(codiceDeposito);
		}

		righeFattureVeditaImpl.setNumeroBollettario(numeroBollettario);
		righeFattureVeditaImpl.setNumeroBolla(numeroBolla);

		if (dataBollaEvasa == Long.MIN_VALUE) {
			righeFattureVeditaImpl.setDataBollaEvasa(null);
		}
		else {
			righeFattureVeditaImpl.setDataBollaEvasa(new Date(dataBollaEvasa));
		}

		righeFattureVeditaImpl.setRigoDDT(rigoDDT);
		righeFattureVeditaImpl.setTipoRigoDDT(tipoRigoDDT);

		if (codiceCausaleMagazzino == null) {
			righeFattureVeditaImpl.setCodiceCausaleMagazzino(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setCodiceCausaleMagazzino(codiceCausaleMagazzino);
		}

		if (codiceDepositoMagazzino == null) {
			righeFattureVeditaImpl.setCodiceDepositoMagazzino(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setCodiceDepositoMagazzino(codiceDepositoMagazzino);
		}

		if (codiceArticolo == null) {
			righeFattureVeditaImpl.setCodiceArticolo(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setCodiceArticolo(codiceArticolo);
		}

		if (codiceVariante == null) {
			righeFattureVeditaImpl.setCodiceVariante(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setCodiceVariante(codiceVariante);
		}

		if (codiceTestoDescrizioni == null) {
			righeFattureVeditaImpl.setCodiceTestoDescrizioni(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setCodiceTestoDescrizioni(codiceTestoDescrizioni);
		}

		if (descrizione == null) {
			righeFattureVeditaImpl.setDescrizione(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setDescrizione(descrizione);
		}

		if (unitaMisura == null) {
			righeFattureVeditaImpl.setUnitaMisura(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setUnitaMisura(unitaMisura);
		}

		righeFattureVeditaImpl.setNumeroDecimalliQuant(numeroDecimalliQuant);
		righeFattureVeditaImpl.setQuantita1(quantita1);
		righeFattureVeditaImpl.setQuantita2(quantita2);
		righeFattureVeditaImpl.setQuantita3(quantita3);
		righeFattureVeditaImpl.setQuantita(quantita);

		if (unitaMisura2 == null) {
			righeFattureVeditaImpl.setUnitaMisura2(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setUnitaMisura2(unitaMisura2);
		}

		righeFattureVeditaImpl.setQuantitaUnitMisSec(quantitaUnitMisSec);
		righeFattureVeditaImpl.setNumeroDecimalliPrezzo(numeroDecimalliPrezzo);
		righeFattureVeditaImpl.setPrezzo(prezzo);
		righeFattureVeditaImpl.setPrezzoIVA(prezzoIVA);
		righeFattureVeditaImpl.setImportoLordo(importoLordo);
		righeFattureVeditaImpl.setImportoLordoIVA(importoLordoIVA);
		righeFattureVeditaImpl.setSconto1(sconto1);
		righeFattureVeditaImpl.setSconto2(sconto2);
		righeFattureVeditaImpl.setSconto3(sconto3);
		righeFattureVeditaImpl.setImportoNettoRigo(importoNettoRigo);
		righeFattureVeditaImpl.setImportoNettoRigoIVA(importoNettoRigoIVA);
		righeFattureVeditaImpl.setImportoNetto(importoNetto);
		righeFattureVeditaImpl.setImportoNettoIVA(importoNettoIVA);
		righeFattureVeditaImpl.setImportoCONAIAddebitatoInt(importoCONAIAddebitatoInt);
		righeFattureVeditaImpl.setImportoCONAIAddebitatoOrig(importoCONAIAddebitatoOrig);

		if (codiceIVAFatturazione == null) {
			righeFattureVeditaImpl.setCodiceIVAFatturazione(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setCodiceIVAFatturazione(codiceIVAFatturazione);
		}

		righeFattureVeditaImpl.setNomenclaturaCombinata(nomenclaturaCombinata);
		righeFattureVeditaImpl.setTestStampaDisBase(testStampaDisBase);
		righeFattureVeditaImpl.setTipoOrdineEvaso(tipoOrdineEvaso);
		righeFattureVeditaImpl.setAnnoOrdineEvaso(annoOrdineEvaso);
		righeFattureVeditaImpl.setNumeroOrdineEvaso(numeroOrdineEvaso);
		righeFattureVeditaImpl.setNumeroRigoOrdineEvaso(numeroRigoOrdineEvaso);

		if (tipoEvasione == null) {
			righeFattureVeditaImpl.setTipoEvasione(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setTipoEvasione(tipoEvasione);
		}

		if (descrizioneRiferimento == null) {
			righeFattureVeditaImpl.setDescrizioneRiferimento(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setDescrizioneRiferimento(descrizioneRiferimento);
		}

		righeFattureVeditaImpl.setNumeroPrimaNotaMagazzino(numeroPrimaNotaMagazzino);
		righeFattureVeditaImpl.setNumeroMovimentoMagazzino(numeroMovimentoMagazzino);
		righeFattureVeditaImpl.setTipoEsplosione(tipoEsplosione);
		righeFattureVeditaImpl.setLivelloMassimoEsplosione(livelloMassimoEsplosione);

		if (libStr1 == null) {
			righeFattureVeditaImpl.setLibStr1(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setLibStr1(libStr1);
		}

		if (libStr2 == null) {
			righeFattureVeditaImpl.setLibStr2(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setLibStr2(libStr2);
		}

		if (libStr3 == null) {
			righeFattureVeditaImpl.setLibStr3(StringPool.BLANK);
		}
		else {
			righeFattureVeditaImpl.setLibStr3(libStr3);
		}

		righeFattureVeditaImpl.setLibDbl1(libDbl1);
		righeFattureVeditaImpl.setLibDbl2(libDbl2);
		righeFattureVeditaImpl.setLibDbl3(libDbl3);
		righeFattureVeditaImpl.setLibLng1(libLng1);
		righeFattureVeditaImpl.setLibLng2(libLng2);
		righeFattureVeditaImpl.setLibLng3(libLng3);

		if (libDat1 == Long.MIN_VALUE) {
			righeFattureVeditaImpl.setLibDat1(null);
		}
		else {
			righeFattureVeditaImpl.setLibDat1(new Date(libDat1));
		}

		if (libDat2 == Long.MIN_VALUE) {
			righeFattureVeditaImpl.setLibDat2(null);
		}
		else {
			righeFattureVeditaImpl.setLibDat2(new Date(libDat2));
		}

		if (libDat3 == Long.MIN_VALUE) {
			righeFattureVeditaImpl.setLibDat3(null);
		}
		else {
			righeFattureVeditaImpl.setLibDat3(new Date(libDat3));
		}

		righeFattureVeditaImpl.setTestFatturazioneAuto(testFatturazioneAuto);

		righeFattureVeditaImpl.resetOriginalValues();

		return righeFattureVeditaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		anno = objectInput.readInt();
		codiceAttivita = objectInput.readUTF();
		codiceCentro = objectInput.readUTF();
		numeroProtocollo = objectInput.readInt();
		numeroRigo = objectInput.readInt();
		statoRigo = objectInput.readBoolean();
		annoBolla = objectInput.readInt();
		tipoIdentificativoBolla = objectInput.readInt();
		tipoBolla = objectInput.readUTF();
		codiceDeposito = objectInput.readUTF();
		numeroBollettario = objectInput.readInt();
		numeroBolla = objectInput.readInt();
		dataBollaEvasa = objectInput.readLong();
		rigoDDT = objectInput.readInt();
		tipoRigoDDT = objectInput.readInt();
		codiceCausaleMagazzino = objectInput.readUTF();
		codiceDepositoMagazzino = objectInput.readUTF();
		codiceArticolo = objectInput.readUTF();
		codiceVariante = objectInput.readUTF();
		codiceTestoDescrizioni = objectInput.readUTF();
		descrizione = objectInput.readUTF();
		unitaMisura = objectInput.readUTF();
		numeroDecimalliQuant = objectInput.readInt();
		quantita1 = objectInput.readDouble();
		quantita2 = objectInput.readDouble();
		quantita3 = objectInput.readDouble();
		quantita = objectInput.readDouble();
		unitaMisura2 = objectInput.readUTF();
		quantitaUnitMisSec = objectInput.readDouble();
		numeroDecimalliPrezzo = objectInput.readInt();
		prezzo = objectInput.readDouble();
		prezzoIVA = objectInput.readDouble();
		importoLordo = objectInput.readDouble();
		importoLordoIVA = objectInput.readDouble();
		sconto1 = objectInput.readDouble();
		sconto2 = objectInput.readDouble();
		sconto3 = objectInput.readDouble();
		importoNettoRigo = objectInput.readDouble();
		importoNettoRigoIVA = objectInput.readDouble();
		importoNetto = objectInput.readDouble();
		importoNettoIVA = objectInput.readDouble();
		importoCONAIAddebitatoInt = objectInput.readDouble();
		importoCONAIAddebitatoOrig = objectInput.readDouble();
		codiceIVAFatturazione = objectInput.readUTF();
		nomenclaturaCombinata = objectInput.readInt();
		testStampaDisBase = objectInput.readBoolean();
		tipoOrdineEvaso = objectInput.readInt();
		annoOrdineEvaso = objectInput.readInt();
		numeroOrdineEvaso = objectInput.readInt();
		numeroRigoOrdineEvaso = objectInput.readInt();
		tipoEvasione = objectInput.readUTF();
		descrizioneRiferimento = objectInput.readUTF();
		numeroPrimaNotaMagazzino = objectInput.readInt();
		numeroMovimentoMagazzino = objectInput.readInt();
		tipoEsplosione = objectInput.readInt();
		livelloMassimoEsplosione = objectInput.readInt();
		libStr1 = objectInput.readUTF();
		libStr2 = objectInput.readUTF();
		libStr3 = objectInput.readUTF();
		libDbl1 = objectInput.readDouble();
		libDbl2 = objectInput.readDouble();
		libDbl3 = objectInput.readDouble();
		libLng1 = objectInput.readLong();
		libLng2 = objectInput.readLong();
		libLng3 = objectInput.readLong();
		libDat1 = objectInput.readLong();
		libDat2 = objectInput.readLong();
		libDat3 = objectInput.readLong();
		testFatturazioneAuto = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(anno);

		if (codiceAttivita == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceAttivita);
		}

		if (codiceCentro == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCentro);
		}

		objectOutput.writeInt(numeroProtocollo);
		objectOutput.writeInt(numeroRigo);
		objectOutput.writeBoolean(statoRigo);
		objectOutput.writeInt(annoBolla);
		objectOutput.writeInt(tipoIdentificativoBolla);

		if (tipoBolla == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoBolla);
		}

		if (codiceDeposito == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDeposito);
		}

		objectOutput.writeInt(numeroBollettario);
		objectOutput.writeInt(numeroBolla);
		objectOutput.writeLong(dataBollaEvasa);
		objectOutput.writeInt(rigoDDT);
		objectOutput.writeInt(tipoRigoDDT);

		if (codiceCausaleMagazzino == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCausaleMagazzino);
		}

		if (codiceDepositoMagazzino == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDepositoMagazzino);
		}

		if (codiceArticolo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceArticolo);
		}

		if (codiceVariante == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceVariante);
		}

		if (codiceTestoDescrizioni == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceTestoDescrizioni);
		}

		if (descrizione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizione);
		}

		if (unitaMisura == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(unitaMisura);
		}

		objectOutput.writeInt(numeroDecimalliQuant);
		objectOutput.writeDouble(quantita1);
		objectOutput.writeDouble(quantita2);
		objectOutput.writeDouble(quantita3);
		objectOutput.writeDouble(quantita);

		if (unitaMisura2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(unitaMisura2);
		}

		objectOutput.writeDouble(quantitaUnitMisSec);
		objectOutput.writeInt(numeroDecimalliPrezzo);
		objectOutput.writeDouble(prezzo);
		objectOutput.writeDouble(prezzoIVA);
		objectOutput.writeDouble(importoLordo);
		objectOutput.writeDouble(importoLordoIVA);
		objectOutput.writeDouble(sconto1);
		objectOutput.writeDouble(sconto2);
		objectOutput.writeDouble(sconto3);
		objectOutput.writeDouble(importoNettoRigo);
		objectOutput.writeDouble(importoNettoRigoIVA);
		objectOutput.writeDouble(importoNetto);
		objectOutput.writeDouble(importoNettoIVA);
		objectOutput.writeDouble(importoCONAIAddebitatoInt);
		objectOutput.writeDouble(importoCONAIAddebitatoOrig);

		if (codiceIVAFatturazione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVAFatturazione);
		}

		objectOutput.writeInt(nomenclaturaCombinata);
		objectOutput.writeBoolean(testStampaDisBase);
		objectOutput.writeInt(tipoOrdineEvaso);
		objectOutput.writeInt(annoOrdineEvaso);
		objectOutput.writeInt(numeroOrdineEvaso);
		objectOutput.writeInt(numeroRigoOrdineEvaso);

		if (tipoEvasione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoEvasione);
		}

		if (descrizioneRiferimento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizioneRiferimento);
		}

		objectOutput.writeInt(numeroPrimaNotaMagazzino);
		objectOutput.writeInt(numeroMovimentoMagazzino);
		objectOutput.writeInt(tipoEsplosione);
		objectOutput.writeInt(livelloMassimoEsplosione);

		if (libStr1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr1);
		}

		if (libStr2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr2);
		}

		if (libStr3 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr3);
		}

		objectOutput.writeDouble(libDbl1);
		objectOutput.writeDouble(libDbl2);
		objectOutput.writeDouble(libDbl3);
		objectOutput.writeLong(libLng1);
		objectOutput.writeLong(libLng2);
		objectOutput.writeLong(libLng3);
		objectOutput.writeLong(libDat1);
		objectOutput.writeLong(libDat2);
		objectOutput.writeLong(libDat3);
		objectOutput.writeBoolean(testFatturazioneAuto);
	}

	public int anno;
	public String codiceAttivita;
	public String codiceCentro;
	public int numeroProtocollo;
	public int numeroRigo;
	public boolean statoRigo;
	public int annoBolla;
	public int tipoIdentificativoBolla;
	public String tipoBolla;
	public String codiceDeposito;
	public int numeroBollettario;
	public int numeroBolla;
	public long dataBollaEvasa;
	public int rigoDDT;
	public int tipoRigoDDT;
	public String codiceCausaleMagazzino;
	public String codiceDepositoMagazzino;
	public String codiceArticolo;
	public String codiceVariante;
	public String codiceTestoDescrizioni;
	public String descrizione;
	public String unitaMisura;
	public int numeroDecimalliQuant;
	public double quantita1;
	public double quantita2;
	public double quantita3;
	public double quantita;
	public String unitaMisura2;
	public double quantitaUnitMisSec;
	public int numeroDecimalliPrezzo;
	public double prezzo;
	public double prezzoIVA;
	public double importoLordo;
	public double importoLordoIVA;
	public double sconto1;
	public double sconto2;
	public double sconto3;
	public double importoNettoRigo;
	public double importoNettoRigoIVA;
	public double importoNetto;
	public double importoNettoIVA;
	public double importoCONAIAddebitatoInt;
	public double importoCONAIAddebitatoOrig;
	public String codiceIVAFatturazione;
	public int nomenclaturaCombinata;
	public boolean testStampaDisBase;
	public int tipoOrdineEvaso;
	public int annoOrdineEvaso;
	public int numeroOrdineEvaso;
	public int numeroRigoOrdineEvaso;
	public String tipoEvasione;
	public String descrizioneRiferimento;
	public int numeroPrimaNotaMagazzino;
	public int numeroMovimentoMagazzino;
	public int tipoEsplosione;
	public int livelloMassimoEsplosione;
	public String libStr1;
	public String libStr2;
	public String libStr3;
	public double libDbl1;
	public double libDbl2;
	public double libDbl3;
	public long libLng1;
	public long libLng2;
	public long libLng3;
	public long libDat1;
	public long libDat2;
	public long libDat3;
	public boolean testFatturazioneAuto;
}