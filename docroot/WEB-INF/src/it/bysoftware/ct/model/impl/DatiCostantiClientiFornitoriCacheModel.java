/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.DatiCostantiClientiFornitori;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing DatiCostantiClientiFornitori in entity cache.
 *
 * @author Mario Torrisi
 * @see DatiCostantiClientiFornitori
 * @generated
 */
public class DatiCostantiClientiFornitoriCacheModel implements CacheModel<DatiCostantiClientiFornitori>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{tipoSoggetto=");
		sb.append(tipoSoggetto);
		sb.append(", codiceSoggetto=");
		sb.append(codiceSoggetto);
		sb.append(", codiceSottoconto=");
		sb.append(codiceSottoconto);
		sb.append(", massimoScoperto=");
		sb.append(massimoScoperto);
		sb.append(", gestioneEstrattoConto=");
		sb.append(gestioneEstrattoConto);
		sb.append(", codicePagamento=");
		sb.append(codicePagamento);
		sb.append(", codiceBancaPagFor=");
		sb.append(codiceBancaPagFor);
		sb.append(", codiceAgenziaPagFor=");
		sb.append(codiceAgenziaPagFor);
		sb.append(", codiceContropartita=");
		sb.append(codiceContropartita);
		sb.append(", calcoloIntMora=");
		sb.append(calcoloIntMora);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public DatiCostantiClientiFornitori toEntityModel() {
		DatiCostantiClientiFornitoriImpl datiCostantiClientiFornitoriImpl = new DatiCostantiClientiFornitoriImpl();

		datiCostantiClientiFornitoriImpl.setTipoSoggetto(tipoSoggetto);

		if (codiceSoggetto == null) {
			datiCostantiClientiFornitoriImpl.setCodiceSoggetto(StringPool.BLANK);
		}
		else {
			datiCostantiClientiFornitoriImpl.setCodiceSoggetto(codiceSoggetto);
		}

		if (codiceSottoconto == null) {
			datiCostantiClientiFornitoriImpl.setCodiceSottoconto(StringPool.BLANK);
		}
		else {
			datiCostantiClientiFornitoriImpl.setCodiceSottoconto(codiceSottoconto);
		}

		datiCostantiClientiFornitoriImpl.setMassimoScoperto(massimoScoperto);
		datiCostantiClientiFornitoriImpl.setGestioneEstrattoConto(gestioneEstrattoConto);

		if (codicePagamento == null) {
			datiCostantiClientiFornitoriImpl.setCodicePagamento(StringPool.BLANK);
		}
		else {
			datiCostantiClientiFornitoriImpl.setCodicePagamento(codicePagamento);
		}

		if (codiceBancaPagFor == null) {
			datiCostantiClientiFornitoriImpl.setCodiceBancaPagFor(StringPool.BLANK);
		}
		else {
			datiCostantiClientiFornitoriImpl.setCodiceBancaPagFor(codiceBancaPagFor);
		}

		if (codiceAgenziaPagFor == null) {
			datiCostantiClientiFornitoriImpl.setCodiceAgenziaPagFor(StringPool.BLANK);
		}
		else {
			datiCostantiClientiFornitoriImpl.setCodiceAgenziaPagFor(codiceAgenziaPagFor);
		}

		if (codiceContropartita == null) {
			datiCostantiClientiFornitoriImpl.setCodiceContropartita(StringPool.BLANK);
		}
		else {
			datiCostantiClientiFornitoriImpl.setCodiceContropartita(codiceContropartita);
		}

		datiCostantiClientiFornitoriImpl.setCalcoloIntMora(calcoloIntMora);

		datiCostantiClientiFornitoriImpl.resetOriginalValues();

		return datiCostantiClientiFornitoriImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		tipoSoggetto = objectInput.readBoolean();
		codiceSoggetto = objectInput.readUTF();
		codiceSottoconto = objectInput.readUTF();
		massimoScoperto = objectInput.readDouble();
		gestioneEstrattoConto = objectInput.readBoolean();
		codicePagamento = objectInput.readUTF();
		codiceBancaPagFor = objectInput.readUTF();
		codiceAgenziaPagFor = objectInput.readUTF();
		codiceContropartita = objectInput.readUTF();
		calcoloIntMora = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeBoolean(tipoSoggetto);

		if (codiceSoggetto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceSoggetto);
		}

		if (codiceSottoconto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceSottoconto);
		}

		objectOutput.writeDouble(massimoScoperto);
		objectOutput.writeBoolean(gestioneEstrattoConto);

		if (codicePagamento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codicePagamento);
		}

		if (codiceBancaPagFor == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceBancaPagFor);
		}

		if (codiceAgenziaPagFor == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceAgenziaPagFor);
		}

		if (codiceContropartita == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceContropartita);
		}

		objectOutput.writeBoolean(calcoloIntMora);
	}

	public boolean tipoSoggetto;
	public String codiceSoggetto;
	public String codiceSottoconto;
	public double massimoScoperto;
	public boolean gestioneEstrattoConto;
	public String codicePagamento;
	public String codiceBancaPagFor;
	public String codiceAgenziaPagFor;
	public String codiceContropartita;
	public boolean calcoloIntMora;
}