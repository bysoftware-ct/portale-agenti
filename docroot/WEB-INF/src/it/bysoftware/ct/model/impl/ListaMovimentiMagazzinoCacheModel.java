/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.ListaMovimentiMagazzino;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ListaMovimentiMagazzino in entity cache.
 *
 * @author Mario Torrisi
 * @see ListaMovimentiMagazzino
 * @generated
 */
public class ListaMovimentiMagazzinoCacheModel implements CacheModel<ListaMovimentiMagazzino>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{ID=");
		sb.append(ID);
		sb.append(", codiceArticolo=");
		sb.append(codiceArticolo);
		sb.append(", codiceVariante=");
		sb.append(codiceVariante);
		sb.append(", descrizione=");
		sb.append(descrizione);
		sb.append(", quantita=");
		sb.append(quantita);
		sb.append(", testCaricoScarico=");
		sb.append(testCaricoScarico);
		sb.append(", soloValore=");
		sb.append(soloValore);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ListaMovimentiMagazzino toEntityModel() {
		ListaMovimentiMagazzinoImpl listaMovimentiMagazzinoImpl = new ListaMovimentiMagazzinoImpl();

		if (ID == null) {
			listaMovimentiMagazzinoImpl.setID(StringPool.BLANK);
		}
		else {
			listaMovimentiMagazzinoImpl.setID(ID);
		}

		if (codiceArticolo == null) {
			listaMovimentiMagazzinoImpl.setCodiceArticolo(StringPool.BLANK);
		}
		else {
			listaMovimentiMagazzinoImpl.setCodiceArticolo(codiceArticolo);
		}

		if (codiceVariante == null) {
			listaMovimentiMagazzinoImpl.setCodiceVariante(StringPool.BLANK);
		}
		else {
			listaMovimentiMagazzinoImpl.setCodiceVariante(codiceVariante);
		}

		if (descrizione == null) {
			listaMovimentiMagazzinoImpl.setDescrizione(StringPool.BLANK);
		}
		else {
			listaMovimentiMagazzinoImpl.setDescrizione(descrizione);
		}

		if (quantita == null) {
			listaMovimentiMagazzinoImpl.setQuantita(StringPool.BLANK);
		}
		else {
			listaMovimentiMagazzinoImpl.setQuantita(quantita);
		}

		listaMovimentiMagazzinoImpl.setTestCaricoScarico(testCaricoScarico);
		listaMovimentiMagazzinoImpl.setSoloValore(soloValore);

		listaMovimentiMagazzinoImpl.resetOriginalValues();

		return listaMovimentiMagazzinoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ID = objectInput.readUTF();
		codiceArticolo = objectInput.readUTF();
		codiceVariante = objectInput.readUTF();
		descrizione = objectInput.readUTF();
		quantita = objectInput.readUTF();
		testCaricoScarico = objectInput.readInt();
		soloValore = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (ID == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ID);
		}

		if (codiceArticolo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceArticolo);
		}

		if (codiceVariante == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceVariante);
		}

		if (descrizione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizione);
		}

		if (quantita == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(quantita);
		}

		objectOutput.writeInt(testCaricoScarico);
		objectOutput.writeInt(soloValore);
	}

	public String ID;
	public String codiceArticolo;
	public String codiceVariante;
	public String descrizione;
	public String quantita;
	public int testCaricoScarico;
	public int soloValore;
}