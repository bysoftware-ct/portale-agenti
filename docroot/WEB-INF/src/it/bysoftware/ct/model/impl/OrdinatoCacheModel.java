/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Ordinato;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Ordinato in entity cache.
 *
 * @author Mario Torrisi
 * @see Ordinato
 * @generated
 */
public class OrdinatoCacheModel implements CacheModel<Ordinato>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{codiceDesposito=");
		sb.append(codiceDesposito);
		sb.append(", codiceArticolo=");
		sb.append(codiceArticolo);
		sb.append(", codiceVariante=");
		sb.append(codiceVariante);
		sb.append(", quantitaOrdinataFornitore=");
		sb.append(quantitaOrdinataFornitore);
		sb.append(", quantitaOrdinataProduzione=");
		sb.append(quantitaOrdinataProduzione);
		sb.append(", quantitaOrdinataAltri=");
		sb.append(quantitaOrdinataAltri);
		sb.append(", quantitaImpegnataClienti=");
		sb.append(quantitaImpegnataClienti);
		sb.append(", quantitaImpegnataProduzione=");
		sb.append(quantitaImpegnataProduzione);
		sb.append(", quantitaImpegnataAltri=");
		sb.append(quantitaImpegnataAltri);
		sb.append(", quantitaOrdinataFornitoreUnMis2=");
		sb.append(quantitaOrdinataFornitoreUnMis2);
		sb.append(", quantitaOrdinataProduzioneUnMis2=");
		sb.append(quantitaOrdinataProduzioneUnMis2);
		sb.append(", quantitaOrdinataAltriUnMis2=");
		sb.append(quantitaOrdinataAltriUnMis2);
		sb.append(", quantitaImpegnataClientiUnMis2=");
		sb.append(quantitaImpegnataClientiUnMis2);
		sb.append(", quantitaImpegnataProduzioneUnMis2=");
		sb.append(quantitaImpegnataProduzioneUnMis2);
		sb.append(", quantitaImpegnataAltriUnMis2=");
		sb.append(quantitaImpegnataAltriUnMis2);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Ordinato toEntityModel() {
		OrdinatoImpl ordinatoImpl = new OrdinatoImpl();

		if (codiceDesposito == null) {
			ordinatoImpl.setCodiceDesposito(StringPool.BLANK);
		}
		else {
			ordinatoImpl.setCodiceDesposito(codiceDesposito);
		}

		if (codiceArticolo == null) {
			ordinatoImpl.setCodiceArticolo(StringPool.BLANK);
		}
		else {
			ordinatoImpl.setCodiceArticolo(codiceArticolo);
		}

		if (codiceVariante == null) {
			ordinatoImpl.setCodiceVariante(StringPool.BLANK);
		}
		else {
			ordinatoImpl.setCodiceVariante(codiceVariante);
		}

		ordinatoImpl.setQuantitaOrdinataFornitore(quantitaOrdinataFornitore);
		ordinatoImpl.setQuantitaOrdinataProduzione(quantitaOrdinataProduzione);
		ordinatoImpl.setQuantitaOrdinataAltri(quantitaOrdinataAltri);
		ordinatoImpl.setQuantitaImpegnataClienti(quantitaImpegnataClienti);
		ordinatoImpl.setQuantitaImpegnataProduzione(quantitaImpegnataProduzione);
		ordinatoImpl.setQuantitaImpegnataAltri(quantitaImpegnataAltri);
		ordinatoImpl.setQuantitaOrdinataFornitoreUnMis2(quantitaOrdinataFornitoreUnMis2);
		ordinatoImpl.setQuantitaOrdinataProduzioneUnMis2(quantitaOrdinataProduzioneUnMis2);
		ordinatoImpl.setQuantitaOrdinataAltriUnMis2(quantitaOrdinataAltriUnMis2);
		ordinatoImpl.setQuantitaImpegnataClientiUnMis2(quantitaImpegnataClientiUnMis2);
		ordinatoImpl.setQuantitaImpegnataProduzioneUnMis2(quantitaImpegnataProduzioneUnMis2);
		ordinatoImpl.setQuantitaImpegnataAltriUnMis2(quantitaImpegnataAltriUnMis2);

		ordinatoImpl.resetOriginalValues();

		return ordinatoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		codiceDesposito = objectInput.readUTF();
		codiceArticolo = objectInput.readUTF();
		codiceVariante = objectInput.readUTF();
		quantitaOrdinataFornitore = objectInput.readDouble();
		quantitaOrdinataProduzione = objectInput.readDouble();
		quantitaOrdinataAltri = objectInput.readDouble();
		quantitaImpegnataClienti = objectInput.readDouble();
		quantitaImpegnataProduzione = objectInput.readDouble();
		quantitaImpegnataAltri = objectInput.readDouble();
		quantitaOrdinataFornitoreUnMis2 = objectInput.readDouble();
		quantitaOrdinataProduzioneUnMis2 = objectInput.readDouble();
		quantitaOrdinataAltriUnMis2 = objectInput.readDouble();
		quantitaImpegnataClientiUnMis2 = objectInput.readDouble();
		quantitaImpegnataProduzioneUnMis2 = objectInput.readDouble();
		quantitaImpegnataAltriUnMis2 = objectInput.readDouble();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (codiceDesposito == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDesposito);
		}

		if (codiceArticolo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceArticolo);
		}

		if (codiceVariante == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceVariante);
		}

		objectOutput.writeDouble(quantitaOrdinataFornitore);
		objectOutput.writeDouble(quantitaOrdinataProduzione);
		objectOutput.writeDouble(quantitaOrdinataAltri);
		objectOutput.writeDouble(quantitaImpegnataClienti);
		objectOutput.writeDouble(quantitaImpegnataProduzione);
		objectOutput.writeDouble(quantitaImpegnataAltri);
		objectOutput.writeDouble(quantitaOrdinataFornitoreUnMis2);
		objectOutput.writeDouble(quantitaOrdinataProduzioneUnMis2);
		objectOutput.writeDouble(quantitaOrdinataAltriUnMis2);
		objectOutput.writeDouble(quantitaImpegnataClientiUnMis2);
		objectOutput.writeDouble(quantitaImpegnataProduzioneUnMis2);
		objectOutput.writeDouble(quantitaImpegnataAltriUnMis2);
	}

	public String codiceDesposito;
	public String codiceArticolo;
	public String codiceVariante;
	public double quantitaOrdinataFornitore;
	public double quantitaOrdinataProduzione;
	public double quantitaOrdinataAltri;
	public double quantitaImpegnataClienti;
	public double quantitaImpegnataProduzione;
	public double quantitaImpegnataAltri;
	public double quantitaOrdinataFornitoreUnMis2;
	public double quantitaOrdinataProduzioneUnMis2;
	public double quantitaOrdinataAltriUnMis2;
	public double quantitaImpegnataClientiUnMis2;
	public double quantitaImpegnataProduzioneUnMis2;
	public double quantitaImpegnataAltriUnMis2;
}