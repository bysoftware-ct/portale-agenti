/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.Articoli;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Articoli in entity cache.
 *
 * @author Mario Torrisi
 * @see Articoli
 * @generated
 */
public class ArticoliCacheModel implements CacheModel<Articoli>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(177);

		sb.append("{codiceArticolo=");
		sb.append(codiceArticolo);
		sb.append(", descrizione=");
		sb.append(descrizione);
		sb.append(", descrizioneAggiuntiva=");
		sb.append(descrizioneAggiuntiva);
		sb.append(", descrizioneBreveFiscale=");
		sb.append(descrizioneBreveFiscale);
		sb.append(", pathImmagine=");
		sb.append(pathImmagine);
		sb.append(", unitaMisura=");
		sb.append(unitaMisura);
		sb.append(", categoriaMerceologica=");
		sb.append(categoriaMerceologica);
		sb.append(", categoriaFiscale=");
		sb.append(categoriaFiscale);
		sb.append(", codiceIVA=");
		sb.append(codiceIVA);
		sb.append(", codiceProvvigioni=");
		sb.append(codiceProvvigioni);
		sb.append(", codiceSconto1=");
		sb.append(codiceSconto1);
		sb.append(", codiceSconto2=");
		sb.append(codiceSconto2);
		sb.append(", categoriaInventario=");
		sb.append(categoriaInventario);
		sb.append(", prezzo1=");
		sb.append(prezzo1);
		sb.append(", prezzo2=");
		sb.append(prezzo2);
		sb.append(", prezzo3=");
		sb.append(prezzo3);
		sb.append(", codiceIVAPrezzo1=");
		sb.append(codiceIVAPrezzo1);
		sb.append(", codiceIVAPrezzo2=");
		sb.append(codiceIVAPrezzo2);
		sb.append(", codiceIVAPrezzo3=");
		sb.append(codiceIVAPrezzo3);
		sb.append(", numeroDecimaliPrezzo=");
		sb.append(numeroDecimaliPrezzo);
		sb.append(", numeroDecimaliQuantita=");
		sb.append(numeroDecimaliQuantita);
		sb.append(", numeroMesiPerinvenduto=");
		sb.append(numeroMesiPerinvenduto);
		sb.append(", unitaMisura2=");
		sb.append(unitaMisura2);
		sb.append(", fattoreMoltiplicativo=");
		sb.append(fattoreMoltiplicativo);
		sb.append(", fattoreDivisione=");
		sb.append(fattoreDivisione);
		sb.append(", ragruppamentoLibero=");
		sb.append(ragruppamentoLibero);
		sb.append(", tipoLivelloDistintaBase=");
		sb.append(tipoLivelloDistintaBase);
		sb.append(", tipoCostoStatistico=");
		sb.append(tipoCostoStatistico);
		sb.append(", tipoGestioneArticolo=");
		sb.append(tipoGestioneArticolo);
		sb.append(", tipoApprovvigionamentoArticolo=");
		sb.append(tipoApprovvigionamentoArticolo);
		sb.append(", nomenclaturaCombinata=");
		sb.append(nomenclaturaCombinata);
		sb.append(", descrizioneEstesa=");
		sb.append(descrizioneEstesa);
		sb.append(", generazioneMovimenti=");
		sb.append(generazioneMovimenti);
		sb.append(", tipoEsplosioneDistintaBase=");
		sb.append(tipoEsplosioneDistintaBase);
		sb.append(", livelloMaxEsplosioneDistintaBase=");
		sb.append(livelloMaxEsplosioneDistintaBase);
		sb.append(", valorizzazioneMagazzino=");
		sb.append(valorizzazioneMagazzino);
		sb.append(", abilitatoEC=");
		sb.append(abilitatoEC);
		sb.append(", categoriaEC=");
		sb.append(categoriaEC);
		sb.append(", quantitaMinimaEC=");
		sb.append(quantitaMinimaEC);
		sb.append(", quantitaDefaultEC=");
		sb.append(quantitaDefaultEC);
		sb.append(", codiceSchedaTecnicaEC=");
		sb.append(codiceSchedaTecnicaEC);
		sb.append(", giorniPrevistaConsegna=");
		sb.append(giorniPrevistaConsegna);
		sb.append(", gestioneLotti=");
		sb.append(gestioneLotti);
		sb.append(", creazioneLotti=");
		sb.append(creazioneLotti);
		sb.append(", prefissoCodiceLottoAutomatico=");
		sb.append(prefissoCodiceLottoAutomatico);
		sb.append(", numeroCifreProgressivo=");
		sb.append(numeroCifreProgressivo);
		sb.append(", scaricoAutomaticoLotti=");
		sb.append(scaricoAutomaticoLotti);
		sb.append(", giorniInizioValiditaLotto=");
		sb.append(giorniInizioValiditaLotto);
		sb.append(", giorniValiditaLotto=");
		sb.append(giorniValiditaLotto);
		sb.append(", giorniPreavvisoScadenzaLotto=");
		sb.append(giorniPreavvisoScadenzaLotto);
		sb.append(", fattoreMoltQuantRiservata=");
		sb.append(fattoreMoltQuantRiservata);
		sb.append(", fattoreDivQuantRiservata=");
		sb.append(fattoreDivQuantRiservata);
		sb.append(", obsoleto=");
		sb.append(obsoleto);
		sb.append(", comeImballo=");
		sb.append(comeImballo);
		sb.append(", fattoreMoltCalcoloImballo=");
		sb.append(fattoreMoltCalcoloImballo);
		sb.append(", fattoreDivCalcoloImballo=");
		sb.append(fattoreDivCalcoloImballo);
		sb.append(", pesoLordo=");
		sb.append(pesoLordo);
		sb.append(", tara=");
		sb.append(tara);
		sb.append(", calcoloVolume=");
		sb.append(calcoloVolume);
		sb.append(", lunghezza=");
		sb.append(lunghezza);
		sb.append(", larghezza=");
		sb.append(larghezza);
		sb.append(", profondita=");
		sb.append(profondita);
		sb.append(", codiceImballo=");
		sb.append(codiceImballo);
		sb.append(", fattoreMoltUnitMisSupp=");
		sb.append(fattoreMoltUnitMisSupp);
		sb.append(", fattoreDivUnitMisSupp=");
		sb.append(fattoreDivUnitMisSupp);
		sb.append(", stampaetichette=");
		sb.append(stampaetichette);
		sb.append(", gestioneVarianti=");
		sb.append(gestioneVarianti);
		sb.append(", gestioneVariantiDistintaBase=");
		sb.append(gestioneVariantiDistintaBase);
		sb.append(", liberoString1=");
		sb.append(liberoString1);
		sb.append(", liberoString2=");
		sb.append(liberoString2);
		sb.append(", liberoString3=");
		sb.append(liberoString3);
		sb.append(", liberoString4=");
		sb.append(liberoString4);
		sb.append(", liberoString5=");
		sb.append(liberoString5);
		sb.append(", liberoDate1=");
		sb.append(liberoDate1);
		sb.append(", liberoDate2=");
		sb.append(liberoDate2);
		sb.append(", liberoDate3=");
		sb.append(liberoDate3);
		sb.append(", liberoDate4=");
		sb.append(liberoDate4);
		sb.append(", liberoDate5=");
		sb.append(liberoDate5);
		sb.append(", liberoLong1=");
		sb.append(liberoLong1);
		sb.append(", liberoLong2=");
		sb.append(liberoLong2);
		sb.append(", liberoLong3=");
		sb.append(liberoLong3);
		sb.append(", liberoLong4=");
		sb.append(liberoLong4);
		sb.append(", liberoLong5=");
		sb.append(liberoLong5);
		sb.append(", liberoDouble1=");
		sb.append(liberoDouble1);
		sb.append(", liberoDouble2=");
		sb.append(liberoDouble2);
		sb.append(", liberoDouble3=");
		sb.append(liberoDouble3);
		sb.append(", liberoDouble4=");
		sb.append(liberoDouble4);
		sb.append(", liberoDouble5=");
		sb.append(liberoDouble5);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Articoli toEntityModel() {
		ArticoliImpl articoliImpl = new ArticoliImpl();

		if (codiceArticolo == null) {
			articoliImpl.setCodiceArticolo(StringPool.BLANK);
		}
		else {
			articoliImpl.setCodiceArticolo(codiceArticolo);
		}

		if (descrizione == null) {
			articoliImpl.setDescrizione(StringPool.BLANK);
		}
		else {
			articoliImpl.setDescrizione(descrizione);
		}

		if (descrizioneAggiuntiva == null) {
			articoliImpl.setDescrizioneAggiuntiva(StringPool.BLANK);
		}
		else {
			articoliImpl.setDescrizioneAggiuntiva(descrizioneAggiuntiva);
		}

		if (descrizioneBreveFiscale == null) {
			articoliImpl.setDescrizioneBreveFiscale(StringPool.BLANK);
		}
		else {
			articoliImpl.setDescrizioneBreveFiscale(descrizioneBreveFiscale);
		}

		if (pathImmagine == null) {
			articoliImpl.setPathImmagine(StringPool.BLANK);
		}
		else {
			articoliImpl.setPathImmagine(pathImmagine);
		}

		if (unitaMisura == null) {
			articoliImpl.setUnitaMisura(StringPool.BLANK);
		}
		else {
			articoliImpl.setUnitaMisura(unitaMisura);
		}

		if (categoriaMerceologica == null) {
			articoliImpl.setCategoriaMerceologica(StringPool.BLANK);
		}
		else {
			articoliImpl.setCategoriaMerceologica(categoriaMerceologica);
		}

		if (categoriaFiscale == null) {
			articoliImpl.setCategoriaFiscale(StringPool.BLANK);
		}
		else {
			articoliImpl.setCategoriaFiscale(categoriaFiscale);
		}

		if (codiceIVA == null) {
			articoliImpl.setCodiceIVA(StringPool.BLANK);
		}
		else {
			articoliImpl.setCodiceIVA(codiceIVA);
		}

		if (codiceProvvigioni == null) {
			articoliImpl.setCodiceProvvigioni(StringPool.BLANK);
		}
		else {
			articoliImpl.setCodiceProvvigioni(codiceProvvigioni);
		}

		if (codiceSconto1 == null) {
			articoliImpl.setCodiceSconto1(StringPool.BLANK);
		}
		else {
			articoliImpl.setCodiceSconto1(codiceSconto1);
		}

		if (codiceSconto2 == null) {
			articoliImpl.setCodiceSconto2(StringPool.BLANK);
		}
		else {
			articoliImpl.setCodiceSconto2(codiceSconto2);
		}

		if (categoriaInventario == null) {
			articoliImpl.setCategoriaInventario(StringPool.BLANK);
		}
		else {
			articoliImpl.setCategoriaInventario(categoriaInventario);
		}

		articoliImpl.setPrezzo1(prezzo1);
		articoliImpl.setPrezzo2(prezzo2);
		articoliImpl.setPrezzo3(prezzo3);

		if (codiceIVAPrezzo1 == null) {
			articoliImpl.setCodiceIVAPrezzo1(StringPool.BLANK);
		}
		else {
			articoliImpl.setCodiceIVAPrezzo1(codiceIVAPrezzo1);
		}

		if (codiceIVAPrezzo2 == null) {
			articoliImpl.setCodiceIVAPrezzo2(StringPool.BLANK);
		}
		else {
			articoliImpl.setCodiceIVAPrezzo2(codiceIVAPrezzo2);
		}

		if (codiceIVAPrezzo3 == null) {
			articoliImpl.setCodiceIVAPrezzo3(StringPool.BLANK);
		}
		else {
			articoliImpl.setCodiceIVAPrezzo3(codiceIVAPrezzo3);
		}

		articoliImpl.setNumeroDecimaliPrezzo(numeroDecimaliPrezzo);
		articoliImpl.setNumeroDecimaliQuantita(numeroDecimaliQuantita);
		articoliImpl.setNumeroMesiPerinvenduto(numeroMesiPerinvenduto);

		if (unitaMisura2 == null) {
			articoliImpl.setUnitaMisura2(StringPool.BLANK);
		}
		else {
			articoliImpl.setUnitaMisura2(unitaMisura2);
		}

		articoliImpl.setFattoreMoltiplicativo(fattoreMoltiplicativo);
		articoliImpl.setFattoreDivisione(fattoreDivisione);

		if (ragruppamentoLibero == null) {
			articoliImpl.setRagruppamentoLibero(StringPool.BLANK);
		}
		else {
			articoliImpl.setRagruppamentoLibero(ragruppamentoLibero);
		}

		articoliImpl.setTipoLivelloDistintaBase(tipoLivelloDistintaBase);
		articoliImpl.setTipoCostoStatistico(tipoCostoStatistico);
		articoliImpl.setTipoGestioneArticolo(tipoGestioneArticolo);
		articoliImpl.setTipoApprovvigionamentoArticolo(tipoApprovvigionamentoArticolo);
		articoliImpl.setNomenclaturaCombinata(nomenclaturaCombinata);

		if (descrizioneEstesa == null) {
			articoliImpl.setDescrizioneEstesa(StringPool.BLANK);
		}
		else {
			articoliImpl.setDescrizioneEstesa(descrizioneEstesa);
		}

		articoliImpl.setGenerazioneMovimenti(generazioneMovimenti);
		articoliImpl.setTipoEsplosioneDistintaBase(tipoEsplosioneDistintaBase);
		articoliImpl.setLivelloMaxEsplosioneDistintaBase(livelloMaxEsplosioneDistintaBase);
		articoliImpl.setValorizzazioneMagazzino(valorizzazioneMagazzino);
		articoliImpl.setAbilitatoEC(abilitatoEC);

		if (categoriaEC == null) {
			articoliImpl.setCategoriaEC(StringPool.BLANK);
		}
		else {
			articoliImpl.setCategoriaEC(categoriaEC);
		}

		articoliImpl.setQuantitaMinimaEC(quantitaMinimaEC);
		articoliImpl.setQuantitaDefaultEC(quantitaDefaultEC);

		if (codiceSchedaTecnicaEC == null) {
			articoliImpl.setCodiceSchedaTecnicaEC(StringPool.BLANK);
		}
		else {
			articoliImpl.setCodiceSchedaTecnicaEC(codiceSchedaTecnicaEC);
		}

		articoliImpl.setGiorniPrevistaConsegna(giorniPrevistaConsegna);
		articoliImpl.setGestioneLotti(gestioneLotti);
		articoliImpl.setCreazioneLotti(creazioneLotti);

		if (prefissoCodiceLottoAutomatico == null) {
			articoliImpl.setPrefissoCodiceLottoAutomatico(StringPool.BLANK);
		}
		else {
			articoliImpl.setPrefissoCodiceLottoAutomatico(prefissoCodiceLottoAutomatico);
		}

		articoliImpl.setNumeroCifreProgressivo(numeroCifreProgressivo);
		articoliImpl.setScaricoAutomaticoLotti(scaricoAutomaticoLotti);
		articoliImpl.setGiorniInizioValiditaLotto(giorniInizioValiditaLotto);
		articoliImpl.setGiorniValiditaLotto(giorniValiditaLotto);
		articoliImpl.setGiorniPreavvisoScadenzaLotto(giorniPreavvisoScadenzaLotto);
		articoliImpl.setFattoreMoltQuantRiservata(fattoreMoltQuantRiservata);
		articoliImpl.setFattoreDivQuantRiservata(fattoreDivQuantRiservata);
		articoliImpl.setObsoleto(obsoleto);
		articoliImpl.setComeImballo(comeImballo);
		articoliImpl.setFattoreMoltCalcoloImballo(fattoreMoltCalcoloImballo);
		articoliImpl.setFattoreDivCalcoloImballo(fattoreDivCalcoloImballo);
		articoliImpl.setPesoLordo(pesoLordo);
		articoliImpl.setTara(tara);
		articoliImpl.setCalcoloVolume(calcoloVolume);
		articoliImpl.setLunghezza(lunghezza);
		articoliImpl.setLarghezza(larghezza);
		articoliImpl.setProfondita(profondita);

		if (codiceImballo == null) {
			articoliImpl.setCodiceImballo(StringPool.BLANK);
		}
		else {
			articoliImpl.setCodiceImballo(codiceImballo);
		}

		articoliImpl.setFattoreMoltUnitMisSupp(fattoreMoltUnitMisSupp);
		articoliImpl.setFattoreDivUnitMisSupp(fattoreDivUnitMisSupp);
		articoliImpl.setStampaetichette(stampaetichette);
		articoliImpl.setGestioneVarianti(gestioneVarianti);
		articoliImpl.setGestioneVariantiDistintaBase(gestioneVariantiDistintaBase);

		if (liberoString1 == null) {
			articoliImpl.setLiberoString1(StringPool.BLANK);
		}
		else {
			articoliImpl.setLiberoString1(liberoString1);
		}

		if (liberoString2 == null) {
			articoliImpl.setLiberoString2(StringPool.BLANK);
		}
		else {
			articoliImpl.setLiberoString2(liberoString2);
		}

		if (liberoString3 == null) {
			articoliImpl.setLiberoString3(StringPool.BLANK);
		}
		else {
			articoliImpl.setLiberoString3(liberoString3);
		}

		if (liberoString4 == null) {
			articoliImpl.setLiberoString4(StringPool.BLANK);
		}
		else {
			articoliImpl.setLiberoString4(liberoString4);
		}

		if (liberoString5 == null) {
			articoliImpl.setLiberoString5(StringPool.BLANK);
		}
		else {
			articoliImpl.setLiberoString5(liberoString5);
		}

		if (liberoDate1 == Long.MIN_VALUE) {
			articoliImpl.setLiberoDate1(null);
		}
		else {
			articoliImpl.setLiberoDate1(new Date(liberoDate1));
		}

		if (liberoDate2 == Long.MIN_VALUE) {
			articoliImpl.setLiberoDate2(null);
		}
		else {
			articoliImpl.setLiberoDate2(new Date(liberoDate2));
		}

		if (liberoDate3 == Long.MIN_VALUE) {
			articoliImpl.setLiberoDate3(null);
		}
		else {
			articoliImpl.setLiberoDate3(new Date(liberoDate3));
		}

		if (liberoDate4 == Long.MIN_VALUE) {
			articoliImpl.setLiberoDate4(null);
		}
		else {
			articoliImpl.setLiberoDate4(new Date(liberoDate4));
		}

		if (liberoDate5 == Long.MIN_VALUE) {
			articoliImpl.setLiberoDate5(null);
		}
		else {
			articoliImpl.setLiberoDate5(new Date(liberoDate5));
		}

		articoliImpl.setLiberoLong1(liberoLong1);
		articoliImpl.setLiberoLong2(liberoLong2);
		articoliImpl.setLiberoLong3(liberoLong3);
		articoliImpl.setLiberoLong4(liberoLong4);
		articoliImpl.setLiberoLong5(liberoLong5);
		articoliImpl.setLiberoDouble1(liberoDouble1);
		articoliImpl.setLiberoDouble2(liberoDouble2);
		articoliImpl.setLiberoDouble3(liberoDouble3);
		articoliImpl.setLiberoDouble4(liberoDouble4);
		articoliImpl.setLiberoDouble5(liberoDouble5);

		articoliImpl.resetOriginalValues();

		return articoliImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		codiceArticolo = objectInput.readUTF();
		descrizione = objectInput.readUTF();
		descrizioneAggiuntiva = objectInput.readUTF();
		descrizioneBreveFiscale = objectInput.readUTF();
		pathImmagine = objectInput.readUTF();
		unitaMisura = objectInput.readUTF();
		categoriaMerceologica = objectInput.readUTF();
		categoriaFiscale = objectInput.readUTF();
		codiceIVA = objectInput.readUTF();
		codiceProvvigioni = objectInput.readUTF();
		codiceSconto1 = objectInput.readUTF();
		codiceSconto2 = objectInput.readUTF();
		categoriaInventario = objectInput.readUTF();
		prezzo1 = objectInput.readDouble();
		prezzo2 = objectInput.readDouble();
		prezzo3 = objectInput.readDouble();
		codiceIVAPrezzo1 = objectInput.readUTF();
		codiceIVAPrezzo2 = objectInput.readUTF();
		codiceIVAPrezzo3 = objectInput.readUTF();
		numeroDecimaliPrezzo = objectInput.readInt();
		numeroDecimaliQuantita = objectInput.readInt();
		numeroMesiPerinvenduto = objectInput.readInt();
		unitaMisura2 = objectInput.readUTF();
		fattoreMoltiplicativo = objectInput.readDouble();
		fattoreDivisione = objectInput.readDouble();
		ragruppamentoLibero = objectInput.readUTF();
		tipoLivelloDistintaBase = objectInput.readInt();
		tipoCostoStatistico = objectInput.readInt();
		tipoGestioneArticolo = objectInput.readInt();
		tipoApprovvigionamentoArticolo = objectInput.readInt();
		nomenclaturaCombinata = objectInput.readDouble();
		descrizioneEstesa = objectInput.readUTF();
		generazioneMovimenti = objectInput.readBoolean();
		tipoEsplosioneDistintaBase = objectInput.readInt();
		livelloMaxEsplosioneDistintaBase = objectInput.readInt();
		valorizzazioneMagazzino = objectInput.readBoolean();
		abilitatoEC = objectInput.readBoolean();
		categoriaEC = objectInput.readUTF();
		quantitaMinimaEC = objectInput.readDouble();
		quantitaDefaultEC = objectInput.readDouble();
		codiceSchedaTecnicaEC = objectInput.readUTF();
		giorniPrevistaConsegna = objectInput.readInt();
		gestioneLotti = objectInput.readBoolean();
		creazioneLotti = objectInput.readBoolean();
		prefissoCodiceLottoAutomatico = objectInput.readUTF();
		numeroCifreProgressivo = objectInput.readInt();
		scaricoAutomaticoLotti = objectInput.readBoolean();
		giorniInizioValiditaLotto = objectInput.readInt();
		giorniValiditaLotto = objectInput.readInt();
		giorniPreavvisoScadenzaLotto = objectInput.readInt();
		fattoreMoltQuantRiservata = objectInput.readDouble();
		fattoreDivQuantRiservata = objectInput.readDouble();
		obsoleto = objectInput.readBoolean();
		comeImballo = objectInput.readBoolean();
		fattoreMoltCalcoloImballo = objectInput.readDouble();
		fattoreDivCalcoloImballo = objectInput.readDouble();
		pesoLordo = objectInput.readDouble();
		tara = objectInput.readDouble();
		calcoloVolume = objectInput.readInt();
		lunghezza = objectInput.readDouble();
		larghezza = objectInput.readDouble();
		profondita = objectInput.readDouble();
		codiceImballo = objectInput.readUTF();
		fattoreMoltUnitMisSupp = objectInput.readDouble();
		fattoreDivUnitMisSupp = objectInput.readDouble();
		stampaetichette = objectInput.readInt();
		gestioneVarianti = objectInput.readBoolean();
		gestioneVariantiDistintaBase = objectInput.readBoolean();
		liberoString1 = objectInput.readUTF();
		liberoString2 = objectInput.readUTF();
		liberoString3 = objectInput.readUTF();
		liberoString4 = objectInput.readUTF();
		liberoString5 = objectInput.readUTF();
		liberoDate1 = objectInput.readLong();
		liberoDate2 = objectInput.readLong();
		liberoDate3 = objectInput.readLong();
		liberoDate4 = objectInput.readLong();
		liberoDate5 = objectInput.readLong();
		liberoLong1 = objectInput.readLong();
		liberoLong2 = objectInput.readLong();
		liberoLong3 = objectInput.readLong();
		liberoLong4 = objectInput.readLong();
		liberoLong5 = objectInput.readLong();
		liberoDouble1 = objectInput.readDouble();
		liberoDouble2 = objectInput.readDouble();
		liberoDouble3 = objectInput.readDouble();
		liberoDouble4 = objectInput.readDouble();
		liberoDouble5 = objectInput.readDouble();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (codiceArticolo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceArticolo);
		}

		if (descrizione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizione);
		}

		if (descrizioneAggiuntiva == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizioneAggiuntiva);
		}

		if (descrizioneBreveFiscale == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizioneBreveFiscale);
		}

		if (pathImmagine == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(pathImmagine);
		}

		if (unitaMisura == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(unitaMisura);
		}

		if (categoriaMerceologica == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(categoriaMerceologica);
		}

		if (categoriaFiscale == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(categoriaFiscale);
		}

		if (codiceIVA == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVA);
		}

		if (codiceProvvigioni == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceProvvigioni);
		}

		if (codiceSconto1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceSconto1);
		}

		if (codiceSconto2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceSconto2);
		}

		if (categoriaInventario == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(categoriaInventario);
		}

		objectOutput.writeDouble(prezzo1);
		objectOutput.writeDouble(prezzo2);
		objectOutput.writeDouble(prezzo3);

		if (codiceIVAPrezzo1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVAPrezzo1);
		}

		if (codiceIVAPrezzo2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVAPrezzo2);
		}

		if (codiceIVAPrezzo3 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVAPrezzo3);
		}

		objectOutput.writeInt(numeroDecimaliPrezzo);
		objectOutput.writeInt(numeroDecimaliQuantita);
		objectOutput.writeInt(numeroMesiPerinvenduto);

		if (unitaMisura2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(unitaMisura2);
		}

		objectOutput.writeDouble(fattoreMoltiplicativo);
		objectOutput.writeDouble(fattoreDivisione);

		if (ragruppamentoLibero == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ragruppamentoLibero);
		}

		objectOutput.writeInt(tipoLivelloDistintaBase);
		objectOutput.writeInt(tipoCostoStatistico);
		objectOutput.writeInt(tipoGestioneArticolo);
		objectOutput.writeInt(tipoApprovvigionamentoArticolo);
		objectOutput.writeDouble(nomenclaturaCombinata);

		if (descrizioneEstesa == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizioneEstesa);
		}

		objectOutput.writeBoolean(generazioneMovimenti);
		objectOutput.writeInt(tipoEsplosioneDistintaBase);
		objectOutput.writeInt(livelloMaxEsplosioneDistintaBase);
		objectOutput.writeBoolean(valorizzazioneMagazzino);
		objectOutput.writeBoolean(abilitatoEC);

		if (categoriaEC == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(categoriaEC);
		}

		objectOutput.writeDouble(quantitaMinimaEC);
		objectOutput.writeDouble(quantitaDefaultEC);

		if (codiceSchedaTecnicaEC == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceSchedaTecnicaEC);
		}

		objectOutput.writeInt(giorniPrevistaConsegna);
		objectOutput.writeBoolean(gestioneLotti);
		objectOutput.writeBoolean(creazioneLotti);

		if (prefissoCodiceLottoAutomatico == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(prefissoCodiceLottoAutomatico);
		}

		objectOutput.writeInt(numeroCifreProgressivo);
		objectOutput.writeBoolean(scaricoAutomaticoLotti);
		objectOutput.writeInt(giorniInizioValiditaLotto);
		objectOutput.writeInt(giorniValiditaLotto);
		objectOutput.writeInt(giorniPreavvisoScadenzaLotto);
		objectOutput.writeDouble(fattoreMoltQuantRiservata);
		objectOutput.writeDouble(fattoreDivQuantRiservata);
		objectOutput.writeBoolean(obsoleto);
		objectOutput.writeBoolean(comeImballo);
		objectOutput.writeDouble(fattoreMoltCalcoloImballo);
		objectOutput.writeDouble(fattoreDivCalcoloImballo);
		objectOutput.writeDouble(pesoLordo);
		objectOutput.writeDouble(tara);
		objectOutput.writeInt(calcoloVolume);
		objectOutput.writeDouble(lunghezza);
		objectOutput.writeDouble(larghezza);
		objectOutput.writeDouble(profondita);

		if (codiceImballo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceImballo);
		}

		objectOutput.writeDouble(fattoreMoltUnitMisSupp);
		objectOutput.writeDouble(fattoreDivUnitMisSupp);
		objectOutput.writeInt(stampaetichette);
		objectOutput.writeBoolean(gestioneVarianti);
		objectOutput.writeBoolean(gestioneVariantiDistintaBase);

		if (liberoString1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(liberoString1);
		}

		if (liberoString2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(liberoString2);
		}

		if (liberoString3 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(liberoString3);
		}

		if (liberoString4 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(liberoString4);
		}

		if (liberoString5 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(liberoString5);
		}

		objectOutput.writeLong(liberoDate1);
		objectOutput.writeLong(liberoDate2);
		objectOutput.writeLong(liberoDate3);
		objectOutput.writeLong(liberoDate4);
		objectOutput.writeLong(liberoDate5);
		objectOutput.writeLong(liberoLong1);
		objectOutput.writeLong(liberoLong2);
		objectOutput.writeLong(liberoLong3);
		objectOutput.writeLong(liberoLong4);
		objectOutput.writeLong(liberoLong5);
		objectOutput.writeDouble(liberoDouble1);
		objectOutput.writeDouble(liberoDouble2);
		objectOutput.writeDouble(liberoDouble3);
		objectOutput.writeDouble(liberoDouble4);
		objectOutput.writeDouble(liberoDouble5);
	}

	public String codiceArticolo;
	public String descrizione;
	public String descrizioneAggiuntiva;
	public String descrizioneBreveFiscale;
	public String pathImmagine;
	public String unitaMisura;
	public String categoriaMerceologica;
	public String categoriaFiscale;
	public String codiceIVA;
	public String codiceProvvigioni;
	public String codiceSconto1;
	public String codiceSconto2;
	public String categoriaInventario;
	public double prezzo1;
	public double prezzo2;
	public double prezzo3;
	public String codiceIVAPrezzo1;
	public String codiceIVAPrezzo2;
	public String codiceIVAPrezzo3;
	public int numeroDecimaliPrezzo;
	public int numeroDecimaliQuantita;
	public int numeroMesiPerinvenduto;
	public String unitaMisura2;
	public double fattoreMoltiplicativo;
	public double fattoreDivisione;
	public String ragruppamentoLibero;
	public int tipoLivelloDistintaBase;
	public int tipoCostoStatistico;
	public int tipoGestioneArticolo;
	public int tipoApprovvigionamentoArticolo;
	public double nomenclaturaCombinata;
	public String descrizioneEstesa;
	public boolean generazioneMovimenti;
	public int tipoEsplosioneDistintaBase;
	public int livelloMaxEsplosioneDistintaBase;
	public boolean valorizzazioneMagazzino;
	public boolean abilitatoEC;
	public String categoriaEC;
	public double quantitaMinimaEC;
	public double quantitaDefaultEC;
	public String codiceSchedaTecnicaEC;
	public int giorniPrevistaConsegna;
	public boolean gestioneLotti;
	public boolean creazioneLotti;
	public String prefissoCodiceLottoAutomatico;
	public int numeroCifreProgressivo;
	public boolean scaricoAutomaticoLotti;
	public int giorniInizioValiditaLotto;
	public int giorniValiditaLotto;
	public int giorniPreavvisoScadenzaLotto;
	public double fattoreMoltQuantRiservata;
	public double fattoreDivQuantRiservata;
	public boolean obsoleto;
	public boolean comeImballo;
	public double fattoreMoltCalcoloImballo;
	public double fattoreDivCalcoloImballo;
	public double pesoLordo;
	public double tara;
	public int calcoloVolume;
	public double lunghezza;
	public double larghezza;
	public double profondita;
	public String codiceImballo;
	public double fattoreMoltUnitMisSupp;
	public double fattoreDivUnitMisSupp;
	public int stampaetichette;
	public boolean gestioneVarianti;
	public boolean gestioneVariantiDistintaBase;
	public String liberoString1;
	public String liberoString2;
	public String liberoString3;
	public String liberoString4;
	public String liberoString5;
	public long liberoDate1;
	public long liberoDate2;
	public long liberoDate3;
	public long liberoDate4;
	public long liberoDate5;
	public long liberoLong1;
	public long liberoLong2;
	public long liberoLong3;
	public long liberoLong4;
	public long liberoLong5;
	public double liberoDouble1;
	public double liberoDouble2;
	public double liberoDouble3;
	public double liberoDouble4;
	public double liberoDouble5;
}