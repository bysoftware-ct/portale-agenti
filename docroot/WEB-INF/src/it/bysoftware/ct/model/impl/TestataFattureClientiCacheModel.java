/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.TestataFattureClienti;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing TestataFattureClienti in entity cache.
 *
 * @author Mario Torrisi
 * @see TestataFattureClienti
 * @generated
 */
public class TestataFattureClientiCacheModel implements CacheModel<TestataFattureClienti>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(103);

		sb.append("{anno=");
		sb.append(anno);
		sb.append(", codiceAttivita=");
		sb.append(codiceAttivita);
		sb.append(", codiceCentro=");
		sb.append(codiceCentro);
		sb.append(", numeroProtocollo=");
		sb.append(numeroProtocollo);
		sb.append(", tipoDocumento=");
		sb.append(tipoDocumento);
		sb.append(", codiceCentroContAnalitica=");
		sb.append(codiceCentroContAnalitica);
		sb.append(", idTipoDocumentoOrigine=");
		sb.append(idTipoDocumentoOrigine);
		sb.append(", statoFattura=");
		sb.append(statoFattura);
		sb.append(", dataRegistrazione=");
		sb.append(dataRegistrazione);
		sb.append(", dataOperazione=");
		sb.append(dataOperazione);
		sb.append(", dataAnnotazione=");
		sb.append(dataAnnotazione);
		sb.append(", dataDocumento=");
		sb.append(dataDocumento);
		sb.append(", numeroDocumento=");
		sb.append(numeroDocumento);
		sb.append(", riferimentoAScontrino=");
		sb.append(riferimentoAScontrino);
		sb.append(", descrizioneEstremiDocumento=");
		sb.append(descrizioneEstremiDocumento);
		sb.append(", tipoSoggetto=");
		sb.append(tipoSoggetto);
		sb.append(", codiceCliente=");
		sb.append(codiceCliente);
		sb.append(", descrizioneAggiuntivaFattura=");
		sb.append(descrizioneAggiuntivaFattura);
		sb.append(", estremiOrdine=");
		sb.append(estremiOrdine);
		sb.append(", estremiBolla=");
		sb.append(estremiBolla);
		sb.append(", codicePagamento=");
		sb.append(codicePagamento);
		sb.append(", codiceAgente=");
		sb.append(codiceAgente);
		sb.append(", codiceGruppoAgenti=");
		sb.append(codiceGruppoAgenti);
		sb.append(", annotazioni=");
		sb.append(annotazioni);
		sb.append(", scontoChiusura=");
		sb.append(scontoChiusura);
		sb.append(", scontoProntaCassa=");
		sb.append(scontoProntaCassa);
		sb.append(", pertualeSpeseTrasp=");
		sb.append(pertualeSpeseTrasp);
		sb.append(", importoSpeseTrasp=");
		sb.append(importoSpeseTrasp);
		sb.append(", importoSpeseImb=");
		sb.append(importoSpeseImb);
		sb.append(", importoSpeseVarie=");
		sb.append(importoSpeseVarie);
		sb.append(", importoSpeseBancarie=");
		sb.append(importoSpeseBancarie);
		sb.append(", codiceIVATrasp=");
		sb.append(codiceIVATrasp);
		sb.append(", codiceIVAImb=");
		sb.append(codiceIVAImb);
		sb.append(", codiceIVAVarie=");
		sb.append(codiceIVAVarie);
		sb.append(", codiceIVABancarie=");
		sb.append(codiceIVABancarie);
		sb.append(", totaleScontiCorpo=");
		sb.append(totaleScontiCorpo);
		sb.append(", imponibile=");
		sb.append(imponibile);
		sb.append(", IVA=");
		sb.append(IVA);
		sb.append(", importoFattura=");
		sb.append(importoFattura);
		sb.append(", libStr1=");
		sb.append(libStr1);
		sb.append(", libStr2=");
		sb.append(libStr2);
		sb.append(", libStr3=");
		sb.append(libStr3);
		sb.append(", libDbl1=");
		sb.append(libDbl1);
		sb.append(", libDbl2=");
		sb.append(libDbl2);
		sb.append(", libDbl3=");
		sb.append(libDbl3);
		sb.append(", libLng1=");
		sb.append(libLng1);
		sb.append(", libLng2=");
		sb.append(libLng2);
		sb.append(", libLng3=");
		sb.append(libLng3);
		sb.append(", libDat1=");
		sb.append(libDat1);
		sb.append(", libDat2=");
		sb.append(libDat2);
		sb.append(", libDat3=");
		sb.append(libDat3);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public TestataFattureClienti toEntityModel() {
		TestataFattureClientiImpl testataFattureClientiImpl = new TestataFattureClientiImpl();

		testataFattureClientiImpl.setAnno(anno);

		if (codiceAttivita == null) {
			testataFattureClientiImpl.setCodiceAttivita(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setCodiceAttivita(codiceAttivita);
		}

		if (codiceCentro == null) {
			testataFattureClientiImpl.setCodiceCentro(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setCodiceCentro(codiceCentro);
		}

		testataFattureClientiImpl.setNumeroProtocollo(numeroProtocollo);

		if (tipoDocumento == null) {
			testataFattureClientiImpl.setTipoDocumento(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setTipoDocumento(tipoDocumento);
		}

		if (codiceCentroContAnalitica == null) {
			testataFattureClientiImpl.setCodiceCentroContAnalitica(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setCodiceCentroContAnalitica(codiceCentroContAnalitica);
		}

		testataFattureClientiImpl.setIdTipoDocumentoOrigine(idTipoDocumentoOrigine);
		testataFattureClientiImpl.setStatoFattura(statoFattura);

		if (dataRegistrazione == Long.MIN_VALUE) {
			testataFattureClientiImpl.setDataRegistrazione(null);
		}
		else {
			testataFattureClientiImpl.setDataRegistrazione(new Date(
					dataRegistrazione));
		}

		if (dataOperazione == Long.MIN_VALUE) {
			testataFattureClientiImpl.setDataOperazione(null);
		}
		else {
			testataFattureClientiImpl.setDataOperazione(new Date(dataOperazione));
		}

		if (dataAnnotazione == Long.MIN_VALUE) {
			testataFattureClientiImpl.setDataAnnotazione(null);
		}
		else {
			testataFattureClientiImpl.setDataAnnotazione(new Date(
					dataAnnotazione));
		}

		if (dataDocumento == Long.MIN_VALUE) {
			testataFattureClientiImpl.setDataDocumento(null);
		}
		else {
			testataFattureClientiImpl.setDataDocumento(new Date(dataDocumento));
		}

		testataFattureClientiImpl.setNumeroDocumento(numeroDocumento);
		testataFattureClientiImpl.setRiferimentoAScontrino(riferimentoAScontrino);

		if (descrizioneEstremiDocumento == null) {
			testataFattureClientiImpl.setDescrizioneEstremiDocumento(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setDescrizioneEstremiDocumento(descrizioneEstremiDocumento);
		}

		testataFattureClientiImpl.setTipoSoggetto(tipoSoggetto);

		if (codiceCliente == null) {
			testataFattureClientiImpl.setCodiceCliente(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setCodiceCliente(codiceCliente);
		}

		if (descrizioneAggiuntivaFattura == null) {
			testataFattureClientiImpl.setDescrizioneAggiuntivaFattura(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setDescrizioneAggiuntivaFattura(descrizioneAggiuntivaFattura);
		}

		if (estremiOrdine == null) {
			testataFattureClientiImpl.setEstremiOrdine(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setEstremiOrdine(estremiOrdine);
		}

		if (estremiBolla == null) {
			testataFattureClientiImpl.setEstremiBolla(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setEstremiBolla(estremiBolla);
		}

		if (codicePagamento == null) {
			testataFattureClientiImpl.setCodicePagamento(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setCodicePagamento(codicePagamento);
		}

		if (codiceAgente == null) {
			testataFattureClientiImpl.setCodiceAgente(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setCodiceAgente(codiceAgente);
		}

		if (codiceGruppoAgenti == null) {
			testataFattureClientiImpl.setCodiceGruppoAgenti(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setCodiceGruppoAgenti(codiceGruppoAgenti);
		}

		if (annotazioni == null) {
			testataFattureClientiImpl.setAnnotazioni(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setAnnotazioni(annotazioni);
		}

		testataFattureClientiImpl.setScontoChiusura(scontoChiusura);
		testataFattureClientiImpl.setScontoProntaCassa(scontoProntaCassa);
		testataFattureClientiImpl.setPertualeSpeseTrasp(pertualeSpeseTrasp);
		testataFattureClientiImpl.setImportoSpeseTrasp(importoSpeseTrasp);
		testataFattureClientiImpl.setImportoSpeseImb(importoSpeseImb);
		testataFattureClientiImpl.setImportoSpeseVarie(importoSpeseVarie);
		testataFattureClientiImpl.setImportoSpeseBancarie(importoSpeseBancarie);

		if (codiceIVATrasp == null) {
			testataFattureClientiImpl.setCodiceIVATrasp(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setCodiceIVATrasp(codiceIVATrasp);
		}

		if (codiceIVAImb == null) {
			testataFattureClientiImpl.setCodiceIVAImb(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setCodiceIVAImb(codiceIVAImb);
		}

		if (codiceIVAVarie == null) {
			testataFattureClientiImpl.setCodiceIVAVarie(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setCodiceIVAVarie(codiceIVAVarie);
		}

		if (codiceIVABancarie == null) {
			testataFattureClientiImpl.setCodiceIVABancarie(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setCodiceIVABancarie(codiceIVABancarie);
		}

		testataFattureClientiImpl.setTotaleScontiCorpo(totaleScontiCorpo);
		testataFattureClientiImpl.setImponibile(imponibile);
		testataFattureClientiImpl.setIVA(IVA);
		testataFattureClientiImpl.setImportoFattura(importoFattura);

		if (libStr1 == null) {
			testataFattureClientiImpl.setLibStr1(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setLibStr1(libStr1);
		}

		if (libStr2 == null) {
			testataFattureClientiImpl.setLibStr2(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setLibStr2(libStr2);
		}

		if (libStr3 == null) {
			testataFattureClientiImpl.setLibStr3(StringPool.BLANK);
		}
		else {
			testataFattureClientiImpl.setLibStr3(libStr3);
		}

		testataFattureClientiImpl.setLibDbl1(libDbl1);
		testataFattureClientiImpl.setLibDbl2(libDbl2);
		testataFattureClientiImpl.setLibDbl3(libDbl3);
		testataFattureClientiImpl.setLibLng1(libLng1);
		testataFattureClientiImpl.setLibLng2(libLng2);
		testataFattureClientiImpl.setLibLng3(libLng3);

		if (libDat1 == Long.MIN_VALUE) {
			testataFattureClientiImpl.setLibDat1(null);
		}
		else {
			testataFattureClientiImpl.setLibDat1(new Date(libDat1));
		}

		if (libDat2 == Long.MIN_VALUE) {
			testataFattureClientiImpl.setLibDat2(null);
		}
		else {
			testataFattureClientiImpl.setLibDat2(new Date(libDat2));
		}

		if (libDat3 == Long.MIN_VALUE) {
			testataFattureClientiImpl.setLibDat3(null);
		}
		else {
			testataFattureClientiImpl.setLibDat3(new Date(libDat3));
		}

		testataFattureClientiImpl.resetOriginalValues();

		return testataFattureClientiImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		anno = objectInput.readInt();
		codiceAttivita = objectInput.readUTF();
		codiceCentro = objectInput.readUTF();
		numeroProtocollo = objectInput.readInt();
		tipoDocumento = objectInput.readUTF();
		codiceCentroContAnalitica = objectInput.readUTF();
		idTipoDocumentoOrigine = objectInput.readInt();
		statoFattura = objectInput.readBoolean();
		dataRegistrazione = objectInput.readLong();
		dataOperazione = objectInput.readLong();
		dataAnnotazione = objectInput.readLong();
		dataDocumento = objectInput.readLong();
		numeroDocumento = objectInput.readInt();
		riferimentoAScontrino = objectInput.readInt();
		descrizioneEstremiDocumento = objectInput.readUTF();
		tipoSoggetto = objectInput.readBoolean();
		codiceCliente = objectInput.readUTF();
		descrizioneAggiuntivaFattura = objectInput.readUTF();
		estremiOrdine = objectInput.readUTF();
		estremiBolla = objectInput.readUTF();
		codicePagamento = objectInput.readUTF();
		codiceAgente = objectInput.readUTF();
		codiceGruppoAgenti = objectInput.readUTF();
		annotazioni = objectInput.readUTF();
		scontoChiusura = objectInput.readDouble();
		scontoProntaCassa = objectInput.readDouble();
		pertualeSpeseTrasp = objectInput.readDouble();
		importoSpeseTrasp = objectInput.readDouble();
		importoSpeseImb = objectInput.readDouble();
		importoSpeseVarie = objectInput.readDouble();
		importoSpeseBancarie = objectInput.readDouble();
		codiceIVATrasp = objectInput.readUTF();
		codiceIVAImb = objectInput.readUTF();
		codiceIVAVarie = objectInput.readUTF();
		codiceIVABancarie = objectInput.readUTF();
		totaleScontiCorpo = objectInput.readDouble();
		imponibile = objectInput.readDouble();
		IVA = objectInput.readDouble();
		importoFattura = objectInput.readDouble();
		libStr1 = objectInput.readUTF();
		libStr2 = objectInput.readUTF();
		libStr3 = objectInput.readUTF();
		libDbl1 = objectInput.readDouble();
		libDbl2 = objectInput.readDouble();
		libDbl3 = objectInput.readDouble();
		libLng1 = objectInput.readLong();
		libLng2 = objectInput.readLong();
		libLng3 = objectInput.readLong();
		libDat1 = objectInput.readLong();
		libDat2 = objectInput.readLong();
		libDat3 = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(anno);

		if (codiceAttivita == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceAttivita);
		}

		if (codiceCentro == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCentro);
		}

		objectOutput.writeInt(numeroProtocollo);

		if (tipoDocumento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoDocumento);
		}

		if (codiceCentroContAnalitica == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCentroContAnalitica);
		}

		objectOutput.writeInt(idTipoDocumentoOrigine);
		objectOutput.writeBoolean(statoFattura);
		objectOutput.writeLong(dataRegistrazione);
		objectOutput.writeLong(dataOperazione);
		objectOutput.writeLong(dataAnnotazione);
		objectOutput.writeLong(dataDocumento);
		objectOutput.writeInt(numeroDocumento);
		objectOutput.writeInt(riferimentoAScontrino);

		if (descrizioneEstremiDocumento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizioneEstremiDocumento);
		}

		objectOutput.writeBoolean(tipoSoggetto);

		if (codiceCliente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCliente);
		}

		if (descrizioneAggiuntivaFattura == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizioneAggiuntivaFattura);
		}

		if (estremiOrdine == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(estremiOrdine);
		}

		if (estremiBolla == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(estremiBolla);
		}

		if (codicePagamento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codicePagamento);
		}

		if (codiceAgente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceAgente);
		}

		if (codiceGruppoAgenti == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceGruppoAgenti);
		}

		if (annotazioni == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(annotazioni);
		}

		objectOutput.writeDouble(scontoChiusura);
		objectOutput.writeDouble(scontoProntaCassa);
		objectOutput.writeDouble(pertualeSpeseTrasp);
		objectOutput.writeDouble(importoSpeseTrasp);
		objectOutput.writeDouble(importoSpeseImb);
		objectOutput.writeDouble(importoSpeseVarie);
		objectOutput.writeDouble(importoSpeseBancarie);

		if (codiceIVATrasp == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVATrasp);
		}

		if (codiceIVAImb == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVAImb);
		}

		if (codiceIVAVarie == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVAVarie);
		}

		if (codiceIVABancarie == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVABancarie);
		}

		objectOutput.writeDouble(totaleScontiCorpo);
		objectOutput.writeDouble(imponibile);
		objectOutput.writeDouble(IVA);
		objectOutput.writeDouble(importoFattura);

		if (libStr1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr1);
		}

		if (libStr2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr2);
		}

		if (libStr3 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr3);
		}

		objectOutput.writeDouble(libDbl1);
		objectOutput.writeDouble(libDbl2);
		objectOutput.writeDouble(libDbl3);
		objectOutput.writeLong(libLng1);
		objectOutput.writeLong(libLng2);
		objectOutput.writeLong(libLng3);
		objectOutput.writeLong(libDat1);
		objectOutput.writeLong(libDat2);
		objectOutput.writeLong(libDat3);
	}

	public int anno;
	public String codiceAttivita;
	public String codiceCentro;
	public int numeroProtocollo;
	public String tipoDocumento;
	public String codiceCentroContAnalitica;
	public int idTipoDocumentoOrigine;
	public boolean statoFattura;
	public long dataRegistrazione;
	public long dataOperazione;
	public long dataAnnotazione;
	public long dataDocumento;
	public int numeroDocumento;
	public int riferimentoAScontrino;
	public String descrizioneEstremiDocumento;
	public boolean tipoSoggetto;
	public String codiceCliente;
	public String descrizioneAggiuntivaFattura;
	public String estremiOrdine;
	public String estremiBolla;
	public String codicePagamento;
	public String codiceAgente;
	public String codiceGruppoAgenti;
	public String annotazioni;
	public double scontoChiusura;
	public double scontoProntaCassa;
	public double pertualeSpeseTrasp;
	public double importoSpeseTrasp;
	public double importoSpeseImb;
	public double importoSpeseVarie;
	public double importoSpeseBancarie;
	public String codiceIVATrasp;
	public String codiceIVAImb;
	public String codiceIVAVarie;
	public String codiceIVABancarie;
	public double totaleScontiCorpo;
	public double imponibile;
	public double IVA;
	public double importoFattura;
	public String libStr1;
	public String libStr2;
	public String libStr3;
	public double libDbl1;
	public double libDbl2;
	public double libDbl3;
	public long libLng1;
	public long libLng2;
	public long libLng3;
	public long libDat1;
	public long libDat2;
	public long libDat3;
}