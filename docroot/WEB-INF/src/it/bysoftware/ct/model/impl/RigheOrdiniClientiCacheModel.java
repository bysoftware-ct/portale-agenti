/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.RigheOrdiniClienti;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing RigheOrdiniClienti in entity cache.
 *
 * @author Mario Torrisi
 * @see RigheOrdiniClienti
 * @generated
 */
public class RigheOrdiniClientiCacheModel implements CacheModel<RigheOrdiniClienti>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(127);

		sb.append("{anno=");
		sb.append(anno);
		sb.append(", codiceAttivita=");
		sb.append(codiceAttivita);
		sb.append(", codiceCentro=");
		sb.append(codiceCentro);
		sb.append(", codiceDeposito=");
		sb.append(codiceDeposito);
		sb.append(", tipoOrdine=");
		sb.append(tipoOrdine);
		sb.append(", numeroOrdine=");
		sb.append(numeroOrdine);
		sb.append(", numeroRigo=");
		sb.append(numeroRigo);
		sb.append(", statoRigo=");
		sb.append(statoRigo);
		sb.append(", tipoRigo=");
		sb.append(tipoRigo);
		sb.append(", codiceCausaleMagazzino=");
		sb.append(codiceCausaleMagazzino);
		sb.append(", codiceDepositoMov=");
		sb.append(codiceDepositoMov);
		sb.append(", codiceArticolo=");
		sb.append(codiceArticolo);
		sb.append(", codiceVariante=");
		sb.append(codiceVariante);
		sb.append(", descrizione=");
		sb.append(descrizione);
		sb.append(", codiceUnitMis=");
		sb.append(codiceUnitMis);
		sb.append(", decimaliQuant=");
		sb.append(decimaliQuant);
		sb.append(", quantita1=");
		sb.append(quantita1);
		sb.append(", quantita2=");
		sb.append(quantita2);
		sb.append(", quantita3=");
		sb.append(quantita3);
		sb.append(", quantita=");
		sb.append(quantita);
		sb.append(", codiceUnitMis2=");
		sb.append(codiceUnitMis2);
		sb.append(", quantitaUnitMis2=");
		sb.append(quantitaUnitMis2);
		sb.append(", decimaliPrezzo=");
		sb.append(decimaliPrezzo);
		sb.append(", prezzo=");
		sb.append(prezzo);
		sb.append(", importoLordo=");
		sb.append(importoLordo);
		sb.append(", sconto1=");
		sb.append(sconto1);
		sb.append(", sconto2=");
		sb.append(sconto2);
		sb.append(", sconto3=");
		sb.append(sconto3);
		sb.append(", importoNetto=");
		sb.append(importoNetto);
		sb.append(", importo=");
		sb.append(importo);
		sb.append(", codiceIVAFatturazione=");
		sb.append(codiceIVAFatturazione);
		sb.append(", tipoProvviggione=");
		sb.append(tipoProvviggione);
		sb.append(", percentualeProvvAgente=");
		sb.append(percentualeProvvAgente);
		sb.append(", importoProvvAgente=");
		sb.append(importoProvvAgente);
		sb.append(", codiceSottoconto=");
		sb.append(codiceSottoconto);
		sb.append(", nomenclaturaCombinata=");
		sb.append(nomenclaturaCombinata);
		sb.append(", stampaDistBase=");
		sb.append(stampaDistBase);
		sb.append(", codiceCliente=");
		sb.append(codiceCliente);
		sb.append(", riferimentoOrdineCliente=");
		sb.append(riferimentoOrdineCliente);
		sb.append(", dataOridine=");
		sb.append(dataOridine);
		sb.append(", statoEvasione=");
		sb.append(statoEvasione);
		sb.append(", dataPrevistaConsegna=");
		sb.append(dataPrevistaConsegna);
		sb.append(", dataRegistrazioneOrdine=");
		sb.append(dataRegistrazioneOrdine);
		sb.append(", numeroPrimaNota=");
		sb.append(numeroPrimaNota);
		sb.append(", numeroProgressivo=");
		sb.append(numeroProgressivo);
		sb.append(", tipoCompEspl=");
		sb.append(tipoCompEspl);
		sb.append(", maxLivCompEspl=");
		sb.append(maxLivCompEspl);
		sb.append(", generazioneOrdFornit=");
		sb.append(generazioneOrdFornit);
		sb.append(", esercizioProOrd=");
		sb.append(esercizioProOrd);
		sb.append(", numPropOrdine=");
		sb.append(numPropOrdine);
		sb.append(", numRigoPropOrdine=");
		sb.append(numRigoPropOrdine);
		sb.append(", libStr1=");
		sb.append(libStr1);
		sb.append(", libStr2=");
		sb.append(libStr2);
		sb.append(", libStr3=");
		sb.append(libStr3);
		sb.append(", libDbl1=");
		sb.append(libDbl1);
		sb.append(", libDbl2=");
		sb.append(libDbl2);
		sb.append(", libDbl3=");
		sb.append(libDbl3);
		sb.append(", libDat1=");
		sb.append(libDat1);
		sb.append(", libDat2=");
		sb.append(libDat2);
		sb.append(", libDat3=");
		sb.append(libDat3);
		sb.append(", libLng1=");
		sb.append(libLng1);
		sb.append(", libLng2=");
		sb.append(libLng2);
		sb.append(", libLng3=");
		sb.append(libLng3);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public RigheOrdiniClienti toEntityModel() {
		RigheOrdiniClientiImpl righeOrdiniClientiImpl = new RigheOrdiniClientiImpl();

		righeOrdiniClientiImpl.setAnno(anno);

		if (codiceAttivita == null) {
			righeOrdiniClientiImpl.setCodiceAttivita(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setCodiceAttivita(codiceAttivita);
		}

		if (codiceCentro == null) {
			righeOrdiniClientiImpl.setCodiceCentro(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setCodiceCentro(codiceCentro);
		}

		if (codiceDeposito == null) {
			righeOrdiniClientiImpl.setCodiceDeposito(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setCodiceDeposito(codiceDeposito);
		}

		righeOrdiniClientiImpl.setTipoOrdine(tipoOrdine);
		righeOrdiniClientiImpl.setNumeroOrdine(numeroOrdine);
		righeOrdiniClientiImpl.setNumeroRigo(numeroRigo);
		righeOrdiniClientiImpl.setStatoRigo(statoRigo);
		righeOrdiniClientiImpl.setTipoRigo(tipoRigo);

		if (codiceCausaleMagazzino == null) {
			righeOrdiniClientiImpl.setCodiceCausaleMagazzino(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setCodiceCausaleMagazzino(codiceCausaleMagazzino);
		}

		if (codiceDepositoMov == null) {
			righeOrdiniClientiImpl.setCodiceDepositoMov(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setCodiceDepositoMov(codiceDepositoMov);
		}

		if (codiceArticolo == null) {
			righeOrdiniClientiImpl.setCodiceArticolo(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setCodiceArticolo(codiceArticolo);
		}

		if (codiceVariante == null) {
			righeOrdiniClientiImpl.setCodiceVariante(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setCodiceVariante(codiceVariante);
		}

		if (descrizione == null) {
			righeOrdiniClientiImpl.setDescrizione(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setDescrizione(descrizione);
		}

		if (codiceUnitMis == null) {
			righeOrdiniClientiImpl.setCodiceUnitMis(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setCodiceUnitMis(codiceUnitMis);
		}

		righeOrdiniClientiImpl.setDecimaliQuant(decimaliQuant);
		righeOrdiniClientiImpl.setQuantita1(quantita1);
		righeOrdiniClientiImpl.setQuantita2(quantita2);
		righeOrdiniClientiImpl.setQuantita3(quantita3);
		righeOrdiniClientiImpl.setQuantita(quantita);

		if (codiceUnitMis2 == null) {
			righeOrdiniClientiImpl.setCodiceUnitMis2(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setCodiceUnitMis2(codiceUnitMis2);
		}

		righeOrdiniClientiImpl.setQuantitaUnitMis2(quantitaUnitMis2);
		righeOrdiniClientiImpl.setDecimaliPrezzo(decimaliPrezzo);
		righeOrdiniClientiImpl.setPrezzo(prezzo);
		righeOrdiniClientiImpl.setImportoLordo(importoLordo);
		righeOrdiniClientiImpl.setSconto1(sconto1);
		righeOrdiniClientiImpl.setSconto2(sconto2);
		righeOrdiniClientiImpl.setSconto3(sconto3);
		righeOrdiniClientiImpl.setImportoNetto(importoNetto);
		righeOrdiniClientiImpl.setImporto(importo);

		if (codiceIVAFatturazione == null) {
			righeOrdiniClientiImpl.setCodiceIVAFatturazione(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setCodiceIVAFatturazione(codiceIVAFatturazione);
		}

		righeOrdiniClientiImpl.setTipoProvviggione(tipoProvviggione);
		righeOrdiniClientiImpl.setPercentualeProvvAgente(percentualeProvvAgente);
		righeOrdiniClientiImpl.setImportoProvvAgente(importoProvvAgente);

		if (codiceSottoconto == null) {
			righeOrdiniClientiImpl.setCodiceSottoconto(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setCodiceSottoconto(codiceSottoconto);
		}

		righeOrdiniClientiImpl.setNomenclaturaCombinata(nomenclaturaCombinata);
		righeOrdiniClientiImpl.setStampaDistBase(stampaDistBase);

		if (codiceCliente == null) {
			righeOrdiniClientiImpl.setCodiceCliente(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setCodiceCliente(codiceCliente);
		}

		if (riferimentoOrdineCliente == null) {
			righeOrdiniClientiImpl.setRiferimentoOrdineCliente(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setRiferimentoOrdineCliente(riferimentoOrdineCliente);
		}

		if (dataOridine == Long.MIN_VALUE) {
			righeOrdiniClientiImpl.setDataOridine(null);
		}
		else {
			righeOrdiniClientiImpl.setDataOridine(new Date(dataOridine));
		}

		righeOrdiniClientiImpl.setStatoEvasione(statoEvasione);

		if (dataPrevistaConsegna == Long.MIN_VALUE) {
			righeOrdiniClientiImpl.setDataPrevistaConsegna(null);
		}
		else {
			righeOrdiniClientiImpl.setDataPrevistaConsegna(new Date(
					dataPrevistaConsegna));
		}

		if (dataRegistrazioneOrdine == Long.MIN_VALUE) {
			righeOrdiniClientiImpl.setDataRegistrazioneOrdine(null);
		}
		else {
			righeOrdiniClientiImpl.setDataRegistrazioneOrdine(new Date(
					dataRegistrazioneOrdine));
		}

		righeOrdiniClientiImpl.setNumeroPrimaNota(numeroPrimaNota);
		righeOrdiniClientiImpl.setNumeroProgressivo(numeroProgressivo);
		righeOrdiniClientiImpl.setTipoCompEspl(tipoCompEspl);
		righeOrdiniClientiImpl.setMaxLivCompEspl(maxLivCompEspl);
		righeOrdiniClientiImpl.setGenerazioneOrdFornit(generazioneOrdFornit);
		righeOrdiniClientiImpl.setEsercizioProOrd(esercizioProOrd);
		righeOrdiniClientiImpl.setNumPropOrdine(numPropOrdine);
		righeOrdiniClientiImpl.setNumRigoPropOrdine(numRigoPropOrdine);

		if (libStr1 == null) {
			righeOrdiniClientiImpl.setLibStr1(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setLibStr1(libStr1);
		}

		if (libStr2 == null) {
			righeOrdiniClientiImpl.setLibStr2(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setLibStr2(libStr2);
		}

		if (libStr3 == null) {
			righeOrdiniClientiImpl.setLibStr3(StringPool.BLANK);
		}
		else {
			righeOrdiniClientiImpl.setLibStr3(libStr3);
		}

		righeOrdiniClientiImpl.setLibDbl1(libDbl1);
		righeOrdiniClientiImpl.setLibDbl2(libDbl2);
		righeOrdiniClientiImpl.setLibDbl3(libDbl3);

		if (libDat1 == Long.MIN_VALUE) {
			righeOrdiniClientiImpl.setLibDat1(null);
		}
		else {
			righeOrdiniClientiImpl.setLibDat1(new Date(libDat1));
		}

		if (libDat2 == Long.MIN_VALUE) {
			righeOrdiniClientiImpl.setLibDat2(null);
		}
		else {
			righeOrdiniClientiImpl.setLibDat2(new Date(libDat2));
		}

		if (libDat3 == Long.MIN_VALUE) {
			righeOrdiniClientiImpl.setLibDat3(null);
		}
		else {
			righeOrdiniClientiImpl.setLibDat3(new Date(libDat3));
		}

		righeOrdiniClientiImpl.setLibLng1(libLng1);
		righeOrdiniClientiImpl.setLibLng2(libLng2);
		righeOrdiniClientiImpl.setLibLng3(libLng3);

		righeOrdiniClientiImpl.resetOriginalValues();

		return righeOrdiniClientiImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		anno = objectInput.readInt();
		codiceAttivita = objectInput.readUTF();
		codiceCentro = objectInput.readUTF();
		codiceDeposito = objectInput.readUTF();
		tipoOrdine = objectInput.readInt();
		numeroOrdine = objectInput.readInt();
		numeroRigo = objectInput.readInt();
		statoRigo = objectInput.readBoolean();
		tipoRigo = objectInput.readInt();
		codiceCausaleMagazzino = objectInput.readUTF();
		codiceDepositoMov = objectInput.readUTF();
		codiceArticolo = objectInput.readUTF();
		codiceVariante = objectInput.readUTF();
		descrizione = objectInput.readUTF();
		codiceUnitMis = objectInput.readUTF();
		decimaliQuant = objectInput.readInt();
		quantita1 = objectInput.readDouble();
		quantita2 = objectInput.readDouble();
		quantita3 = objectInput.readDouble();
		quantita = objectInput.readDouble();
		codiceUnitMis2 = objectInput.readUTF();
		quantitaUnitMis2 = objectInput.readDouble();
		decimaliPrezzo = objectInput.readInt();
		prezzo = objectInput.readDouble();
		importoLordo = objectInput.readDouble();
		sconto1 = objectInput.readDouble();
		sconto2 = objectInput.readDouble();
		sconto3 = objectInput.readDouble();
		importoNetto = objectInput.readDouble();
		importo = objectInput.readDouble();
		codiceIVAFatturazione = objectInput.readUTF();
		tipoProvviggione = objectInput.readBoolean();
		percentualeProvvAgente = objectInput.readDouble();
		importoProvvAgente = objectInput.readDouble();
		codiceSottoconto = objectInput.readUTF();
		nomenclaturaCombinata = objectInput.readInt();
		stampaDistBase = objectInput.readBoolean();
		codiceCliente = objectInput.readUTF();
		riferimentoOrdineCliente = objectInput.readUTF();
		dataOridine = objectInput.readLong();
		statoEvasione = objectInput.readBoolean();
		dataPrevistaConsegna = objectInput.readLong();
		dataRegistrazioneOrdine = objectInput.readLong();
		numeroPrimaNota = objectInput.readInt();
		numeroProgressivo = objectInput.readInt();
		tipoCompEspl = objectInput.readInt();
		maxLivCompEspl = objectInput.readInt();
		generazioneOrdFornit = objectInput.readInt();
		esercizioProOrd = objectInput.readInt();
		numPropOrdine = objectInput.readInt();
		numRigoPropOrdine = objectInput.readInt();
		libStr1 = objectInput.readUTF();
		libStr2 = objectInput.readUTF();
		libStr3 = objectInput.readUTF();
		libDbl1 = objectInput.readDouble();
		libDbl2 = objectInput.readDouble();
		libDbl3 = objectInput.readDouble();
		libDat1 = objectInput.readLong();
		libDat2 = objectInput.readLong();
		libDat3 = objectInput.readLong();
		libLng1 = objectInput.readLong();
		libLng2 = objectInput.readLong();
		libLng3 = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(anno);

		if (codiceAttivita == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceAttivita);
		}

		if (codiceCentro == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCentro);
		}

		if (codiceDeposito == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDeposito);
		}

		objectOutput.writeInt(tipoOrdine);
		objectOutput.writeInt(numeroOrdine);
		objectOutput.writeInt(numeroRigo);
		objectOutput.writeBoolean(statoRigo);
		objectOutput.writeInt(tipoRigo);

		if (codiceCausaleMagazzino == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCausaleMagazzino);
		}

		if (codiceDepositoMov == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDepositoMov);
		}

		if (codiceArticolo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceArticolo);
		}

		if (codiceVariante == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceVariante);
		}

		if (descrizione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizione);
		}

		if (codiceUnitMis == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceUnitMis);
		}

		objectOutput.writeInt(decimaliQuant);
		objectOutput.writeDouble(quantita1);
		objectOutput.writeDouble(quantita2);
		objectOutput.writeDouble(quantita3);
		objectOutput.writeDouble(quantita);

		if (codiceUnitMis2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceUnitMis2);
		}

		objectOutput.writeDouble(quantitaUnitMis2);
		objectOutput.writeInt(decimaliPrezzo);
		objectOutput.writeDouble(prezzo);
		objectOutput.writeDouble(importoLordo);
		objectOutput.writeDouble(sconto1);
		objectOutput.writeDouble(sconto2);
		objectOutput.writeDouble(sconto3);
		objectOutput.writeDouble(importoNetto);
		objectOutput.writeDouble(importo);

		if (codiceIVAFatturazione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVAFatturazione);
		}

		objectOutput.writeBoolean(tipoProvviggione);
		objectOutput.writeDouble(percentualeProvvAgente);
		objectOutput.writeDouble(importoProvvAgente);

		if (codiceSottoconto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceSottoconto);
		}

		objectOutput.writeInt(nomenclaturaCombinata);
		objectOutput.writeBoolean(stampaDistBase);

		if (codiceCliente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCliente);
		}

		if (riferimentoOrdineCliente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(riferimentoOrdineCliente);
		}

		objectOutput.writeLong(dataOridine);
		objectOutput.writeBoolean(statoEvasione);
		objectOutput.writeLong(dataPrevistaConsegna);
		objectOutput.writeLong(dataRegistrazioneOrdine);
		objectOutput.writeInt(numeroPrimaNota);
		objectOutput.writeInt(numeroProgressivo);
		objectOutput.writeInt(tipoCompEspl);
		objectOutput.writeInt(maxLivCompEspl);
		objectOutput.writeInt(generazioneOrdFornit);
		objectOutput.writeInt(esercizioProOrd);
		objectOutput.writeInt(numPropOrdine);
		objectOutput.writeInt(numRigoPropOrdine);

		if (libStr1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr1);
		}

		if (libStr2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr2);
		}

		if (libStr3 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(libStr3);
		}

		objectOutput.writeDouble(libDbl1);
		objectOutput.writeDouble(libDbl2);
		objectOutput.writeDouble(libDbl3);
		objectOutput.writeLong(libDat1);
		objectOutput.writeLong(libDat2);
		objectOutput.writeLong(libDat3);
		objectOutput.writeLong(libLng1);
		objectOutput.writeLong(libLng2);
		objectOutput.writeLong(libLng3);
	}

	public int anno;
	public String codiceAttivita;
	public String codiceCentro;
	public String codiceDeposito;
	public int tipoOrdine;
	public int numeroOrdine;
	public int numeroRigo;
	public boolean statoRigo;
	public int tipoRigo;
	public String codiceCausaleMagazzino;
	public String codiceDepositoMov;
	public String codiceArticolo;
	public String codiceVariante;
	public String descrizione;
	public String codiceUnitMis;
	public int decimaliQuant;
	public double quantita1;
	public double quantita2;
	public double quantita3;
	public double quantita;
	public String codiceUnitMis2;
	public double quantitaUnitMis2;
	public int decimaliPrezzo;
	public double prezzo;
	public double importoLordo;
	public double sconto1;
	public double sconto2;
	public double sconto3;
	public double importoNetto;
	public double importo;
	public String codiceIVAFatturazione;
	public boolean tipoProvviggione;
	public double percentualeProvvAgente;
	public double importoProvvAgente;
	public String codiceSottoconto;
	public int nomenclaturaCombinata;
	public boolean stampaDistBase;
	public String codiceCliente;
	public String riferimentoOrdineCliente;
	public long dataOridine;
	public boolean statoEvasione;
	public long dataPrevistaConsegna;
	public long dataRegistrazioneOrdine;
	public int numeroPrimaNota;
	public int numeroProgressivo;
	public int tipoCompEspl;
	public int maxLivCompEspl;
	public int generazioneOrdFornit;
	public int esercizioProOrd;
	public int numPropOrdine;
	public int numRigoPropOrdine;
	public String libStr1;
	public String libStr2;
	public String libStr3;
	public double libDbl1;
	public double libDbl2;
	public double libDbl3;
	public long libDat1;
	public long libDat2;
	public long libDat3;
	public long libLng1;
	public long libLng2;
	public long libLng3;
}