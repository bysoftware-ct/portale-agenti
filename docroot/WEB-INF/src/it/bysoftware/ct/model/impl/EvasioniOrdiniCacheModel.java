/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.EvasioniOrdini;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing EvasioniOrdini in entity cache.
 *
 * @author Mario Torrisi
 * @see EvasioniOrdini
 * @generated
 */
public class EvasioniOrdiniCacheModel implements CacheModel<EvasioniOrdini>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(41);

		sb.append("{ID=");
		sb.append(ID);
		sb.append(", anno=");
		sb.append(anno);
		sb.append(", codiceAttivita=");
		sb.append(codiceAttivita);
		sb.append(", codiceCentro=");
		sb.append(codiceCentro);
		sb.append(", codiceDeposito=");
		sb.append(codiceDeposito);
		sb.append(", tipoOrdine=");
		sb.append(tipoOrdine);
		sb.append(", numeroOrdine=");
		sb.append(numeroOrdine);
		sb.append(", codiceCliente=");
		sb.append(codiceCliente);
		sb.append(", numeroRigo=");
		sb.append(numeroRigo);
		sb.append(", codiceArticolo=");
		sb.append(codiceArticolo);
		sb.append(", codiceVariante=");
		sb.append(codiceVariante);
		sb.append(", descrizione=");
		sb.append(descrizione);
		sb.append(", unitaMisura=");
		sb.append(unitaMisura);
		sb.append(", quantitaOrdinata=");
		sb.append(quantitaOrdinata);
		sb.append(", numeroProgrezzivoEvasione=");
		sb.append(numeroProgrezzivoEvasione);
		sb.append(", quantitaConsegnata=");
		sb.append(quantitaConsegnata);
		sb.append(", dataDocumentoConsegna=");
		sb.append(dataDocumentoConsegna);
		sb.append(", testAccontoSaldo=");
		sb.append(testAccontoSaldo);
		sb.append(", tipoDocumentoEvasione=");
		sb.append(tipoDocumentoEvasione);
		sb.append(", numeroDocumentoEvasione=");
		sb.append(numeroDocumentoEvasione);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public EvasioniOrdini toEntityModel() {
		EvasioniOrdiniImpl evasioniOrdiniImpl = new EvasioniOrdiniImpl();

		if (ID == null) {
			evasioniOrdiniImpl.setID(StringPool.BLANK);
		}
		else {
			evasioniOrdiniImpl.setID(ID);
		}

		evasioniOrdiniImpl.setAnno(anno);

		if (codiceAttivita == null) {
			evasioniOrdiniImpl.setCodiceAttivita(StringPool.BLANK);
		}
		else {
			evasioniOrdiniImpl.setCodiceAttivita(codiceAttivita);
		}

		if (codiceCentro == null) {
			evasioniOrdiniImpl.setCodiceCentro(StringPool.BLANK);
		}
		else {
			evasioniOrdiniImpl.setCodiceCentro(codiceCentro);
		}

		if (codiceDeposito == null) {
			evasioniOrdiniImpl.setCodiceDeposito(StringPool.BLANK);
		}
		else {
			evasioniOrdiniImpl.setCodiceDeposito(codiceDeposito);
		}

		evasioniOrdiniImpl.setTipoOrdine(tipoOrdine);
		evasioniOrdiniImpl.setNumeroOrdine(numeroOrdine);

		if (codiceCliente == null) {
			evasioniOrdiniImpl.setCodiceCliente(StringPool.BLANK);
		}
		else {
			evasioniOrdiniImpl.setCodiceCliente(codiceCliente);
		}

		evasioniOrdiniImpl.setNumeroRigo(numeroRigo);

		if (codiceArticolo == null) {
			evasioniOrdiniImpl.setCodiceArticolo(StringPool.BLANK);
		}
		else {
			evasioniOrdiniImpl.setCodiceArticolo(codiceArticolo);
		}

		if (codiceVariante == null) {
			evasioniOrdiniImpl.setCodiceVariante(StringPool.BLANK);
		}
		else {
			evasioniOrdiniImpl.setCodiceVariante(codiceVariante);
		}

		if (descrizione == null) {
			evasioniOrdiniImpl.setDescrizione(StringPool.BLANK);
		}
		else {
			evasioniOrdiniImpl.setDescrizione(descrizione);
		}

		if (unitaMisura == null) {
			evasioniOrdiniImpl.setUnitaMisura(StringPool.BLANK);
		}
		else {
			evasioniOrdiniImpl.setUnitaMisura(unitaMisura);
		}

		evasioniOrdiniImpl.setQuantitaOrdinata(quantitaOrdinata);
		evasioniOrdiniImpl.setNumeroProgrezzivoEvasione(numeroProgrezzivoEvasione);
		evasioniOrdiniImpl.setQuantitaConsegnata(quantitaConsegnata);

		if (dataDocumentoConsegna == Long.MIN_VALUE) {
			evasioniOrdiniImpl.setDataDocumentoConsegna(null);
		}
		else {
			evasioniOrdiniImpl.setDataDocumentoConsegna(new Date(
					dataDocumentoConsegna));
		}

		if (testAccontoSaldo == null) {
			evasioniOrdiniImpl.setTestAccontoSaldo(StringPool.BLANK);
		}
		else {
			evasioniOrdiniImpl.setTestAccontoSaldo(testAccontoSaldo);
		}

		if (tipoDocumentoEvasione == null) {
			evasioniOrdiniImpl.setTipoDocumentoEvasione(StringPool.BLANK);
		}
		else {
			evasioniOrdiniImpl.setTipoDocumentoEvasione(tipoDocumentoEvasione);
		}

		evasioniOrdiniImpl.setNumeroDocumentoEvasione(numeroDocumentoEvasione);

		evasioniOrdiniImpl.resetOriginalValues();

		return evasioniOrdiniImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ID = objectInput.readUTF();
		anno = objectInput.readInt();
		codiceAttivita = objectInput.readUTF();
		codiceCentro = objectInput.readUTF();
		codiceDeposito = objectInput.readUTF();
		tipoOrdine = objectInput.readInt();
		numeroOrdine = objectInput.readInt();
		codiceCliente = objectInput.readUTF();
		numeroRigo = objectInput.readInt();
		codiceArticolo = objectInput.readUTF();
		codiceVariante = objectInput.readUTF();
		descrizione = objectInput.readUTF();
		unitaMisura = objectInput.readUTF();
		quantitaOrdinata = objectInput.readDouble();
		numeroProgrezzivoEvasione = objectInput.readInt();
		quantitaConsegnata = objectInput.readDouble();
		dataDocumentoConsegna = objectInput.readLong();
		testAccontoSaldo = objectInput.readUTF();
		tipoDocumentoEvasione = objectInput.readUTF();
		numeroDocumentoEvasione = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (ID == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ID);
		}

		objectOutput.writeInt(anno);

		if (codiceAttivita == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceAttivita);
		}

		if (codiceCentro == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCentro);
		}

		if (codiceDeposito == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDeposito);
		}

		objectOutput.writeInt(tipoOrdine);
		objectOutput.writeInt(numeroOrdine);

		if (codiceCliente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceCliente);
		}

		objectOutput.writeInt(numeroRigo);

		if (codiceArticolo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceArticolo);
		}

		if (codiceVariante == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceVariante);
		}

		if (descrizione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizione);
		}

		if (unitaMisura == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(unitaMisura);
		}

		objectOutput.writeDouble(quantitaOrdinata);
		objectOutput.writeInt(numeroProgrezzivoEvasione);
		objectOutput.writeDouble(quantitaConsegnata);
		objectOutput.writeLong(dataDocumentoConsegna);

		if (testAccontoSaldo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(testAccontoSaldo);
		}

		if (tipoDocumentoEvasione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoDocumentoEvasione);
		}

		objectOutput.writeInt(numeroDocumentoEvasione);
	}

	public String ID;
	public int anno;
	public String codiceAttivita;
	public String codiceCentro;
	public String codiceDeposito;
	public int tipoOrdine;
	public int numeroOrdine;
	public String codiceCliente;
	public int numeroRigo;
	public String codiceArticolo;
	public String codiceVariante;
	public String descrizione;
	public String unitaMisura;
	public double quantitaOrdinata;
	public int numeroProgrezzivoEvasione;
	public double quantitaConsegnata;
	public long dataDocumentoConsegna;
	public String testAccontoSaldo;
	public String tipoDocumentoEvasione;
	public int numeroDocumentoEvasione;
}