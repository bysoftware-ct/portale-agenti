/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.AnagraficaAgenti;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing AnagraficaAgenti in entity cache.
 *
 * @author Mario Torrisi
 * @see AnagraficaAgenti
 * @generated
 */
public class AnagraficaAgentiCacheModel implements CacheModel<AnagraficaAgenti>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{codiceAgente=");
		sb.append(codiceAgente);
		sb.append(", nome=");
		sb.append(nome);
		sb.append(", codiceZona=");
		sb.append(codiceZona);
		sb.append(", codiceProvvRigo=");
		sb.append(codiceProvvRigo);
		sb.append(", codiceProvvChiusura=");
		sb.append(codiceProvvChiusura);
		sb.append(", budget=");
		sb.append(budget);
		sb.append(", tipoLiquidazione=");
		sb.append(tipoLiquidazione);
		sb.append(", giorniToll=");
		sb.append(giorniToll);
		sb.append(", tipoDivisa=");
		sb.append(tipoDivisa);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public AnagraficaAgenti toEntityModel() {
		AnagraficaAgentiImpl anagraficaAgentiImpl = new AnagraficaAgentiImpl();

		if (codiceAgente == null) {
			anagraficaAgentiImpl.setCodiceAgente(StringPool.BLANK);
		}
		else {
			anagraficaAgentiImpl.setCodiceAgente(codiceAgente);
		}

		if (nome == null) {
			anagraficaAgentiImpl.setNome(StringPool.BLANK);
		}
		else {
			anagraficaAgentiImpl.setNome(nome);
		}

		if (codiceZona == null) {
			anagraficaAgentiImpl.setCodiceZona(StringPool.BLANK);
		}
		else {
			anagraficaAgentiImpl.setCodiceZona(codiceZona);
		}

		if (codiceProvvRigo == null) {
			anagraficaAgentiImpl.setCodiceProvvRigo(StringPool.BLANK);
		}
		else {
			anagraficaAgentiImpl.setCodiceProvvRigo(codiceProvvRigo);
		}

		if (codiceProvvChiusura == null) {
			anagraficaAgentiImpl.setCodiceProvvChiusura(StringPool.BLANK);
		}
		else {
			anagraficaAgentiImpl.setCodiceProvvChiusura(codiceProvvChiusura);
		}

		anagraficaAgentiImpl.setBudget(budget);
		anagraficaAgentiImpl.setTipoLiquidazione(tipoLiquidazione);
		anagraficaAgentiImpl.setGiorniToll(giorniToll);
		anagraficaAgentiImpl.setTipoDivisa(tipoDivisa);

		anagraficaAgentiImpl.resetOriginalValues();

		return anagraficaAgentiImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		codiceAgente = objectInput.readUTF();
		nome = objectInput.readUTF();
		codiceZona = objectInput.readUTF();
		codiceProvvRigo = objectInput.readUTF();
		codiceProvvChiusura = objectInput.readUTF();
		budget = objectInput.readDouble();
		tipoLiquidazione = objectInput.readInt();
		giorniToll = objectInput.readInt();
		tipoDivisa = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (codiceAgente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceAgente);
		}

		if (nome == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nome);
		}

		if (codiceZona == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceZona);
		}

		if (codiceProvvRigo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceProvvRigo);
		}

		if (codiceProvvChiusura == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceProvvChiusura);
		}

		objectOutput.writeDouble(budget);
		objectOutput.writeInt(tipoLiquidazione);
		objectOutput.writeInt(giorniToll);
		objectOutput.writeBoolean(tipoDivisa);
	}

	public String codiceAgente;
	public String nome;
	public String codiceZona;
	public String codiceProvvRigo;
	public String codiceProvvChiusura;
	public double budget;
	public int tipoLiquidazione;
	public int giorniToll;
	public boolean tipoDivisa;
}