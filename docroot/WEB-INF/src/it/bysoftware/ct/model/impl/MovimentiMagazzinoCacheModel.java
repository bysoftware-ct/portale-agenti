/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.MovimentiMagazzino;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing MovimentiMagazzino in entity cache.
 *
 * @author Mario Torrisi
 * @see MovimentiMagazzino
 * @generated
 */
public class MovimentiMagazzinoCacheModel implements CacheModel<MovimentiMagazzino>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(35);

		sb.append("{ID=");
		sb.append(ID);
		sb.append(", anno=");
		sb.append(anno);
		sb.append(", codiceArticolo=");
		sb.append(codiceArticolo);
		sb.append(", codiceVariante=");
		sb.append(codiceVariante);
		sb.append(", descrizione=");
		sb.append(descrizione);
		sb.append(", unitaMisura=");
		sb.append(unitaMisura);
		sb.append(", quantita=");
		sb.append(quantita);
		sb.append(", testCaricoScarico=");
		sb.append(testCaricoScarico);
		sb.append(", dataRegistrazione=");
		sb.append(dataRegistrazione);
		sb.append(", tipoSoggetto=");
		sb.append(tipoSoggetto);
		sb.append(", codiceSoggetto=");
		sb.append(codiceSoggetto);
		sb.append(", ragioneSociale=");
		sb.append(ragioneSociale);
		sb.append(", estremiDocumenti=");
		sb.append(estremiDocumenti);
		sb.append(", dataDocumento=");
		sb.append(dataDocumento);
		sb.append(", numeroDocumento=");
		sb.append(numeroDocumento);
		sb.append(", tipoDocumento=");
		sb.append(tipoDocumento);
		sb.append(", soloValore=");
		sb.append(soloValore);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public MovimentiMagazzino toEntityModel() {
		MovimentiMagazzinoImpl movimentiMagazzinoImpl = new MovimentiMagazzinoImpl();

		if (ID == null) {
			movimentiMagazzinoImpl.setID(StringPool.BLANK);
		}
		else {
			movimentiMagazzinoImpl.setID(ID);
		}

		movimentiMagazzinoImpl.setAnno(anno);

		if (codiceArticolo == null) {
			movimentiMagazzinoImpl.setCodiceArticolo(StringPool.BLANK);
		}
		else {
			movimentiMagazzinoImpl.setCodiceArticolo(codiceArticolo);
		}

		if (codiceVariante == null) {
			movimentiMagazzinoImpl.setCodiceVariante(StringPool.BLANK);
		}
		else {
			movimentiMagazzinoImpl.setCodiceVariante(codiceVariante);
		}

		if (descrizione == null) {
			movimentiMagazzinoImpl.setDescrizione(StringPool.BLANK);
		}
		else {
			movimentiMagazzinoImpl.setDescrizione(descrizione);
		}

		if (unitaMisura == null) {
			movimentiMagazzinoImpl.setUnitaMisura(StringPool.BLANK);
		}
		else {
			movimentiMagazzinoImpl.setUnitaMisura(unitaMisura);
		}

		movimentiMagazzinoImpl.setQuantita(quantita);
		movimentiMagazzinoImpl.setTestCaricoScarico(testCaricoScarico);

		if (dataRegistrazione == Long.MIN_VALUE) {
			movimentiMagazzinoImpl.setDataRegistrazione(null);
		}
		else {
			movimentiMagazzinoImpl.setDataRegistrazione(new Date(
					dataRegistrazione));
		}

		movimentiMagazzinoImpl.setTipoSoggetto(tipoSoggetto);

		if (codiceSoggetto == null) {
			movimentiMagazzinoImpl.setCodiceSoggetto(StringPool.BLANK);
		}
		else {
			movimentiMagazzinoImpl.setCodiceSoggetto(codiceSoggetto);
		}

		if (ragioneSociale == null) {
			movimentiMagazzinoImpl.setRagioneSociale(StringPool.BLANK);
		}
		else {
			movimentiMagazzinoImpl.setRagioneSociale(ragioneSociale);
		}

		if (estremiDocumenti == null) {
			movimentiMagazzinoImpl.setEstremiDocumenti(StringPool.BLANK);
		}
		else {
			movimentiMagazzinoImpl.setEstremiDocumenti(estremiDocumenti);
		}

		if (dataDocumento == Long.MIN_VALUE) {
			movimentiMagazzinoImpl.setDataDocumento(null);
		}
		else {
			movimentiMagazzinoImpl.setDataDocumento(new Date(dataDocumento));
		}

		movimentiMagazzinoImpl.setNumeroDocumento(numeroDocumento);

		if (tipoDocumento == null) {
			movimentiMagazzinoImpl.setTipoDocumento(StringPool.BLANK);
		}
		else {
			movimentiMagazzinoImpl.setTipoDocumento(tipoDocumento);
		}

		movimentiMagazzinoImpl.setSoloValore(soloValore);

		movimentiMagazzinoImpl.resetOriginalValues();

		return movimentiMagazzinoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ID = objectInput.readUTF();
		anno = objectInput.readInt();
		codiceArticolo = objectInput.readUTF();
		codiceVariante = objectInput.readUTF();
		descrizione = objectInput.readUTF();
		unitaMisura = objectInput.readUTF();
		quantita = objectInput.readDouble();
		testCaricoScarico = objectInput.readInt();
		dataRegistrazione = objectInput.readLong();
		tipoSoggetto = objectInput.readBoolean();
		codiceSoggetto = objectInput.readUTF();
		ragioneSociale = objectInput.readUTF();
		estremiDocumenti = objectInput.readUTF();
		dataDocumento = objectInput.readLong();
		numeroDocumento = objectInput.readInt();
		tipoDocumento = objectInput.readUTF();
		soloValore = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (ID == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ID);
		}

		objectOutput.writeInt(anno);

		if (codiceArticolo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceArticolo);
		}

		if (codiceVariante == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceVariante);
		}

		if (descrizione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizione);
		}

		if (unitaMisura == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(unitaMisura);
		}

		objectOutput.writeDouble(quantita);
		objectOutput.writeInt(testCaricoScarico);
		objectOutput.writeLong(dataRegistrazione);
		objectOutput.writeBoolean(tipoSoggetto);

		if (codiceSoggetto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceSoggetto);
		}

		if (ragioneSociale == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ragioneSociale);
		}

		if (estremiDocumenti == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(estremiDocumenti);
		}

		objectOutput.writeLong(dataDocumento);
		objectOutput.writeInt(numeroDocumento);

		if (tipoDocumento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoDocumento);
		}

		objectOutput.writeInt(soloValore);
	}

	public String ID;
	public int anno;
	public String codiceArticolo;
	public String codiceVariante;
	public String descrizione;
	public String unitaMisura;
	public Double quantita;
	public int testCaricoScarico;
	public long dataRegistrazione;
	public boolean tipoSoggetto;
	public String codiceSoggetto;
	public String ragioneSociale;
	public String estremiDocumenti;
	public long dataDocumento;
	public int numeroDocumento;
	public String tipoDocumento;
	public int soloValore;
}