/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.bysoftware.ct.model.ListiniPrezziArticoli;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ListiniPrezziArticoli in entity cache.
 *
 * @author Mario Torrisi
 * @see ListiniPrezziArticoli
 * @generated
 */
public class ListiniPrezziArticoliCacheModel implements CacheModel<ListiniPrezziArticoli>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(35);

		sb.append("{codiceListino=");
		sb.append(codiceListino);
		sb.append(", codiceDivisa=");
		sb.append(codiceDivisa);
		sb.append(", tipoSoggetto=");
		sb.append(tipoSoggetto);
		sb.append(", codiceSoggetto=");
		sb.append(codiceSoggetto);
		sb.append(", dataInizioValidita=");
		sb.append(dataInizioValidita);
		sb.append(", codiceArticolo=");
		sb.append(codiceArticolo);
		sb.append(", codiceVariante=");
		sb.append(codiceVariante);
		sb.append(", codiceLavorazione=");
		sb.append(codiceLavorazione);
		sb.append(", quantInizioValiditaPrezzo=");
		sb.append(quantInizioValiditaPrezzo);
		sb.append(", dataFineValidita=");
		sb.append(dataFineValidita);
		sb.append(", quantFineValiditaPrezzo=");
		sb.append(quantFineValiditaPrezzo);
		sb.append(", prezzoNettoIVA=");
		sb.append(prezzoNettoIVA);
		sb.append(", codiceIVA=");
		sb.append(codiceIVA);
		sb.append(", utilizzoSconti=");
		sb.append(utilizzoSconti);
		sb.append(", scontoListino1=");
		sb.append(scontoListino1);
		sb.append(", scontoListino2=");
		sb.append(scontoListino2);
		sb.append(", scontoListino3=");
		sb.append(scontoListino3);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ListiniPrezziArticoli toEntityModel() {
		ListiniPrezziArticoliImpl listiniPrezziArticoliImpl = new ListiniPrezziArticoliImpl();

		if (codiceListino == null) {
			listiniPrezziArticoliImpl.setCodiceListino(StringPool.BLANK);
		}
		else {
			listiniPrezziArticoliImpl.setCodiceListino(codiceListino);
		}

		if (codiceDivisa == null) {
			listiniPrezziArticoliImpl.setCodiceDivisa(StringPool.BLANK);
		}
		else {
			listiniPrezziArticoliImpl.setCodiceDivisa(codiceDivisa);
		}

		listiniPrezziArticoliImpl.setTipoSoggetto(tipoSoggetto);

		if (codiceSoggetto == null) {
			listiniPrezziArticoliImpl.setCodiceSoggetto(StringPool.BLANK);
		}
		else {
			listiniPrezziArticoliImpl.setCodiceSoggetto(codiceSoggetto);
		}

		if (dataInizioValidita == Long.MIN_VALUE) {
			listiniPrezziArticoliImpl.setDataInizioValidita(null);
		}
		else {
			listiniPrezziArticoliImpl.setDataInizioValidita(new Date(
					dataInizioValidita));
		}

		if (codiceArticolo == null) {
			listiniPrezziArticoliImpl.setCodiceArticolo(StringPool.BLANK);
		}
		else {
			listiniPrezziArticoliImpl.setCodiceArticolo(codiceArticolo);
		}

		if (codiceVariante == null) {
			listiniPrezziArticoliImpl.setCodiceVariante(StringPool.BLANK);
		}
		else {
			listiniPrezziArticoliImpl.setCodiceVariante(codiceVariante);
		}

		if (codiceLavorazione == null) {
			listiniPrezziArticoliImpl.setCodiceLavorazione(StringPool.BLANK);
		}
		else {
			listiniPrezziArticoliImpl.setCodiceLavorazione(codiceLavorazione);
		}

		listiniPrezziArticoliImpl.setQuantInizioValiditaPrezzo(quantInizioValiditaPrezzo);

		if (dataFineValidita == Long.MIN_VALUE) {
			listiniPrezziArticoliImpl.setDataFineValidita(null);
		}
		else {
			listiniPrezziArticoliImpl.setDataFineValidita(new Date(
					dataFineValidita));
		}

		listiniPrezziArticoliImpl.setQuantFineValiditaPrezzo(quantFineValiditaPrezzo);
		listiniPrezziArticoliImpl.setPrezzoNettoIVA(prezzoNettoIVA);

		if (codiceIVA == null) {
			listiniPrezziArticoliImpl.setCodiceIVA(StringPool.BLANK);
		}
		else {
			listiniPrezziArticoliImpl.setCodiceIVA(codiceIVA);
		}

		listiniPrezziArticoliImpl.setUtilizzoSconti(utilizzoSconti);
		listiniPrezziArticoliImpl.setScontoListino1(scontoListino1);
		listiniPrezziArticoliImpl.setScontoListino2(scontoListino2);
		listiniPrezziArticoliImpl.setScontoListino3(scontoListino3);

		listiniPrezziArticoliImpl.resetOriginalValues();

		return listiniPrezziArticoliImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		codiceListino = objectInput.readUTF();
		codiceDivisa = objectInput.readUTF();
		tipoSoggetto = objectInput.readInt();
		codiceSoggetto = objectInput.readUTF();
		dataInizioValidita = objectInput.readLong();
		codiceArticolo = objectInput.readUTF();
		codiceVariante = objectInput.readUTF();
		codiceLavorazione = objectInput.readUTF();
		quantInizioValiditaPrezzo = objectInput.readDouble();
		dataFineValidita = objectInput.readLong();
		quantFineValiditaPrezzo = objectInput.readDouble();
		prezzoNettoIVA = objectInput.readDouble();
		codiceIVA = objectInput.readUTF();
		utilizzoSconti = objectInput.readBoolean();
		scontoListino1 = objectInput.readDouble();
		scontoListino2 = objectInput.readDouble();
		scontoListino3 = objectInput.readDouble();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (codiceListino == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceListino);
		}

		if (codiceDivisa == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceDivisa);
		}

		objectOutput.writeInt(tipoSoggetto);

		if (codiceSoggetto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceSoggetto);
		}

		objectOutput.writeLong(dataInizioValidita);

		if (codiceArticolo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceArticolo);
		}

		if (codiceVariante == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceVariante);
		}

		if (codiceLavorazione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceLavorazione);
		}

		objectOutput.writeDouble(quantInizioValiditaPrezzo);
		objectOutput.writeLong(dataFineValidita);
		objectOutput.writeDouble(quantFineValiditaPrezzo);
		objectOutput.writeDouble(prezzoNettoIVA);

		if (codiceIVA == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codiceIVA);
		}

		objectOutput.writeBoolean(utilizzoSconti);
		objectOutput.writeDouble(scontoListino1);
		objectOutput.writeDouble(scontoListino2);
		objectOutput.writeDouble(scontoListino3);
	}

	public String codiceListino;
	public String codiceDivisa;
	public int tipoSoggetto;
	public String codiceSoggetto;
	public long dataInizioValidita;
	public String codiceArticolo;
	public String codiceVariante;
	public String codiceLavorazione;
	public double quantInizioValiditaPrezzo;
	public long dataFineValidita;
	public double quantFineValiditaPrezzo;
	public double prezzoNettoIVA;
	public String codiceIVA;
	public boolean utilizzoSconti;
	public double scontoListino1;
	public double scontoListino2;
	public double scontoListino3;
}