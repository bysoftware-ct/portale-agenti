/***********************************************************************
 * Copyright (c) 2016:
 * BySoftware Soc. coop. a.r.l., Belpasso (CT) - Italy
 *
 * See http://www.bysoftware.ct.it for details on the copyright holder.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***********************************************************************/
package it.bysoftware.ct.utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * 
 * @author mario
 */
public class Report {

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(Report.class);

    /**
     * Compiled report folder name.
     */
    private static final String JASPER_REPORT_FOLDER = "/home/mario/ITS";

    /**
     * Compiled report file name.
     */
    private static final String JASPER_FILENAME = "order";

    /**
     * Connection taken from data source.
     */
    private Connection conn;

    /**
     * Create an object to print report files in PDF format.
     * 
     * @param c
     *            the connection {@link Connection}
     */
    public Report(final Connection c) {
	this.conn = c;
    }

    /**
     * Prints the report.
     * 
     * @param idAzienda
     *            company Id to get folder containing jasper files.
     * @param logo
     *            logo name
     * @param codiceCliente
     *            customer code
     * @param anno
     *            report year
     * @param tipo
     *            type of order
     * @param numero
     *            order number
     * @return the path to the just created PDF report
     * @throws JRException
     *             exception
     * @throws ClassNotFoundException
     *             exception
     * @throws SQLException
     *             exception
     */
    public final String print(final long idAzienda, final String logo,
	    final String codiceCliente, final int anno, final int tipo,
	    final int numero) throws JRException, ClassNotFoundException,
	    SQLException {
	Map<String, Object> parametersMap = new HashMap<String, Object>();
	parametersMap.put("logo", logo + ".jpg");
	parametersMap.put("anno", (short) anno);
	parametersMap.put("tipo", (short) tipo);
	parametersMap.put("numero", numero);

	this.logger.debug("****" + JASPER_REPORT_FOLDER + "/" + idAzienda + "/"
		+ JASPER_FILENAME + ".jasper");
	// caricamento file JRXML
	// JasperDesign jasperDesign = JRXmlLoader.load(path + "/"
	// + JASPER_FILENAME + ".jrxml");
	// // compilazione del file e generazione del file JASPER
	// JasperCompileManager.compileReportToFile(jasperDesign,
	// path + "/" + JASPER_FILENAME + ".jasper");

	// rendering e generazione del file PDF
	JasperPrint jp = JasperFillManager.fillReport(JASPER_REPORT_FOLDER
		+ "/" + idAzienda + "/" + JASPER_FILENAME + ".jasper",
		parametersMap, conn);
	JasperExportManager.exportReportToPdfFile(jp, "/tmp/" + codiceCliente
		+ "_" + anno + "_" + numero + ".pdf");

	return "/tmp/" + codiceCliente + "_" + anno + "_" + numero + ".pdf";

    }
}
