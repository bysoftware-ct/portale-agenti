/***********************************************************************
 * Copyright (c) 2016:
 * BySoftware Soc. coop. a.r.l., Belpasso (CT) - Italy
 *
 * See http://www.bysoftware.ct.it for details on the copyright holder.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***********************************************************************/

package it.bysoftware.ct.utils;

/**
 * This class contains all portlets constants. 
 *
 * @author Mario Torrisi torrisi.mario@gmail.com
 */

public final class Constants {
    /**
     * Key to look for Codice Agente in ExpandoTable.
     */
    public static final String CODICE_AGENTE = "CodiceAgente";
    /**
     * Key to look for Codice Azienda in ExpandoTable.
     */
    public static final String CODICE_AZIENDA = "CodiceAzienda";
    
    /**
     * Client order type.
     */
    public static final int TIPO_ORDINE = 5;
        
    /**
     * Separator used in exported file.
     */
    public static final String SEPARATOR = "\";\"";
    
    /**
     * Use default String value.
     */
    public static final String STRING_DEFAULT = "_~";

    /**
     * Use default Document type.
     */
    public static final String TIPO_DOCUMENTO = "OAC";
    
    /**
     * 1GB constant.
     */
    public static final int ONE_GB = 1073741824;
    
    /**
     * Item sheet folder name.
     */
    public static final String ITEM_SHEET_FOLDER_NAME = "schede-tecniche";
    
    /**
     * Agent constant.
     */
    public static final String AGENTE = "agente";
    
    /**
     * Customer blocked with alert.
     */
    public static final int BLOCKED_ALERT = 1;
    
    /**
     * Customer blocked.
     */
    public static final int BLOCKED = 2;
    
    /**
     * Obsolete customer.
     */
    public static final int OBSOLETE = 3;
        
    /**
     * Avoid the class be instantiable.
     */
    private Constants() { }
}
