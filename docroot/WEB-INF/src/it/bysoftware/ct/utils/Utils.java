/***********************************************************************
 * Copyright (c) 2016:
 * BySoftware Soc. coop. a.r.l., Belpasso (CT) - Italy
 *
 * See http://www.bysoftware.ct.it for details on the copyright holder.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***********************************************************************/

package it.bysoftware.ct.utils;

import java.util.Calendar;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ResourceAction;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.ResourcePermission;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.ResourceActionLocalServiceUtil;
import com.liferay.portal.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.model.DLFolderConstants;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;

/**
 * Provide some Utilities to deal with Document Library.
 * 
 * @author Mario Torrisi torrisi.mario@gmail.com
 *
 */
public final class Utils {

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private static final Log LOGGER = LogFactoryUtil.getLog(Utils.class);

    /**
     * Default constructor to avoid class instatiation.
     */
    private Utils() {
    }

    /**
     * Returns current year.
     * 
     * @return current year
     */
    public static int getYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    /**
     * Returns Azienda {@link DLFolder} folder.
     * 
     * @param groupId
     *            azienda groupId
     * @param azienda
     *            {@link User} user
     * @return the {@link DLFolder}
     * @throws PortalException
     *             if {@link PortalException}
     * @throws SystemException
     *             if {@link SystemException}
     */
    public static DLFolder getAziendaFolder(final long groupId,
            final User azienda) throws PortalException, SystemException {
        return DLFolderLocalServiceUtil.getFolder(groupId,
                DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,
                azienda.getScreenName());
    }

    /**
     * Returns Agent {@link DLFolder} folder.
     * 
     * @param groupId
     *            agent grouId
     * @param aziendaFolder
     *            azienda {@link DLFolder} folder
     * @param agente
     *            agent {@link User} user
     * @return the {@link DLFolder}
     * @throws PortalException
     *             if {@link PortalException}
     * @throws SystemException
     *             if {@link SystemException}
     */
    public static DLFolder getAgenteFolder(final long groupId,
            final DLFolder aziendaFolder, final User agente)
            throws PortalException, SystemException {
        return DLFolderLocalServiceUtil.getFolder(groupId,
                aziendaFolder.getFolderId(), agente.getScreenName());
    }

    /**
     * Returns Agent {@link DLFolder} year folder.
     * 
     * @param groupId
     *            agent grouId
     * @param agenteFolder
     *            agent {@link DLFolder} folder
     * @param year
     *            current year
     * @return the {@link DLFolder}
     * @throws PortalException
     *             if {@link PortalException}
     * @throws SystemException
     *             if {@link SystemException}
     */
    public static DLFolder getAgenteYearFolder(final long groupId,
            final DLFolder agenteFolder, final int year)
            throws PortalException, SystemException {

        try {
            return DLFolderLocalServiceUtil.getFolder(groupId,
                    agenteFolder.getFolderId(), String.valueOf(year));
        } catch (PortalException | SystemException e) {
            LOGGER.warn("Year folder doesn'exist, creating a new year folder"
                    + " for the agent...");
            return DLFolderLocalServiceUtil.addFolder(agenteFolder.getUserId(),
                    groupId, agenteFolder.getRepositoryId(), false,
                    agenteFolder.getFolderId(),
                    String.valueOf(Calendar.getInstance().get(Calendar.YEAR)),
                    "Directory di " + agenteFolder.getUserName(), false,
                    new ServiceContext());
        }

    }

    /**
     * Returns Item Sheet {@link DLFolder} folder.
     * 
     * @param groupId
     *            agent grouId
     * @param aziendaFolder
     *            azienda {@link DLFolder} folder
     * @return the {@link DLFolder}
     * @throws PortalException
     *             if {@link PortalException}
     * @throws SystemException
     *             if {@link SystemException}
     */
    public static DLFolder getgetItemSheetFolder(final long groupId,
            final DLFolder aziendaFolder) throws PortalException,
            SystemException {
        return DLFolderLocalServiceUtil.getFolder(groupId,
                aziendaFolder.getFolderId(), Constants.ITEM_SHEET_FOLDER_NAME);
    }

    /**
     * Sets {@link ActionKeys.VIEW} permission to the the file.
     * 
     * @param fileEntry
     *            the file
     * @throws SystemException system exception
     * @throws PortalException portal exception
     * @throws Exception exception
     */
    public static void setFilePermissions(final FileEntry fileEntry)
            throws PortalException, SystemException {
        ResourcePermission resourcePermission = null;
        final Role siteMemberRole = RoleLocalServiceUtil.getRole(
                fileEntry.getCompanyId(), Constants.AGENTE);
        ResourceAction resourceAction = ResourceActionLocalServiceUtil
                .getResourceAction(DLFileEntry.class.getName(),
                        ActionKeys.VIEW);
        try {
            resourcePermission = ResourcePermissionLocalServiceUtil
                    .getResourcePermission(fileEntry.getCompanyId(),
                            DLFileEntry.class.getName(),
                            ResourceConstants.SCOPE_INDIVIDUAL,
                            String.valueOf(fileEntry.getPrimaryKey()),
                            siteMemberRole.getRoleId());

            if (Validator.isNotNull(resourcePermission)) {

                resourcePermission.setActionIds(resourceAction
                        .getBitwiseValue());
                ResourcePermissionLocalServiceUtil
                        .updateResourcePermission(resourcePermission);
            }
        } catch (com.liferay.portal.NoSuchResourcePermissionException e) {

            resourcePermission = ResourcePermissionLocalServiceUtil
                    .createResourcePermission(CounterLocalServiceUtil
                            .increment());
            resourcePermission.setCompanyId(fileEntry.getCompanyId());
            resourcePermission.setName(DLFileEntry.class.getName());
            resourcePermission.setScope(ResourceConstants.SCOPE_INDIVIDUAL);
            resourcePermission.setPrimKey(String.valueOf(fileEntry
                    .getPrimaryKey()));
            resourcePermission.setRoleId(siteMemberRole.getRoleId());
            resourcePermission.setActionIds(resourceAction.getBitwiseValue());
            ResourcePermissionLocalServiceUtil
                    .addResourcePermission(resourcePermission);
        }
    }
}
