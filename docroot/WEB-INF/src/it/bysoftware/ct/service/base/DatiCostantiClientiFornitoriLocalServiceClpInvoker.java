/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.base;

import it.bysoftware.ct.service.DatiCostantiClientiFornitoriLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Mario Torrisi
 * @generated
 */
public class DatiCostantiClientiFornitoriLocalServiceClpInvoker {
	public DatiCostantiClientiFornitoriLocalServiceClpInvoker() {
		_methodName0 = "addDatiCostantiClientiFornitori";

		_methodParameterTypes0 = new String[] {
				"it.bysoftware.ct.model.DatiCostantiClientiFornitori"
			};

		_methodName1 = "createDatiCostantiClientiFornitori";

		_methodParameterTypes1 = new String[] {
				"it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK"
			};

		_methodName2 = "deleteDatiCostantiClientiFornitori";

		_methodParameterTypes2 = new String[] {
				"it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK"
			};

		_methodName3 = "deleteDatiCostantiClientiFornitori";

		_methodParameterTypes3 = new String[] {
				"it.bysoftware.ct.model.DatiCostantiClientiFornitori"
			};

		_methodName4 = "dynamicQuery";

		_methodParameterTypes4 = new String[] {  };

		_methodName5 = "dynamicQuery";

		_methodParameterTypes5 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName6 = "dynamicQuery";

		_methodParameterTypes6 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
			};

		_methodName7 = "dynamicQuery";

		_methodParameterTypes7 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
				"com.liferay.portal.kernel.util.OrderByComparator"
			};

		_methodName8 = "dynamicQueryCount";

		_methodParameterTypes8 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName9 = "dynamicQueryCount";

		_methodParameterTypes9 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery",
				"com.liferay.portal.kernel.dao.orm.Projection"
			};

		_methodName10 = "fetchDatiCostantiClientiFornitori";

		_methodParameterTypes10 = new String[] {
				"it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK"
			};

		_methodName11 = "getDatiCostantiClientiFornitori";

		_methodParameterTypes11 = new String[] {
				"it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK"
			};

		_methodName12 = "getPersistedModel";

		_methodParameterTypes12 = new String[] { "java.io.Serializable" };

		_methodName13 = "getDatiCostantiClientiFornitoris";

		_methodParameterTypes13 = new String[] { "int", "int" };

		_methodName14 = "getDatiCostantiClientiFornitorisCount";

		_methodParameterTypes14 = new String[] {  };

		_methodName15 = "updateDatiCostantiClientiFornitori";

		_methodParameterTypes15 = new String[] {
				"it.bysoftware.ct.model.DatiCostantiClientiFornitori"
			};

		_methodName154 = "getBeanIdentifier";

		_methodParameterTypes154 = new String[] {  };

		_methodName155 = "setBeanIdentifier";

		_methodParameterTypes155 = new String[] { "java.lang.String" };
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		if (_methodName0.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.addDatiCostantiClientiFornitori((it.bysoftware.ct.model.DatiCostantiClientiFornitori)arguments[0]);
		}

		if (_methodName1.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.createDatiCostantiClientiFornitori((it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK)arguments[0]);
		}

		if (_methodName2.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.deleteDatiCostantiClientiFornitori((it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK)arguments[0]);
		}

		if (_methodName3.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.deleteDatiCostantiClientiFornitori((it.bysoftware.ct.model.DatiCostantiClientiFornitori)arguments[0]);
		}

		if (_methodName4.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.dynamicQuery();
		}

		if (_methodName5.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName6.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName7.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue(),
				(com.liferay.portal.kernel.util.OrderByComparator)arguments[3]);
		}

		if (_methodName8.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName9.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				(com.liferay.portal.kernel.dao.orm.Projection)arguments[1]);
		}

		if (_methodName10.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.fetchDatiCostantiClientiFornitori((it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK)arguments[0]);
		}

		if (_methodName11.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.getDatiCostantiClientiFornitori((it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK)arguments[0]);
		}

		if (_methodName12.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.getPersistedModel((java.io.Serializable)arguments[0]);
		}

		if (_methodName13.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.getDatiCostantiClientiFornitoris(((Integer)arguments[0]).intValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName14.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.getDatiCostantiClientiFornitorisCount();
		}

		if (_methodName15.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.updateDatiCostantiClientiFornitori((it.bysoftware.ct.model.DatiCostantiClientiFornitori)arguments[0]);
		}

		if (_methodName154.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes154, parameterTypes)) {
			return DatiCostantiClientiFornitoriLocalServiceUtil.getBeanIdentifier();
		}

		if (_methodName155.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes155, parameterTypes)) {
			DatiCostantiClientiFornitoriLocalServiceUtil.setBeanIdentifier((java.lang.String)arguments[0]);

			return null;
		}

		throw new UnsupportedOperationException();
	}

	private String _methodName0;
	private String[] _methodParameterTypes0;
	private String _methodName1;
	private String[] _methodParameterTypes1;
	private String _methodName2;
	private String[] _methodParameterTypes2;
	private String _methodName3;
	private String[] _methodParameterTypes3;
	private String _methodName4;
	private String[] _methodParameterTypes4;
	private String _methodName5;
	private String[] _methodParameterTypes5;
	private String _methodName6;
	private String[] _methodParameterTypes6;
	private String _methodName7;
	private String[] _methodParameterTypes7;
	private String _methodName8;
	private String[] _methodParameterTypes8;
	private String _methodName9;
	private String[] _methodParameterTypes9;
	private String _methodName10;
	private String[] _methodParameterTypes10;
	private String _methodName11;
	private String[] _methodParameterTypes11;
	private String _methodName12;
	private String[] _methodParameterTypes12;
	private String _methodName13;
	private String[] _methodParameterTypes13;
	private String _methodName14;
	private String[] _methodParameterTypes14;
	private String _methodName15;
	private String[] _methodParameterTypes15;
	private String _methodName154;
	private String[] _methodParameterTypes154;
	private String _methodName155;
	private String[] _methodParameterTypes155;
}