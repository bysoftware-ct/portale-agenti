/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.base;

import it.bysoftware.ct.service.WKOrdiniClientiLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Mario Torrisi
 * @generated
 */
public class WKOrdiniClientiLocalServiceClpInvoker {
	public WKOrdiniClientiLocalServiceClpInvoker() {
		_methodName0 = "addWKOrdiniClienti";

		_methodParameterTypes0 = new String[] {
				"it.bysoftware.ct.model.WKOrdiniClienti"
			};

		_methodName1 = "createWKOrdiniClienti";

		_methodParameterTypes1 = new String[] {
				"it.bysoftware.ct.service.persistence.WKOrdiniClientiPK"
			};

		_methodName2 = "deleteWKOrdiniClienti";

		_methodParameterTypes2 = new String[] {
				"it.bysoftware.ct.service.persistence.WKOrdiniClientiPK"
			};

		_methodName3 = "deleteWKOrdiniClienti";

		_methodParameterTypes3 = new String[] {
				"it.bysoftware.ct.model.WKOrdiniClienti"
			};

		_methodName4 = "dynamicQuery";

		_methodParameterTypes4 = new String[] {  };

		_methodName5 = "dynamicQuery";

		_methodParameterTypes5 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName6 = "dynamicQuery";

		_methodParameterTypes6 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
			};

		_methodName7 = "dynamicQuery";

		_methodParameterTypes7 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
				"com.liferay.portal.kernel.util.OrderByComparator"
			};

		_methodName8 = "dynamicQueryCount";

		_methodParameterTypes8 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName9 = "dynamicQueryCount";

		_methodParameterTypes9 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery",
				"com.liferay.portal.kernel.dao.orm.Projection"
			};

		_methodName10 = "fetchWKOrdiniClienti";

		_methodParameterTypes10 = new String[] {
				"it.bysoftware.ct.service.persistence.WKOrdiniClientiPK"
			};

		_methodName11 = "getWKOrdiniClienti";

		_methodParameterTypes11 = new String[] {
				"it.bysoftware.ct.service.persistence.WKOrdiniClientiPK"
			};

		_methodName12 = "getPersistedModel";

		_methodParameterTypes12 = new String[] { "java.io.Serializable" };

		_methodName13 = "getWKOrdiniClientis";

		_methodParameterTypes13 = new String[] { "int", "int" };

		_methodName14 = "getWKOrdiniClientisCount";

		_methodParameterTypes14 = new String[] {  };

		_methodName15 = "updateWKOrdiniClienti";

		_methodParameterTypes15 = new String[] {
				"it.bysoftware.ct.model.WKOrdiniClienti"
			};

		_methodName154 = "getBeanIdentifier";

		_methodParameterTypes154 = new String[] {  };

		_methodName155 = "setBeanIdentifier";

		_methodParameterTypes155 = new String[] { "java.lang.String" };

		_methodName160 = "getNumeroOrdine";

		_methodParameterTypes160 = new String[] { "int", "int" };

		_methodName161 = "getOrdiniByCodiceCliente";

		_methodParameterTypes161 = new String[] { "java.lang.String" };

		_methodName162 = "getOrdiniClientiAcquisitiCount";

		_methodParameterTypes162 = new String[] {  };

		_methodName163 = "getOrdiniClientiDaAcquisireCount";

		_methodParameterTypes163 = new String[] {  };

		_methodName164 = "getOrdiniByStatoOrdine";

		_methodParameterTypes164 = new String[] { "boolean" };

		_methodName165 = "searchOrdini";

		_methodParameterTypes165 = new String[] {
				"java.lang.Boolean", "java.lang.String", "java.sql.Date",
				"java.sql.Date", "int", "int",
				"com.liferay.portal.kernel.util.OrderByComparator"
			};
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		if (_methodName0.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.addWKOrdiniClienti((it.bysoftware.ct.model.WKOrdiniClienti)arguments[0]);
		}

		if (_methodName1.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.createWKOrdiniClienti((it.bysoftware.ct.service.persistence.WKOrdiniClientiPK)arguments[0]);
		}

		if (_methodName2.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.deleteWKOrdiniClienti((it.bysoftware.ct.service.persistence.WKOrdiniClientiPK)arguments[0]);
		}

		if (_methodName3.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.deleteWKOrdiniClienti((it.bysoftware.ct.model.WKOrdiniClienti)arguments[0]);
		}

		if (_methodName4.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.dynamicQuery();
		}

		if (_methodName5.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName6.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName7.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue(),
				(com.liferay.portal.kernel.util.OrderByComparator)arguments[3]);
		}

		if (_methodName8.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName9.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				(com.liferay.portal.kernel.dao.orm.Projection)arguments[1]);
		}

		if (_methodName10.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.fetchWKOrdiniClienti((it.bysoftware.ct.service.persistence.WKOrdiniClientiPK)arguments[0]);
		}

		if (_methodName11.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.getWKOrdiniClienti((it.bysoftware.ct.service.persistence.WKOrdiniClientiPK)arguments[0]);
		}

		if (_methodName12.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.getPersistedModel((java.io.Serializable)arguments[0]);
		}

		if (_methodName13.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.getWKOrdiniClientis(((Integer)arguments[0]).intValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName14.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.getWKOrdiniClientisCount();
		}

		if (_methodName15.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.updateWKOrdiniClienti((it.bysoftware.ct.model.WKOrdiniClienti)arguments[0]);
		}

		if (_methodName154.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes154, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.getBeanIdentifier();
		}

		if (_methodName155.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes155, parameterTypes)) {
			WKOrdiniClientiLocalServiceUtil.setBeanIdentifier((java.lang.String)arguments[0]);

			return null;
		}

		if (_methodName160.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes160, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.getNumeroOrdine(((Integer)arguments[0]).intValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName161.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes161, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.getOrdiniByCodiceCliente((java.lang.String)arguments[0]);
		}

		if (_methodName162.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes162, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.getOrdiniClientiAcquisitiCount();
		}

		if (_methodName163.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes163, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.getOrdiniClientiDaAcquisireCount();
		}

		if (_methodName164.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes164, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.getOrdiniByStatoOrdine(((Boolean)arguments[0]).booleanValue());
		}

		if (_methodName165.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes165, parameterTypes)) {
			return WKOrdiniClientiLocalServiceUtil.searchOrdini((java.lang.Boolean)arguments[0],
				(java.lang.String)arguments[1], (java.sql.Date)arguments[2],
				(java.sql.Date)arguments[3],
				((Integer)arguments[4]).intValue(),
				((Integer)arguments[5]).intValue(),
				(com.liferay.portal.kernel.util.OrderByComparator)arguments[6]);
		}

		throw new UnsupportedOperationException();
	}

	private String _methodName0;
	private String[] _methodParameterTypes0;
	private String _methodName1;
	private String[] _methodParameterTypes1;
	private String _methodName2;
	private String[] _methodParameterTypes2;
	private String _methodName3;
	private String[] _methodParameterTypes3;
	private String _methodName4;
	private String[] _methodParameterTypes4;
	private String _methodName5;
	private String[] _methodParameterTypes5;
	private String _methodName6;
	private String[] _methodParameterTypes6;
	private String _methodName7;
	private String[] _methodParameterTypes7;
	private String _methodName8;
	private String[] _methodParameterTypes8;
	private String _methodName9;
	private String[] _methodParameterTypes9;
	private String _methodName10;
	private String[] _methodParameterTypes10;
	private String _methodName11;
	private String[] _methodParameterTypes11;
	private String _methodName12;
	private String[] _methodParameterTypes12;
	private String _methodName13;
	private String[] _methodParameterTypes13;
	private String _methodName14;
	private String[] _methodParameterTypes14;
	private String _methodName15;
	private String[] _methodParameterTypes15;
	private String _methodName154;
	private String[] _methodParameterTypes154;
	private String _methodName155;
	private String[] _methodParameterTypes155;
	private String _methodName160;
	private String[] _methodParameterTypes160;
	private String _methodName161;
	private String[] _methodParameterTypes161;
	private String _methodName162;
	private String[] _methodParameterTypes162;
	private String _methodName163;
	private String[] _methodParameterTypes163;
	private String _methodName164;
	private String[] _methodParameterTypes164;
	private String _methodName165;
	private String[] _methodParameterTypes165;
}