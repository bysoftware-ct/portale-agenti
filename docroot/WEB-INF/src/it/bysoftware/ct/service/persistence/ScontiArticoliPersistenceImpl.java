/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchScontiArticoliException;
import it.bysoftware.ct.model.ScontiArticoli;
import it.bysoftware.ct.model.impl.ScontiArticoliImpl;
import it.bysoftware.ct.model.impl.ScontiArticoliModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the sconti articoli service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ScontiArticoliPersistence
 * @see ScontiArticoliUtil
 * @generated
 */
public class ScontiArticoliPersistenceImpl extends BasePersistenceImpl<ScontiArticoli>
	implements ScontiArticoliPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ScontiArticoliUtil} to access the sconti articoli persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ScontiArticoliImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ScontiArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ScontiArticoliModelImpl.FINDER_CACHE_ENABLED,
			ScontiArticoliImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ScontiArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ScontiArticoliModelImpl.FINDER_CACHE_ENABLED,
			ScontiArticoliImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ScontiArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ScontiArticoliModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public ScontiArticoliPersistenceImpl() {
		setModelClass(ScontiArticoli.class);
	}

	/**
	 * Caches the sconti articoli in the entity cache if it is enabled.
	 *
	 * @param scontiArticoli the sconti articoli
	 */
	@Override
	public void cacheResult(ScontiArticoli scontiArticoli) {
		EntityCacheUtil.putResult(ScontiArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ScontiArticoliImpl.class, scontiArticoli.getPrimaryKey(),
			scontiArticoli);

		scontiArticoli.resetOriginalValues();
	}

	/**
	 * Caches the sconti articolis in the entity cache if it is enabled.
	 *
	 * @param scontiArticolis the sconti articolis
	 */
	@Override
	public void cacheResult(List<ScontiArticoli> scontiArticolis) {
		for (ScontiArticoli scontiArticoli : scontiArticolis) {
			if (EntityCacheUtil.getResult(
						ScontiArticoliModelImpl.ENTITY_CACHE_ENABLED,
						ScontiArticoliImpl.class, scontiArticoli.getPrimaryKey()) == null) {
				cacheResult(scontiArticoli);
			}
			else {
				scontiArticoli.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all sconti articolis.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ScontiArticoliImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ScontiArticoliImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the sconti articoli.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ScontiArticoli scontiArticoli) {
		EntityCacheUtil.removeResult(ScontiArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ScontiArticoliImpl.class, scontiArticoli.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ScontiArticoli> scontiArticolis) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ScontiArticoli scontiArticoli : scontiArticolis) {
			EntityCacheUtil.removeResult(ScontiArticoliModelImpl.ENTITY_CACHE_ENABLED,
				ScontiArticoliImpl.class, scontiArticoli.getPrimaryKey());
		}
	}

	/**
	 * Creates a new sconti articoli with the primary key. Does not add the sconti articoli to the database.
	 *
	 * @param scontiArticoliPK the primary key for the new sconti articoli
	 * @return the new sconti articoli
	 */
	@Override
	public ScontiArticoli create(ScontiArticoliPK scontiArticoliPK) {
		ScontiArticoli scontiArticoli = new ScontiArticoliImpl();

		scontiArticoli.setNew(true);
		scontiArticoli.setPrimaryKey(scontiArticoliPK);

		return scontiArticoli;
	}

	/**
	 * Removes the sconti articoli with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param scontiArticoliPK the primary key of the sconti articoli
	 * @return the sconti articoli that was removed
	 * @throws it.bysoftware.ct.NoSuchScontiArticoliException if a sconti articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ScontiArticoli remove(ScontiArticoliPK scontiArticoliPK)
		throws NoSuchScontiArticoliException, SystemException {
		return remove((Serializable)scontiArticoliPK);
	}

	/**
	 * Removes the sconti articoli with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the sconti articoli
	 * @return the sconti articoli that was removed
	 * @throws it.bysoftware.ct.NoSuchScontiArticoliException if a sconti articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ScontiArticoli remove(Serializable primaryKey)
		throws NoSuchScontiArticoliException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ScontiArticoli scontiArticoli = (ScontiArticoli)session.get(ScontiArticoliImpl.class,
					primaryKey);

			if (scontiArticoli == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchScontiArticoliException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(scontiArticoli);
		}
		catch (NoSuchScontiArticoliException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ScontiArticoli removeImpl(ScontiArticoli scontiArticoli)
		throws SystemException {
		scontiArticoli = toUnwrappedModel(scontiArticoli);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(scontiArticoli)) {
				scontiArticoli = (ScontiArticoli)session.get(ScontiArticoliImpl.class,
						scontiArticoli.getPrimaryKeyObj());
			}

			if (scontiArticoli != null) {
				session.delete(scontiArticoli);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (scontiArticoli != null) {
			clearCache(scontiArticoli);
		}

		return scontiArticoli;
	}

	@Override
	public ScontiArticoli updateImpl(
		it.bysoftware.ct.model.ScontiArticoli scontiArticoli)
		throws SystemException {
		scontiArticoli = toUnwrappedModel(scontiArticoli);

		boolean isNew = scontiArticoli.isNew();

		Session session = null;

		try {
			session = openSession();

			if (scontiArticoli.isNew()) {
				session.save(scontiArticoli);

				scontiArticoli.setNew(false);
			}
			else {
				session.merge(scontiArticoli);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ScontiArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ScontiArticoliImpl.class, scontiArticoli.getPrimaryKey(),
			scontiArticoli);

		return scontiArticoli;
	}

	protected ScontiArticoli toUnwrappedModel(ScontiArticoli scontiArticoli) {
		if (scontiArticoli instanceof ScontiArticoliImpl) {
			return scontiArticoli;
		}

		ScontiArticoliImpl scontiArticoliImpl = new ScontiArticoliImpl();

		scontiArticoliImpl.setNew(scontiArticoli.isNew());
		scontiArticoliImpl.setPrimaryKey(scontiArticoli.getPrimaryKey());

		scontiArticoliImpl.setCodScontoCliente(scontiArticoli.getCodScontoCliente());
		scontiArticoliImpl.setCodScontoArticolo(scontiArticoli.getCodScontoArticolo());
		scontiArticoliImpl.setSconto(scontiArticoli.getSconto());

		return scontiArticoliImpl;
	}

	/**
	 * Returns the sconti articoli with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the sconti articoli
	 * @return the sconti articoli
	 * @throws it.bysoftware.ct.NoSuchScontiArticoliException if a sconti articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ScontiArticoli findByPrimaryKey(Serializable primaryKey)
		throws NoSuchScontiArticoliException, SystemException {
		ScontiArticoli scontiArticoli = fetchByPrimaryKey(primaryKey);

		if (scontiArticoli == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchScontiArticoliException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return scontiArticoli;
	}

	/**
	 * Returns the sconti articoli with the primary key or throws a {@link it.bysoftware.ct.NoSuchScontiArticoliException} if it could not be found.
	 *
	 * @param scontiArticoliPK the primary key of the sconti articoli
	 * @return the sconti articoli
	 * @throws it.bysoftware.ct.NoSuchScontiArticoliException if a sconti articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ScontiArticoli findByPrimaryKey(ScontiArticoliPK scontiArticoliPK)
		throws NoSuchScontiArticoliException, SystemException {
		return findByPrimaryKey((Serializable)scontiArticoliPK);
	}

	/**
	 * Returns the sconti articoli with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the sconti articoli
	 * @return the sconti articoli, or <code>null</code> if a sconti articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ScontiArticoli fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ScontiArticoli scontiArticoli = (ScontiArticoli)EntityCacheUtil.getResult(ScontiArticoliModelImpl.ENTITY_CACHE_ENABLED,
				ScontiArticoliImpl.class, primaryKey);

		if (scontiArticoli == _nullScontiArticoli) {
			return null;
		}

		if (scontiArticoli == null) {
			Session session = null;

			try {
				session = openSession();

				scontiArticoli = (ScontiArticoli)session.get(ScontiArticoliImpl.class,
						primaryKey);

				if (scontiArticoli != null) {
					cacheResult(scontiArticoli);
				}
				else {
					EntityCacheUtil.putResult(ScontiArticoliModelImpl.ENTITY_CACHE_ENABLED,
						ScontiArticoliImpl.class, primaryKey,
						_nullScontiArticoli);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ScontiArticoliModelImpl.ENTITY_CACHE_ENABLED,
					ScontiArticoliImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return scontiArticoli;
	}

	/**
	 * Returns the sconti articoli with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param scontiArticoliPK the primary key of the sconti articoli
	 * @return the sconti articoli, or <code>null</code> if a sconti articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ScontiArticoli fetchByPrimaryKey(ScontiArticoliPK scontiArticoliPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)scontiArticoliPK);
	}

	/**
	 * Returns all the sconti articolis.
	 *
	 * @return the sconti articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ScontiArticoli> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the sconti articolis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScontiArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of sconti articolis
	 * @param end the upper bound of the range of sconti articolis (not inclusive)
	 * @return the range of sconti articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ScontiArticoli> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the sconti articolis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScontiArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of sconti articolis
	 * @param end the upper bound of the range of sconti articolis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of sconti articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ScontiArticoli> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ScontiArticoli> list = (List<ScontiArticoli>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_SCONTIARTICOLI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SCONTIARTICOLI;

				if (pagination) {
					sql = sql.concat(ScontiArticoliModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ScontiArticoli>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ScontiArticoli>(list);
				}
				else {
					list = (List<ScontiArticoli>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the sconti articolis from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ScontiArticoli scontiArticoli : findAll()) {
			remove(scontiArticoli);
		}
	}

	/**
	 * Returns the number of sconti articolis.
	 *
	 * @return the number of sconti articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SCONTIARTICOLI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the sconti articoli persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.ScontiArticoli")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ScontiArticoli>> listenersList = new ArrayList<ModelListener<ScontiArticoli>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ScontiArticoli>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ScontiArticoliImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_SCONTIARTICOLI = "SELECT scontiArticoli FROM ScontiArticoli scontiArticoli";
	private static final String _SQL_COUNT_SCONTIARTICOLI = "SELECT COUNT(scontiArticoli) FROM ScontiArticoli scontiArticoli";
	private static final String _ORDER_BY_ENTITY_ALIAS = "scontiArticoli.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ScontiArticoli exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ScontiArticoliPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"codScontoCliente", "codScontoArticolo", "sconto"
			});
	private static ScontiArticoli _nullScontiArticoli = new ScontiArticoliImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ScontiArticoli> toCacheModel() {
				return _nullScontiArticoliCacheModel;
			}
		};

	private static CacheModel<ScontiArticoli> _nullScontiArticoliCacheModel = new CacheModel<ScontiArticoli>() {
			@Override
			public ScontiArticoli toEntityModel() {
				return _nullScontiArticoli;
			}
		};
}