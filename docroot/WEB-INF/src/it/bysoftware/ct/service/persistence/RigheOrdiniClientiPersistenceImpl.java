/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchRigheOrdiniClientiException;
import it.bysoftware.ct.model.RigheOrdiniClienti;
import it.bysoftware.ct.model.impl.RigheOrdiniClientiImpl;
import it.bysoftware.ct.model.impl.RigheOrdiniClientiModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the righe ordini clienti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see RigheOrdiniClientiPersistence
 * @see RigheOrdiniClientiUtil
 * @generated
 */
public class RigheOrdiniClientiPersistenceImpl extends BasePersistenceImpl<RigheOrdiniClienti>
	implements RigheOrdiniClientiPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link RigheOrdiniClientiUtil} to access the righe ordini clienti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = RigheOrdiniClientiImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(RigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			RigheOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			RigheOrdiniClientiImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(RigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			RigheOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			RigheOrdiniClientiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(RigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			RigheOrdiniClientiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TESTATAORDINE =
		new FinderPath(RigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			RigheOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			RigheOrdiniClientiImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTestataOrdine",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				String.class.getName(), String.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TESTATAORDINE =
		new FinderPath(RigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			RigheOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			RigheOrdiniClientiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTestataOrdine",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				String.class.getName(), String.class.getName(),
				Integer.class.getName(), Integer.class.getName()
			},
			RigheOrdiniClientiModelImpl.ANNO_COLUMN_BITMASK |
			RigheOrdiniClientiModelImpl.CODICEATTIVITA_COLUMN_BITMASK |
			RigheOrdiniClientiModelImpl.CODICECENTRO_COLUMN_BITMASK |
			RigheOrdiniClientiModelImpl.CODICEDEPOSITO_COLUMN_BITMASK |
			RigheOrdiniClientiModelImpl.TIPOORDINE_COLUMN_BITMASK |
			RigheOrdiniClientiModelImpl.NUMEROORDINE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TESTATAORDINE = new FinderPath(RigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			RigheOrdiniClientiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTestataOrdine",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				String.class.getName(), String.class.getName(),
				Integer.class.getName(), Integer.class.getName()
			});

	/**
	 * Returns all the righe ordini clientis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @return the matching righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<RigheOrdiniClienti> findByTestataOrdine(int anno,
		String codiceAttivita, String codiceCentro, String codiceDeposito,
		int tipoOrdine, int numeroOrdine) throws SystemException {
		return findByTestataOrdine(anno, codiceAttivita, codiceCentro,
			codiceDeposito, tipoOrdine, numeroOrdine, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the righe ordini clientis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param start the lower bound of the range of righe ordini clientis
	 * @param end the upper bound of the range of righe ordini clientis (not inclusive)
	 * @return the range of matching righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<RigheOrdiniClienti> findByTestataOrdine(int anno,
		String codiceAttivita, String codiceCentro, String codiceDeposito,
		int tipoOrdine, int numeroOrdine, int start, int end)
		throws SystemException {
		return findByTestataOrdine(anno, codiceAttivita, codiceCentro,
			codiceDeposito, tipoOrdine, numeroOrdine, start, end, null);
	}

	/**
	 * Returns an ordered range of all the righe ordini clientis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param start the lower bound of the range of righe ordini clientis
	 * @param end the upper bound of the range of righe ordini clientis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<RigheOrdiniClienti> findByTestataOrdine(int anno,
		String codiceAttivita, String codiceCentro, String codiceDeposito,
		int tipoOrdine, int numeroOrdine, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TESTATAORDINE;
			finderArgs = new Object[] {
					anno, codiceAttivita, codiceCentro, codiceDeposito,
					tipoOrdine, numeroOrdine
				};
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TESTATAORDINE;
			finderArgs = new Object[] {
					anno, codiceAttivita, codiceCentro, codiceDeposito,
					tipoOrdine, numeroOrdine,
					
					start, end, orderByComparator
				};
		}

		List<RigheOrdiniClienti> list = (List<RigheOrdiniClienti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (RigheOrdiniClienti righeOrdiniClienti : list) {
				if ((anno != righeOrdiniClienti.getAnno()) ||
						!Validator.equals(codiceAttivita,
							righeOrdiniClienti.getCodiceAttivita()) ||
						!Validator.equals(codiceCentro,
							righeOrdiniClienti.getCodiceCentro()) ||
						!Validator.equals(codiceDeposito,
							righeOrdiniClienti.getCodiceDeposito()) ||
						(tipoOrdine != righeOrdiniClienti.getTipoOrdine()) ||
						(numeroOrdine != righeOrdiniClienti.getNumeroOrdine())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(8 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(8);
			}

			query.append(_SQL_SELECT_RIGHEORDINICLIENTI_WHERE);

			query.append(_FINDER_COLUMN_TESTATAORDINE_ANNO_2);

			boolean bindCodiceAttivita = false;

			if (codiceAttivita == null) {
				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEATTIVITA_1);
			}
			else if (codiceAttivita.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEATTIVITA_3);
			}
			else {
				bindCodiceAttivita = true;

				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEATTIVITA_2);
			}

			boolean bindCodiceCentro = false;

			if (codiceCentro == null) {
				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICECENTRO_1);
			}
			else if (codiceCentro.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICECENTRO_3);
			}
			else {
				bindCodiceCentro = true;

				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICECENTRO_2);
			}

			boolean bindCodiceDeposito = false;

			if (codiceDeposito == null) {
				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEDEPOSITO_1);
			}
			else if (codiceDeposito.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEDEPOSITO_3);
			}
			else {
				bindCodiceDeposito = true;

				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEDEPOSITO_2);
			}

			query.append(_FINDER_COLUMN_TESTATAORDINE_TIPOORDINE_2);

			query.append(_FINDER_COLUMN_TESTATAORDINE_NUMEROORDINE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(RigheOrdiniClientiModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(anno);

				if (bindCodiceAttivita) {
					qPos.add(codiceAttivita);
				}

				if (bindCodiceCentro) {
					qPos.add(codiceCentro);
				}

				if (bindCodiceDeposito) {
					qPos.add(codiceDeposito);
				}

				qPos.add(tipoOrdine);

				qPos.add(numeroOrdine);

				if (!pagination) {
					list = (List<RigheOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<RigheOrdiniClienti>(list);
				}
				else {
					list = (List<RigheOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first righe ordini clienti in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching righe ordini clienti
	 * @throws it.bysoftware.ct.NoSuchRigheOrdiniClientiException if a matching righe ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheOrdiniClienti findByTestataOrdine_First(int anno,
		String codiceAttivita, String codiceCentro, String codiceDeposito,
		int tipoOrdine, int numeroOrdine, OrderByComparator orderByComparator)
		throws NoSuchRigheOrdiniClientiException, SystemException {
		RigheOrdiniClienti righeOrdiniClienti = fetchByTestataOrdine_First(anno,
				codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
				numeroOrdine, orderByComparator);

		if (righeOrdiniClienti != null) {
			return righeOrdiniClienti;
		}

		StringBundler msg = new StringBundler(14);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("anno=");
		msg.append(anno);

		msg.append(", codiceAttivita=");
		msg.append(codiceAttivita);

		msg.append(", codiceCentro=");
		msg.append(codiceCentro);

		msg.append(", codiceDeposito=");
		msg.append(codiceDeposito);

		msg.append(", tipoOrdine=");
		msg.append(tipoOrdine);

		msg.append(", numeroOrdine=");
		msg.append(numeroOrdine);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchRigheOrdiniClientiException(msg.toString());
	}

	/**
	 * Returns the first righe ordini clienti in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching righe ordini clienti, or <code>null</code> if a matching righe ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheOrdiniClienti fetchByTestataOrdine_First(int anno,
		String codiceAttivita, String codiceCentro, String codiceDeposito,
		int tipoOrdine, int numeroOrdine, OrderByComparator orderByComparator)
		throws SystemException {
		List<RigheOrdiniClienti> list = findByTestataOrdine(anno,
				codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
				numeroOrdine, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last righe ordini clienti in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching righe ordini clienti
	 * @throws it.bysoftware.ct.NoSuchRigheOrdiniClientiException if a matching righe ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheOrdiniClienti findByTestataOrdine_Last(int anno,
		String codiceAttivita, String codiceCentro, String codiceDeposito,
		int tipoOrdine, int numeroOrdine, OrderByComparator orderByComparator)
		throws NoSuchRigheOrdiniClientiException, SystemException {
		RigheOrdiniClienti righeOrdiniClienti = fetchByTestataOrdine_Last(anno,
				codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
				numeroOrdine, orderByComparator);

		if (righeOrdiniClienti != null) {
			return righeOrdiniClienti;
		}

		StringBundler msg = new StringBundler(14);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("anno=");
		msg.append(anno);

		msg.append(", codiceAttivita=");
		msg.append(codiceAttivita);

		msg.append(", codiceCentro=");
		msg.append(codiceCentro);

		msg.append(", codiceDeposito=");
		msg.append(codiceDeposito);

		msg.append(", tipoOrdine=");
		msg.append(tipoOrdine);

		msg.append(", numeroOrdine=");
		msg.append(numeroOrdine);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchRigheOrdiniClientiException(msg.toString());
	}

	/**
	 * Returns the last righe ordini clienti in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching righe ordini clienti, or <code>null</code> if a matching righe ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheOrdiniClienti fetchByTestataOrdine_Last(int anno,
		String codiceAttivita, String codiceCentro, String codiceDeposito,
		int tipoOrdine, int numeroOrdine, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByTestataOrdine(anno, codiceAttivita, codiceCentro,
				codiceDeposito, tipoOrdine, numeroOrdine);

		if (count == 0) {
			return null;
		}

		List<RigheOrdiniClienti> list = findByTestataOrdine(anno,
				codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
				numeroOrdine, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the righe ordini clientis before and after the current righe ordini clienti in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * @param righeOrdiniClientiPK the primary key of the current righe ordini clienti
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next righe ordini clienti
	 * @throws it.bysoftware.ct.NoSuchRigheOrdiniClientiException if a righe ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheOrdiniClienti[] findByTestataOrdine_PrevAndNext(
		RigheOrdiniClientiPK righeOrdiniClientiPK, int anno,
		String codiceAttivita, String codiceCentro, String codiceDeposito,
		int tipoOrdine, int numeroOrdine, OrderByComparator orderByComparator)
		throws NoSuchRigheOrdiniClientiException, SystemException {
		RigheOrdiniClienti righeOrdiniClienti = findByPrimaryKey(righeOrdiniClientiPK);

		Session session = null;

		try {
			session = openSession();

			RigheOrdiniClienti[] array = new RigheOrdiniClientiImpl[3];

			array[0] = getByTestataOrdine_PrevAndNext(session,
					righeOrdiniClienti, anno, codiceAttivita, codiceCentro,
					codiceDeposito, tipoOrdine, numeroOrdine,
					orderByComparator, true);

			array[1] = righeOrdiniClienti;

			array[2] = getByTestataOrdine_PrevAndNext(session,
					righeOrdiniClienti, anno, codiceAttivita, codiceCentro,
					codiceDeposito, tipoOrdine, numeroOrdine,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected RigheOrdiniClienti getByTestataOrdine_PrevAndNext(
		Session session, RigheOrdiniClienti righeOrdiniClienti, int anno,
		String codiceAttivita, String codiceCentro, String codiceDeposito,
		int tipoOrdine, int numeroOrdine, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_RIGHEORDINICLIENTI_WHERE);

		query.append(_FINDER_COLUMN_TESTATAORDINE_ANNO_2);

		boolean bindCodiceAttivita = false;

		if (codiceAttivita == null) {
			query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEATTIVITA_1);
		}
		else if (codiceAttivita.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEATTIVITA_3);
		}
		else {
			bindCodiceAttivita = true;

			query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEATTIVITA_2);
		}

		boolean bindCodiceCentro = false;

		if (codiceCentro == null) {
			query.append(_FINDER_COLUMN_TESTATAORDINE_CODICECENTRO_1);
		}
		else if (codiceCentro.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_TESTATAORDINE_CODICECENTRO_3);
		}
		else {
			bindCodiceCentro = true;

			query.append(_FINDER_COLUMN_TESTATAORDINE_CODICECENTRO_2);
		}

		boolean bindCodiceDeposito = false;

		if (codiceDeposito == null) {
			query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEDEPOSITO_1);
		}
		else if (codiceDeposito.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEDEPOSITO_3);
		}
		else {
			bindCodiceDeposito = true;

			query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEDEPOSITO_2);
		}

		query.append(_FINDER_COLUMN_TESTATAORDINE_TIPOORDINE_2);

		query.append(_FINDER_COLUMN_TESTATAORDINE_NUMEROORDINE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(RigheOrdiniClientiModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(anno);

		if (bindCodiceAttivita) {
			qPos.add(codiceAttivita);
		}

		if (bindCodiceCentro) {
			qPos.add(codiceCentro);
		}

		if (bindCodiceDeposito) {
			qPos.add(codiceDeposito);
		}

		qPos.add(tipoOrdine);

		qPos.add(numeroOrdine);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(righeOrdiniClienti);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<RigheOrdiniClienti> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the righe ordini clientis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; from the database.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByTestataOrdine(int anno, String codiceAttivita,
		String codiceCentro, String codiceDeposito, int tipoOrdine,
		int numeroOrdine) throws SystemException {
		for (RigheOrdiniClienti righeOrdiniClienti : findByTestataOrdine(anno,
				codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
				numeroOrdine, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(righeOrdiniClienti);
		}
	}

	/**
	 * Returns the number of righe ordini clientis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @return the number of matching righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByTestataOrdine(int anno, String codiceAttivita,
		String codiceCentro, String codiceDeposito, int tipoOrdine,
		int numeroOrdine) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_TESTATAORDINE;

		Object[] finderArgs = new Object[] {
				anno, codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
				numeroOrdine
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(7);

			query.append(_SQL_COUNT_RIGHEORDINICLIENTI_WHERE);

			query.append(_FINDER_COLUMN_TESTATAORDINE_ANNO_2);

			boolean bindCodiceAttivita = false;

			if (codiceAttivita == null) {
				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEATTIVITA_1);
			}
			else if (codiceAttivita.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEATTIVITA_3);
			}
			else {
				bindCodiceAttivita = true;

				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEATTIVITA_2);
			}

			boolean bindCodiceCentro = false;

			if (codiceCentro == null) {
				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICECENTRO_1);
			}
			else if (codiceCentro.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICECENTRO_3);
			}
			else {
				bindCodiceCentro = true;

				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICECENTRO_2);
			}

			boolean bindCodiceDeposito = false;

			if (codiceDeposito == null) {
				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEDEPOSITO_1);
			}
			else if (codiceDeposito.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEDEPOSITO_3);
			}
			else {
				bindCodiceDeposito = true;

				query.append(_FINDER_COLUMN_TESTATAORDINE_CODICEDEPOSITO_2);
			}

			query.append(_FINDER_COLUMN_TESTATAORDINE_TIPOORDINE_2);

			query.append(_FINDER_COLUMN_TESTATAORDINE_NUMEROORDINE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(anno);

				if (bindCodiceAttivita) {
					qPos.add(codiceAttivita);
				}

				if (bindCodiceCentro) {
					qPos.add(codiceCentro);
				}

				if (bindCodiceDeposito) {
					qPos.add(codiceDeposito);
				}

				qPos.add(tipoOrdine);

				qPos.add(numeroOrdine);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_TESTATAORDINE_ANNO_2 = "righeOrdiniClienti.id.anno = ? AND ";
	private static final String _FINDER_COLUMN_TESTATAORDINE_CODICEATTIVITA_1 = "righeOrdiniClienti.id.codiceAttivita IS NULL AND ";
	private static final String _FINDER_COLUMN_TESTATAORDINE_CODICEATTIVITA_2 = "righeOrdiniClienti.id.codiceAttivita = ? AND ";
	private static final String _FINDER_COLUMN_TESTATAORDINE_CODICEATTIVITA_3 = "(righeOrdiniClienti.id.codiceAttivita IS NULL OR righeOrdiniClienti.id.codiceAttivita = '') AND ";
	private static final String _FINDER_COLUMN_TESTATAORDINE_CODICECENTRO_1 = "righeOrdiniClienti.id.codiceCentro IS NULL AND ";
	private static final String _FINDER_COLUMN_TESTATAORDINE_CODICECENTRO_2 = "righeOrdiniClienti.id.codiceCentro = ? AND ";
	private static final String _FINDER_COLUMN_TESTATAORDINE_CODICECENTRO_3 = "(righeOrdiniClienti.id.codiceCentro IS NULL OR righeOrdiniClienti.id.codiceCentro = '') AND ";
	private static final String _FINDER_COLUMN_TESTATAORDINE_CODICEDEPOSITO_1 = "righeOrdiniClienti.id.codiceDeposito IS NULL AND ";
	private static final String _FINDER_COLUMN_TESTATAORDINE_CODICEDEPOSITO_2 = "righeOrdiniClienti.id.codiceDeposito = ? AND ";
	private static final String _FINDER_COLUMN_TESTATAORDINE_CODICEDEPOSITO_3 = "(righeOrdiniClienti.id.codiceDeposito IS NULL OR righeOrdiniClienti.id.codiceDeposito = '') AND ";
	private static final String _FINDER_COLUMN_TESTATAORDINE_TIPOORDINE_2 = "righeOrdiniClienti.id.tipoOrdine = ? AND ";
	private static final String _FINDER_COLUMN_TESTATAORDINE_NUMEROORDINE_2 = "righeOrdiniClienti.id.numeroOrdine = ?";

	public RigheOrdiniClientiPersistenceImpl() {
		setModelClass(RigheOrdiniClienti.class);
	}

	/**
	 * Caches the righe ordini clienti in the entity cache if it is enabled.
	 *
	 * @param righeOrdiniClienti the righe ordini clienti
	 */
	@Override
	public void cacheResult(RigheOrdiniClienti righeOrdiniClienti) {
		EntityCacheUtil.putResult(RigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			RigheOrdiniClientiImpl.class, righeOrdiniClienti.getPrimaryKey(),
			righeOrdiniClienti);

		righeOrdiniClienti.resetOriginalValues();
	}

	/**
	 * Caches the righe ordini clientis in the entity cache if it is enabled.
	 *
	 * @param righeOrdiniClientis the righe ordini clientis
	 */
	@Override
	public void cacheResult(List<RigheOrdiniClienti> righeOrdiniClientis) {
		for (RigheOrdiniClienti righeOrdiniClienti : righeOrdiniClientis) {
			if (EntityCacheUtil.getResult(
						RigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
						RigheOrdiniClientiImpl.class,
						righeOrdiniClienti.getPrimaryKey()) == null) {
				cacheResult(righeOrdiniClienti);
			}
			else {
				righeOrdiniClienti.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all righe ordini clientis.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(RigheOrdiniClientiImpl.class.getName());
		}

		EntityCacheUtil.clearCache(RigheOrdiniClientiImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the righe ordini clienti.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(RigheOrdiniClienti righeOrdiniClienti) {
		EntityCacheUtil.removeResult(RigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			RigheOrdiniClientiImpl.class, righeOrdiniClienti.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<RigheOrdiniClienti> righeOrdiniClientis) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (RigheOrdiniClienti righeOrdiniClienti : righeOrdiniClientis) {
			EntityCacheUtil.removeResult(RigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
				RigheOrdiniClientiImpl.class, righeOrdiniClienti.getPrimaryKey());
		}
	}

	/**
	 * Creates a new righe ordini clienti with the primary key. Does not add the righe ordini clienti to the database.
	 *
	 * @param righeOrdiniClientiPK the primary key for the new righe ordini clienti
	 * @return the new righe ordini clienti
	 */
	@Override
	public RigheOrdiniClienti create(RigheOrdiniClientiPK righeOrdiniClientiPK) {
		RigheOrdiniClienti righeOrdiniClienti = new RigheOrdiniClientiImpl();

		righeOrdiniClienti.setNew(true);
		righeOrdiniClienti.setPrimaryKey(righeOrdiniClientiPK);

		return righeOrdiniClienti;
	}

	/**
	 * Removes the righe ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param righeOrdiniClientiPK the primary key of the righe ordini clienti
	 * @return the righe ordini clienti that was removed
	 * @throws it.bysoftware.ct.NoSuchRigheOrdiniClientiException if a righe ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheOrdiniClienti remove(RigheOrdiniClientiPK righeOrdiniClientiPK)
		throws NoSuchRigheOrdiniClientiException, SystemException {
		return remove((Serializable)righeOrdiniClientiPK);
	}

	/**
	 * Removes the righe ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the righe ordini clienti
	 * @return the righe ordini clienti that was removed
	 * @throws it.bysoftware.ct.NoSuchRigheOrdiniClientiException if a righe ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheOrdiniClienti remove(Serializable primaryKey)
		throws NoSuchRigheOrdiniClientiException, SystemException {
		Session session = null;

		try {
			session = openSession();

			RigheOrdiniClienti righeOrdiniClienti = (RigheOrdiniClienti)session.get(RigheOrdiniClientiImpl.class,
					primaryKey);

			if (righeOrdiniClienti == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchRigheOrdiniClientiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(righeOrdiniClienti);
		}
		catch (NoSuchRigheOrdiniClientiException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected RigheOrdiniClienti removeImpl(
		RigheOrdiniClienti righeOrdiniClienti) throws SystemException {
		righeOrdiniClienti = toUnwrappedModel(righeOrdiniClienti);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(righeOrdiniClienti)) {
				righeOrdiniClienti = (RigheOrdiniClienti)session.get(RigheOrdiniClientiImpl.class,
						righeOrdiniClienti.getPrimaryKeyObj());
			}

			if (righeOrdiniClienti != null) {
				session.delete(righeOrdiniClienti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (righeOrdiniClienti != null) {
			clearCache(righeOrdiniClienti);
		}

		return righeOrdiniClienti;
	}

	@Override
	public RigheOrdiniClienti updateImpl(
		it.bysoftware.ct.model.RigheOrdiniClienti righeOrdiniClienti)
		throws SystemException {
		righeOrdiniClienti = toUnwrappedModel(righeOrdiniClienti);

		boolean isNew = righeOrdiniClienti.isNew();

		RigheOrdiniClientiModelImpl righeOrdiniClientiModelImpl = (RigheOrdiniClientiModelImpl)righeOrdiniClienti;

		Session session = null;

		try {
			session = openSession();

			if (righeOrdiniClienti.isNew()) {
				session.save(righeOrdiniClienti);

				righeOrdiniClienti.setNew(false);
			}
			else {
				session.merge(righeOrdiniClienti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !RigheOrdiniClientiModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((righeOrdiniClientiModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TESTATAORDINE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						righeOrdiniClientiModelImpl.getOriginalAnno(),
						righeOrdiniClientiModelImpl.getOriginalCodiceAttivita(),
						righeOrdiniClientiModelImpl.getOriginalCodiceCentro(),
						righeOrdiniClientiModelImpl.getOriginalCodiceDeposito(),
						righeOrdiniClientiModelImpl.getOriginalTipoOrdine(),
						righeOrdiniClientiModelImpl.getOriginalNumeroOrdine()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TESTATAORDINE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TESTATAORDINE,
					args);

				args = new Object[] {
						righeOrdiniClientiModelImpl.getAnno(),
						righeOrdiniClientiModelImpl.getCodiceAttivita(),
						righeOrdiniClientiModelImpl.getCodiceCentro(),
						righeOrdiniClientiModelImpl.getCodiceDeposito(),
						righeOrdiniClientiModelImpl.getTipoOrdine(),
						righeOrdiniClientiModelImpl.getNumeroOrdine()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TESTATAORDINE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TESTATAORDINE,
					args);
			}
		}

		EntityCacheUtil.putResult(RigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			RigheOrdiniClientiImpl.class, righeOrdiniClienti.getPrimaryKey(),
			righeOrdiniClienti);

		return righeOrdiniClienti;
	}

	protected RigheOrdiniClienti toUnwrappedModel(
		RigheOrdiniClienti righeOrdiniClienti) {
		if (righeOrdiniClienti instanceof RigheOrdiniClientiImpl) {
			return righeOrdiniClienti;
		}

		RigheOrdiniClientiImpl righeOrdiniClientiImpl = new RigheOrdiniClientiImpl();

		righeOrdiniClientiImpl.setNew(righeOrdiniClienti.isNew());
		righeOrdiniClientiImpl.setPrimaryKey(righeOrdiniClienti.getPrimaryKey());

		righeOrdiniClientiImpl.setAnno(righeOrdiniClienti.getAnno());
		righeOrdiniClientiImpl.setCodiceAttivita(righeOrdiniClienti.getCodiceAttivita());
		righeOrdiniClientiImpl.setCodiceCentro(righeOrdiniClienti.getCodiceCentro());
		righeOrdiniClientiImpl.setCodiceDeposito(righeOrdiniClienti.getCodiceDeposito());
		righeOrdiniClientiImpl.setTipoOrdine(righeOrdiniClienti.getTipoOrdine());
		righeOrdiniClientiImpl.setNumeroOrdine(righeOrdiniClienti.getNumeroOrdine());
		righeOrdiniClientiImpl.setNumeroRigo(righeOrdiniClienti.getNumeroRigo());
		righeOrdiniClientiImpl.setStatoRigo(righeOrdiniClienti.isStatoRigo());
		righeOrdiniClientiImpl.setTipoRigo(righeOrdiniClienti.getTipoRigo());
		righeOrdiniClientiImpl.setCodiceCausaleMagazzino(righeOrdiniClienti.getCodiceCausaleMagazzino());
		righeOrdiniClientiImpl.setCodiceDepositoMov(righeOrdiniClienti.getCodiceDepositoMov());
		righeOrdiniClientiImpl.setCodiceArticolo(righeOrdiniClienti.getCodiceArticolo());
		righeOrdiniClientiImpl.setCodiceVariante(righeOrdiniClienti.getCodiceVariante());
		righeOrdiniClientiImpl.setDescrizione(righeOrdiniClienti.getDescrizione());
		righeOrdiniClientiImpl.setCodiceUnitMis(righeOrdiniClienti.getCodiceUnitMis());
		righeOrdiniClientiImpl.setDecimaliQuant(righeOrdiniClienti.getDecimaliQuant());
		righeOrdiniClientiImpl.setQuantita1(righeOrdiniClienti.getQuantita1());
		righeOrdiniClientiImpl.setQuantita2(righeOrdiniClienti.getQuantita2());
		righeOrdiniClientiImpl.setQuantita3(righeOrdiniClienti.getQuantita3());
		righeOrdiniClientiImpl.setQuantita(righeOrdiniClienti.getQuantita());
		righeOrdiniClientiImpl.setCodiceUnitMis2(righeOrdiniClienti.getCodiceUnitMis2());
		righeOrdiniClientiImpl.setQuantitaUnitMis2(righeOrdiniClienti.getQuantitaUnitMis2());
		righeOrdiniClientiImpl.setDecimaliPrezzo(righeOrdiniClienti.getDecimaliPrezzo());
		righeOrdiniClientiImpl.setPrezzo(righeOrdiniClienti.getPrezzo());
		righeOrdiniClientiImpl.setImportoLordo(righeOrdiniClienti.getImportoLordo());
		righeOrdiniClientiImpl.setSconto1(righeOrdiniClienti.getSconto1());
		righeOrdiniClientiImpl.setSconto2(righeOrdiniClienti.getSconto2());
		righeOrdiniClientiImpl.setSconto3(righeOrdiniClienti.getSconto3());
		righeOrdiniClientiImpl.setImportoNetto(righeOrdiniClienti.getImportoNetto());
		righeOrdiniClientiImpl.setImporto(righeOrdiniClienti.getImporto());
		righeOrdiniClientiImpl.setCodiceIVAFatturazione(righeOrdiniClienti.getCodiceIVAFatturazione());
		righeOrdiniClientiImpl.setTipoProvviggione(righeOrdiniClienti.isTipoProvviggione());
		righeOrdiniClientiImpl.setPercentualeProvvAgente(righeOrdiniClienti.getPercentualeProvvAgente());
		righeOrdiniClientiImpl.setImportoProvvAgente(righeOrdiniClienti.getImportoProvvAgente());
		righeOrdiniClientiImpl.setCodiceSottoconto(righeOrdiniClienti.getCodiceSottoconto());
		righeOrdiniClientiImpl.setNomenclaturaCombinata(righeOrdiniClienti.getNomenclaturaCombinata());
		righeOrdiniClientiImpl.setStampaDistBase(righeOrdiniClienti.isStampaDistBase());
		righeOrdiniClientiImpl.setCodiceCliente(righeOrdiniClienti.getCodiceCliente());
		righeOrdiniClientiImpl.setRiferimentoOrdineCliente(righeOrdiniClienti.getRiferimentoOrdineCliente());
		righeOrdiniClientiImpl.setDataOridine(righeOrdiniClienti.getDataOridine());
		righeOrdiniClientiImpl.setStatoEvasione(righeOrdiniClienti.isStatoEvasione());
		righeOrdiniClientiImpl.setDataPrevistaConsegna(righeOrdiniClienti.getDataPrevistaConsegna());
		righeOrdiniClientiImpl.setDataRegistrazioneOrdine(righeOrdiniClienti.getDataRegistrazioneOrdine());
		righeOrdiniClientiImpl.setNumeroPrimaNota(righeOrdiniClienti.getNumeroPrimaNota());
		righeOrdiniClientiImpl.setNumeroProgressivo(righeOrdiniClienti.getNumeroProgressivo());
		righeOrdiniClientiImpl.setTipoCompEspl(righeOrdiniClienti.getTipoCompEspl());
		righeOrdiniClientiImpl.setMaxLivCompEspl(righeOrdiniClienti.getMaxLivCompEspl());
		righeOrdiniClientiImpl.setGenerazioneOrdFornit(righeOrdiniClienti.getGenerazioneOrdFornit());
		righeOrdiniClientiImpl.setEsercizioProOrd(righeOrdiniClienti.getEsercizioProOrd());
		righeOrdiniClientiImpl.setNumPropOrdine(righeOrdiniClienti.getNumPropOrdine());
		righeOrdiniClientiImpl.setNumRigoPropOrdine(righeOrdiniClienti.getNumRigoPropOrdine());
		righeOrdiniClientiImpl.setLibStr1(righeOrdiniClienti.getLibStr1());
		righeOrdiniClientiImpl.setLibStr2(righeOrdiniClienti.getLibStr2());
		righeOrdiniClientiImpl.setLibStr3(righeOrdiniClienti.getLibStr3());
		righeOrdiniClientiImpl.setLibDbl1(righeOrdiniClienti.getLibDbl1());
		righeOrdiniClientiImpl.setLibDbl2(righeOrdiniClienti.getLibDbl2());
		righeOrdiniClientiImpl.setLibDbl3(righeOrdiniClienti.getLibDbl3());
		righeOrdiniClientiImpl.setLibDat1(righeOrdiniClienti.getLibDat1());
		righeOrdiniClientiImpl.setLibDat2(righeOrdiniClienti.getLibDat2());
		righeOrdiniClientiImpl.setLibDat3(righeOrdiniClienti.getLibDat3());
		righeOrdiniClientiImpl.setLibLng1(righeOrdiniClienti.getLibLng1());
		righeOrdiniClientiImpl.setLibLng2(righeOrdiniClienti.getLibLng2());
		righeOrdiniClientiImpl.setLibLng3(righeOrdiniClienti.getLibLng3());

		return righeOrdiniClientiImpl;
	}

	/**
	 * Returns the righe ordini clienti with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the righe ordini clienti
	 * @return the righe ordini clienti
	 * @throws it.bysoftware.ct.NoSuchRigheOrdiniClientiException if a righe ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheOrdiniClienti findByPrimaryKey(Serializable primaryKey)
		throws NoSuchRigheOrdiniClientiException, SystemException {
		RigheOrdiniClienti righeOrdiniClienti = fetchByPrimaryKey(primaryKey);

		if (righeOrdiniClienti == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchRigheOrdiniClientiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return righeOrdiniClienti;
	}

	/**
	 * Returns the righe ordini clienti with the primary key or throws a {@link it.bysoftware.ct.NoSuchRigheOrdiniClientiException} if it could not be found.
	 *
	 * @param righeOrdiniClientiPK the primary key of the righe ordini clienti
	 * @return the righe ordini clienti
	 * @throws it.bysoftware.ct.NoSuchRigheOrdiniClientiException if a righe ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheOrdiniClienti findByPrimaryKey(
		RigheOrdiniClientiPK righeOrdiniClientiPK)
		throws NoSuchRigheOrdiniClientiException, SystemException {
		return findByPrimaryKey((Serializable)righeOrdiniClientiPK);
	}

	/**
	 * Returns the righe ordini clienti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the righe ordini clienti
	 * @return the righe ordini clienti, or <code>null</code> if a righe ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheOrdiniClienti fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		RigheOrdiniClienti righeOrdiniClienti = (RigheOrdiniClienti)EntityCacheUtil.getResult(RigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
				RigheOrdiniClientiImpl.class, primaryKey);

		if (righeOrdiniClienti == _nullRigheOrdiniClienti) {
			return null;
		}

		if (righeOrdiniClienti == null) {
			Session session = null;

			try {
				session = openSession();

				righeOrdiniClienti = (RigheOrdiniClienti)session.get(RigheOrdiniClientiImpl.class,
						primaryKey);

				if (righeOrdiniClienti != null) {
					cacheResult(righeOrdiniClienti);
				}
				else {
					EntityCacheUtil.putResult(RigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
						RigheOrdiniClientiImpl.class, primaryKey,
						_nullRigheOrdiniClienti);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(RigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
					RigheOrdiniClientiImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return righeOrdiniClienti;
	}

	/**
	 * Returns the righe ordini clienti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param righeOrdiniClientiPK the primary key of the righe ordini clienti
	 * @return the righe ordini clienti, or <code>null</code> if a righe ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheOrdiniClienti fetchByPrimaryKey(
		RigheOrdiniClientiPK righeOrdiniClientiPK) throws SystemException {
		return fetchByPrimaryKey((Serializable)righeOrdiniClientiPK);
	}

	/**
	 * Returns all the righe ordini clientis.
	 *
	 * @return the righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<RigheOrdiniClienti> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the righe ordini clientis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of righe ordini clientis
	 * @param end the upper bound of the range of righe ordini clientis (not inclusive)
	 * @return the range of righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<RigheOrdiniClienti> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the righe ordini clientis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of righe ordini clientis
	 * @param end the upper bound of the range of righe ordini clientis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<RigheOrdiniClienti> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<RigheOrdiniClienti> list = (List<RigheOrdiniClienti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_RIGHEORDINICLIENTI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_RIGHEORDINICLIENTI;

				if (pagination) {
					sql = sql.concat(RigheOrdiniClientiModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<RigheOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<RigheOrdiniClienti>(list);
				}
				else {
					list = (List<RigheOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the righe ordini clientis from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (RigheOrdiniClienti righeOrdiniClienti : findAll()) {
			remove(righeOrdiniClienti);
		}
	}

	/**
	 * Returns the number of righe ordini clientis.
	 *
	 * @return the number of righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_RIGHEORDINICLIENTI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the righe ordini clienti persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.RigheOrdiniClienti")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<RigheOrdiniClienti>> listenersList = new ArrayList<ModelListener<RigheOrdiniClienti>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<RigheOrdiniClienti>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(RigheOrdiniClientiImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_RIGHEORDINICLIENTI = "SELECT righeOrdiniClienti FROM RigheOrdiniClienti righeOrdiniClienti";
	private static final String _SQL_SELECT_RIGHEORDINICLIENTI_WHERE = "SELECT righeOrdiniClienti FROM RigheOrdiniClienti righeOrdiniClienti WHERE ";
	private static final String _SQL_COUNT_RIGHEORDINICLIENTI = "SELECT COUNT(righeOrdiniClienti) FROM RigheOrdiniClienti righeOrdiniClienti";
	private static final String _SQL_COUNT_RIGHEORDINICLIENTI_WHERE = "SELECT COUNT(righeOrdiniClienti) FROM RigheOrdiniClienti righeOrdiniClienti WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "righeOrdiniClienti.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No RigheOrdiniClienti exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No RigheOrdiniClienti exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(RigheOrdiniClientiPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"anno", "codiceAttivita", "codiceCentro", "codiceDeposito",
				"tipoOrdine", "numeroOrdine", "numeroRigo", "statoRigo",
				"tipoRigo", "codiceCausaleMagazzino", "codiceDepositoMov",
				"codiceArticolo", "codiceVariante", "descrizione",
				"codiceUnitMis", "decimaliQuant", "quantita1", "quantita2",
				"quantita3", "quantita", "codiceUnitMis2", "quantitaUnitMis2",
				"decimaliPrezzo", "prezzo", "importoLordo", "sconto1", "sconto2",
				"sconto3", "importoNetto", "importo", "codiceIVAFatturazione",
				"tipoProvviggione", "percentualeProvvAgente",
				"importoProvvAgente", "codiceSottoconto",
				"nomenclaturaCombinata", "stampaDistBase", "codiceCliente",
				"riferimentoOrdineCliente", "dataOridine", "statoEvasione",
				"dataPrevistaConsegna", "dataRegistrazioneOrdine",
				"numeroPrimaNota", "numeroProgressivo", "tipoCompEspl",
				"maxLivCompEspl", "generazioneOrdFornit", "esercizioProOrd",
				"numPropOrdine", "numRigoPropOrdine", "libStr1", "libStr2",
				"libStr3", "libDbl1", "libDbl2", "libDbl3", "libDat1", "libDat2",
				"libDat3", "libLng1", "libLng2", "libLng3"
			});
	private static RigheOrdiniClienti _nullRigheOrdiniClienti = new RigheOrdiniClientiImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<RigheOrdiniClienti> toCacheModel() {
				return _nullRigheOrdiniClientiCacheModel;
			}
		};

	private static CacheModel<RigheOrdiniClienti> _nullRigheOrdiniClientiCacheModel =
		new CacheModel<RigheOrdiniClienti>() {
			@Override
			public RigheOrdiniClienti toEntityModel() {
				return _nullRigheOrdiniClienti;
			}
		};
}