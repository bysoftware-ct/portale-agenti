/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchPianoPagamentiException;
import it.bysoftware.ct.model.PianoPagamenti;
import it.bysoftware.ct.model.impl.PianoPagamentiImpl;
import it.bysoftware.ct.model.impl.PianoPagamentiModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the piano pagamenti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see PianoPagamentiPersistence
 * @see PianoPagamentiUtil
 * @generated
 */
public class PianoPagamentiPersistenceImpl extends BasePersistenceImpl<PianoPagamenti>
	implements PianoPagamentiPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link PianoPagamentiUtil} to access the piano pagamenti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = PianoPagamentiImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PianoPagamentiModelImpl.ENTITY_CACHE_ENABLED,
			PianoPagamentiModelImpl.FINDER_CACHE_ENABLED,
			PianoPagamentiImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PianoPagamentiModelImpl.ENTITY_CACHE_ENABLED,
			PianoPagamentiModelImpl.FINDER_CACHE_ENABLED,
			PianoPagamentiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PianoPagamentiModelImpl.ENTITY_CACHE_ENABLED,
			PianoPagamentiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public PianoPagamentiPersistenceImpl() {
		setModelClass(PianoPagamenti.class);
	}

	/**
	 * Caches the piano pagamenti in the entity cache if it is enabled.
	 *
	 * @param pianoPagamenti the piano pagamenti
	 */
	@Override
	public void cacheResult(PianoPagamenti pianoPagamenti) {
		EntityCacheUtil.putResult(PianoPagamentiModelImpl.ENTITY_CACHE_ENABLED,
			PianoPagamentiImpl.class, pianoPagamenti.getPrimaryKey(),
			pianoPagamenti);

		pianoPagamenti.resetOriginalValues();
	}

	/**
	 * Caches the piano pagamentis in the entity cache if it is enabled.
	 *
	 * @param pianoPagamentis the piano pagamentis
	 */
	@Override
	public void cacheResult(List<PianoPagamenti> pianoPagamentis) {
		for (PianoPagamenti pianoPagamenti : pianoPagamentis) {
			if (EntityCacheUtil.getResult(
						PianoPagamentiModelImpl.ENTITY_CACHE_ENABLED,
						PianoPagamentiImpl.class, pianoPagamenti.getPrimaryKey()) == null) {
				cacheResult(pianoPagamenti);
			}
			else {
				pianoPagamenti.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all piano pagamentis.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(PianoPagamentiImpl.class.getName());
		}

		EntityCacheUtil.clearCache(PianoPagamentiImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the piano pagamenti.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(PianoPagamenti pianoPagamenti) {
		EntityCacheUtil.removeResult(PianoPagamentiModelImpl.ENTITY_CACHE_ENABLED,
			PianoPagamentiImpl.class, pianoPagamenti.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<PianoPagamenti> pianoPagamentis) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (PianoPagamenti pianoPagamenti : pianoPagamentis) {
			EntityCacheUtil.removeResult(PianoPagamentiModelImpl.ENTITY_CACHE_ENABLED,
				PianoPagamentiImpl.class, pianoPagamenti.getPrimaryKey());
		}
	}

	/**
	 * Creates a new piano pagamenti with the primary key. Does not add the piano pagamenti to the database.
	 *
	 * @param codicePianoPagamento the primary key for the new piano pagamenti
	 * @return the new piano pagamenti
	 */
	@Override
	public PianoPagamenti create(String codicePianoPagamento) {
		PianoPagamenti pianoPagamenti = new PianoPagamentiImpl();

		pianoPagamenti.setNew(true);
		pianoPagamenti.setPrimaryKey(codicePianoPagamento);

		return pianoPagamenti;
	}

	/**
	 * Removes the piano pagamenti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param codicePianoPagamento the primary key of the piano pagamenti
	 * @return the piano pagamenti that was removed
	 * @throws it.bysoftware.ct.NoSuchPianoPagamentiException if a piano pagamenti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PianoPagamenti remove(String codicePianoPagamento)
		throws NoSuchPianoPagamentiException, SystemException {
		return remove((Serializable)codicePianoPagamento);
	}

	/**
	 * Removes the piano pagamenti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the piano pagamenti
	 * @return the piano pagamenti that was removed
	 * @throws it.bysoftware.ct.NoSuchPianoPagamentiException if a piano pagamenti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PianoPagamenti remove(Serializable primaryKey)
		throws NoSuchPianoPagamentiException, SystemException {
		Session session = null;

		try {
			session = openSession();

			PianoPagamenti pianoPagamenti = (PianoPagamenti)session.get(PianoPagamentiImpl.class,
					primaryKey);

			if (pianoPagamenti == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchPianoPagamentiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(pianoPagamenti);
		}
		catch (NoSuchPianoPagamentiException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected PianoPagamenti removeImpl(PianoPagamenti pianoPagamenti)
		throws SystemException {
		pianoPagamenti = toUnwrappedModel(pianoPagamenti);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(pianoPagamenti)) {
				pianoPagamenti = (PianoPagamenti)session.get(PianoPagamentiImpl.class,
						pianoPagamenti.getPrimaryKeyObj());
			}

			if (pianoPagamenti != null) {
				session.delete(pianoPagamenti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (pianoPagamenti != null) {
			clearCache(pianoPagamenti);
		}

		return pianoPagamenti;
	}

	@Override
	public PianoPagamenti updateImpl(
		it.bysoftware.ct.model.PianoPagamenti pianoPagamenti)
		throws SystemException {
		pianoPagamenti = toUnwrappedModel(pianoPagamenti);

		boolean isNew = pianoPagamenti.isNew();

		Session session = null;

		try {
			session = openSession();

			if (pianoPagamenti.isNew()) {
				session.save(pianoPagamenti);

				pianoPagamenti.setNew(false);
			}
			else {
				session.merge(pianoPagamenti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(PianoPagamentiModelImpl.ENTITY_CACHE_ENABLED,
			PianoPagamentiImpl.class, pianoPagamenti.getPrimaryKey(),
			pianoPagamenti);

		return pianoPagamenti;
	}

	protected PianoPagamenti toUnwrappedModel(PianoPagamenti pianoPagamenti) {
		if (pianoPagamenti instanceof PianoPagamentiImpl) {
			return pianoPagamenti;
		}

		PianoPagamentiImpl pianoPagamentiImpl = new PianoPagamentiImpl();

		pianoPagamentiImpl.setNew(pianoPagamenti.isNew());
		pianoPagamentiImpl.setPrimaryKey(pianoPagamenti.getPrimaryKey());

		pianoPagamentiImpl.setCodicePianoPagamento(pianoPagamenti.getCodicePianoPagamento());
		pianoPagamentiImpl.setDescrizione(pianoPagamenti.getDescrizione());
		pianoPagamentiImpl.setPrimoMeseEscluso(pianoPagamenti.getPrimoMeseEscluso());
		pianoPagamentiImpl.setGiornoPrimoMeseSucc(pianoPagamenti.getGiornoPrimoMeseSucc());
		pianoPagamentiImpl.setGiornoSecondoMeseSucc(pianoPagamenti.getGiornoSecondoMeseSucc());
		pianoPagamentiImpl.setSecondoMeseEscluso(pianoPagamenti.getSecondoMeseEscluso());
		pianoPagamentiImpl.setInizioFattDaBolla(pianoPagamenti.isInizioFattDaBolla());
		pianoPagamentiImpl.setPercScChiusura(pianoPagamenti.getPercScChiusura());
		pianoPagamentiImpl.setPercScCassa(pianoPagamenti.getPercScCassa());

		return pianoPagamentiImpl;
	}

	/**
	 * Returns the piano pagamenti with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the piano pagamenti
	 * @return the piano pagamenti
	 * @throws it.bysoftware.ct.NoSuchPianoPagamentiException if a piano pagamenti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PianoPagamenti findByPrimaryKey(Serializable primaryKey)
		throws NoSuchPianoPagamentiException, SystemException {
		PianoPagamenti pianoPagamenti = fetchByPrimaryKey(primaryKey);

		if (pianoPagamenti == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchPianoPagamentiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return pianoPagamenti;
	}

	/**
	 * Returns the piano pagamenti with the primary key or throws a {@link it.bysoftware.ct.NoSuchPianoPagamentiException} if it could not be found.
	 *
	 * @param codicePianoPagamento the primary key of the piano pagamenti
	 * @return the piano pagamenti
	 * @throws it.bysoftware.ct.NoSuchPianoPagamentiException if a piano pagamenti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PianoPagamenti findByPrimaryKey(String codicePianoPagamento)
		throws NoSuchPianoPagamentiException, SystemException {
		return findByPrimaryKey((Serializable)codicePianoPagamento);
	}

	/**
	 * Returns the piano pagamenti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the piano pagamenti
	 * @return the piano pagamenti, or <code>null</code> if a piano pagamenti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PianoPagamenti fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		PianoPagamenti pianoPagamenti = (PianoPagamenti)EntityCacheUtil.getResult(PianoPagamentiModelImpl.ENTITY_CACHE_ENABLED,
				PianoPagamentiImpl.class, primaryKey);

		if (pianoPagamenti == _nullPianoPagamenti) {
			return null;
		}

		if (pianoPagamenti == null) {
			Session session = null;

			try {
				session = openSession();

				pianoPagamenti = (PianoPagamenti)session.get(PianoPagamentiImpl.class,
						primaryKey);

				if (pianoPagamenti != null) {
					cacheResult(pianoPagamenti);
				}
				else {
					EntityCacheUtil.putResult(PianoPagamentiModelImpl.ENTITY_CACHE_ENABLED,
						PianoPagamentiImpl.class, primaryKey,
						_nullPianoPagamenti);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(PianoPagamentiModelImpl.ENTITY_CACHE_ENABLED,
					PianoPagamentiImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return pianoPagamenti;
	}

	/**
	 * Returns the piano pagamenti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param codicePianoPagamento the primary key of the piano pagamenti
	 * @return the piano pagamenti, or <code>null</code> if a piano pagamenti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PianoPagamenti fetchByPrimaryKey(String codicePianoPagamento)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)codicePianoPagamento);
	}

	/**
	 * Returns all the piano pagamentis.
	 *
	 * @return the piano pagamentis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PianoPagamenti> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the piano pagamentis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianoPagamentiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of piano pagamentis
	 * @param end the upper bound of the range of piano pagamentis (not inclusive)
	 * @return the range of piano pagamentis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PianoPagamenti> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the piano pagamentis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianoPagamentiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of piano pagamentis
	 * @param end the upper bound of the range of piano pagamentis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of piano pagamentis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PianoPagamenti> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<PianoPagamenti> list = (List<PianoPagamenti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PIANOPAGAMENTI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PIANOPAGAMENTI;

				if (pagination) {
					sql = sql.concat(PianoPagamentiModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<PianoPagamenti>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PianoPagamenti>(list);
				}
				else {
					list = (List<PianoPagamenti>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the piano pagamentis from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (PianoPagamenti pianoPagamenti : findAll()) {
			remove(pianoPagamenti);
		}
	}

	/**
	 * Returns the number of piano pagamentis.
	 *
	 * @return the number of piano pagamentis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PIANOPAGAMENTI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the piano pagamenti persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.PianoPagamenti")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<PianoPagamenti>> listenersList = new ArrayList<ModelListener<PianoPagamenti>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<PianoPagamenti>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(PianoPagamentiImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PIANOPAGAMENTI = "SELECT pianoPagamenti FROM PianoPagamenti pianoPagamenti";
	private static final String _SQL_COUNT_PIANOPAGAMENTI = "SELECT COUNT(pianoPagamenti) FROM PianoPagamenti pianoPagamenti";
	private static final String _ORDER_BY_ENTITY_ALIAS = "pianoPagamenti.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PianoPagamenti exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(PianoPagamentiPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"codicePianoPagamento", "descrizione", "primoMeseEscluso",
				"giornoPrimoMeseSucc", "giornoSecondoMeseSucc",
				"secondoMeseEscluso", "inizioFattDaBolla", "percScChiusura",
				"percScCassa"
			});
	private static PianoPagamenti _nullPianoPagamenti = new PianoPagamentiImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<PianoPagamenti> toCacheModel() {
				return _nullPianoPagamentiCacheModel;
			}
		};

	private static CacheModel<PianoPagamenti> _nullPianoPagamentiCacheModel = new CacheModel<PianoPagamenti>() {
			@Override
			public PianoPagamenti toEntityModel() {
				return _nullPianoPagamenti;
			}
		};
}