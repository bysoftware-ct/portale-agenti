/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchEvasioniOrdiniException;
import it.bysoftware.ct.model.EvasioniOrdini;
import it.bysoftware.ct.model.impl.EvasioniOrdiniImpl;
import it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the evasioni ordini service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see EvasioniOrdiniPersistence
 * @see EvasioniOrdiniUtil
 * @generated
 */
public class EvasioniOrdiniPersistenceImpl extends BasePersistenceImpl<EvasioniOrdini>
	implements EvasioniOrdiniPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EvasioniOrdiniUtil} to access the evasioni ordini persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EvasioniOrdiniImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EvasioniOrdiniModelImpl.ENTITY_CACHE_ENABLED,
			EvasioniOrdiniModelImpl.FINDER_CACHE_ENABLED,
			EvasioniOrdiniImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EvasioniOrdiniModelImpl.ENTITY_CACHE_ENABLED,
			EvasioniOrdiniModelImpl.FINDER_CACHE_ENABLED,
			EvasioniOrdiniImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EvasioniOrdiniModelImpl.ENTITY_CACHE_ENABLED,
			EvasioniOrdiniModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CLIENTEANDORDINE =
		new FinderPath(EvasioniOrdiniModelImpl.ENTITY_CACHE_ENABLED,
			EvasioniOrdiniModelImpl.FINDER_CACHE_ENABLED,
			EvasioniOrdiniImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByClienteAndOrdine",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				String.class.getName(), String.class.getName(),
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_CLIENTEANDORDINE =
		new FinderPath(EvasioniOrdiniModelImpl.ENTITY_CACHE_ENABLED,
			EvasioniOrdiniModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByClienteAndOrdine",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				String.class.getName(), String.class.getName(),
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), String.class.getName()
			});

	/**
	 * Returns all the evasioni ordinis where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param codiceArticolo the codice articolo
	 * @return the matching evasioni ordinis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvasioniOrdini> findByClienteAndOrdine(String codiceCliente,
		int anno, String codiceAttivita, String codiceCentro,
		String codiceDeposito, int tipoOrdine, int numeroOrdine,
		String codiceArticolo) throws SystemException {
		return findByClienteAndOrdine(codiceCliente, anno, codiceAttivita,
			codiceCentro, codiceDeposito, tipoOrdine, numeroOrdine,
			codiceArticolo, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the evasioni ordinis where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceCliente the codice cliente
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param codiceArticolo the codice articolo
	 * @param start the lower bound of the range of evasioni ordinis
	 * @param end the upper bound of the range of evasioni ordinis (not inclusive)
	 * @return the range of matching evasioni ordinis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvasioniOrdini> findByClienteAndOrdine(String codiceCliente,
		int anno, String codiceAttivita, String codiceCentro,
		String codiceDeposito, int tipoOrdine, int numeroOrdine,
		String codiceArticolo, int start, int end) throws SystemException {
		return findByClienteAndOrdine(codiceCliente, anno, codiceAttivita,
			codiceCentro, codiceDeposito, tipoOrdine, numeroOrdine,
			codiceArticolo, start, end, null);
	}

	/**
	 * Returns an ordered range of all the evasioni ordinis where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceCliente the codice cliente
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param codiceArticolo the codice articolo
	 * @param start the lower bound of the range of evasioni ordinis
	 * @param end the upper bound of the range of evasioni ordinis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching evasioni ordinis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvasioniOrdini> findByClienteAndOrdine(String codiceCliente,
		int anno, String codiceAttivita, String codiceCentro,
		String codiceDeposito, int tipoOrdine, int numeroOrdine,
		String codiceArticolo, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CLIENTEANDORDINE;
		finderArgs = new Object[] {
				codiceCliente, anno, codiceAttivita, codiceCentro,
				codiceDeposito, tipoOrdine, numeroOrdine, codiceArticolo,
				
				start, end, orderByComparator
			};

		List<EvasioniOrdini> list = (List<EvasioniOrdini>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EvasioniOrdini evasioniOrdini : list) {
				if (!Validator.equals(codiceCliente,
							evasioniOrdini.getCodiceCliente()) ||
						(anno != evasioniOrdini.getAnno()) ||
						!Validator.equals(codiceAttivita,
							evasioniOrdini.getCodiceAttivita()) ||
						!Validator.equals(codiceCentro,
							evasioniOrdini.getCodiceCentro()) ||
						!Validator.equals(codiceDeposito,
							evasioniOrdini.getCodiceDeposito()) ||
						(tipoOrdine != evasioniOrdini.getTipoOrdine()) ||
						(numeroOrdine != evasioniOrdini.getNumeroOrdine()) ||
						Validator.equals(codiceArticolo,
							evasioniOrdini.getCodiceArticolo())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(10 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(10);
			}

			query.append(_SQL_SELECT_EVASIONIORDINI_WHERE);

			boolean bindCodiceCliente = false;

			if (codiceCliente == null) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECLIENTE_1);
			}
			else if (codiceCliente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECLIENTE_3);
			}
			else {
				bindCodiceCliente = true;

				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECLIENTE_2);
			}

			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_ANNO_2);

			boolean bindCodiceAttivita = false;

			if (codiceAttivita == null) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEATTIVITA_1);
			}
			else if (codiceAttivita.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEATTIVITA_3);
			}
			else {
				bindCodiceAttivita = true;

				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEATTIVITA_2);
			}

			boolean bindCodiceCentro = false;

			if (codiceCentro == null) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECENTRO_1);
			}
			else if (codiceCentro.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECENTRO_3);
			}
			else {
				bindCodiceCentro = true;

				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECENTRO_2);
			}

			boolean bindCodiceDeposito = false;

			if (codiceDeposito == null) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEDEPOSITO_1);
			}
			else if (codiceDeposito.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEDEPOSITO_3);
			}
			else {
				bindCodiceDeposito = true;

				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEDEPOSITO_2);
			}

			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_TIPOORDINE_2);

			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_NUMEROORDINE_2);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEARTICOLO_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EvasioniOrdiniModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceCliente) {
					qPos.add(codiceCliente);
				}

				qPos.add(anno);

				if (bindCodiceAttivita) {
					qPos.add(codiceAttivita);
				}

				if (bindCodiceCentro) {
					qPos.add(codiceCentro);
				}

				if (bindCodiceDeposito) {
					qPos.add(codiceDeposito);
				}

				qPos.add(tipoOrdine);

				qPos.add(numeroOrdine);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (!pagination) {
					list = (List<EvasioniOrdini>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EvasioniOrdini>(list);
				}
				else {
					list = (List<EvasioniOrdini>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first evasioni ordini in the ordered set where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param codiceArticolo the codice articolo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evasioni ordini
	 * @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a matching evasioni ordini could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvasioniOrdini findByClienteAndOrdine_First(String codiceCliente,
		int anno, String codiceAttivita, String codiceCentro,
		String codiceDeposito, int tipoOrdine, int numeroOrdine,
		String codiceArticolo, OrderByComparator orderByComparator)
		throws NoSuchEvasioniOrdiniException, SystemException {
		EvasioniOrdini evasioniOrdini = fetchByClienteAndOrdine_First(codiceCliente,
				anno, codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
				numeroOrdine, codiceArticolo, orderByComparator);

		if (evasioniOrdini != null) {
			return evasioniOrdini;
		}

		StringBundler msg = new StringBundler(18);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceCliente=");
		msg.append(codiceCliente);

		msg.append(", anno=");
		msg.append(anno);

		msg.append(", codiceAttivita=");
		msg.append(codiceAttivita);

		msg.append(", codiceCentro=");
		msg.append(codiceCentro);

		msg.append(", codiceDeposito=");
		msg.append(codiceDeposito);

		msg.append(", tipoOrdine=");
		msg.append(tipoOrdine);

		msg.append(", numeroOrdine=");
		msg.append(numeroOrdine);

		msg.append(", codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvasioniOrdiniException(msg.toString());
	}

	/**
	 * Returns the first evasioni ordini in the ordered set where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param codiceArticolo the codice articolo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evasioni ordini, or <code>null</code> if a matching evasioni ordini could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvasioniOrdini fetchByClienteAndOrdine_First(String codiceCliente,
		int anno, String codiceAttivita, String codiceCentro,
		String codiceDeposito, int tipoOrdine, int numeroOrdine,
		String codiceArticolo, OrderByComparator orderByComparator)
		throws SystemException {
		List<EvasioniOrdini> list = findByClienteAndOrdine(codiceCliente, anno,
				codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
				numeroOrdine, codiceArticolo, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last evasioni ordini in the ordered set where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param codiceArticolo the codice articolo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evasioni ordini
	 * @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a matching evasioni ordini could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvasioniOrdini findByClienteAndOrdine_Last(String codiceCliente,
		int anno, String codiceAttivita, String codiceCentro,
		String codiceDeposito, int tipoOrdine, int numeroOrdine,
		String codiceArticolo, OrderByComparator orderByComparator)
		throws NoSuchEvasioniOrdiniException, SystemException {
		EvasioniOrdini evasioniOrdini = fetchByClienteAndOrdine_Last(codiceCliente,
				anno, codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
				numeroOrdine, codiceArticolo, orderByComparator);

		if (evasioniOrdini != null) {
			return evasioniOrdini;
		}

		StringBundler msg = new StringBundler(18);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceCliente=");
		msg.append(codiceCliente);

		msg.append(", anno=");
		msg.append(anno);

		msg.append(", codiceAttivita=");
		msg.append(codiceAttivita);

		msg.append(", codiceCentro=");
		msg.append(codiceCentro);

		msg.append(", codiceDeposito=");
		msg.append(codiceDeposito);

		msg.append(", tipoOrdine=");
		msg.append(tipoOrdine);

		msg.append(", numeroOrdine=");
		msg.append(numeroOrdine);

		msg.append(", codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvasioniOrdiniException(msg.toString());
	}

	/**
	 * Returns the last evasioni ordini in the ordered set where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param codiceArticolo the codice articolo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evasioni ordini, or <code>null</code> if a matching evasioni ordini could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvasioniOrdini fetchByClienteAndOrdine_Last(String codiceCliente,
		int anno, String codiceAttivita, String codiceCentro,
		String codiceDeposito, int tipoOrdine, int numeroOrdine,
		String codiceArticolo, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByClienteAndOrdine(codiceCliente, anno,
				codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
				numeroOrdine, codiceArticolo);

		if (count == 0) {
			return null;
		}

		List<EvasioniOrdini> list = findByClienteAndOrdine(codiceCliente, anno,
				codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
				numeroOrdine, codiceArticolo, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the evasioni ordinis before and after the current evasioni ordini in the ordered set where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	 *
	 * @param ID the primary key of the current evasioni ordini
	 * @param codiceCliente the codice cliente
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param codiceArticolo the codice articolo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next evasioni ordini
	 * @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a evasioni ordini with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvasioniOrdini[] findByClienteAndOrdine_PrevAndNext(String ID,
		String codiceCliente, int anno, String codiceAttivita,
		String codiceCentro, String codiceDeposito, int tipoOrdine,
		int numeroOrdine, String codiceArticolo,
		OrderByComparator orderByComparator)
		throws NoSuchEvasioniOrdiniException, SystemException {
		EvasioniOrdini evasioniOrdini = findByPrimaryKey(ID);

		Session session = null;

		try {
			session = openSession();

			EvasioniOrdini[] array = new EvasioniOrdiniImpl[3];

			array[0] = getByClienteAndOrdine_PrevAndNext(session,
					evasioniOrdini, codiceCliente, anno, codiceAttivita,
					codiceCentro, codiceDeposito, tipoOrdine, numeroOrdine,
					codiceArticolo, orderByComparator, true);

			array[1] = evasioniOrdini;

			array[2] = getByClienteAndOrdine_PrevAndNext(session,
					evasioniOrdini, codiceCliente, anno, codiceAttivita,
					codiceCentro, codiceDeposito, tipoOrdine, numeroOrdine,
					codiceArticolo, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EvasioniOrdini getByClienteAndOrdine_PrevAndNext(
		Session session, EvasioniOrdini evasioniOrdini, String codiceCliente,
		int anno, String codiceAttivita, String codiceCentro,
		String codiceDeposito, int tipoOrdine, int numeroOrdine,
		String codiceArticolo, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EVASIONIORDINI_WHERE);

		boolean bindCodiceCliente = false;

		if (codiceCliente == null) {
			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECLIENTE_1);
		}
		else if (codiceCliente.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECLIENTE_3);
		}
		else {
			bindCodiceCliente = true;

			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECLIENTE_2);
		}

		query.append(_FINDER_COLUMN_CLIENTEANDORDINE_ANNO_2);

		boolean bindCodiceAttivita = false;

		if (codiceAttivita == null) {
			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEATTIVITA_1);
		}
		else if (codiceAttivita.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEATTIVITA_3);
		}
		else {
			bindCodiceAttivita = true;

			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEATTIVITA_2);
		}

		boolean bindCodiceCentro = false;

		if (codiceCentro == null) {
			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECENTRO_1);
		}
		else if (codiceCentro.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECENTRO_3);
		}
		else {
			bindCodiceCentro = true;

			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECENTRO_2);
		}

		boolean bindCodiceDeposito = false;

		if (codiceDeposito == null) {
			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEDEPOSITO_1);
		}
		else if (codiceDeposito.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEDEPOSITO_3);
		}
		else {
			bindCodiceDeposito = true;

			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEDEPOSITO_2);
		}

		query.append(_FINDER_COLUMN_CLIENTEANDORDINE_TIPOORDINE_2);

		query.append(_FINDER_COLUMN_CLIENTEANDORDINE_NUMEROORDINE_2);

		boolean bindCodiceArticolo = false;

		if (codiceArticolo == null) {
			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEARTICOLO_1);
		}
		else if (codiceArticolo.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEARTICOLO_3);
		}
		else {
			bindCodiceArticolo = true;

			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEARTICOLO_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EvasioniOrdiniModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodiceCliente) {
			qPos.add(codiceCliente);
		}

		qPos.add(anno);

		if (bindCodiceAttivita) {
			qPos.add(codiceAttivita);
		}

		if (bindCodiceCentro) {
			qPos.add(codiceCentro);
		}

		if (bindCodiceDeposito) {
			qPos.add(codiceDeposito);
		}

		qPos.add(tipoOrdine);

		qPos.add(numeroOrdine);

		if (bindCodiceArticolo) {
			qPos.add(codiceArticolo);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(evasioniOrdini);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EvasioniOrdini> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the evasioni ordinis where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63; from the database.
	 *
	 * @param codiceCliente the codice cliente
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param codiceArticolo the codice articolo
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByClienteAndOrdine(String codiceCliente, int anno,
		String codiceAttivita, String codiceCentro, String codiceDeposito,
		int tipoOrdine, int numeroOrdine, String codiceArticolo)
		throws SystemException {
		for (EvasioniOrdini evasioniOrdini : findByClienteAndOrdine(
				codiceCliente, anno, codiceAttivita, codiceCentro,
				codiceDeposito, tipoOrdine, numeroOrdine, codiceArticolo,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(evasioniOrdini);
		}
	}

	/**
	 * Returns the number of evasioni ordinis where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param codiceDeposito the codice deposito
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param codiceArticolo the codice articolo
	 * @return the number of matching evasioni ordinis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByClienteAndOrdine(String codiceCliente, int anno,
		String codiceAttivita, String codiceCentro, String codiceDeposito,
		int tipoOrdine, int numeroOrdine, String codiceArticolo)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_CLIENTEANDORDINE;

		Object[] finderArgs = new Object[] {
				codiceCliente, anno, codiceAttivita, codiceCentro,
				codiceDeposito, tipoOrdine, numeroOrdine, codiceArticolo
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(9);

			query.append(_SQL_COUNT_EVASIONIORDINI_WHERE);

			boolean bindCodiceCliente = false;

			if (codiceCliente == null) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECLIENTE_1);
			}
			else if (codiceCliente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECLIENTE_3);
			}
			else {
				bindCodiceCliente = true;

				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECLIENTE_2);
			}

			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_ANNO_2);

			boolean bindCodiceAttivita = false;

			if (codiceAttivita == null) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEATTIVITA_1);
			}
			else if (codiceAttivita.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEATTIVITA_3);
			}
			else {
				bindCodiceAttivita = true;

				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEATTIVITA_2);
			}

			boolean bindCodiceCentro = false;

			if (codiceCentro == null) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECENTRO_1);
			}
			else if (codiceCentro.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECENTRO_3);
			}
			else {
				bindCodiceCentro = true;

				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICECENTRO_2);
			}

			boolean bindCodiceDeposito = false;

			if (codiceDeposito == null) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEDEPOSITO_1);
			}
			else if (codiceDeposito.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEDEPOSITO_3);
			}
			else {
				bindCodiceDeposito = true;

				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEDEPOSITO_2);
			}

			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_TIPOORDINE_2);

			query.append(_FINDER_COLUMN_CLIENTEANDORDINE_NUMEROORDINE_2);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_CLIENTEANDORDINE_CODICEARTICOLO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceCliente) {
					qPos.add(codiceCliente);
				}

				qPos.add(anno);

				if (bindCodiceAttivita) {
					qPos.add(codiceAttivita);
				}

				if (bindCodiceCentro) {
					qPos.add(codiceCentro);
				}

				if (bindCodiceDeposito) {
					qPos.add(codiceDeposito);
				}

				qPos.add(tipoOrdine);

				qPos.add(numeroOrdine);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_CODICECLIENTE_1 = "evasioniOrdini.codiceCliente IS NULL AND ";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_CODICECLIENTE_2 = "evasioniOrdini.codiceCliente = ? AND ";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_CODICECLIENTE_3 = "(evasioniOrdini.codiceCliente IS NULL OR evasioniOrdini.codiceCliente = '') AND ";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_ANNO_2 = "evasioniOrdini.anno = ? AND ";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_CODICEATTIVITA_1 =
		"evasioniOrdini.codiceAttivita IS NULL AND ";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_CODICEATTIVITA_2 =
		"evasioniOrdini.codiceAttivita = ? AND ";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_CODICEATTIVITA_3 =
		"(evasioniOrdini.codiceAttivita IS NULL OR evasioniOrdini.codiceAttivita = '') AND ";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_CODICECENTRO_1 = "evasioniOrdini.codiceCentro IS NULL AND ";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_CODICECENTRO_2 = "evasioniOrdini.codiceCentro = ? AND ";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_CODICECENTRO_3 = "(evasioniOrdini.codiceCentro IS NULL OR evasioniOrdini.codiceCentro = '') AND ";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_CODICEDEPOSITO_1 =
		"evasioniOrdini.codiceDeposito IS NULL AND ";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_CODICEDEPOSITO_2 =
		"evasioniOrdini.codiceDeposito = ? AND ";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_CODICEDEPOSITO_3 =
		"(evasioniOrdini.codiceDeposito IS NULL OR evasioniOrdini.codiceDeposito = '') AND ";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_TIPOORDINE_2 = "evasioniOrdini.tipoOrdine = ? AND ";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_NUMEROORDINE_2 = "evasioniOrdini.numeroOrdine = ? AND ";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_CODICEARTICOLO_1 =
		"evasioniOrdini.codiceArticolo IS NOT NULL";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_CODICEARTICOLO_2 =
		"evasioniOrdini.codiceArticolo != ?";
	private static final String _FINDER_COLUMN_CLIENTEANDORDINE_CODICEARTICOLO_3 =
		"(evasioniOrdini.codiceArticolo IS NULL OR evasioniOrdini.codiceArticolo != '')";

	public EvasioniOrdiniPersistenceImpl() {
		setModelClass(EvasioniOrdini.class);
	}

	/**
	 * Caches the evasioni ordini in the entity cache if it is enabled.
	 *
	 * @param evasioniOrdini the evasioni ordini
	 */
	@Override
	public void cacheResult(EvasioniOrdini evasioniOrdini) {
		EntityCacheUtil.putResult(EvasioniOrdiniModelImpl.ENTITY_CACHE_ENABLED,
			EvasioniOrdiniImpl.class, evasioniOrdini.getPrimaryKey(),
			evasioniOrdini);

		evasioniOrdini.resetOriginalValues();
	}

	/**
	 * Caches the evasioni ordinis in the entity cache if it is enabled.
	 *
	 * @param evasioniOrdinis the evasioni ordinis
	 */
	@Override
	public void cacheResult(List<EvasioniOrdini> evasioniOrdinis) {
		for (EvasioniOrdini evasioniOrdini : evasioniOrdinis) {
			if (EntityCacheUtil.getResult(
						EvasioniOrdiniModelImpl.ENTITY_CACHE_ENABLED,
						EvasioniOrdiniImpl.class, evasioniOrdini.getPrimaryKey()) == null) {
				cacheResult(evasioniOrdini);
			}
			else {
				evasioniOrdini.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all evasioni ordinis.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(EvasioniOrdiniImpl.class.getName());
		}

		EntityCacheUtil.clearCache(EvasioniOrdiniImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the evasioni ordini.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EvasioniOrdini evasioniOrdini) {
		EntityCacheUtil.removeResult(EvasioniOrdiniModelImpl.ENTITY_CACHE_ENABLED,
			EvasioniOrdiniImpl.class, evasioniOrdini.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<EvasioniOrdini> evasioniOrdinis) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (EvasioniOrdini evasioniOrdini : evasioniOrdinis) {
			EntityCacheUtil.removeResult(EvasioniOrdiniModelImpl.ENTITY_CACHE_ENABLED,
				EvasioniOrdiniImpl.class, evasioniOrdini.getPrimaryKey());
		}
	}

	/**
	 * Creates a new evasioni ordini with the primary key. Does not add the evasioni ordini to the database.
	 *
	 * @param ID the primary key for the new evasioni ordini
	 * @return the new evasioni ordini
	 */
	@Override
	public EvasioniOrdini create(String ID) {
		EvasioniOrdini evasioniOrdini = new EvasioniOrdiniImpl();

		evasioniOrdini.setNew(true);
		evasioniOrdini.setPrimaryKey(ID);

		return evasioniOrdini;
	}

	/**
	 * Removes the evasioni ordini with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ID the primary key of the evasioni ordini
	 * @return the evasioni ordini that was removed
	 * @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a evasioni ordini with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvasioniOrdini remove(String ID)
		throws NoSuchEvasioniOrdiniException, SystemException {
		return remove((Serializable)ID);
	}

	/**
	 * Removes the evasioni ordini with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the evasioni ordini
	 * @return the evasioni ordini that was removed
	 * @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a evasioni ordini with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvasioniOrdini remove(Serializable primaryKey)
		throws NoSuchEvasioniOrdiniException, SystemException {
		Session session = null;

		try {
			session = openSession();

			EvasioniOrdini evasioniOrdini = (EvasioniOrdini)session.get(EvasioniOrdiniImpl.class,
					primaryKey);

			if (evasioniOrdini == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEvasioniOrdiniException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(evasioniOrdini);
		}
		catch (NoSuchEvasioniOrdiniException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EvasioniOrdini removeImpl(EvasioniOrdini evasioniOrdini)
		throws SystemException {
		evasioniOrdini = toUnwrappedModel(evasioniOrdini);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(evasioniOrdini)) {
				evasioniOrdini = (EvasioniOrdini)session.get(EvasioniOrdiniImpl.class,
						evasioniOrdini.getPrimaryKeyObj());
			}

			if (evasioniOrdini != null) {
				session.delete(evasioniOrdini);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (evasioniOrdini != null) {
			clearCache(evasioniOrdini);
		}

		return evasioniOrdini;
	}

	@Override
	public EvasioniOrdini updateImpl(
		it.bysoftware.ct.model.EvasioniOrdini evasioniOrdini)
		throws SystemException {
		evasioniOrdini = toUnwrappedModel(evasioniOrdini);

		boolean isNew = evasioniOrdini.isNew();

		Session session = null;

		try {
			session = openSession();

			if (evasioniOrdini.isNew()) {
				session.save(evasioniOrdini);

				evasioniOrdini.setNew(false);
			}
			else {
				session.merge(evasioniOrdini);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !EvasioniOrdiniModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(EvasioniOrdiniModelImpl.ENTITY_CACHE_ENABLED,
			EvasioniOrdiniImpl.class, evasioniOrdini.getPrimaryKey(),
			evasioniOrdini);

		return evasioniOrdini;
	}

	protected EvasioniOrdini toUnwrappedModel(EvasioniOrdini evasioniOrdini) {
		if (evasioniOrdini instanceof EvasioniOrdiniImpl) {
			return evasioniOrdini;
		}

		EvasioniOrdiniImpl evasioniOrdiniImpl = new EvasioniOrdiniImpl();

		evasioniOrdiniImpl.setNew(evasioniOrdini.isNew());
		evasioniOrdiniImpl.setPrimaryKey(evasioniOrdini.getPrimaryKey());

		evasioniOrdiniImpl.setID(evasioniOrdini.getID());
		evasioniOrdiniImpl.setAnno(evasioniOrdini.getAnno());
		evasioniOrdiniImpl.setCodiceAttivita(evasioniOrdini.getCodiceAttivita());
		evasioniOrdiniImpl.setCodiceCentro(evasioniOrdini.getCodiceCentro());
		evasioniOrdiniImpl.setCodiceDeposito(evasioniOrdini.getCodiceDeposito());
		evasioniOrdiniImpl.setTipoOrdine(evasioniOrdini.getTipoOrdine());
		evasioniOrdiniImpl.setNumeroOrdine(evasioniOrdini.getNumeroOrdine());
		evasioniOrdiniImpl.setCodiceCliente(evasioniOrdini.getCodiceCliente());
		evasioniOrdiniImpl.setNumeroRigo(evasioniOrdini.getNumeroRigo());
		evasioniOrdiniImpl.setCodiceArticolo(evasioniOrdini.getCodiceArticolo());
		evasioniOrdiniImpl.setCodiceVariante(evasioniOrdini.getCodiceVariante());
		evasioniOrdiniImpl.setDescrizione(evasioniOrdini.getDescrizione());
		evasioniOrdiniImpl.setUnitaMisura(evasioniOrdini.getUnitaMisura());
		evasioniOrdiniImpl.setQuantitaOrdinata(evasioniOrdini.getQuantitaOrdinata());
		evasioniOrdiniImpl.setNumeroProgrezzivoEvasione(evasioniOrdini.getNumeroProgrezzivoEvasione());
		evasioniOrdiniImpl.setQuantitaConsegnata(evasioniOrdini.getQuantitaConsegnata());
		evasioniOrdiniImpl.setDataDocumentoConsegna(evasioniOrdini.getDataDocumentoConsegna());
		evasioniOrdiniImpl.setTestAccontoSaldo(evasioniOrdini.getTestAccontoSaldo());
		evasioniOrdiniImpl.setTipoDocumentoEvasione(evasioniOrdini.getTipoDocumentoEvasione());
		evasioniOrdiniImpl.setNumeroDocumentoEvasione(evasioniOrdini.getNumeroDocumentoEvasione());

		return evasioniOrdiniImpl;
	}

	/**
	 * Returns the evasioni ordini with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the evasioni ordini
	 * @return the evasioni ordini
	 * @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a evasioni ordini with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvasioniOrdini findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEvasioniOrdiniException, SystemException {
		EvasioniOrdini evasioniOrdini = fetchByPrimaryKey(primaryKey);

		if (evasioniOrdini == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEvasioniOrdiniException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return evasioniOrdini;
	}

	/**
	 * Returns the evasioni ordini with the primary key or throws a {@link it.bysoftware.ct.NoSuchEvasioniOrdiniException} if it could not be found.
	 *
	 * @param ID the primary key of the evasioni ordini
	 * @return the evasioni ordini
	 * @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a evasioni ordini with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvasioniOrdini findByPrimaryKey(String ID)
		throws NoSuchEvasioniOrdiniException, SystemException {
		return findByPrimaryKey((Serializable)ID);
	}

	/**
	 * Returns the evasioni ordini with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the evasioni ordini
	 * @return the evasioni ordini, or <code>null</code> if a evasioni ordini with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvasioniOrdini fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		EvasioniOrdini evasioniOrdini = (EvasioniOrdini)EntityCacheUtil.getResult(EvasioniOrdiniModelImpl.ENTITY_CACHE_ENABLED,
				EvasioniOrdiniImpl.class, primaryKey);

		if (evasioniOrdini == _nullEvasioniOrdini) {
			return null;
		}

		if (evasioniOrdini == null) {
			Session session = null;

			try {
				session = openSession();

				evasioniOrdini = (EvasioniOrdini)session.get(EvasioniOrdiniImpl.class,
						primaryKey);

				if (evasioniOrdini != null) {
					cacheResult(evasioniOrdini);
				}
				else {
					EntityCacheUtil.putResult(EvasioniOrdiniModelImpl.ENTITY_CACHE_ENABLED,
						EvasioniOrdiniImpl.class, primaryKey,
						_nullEvasioniOrdini);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(EvasioniOrdiniModelImpl.ENTITY_CACHE_ENABLED,
					EvasioniOrdiniImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return evasioniOrdini;
	}

	/**
	 * Returns the evasioni ordini with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ID the primary key of the evasioni ordini
	 * @return the evasioni ordini, or <code>null</code> if a evasioni ordini with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvasioniOrdini fetchByPrimaryKey(String ID)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)ID);
	}

	/**
	 * Returns all the evasioni ordinis.
	 *
	 * @return the evasioni ordinis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvasioniOrdini> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the evasioni ordinis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of evasioni ordinis
	 * @param end the upper bound of the range of evasioni ordinis (not inclusive)
	 * @return the range of evasioni ordinis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvasioniOrdini> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the evasioni ordinis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of evasioni ordinis
	 * @param end the upper bound of the range of evasioni ordinis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of evasioni ordinis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvasioniOrdini> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<EvasioniOrdini> list = (List<EvasioniOrdini>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_EVASIONIORDINI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_EVASIONIORDINI;

				if (pagination) {
					sql = sql.concat(EvasioniOrdiniModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<EvasioniOrdini>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EvasioniOrdini>(list);
				}
				else {
					list = (List<EvasioniOrdini>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the evasioni ordinis from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (EvasioniOrdini evasioniOrdini : findAll()) {
			remove(evasioniOrdini);
		}
	}

	/**
	 * Returns the number of evasioni ordinis.
	 *
	 * @return the number of evasioni ordinis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_EVASIONIORDINI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the evasioni ordini persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.EvasioniOrdini")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<EvasioniOrdini>> listenersList = new ArrayList<ModelListener<EvasioniOrdini>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<EvasioniOrdini>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(EvasioniOrdiniImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_EVASIONIORDINI = "SELECT evasioniOrdini FROM EvasioniOrdini evasioniOrdini";
	private static final String _SQL_SELECT_EVASIONIORDINI_WHERE = "SELECT evasioniOrdini FROM EvasioniOrdini evasioniOrdini WHERE ";
	private static final String _SQL_COUNT_EVASIONIORDINI = "SELECT COUNT(evasioniOrdini) FROM EvasioniOrdini evasioniOrdini";
	private static final String _SQL_COUNT_EVASIONIORDINI_WHERE = "SELECT COUNT(evasioniOrdini) FROM EvasioniOrdini evasioniOrdini WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "evasioniOrdini.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No EvasioniOrdini exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No EvasioniOrdini exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(EvasioniOrdiniPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"anno", "codiceAttivita", "codiceCentro", "codiceDeposito",
				"tipoOrdine", "numeroOrdine", "codiceCliente", "numeroRigo",
				"codiceArticolo", "codiceVariante", "descrizione", "unitaMisura",
				"quantitaOrdinata", "numeroProgrezzivoEvasione",
				"quantitaConsegnata", "dataDocumentoConsegna",
				"testAccontoSaldo", "tipoDocumentoEvasione",
				"numeroDocumentoEvasione"
			});
	private static EvasioniOrdini _nullEvasioniOrdini = new EvasioniOrdiniImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<EvasioniOrdini> toCacheModel() {
				return _nullEvasioniOrdiniCacheModel;
			}
		};

	private static CacheModel<EvasioniOrdini> _nullEvasioniOrdiniCacheModel = new CacheModel<EvasioniOrdini>() {
			@Override
			public EvasioniOrdini toEntityModel() {
				return _nullEvasioniOrdini;
			}
		};
}