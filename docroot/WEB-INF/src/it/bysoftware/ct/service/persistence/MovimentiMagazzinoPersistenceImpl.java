/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchMovimentiMagazzinoException;
import it.bysoftware.ct.model.MovimentiMagazzino;
import it.bysoftware.ct.model.impl.MovimentiMagazzinoImpl;
import it.bysoftware.ct.model.impl.MovimentiMagazzinoModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the movimenti magazzino service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see MovimentiMagazzinoPersistence
 * @see MovimentiMagazzinoUtil
 * @generated
 */
public class MovimentiMagazzinoPersistenceImpl extends BasePersistenceImpl<MovimentiMagazzino>
	implements MovimentiMagazzinoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link MovimentiMagazzinoUtil} to access the movimenti magazzino persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = MovimentiMagazzinoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			MovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED,
			MovimentiMagazzinoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			MovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED,
			MovimentiMagazzinoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			MovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTICOLOVARIANTE =
		new FinderPath(MovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			MovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED,
			MovimentiMagazzinoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByArticoloVariante",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				String.class.getName(), Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_ARTICOLOVARIANTE =
		new FinderPath(MovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			MovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByArticoloVariante",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				String.class.getName(), Integer.class.getName()
			});

	/**
	 * Returns all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @return the matching movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MovimentiMagazzino> findByArticoloVariante(int anno,
		String codiceArticolo, String codiceVariante, int soloValore)
		throws SystemException {
		return findByArticoloVariante(anno, codiceArticolo, codiceVariante,
			soloValore, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @param start the lower bound of the range of movimenti magazzinos
	 * @param end the upper bound of the range of movimenti magazzinos (not inclusive)
	 * @return the range of matching movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MovimentiMagazzino> findByArticoloVariante(int anno,
		String codiceArticolo, String codiceVariante, int soloValore,
		int start, int end) throws SystemException {
		return findByArticoloVariante(anno, codiceArticolo, codiceVariante,
			soloValore, start, end, null);
	}

	/**
	 * Returns an ordered range of all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @param start the lower bound of the range of movimenti magazzinos
	 * @param end the upper bound of the range of movimenti magazzinos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MovimentiMagazzino> findByArticoloVariante(int anno,
		String codiceArticolo, String codiceVariante, int soloValore,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTICOLOVARIANTE;
		finderArgs = new Object[] {
				anno, codiceArticolo, codiceVariante, soloValore,
				
				start, end, orderByComparator
			};

		List<MovimentiMagazzino> list = (List<MovimentiMagazzino>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (MovimentiMagazzino movimentiMagazzino : list) {
				if ((anno != movimentiMagazzino.getAnno()) ||
						!Validator.equals(codiceArticolo,
							movimentiMagazzino.getCodiceArticolo()) ||
						!Validator.equals(codiceVariante,
							movimentiMagazzino.getCodiceVariante()) ||
						(soloValore == movimentiMagazzino.getSoloValore())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(6 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(6);
			}

			query.append(_SQL_SELECT_MOVIMENTIMAGAZZINO_WHERE);

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_ANNO_2);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_2);
			}

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_SOLOVALORE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(MovimentiMagazzinoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(anno);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				qPos.add(soloValore);

				if (!pagination) {
					list = (List<MovimentiMagazzino>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<MovimentiMagazzino>(list);
				}
				else {
					list = (List<MovimentiMagazzino>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a matching movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino findByArticoloVariante_First(int anno,
		String codiceArticolo, String codiceVariante, int soloValore,
		OrderByComparator orderByComparator)
		throws NoSuchMovimentiMagazzinoException, SystemException {
		MovimentiMagazzino movimentiMagazzino = fetchByArticoloVariante_First(anno,
				codiceArticolo, codiceVariante, soloValore, orderByComparator);

		if (movimentiMagazzino != null) {
			return movimentiMagazzino;
		}

		StringBundler msg = new StringBundler(10);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("anno=");
		msg.append(anno);

		msg.append(", codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(", soloValore=");
		msg.append(soloValore);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMovimentiMagazzinoException(msg.toString());
	}

	/**
	 * Returns the first movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching movimenti magazzino, or <code>null</code> if a matching movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino fetchByArticoloVariante_First(int anno,
		String codiceArticolo, String codiceVariante, int soloValore,
		OrderByComparator orderByComparator) throws SystemException {
		List<MovimentiMagazzino> list = findByArticoloVariante(anno,
				codiceArticolo, codiceVariante, soloValore, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a matching movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino findByArticoloVariante_Last(int anno,
		String codiceArticolo, String codiceVariante, int soloValore,
		OrderByComparator orderByComparator)
		throws NoSuchMovimentiMagazzinoException, SystemException {
		MovimentiMagazzino movimentiMagazzino = fetchByArticoloVariante_Last(anno,
				codiceArticolo, codiceVariante, soloValore, orderByComparator);

		if (movimentiMagazzino != null) {
			return movimentiMagazzino;
		}

		StringBundler msg = new StringBundler(10);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("anno=");
		msg.append(anno);

		msg.append(", codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(", soloValore=");
		msg.append(soloValore);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMovimentiMagazzinoException(msg.toString());
	}

	/**
	 * Returns the last movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching movimenti magazzino, or <code>null</code> if a matching movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino fetchByArticoloVariante_Last(int anno,
		String codiceArticolo, String codiceVariante, int soloValore,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByArticoloVariante(anno, codiceArticolo,
				codiceVariante, soloValore);

		if (count == 0) {
			return null;
		}

		List<MovimentiMagazzino> list = findByArticoloVariante(anno,
				codiceArticolo, codiceVariante, soloValore, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the movimenti magazzinos before and after the current movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	 *
	 * @param ID the primary key of the current movimenti magazzino
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino[] findByArticoloVariante_PrevAndNext(String ID,
		int anno, String codiceArticolo, String codiceVariante, int soloValore,
		OrderByComparator orderByComparator)
		throws NoSuchMovimentiMagazzinoException, SystemException {
		MovimentiMagazzino movimentiMagazzino = findByPrimaryKey(ID);

		Session session = null;

		try {
			session = openSession();

			MovimentiMagazzino[] array = new MovimentiMagazzinoImpl[3];

			array[0] = getByArticoloVariante_PrevAndNext(session,
					movimentiMagazzino, anno, codiceArticolo, codiceVariante,
					soloValore, orderByComparator, true);

			array[1] = movimentiMagazzino;

			array[2] = getByArticoloVariante_PrevAndNext(session,
					movimentiMagazzino, anno, codiceArticolo, codiceVariante,
					soloValore, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected MovimentiMagazzino getByArticoloVariante_PrevAndNext(
		Session session, MovimentiMagazzino movimentiMagazzino, int anno,
		String codiceArticolo, String codiceVariante, int soloValore,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_MOVIMENTIMAGAZZINO_WHERE);

		query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_ANNO_2);

		boolean bindCodiceArticolo = false;

		if (codiceArticolo == null) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_1);
		}
		else if (codiceArticolo.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_3);
		}
		else {
			bindCodiceArticolo = true;

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_2);
		}

		boolean bindCodiceVariante = false;

		if (codiceVariante == null) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_1);
		}
		else if (codiceVariante.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_3);
		}
		else {
			bindCodiceVariante = true;

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_2);
		}

		query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_SOLOVALORE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(MovimentiMagazzinoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(anno);

		if (bindCodiceArticolo) {
			qPos.add(codiceArticolo);
		}

		if (bindCodiceVariante) {
			qPos.add(codiceVariante);
		}

		qPos.add(soloValore);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(movimentiMagazzino);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<MovimentiMagazzino> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63; from the database.
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByArticoloVariante(int anno, String codiceArticolo,
		String codiceVariante, int soloValore) throws SystemException {
		for (MovimentiMagazzino movimentiMagazzino : findByArticoloVariante(
				anno, codiceArticolo, codiceVariante, soloValore,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(movimentiMagazzino);
		}
	}

	/**
	 * Returns the number of movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @return the number of matching movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByArticoloVariante(int anno, String codiceArticolo,
		String codiceVariante, int soloValore) throws SystemException {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_ARTICOLOVARIANTE;

		Object[] finderArgs = new Object[] {
				anno, codiceArticolo, codiceVariante, soloValore
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(5);

			query.append(_SQL_COUNT_MOVIMENTIMAGAZZINO_WHERE);

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_ANNO_2);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_2);
			}

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_SOLOVALORE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(anno);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				qPos.add(soloValore);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_ANNO_2 = "movimentiMagazzino.anno = ? AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_1 =
		"movimentiMagazzino.codiceArticolo IS NULL AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_2 =
		"movimentiMagazzino.codiceArticolo = ? AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_3 =
		"(movimentiMagazzino.codiceArticolo IS NULL OR movimentiMagazzino.codiceArticolo = '') AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_1 =
		"movimentiMagazzino.codiceVariante IS NULL AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_2 =
		"movimentiMagazzino.codiceVariante = ? AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_3 =
		"(movimentiMagazzino.codiceVariante IS NULL OR movimentiMagazzino.codiceVariante = '') AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_SOLOVALORE_2 = "movimentiMagazzino.soloValore != ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTICOLOVARIANTECARICOSCARICO =
		new FinderPath(MovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			MovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED,
			MovimentiMagazzinoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByArticoloVarianteCaricoScarico",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_ARTICOLOVARIANTECARICOSCARICO =
		new FinderPath(MovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			MovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"countByArticoloVarianteCaricoScarico",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName()
			});

	/**
	 * Returns all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @return the matching movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MovimentiMagazzino> findByArticoloVarianteCaricoScarico(
		int anno, String codiceArticolo, String codiceVariante,
		int testCaricoScarico, int soloValore) throws SystemException {
		return findByArticoloVarianteCaricoScarico(anno, codiceArticolo,
			codiceVariante, testCaricoScarico, soloValore, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @param start the lower bound of the range of movimenti magazzinos
	 * @param end the upper bound of the range of movimenti magazzinos (not inclusive)
	 * @return the range of matching movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MovimentiMagazzino> findByArticoloVarianteCaricoScarico(
		int anno, String codiceArticolo, String codiceVariante,
		int testCaricoScarico, int soloValore, int start, int end)
		throws SystemException {
		return findByArticoloVarianteCaricoScarico(anno, codiceArticolo,
			codiceVariante, testCaricoScarico, soloValore, start, end, null);
	}

	/**
	 * Returns an ordered range of all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @param start the lower bound of the range of movimenti magazzinos
	 * @param end the upper bound of the range of movimenti magazzinos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MovimentiMagazzino> findByArticoloVarianteCaricoScarico(
		int anno, String codiceArticolo, String codiceVariante,
		int testCaricoScarico, int soloValore, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTICOLOVARIANTECARICOSCARICO;
		finderArgs = new Object[] {
				anno, codiceArticolo, codiceVariante, testCaricoScarico,
				soloValore,
				
				start, end, orderByComparator
			};

		List<MovimentiMagazzino> list = (List<MovimentiMagazzino>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (MovimentiMagazzino movimentiMagazzino : list) {
				if ((anno != movimentiMagazzino.getAnno()) ||
						!Validator.equals(codiceArticolo,
							movimentiMagazzino.getCodiceArticolo()) ||
						!Validator.equals(codiceVariante,
							movimentiMagazzino.getCodiceVariante()) ||
						(testCaricoScarico != movimentiMagazzino.getTestCaricoScarico()) ||
						(soloValore == movimentiMagazzino.getSoloValore())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(7 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(7);
			}

			query.append(_SQL_SELECT_MOVIMENTIMAGAZZINO_WHERE);

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_ANNO_2);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_2);
			}

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_TESTCARICOSCARICO_2);

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_SOLOVALORE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(MovimentiMagazzinoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(anno);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				qPos.add(testCaricoScarico);

				qPos.add(soloValore);

				if (!pagination) {
					list = (List<MovimentiMagazzino>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<MovimentiMagazzino>(list);
				}
				else {
					list = (List<MovimentiMagazzino>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a matching movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino findByArticoloVarianteCaricoScarico_First(
		int anno, String codiceArticolo, String codiceVariante,
		int testCaricoScarico, int soloValore,
		OrderByComparator orderByComparator)
		throws NoSuchMovimentiMagazzinoException, SystemException {
		MovimentiMagazzino movimentiMagazzino = fetchByArticoloVarianteCaricoScarico_First(anno,
				codiceArticolo, codiceVariante, testCaricoScarico, soloValore,
				orderByComparator);

		if (movimentiMagazzino != null) {
			return movimentiMagazzino;
		}

		StringBundler msg = new StringBundler(12);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("anno=");
		msg.append(anno);

		msg.append(", codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(", testCaricoScarico=");
		msg.append(testCaricoScarico);

		msg.append(", soloValore=");
		msg.append(soloValore);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMovimentiMagazzinoException(msg.toString());
	}

	/**
	 * Returns the first movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching movimenti magazzino, or <code>null</code> if a matching movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino fetchByArticoloVarianteCaricoScarico_First(
		int anno, String codiceArticolo, String codiceVariante,
		int testCaricoScarico, int soloValore,
		OrderByComparator orderByComparator) throws SystemException {
		List<MovimentiMagazzino> list = findByArticoloVarianteCaricoScarico(anno,
				codiceArticolo, codiceVariante, testCaricoScarico, soloValore,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a matching movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino findByArticoloVarianteCaricoScarico_Last(
		int anno, String codiceArticolo, String codiceVariante,
		int testCaricoScarico, int soloValore,
		OrderByComparator orderByComparator)
		throws NoSuchMovimentiMagazzinoException, SystemException {
		MovimentiMagazzino movimentiMagazzino = fetchByArticoloVarianteCaricoScarico_Last(anno,
				codiceArticolo, codiceVariante, testCaricoScarico, soloValore,
				orderByComparator);

		if (movimentiMagazzino != null) {
			return movimentiMagazzino;
		}

		StringBundler msg = new StringBundler(12);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("anno=");
		msg.append(anno);

		msg.append(", codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(", testCaricoScarico=");
		msg.append(testCaricoScarico);

		msg.append(", soloValore=");
		msg.append(soloValore);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMovimentiMagazzinoException(msg.toString());
	}

	/**
	 * Returns the last movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching movimenti magazzino, or <code>null</code> if a matching movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino fetchByArticoloVarianteCaricoScarico_Last(
		int anno, String codiceArticolo, String codiceVariante,
		int testCaricoScarico, int soloValore,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByArticoloVarianteCaricoScarico(anno, codiceArticolo,
				codiceVariante, testCaricoScarico, soloValore);

		if (count == 0) {
			return null;
		}

		List<MovimentiMagazzino> list = findByArticoloVarianteCaricoScarico(anno,
				codiceArticolo, codiceVariante, testCaricoScarico, soloValore,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the movimenti magazzinos before and after the current movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	 *
	 * @param ID the primary key of the current movimenti magazzino
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino[] findByArticoloVarianteCaricoScarico_PrevAndNext(
		String ID, int anno, String codiceArticolo, String codiceVariante,
		int testCaricoScarico, int soloValore,
		OrderByComparator orderByComparator)
		throws NoSuchMovimentiMagazzinoException, SystemException {
		MovimentiMagazzino movimentiMagazzino = findByPrimaryKey(ID);

		Session session = null;

		try {
			session = openSession();

			MovimentiMagazzino[] array = new MovimentiMagazzinoImpl[3];

			array[0] = getByArticoloVarianteCaricoScarico_PrevAndNext(session,
					movimentiMagazzino, anno, codiceArticolo, codiceVariante,
					testCaricoScarico, soloValore, orderByComparator, true);

			array[1] = movimentiMagazzino;

			array[2] = getByArticoloVarianteCaricoScarico_PrevAndNext(session,
					movimentiMagazzino, anno, codiceArticolo, codiceVariante,
					testCaricoScarico, soloValore, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected MovimentiMagazzino getByArticoloVarianteCaricoScarico_PrevAndNext(
		Session session, MovimentiMagazzino movimentiMagazzino, int anno,
		String codiceArticolo, String codiceVariante, int testCaricoScarico,
		int soloValore, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_MOVIMENTIMAGAZZINO_WHERE);

		query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_ANNO_2);

		boolean bindCodiceArticolo = false;

		if (codiceArticolo == null) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_1);
		}
		else if (codiceArticolo.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_3);
		}
		else {
			bindCodiceArticolo = true;

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_2);
		}

		boolean bindCodiceVariante = false;

		if (codiceVariante == null) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_1);
		}
		else if (codiceVariante.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_3);
		}
		else {
			bindCodiceVariante = true;

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_2);
		}

		query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_TESTCARICOSCARICO_2);

		query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_SOLOVALORE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(MovimentiMagazzinoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(anno);

		if (bindCodiceArticolo) {
			qPos.add(codiceArticolo);
		}

		if (bindCodiceVariante) {
			qPos.add(codiceVariante);
		}

		qPos.add(testCaricoScarico);

		qPos.add(soloValore);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(movimentiMagazzino);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<MovimentiMagazzino> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63; from the database.
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByArticoloVarianteCaricoScarico(int anno,
		String codiceArticolo, String codiceVariante, int testCaricoScarico,
		int soloValore) throws SystemException {
		for (MovimentiMagazzino movimentiMagazzino : findByArticoloVarianteCaricoScarico(
				anno, codiceArticolo, codiceVariante, testCaricoScarico,
				soloValore, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(movimentiMagazzino);
		}
	}

	/**
	 * Returns the number of movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	 *
	 * @param anno the anno
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @return the number of matching movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByArticoloVarianteCaricoScarico(int anno,
		String codiceArticolo, String codiceVariante, int testCaricoScarico,
		int soloValore) throws SystemException {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_ARTICOLOVARIANTECARICOSCARICO;

		Object[] finderArgs = new Object[] {
				anno, codiceArticolo, codiceVariante, testCaricoScarico,
				soloValore
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(6);

			query.append(_SQL_COUNT_MOVIMENTIMAGAZZINO_WHERE);

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_ANNO_2);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_2);
			}

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_TESTCARICOSCARICO_2);

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_SOLOVALORE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(anno);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				qPos.add(testCaricoScarico);

				qPos.add(soloValore);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_ANNO_2 =
		"movimentiMagazzino.anno = ? AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_1 =
		"movimentiMagazzino.codiceArticolo IS NULL AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_2 =
		"movimentiMagazzino.codiceArticolo = ? AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_3 =
		"(movimentiMagazzino.codiceArticolo IS NULL OR movimentiMagazzino.codiceArticolo = '') AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_1 =
		"movimentiMagazzino.codiceVariante IS NULL AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_2 =
		"movimentiMagazzino.codiceVariante = ? AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_3 =
		"(movimentiMagazzino.codiceVariante IS NULL OR movimentiMagazzino.codiceVariante = '') AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_TESTCARICOSCARICO_2 =
		"movimentiMagazzino.testCaricoScarico = ? AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_SOLOVALORE_2 =
		"movimentiMagazzino.soloValore != ?";

	public MovimentiMagazzinoPersistenceImpl() {
		setModelClass(MovimentiMagazzino.class);
	}

	/**
	 * Caches the movimenti magazzino in the entity cache if it is enabled.
	 *
	 * @param movimentiMagazzino the movimenti magazzino
	 */
	@Override
	public void cacheResult(MovimentiMagazzino movimentiMagazzino) {
		EntityCacheUtil.putResult(MovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			MovimentiMagazzinoImpl.class, movimentiMagazzino.getPrimaryKey(),
			movimentiMagazzino);

		movimentiMagazzino.resetOriginalValues();
	}

	/**
	 * Caches the movimenti magazzinos in the entity cache if it is enabled.
	 *
	 * @param movimentiMagazzinos the movimenti magazzinos
	 */
	@Override
	public void cacheResult(List<MovimentiMagazzino> movimentiMagazzinos) {
		for (MovimentiMagazzino movimentiMagazzino : movimentiMagazzinos) {
			if (EntityCacheUtil.getResult(
						MovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
						MovimentiMagazzinoImpl.class,
						movimentiMagazzino.getPrimaryKey()) == null) {
				cacheResult(movimentiMagazzino);
			}
			else {
				movimentiMagazzino.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all movimenti magazzinos.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(MovimentiMagazzinoImpl.class.getName());
		}

		EntityCacheUtil.clearCache(MovimentiMagazzinoImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the movimenti magazzino.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(MovimentiMagazzino movimentiMagazzino) {
		EntityCacheUtil.removeResult(MovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			MovimentiMagazzinoImpl.class, movimentiMagazzino.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<MovimentiMagazzino> movimentiMagazzinos) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (MovimentiMagazzino movimentiMagazzino : movimentiMagazzinos) {
			EntityCacheUtil.removeResult(MovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
				MovimentiMagazzinoImpl.class, movimentiMagazzino.getPrimaryKey());
		}
	}

	/**
	 * Creates a new movimenti magazzino with the primary key. Does not add the movimenti magazzino to the database.
	 *
	 * @param ID the primary key for the new movimenti magazzino
	 * @return the new movimenti magazzino
	 */
	@Override
	public MovimentiMagazzino create(String ID) {
		MovimentiMagazzino movimentiMagazzino = new MovimentiMagazzinoImpl();

		movimentiMagazzino.setNew(true);
		movimentiMagazzino.setPrimaryKey(ID);

		return movimentiMagazzino;
	}

	/**
	 * Removes the movimenti magazzino with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ID the primary key of the movimenti magazzino
	 * @return the movimenti magazzino that was removed
	 * @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino remove(String ID)
		throws NoSuchMovimentiMagazzinoException, SystemException {
		return remove((Serializable)ID);
	}

	/**
	 * Removes the movimenti magazzino with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the movimenti magazzino
	 * @return the movimenti magazzino that was removed
	 * @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino remove(Serializable primaryKey)
		throws NoSuchMovimentiMagazzinoException, SystemException {
		Session session = null;

		try {
			session = openSession();

			MovimentiMagazzino movimentiMagazzino = (MovimentiMagazzino)session.get(MovimentiMagazzinoImpl.class,
					primaryKey);

			if (movimentiMagazzino == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchMovimentiMagazzinoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(movimentiMagazzino);
		}
		catch (NoSuchMovimentiMagazzinoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected MovimentiMagazzino removeImpl(
		MovimentiMagazzino movimentiMagazzino) throws SystemException {
		movimentiMagazzino = toUnwrappedModel(movimentiMagazzino);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(movimentiMagazzino)) {
				movimentiMagazzino = (MovimentiMagazzino)session.get(MovimentiMagazzinoImpl.class,
						movimentiMagazzino.getPrimaryKeyObj());
			}

			if (movimentiMagazzino != null) {
				session.delete(movimentiMagazzino);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (movimentiMagazzino != null) {
			clearCache(movimentiMagazzino);
		}

		return movimentiMagazzino;
	}

	@Override
	public MovimentiMagazzino updateImpl(
		it.bysoftware.ct.model.MovimentiMagazzino movimentiMagazzino)
		throws SystemException {
		movimentiMagazzino = toUnwrappedModel(movimentiMagazzino);

		boolean isNew = movimentiMagazzino.isNew();

		Session session = null;

		try {
			session = openSession();

			if (movimentiMagazzino.isNew()) {
				session.save(movimentiMagazzino);

				movimentiMagazzino.setNew(false);
			}
			else {
				session.merge(movimentiMagazzino);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !MovimentiMagazzinoModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(MovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			MovimentiMagazzinoImpl.class, movimentiMagazzino.getPrimaryKey(),
			movimentiMagazzino);

		return movimentiMagazzino;
	}

	protected MovimentiMagazzino toUnwrappedModel(
		MovimentiMagazzino movimentiMagazzino) {
		if (movimentiMagazzino instanceof MovimentiMagazzinoImpl) {
			return movimentiMagazzino;
		}

		MovimentiMagazzinoImpl movimentiMagazzinoImpl = new MovimentiMagazzinoImpl();

		movimentiMagazzinoImpl.setNew(movimentiMagazzino.isNew());
		movimentiMagazzinoImpl.setPrimaryKey(movimentiMagazzino.getPrimaryKey());

		movimentiMagazzinoImpl.setID(movimentiMagazzino.getID());
		movimentiMagazzinoImpl.setAnno(movimentiMagazzino.getAnno());
		movimentiMagazzinoImpl.setCodiceArticolo(movimentiMagazzino.getCodiceArticolo());
		movimentiMagazzinoImpl.setCodiceVariante(movimentiMagazzino.getCodiceVariante());
		movimentiMagazzinoImpl.setDescrizione(movimentiMagazzino.getDescrizione());
		movimentiMagazzinoImpl.setUnitaMisura(movimentiMagazzino.getUnitaMisura());
		movimentiMagazzinoImpl.setQuantita(movimentiMagazzino.getQuantita());
		movimentiMagazzinoImpl.setTestCaricoScarico(movimentiMagazzino.getTestCaricoScarico());
		movimentiMagazzinoImpl.setDataRegistrazione(movimentiMagazzino.getDataRegistrazione());
		movimentiMagazzinoImpl.setTipoSoggetto(movimentiMagazzino.isTipoSoggetto());
		movimentiMagazzinoImpl.setCodiceSoggetto(movimentiMagazzino.getCodiceSoggetto());
		movimentiMagazzinoImpl.setRagioneSociale(movimentiMagazzino.getRagioneSociale());
		movimentiMagazzinoImpl.setEstremiDocumenti(movimentiMagazzino.getEstremiDocumenti());
		movimentiMagazzinoImpl.setDataDocumento(movimentiMagazzino.getDataDocumento());
		movimentiMagazzinoImpl.setNumeroDocumento(movimentiMagazzino.getNumeroDocumento());
		movimentiMagazzinoImpl.setTipoDocumento(movimentiMagazzino.getTipoDocumento());
		movimentiMagazzinoImpl.setSoloValore(movimentiMagazzino.getSoloValore());

		return movimentiMagazzinoImpl;
	}

	/**
	 * Returns the movimenti magazzino with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the movimenti magazzino
	 * @return the movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino findByPrimaryKey(Serializable primaryKey)
		throws NoSuchMovimentiMagazzinoException, SystemException {
		MovimentiMagazzino movimentiMagazzino = fetchByPrimaryKey(primaryKey);

		if (movimentiMagazzino == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchMovimentiMagazzinoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return movimentiMagazzino;
	}

	/**
	 * Returns the movimenti magazzino with the primary key or throws a {@link it.bysoftware.ct.NoSuchMovimentiMagazzinoException} if it could not be found.
	 *
	 * @param ID the primary key of the movimenti magazzino
	 * @return the movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino findByPrimaryKey(String ID)
		throws NoSuchMovimentiMagazzinoException, SystemException {
		return findByPrimaryKey((Serializable)ID);
	}

	/**
	 * Returns the movimenti magazzino with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the movimenti magazzino
	 * @return the movimenti magazzino, or <code>null</code> if a movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		MovimentiMagazzino movimentiMagazzino = (MovimentiMagazzino)EntityCacheUtil.getResult(MovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
				MovimentiMagazzinoImpl.class, primaryKey);

		if (movimentiMagazzino == _nullMovimentiMagazzino) {
			return null;
		}

		if (movimentiMagazzino == null) {
			Session session = null;

			try {
				session = openSession();

				movimentiMagazzino = (MovimentiMagazzino)session.get(MovimentiMagazzinoImpl.class,
						primaryKey);

				if (movimentiMagazzino != null) {
					cacheResult(movimentiMagazzino);
				}
				else {
					EntityCacheUtil.putResult(MovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
						MovimentiMagazzinoImpl.class, primaryKey,
						_nullMovimentiMagazzino);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(MovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
					MovimentiMagazzinoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return movimentiMagazzino;
	}

	/**
	 * Returns the movimenti magazzino with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ID the primary key of the movimenti magazzino
	 * @return the movimenti magazzino, or <code>null</code> if a movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MovimentiMagazzino fetchByPrimaryKey(String ID)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)ID);
	}

	/**
	 * Returns all the movimenti magazzinos.
	 *
	 * @return the movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MovimentiMagazzino> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the movimenti magazzinos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of movimenti magazzinos
	 * @param end the upper bound of the range of movimenti magazzinos (not inclusive)
	 * @return the range of movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MovimentiMagazzino> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the movimenti magazzinos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of movimenti magazzinos
	 * @param end the upper bound of the range of movimenti magazzinos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MovimentiMagazzino> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<MovimentiMagazzino> list = (List<MovimentiMagazzino>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_MOVIMENTIMAGAZZINO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_MOVIMENTIMAGAZZINO;

				if (pagination) {
					sql = sql.concat(MovimentiMagazzinoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<MovimentiMagazzino>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<MovimentiMagazzino>(list);
				}
				else {
					list = (List<MovimentiMagazzino>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the movimenti magazzinos from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (MovimentiMagazzino movimentiMagazzino : findAll()) {
			remove(movimentiMagazzino);
		}
	}

	/**
	 * Returns the number of movimenti magazzinos.
	 *
	 * @return the number of movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_MOVIMENTIMAGAZZINO);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the movimenti magazzino persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.MovimentiMagazzino")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<MovimentiMagazzino>> listenersList = new ArrayList<ModelListener<MovimentiMagazzino>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<MovimentiMagazzino>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(MovimentiMagazzinoImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_MOVIMENTIMAGAZZINO = "SELECT movimentiMagazzino FROM MovimentiMagazzino movimentiMagazzino";
	private static final String _SQL_SELECT_MOVIMENTIMAGAZZINO_WHERE = "SELECT movimentiMagazzino FROM MovimentiMagazzino movimentiMagazzino WHERE ";
	private static final String _SQL_COUNT_MOVIMENTIMAGAZZINO = "SELECT COUNT(movimentiMagazzino) FROM MovimentiMagazzino movimentiMagazzino";
	private static final String _SQL_COUNT_MOVIMENTIMAGAZZINO_WHERE = "SELECT COUNT(movimentiMagazzino) FROM MovimentiMagazzino movimentiMagazzino WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "movimentiMagazzino.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No MovimentiMagazzino exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No MovimentiMagazzino exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(MovimentiMagazzinoPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"anno", "codiceArticolo", "codiceVariante", "descrizione",
				"unitaMisura", "quantita", "testCaricoScarico",
				"dataRegistrazione", "tipoSoggetto", "codiceSoggetto",
				"ragioneSociale", "estremiDocumenti", "dataDocumento",
				"numeroDocumento", "tipoDocumento", "soloValore"
			});
	private static MovimentiMagazzino _nullMovimentiMagazzino = new MovimentiMagazzinoImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<MovimentiMagazzino> toCacheModel() {
				return _nullMovimentiMagazzinoCacheModel;
			}
		};

	private static CacheModel<MovimentiMagazzino> _nullMovimentiMagazzinoCacheModel =
		new CacheModel<MovimentiMagazzino>() {
			@Override
			public MovimentiMagazzino toEntityModel() {
				return _nullMovimentiMagazzino;
			}
		};
}