/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchUltimiPrezziArticoliException;
import it.bysoftware.ct.model.UltimiPrezziArticoli;
import it.bysoftware.ct.model.impl.UltimiPrezziArticoliImpl;
import it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the ultimi prezzi articoli service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see UltimiPrezziArticoliPersistence
 * @see UltimiPrezziArticoliUtil
 * @generated
 */
public class UltimiPrezziArticoliPersistenceImpl extends BasePersistenceImpl<UltimiPrezziArticoli>
	implements UltimiPrezziArticoliPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UltimiPrezziArticoliUtil} to access the ultimi prezzi articoli persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UltimiPrezziArticoliImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			UltimiPrezziArticoliModelImpl.FINDER_CACHE_ENABLED,
			UltimiPrezziArticoliImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			UltimiPrezziArticoliModelImpl.FINDER_CACHE_ENABLED,
			UltimiPrezziArticoliImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			UltimiPrezziArticoliModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ULTIMECONDIZIONI =
		new FinderPath(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			UltimiPrezziArticoliModelImpl.FINDER_CACHE_ENABLED,
			UltimiPrezziArticoliImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUltimeCondizioni",
			new String[] {
				Boolean.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ULTIMECONDIZIONI =
		new FinderPath(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			UltimiPrezziArticoliModelImpl.FINDER_CACHE_ENABLED,
			UltimiPrezziArticoliImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUltimeCondizioni",
			new String[] { Boolean.class.getName(), String.class.getName() },
			UltimiPrezziArticoliModelImpl.TIPOSOGGETTO_COLUMN_BITMASK |
			UltimiPrezziArticoliModelImpl.CODICESOGGETTO_COLUMN_BITMASK |
			UltimiPrezziArticoliModelImpl.DATADOCUMENTO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ULTIMECONDIZIONI = new FinderPath(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			UltimiPrezziArticoliModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUltimeCondizioni",
			new String[] { Boolean.class.getName(), String.class.getName() });

	/**
	 * Returns all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @return the matching ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UltimiPrezziArticoli> findByUltimeCondizioni(
		boolean tipoSoggetto, String codiceSoggetto) throws SystemException {
		return findByUltimeCondizioni(tipoSoggetto, codiceSoggetto,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param start the lower bound of the range of ultimi prezzi articolis
	 * @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	 * @return the range of matching ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UltimiPrezziArticoli> findByUltimeCondizioni(
		boolean tipoSoggetto, String codiceSoggetto, int start, int end)
		throws SystemException {
		return findByUltimeCondizioni(tipoSoggetto, codiceSoggetto, start, end,
			null);
	}

	/**
	 * Returns an ordered range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param start the lower bound of the range of ultimi prezzi articolis
	 * @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UltimiPrezziArticoli> findByUltimeCondizioni(
		boolean tipoSoggetto, String codiceSoggetto, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ULTIMECONDIZIONI;
			finderArgs = new Object[] { tipoSoggetto, codiceSoggetto };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ULTIMECONDIZIONI;
			finderArgs = new Object[] {
					tipoSoggetto, codiceSoggetto,
					
					start, end, orderByComparator
				};
		}

		List<UltimiPrezziArticoli> list = (List<UltimiPrezziArticoli>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UltimiPrezziArticoli ultimiPrezziArticoli : list) {
				if ((tipoSoggetto != ultimiPrezziArticoli.getTipoSoggetto()) ||
						!Validator.equals(codiceSoggetto,
							ultimiPrezziArticoli.getCodiceSoggetto())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ULTIMIPREZZIARTICOLI_WHERE);

			query.append(_FINDER_COLUMN_ULTIMECONDIZIONI_TIPOSOGGETTO_2);

			boolean bindCodiceSoggetto = false;

			if (codiceSoggetto == null) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONI_CODICESOGGETTO_1);
			}
			else if (codiceSoggetto.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONI_CODICESOGGETTO_3);
			}
			else {
				bindCodiceSoggetto = true;

				query.append(_FINDER_COLUMN_ULTIMECONDIZIONI_CODICESOGGETTO_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UltimiPrezziArticoliModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(tipoSoggetto);

				if (bindCodiceSoggetto) {
					qPos.add(codiceSoggetto);
				}

				if (!pagination) {
					list = (List<UltimiPrezziArticoli>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UltimiPrezziArticoli>(list);
				}
				else {
					list = (List<UltimiPrezziArticoli>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ultimi prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli findByUltimeCondizioni_First(
		boolean tipoSoggetto, String codiceSoggetto,
		OrderByComparator orderByComparator)
		throws NoSuchUltimiPrezziArticoliException, SystemException {
		UltimiPrezziArticoli ultimiPrezziArticoli = fetchByUltimeCondizioni_First(tipoSoggetto,
				codiceSoggetto, orderByComparator);

		if (ultimiPrezziArticoli != null) {
			return ultimiPrezziArticoli;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("tipoSoggetto=");
		msg.append(tipoSoggetto);

		msg.append(", codiceSoggetto=");
		msg.append(codiceSoggetto);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUltimiPrezziArticoliException(msg.toString());
	}

	/**
	 * Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli fetchByUltimeCondizioni_First(
		boolean tipoSoggetto, String codiceSoggetto,
		OrderByComparator orderByComparator) throws SystemException {
		List<UltimiPrezziArticoli> list = findByUltimeCondizioni(tipoSoggetto,
				codiceSoggetto, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ultimi prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli findByUltimeCondizioni_Last(
		boolean tipoSoggetto, String codiceSoggetto,
		OrderByComparator orderByComparator)
		throws NoSuchUltimiPrezziArticoliException, SystemException {
		UltimiPrezziArticoli ultimiPrezziArticoli = fetchByUltimeCondizioni_Last(tipoSoggetto,
				codiceSoggetto, orderByComparator);

		if (ultimiPrezziArticoli != null) {
			return ultimiPrezziArticoli;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("tipoSoggetto=");
		msg.append(tipoSoggetto);

		msg.append(", codiceSoggetto=");
		msg.append(codiceSoggetto);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUltimiPrezziArticoliException(msg.toString());
	}

	/**
	 * Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli fetchByUltimeCondizioni_Last(
		boolean tipoSoggetto, String codiceSoggetto,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUltimeCondizioni(tipoSoggetto, codiceSoggetto);

		if (count == 0) {
			return null;
		}

		List<UltimiPrezziArticoli> list = findByUltimeCondizioni(tipoSoggetto,
				codiceSoggetto, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ultimi prezzi articolis before and after the current ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	 *
	 * @param ultimiPrezziArticoliPK the primary key of the current ultimi prezzi articoli
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ultimi prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli[] findByUltimeCondizioni_PrevAndNext(
		UltimiPrezziArticoliPK ultimiPrezziArticoliPK, boolean tipoSoggetto,
		String codiceSoggetto, OrderByComparator orderByComparator)
		throws NoSuchUltimiPrezziArticoliException, SystemException {
		UltimiPrezziArticoli ultimiPrezziArticoli = findByPrimaryKey(ultimiPrezziArticoliPK);

		Session session = null;

		try {
			session = openSession();

			UltimiPrezziArticoli[] array = new UltimiPrezziArticoliImpl[3];

			array[0] = getByUltimeCondizioni_PrevAndNext(session,
					ultimiPrezziArticoli, tipoSoggetto, codiceSoggetto,
					orderByComparator, true);

			array[1] = ultimiPrezziArticoli;

			array[2] = getByUltimeCondizioni_PrevAndNext(session,
					ultimiPrezziArticoli, tipoSoggetto, codiceSoggetto,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UltimiPrezziArticoli getByUltimeCondizioni_PrevAndNext(
		Session session, UltimiPrezziArticoli ultimiPrezziArticoli,
		boolean tipoSoggetto, String codiceSoggetto,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ULTIMIPREZZIARTICOLI_WHERE);

		query.append(_FINDER_COLUMN_ULTIMECONDIZIONI_TIPOSOGGETTO_2);

		boolean bindCodiceSoggetto = false;

		if (codiceSoggetto == null) {
			query.append(_FINDER_COLUMN_ULTIMECONDIZIONI_CODICESOGGETTO_1);
		}
		else if (codiceSoggetto.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ULTIMECONDIZIONI_CODICESOGGETTO_3);
		}
		else {
			bindCodiceSoggetto = true;

			query.append(_FINDER_COLUMN_ULTIMECONDIZIONI_CODICESOGGETTO_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UltimiPrezziArticoliModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(tipoSoggetto);

		if (bindCodiceSoggetto) {
			qPos.add(codiceSoggetto);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ultimiPrezziArticoli);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UltimiPrezziArticoli> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; from the database.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUltimeCondizioni(boolean tipoSoggetto,
		String codiceSoggetto) throws SystemException {
		for (UltimiPrezziArticoli ultimiPrezziArticoli : findByUltimeCondizioni(
				tipoSoggetto, codiceSoggetto, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(ultimiPrezziArticoli);
		}
	}

	/**
	 * Returns the number of ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @return the number of matching ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUltimeCondizioni(boolean tipoSoggetto,
		String codiceSoggetto) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ULTIMECONDIZIONI;

		Object[] finderArgs = new Object[] { tipoSoggetto, codiceSoggetto };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ULTIMIPREZZIARTICOLI_WHERE);

			query.append(_FINDER_COLUMN_ULTIMECONDIZIONI_TIPOSOGGETTO_2);

			boolean bindCodiceSoggetto = false;

			if (codiceSoggetto == null) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONI_CODICESOGGETTO_1);
			}
			else if (codiceSoggetto.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONI_CODICESOGGETTO_3);
			}
			else {
				bindCodiceSoggetto = true;

				query.append(_FINDER_COLUMN_ULTIMECONDIZIONI_CODICESOGGETTO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(tipoSoggetto);

				if (bindCodiceSoggetto) {
					qPos.add(codiceSoggetto);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ULTIMECONDIZIONI_TIPOSOGGETTO_2 = "ultimiPrezziArticoli.id.tipoSoggetto = ? AND ";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONI_CODICESOGGETTO_1 =
		"ultimiPrezziArticoli.id.codiceSoggetto IS NULL";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONI_CODICESOGGETTO_2 =
		"ultimiPrezziArticoli.id.codiceSoggetto = ?";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONI_CODICESOGGETTO_3 =
		"(ultimiPrezziArticoli.id.codiceSoggetto IS NULL OR ultimiPrezziArticoli.id.codiceSoggetto = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ULTIMECONDIZIONIARTICOLO =
		new FinderPath(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			UltimiPrezziArticoliModelImpl.FINDER_CACHE_ENABLED,
			UltimiPrezziArticoliImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUltimeCondizioniArticolo",
			new String[] {
				Boolean.class.getName(), String.class.getName(),
				String.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ULTIMECONDIZIONIARTICOLO =
		new FinderPath(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			UltimiPrezziArticoliModelImpl.FINDER_CACHE_ENABLED,
			UltimiPrezziArticoliImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUltimeCondizioniArticolo",
			new String[] {
				Boolean.class.getName(), String.class.getName(),
				String.class.getName(), String.class.getName()
			},
			UltimiPrezziArticoliModelImpl.TIPOSOGGETTO_COLUMN_BITMASK |
			UltimiPrezziArticoliModelImpl.CODICESOGGETTO_COLUMN_BITMASK |
			UltimiPrezziArticoliModelImpl.CODICEARTICOLO_COLUMN_BITMASK |
			UltimiPrezziArticoliModelImpl.CODICEVARIANTE_COLUMN_BITMASK |
			UltimiPrezziArticoliModelImpl.DATADOCUMENTO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ULTIMECONDIZIONIARTICOLO =
		new FinderPath(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			UltimiPrezziArticoliModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUltimeCondizioniArticolo",
			new String[] {
				Boolean.class.getName(), String.class.getName(),
				String.class.getName(), String.class.getName()
			});

	/**
	 * Returns all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @return the matching ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UltimiPrezziArticoli> findByUltimeCondizioniArticolo(
		boolean tipoSoggetto, String codiceSoggetto, String codiceArticolo,
		String codiceVariante) throws SystemException {
		return findByUltimeCondizioniArticolo(tipoSoggetto, codiceSoggetto,
			codiceArticolo, codiceVariante, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param start the lower bound of the range of ultimi prezzi articolis
	 * @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	 * @return the range of matching ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UltimiPrezziArticoli> findByUltimeCondizioniArticolo(
		boolean tipoSoggetto, String codiceSoggetto, String codiceArticolo,
		String codiceVariante, int start, int end) throws SystemException {
		return findByUltimeCondizioniArticolo(tipoSoggetto, codiceSoggetto,
			codiceArticolo, codiceVariante, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param start the lower bound of the range of ultimi prezzi articolis
	 * @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UltimiPrezziArticoli> findByUltimeCondizioniArticolo(
		boolean tipoSoggetto, String codiceSoggetto, String codiceArticolo,
		String codiceVariante, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ULTIMECONDIZIONIARTICOLO;
			finderArgs = new Object[] {
					tipoSoggetto, codiceSoggetto, codiceArticolo, codiceVariante
				};
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ULTIMECONDIZIONIARTICOLO;
			finderArgs = new Object[] {
					tipoSoggetto, codiceSoggetto, codiceArticolo, codiceVariante,
					
					start, end, orderByComparator
				};
		}

		List<UltimiPrezziArticoli> list = (List<UltimiPrezziArticoli>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UltimiPrezziArticoli ultimiPrezziArticoli : list) {
				if ((tipoSoggetto != ultimiPrezziArticoli.getTipoSoggetto()) ||
						!Validator.equals(codiceSoggetto,
							ultimiPrezziArticoli.getCodiceSoggetto()) ||
						!Validator.equals(codiceArticolo,
							ultimiPrezziArticoli.getCodiceArticolo()) ||
						!Validator.equals(codiceVariante,
							ultimiPrezziArticoli.getCodiceVariante())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(6 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(6);
			}

			query.append(_SQL_SELECT_ULTIMIPREZZIARTICOLI_WHERE);

			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_TIPOSOGGETTO_2);

			boolean bindCodiceSoggetto = false;

			if (codiceSoggetto == null) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICESOGGETTO_1);
			}
			else if (codiceSoggetto.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICESOGGETTO_3);
			}
			else {
				bindCodiceSoggetto = true;

				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICESOGGETTO_2);
			}

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEVARIANTE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UltimiPrezziArticoliModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(tipoSoggetto);

				if (bindCodiceSoggetto) {
					qPos.add(codiceSoggetto);
				}

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				if (!pagination) {
					list = (List<UltimiPrezziArticoli>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UltimiPrezziArticoli>(list);
				}
				else {
					list = (List<UltimiPrezziArticoli>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ultimi prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli findByUltimeCondizioniArticolo_First(
		boolean tipoSoggetto, String codiceSoggetto, String codiceArticolo,
		String codiceVariante, OrderByComparator orderByComparator)
		throws NoSuchUltimiPrezziArticoliException, SystemException {
		UltimiPrezziArticoli ultimiPrezziArticoli = fetchByUltimeCondizioniArticolo_First(tipoSoggetto,
				codiceSoggetto, codiceArticolo, codiceVariante,
				orderByComparator);

		if (ultimiPrezziArticoli != null) {
			return ultimiPrezziArticoli;
		}

		StringBundler msg = new StringBundler(10);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("tipoSoggetto=");
		msg.append(tipoSoggetto);

		msg.append(", codiceSoggetto=");
		msg.append(codiceSoggetto);

		msg.append(", codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUltimiPrezziArticoliException(msg.toString());
	}

	/**
	 * Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli fetchByUltimeCondizioniArticolo_First(
		boolean tipoSoggetto, String codiceSoggetto, String codiceArticolo,
		String codiceVariante, OrderByComparator orderByComparator)
		throws SystemException {
		List<UltimiPrezziArticoli> list = findByUltimeCondizioniArticolo(tipoSoggetto,
				codiceSoggetto, codiceArticolo, codiceVariante, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ultimi prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli findByUltimeCondizioniArticolo_Last(
		boolean tipoSoggetto, String codiceSoggetto, String codiceArticolo,
		String codiceVariante, OrderByComparator orderByComparator)
		throws NoSuchUltimiPrezziArticoliException, SystemException {
		UltimiPrezziArticoli ultimiPrezziArticoli = fetchByUltimeCondizioniArticolo_Last(tipoSoggetto,
				codiceSoggetto, codiceArticolo, codiceVariante,
				orderByComparator);

		if (ultimiPrezziArticoli != null) {
			return ultimiPrezziArticoli;
		}

		StringBundler msg = new StringBundler(10);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("tipoSoggetto=");
		msg.append(tipoSoggetto);

		msg.append(", codiceSoggetto=");
		msg.append(codiceSoggetto);

		msg.append(", codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUltimiPrezziArticoliException(msg.toString());
	}

	/**
	 * Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli fetchByUltimeCondizioniArticolo_Last(
		boolean tipoSoggetto, String codiceSoggetto, String codiceArticolo,
		String codiceVariante, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByUltimeCondizioniArticolo(tipoSoggetto,
				codiceSoggetto, codiceArticolo, codiceVariante);

		if (count == 0) {
			return null;
		}

		List<UltimiPrezziArticoli> list = findByUltimeCondizioniArticolo(tipoSoggetto,
				codiceSoggetto, codiceArticolo, codiceVariante, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ultimi prezzi articolis before and after the current ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * @param ultimiPrezziArticoliPK the primary key of the current ultimi prezzi articoli
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ultimi prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli[] findByUltimeCondizioniArticolo_PrevAndNext(
		UltimiPrezziArticoliPK ultimiPrezziArticoliPK, boolean tipoSoggetto,
		String codiceSoggetto, String codiceArticolo, String codiceVariante,
		OrderByComparator orderByComparator)
		throws NoSuchUltimiPrezziArticoliException, SystemException {
		UltimiPrezziArticoli ultimiPrezziArticoli = findByPrimaryKey(ultimiPrezziArticoliPK);

		Session session = null;

		try {
			session = openSession();

			UltimiPrezziArticoli[] array = new UltimiPrezziArticoliImpl[3];

			array[0] = getByUltimeCondizioniArticolo_PrevAndNext(session,
					ultimiPrezziArticoli, tipoSoggetto, codiceSoggetto,
					codiceArticolo, codiceVariante, orderByComparator, true);

			array[1] = ultimiPrezziArticoli;

			array[2] = getByUltimeCondizioniArticolo_PrevAndNext(session,
					ultimiPrezziArticoli, tipoSoggetto, codiceSoggetto,
					codiceArticolo, codiceVariante, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UltimiPrezziArticoli getByUltimeCondizioniArticolo_PrevAndNext(
		Session session, UltimiPrezziArticoli ultimiPrezziArticoli,
		boolean tipoSoggetto, String codiceSoggetto, String codiceArticolo,
		String codiceVariante, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ULTIMIPREZZIARTICOLI_WHERE);

		query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_TIPOSOGGETTO_2);

		boolean bindCodiceSoggetto = false;

		if (codiceSoggetto == null) {
			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICESOGGETTO_1);
		}
		else if (codiceSoggetto.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICESOGGETTO_3);
		}
		else {
			bindCodiceSoggetto = true;

			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICESOGGETTO_2);
		}

		boolean bindCodiceArticolo = false;

		if (codiceArticolo == null) {
			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEARTICOLO_1);
		}
		else if (codiceArticolo.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEARTICOLO_3);
		}
		else {
			bindCodiceArticolo = true;

			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEARTICOLO_2);
		}

		boolean bindCodiceVariante = false;

		if (codiceVariante == null) {
			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEVARIANTE_1);
		}
		else if (codiceVariante.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEVARIANTE_3);
		}
		else {
			bindCodiceVariante = true;

			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEVARIANTE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UltimiPrezziArticoliModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(tipoSoggetto);

		if (bindCodiceSoggetto) {
			qPos.add(codiceSoggetto);
		}

		if (bindCodiceArticolo) {
			qPos.add(codiceArticolo);
		}

		if (bindCodiceVariante) {
			qPos.add(codiceVariante);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ultimiPrezziArticoli);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UltimiPrezziArticoli> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; from the database.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUltimeCondizioniArticolo(boolean tipoSoggetto,
		String codiceSoggetto, String codiceArticolo, String codiceVariante)
		throws SystemException {
		for (UltimiPrezziArticoli ultimiPrezziArticoli : findByUltimeCondizioniArticolo(
				tipoSoggetto, codiceSoggetto, codiceArticolo, codiceVariante,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(ultimiPrezziArticoli);
		}
	}

	/**
	 * Returns the number of ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceSoggetto the codice soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @return the number of matching ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUltimeCondizioniArticolo(boolean tipoSoggetto,
		String codiceSoggetto, String codiceArticolo, String codiceVariante)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ULTIMECONDIZIONIARTICOLO;

		Object[] finderArgs = new Object[] {
				tipoSoggetto, codiceSoggetto, codiceArticolo, codiceVariante
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(5);

			query.append(_SQL_COUNT_ULTIMIPREZZIARTICOLI_WHERE);

			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_TIPOSOGGETTO_2);

			boolean bindCodiceSoggetto = false;

			if (codiceSoggetto == null) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICESOGGETTO_1);
			}
			else if (codiceSoggetto.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICESOGGETTO_3);
			}
			else {
				bindCodiceSoggetto = true;

				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICESOGGETTO_2);
			}

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEVARIANTE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(tipoSoggetto);

				if (bindCodiceSoggetto) {
					qPos.add(codiceSoggetto);
				}

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_TIPOSOGGETTO_2 =
		"ultimiPrezziArticoli.id.tipoSoggetto = ? AND ";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICESOGGETTO_1 =
		"ultimiPrezziArticoli.id.codiceSoggetto IS NULL AND ";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICESOGGETTO_2 =
		"ultimiPrezziArticoli.id.codiceSoggetto = ? AND ";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICESOGGETTO_3 =
		"(ultimiPrezziArticoli.id.codiceSoggetto IS NULL OR ultimiPrezziArticoli.id.codiceSoggetto = '') AND ";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEARTICOLO_1 =
		"ultimiPrezziArticoli.id.codiceArticolo IS NULL AND ";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEARTICOLO_2 =
		"ultimiPrezziArticoli.id.codiceArticolo = ? AND ";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEARTICOLO_3 =
		"(ultimiPrezziArticoli.id.codiceArticolo IS NULL OR ultimiPrezziArticoli.id.codiceArticolo = '') AND ";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEVARIANTE_1 =
		"ultimiPrezziArticoli.id.codiceVariante IS NULL";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEVARIANTE_2 =
		"ultimiPrezziArticoli.id.codiceVariante = ?";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIARTICOLO_CODICEVARIANTE_3 =
		"(ultimiPrezziArticoli.id.codiceVariante IS NULL OR ultimiPrezziArticoli.id.codiceVariante = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ULTIMECONDIZIONIFORNITORI =
		new FinderPath(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			UltimiPrezziArticoliModelImpl.FINDER_CACHE_ENABLED,
			UltimiPrezziArticoliImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUltimeCondizioniFornitori",
			new String[] {
				Boolean.class.getName(), String.class.getName(),
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ULTIMECONDIZIONIFORNITORI =
		new FinderPath(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			UltimiPrezziArticoliModelImpl.FINDER_CACHE_ENABLED,
			UltimiPrezziArticoliImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUltimeCondizioniFornitori",
			new String[] {
				Boolean.class.getName(), String.class.getName(),
				String.class.getName()
			},
			UltimiPrezziArticoliModelImpl.TIPOSOGGETTO_COLUMN_BITMASK |
			UltimiPrezziArticoliModelImpl.CODICEARTICOLO_COLUMN_BITMASK |
			UltimiPrezziArticoliModelImpl.CODICEVARIANTE_COLUMN_BITMASK |
			UltimiPrezziArticoliModelImpl.DATADOCUMENTO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ULTIMECONDIZIONIFORNITORI =
		new FinderPath(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			UltimiPrezziArticoliModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUltimeCondizioniFornitori",
			new String[] {
				Boolean.class.getName(), String.class.getName(),
				String.class.getName()
			});

	/**
	 * Returns all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @return the matching ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UltimiPrezziArticoli> findByUltimeCondizioniFornitori(
		boolean tipoSoggetto, String codiceArticolo, String codiceVariante)
		throws SystemException {
		return findByUltimeCondizioniFornitori(tipoSoggetto, codiceArticolo,
			codiceVariante, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param start the lower bound of the range of ultimi prezzi articolis
	 * @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	 * @return the range of matching ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UltimiPrezziArticoli> findByUltimeCondizioniFornitori(
		boolean tipoSoggetto, String codiceArticolo, String codiceVariante,
		int start, int end) throws SystemException {
		return findByUltimeCondizioniFornitori(tipoSoggetto, codiceArticolo,
			codiceVariante, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param start the lower bound of the range of ultimi prezzi articolis
	 * @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UltimiPrezziArticoli> findByUltimeCondizioniFornitori(
		boolean tipoSoggetto, String codiceArticolo, String codiceVariante,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ULTIMECONDIZIONIFORNITORI;
			finderArgs = new Object[] {
					tipoSoggetto, codiceArticolo, codiceVariante
				};
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ULTIMECONDIZIONIFORNITORI;
			finderArgs = new Object[] {
					tipoSoggetto, codiceArticolo, codiceVariante,
					
					start, end, orderByComparator
				};
		}

		List<UltimiPrezziArticoli> list = (List<UltimiPrezziArticoli>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (UltimiPrezziArticoli ultimiPrezziArticoli : list) {
				if ((tipoSoggetto != ultimiPrezziArticoli.getTipoSoggetto()) ||
						!Validator.equals(codiceArticolo,
							ultimiPrezziArticoli.getCodiceArticolo()) ||
						!Validator.equals(codiceVariante,
							ultimiPrezziArticoli.getCodiceVariante())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_ULTIMIPREZZIARTICOLI_WHERE);

			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_TIPOSOGGETTO_2);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEVARIANTE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UltimiPrezziArticoliModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(tipoSoggetto);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				if (!pagination) {
					list = (List<UltimiPrezziArticoli>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UltimiPrezziArticoli>(list);
				}
				else {
					list = (List<UltimiPrezziArticoli>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ultimi prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli findByUltimeCondizioniFornitori_First(
		boolean tipoSoggetto, String codiceArticolo, String codiceVariante,
		OrderByComparator orderByComparator)
		throws NoSuchUltimiPrezziArticoliException, SystemException {
		UltimiPrezziArticoli ultimiPrezziArticoli = fetchByUltimeCondizioniFornitori_First(tipoSoggetto,
				codiceArticolo, codiceVariante, orderByComparator);

		if (ultimiPrezziArticoli != null) {
			return ultimiPrezziArticoli;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("tipoSoggetto=");
		msg.append(tipoSoggetto);

		msg.append(", codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUltimiPrezziArticoliException(msg.toString());
	}

	/**
	 * Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli fetchByUltimeCondizioniFornitori_First(
		boolean tipoSoggetto, String codiceArticolo, String codiceVariante,
		OrderByComparator orderByComparator) throws SystemException {
		List<UltimiPrezziArticoli> list = findByUltimeCondizioniFornitori(tipoSoggetto,
				codiceArticolo, codiceVariante, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ultimi prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli findByUltimeCondizioniFornitori_Last(
		boolean tipoSoggetto, String codiceArticolo, String codiceVariante,
		OrderByComparator orderByComparator)
		throws NoSuchUltimiPrezziArticoliException, SystemException {
		UltimiPrezziArticoli ultimiPrezziArticoli = fetchByUltimeCondizioniFornitori_Last(tipoSoggetto,
				codiceArticolo, codiceVariante, orderByComparator);

		if (ultimiPrezziArticoli != null) {
			return ultimiPrezziArticoli;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("tipoSoggetto=");
		msg.append(tipoSoggetto);

		msg.append(", codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUltimiPrezziArticoliException(msg.toString());
	}

	/**
	 * Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli fetchByUltimeCondizioniFornitori_Last(
		boolean tipoSoggetto, String codiceArticolo, String codiceVariante,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUltimeCondizioniFornitori(tipoSoggetto,
				codiceArticolo, codiceVariante);

		if (count == 0) {
			return null;
		}

		List<UltimiPrezziArticoli> list = findByUltimeCondizioniFornitori(tipoSoggetto,
				codiceArticolo, codiceVariante, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ultimi prezzi articolis before and after the current ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * @param ultimiPrezziArticoliPK the primary key of the current ultimi prezzi articoli
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ultimi prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli[] findByUltimeCondizioniFornitori_PrevAndNext(
		UltimiPrezziArticoliPK ultimiPrezziArticoliPK, boolean tipoSoggetto,
		String codiceArticolo, String codiceVariante,
		OrderByComparator orderByComparator)
		throws NoSuchUltimiPrezziArticoliException, SystemException {
		UltimiPrezziArticoli ultimiPrezziArticoli = findByPrimaryKey(ultimiPrezziArticoliPK);

		Session session = null;

		try {
			session = openSession();

			UltimiPrezziArticoli[] array = new UltimiPrezziArticoliImpl[3];

			array[0] = getByUltimeCondizioniFornitori_PrevAndNext(session,
					ultimiPrezziArticoli, tipoSoggetto, codiceArticolo,
					codiceVariante, orderByComparator, true);

			array[1] = ultimiPrezziArticoli;

			array[2] = getByUltimeCondizioniFornitori_PrevAndNext(session,
					ultimiPrezziArticoli, tipoSoggetto, codiceArticolo,
					codiceVariante, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UltimiPrezziArticoli getByUltimeCondizioniFornitori_PrevAndNext(
		Session session, UltimiPrezziArticoli ultimiPrezziArticoli,
		boolean tipoSoggetto, String codiceArticolo, String codiceVariante,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ULTIMIPREZZIARTICOLI_WHERE);

		query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_TIPOSOGGETTO_2);

		boolean bindCodiceArticolo = false;

		if (codiceArticolo == null) {
			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEARTICOLO_1);
		}
		else if (codiceArticolo.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEARTICOLO_3);
		}
		else {
			bindCodiceArticolo = true;

			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEARTICOLO_2);
		}

		boolean bindCodiceVariante = false;

		if (codiceVariante == null) {
			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEVARIANTE_1);
		}
		else if (codiceVariante.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEVARIANTE_3);
		}
		else {
			bindCodiceVariante = true;

			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEVARIANTE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UltimiPrezziArticoliModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(tipoSoggetto);

		if (bindCodiceArticolo) {
			qPos.add(codiceArticolo);
		}

		if (bindCodiceVariante) {
			qPos.add(codiceVariante);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ultimiPrezziArticoli);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UltimiPrezziArticoli> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; from the database.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUltimeCondizioniFornitori(boolean tipoSoggetto,
		String codiceArticolo, String codiceVariante) throws SystemException {
		for (UltimiPrezziArticoli ultimiPrezziArticoli : findByUltimeCondizioniFornitori(
				tipoSoggetto, codiceArticolo, codiceVariante,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(ultimiPrezziArticoli);
		}
	}

	/**
	 * Returns the number of ultimi prezzi articolis where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @return the number of matching ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUltimeCondizioniFornitori(boolean tipoSoggetto,
		String codiceArticolo, String codiceVariante) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ULTIMECONDIZIONIFORNITORI;

		Object[] finderArgs = new Object[] {
				tipoSoggetto, codiceArticolo, codiceVariante
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_ULTIMIPREZZIARTICOLI_WHERE);

			query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_TIPOSOGGETTO_2);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEVARIANTE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(tipoSoggetto);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_TIPOSOGGETTO_2 =
		"ultimiPrezziArticoli.id.tipoSoggetto = ? AND ";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEARTICOLO_1 =
		"ultimiPrezziArticoli.id.codiceArticolo IS NULL AND ";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEARTICOLO_2 =
		"ultimiPrezziArticoli.id.codiceArticolo = ? AND ";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEARTICOLO_3 =
		"(ultimiPrezziArticoli.id.codiceArticolo IS NULL OR ultimiPrezziArticoli.id.codiceArticolo = '') AND ";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEVARIANTE_1 =
		"ultimiPrezziArticoli.id.codiceVariante IS NULL";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEVARIANTE_2 =
		"ultimiPrezziArticoli.id.codiceVariante = ?";
	private static final String _FINDER_COLUMN_ULTIMECONDIZIONIFORNITORI_CODICEVARIANTE_3 =
		"(ultimiPrezziArticoli.id.codiceVariante IS NULL OR ultimiPrezziArticoli.id.codiceVariante = '')";

	public UltimiPrezziArticoliPersistenceImpl() {
		setModelClass(UltimiPrezziArticoli.class);
	}

	/**
	 * Caches the ultimi prezzi articoli in the entity cache if it is enabled.
	 *
	 * @param ultimiPrezziArticoli the ultimi prezzi articoli
	 */
	@Override
	public void cacheResult(UltimiPrezziArticoli ultimiPrezziArticoli) {
		EntityCacheUtil.putResult(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			UltimiPrezziArticoliImpl.class,
			ultimiPrezziArticoli.getPrimaryKey(), ultimiPrezziArticoli);

		ultimiPrezziArticoli.resetOriginalValues();
	}

	/**
	 * Caches the ultimi prezzi articolis in the entity cache if it is enabled.
	 *
	 * @param ultimiPrezziArticolis the ultimi prezzi articolis
	 */
	@Override
	public void cacheResult(List<UltimiPrezziArticoli> ultimiPrezziArticolis) {
		for (UltimiPrezziArticoli ultimiPrezziArticoli : ultimiPrezziArticolis) {
			if (EntityCacheUtil.getResult(
						UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
						UltimiPrezziArticoliImpl.class,
						ultimiPrezziArticoli.getPrimaryKey()) == null) {
				cacheResult(ultimiPrezziArticoli);
			}
			else {
				ultimiPrezziArticoli.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all ultimi prezzi articolis.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(UltimiPrezziArticoliImpl.class.getName());
		}

		EntityCacheUtil.clearCache(UltimiPrezziArticoliImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the ultimi prezzi articoli.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UltimiPrezziArticoli ultimiPrezziArticoli) {
		EntityCacheUtil.removeResult(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			UltimiPrezziArticoliImpl.class, ultimiPrezziArticoli.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<UltimiPrezziArticoli> ultimiPrezziArticolis) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UltimiPrezziArticoli ultimiPrezziArticoli : ultimiPrezziArticolis) {
			EntityCacheUtil.removeResult(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
				UltimiPrezziArticoliImpl.class,
				ultimiPrezziArticoli.getPrimaryKey());
		}
	}

	/**
	 * Creates a new ultimi prezzi articoli with the primary key. Does not add the ultimi prezzi articoli to the database.
	 *
	 * @param ultimiPrezziArticoliPK the primary key for the new ultimi prezzi articoli
	 * @return the new ultimi prezzi articoli
	 */
	@Override
	public UltimiPrezziArticoli create(
		UltimiPrezziArticoliPK ultimiPrezziArticoliPK) {
		UltimiPrezziArticoli ultimiPrezziArticoli = new UltimiPrezziArticoliImpl();

		ultimiPrezziArticoli.setNew(true);
		ultimiPrezziArticoli.setPrimaryKey(ultimiPrezziArticoliPK);

		return ultimiPrezziArticoli;
	}

	/**
	 * Removes the ultimi prezzi articoli with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ultimiPrezziArticoliPK the primary key of the ultimi prezzi articoli
	 * @return the ultimi prezzi articoli that was removed
	 * @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli remove(
		UltimiPrezziArticoliPK ultimiPrezziArticoliPK)
		throws NoSuchUltimiPrezziArticoliException, SystemException {
		return remove((Serializable)ultimiPrezziArticoliPK);
	}

	/**
	 * Removes the ultimi prezzi articoli with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the ultimi prezzi articoli
	 * @return the ultimi prezzi articoli that was removed
	 * @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli remove(Serializable primaryKey)
		throws NoSuchUltimiPrezziArticoliException, SystemException {
		Session session = null;

		try {
			session = openSession();

			UltimiPrezziArticoli ultimiPrezziArticoli = (UltimiPrezziArticoli)session.get(UltimiPrezziArticoliImpl.class,
					primaryKey);

			if (ultimiPrezziArticoli == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUltimiPrezziArticoliException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ultimiPrezziArticoli);
		}
		catch (NoSuchUltimiPrezziArticoliException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UltimiPrezziArticoli removeImpl(
		UltimiPrezziArticoli ultimiPrezziArticoli) throws SystemException {
		ultimiPrezziArticoli = toUnwrappedModel(ultimiPrezziArticoli);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ultimiPrezziArticoli)) {
				ultimiPrezziArticoli = (UltimiPrezziArticoli)session.get(UltimiPrezziArticoliImpl.class,
						ultimiPrezziArticoli.getPrimaryKeyObj());
			}

			if (ultimiPrezziArticoli != null) {
				session.delete(ultimiPrezziArticoli);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ultimiPrezziArticoli != null) {
			clearCache(ultimiPrezziArticoli);
		}

		return ultimiPrezziArticoli;
	}

	@Override
	public UltimiPrezziArticoli updateImpl(
		it.bysoftware.ct.model.UltimiPrezziArticoli ultimiPrezziArticoli)
		throws SystemException {
		ultimiPrezziArticoli = toUnwrappedModel(ultimiPrezziArticoli);

		boolean isNew = ultimiPrezziArticoli.isNew();

		UltimiPrezziArticoliModelImpl ultimiPrezziArticoliModelImpl = (UltimiPrezziArticoliModelImpl)ultimiPrezziArticoli;

		Session session = null;

		try {
			session = openSession();

			if (ultimiPrezziArticoli.isNew()) {
				session.save(ultimiPrezziArticoli);

				ultimiPrezziArticoli.setNew(false);
			}
			else {
				session.merge(ultimiPrezziArticoli);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !UltimiPrezziArticoliModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((ultimiPrezziArticoliModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ULTIMECONDIZIONI.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ultimiPrezziArticoliModelImpl.getOriginalTipoSoggetto(),
						ultimiPrezziArticoliModelImpl.getOriginalCodiceSoggetto()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ULTIMECONDIZIONI,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ULTIMECONDIZIONI,
					args);

				args = new Object[] {
						ultimiPrezziArticoliModelImpl.getTipoSoggetto(),
						ultimiPrezziArticoliModelImpl.getCodiceSoggetto()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ULTIMECONDIZIONI,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ULTIMECONDIZIONI,
					args);
			}

			if ((ultimiPrezziArticoliModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ULTIMECONDIZIONIARTICOLO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ultimiPrezziArticoliModelImpl.getOriginalTipoSoggetto(),
						ultimiPrezziArticoliModelImpl.getOriginalCodiceSoggetto(),
						ultimiPrezziArticoliModelImpl.getOriginalCodiceArticolo(),
						ultimiPrezziArticoliModelImpl.getOriginalCodiceVariante()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ULTIMECONDIZIONIARTICOLO,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ULTIMECONDIZIONIARTICOLO,
					args);

				args = new Object[] {
						ultimiPrezziArticoliModelImpl.getTipoSoggetto(),
						ultimiPrezziArticoliModelImpl.getCodiceSoggetto(),
						ultimiPrezziArticoliModelImpl.getCodiceArticolo(),
						ultimiPrezziArticoliModelImpl.getCodiceVariante()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ULTIMECONDIZIONIARTICOLO,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ULTIMECONDIZIONIARTICOLO,
					args);
			}

			if ((ultimiPrezziArticoliModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ULTIMECONDIZIONIFORNITORI.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ultimiPrezziArticoliModelImpl.getOriginalTipoSoggetto(),
						ultimiPrezziArticoliModelImpl.getOriginalCodiceArticolo(),
						ultimiPrezziArticoliModelImpl.getOriginalCodiceVariante()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ULTIMECONDIZIONIFORNITORI,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ULTIMECONDIZIONIFORNITORI,
					args);

				args = new Object[] {
						ultimiPrezziArticoliModelImpl.getTipoSoggetto(),
						ultimiPrezziArticoliModelImpl.getCodiceArticolo(),
						ultimiPrezziArticoliModelImpl.getCodiceVariante()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ULTIMECONDIZIONIFORNITORI,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ULTIMECONDIZIONIFORNITORI,
					args);
			}
		}

		EntityCacheUtil.putResult(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			UltimiPrezziArticoliImpl.class,
			ultimiPrezziArticoli.getPrimaryKey(), ultimiPrezziArticoli);

		return ultimiPrezziArticoli;
	}

	protected UltimiPrezziArticoli toUnwrappedModel(
		UltimiPrezziArticoli ultimiPrezziArticoli) {
		if (ultimiPrezziArticoli instanceof UltimiPrezziArticoliImpl) {
			return ultimiPrezziArticoli;
		}

		UltimiPrezziArticoliImpl ultimiPrezziArticoliImpl = new UltimiPrezziArticoliImpl();

		ultimiPrezziArticoliImpl.setNew(ultimiPrezziArticoli.isNew());
		ultimiPrezziArticoliImpl.setPrimaryKey(ultimiPrezziArticoli.getPrimaryKey());

		ultimiPrezziArticoliImpl.setTipoSoggetto(ultimiPrezziArticoli.isTipoSoggetto());
		ultimiPrezziArticoliImpl.setCodiceSoggetto(ultimiPrezziArticoli.getCodiceSoggetto());
		ultimiPrezziArticoliImpl.setCodiceArticolo(ultimiPrezziArticoli.getCodiceArticolo());
		ultimiPrezziArticoliImpl.setCodiceVariante(ultimiPrezziArticoli.getCodiceVariante());
		ultimiPrezziArticoliImpl.setDataDocumento(ultimiPrezziArticoli.getDataDocumento());
		ultimiPrezziArticoliImpl.setCodiceDivisa(ultimiPrezziArticoli.getCodiceDivisa());
		ultimiPrezziArticoliImpl.setPrezzo(ultimiPrezziArticoli.getPrezzo());
		ultimiPrezziArticoliImpl.setSconto1(ultimiPrezziArticoli.getSconto1());
		ultimiPrezziArticoliImpl.setSconto2(ultimiPrezziArticoli.getSconto2());
		ultimiPrezziArticoliImpl.setSconto3(ultimiPrezziArticoli.getSconto3());
		ultimiPrezziArticoliImpl.setScontoChiusura(ultimiPrezziArticoli.getScontoChiusura());

		return ultimiPrezziArticoliImpl;
	}

	/**
	 * Returns the ultimi prezzi articoli with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the ultimi prezzi articoli
	 * @return the ultimi prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUltimiPrezziArticoliException, SystemException {
		UltimiPrezziArticoli ultimiPrezziArticoli = fetchByPrimaryKey(primaryKey);

		if (ultimiPrezziArticoli == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUltimiPrezziArticoliException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ultimiPrezziArticoli;
	}

	/**
	 * Returns the ultimi prezzi articoli with the primary key or throws a {@link it.bysoftware.ct.NoSuchUltimiPrezziArticoliException} if it could not be found.
	 *
	 * @param ultimiPrezziArticoliPK the primary key of the ultimi prezzi articoli
	 * @return the ultimi prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli findByPrimaryKey(
		UltimiPrezziArticoliPK ultimiPrezziArticoliPK)
		throws NoSuchUltimiPrezziArticoliException, SystemException {
		return findByPrimaryKey((Serializable)ultimiPrezziArticoliPK);
	}

	/**
	 * Returns the ultimi prezzi articoli with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the ultimi prezzi articoli
	 * @return the ultimi prezzi articoli, or <code>null</code> if a ultimi prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		UltimiPrezziArticoli ultimiPrezziArticoli = (UltimiPrezziArticoli)EntityCacheUtil.getResult(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
				UltimiPrezziArticoliImpl.class, primaryKey);

		if (ultimiPrezziArticoli == _nullUltimiPrezziArticoli) {
			return null;
		}

		if (ultimiPrezziArticoli == null) {
			Session session = null;

			try {
				session = openSession();

				ultimiPrezziArticoli = (UltimiPrezziArticoli)session.get(UltimiPrezziArticoliImpl.class,
						primaryKey);

				if (ultimiPrezziArticoli != null) {
					cacheResult(ultimiPrezziArticoli);
				}
				else {
					EntityCacheUtil.putResult(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
						UltimiPrezziArticoliImpl.class, primaryKey,
						_nullUltimiPrezziArticoli);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(UltimiPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
					UltimiPrezziArticoliImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ultimiPrezziArticoli;
	}

	/**
	 * Returns the ultimi prezzi articoli with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ultimiPrezziArticoliPK the primary key of the ultimi prezzi articoli
	 * @return the ultimi prezzi articoli, or <code>null</code> if a ultimi prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UltimiPrezziArticoli fetchByPrimaryKey(
		UltimiPrezziArticoliPK ultimiPrezziArticoliPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)ultimiPrezziArticoliPK);
	}

	/**
	 * Returns all the ultimi prezzi articolis.
	 *
	 * @return the ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UltimiPrezziArticoli> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ultimi prezzi articolis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ultimi prezzi articolis
	 * @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	 * @return the range of ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UltimiPrezziArticoli> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the ultimi prezzi articolis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ultimi prezzi articolis
	 * @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UltimiPrezziArticoli> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UltimiPrezziArticoli> list = (List<UltimiPrezziArticoli>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ULTIMIPREZZIARTICOLI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ULTIMIPREZZIARTICOLI;

				if (pagination) {
					sql = sql.concat(UltimiPrezziArticoliModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UltimiPrezziArticoli>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UltimiPrezziArticoli>(list);
				}
				else {
					list = (List<UltimiPrezziArticoli>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the ultimi prezzi articolis from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (UltimiPrezziArticoli ultimiPrezziArticoli : findAll()) {
			remove(ultimiPrezziArticoli);
		}
	}

	/**
	 * Returns the number of ultimi prezzi articolis.
	 *
	 * @return the number of ultimi prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ULTIMIPREZZIARTICOLI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the ultimi prezzi articoli persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.UltimiPrezziArticoli")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<UltimiPrezziArticoli>> listenersList = new ArrayList<ModelListener<UltimiPrezziArticoli>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<UltimiPrezziArticoli>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(UltimiPrezziArticoliImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ULTIMIPREZZIARTICOLI = "SELECT ultimiPrezziArticoli FROM UltimiPrezziArticoli ultimiPrezziArticoli";
	private static final String _SQL_SELECT_ULTIMIPREZZIARTICOLI_WHERE = "SELECT ultimiPrezziArticoli FROM UltimiPrezziArticoli ultimiPrezziArticoli WHERE ";
	private static final String _SQL_COUNT_ULTIMIPREZZIARTICOLI = "SELECT COUNT(ultimiPrezziArticoli) FROM UltimiPrezziArticoli ultimiPrezziArticoli";
	private static final String _SQL_COUNT_ULTIMIPREZZIARTICOLI_WHERE = "SELECT COUNT(ultimiPrezziArticoli) FROM UltimiPrezziArticoli ultimiPrezziArticoli WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ultimiPrezziArticoli.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UltimiPrezziArticoli exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No UltimiPrezziArticoli exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(UltimiPrezziArticoliPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"tipoSoggetto", "codiceSoggetto", "codiceArticolo",
				"codiceVariante", "dataDocumento", "codiceDivisa", "prezzo",
				"sconto1", "sconto2", "sconto3", "scontoChiusura"
			});
	private static UltimiPrezziArticoli _nullUltimiPrezziArticoli = new UltimiPrezziArticoliImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<UltimiPrezziArticoli> toCacheModel() {
				return _nullUltimiPrezziArticoliCacheModel;
			}
		};

	private static CacheModel<UltimiPrezziArticoli> _nullUltimiPrezziArticoliCacheModel =
		new CacheModel<UltimiPrezziArticoli>() {
			@Override
			public UltimiPrezziArticoli toEntityModel() {
				return _nullUltimiPrezziArticoli;
			}
		};
}