/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException;
import it.bysoftware.ct.model.WKRigheOrdiniClienti;
import it.bysoftware.ct.model.impl.WKRigheOrdiniClientiImpl;
import it.bysoftware.ct.model.impl.WKRigheOrdiniClientiModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the w k righe ordini clienti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see WKRigheOrdiniClientiPersistence
 * @see WKRigheOrdiniClientiUtil
 * @generated
 */
public class WKRigheOrdiniClientiPersistenceImpl extends BasePersistenceImpl<WKRigheOrdiniClienti>
	implements WKRigheOrdiniClientiPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link WKRigheOrdiniClientiUtil} to access the w k righe ordini clienti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = WKRigheOrdiniClientiImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(WKRigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKRigheOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			WKRigheOrdiniClientiImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(WKRigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKRigheOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			WKRigheOrdiniClientiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(WKRigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKRigheOrdiniClientiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ANNOTIPOORDINENUMEROORDINE =
		new FinderPath(WKRigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKRigheOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			WKRigheOrdiniClientiImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByAnnoTipoOrdineNumeroOrdine",
			new String[] {
				Integer.class.getName(), Integer.class.getName(),
				Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOTIPOORDINENUMEROORDINE =
		new FinderPath(WKRigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKRigheOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			WKRigheOrdiniClientiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByAnnoTipoOrdineNumeroOrdine",
			new String[] {
				Integer.class.getName(), Integer.class.getName(),
				Integer.class.getName()
			},
			WKRigheOrdiniClientiModelImpl.ANNO_COLUMN_BITMASK |
			WKRigheOrdiniClientiModelImpl.TIPOORDINE_COLUMN_BITMASK |
			WKRigheOrdiniClientiModelImpl.NUMEROORDINE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ANNOTIPOORDINENUMEROORDINE =
		new FinderPath(WKRigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKRigheOrdiniClientiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByAnnoTipoOrdineNumeroOrdine",
			new String[] {
				Integer.class.getName(), Integer.class.getName(),
				Integer.class.getName()
			});

	/**
	 * Returns all the w k righe ordini clientis where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @return the matching w k righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKRigheOrdiniClienti> findByAnnoTipoOrdineNumeroOrdine(
		int anno, int tipoOrdine, int numeroOrdine) throws SystemException {
		return findByAnnoTipoOrdineNumeroOrdine(anno, tipoOrdine, numeroOrdine,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the w k righe ordini clientis where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKRigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param start the lower bound of the range of w k righe ordini clientis
	 * @param end the upper bound of the range of w k righe ordini clientis (not inclusive)
	 * @return the range of matching w k righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKRigheOrdiniClienti> findByAnnoTipoOrdineNumeroOrdine(
		int anno, int tipoOrdine, int numeroOrdine, int start, int end)
		throws SystemException {
		return findByAnnoTipoOrdineNumeroOrdine(anno, tipoOrdine, numeroOrdine,
			start, end, null);
	}

	/**
	 * Returns an ordered range of all the w k righe ordini clientis where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKRigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param start the lower bound of the range of w k righe ordini clientis
	 * @param end the upper bound of the range of w k righe ordini clientis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching w k righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKRigheOrdiniClienti> findByAnnoTipoOrdineNumeroOrdine(
		int anno, int tipoOrdine, int numeroOrdine, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOTIPOORDINENUMEROORDINE;
			finderArgs = new Object[] { anno, tipoOrdine, numeroOrdine };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ANNOTIPOORDINENUMEROORDINE;
			finderArgs = new Object[] {
					anno, tipoOrdine, numeroOrdine,
					
					start, end, orderByComparator
				};
		}

		List<WKRigheOrdiniClienti> list = (List<WKRigheOrdiniClienti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (WKRigheOrdiniClienti wkRigheOrdiniClienti : list) {
				if ((anno != wkRigheOrdiniClienti.getAnno()) ||
						(tipoOrdine != wkRigheOrdiniClienti.getTipoOrdine()) ||
						(numeroOrdine != wkRigheOrdiniClienti.getNumeroOrdine())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_WKRIGHEORDINICLIENTI_WHERE);

			query.append(_FINDER_COLUMN_ANNOTIPOORDINENUMEROORDINE_ANNO_2);

			query.append(_FINDER_COLUMN_ANNOTIPOORDINENUMEROORDINE_TIPOORDINE_2);

			query.append(_FINDER_COLUMN_ANNOTIPOORDINENUMEROORDINE_NUMEROORDINE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(WKRigheOrdiniClientiModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(anno);

				qPos.add(tipoOrdine);

				qPos.add(numeroOrdine);

				if (!pagination) {
					list = (List<WKRigheOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<WKRigheOrdiniClienti>(list);
				}
				else {
					list = (List<WKRigheOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first w k righe ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching w k righe ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a matching w k righe ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKRigheOrdiniClienti findByAnnoTipoOrdineNumeroOrdine_First(
		int anno, int tipoOrdine, int numeroOrdine,
		OrderByComparator orderByComparator)
		throws NoSuchWKRigheOrdiniClientiException, SystemException {
		WKRigheOrdiniClienti wkRigheOrdiniClienti = fetchByAnnoTipoOrdineNumeroOrdine_First(anno,
				tipoOrdine, numeroOrdine, orderByComparator);

		if (wkRigheOrdiniClienti != null) {
			return wkRigheOrdiniClienti;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("anno=");
		msg.append(anno);

		msg.append(", tipoOrdine=");
		msg.append(tipoOrdine);

		msg.append(", numeroOrdine=");
		msg.append(numeroOrdine);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWKRigheOrdiniClientiException(msg.toString());
	}

	/**
	 * Returns the first w k righe ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching w k righe ordini clienti, or <code>null</code> if a matching w k righe ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKRigheOrdiniClienti fetchByAnnoTipoOrdineNumeroOrdine_First(
		int anno, int tipoOrdine, int numeroOrdine,
		OrderByComparator orderByComparator) throws SystemException {
		List<WKRigheOrdiniClienti> list = findByAnnoTipoOrdineNumeroOrdine(anno,
				tipoOrdine, numeroOrdine, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last w k righe ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching w k righe ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a matching w k righe ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKRigheOrdiniClienti findByAnnoTipoOrdineNumeroOrdine_Last(
		int anno, int tipoOrdine, int numeroOrdine,
		OrderByComparator orderByComparator)
		throws NoSuchWKRigheOrdiniClientiException, SystemException {
		WKRigheOrdiniClienti wkRigheOrdiniClienti = fetchByAnnoTipoOrdineNumeroOrdine_Last(anno,
				tipoOrdine, numeroOrdine, orderByComparator);

		if (wkRigheOrdiniClienti != null) {
			return wkRigheOrdiniClienti;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("anno=");
		msg.append(anno);

		msg.append(", tipoOrdine=");
		msg.append(tipoOrdine);

		msg.append(", numeroOrdine=");
		msg.append(numeroOrdine);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWKRigheOrdiniClientiException(msg.toString());
	}

	/**
	 * Returns the last w k righe ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching w k righe ordini clienti, or <code>null</code> if a matching w k righe ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKRigheOrdiniClienti fetchByAnnoTipoOrdineNumeroOrdine_Last(
		int anno, int tipoOrdine, int numeroOrdine,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByAnnoTipoOrdineNumeroOrdine(anno, tipoOrdine,
				numeroOrdine);

		if (count == 0) {
			return null;
		}

		List<WKRigheOrdiniClienti> list = findByAnnoTipoOrdineNumeroOrdine(anno,
				tipoOrdine, numeroOrdine, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the w k righe ordini clientis before and after the current w k righe ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * @param wkRigheOrdiniClientiPK the primary key of the current w k righe ordini clienti
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next w k righe ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a w k righe ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKRigheOrdiniClienti[] findByAnnoTipoOrdineNumeroOrdine_PrevAndNext(
		WKRigheOrdiniClientiPK wkRigheOrdiniClientiPK, int anno,
		int tipoOrdine, int numeroOrdine, OrderByComparator orderByComparator)
		throws NoSuchWKRigheOrdiniClientiException, SystemException {
		WKRigheOrdiniClienti wkRigheOrdiniClienti = findByPrimaryKey(wkRigheOrdiniClientiPK);

		Session session = null;

		try {
			session = openSession();

			WKRigheOrdiniClienti[] array = new WKRigheOrdiniClientiImpl[3];

			array[0] = getByAnnoTipoOrdineNumeroOrdine_PrevAndNext(session,
					wkRigheOrdiniClienti, anno, tipoOrdine, numeroOrdine,
					orderByComparator, true);

			array[1] = wkRigheOrdiniClienti;

			array[2] = getByAnnoTipoOrdineNumeroOrdine_PrevAndNext(session,
					wkRigheOrdiniClienti, anno, tipoOrdine, numeroOrdine,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected WKRigheOrdiniClienti getByAnnoTipoOrdineNumeroOrdine_PrevAndNext(
		Session session, WKRigheOrdiniClienti wkRigheOrdiniClienti, int anno,
		int tipoOrdine, int numeroOrdine, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_WKRIGHEORDINICLIENTI_WHERE);

		query.append(_FINDER_COLUMN_ANNOTIPOORDINENUMEROORDINE_ANNO_2);

		query.append(_FINDER_COLUMN_ANNOTIPOORDINENUMEROORDINE_TIPOORDINE_2);

		query.append(_FINDER_COLUMN_ANNOTIPOORDINENUMEROORDINE_NUMEROORDINE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(WKRigheOrdiniClientiModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(anno);

		qPos.add(tipoOrdine);

		qPos.add(numeroOrdine);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(wkRigheOrdiniClienti);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<WKRigheOrdiniClienti> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the w k righe ordini clientis where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; from the database.
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByAnnoTipoOrdineNumeroOrdine(int anno, int tipoOrdine,
		int numeroOrdine) throws SystemException {
		for (WKRigheOrdiniClienti wkRigheOrdiniClienti : findByAnnoTipoOrdineNumeroOrdine(
				anno, tipoOrdine, numeroOrdine, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(wkRigheOrdiniClienti);
		}
	}

	/**
	 * Returns the number of w k righe ordini clientis where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param numeroOrdine the numero ordine
	 * @return the number of matching w k righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByAnnoTipoOrdineNumeroOrdine(int anno, int tipoOrdine,
		int numeroOrdine) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ANNOTIPOORDINENUMEROORDINE;

		Object[] finderArgs = new Object[] { anno, tipoOrdine, numeroOrdine };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_WKRIGHEORDINICLIENTI_WHERE);

			query.append(_FINDER_COLUMN_ANNOTIPOORDINENUMEROORDINE_ANNO_2);

			query.append(_FINDER_COLUMN_ANNOTIPOORDINENUMEROORDINE_TIPOORDINE_2);

			query.append(_FINDER_COLUMN_ANNOTIPOORDINENUMEROORDINE_NUMEROORDINE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(anno);

				qPos.add(tipoOrdine);

				qPos.add(numeroOrdine);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ANNOTIPOORDINENUMEROORDINE_ANNO_2 =
		"wkRigheOrdiniClienti.id.anno = ? AND ";
	private static final String _FINDER_COLUMN_ANNOTIPOORDINENUMEROORDINE_TIPOORDINE_2 =
		"wkRigheOrdiniClienti.id.tipoOrdine = ? AND ";
	private static final String _FINDER_COLUMN_ANNOTIPOORDINENUMEROORDINE_NUMEROORDINE_2 =
		"wkRigheOrdiniClienti.id.numeroOrdine = ?";

	public WKRigheOrdiniClientiPersistenceImpl() {
		setModelClass(WKRigheOrdiniClienti.class);
	}

	/**
	 * Caches the w k righe ordini clienti in the entity cache if it is enabled.
	 *
	 * @param wkRigheOrdiniClienti the w k righe ordini clienti
	 */
	@Override
	public void cacheResult(WKRigheOrdiniClienti wkRigheOrdiniClienti) {
		EntityCacheUtil.putResult(WKRigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKRigheOrdiniClientiImpl.class,
			wkRigheOrdiniClienti.getPrimaryKey(), wkRigheOrdiniClienti);

		wkRigheOrdiniClienti.resetOriginalValues();
	}

	/**
	 * Caches the w k righe ordini clientis in the entity cache if it is enabled.
	 *
	 * @param wkRigheOrdiniClientis the w k righe ordini clientis
	 */
	@Override
	public void cacheResult(List<WKRigheOrdiniClienti> wkRigheOrdiniClientis) {
		for (WKRigheOrdiniClienti wkRigheOrdiniClienti : wkRigheOrdiniClientis) {
			if (EntityCacheUtil.getResult(
						WKRigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
						WKRigheOrdiniClientiImpl.class,
						wkRigheOrdiniClienti.getPrimaryKey()) == null) {
				cacheResult(wkRigheOrdiniClienti);
			}
			else {
				wkRigheOrdiniClienti.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all w k righe ordini clientis.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(WKRigheOrdiniClientiImpl.class.getName());
		}

		EntityCacheUtil.clearCache(WKRigheOrdiniClientiImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the w k righe ordini clienti.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(WKRigheOrdiniClienti wkRigheOrdiniClienti) {
		EntityCacheUtil.removeResult(WKRigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKRigheOrdiniClientiImpl.class, wkRigheOrdiniClienti.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<WKRigheOrdiniClienti> wkRigheOrdiniClientis) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (WKRigheOrdiniClienti wkRigheOrdiniClienti : wkRigheOrdiniClientis) {
			EntityCacheUtil.removeResult(WKRigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
				WKRigheOrdiniClientiImpl.class,
				wkRigheOrdiniClienti.getPrimaryKey());
		}
	}

	/**
	 * Creates a new w k righe ordini clienti with the primary key. Does not add the w k righe ordini clienti to the database.
	 *
	 * @param wkRigheOrdiniClientiPK the primary key for the new w k righe ordini clienti
	 * @return the new w k righe ordini clienti
	 */
	@Override
	public WKRigheOrdiniClienti create(
		WKRigheOrdiniClientiPK wkRigheOrdiniClientiPK) {
		WKRigheOrdiniClienti wkRigheOrdiniClienti = new WKRigheOrdiniClientiImpl();

		wkRigheOrdiniClienti.setNew(true);
		wkRigheOrdiniClienti.setPrimaryKey(wkRigheOrdiniClientiPK);

		return wkRigheOrdiniClienti;
	}

	/**
	 * Removes the w k righe ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param wkRigheOrdiniClientiPK the primary key of the w k righe ordini clienti
	 * @return the w k righe ordini clienti that was removed
	 * @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a w k righe ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKRigheOrdiniClienti remove(
		WKRigheOrdiniClientiPK wkRigheOrdiniClientiPK)
		throws NoSuchWKRigheOrdiniClientiException, SystemException {
		return remove((Serializable)wkRigheOrdiniClientiPK);
	}

	/**
	 * Removes the w k righe ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the w k righe ordini clienti
	 * @return the w k righe ordini clienti that was removed
	 * @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a w k righe ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKRigheOrdiniClienti remove(Serializable primaryKey)
		throws NoSuchWKRigheOrdiniClientiException, SystemException {
		Session session = null;

		try {
			session = openSession();

			WKRigheOrdiniClienti wkRigheOrdiniClienti = (WKRigheOrdiniClienti)session.get(WKRigheOrdiniClientiImpl.class,
					primaryKey);

			if (wkRigheOrdiniClienti == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchWKRigheOrdiniClientiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(wkRigheOrdiniClienti);
		}
		catch (NoSuchWKRigheOrdiniClientiException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected WKRigheOrdiniClienti removeImpl(
		WKRigheOrdiniClienti wkRigheOrdiniClienti) throws SystemException {
		wkRigheOrdiniClienti = toUnwrappedModel(wkRigheOrdiniClienti);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(wkRigheOrdiniClienti)) {
				wkRigheOrdiniClienti = (WKRigheOrdiniClienti)session.get(WKRigheOrdiniClientiImpl.class,
						wkRigheOrdiniClienti.getPrimaryKeyObj());
			}

			if (wkRigheOrdiniClienti != null) {
				session.delete(wkRigheOrdiniClienti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (wkRigheOrdiniClienti != null) {
			clearCache(wkRigheOrdiniClienti);
		}

		return wkRigheOrdiniClienti;
	}

	@Override
	public WKRigheOrdiniClienti updateImpl(
		it.bysoftware.ct.model.WKRigheOrdiniClienti wkRigheOrdiniClienti)
		throws SystemException {
		wkRigheOrdiniClienti = toUnwrappedModel(wkRigheOrdiniClienti);

		boolean isNew = wkRigheOrdiniClienti.isNew();

		WKRigheOrdiniClientiModelImpl wkRigheOrdiniClientiModelImpl = (WKRigheOrdiniClientiModelImpl)wkRigheOrdiniClienti;

		Session session = null;

		try {
			session = openSession();

			if (wkRigheOrdiniClienti.isNew()) {
				session.save(wkRigheOrdiniClienti);

				wkRigheOrdiniClienti.setNew(false);
			}
			else {
				session.merge(wkRigheOrdiniClienti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !WKRigheOrdiniClientiModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((wkRigheOrdiniClientiModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOTIPOORDINENUMEROORDINE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						wkRigheOrdiniClientiModelImpl.getOriginalAnno(),
						wkRigheOrdiniClientiModelImpl.getOriginalTipoOrdine(),
						wkRigheOrdiniClientiModelImpl.getOriginalNumeroOrdine()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNOTIPOORDINENUMEROORDINE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOTIPOORDINENUMEROORDINE,
					args);

				args = new Object[] {
						wkRigheOrdiniClientiModelImpl.getAnno(),
						wkRigheOrdiniClientiModelImpl.getTipoOrdine(),
						wkRigheOrdiniClientiModelImpl.getNumeroOrdine()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNOTIPOORDINENUMEROORDINE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOTIPOORDINENUMEROORDINE,
					args);
			}
		}

		EntityCacheUtil.putResult(WKRigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKRigheOrdiniClientiImpl.class,
			wkRigheOrdiniClienti.getPrimaryKey(), wkRigheOrdiniClienti);

		return wkRigheOrdiniClienti;
	}

	protected WKRigheOrdiniClienti toUnwrappedModel(
		WKRigheOrdiniClienti wkRigheOrdiniClienti) {
		if (wkRigheOrdiniClienti instanceof WKRigheOrdiniClientiImpl) {
			return wkRigheOrdiniClienti;
		}

		WKRigheOrdiniClientiImpl wkRigheOrdiniClientiImpl = new WKRigheOrdiniClientiImpl();

		wkRigheOrdiniClientiImpl.setNew(wkRigheOrdiniClienti.isNew());
		wkRigheOrdiniClientiImpl.setPrimaryKey(wkRigheOrdiniClienti.getPrimaryKey());

		wkRigheOrdiniClientiImpl.setAnno(wkRigheOrdiniClienti.getAnno());
		wkRigheOrdiniClientiImpl.setTipoOrdine(wkRigheOrdiniClienti.getTipoOrdine());
		wkRigheOrdiniClientiImpl.setNumeroOrdine(wkRigheOrdiniClienti.getNumeroOrdine());
		wkRigheOrdiniClientiImpl.setNumeroRigo(wkRigheOrdiniClienti.getNumeroRigo());
		wkRigheOrdiniClientiImpl.setStatoRigo(wkRigheOrdiniClienti.isStatoRigo());
		wkRigheOrdiniClientiImpl.setTipoRigo(wkRigheOrdiniClienti.getTipoRigo());
		wkRigheOrdiniClientiImpl.setCodiceDepositoMov(wkRigheOrdiniClienti.getCodiceDepositoMov());
		wkRigheOrdiniClientiImpl.setCodiceArticolo(wkRigheOrdiniClienti.getCodiceArticolo());
		wkRigheOrdiniClientiImpl.setCodiceVariante(wkRigheOrdiniClienti.getCodiceVariante());
		wkRigheOrdiniClientiImpl.setDescrizione(wkRigheOrdiniClienti.getDescrizione());
		wkRigheOrdiniClientiImpl.setCodiceUnitMis(wkRigheOrdiniClienti.getCodiceUnitMis());
		wkRigheOrdiniClientiImpl.setDecimaliQuant(wkRigheOrdiniClienti.getDecimaliQuant());
		wkRigheOrdiniClientiImpl.setQuantita1(wkRigheOrdiniClienti.getQuantita1());
		wkRigheOrdiniClientiImpl.setQuantita2(wkRigheOrdiniClienti.getQuantita2());
		wkRigheOrdiniClientiImpl.setQuantita3(wkRigheOrdiniClienti.getQuantita3());
		wkRigheOrdiniClientiImpl.setQuantita(wkRigheOrdiniClienti.getQuantita());
		wkRigheOrdiniClientiImpl.setCodiceUnitMis2(wkRigheOrdiniClienti.getCodiceUnitMis2());
		wkRigheOrdiniClientiImpl.setQuantitaUnitMis2(wkRigheOrdiniClienti.getQuantitaUnitMis2());
		wkRigheOrdiniClientiImpl.setDecimaliPrezzo(wkRigheOrdiniClienti.getDecimaliPrezzo());
		wkRigheOrdiniClientiImpl.setPrezzo(wkRigheOrdiniClienti.getPrezzo());
		wkRigheOrdiniClientiImpl.setImportoLordo(wkRigheOrdiniClienti.getImportoLordo());
		wkRigheOrdiniClientiImpl.setSconto1(wkRigheOrdiniClienti.getSconto1());
		wkRigheOrdiniClientiImpl.setSconto2(wkRigheOrdiniClienti.getSconto2());
		wkRigheOrdiniClientiImpl.setSconto3(wkRigheOrdiniClienti.getSconto3());
		wkRigheOrdiniClientiImpl.setImportoNetto(wkRigheOrdiniClienti.getImportoNetto());
		wkRigheOrdiniClientiImpl.setImporto(wkRigheOrdiniClienti.getImporto());
		wkRigheOrdiniClientiImpl.setCodiceIVAFatturazione(wkRigheOrdiniClienti.getCodiceIVAFatturazione());
		wkRigheOrdiniClientiImpl.setCodiceCliente(wkRigheOrdiniClienti.getCodiceCliente());
		wkRigheOrdiniClientiImpl.setRiferimentoOrdineCliente(wkRigheOrdiniClienti.getRiferimentoOrdineCliente());
		wkRigheOrdiniClientiImpl.setDataOridine(wkRigheOrdiniClienti.getDataOridine());
		wkRigheOrdiniClientiImpl.setStatoEvasione(wkRigheOrdiniClienti.isStatoEvasione());
		wkRigheOrdiniClientiImpl.setDataPrevistaConsegna(wkRigheOrdiniClienti.getDataPrevistaConsegna());
		wkRigheOrdiniClientiImpl.setDataRegistrazioneOrdine(wkRigheOrdiniClienti.getDataRegistrazioneOrdine());
		wkRigheOrdiniClientiImpl.setLibStr1(wkRigheOrdiniClienti.getLibStr1());
		wkRigheOrdiniClientiImpl.setLibStr2(wkRigheOrdiniClienti.getLibStr2());
		wkRigheOrdiniClientiImpl.setLibStr3(wkRigheOrdiniClienti.getLibStr3());
		wkRigheOrdiniClientiImpl.setLibDbl1(wkRigheOrdiniClienti.getLibDbl1());
		wkRigheOrdiniClientiImpl.setLibDbl2(wkRigheOrdiniClienti.getLibDbl2());
		wkRigheOrdiniClientiImpl.setLibDbl3(wkRigheOrdiniClienti.getLibDbl3());
		wkRigheOrdiniClientiImpl.setLibDat1(wkRigheOrdiniClienti.getLibDat1());
		wkRigheOrdiniClientiImpl.setLibDat2(wkRigheOrdiniClienti.getLibDat2());
		wkRigheOrdiniClientiImpl.setLibDat3(wkRigheOrdiniClienti.getLibDat3());
		wkRigheOrdiniClientiImpl.setLibLng1(wkRigheOrdiniClienti.getLibLng1());
		wkRigheOrdiniClientiImpl.setLibLng2(wkRigheOrdiniClienti.getLibLng2());
		wkRigheOrdiniClientiImpl.setLibLng3(wkRigheOrdiniClienti.getLibLng3());

		return wkRigheOrdiniClientiImpl;
	}

	/**
	 * Returns the w k righe ordini clienti with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the w k righe ordini clienti
	 * @return the w k righe ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a w k righe ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKRigheOrdiniClienti findByPrimaryKey(Serializable primaryKey)
		throws NoSuchWKRigheOrdiniClientiException, SystemException {
		WKRigheOrdiniClienti wkRigheOrdiniClienti = fetchByPrimaryKey(primaryKey);

		if (wkRigheOrdiniClienti == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchWKRigheOrdiniClientiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return wkRigheOrdiniClienti;
	}

	/**
	 * Returns the w k righe ordini clienti with the primary key or throws a {@link it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException} if it could not be found.
	 *
	 * @param wkRigheOrdiniClientiPK the primary key of the w k righe ordini clienti
	 * @return the w k righe ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a w k righe ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKRigheOrdiniClienti findByPrimaryKey(
		WKRigheOrdiniClientiPK wkRigheOrdiniClientiPK)
		throws NoSuchWKRigheOrdiniClientiException, SystemException {
		return findByPrimaryKey((Serializable)wkRigheOrdiniClientiPK);
	}

	/**
	 * Returns the w k righe ordini clienti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the w k righe ordini clienti
	 * @return the w k righe ordini clienti, or <code>null</code> if a w k righe ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKRigheOrdiniClienti fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		WKRigheOrdiniClienti wkRigheOrdiniClienti = (WKRigheOrdiniClienti)EntityCacheUtil.getResult(WKRigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
				WKRigheOrdiniClientiImpl.class, primaryKey);

		if (wkRigheOrdiniClienti == _nullWKRigheOrdiniClienti) {
			return null;
		}

		if (wkRigheOrdiniClienti == null) {
			Session session = null;

			try {
				session = openSession();

				wkRigheOrdiniClienti = (WKRigheOrdiniClienti)session.get(WKRigheOrdiniClientiImpl.class,
						primaryKey);

				if (wkRigheOrdiniClienti != null) {
					cacheResult(wkRigheOrdiniClienti);
				}
				else {
					EntityCacheUtil.putResult(WKRigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
						WKRigheOrdiniClientiImpl.class, primaryKey,
						_nullWKRigheOrdiniClienti);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(WKRigheOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
					WKRigheOrdiniClientiImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return wkRigheOrdiniClienti;
	}

	/**
	 * Returns the w k righe ordini clienti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param wkRigheOrdiniClientiPK the primary key of the w k righe ordini clienti
	 * @return the w k righe ordini clienti, or <code>null</code> if a w k righe ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKRigheOrdiniClienti fetchByPrimaryKey(
		WKRigheOrdiniClientiPK wkRigheOrdiniClientiPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)wkRigheOrdiniClientiPK);
	}

	/**
	 * Returns all the w k righe ordini clientis.
	 *
	 * @return the w k righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKRigheOrdiniClienti> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the w k righe ordini clientis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKRigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of w k righe ordini clientis
	 * @param end the upper bound of the range of w k righe ordini clientis (not inclusive)
	 * @return the range of w k righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKRigheOrdiniClienti> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the w k righe ordini clientis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKRigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of w k righe ordini clientis
	 * @param end the upper bound of the range of w k righe ordini clientis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of w k righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKRigheOrdiniClienti> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<WKRigheOrdiniClienti> list = (List<WKRigheOrdiniClienti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_WKRIGHEORDINICLIENTI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_WKRIGHEORDINICLIENTI;

				if (pagination) {
					sql = sql.concat(WKRigheOrdiniClientiModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<WKRigheOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<WKRigheOrdiniClienti>(list);
				}
				else {
					list = (List<WKRigheOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the w k righe ordini clientis from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (WKRigheOrdiniClienti wkRigheOrdiniClienti : findAll()) {
			remove(wkRigheOrdiniClienti);
		}
	}

	/**
	 * Returns the number of w k righe ordini clientis.
	 *
	 * @return the number of w k righe ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_WKRIGHEORDINICLIENTI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the w k righe ordini clienti persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.WKRigheOrdiniClienti")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<WKRigheOrdiniClienti>> listenersList = new ArrayList<ModelListener<WKRigheOrdiniClienti>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<WKRigheOrdiniClienti>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(WKRigheOrdiniClientiImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_WKRIGHEORDINICLIENTI = "SELECT wkRigheOrdiniClienti FROM WKRigheOrdiniClienti wkRigheOrdiniClienti";
	private static final String _SQL_SELECT_WKRIGHEORDINICLIENTI_WHERE = "SELECT wkRigheOrdiniClienti FROM WKRigheOrdiniClienti wkRigheOrdiniClienti WHERE ";
	private static final String _SQL_COUNT_WKRIGHEORDINICLIENTI = "SELECT COUNT(wkRigheOrdiniClienti) FROM WKRigheOrdiniClienti wkRigheOrdiniClienti";
	private static final String _SQL_COUNT_WKRIGHEORDINICLIENTI_WHERE = "SELECT COUNT(wkRigheOrdiniClienti) FROM WKRigheOrdiniClienti wkRigheOrdiniClienti WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "wkRigheOrdiniClienti.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No WKRigheOrdiniClienti exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No WKRigheOrdiniClienti exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(WKRigheOrdiniClientiPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"anno", "tipoOrdine", "numeroOrdine", "numeroRigo", "statoRigo",
				"tipoRigo", "codiceDepositoMov", "codiceArticolo",
				"codiceVariante", "descrizione", "codiceUnitMis",
				"decimaliQuant", "quantita1", "quantita2", "quantita3",
				"quantita", "codiceUnitMis2", "quantitaUnitMis2",
				"decimaliPrezzo", "prezzo", "importoLordo", "sconto1", "sconto2",
				"sconto3", "importoNetto", "importo", "codiceIVAFatturazione",
				"codiceCliente", "riferimentoOrdineCliente", "dataOridine",
				"statoEvasione", "dataPrevistaConsegna",
				"dataRegistrazioneOrdine", "libStr1", "libStr2", "libStr3",
				"libDbl1", "libDbl2", "libDbl3", "libDat1", "libDat2", "libDat3",
				"libLng1", "libLng2", "libLng3"
			});
	private static WKRigheOrdiniClienti _nullWKRigheOrdiniClienti = new WKRigheOrdiniClientiImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<WKRigheOrdiniClienti> toCacheModel() {
				return _nullWKRigheOrdiniClientiCacheModel;
			}
		};

	private static CacheModel<WKRigheOrdiniClienti> _nullWKRigheOrdiniClientiCacheModel =
		new CacheModel<WKRigheOrdiniClienti>() {
			@Override
			public WKRigheOrdiniClienti toEntityModel() {
				return _nullWKRigheOrdiniClienti;
			}
		};
}