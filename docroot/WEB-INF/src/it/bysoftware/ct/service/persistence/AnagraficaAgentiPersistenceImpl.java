/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchAnagraficaAgentiException;
import it.bysoftware.ct.model.AnagraficaAgenti;
import it.bysoftware.ct.model.impl.AnagraficaAgentiImpl;
import it.bysoftware.ct.model.impl.AnagraficaAgentiModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the anagrafica agenti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see AnagraficaAgentiPersistence
 * @see AnagraficaAgentiUtil
 * @generated
 */
public class AnagraficaAgentiPersistenceImpl extends BasePersistenceImpl<AnagraficaAgenti>
	implements AnagraficaAgentiPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AnagraficaAgentiUtil} to access the anagrafica agenti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AnagraficaAgentiImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AnagraficaAgentiModelImpl.ENTITY_CACHE_ENABLED,
			AnagraficaAgentiModelImpl.FINDER_CACHE_ENABLED,
			AnagraficaAgentiImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AnagraficaAgentiModelImpl.ENTITY_CACHE_ENABLED,
			AnagraficaAgentiModelImpl.FINDER_CACHE_ENABLED,
			AnagraficaAgentiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AnagraficaAgentiModelImpl.ENTITY_CACHE_ENABLED,
			AnagraficaAgentiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public AnagraficaAgentiPersistenceImpl() {
		setModelClass(AnagraficaAgenti.class);
	}

	/**
	 * Caches the anagrafica agenti in the entity cache if it is enabled.
	 *
	 * @param anagraficaAgenti the anagrafica agenti
	 */
	@Override
	public void cacheResult(AnagraficaAgenti anagraficaAgenti) {
		EntityCacheUtil.putResult(AnagraficaAgentiModelImpl.ENTITY_CACHE_ENABLED,
			AnagraficaAgentiImpl.class, anagraficaAgenti.getPrimaryKey(),
			anagraficaAgenti);

		anagraficaAgenti.resetOriginalValues();
	}

	/**
	 * Caches the anagrafica agentis in the entity cache if it is enabled.
	 *
	 * @param anagraficaAgentis the anagrafica agentis
	 */
	@Override
	public void cacheResult(List<AnagraficaAgenti> anagraficaAgentis) {
		for (AnagraficaAgenti anagraficaAgenti : anagraficaAgentis) {
			if (EntityCacheUtil.getResult(
						AnagraficaAgentiModelImpl.ENTITY_CACHE_ENABLED,
						AnagraficaAgentiImpl.class,
						anagraficaAgenti.getPrimaryKey()) == null) {
				cacheResult(anagraficaAgenti);
			}
			else {
				anagraficaAgenti.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all anagrafica agentis.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AnagraficaAgentiImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AnagraficaAgentiImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the anagrafica agenti.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AnagraficaAgenti anagraficaAgenti) {
		EntityCacheUtil.removeResult(AnagraficaAgentiModelImpl.ENTITY_CACHE_ENABLED,
			AnagraficaAgentiImpl.class, anagraficaAgenti.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<AnagraficaAgenti> anagraficaAgentis) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (AnagraficaAgenti anagraficaAgenti : anagraficaAgentis) {
			EntityCacheUtil.removeResult(AnagraficaAgentiModelImpl.ENTITY_CACHE_ENABLED,
				AnagraficaAgentiImpl.class, anagraficaAgenti.getPrimaryKey());
		}
	}

	/**
	 * Creates a new anagrafica agenti with the primary key. Does not add the anagrafica agenti to the database.
	 *
	 * @param codiceAgente the primary key for the new anagrafica agenti
	 * @return the new anagrafica agenti
	 */
	@Override
	public AnagraficaAgenti create(String codiceAgente) {
		AnagraficaAgenti anagraficaAgenti = new AnagraficaAgentiImpl();

		anagraficaAgenti.setNew(true);
		anagraficaAgenti.setPrimaryKey(codiceAgente);

		return anagraficaAgenti;
	}

	/**
	 * Removes the anagrafica agenti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param codiceAgente the primary key of the anagrafica agenti
	 * @return the anagrafica agenti that was removed
	 * @throws it.bysoftware.ct.NoSuchAnagraficaAgentiException if a anagrafica agenti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaAgenti remove(String codiceAgente)
		throws NoSuchAnagraficaAgentiException, SystemException {
		return remove((Serializable)codiceAgente);
	}

	/**
	 * Removes the anagrafica agenti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the anagrafica agenti
	 * @return the anagrafica agenti that was removed
	 * @throws it.bysoftware.ct.NoSuchAnagraficaAgentiException if a anagrafica agenti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaAgenti remove(Serializable primaryKey)
		throws NoSuchAnagraficaAgentiException, SystemException {
		Session session = null;

		try {
			session = openSession();

			AnagraficaAgenti anagraficaAgenti = (AnagraficaAgenti)session.get(AnagraficaAgentiImpl.class,
					primaryKey);

			if (anagraficaAgenti == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAnagraficaAgentiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(anagraficaAgenti);
		}
		catch (NoSuchAnagraficaAgentiException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AnagraficaAgenti removeImpl(AnagraficaAgenti anagraficaAgenti)
		throws SystemException {
		anagraficaAgenti = toUnwrappedModel(anagraficaAgenti);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(anagraficaAgenti)) {
				anagraficaAgenti = (AnagraficaAgenti)session.get(AnagraficaAgentiImpl.class,
						anagraficaAgenti.getPrimaryKeyObj());
			}

			if (anagraficaAgenti != null) {
				session.delete(anagraficaAgenti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (anagraficaAgenti != null) {
			clearCache(anagraficaAgenti);
		}

		return anagraficaAgenti;
	}

	@Override
	public AnagraficaAgenti updateImpl(
		it.bysoftware.ct.model.AnagraficaAgenti anagraficaAgenti)
		throws SystemException {
		anagraficaAgenti = toUnwrappedModel(anagraficaAgenti);

		boolean isNew = anagraficaAgenti.isNew();

		Session session = null;

		try {
			session = openSession();

			if (anagraficaAgenti.isNew()) {
				session.save(anagraficaAgenti);

				anagraficaAgenti.setNew(false);
			}
			else {
				session.merge(anagraficaAgenti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(AnagraficaAgentiModelImpl.ENTITY_CACHE_ENABLED,
			AnagraficaAgentiImpl.class, anagraficaAgenti.getPrimaryKey(),
			anagraficaAgenti);

		return anagraficaAgenti;
	}

	protected AnagraficaAgenti toUnwrappedModel(
		AnagraficaAgenti anagraficaAgenti) {
		if (anagraficaAgenti instanceof AnagraficaAgentiImpl) {
			return anagraficaAgenti;
		}

		AnagraficaAgentiImpl anagraficaAgentiImpl = new AnagraficaAgentiImpl();

		anagraficaAgentiImpl.setNew(anagraficaAgenti.isNew());
		anagraficaAgentiImpl.setPrimaryKey(anagraficaAgenti.getPrimaryKey());

		anagraficaAgentiImpl.setCodiceAgente(anagraficaAgenti.getCodiceAgente());
		anagraficaAgentiImpl.setNome(anagraficaAgenti.getNome());
		anagraficaAgentiImpl.setCodiceZona(anagraficaAgenti.getCodiceZona());
		anagraficaAgentiImpl.setCodiceProvvRigo(anagraficaAgenti.getCodiceProvvRigo());
		anagraficaAgentiImpl.setCodiceProvvChiusura(anagraficaAgenti.getCodiceProvvChiusura());
		anagraficaAgentiImpl.setBudget(anagraficaAgenti.getBudget());
		anagraficaAgentiImpl.setTipoLiquidazione(anagraficaAgenti.getTipoLiquidazione());
		anagraficaAgentiImpl.setGiorniToll(anagraficaAgenti.getGiorniToll());
		anagraficaAgentiImpl.setTipoDivisa(anagraficaAgenti.isTipoDivisa());

		return anagraficaAgentiImpl;
	}

	/**
	 * Returns the anagrafica agenti with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the anagrafica agenti
	 * @return the anagrafica agenti
	 * @throws it.bysoftware.ct.NoSuchAnagraficaAgentiException if a anagrafica agenti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaAgenti findByPrimaryKey(Serializable primaryKey)
		throws NoSuchAnagraficaAgentiException, SystemException {
		AnagraficaAgenti anagraficaAgenti = fetchByPrimaryKey(primaryKey);

		if (anagraficaAgenti == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchAnagraficaAgentiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return anagraficaAgenti;
	}

	/**
	 * Returns the anagrafica agenti with the primary key or throws a {@link it.bysoftware.ct.NoSuchAnagraficaAgentiException} if it could not be found.
	 *
	 * @param codiceAgente the primary key of the anagrafica agenti
	 * @return the anagrafica agenti
	 * @throws it.bysoftware.ct.NoSuchAnagraficaAgentiException if a anagrafica agenti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaAgenti findByPrimaryKey(String codiceAgente)
		throws NoSuchAnagraficaAgentiException, SystemException {
		return findByPrimaryKey((Serializable)codiceAgente);
	}

	/**
	 * Returns the anagrafica agenti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the anagrafica agenti
	 * @return the anagrafica agenti, or <code>null</code> if a anagrafica agenti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaAgenti fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		AnagraficaAgenti anagraficaAgenti = (AnagraficaAgenti)EntityCacheUtil.getResult(AnagraficaAgentiModelImpl.ENTITY_CACHE_ENABLED,
				AnagraficaAgentiImpl.class, primaryKey);

		if (anagraficaAgenti == _nullAnagraficaAgenti) {
			return null;
		}

		if (anagraficaAgenti == null) {
			Session session = null;

			try {
				session = openSession();

				anagraficaAgenti = (AnagraficaAgenti)session.get(AnagraficaAgentiImpl.class,
						primaryKey);

				if (anagraficaAgenti != null) {
					cacheResult(anagraficaAgenti);
				}
				else {
					EntityCacheUtil.putResult(AnagraficaAgentiModelImpl.ENTITY_CACHE_ENABLED,
						AnagraficaAgentiImpl.class, primaryKey,
						_nullAnagraficaAgenti);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(AnagraficaAgentiModelImpl.ENTITY_CACHE_ENABLED,
					AnagraficaAgentiImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return anagraficaAgenti;
	}

	/**
	 * Returns the anagrafica agenti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param codiceAgente the primary key of the anagrafica agenti
	 * @return the anagrafica agenti, or <code>null</code> if a anagrafica agenti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaAgenti fetchByPrimaryKey(String codiceAgente)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)codiceAgente);
	}

	/**
	 * Returns all the anagrafica agentis.
	 *
	 * @return the anagrafica agentis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AnagraficaAgenti> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the anagrafica agentis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficaAgentiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of anagrafica agentis
	 * @param end the upper bound of the range of anagrafica agentis (not inclusive)
	 * @return the range of anagrafica agentis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AnagraficaAgenti> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the anagrafica agentis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficaAgentiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of anagrafica agentis
	 * @param end the upper bound of the range of anagrafica agentis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of anagrafica agentis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AnagraficaAgenti> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<AnagraficaAgenti> list = (List<AnagraficaAgenti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ANAGRAFICAAGENTI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ANAGRAFICAAGENTI;

				if (pagination) {
					sql = sql.concat(AnagraficaAgentiModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<AnagraficaAgenti>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<AnagraficaAgenti>(list);
				}
				else {
					list = (List<AnagraficaAgenti>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the anagrafica agentis from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (AnagraficaAgenti anagraficaAgenti : findAll()) {
			remove(anagraficaAgenti);
		}
	}

	/**
	 * Returns the number of anagrafica agentis.
	 *
	 * @return the number of anagrafica agentis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ANAGRAFICAAGENTI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the anagrafica agenti persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.AnagraficaAgenti")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<AnagraficaAgenti>> listenersList = new ArrayList<ModelListener<AnagraficaAgenti>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<AnagraficaAgenti>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AnagraficaAgentiImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ANAGRAFICAAGENTI = "SELECT anagraficaAgenti FROM AnagraficaAgenti anagraficaAgenti";
	private static final String _SQL_COUNT_ANAGRAFICAAGENTI = "SELECT COUNT(anagraficaAgenti) FROM AnagraficaAgenti anagraficaAgenti";
	private static final String _ORDER_BY_ENTITY_ALIAS = "anagraficaAgenti.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AnagraficaAgenti exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AnagraficaAgentiPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"codiceAgente", "nome", "codiceZona", "codiceProvvRigo",
				"codiceProvvChiusura", "budget", "tipoLiquidazione",
				"giorniToll", "tipoDivisa"
			});
	private static AnagraficaAgenti _nullAnagraficaAgenti = new AnagraficaAgentiImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<AnagraficaAgenti> toCacheModel() {
				return _nullAnagraficaAgentiCacheModel;
			}
		};

	private static CacheModel<AnagraficaAgenti> _nullAnagraficaAgentiCacheModel = new CacheModel<AnagraficaAgenti>() {
			@Override
			public AnagraficaAgenti toEntityModel() {
				return _nullAnagraficaAgenti;
			}
		};
}