/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchOrdinatoException;
import it.bysoftware.ct.model.Ordinato;
import it.bysoftware.ct.model.impl.OrdinatoImpl;
import it.bysoftware.ct.model.impl.OrdinatoModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the ordinato service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see OrdinatoPersistence
 * @see OrdinatoUtil
 * @generated
 */
public class OrdinatoPersistenceImpl extends BasePersistenceImpl<Ordinato>
	implements OrdinatoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link OrdinatoUtil} to access the ordinato persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = OrdinatoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(OrdinatoModelImpl.ENTITY_CACHE_ENABLED,
			OrdinatoModelImpl.FINDER_CACHE_ENABLED, OrdinatoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(OrdinatoModelImpl.ENTITY_CACHE_ENABLED,
			OrdinatoModelImpl.FINDER_CACHE_ENABLED, OrdinatoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(OrdinatoModelImpl.ENTITY_CACHE_ENABLED,
			OrdinatoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_ARTICOLOVARIANTE = new FinderPath(OrdinatoModelImpl.ENTITY_CACHE_ENABLED,
			OrdinatoModelImpl.FINDER_CACHE_ENABLED, OrdinatoImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByArticoloVariante",
			new String[] { String.class.getName(), String.class.getName() },
			OrdinatoModelImpl.CODICEARTICOLO_COLUMN_BITMASK |
			OrdinatoModelImpl.CODICEVARIANTE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ARTICOLOVARIANTE = new FinderPath(OrdinatoModelImpl.ENTITY_CACHE_ENABLED,
			OrdinatoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByArticoloVariante",
			new String[] { String.class.getName(), String.class.getName() });

	/**
	 * Returns the ordinato where codiceArticolo = &#63; and codiceVariante = &#63; or throws a {@link it.bysoftware.ct.NoSuchOrdinatoException} if it could not be found.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @return the matching ordinato
	 * @throws it.bysoftware.ct.NoSuchOrdinatoException if a matching ordinato could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ordinato findByArticoloVariante(String codiceArticolo,
		String codiceVariante) throws NoSuchOrdinatoException, SystemException {
		Ordinato ordinato = fetchByArticoloVariante(codiceArticolo,
				codiceVariante);

		if (ordinato == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("codiceArticolo=");
			msg.append(codiceArticolo);

			msg.append(", codiceVariante=");
			msg.append(codiceVariante);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchOrdinatoException(msg.toString());
		}

		return ordinato;
	}

	/**
	 * Returns the ordinato where codiceArticolo = &#63; and codiceVariante = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @return the matching ordinato, or <code>null</code> if a matching ordinato could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ordinato fetchByArticoloVariante(String codiceArticolo,
		String codiceVariante) throws SystemException {
		return fetchByArticoloVariante(codiceArticolo, codiceVariante, true);
	}

	/**
	 * Returns the ordinato where codiceArticolo = &#63; and codiceVariante = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching ordinato, or <code>null</code> if a matching ordinato could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ordinato fetchByArticoloVariante(String codiceArticolo,
		String codiceVariante, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { codiceArticolo, codiceVariante };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTE,
					finderArgs, this);
		}

		if (result instanceof Ordinato) {
			Ordinato ordinato = (Ordinato)result;

			if (!Validator.equals(codiceArticolo, ordinato.getCodiceArticolo()) ||
					!Validator.equals(codiceVariante,
						ordinato.getCodiceVariante())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_ORDINATO_WHERE);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				List<Ordinato> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTE,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"OrdinatoPersistenceImpl.fetchByArticoloVariante(String, String, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Ordinato ordinato = list.get(0);

					result = ordinato;

					cacheResult(ordinato);

					if ((ordinato.getCodiceArticolo() == null) ||
							!ordinato.getCodiceArticolo().equals(codiceArticolo) ||
							(ordinato.getCodiceVariante() == null) ||
							!ordinato.getCodiceVariante().equals(codiceVariante)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTE,
							finderArgs, ordinato);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTE,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Ordinato)result;
		}
	}

	/**
	 * Removes the ordinato where codiceArticolo = &#63; and codiceVariante = &#63; from the database.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @return the ordinato that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ordinato removeByArticoloVariante(String codiceArticolo,
		String codiceVariante) throws NoSuchOrdinatoException, SystemException {
		Ordinato ordinato = findByArticoloVariante(codiceArticolo,
				codiceVariante);

		return remove(ordinato);
	}

	/**
	 * Returns the number of ordinatos where codiceArticolo = &#63; and codiceVariante = &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @return the number of matching ordinatos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByArticoloVariante(String codiceArticolo,
		String codiceVariante) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ARTICOLOVARIANTE;

		Object[] finderArgs = new Object[] { codiceArticolo, codiceVariante };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ORDINATO_WHERE);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_1 =
		"ordinato.id.codiceArticolo IS NULL AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_2 =
		"ordinato.id.codiceArticolo = ? AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_3 =
		"(ordinato.id.codiceArticolo IS NULL OR ordinato.id.codiceArticolo = '') AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_1 =
		"ordinato.id.codiceVariante IS NULL";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_2 =
		"ordinato.id.codiceVariante = ?";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_3 =
		"(ordinato.id.codiceVariante IS NULL OR ordinato.id.codiceVariante = '')";

	public OrdinatoPersistenceImpl() {
		setModelClass(Ordinato.class);
	}

	/**
	 * Caches the ordinato in the entity cache if it is enabled.
	 *
	 * @param ordinato the ordinato
	 */
	@Override
	public void cacheResult(Ordinato ordinato) {
		EntityCacheUtil.putResult(OrdinatoModelImpl.ENTITY_CACHE_ENABLED,
			OrdinatoImpl.class, ordinato.getPrimaryKey(), ordinato);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTE,
			new Object[] {
				ordinato.getCodiceArticolo(), ordinato.getCodiceVariante()
			}, ordinato);

		ordinato.resetOriginalValues();
	}

	/**
	 * Caches the ordinatos in the entity cache if it is enabled.
	 *
	 * @param ordinatos the ordinatos
	 */
	@Override
	public void cacheResult(List<Ordinato> ordinatos) {
		for (Ordinato ordinato : ordinatos) {
			if (EntityCacheUtil.getResult(
						OrdinatoModelImpl.ENTITY_CACHE_ENABLED,
						OrdinatoImpl.class, ordinato.getPrimaryKey()) == null) {
				cacheResult(ordinato);
			}
			else {
				ordinato.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all ordinatos.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(OrdinatoImpl.class.getName());
		}

		EntityCacheUtil.clearCache(OrdinatoImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the ordinato.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Ordinato ordinato) {
		EntityCacheUtil.removeResult(OrdinatoModelImpl.ENTITY_CACHE_ENABLED,
			OrdinatoImpl.class, ordinato.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(ordinato);
	}

	@Override
	public void clearCache(List<Ordinato> ordinatos) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Ordinato ordinato : ordinatos) {
			EntityCacheUtil.removeResult(OrdinatoModelImpl.ENTITY_CACHE_ENABLED,
				OrdinatoImpl.class, ordinato.getPrimaryKey());

			clearUniqueFindersCache(ordinato);
		}
	}

	protected void cacheUniqueFindersCache(Ordinato ordinato) {
		if (ordinato.isNew()) {
			Object[] args = new Object[] {
					ordinato.getCodiceArticolo(), ordinato.getCodiceVariante()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ARTICOLOVARIANTE,
				args, Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTE,
				args, ordinato);
		}
		else {
			OrdinatoModelImpl ordinatoModelImpl = (OrdinatoModelImpl)ordinato;

			if ((ordinatoModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_ARTICOLOVARIANTE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ordinato.getCodiceArticolo(),
						ordinato.getCodiceVariante()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ARTICOLOVARIANTE,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTE,
					args, ordinato);
			}
		}
	}

	protected void clearUniqueFindersCache(Ordinato ordinato) {
		OrdinatoModelImpl ordinatoModelImpl = (OrdinatoModelImpl)ordinato;

		Object[] args = new Object[] {
				ordinato.getCodiceArticolo(), ordinato.getCodiceVariante()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTICOLOVARIANTE, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTE, args);

		if ((ordinatoModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_ARTICOLOVARIANTE.getColumnBitmask()) != 0) {
			args = new Object[] {
					ordinatoModelImpl.getOriginalCodiceArticolo(),
					ordinatoModelImpl.getOriginalCodiceVariante()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTICOLOVARIANTE,
				args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ARTICOLOVARIANTE,
				args);
		}
	}

	/**
	 * Creates a new ordinato with the primary key. Does not add the ordinato to the database.
	 *
	 * @param ordinatoPK the primary key for the new ordinato
	 * @return the new ordinato
	 */
	@Override
	public Ordinato create(OrdinatoPK ordinatoPK) {
		Ordinato ordinato = new OrdinatoImpl();

		ordinato.setNew(true);
		ordinato.setPrimaryKey(ordinatoPK);

		return ordinato;
	}

	/**
	 * Removes the ordinato with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ordinatoPK the primary key of the ordinato
	 * @return the ordinato that was removed
	 * @throws it.bysoftware.ct.NoSuchOrdinatoException if a ordinato with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ordinato remove(OrdinatoPK ordinatoPK)
		throws NoSuchOrdinatoException, SystemException {
		return remove((Serializable)ordinatoPK);
	}

	/**
	 * Removes the ordinato with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the ordinato
	 * @return the ordinato that was removed
	 * @throws it.bysoftware.ct.NoSuchOrdinatoException if a ordinato with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ordinato remove(Serializable primaryKey)
		throws NoSuchOrdinatoException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Ordinato ordinato = (Ordinato)session.get(OrdinatoImpl.class,
					primaryKey);

			if (ordinato == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchOrdinatoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ordinato);
		}
		catch (NoSuchOrdinatoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Ordinato removeImpl(Ordinato ordinato) throws SystemException {
		ordinato = toUnwrappedModel(ordinato);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ordinato)) {
				ordinato = (Ordinato)session.get(OrdinatoImpl.class,
						ordinato.getPrimaryKeyObj());
			}

			if (ordinato != null) {
				session.delete(ordinato);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ordinato != null) {
			clearCache(ordinato);
		}

		return ordinato;
	}

	@Override
	public Ordinato updateImpl(it.bysoftware.ct.model.Ordinato ordinato)
		throws SystemException {
		ordinato = toUnwrappedModel(ordinato);

		boolean isNew = ordinato.isNew();

		Session session = null;

		try {
			session = openSession();

			if (ordinato.isNew()) {
				session.save(ordinato);

				ordinato.setNew(false);
			}
			else {
				session.merge(ordinato);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !OrdinatoModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(OrdinatoModelImpl.ENTITY_CACHE_ENABLED,
			OrdinatoImpl.class, ordinato.getPrimaryKey(), ordinato);

		clearUniqueFindersCache(ordinato);
		cacheUniqueFindersCache(ordinato);

		return ordinato;
	}

	protected Ordinato toUnwrappedModel(Ordinato ordinato) {
		if (ordinato instanceof OrdinatoImpl) {
			return ordinato;
		}

		OrdinatoImpl ordinatoImpl = new OrdinatoImpl();

		ordinatoImpl.setNew(ordinato.isNew());
		ordinatoImpl.setPrimaryKey(ordinato.getPrimaryKey());

		ordinatoImpl.setCodiceDesposito(ordinato.getCodiceDesposito());
		ordinatoImpl.setCodiceArticolo(ordinato.getCodiceArticolo());
		ordinatoImpl.setCodiceVariante(ordinato.getCodiceVariante());
		ordinatoImpl.setQuantitaOrdinataFornitore(ordinato.getQuantitaOrdinataFornitore());
		ordinatoImpl.setQuantitaOrdinataProduzione(ordinato.getQuantitaOrdinataProduzione());
		ordinatoImpl.setQuantitaOrdinataAltri(ordinato.getQuantitaOrdinataAltri());
		ordinatoImpl.setQuantitaImpegnataClienti(ordinato.getQuantitaImpegnataClienti());
		ordinatoImpl.setQuantitaImpegnataProduzione(ordinato.getQuantitaImpegnataProduzione());
		ordinatoImpl.setQuantitaImpegnataAltri(ordinato.getQuantitaImpegnataAltri());
		ordinatoImpl.setQuantitaOrdinataFornitoreUnMis2(ordinato.getQuantitaOrdinataFornitoreUnMis2());
		ordinatoImpl.setQuantitaOrdinataProduzioneUnMis2(ordinato.getQuantitaOrdinataProduzioneUnMis2());
		ordinatoImpl.setQuantitaOrdinataAltriUnMis2(ordinato.getQuantitaOrdinataAltriUnMis2());
		ordinatoImpl.setQuantitaImpegnataClientiUnMis2(ordinato.getQuantitaImpegnataClientiUnMis2());
		ordinatoImpl.setQuantitaImpegnataProduzioneUnMis2(ordinato.getQuantitaImpegnataProduzioneUnMis2());
		ordinatoImpl.setQuantitaImpegnataAltriUnMis2(ordinato.getQuantitaImpegnataAltriUnMis2());

		return ordinatoImpl;
	}

	/**
	 * Returns the ordinato with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the ordinato
	 * @return the ordinato
	 * @throws it.bysoftware.ct.NoSuchOrdinatoException if a ordinato with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ordinato findByPrimaryKey(Serializable primaryKey)
		throws NoSuchOrdinatoException, SystemException {
		Ordinato ordinato = fetchByPrimaryKey(primaryKey);

		if (ordinato == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchOrdinatoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ordinato;
	}

	/**
	 * Returns the ordinato with the primary key or throws a {@link it.bysoftware.ct.NoSuchOrdinatoException} if it could not be found.
	 *
	 * @param ordinatoPK the primary key of the ordinato
	 * @return the ordinato
	 * @throws it.bysoftware.ct.NoSuchOrdinatoException if a ordinato with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ordinato findByPrimaryKey(OrdinatoPK ordinatoPK)
		throws NoSuchOrdinatoException, SystemException {
		return findByPrimaryKey((Serializable)ordinatoPK);
	}

	/**
	 * Returns the ordinato with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the ordinato
	 * @return the ordinato, or <code>null</code> if a ordinato with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ordinato fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Ordinato ordinato = (Ordinato)EntityCacheUtil.getResult(OrdinatoModelImpl.ENTITY_CACHE_ENABLED,
				OrdinatoImpl.class, primaryKey);

		if (ordinato == _nullOrdinato) {
			return null;
		}

		if (ordinato == null) {
			Session session = null;

			try {
				session = openSession();

				ordinato = (Ordinato)session.get(OrdinatoImpl.class, primaryKey);

				if (ordinato != null) {
					cacheResult(ordinato);
				}
				else {
					EntityCacheUtil.putResult(OrdinatoModelImpl.ENTITY_CACHE_ENABLED,
						OrdinatoImpl.class, primaryKey, _nullOrdinato);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(OrdinatoModelImpl.ENTITY_CACHE_ENABLED,
					OrdinatoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ordinato;
	}

	/**
	 * Returns the ordinato with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ordinatoPK the primary key of the ordinato
	 * @return the ordinato, or <code>null</code> if a ordinato with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ordinato fetchByPrimaryKey(OrdinatoPK ordinatoPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)ordinatoPK);
	}

	/**
	 * Returns all the ordinatos.
	 *
	 * @return the ordinatos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Ordinato> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ordinatos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdinatoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ordinatos
	 * @param end the upper bound of the range of ordinatos (not inclusive)
	 * @return the range of ordinatos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Ordinato> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the ordinatos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdinatoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ordinatos
	 * @param end the upper bound of the range of ordinatos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ordinatos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Ordinato> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Ordinato> list = (List<Ordinato>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ORDINATO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ORDINATO;

				if (pagination) {
					sql = sql.concat(OrdinatoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Ordinato>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Ordinato>(list);
				}
				else {
					list = (List<Ordinato>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the ordinatos from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Ordinato ordinato : findAll()) {
			remove(ordinato);
		}
	}

	/**
	 * Returns the number of ordinatos.
	 *
	 * @return the number of ordinatos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ORDINATO);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the ordinato persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.Ordinato")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Ordinato>> listenersList = new ArrayList<ModelListener<Ordinato>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Ordinato>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(OrdinatoImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ORDINATO = "SELECT ordinato FROM Ordinato ordinato";
	private static final String _SQL_SELECT_ORDINATO_WHERE = "SELECT ordinato FROM Ordinato ordinato WHERE ";
	private static final String _SQL_COUNT_ORDINATO = "SELECT COUNT(ordinato) FROM Ordinato ordinato";
	private static final String _SQL_COUNT_ORDINATO_WHERE = "SELECT COUNT(ordinato) FROM Ordinato ordinato WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ordinato.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Ordinato exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Ordinato exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(OrdinatoPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"codiceDesposito", "codiceArticolo", "codiceVariante",
				"quantitaOrdinataFornitore", "quantitaOrdinataProduzione",
				"quantitaOrdinataAltri", "quantitaImpegnataClienti",
				"quantitaImpegnataProduzione", "quantitaImpegnataAltri",
				"quantitaOrdinataFornitoreUnMis2",
				"quantitaOrdinataProduzioneUnMis2",
				"quantitaOrdinataAltriUnMis2", "quantitaImpegnataClientiUnMis2",
				"quantitaImpegnataProduzioneUnMis2",
				"quantitaImpegnataAltriUnMis2"
			});
	private static Ordinato _nullOrdinato = new OrdinatoImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Ordinato> toCacheModel() {
				return _nullOrdinatoCacheModel;
			}
		};

	private static CacheModel<Ordinato> _nullOrdinatoCacheModel = new CacheModel<Ordinato>() {
			@Override
			public Ordinato toEntityModel() {
				return _nullOrdinato;
			}
		};
}