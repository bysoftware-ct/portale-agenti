/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchDatiClientiFornitoriException;
import it.bysoftware.ct.model.DatiClientiFornitori;
import it.bysoftware.ct.model.impl.DatiClientiFornitoriImpl;
import it.bysoftware.ct.model.impl.DatiClientiFornitoriModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the dati clienti fornitori service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see DatiClientiFornitoriPersistence
 * @see DatiClientiFornitoriUtil
 * @generated
 */
public class DatiClientiFornitoriPersistenceImpl extends BasePersistenceImpl<DatiClientiFornitori>
	implements DatiClientiFornitoriPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link DatiClientiFornitoriUtil} to access the dati clienti fornitori persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = DatiClientiFornitoriImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
			DatiClientiFornitoriImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
			DatiClientiFornitoriImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiClientiFornitoriModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICEAGENTE =
		new FinderPath(DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
			DatiClientiFornitoriImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCodiceAgente",
			new String[] {
				String.class.getName(), Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEAGENTE =
		new FinderPath(DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
			DatiClientiFornitoriImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCodiceAgente",
			new String[] { String.class.getName(), Boolean.class.getName() },
			DatiClientiFornitoriModelImpl.CODICEAGENTE_COLUMN_BITMASK |
			DatiClientiFornitoriModelImpl.TIPOSOGGETTO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CODICEAGENTE = new FinderPath(DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiClientiFornitoriModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCodiceAgente",
			new String[] { String.class.getName(), Boolean.class.getName() });

	/**
	 * Returns all the dati clienti fornitoris where codiceAgente = &#63; and tipoSoggetto = &#63;.
	 *
	 * @param codiceAgente the codice agente
	 * @param tipoSoggetto the tipo soggetto
	 * @return the matching dati clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DatiClientiFornitori> findByCodiceAgente(String codiceAgente,
		boolean tipoSoggetto) throws SystemException {
		return findByCodiceAgente(codiceAgente, tipoSoggetto,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the dati clienti fornitoris where codiceAgente = &#63; and tipoSoggetto = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceAgente the codice agente
	 * @param tipoSoggetto the tipo soggetto
	 * @param start the lower bound of the range of dati clienti fornitoris
	 * @param end the upper bound of the range of dati clienti fornitoris (not inclusive)
	 * @return the range of matching dati clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DatiClientiFornitori> findByCodiceAgente(String codiceAgente,
		boolean tipoSoggetto, int start, int end) throws SystemException {
		return findByCodiceAgente(codiceAgente, tipoSoggetto, start, end, null);
	}

	/**
	 * Returns an ordered range of all the dati clienti fornitoris where codiceAgente = &#63; and tipoSoggetto = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceAgente the codice agente
	 * @param tipoSoggetto the tipo soggetto
	 * @param start the lower bound of the range of dati clienti fornitoris
	 * @param end the upper bound of the range of dati clienti fornitoris (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching dati clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DatiClientiFornitori> findByCodiceAgente(String codiceAgente,
		boolean tipoSoggetto, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEAGENTE;
			finderArgs = new Object[] { codiceAgente, tipoSoggetto };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICEAGENTE;
			finderArgs = new Object[] {
					codiceAgente, tipoSoggetto,
					
					start, end, orderByComparator
				};
		}

		List<DatiClientiFornitori> list = (List<DatiClientiFornitori>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (DatiClientiFornitori datiClientiFornitori : list) {
				if (!Validator.equals(codiceAgente,
							datiClientiFornitori.getCodiceAgente()) ||
						(tipoSoggetto != datiClientiFornitori.getTipoSoggetto())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_DATICLIENTIFORNITORI_WHERE);

			boolean bindCodiceAgente = false;

			if (codiceAgente == null) {
				query.append(_FINDER_COLUMN_CODICEAGENTE_CODICEAGENTE_1);
			}
			else if (codiceAgente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CODICEAGENTE_CODICEAGENTE_3);
			}
			else {
				bindCodiceAgente = true;

				query.append(_FINDER_COLUMN_CODICEAGENTE_CODICEAGENTE_2);
			}

			query.append(_FINDER_COLUMN_CODICEAGENTE_TIPOSOGGETTO_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DatiClientiFornitoriModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceAgente) {
					qPos.add(codiceAgente);
				}

				qPos.add(tipoSoggetto);

				if (!pagination) {
					list = (List<DatiClientiFornitori>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<DatiClientiFornitori>(list);
				}
				else {
					list = (List<DatiClientiFornitori>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first dati clienti fornitori in the ordered set where codiceAgente = &#63; and tipoSoggetto = &#63;.
	 *
	 * @param codiceAgente the codice agente
	 * @param tipoSoggetto the tipo soggetto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dati clienti fornitori
	 * @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a matching dati clienti fornitori could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiClientiFornitori findByCodiceAgente_First(String codiceAgente,
		boolean tipoSoggetto, OrderByComparator orderByComparator)
		throws NoSuchDatiClientiFornitoriException, SystemException {
		DatiClientiFornitori datiClientiFornitori = fetchByCodiceAgente_First(codiceAgente,
				tipoSoggetto, orderByComparator);

		if (datiClientiFornitori != null) {
			return datiClientiFornitori;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceAgente=");
		msg.append(codiceAgente);

		msg.append(", tipoSoggetto=");
		msg.append(tipoSoggetto);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDatiClientiFornitoriException(msg.toString());
	}

	/**
	 * Returns the first dati clienti fornitori in the ordered set where codiceAgente = &#63; and tipoSoggetto = &#63;.
	 *
	 * @param codiceAgente the codice agente
	 * @param tipoSoggetto the tipo soggetto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dati clienti fornitori, or <code>null</code> if a matching dati clienti fornitori could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiClientiFornitori fetchByCodiceAgente_First(String codiceAgente,
		boolean tipoSoggetto, OrderByComparator orderByComparator)
		throws SystemException {
		List<DatiClientiFornitori> list = findByCodiceAgente(codiceAgente,
				tipoSoggetto, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last dati clienti fornitori in the ordered set where codiceAgente = &#63; and tipoSoggetto = &#63;.
	 *
	 * @param codiceAgente the codice agente
	 * @param tipoSoggetto the tipo soggetto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dati clienti fornitori
	 * @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a matching dati clienti fornitori could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiClientiFornitori findByCodiceAgente_Last(String codiceAgente,
		boolean tipoSoggetto, OrderByComparator orderByComparator)
		throws NoSuchDatiClientiFornitoriException, SystemException {
		DatiClientiFornitori datiClientiFornitori = fetchByCodiceAgente_Last(codiceAgente,
				tipoSoggetto, orderByComparator);

		if (datiClientiFornitori != null) {
			return datiClientiFornitori;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceAgente=");
		msg.append(codiceAgente);

		msg.append(", tipoSoggetto=");
		msg.append(tipoSoggetto);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDatiClientiFornitoriException(msg.toString());
	}

	/**
	 * Returns the last dati clienti fornitori in the ordered set where codiceAgente = &#63; and tipoSoggetto = &#63;.
	 *
	 * @param codiceAgente the codice agente
	 * @param tipoSoggetto the tipo soggetto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dati clienti fornitori, or <code>null</code> if a matching dati clienti fornitori could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiClientiFornitori fetchByCodiceAgente_Last(String codiceAgente,
		boolean tipoSoggetto, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByCodiceAgente(codiceAgente, tipoSoggetto);

		if (count == 0) {
			return null;
		}

		List<DatiClientiFornitori> list = findByCodiceAgente(codiceAgente,
				tipoSoggetto, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the dati clienti fornitoris before and after the current dati clienti fornitori in the ordered set where codiceAgente = &#63; and tipoSoggetto = &#63;.
	 *
	 * @param datiClientiFornitoriPK the primary key of the current dati clienti fornitori
	 * @param codiceAgente the codice agente
	 * @param tipoSoggetto the tipo soggetto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next dati clienti fornitori
	 * @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a dati clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiClientiFornitori[] findByCodiceAgente_PrevAndNext(
		DatiClientiFornitoriPK datiClientiFornitoriPK, String codiceAgente,
		boolean tipoSoggetto, OrderByComparator orderByComparator)
		throws NoSuchDatiClientiFornitoriException, SystemException {
		DatiClientiFornitori datiClientiFornitori = findByPrimaryKey(datiClientiFornitoriPK);

		Session session = null;

		try {
			session = openSession();

			DatiClientiFornitori[] array = new DatiClientiFornitoriImpl[3];

			array[0] = getByCodiceAgente_PrevAndNext(session,
					datiClientiFornitori, codiceAgente, tipoSoggetto,
					orderByComparator, true);

			array[1] = datiClientiFornitori;

			array[2] = getByCodiceAgente_PrevAndNext(session,
					datiClientiFornitori, codiceAgente, tipoSoggetto,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DatiClientiFornitori getByCodiceAgente_PrevAndNext(
		Session session, DatiClientiFornitori datiClientiFornitori,
		String codiceAgente, boolean tipoSoggetto,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DATICLIENTIFORNITORI_WHERE);

		boolean bindCodiceAgente = false;

		if (codiceAgente == null) {
			query.append(_FINDER_COLUMN_CODICEAGENTE_CODICEAGENTE_1);
		}
		else if (codiceAgente.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CODICEAGENTE_CODICEAGENTE_3);
		}
		else {
			bindCodiceAgente = true;

			query.append(_FINDER_COLUMN_CODICEAGENTE_CODICEAGENTE_2);
		}

		query.append(_FINDER_COLUMN_CODICEAGENTE_TIPOSOGGETTO_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DatiClientiFornitoriModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodiceAgente) {
			qPos.add(codiceAgente);
		}

		qPos.add(tipoSoggetto);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(datiClientiFornitori);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DatiClientiFornitori> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the dati clienti fornitoris where codiceAgente = &#63; and tipoSoggetto = &#63; from the database.
	 *
	 * @param codiceAgente the codice agente
	 * @param tipoSoggetto the tipo soggetto
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCodiceAgente(String codiceAgente, boolean tipoSoggetto)
		throws SystemException {
		for (DatiClientiFornitori datiClientiFornitori : findByCodiceAgente(
				codiceAgente, tipoSoggetto, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(datiClientiFornitori);
		}
	}

	/**
	 * Returns the number of dati clienti fornitoris where codiceAgente = &#63; and tipoSoggetto = &#63;.
	 *
	 * @param codiceAgente the codice agente
	 * @param tipoSoggetto the tipo soggetto
	 * @return the number of matching dati clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCodiceAgente(String codiceAgente, boolean tipoSoggetto)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICEAGENTE;

		Object[] finderArgs = new Object[] { codiceAgente, tipoSoggetto };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_DATICLIENTIFORNITORI_WHERE);

			boolean bindCodiceAgente = false;

			if (codiceAgente == null) {
				query.append(_FINDER_COLUMN_CODICEAGENTE_CODICEAGENTE_1);
			}
			else if (codiceAgente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CODICEAGENTE_CODICEAGENTE_3);
			}
			else {
				bindCodiceAgente = true;

				query.append(_FINDER_COLUMN_CODICEAGENTE_CODICEAGENTE_2);
			}

			query.append(_FINDER_COLUMN_CODICEAGENTE_TIPOSOGGETTO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceAgente) {
					qPos.add(codiceAgente);
				}

				qPos.add(tipoSoggetto);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CODICEAGENTE_CODICEAGENTE_1 = "datiClientiFornitori.codiceAgente IS NULL AND ";
	private static final String _FINDER_COLUMN_CODICEAGENTE_CODICEAGENTE_2 = "datiClientiFornitori.codiceAgente = ? AND ";
	private static final String _FINDER_COLUMN_CODICEAGENTE_CODICEAGENTE_3 = "(datiClientiFornitori.codiceAgente IS NULL OR datiClientiFornitori.codiceAgente = '') AND ";
	private static final String _FINDER_COLUMN_CODICEAGENTE_TIPOSOGGETTO_2 = "datiClientiFornitori.id.tipoSoggetto = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_CODICESOGGETTO = new FinderPath(DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
			DatiClientiFornitoriImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByCodiceSoggetto",
			new String[] { String.class.getName(), Boolean.class.getName() },
			DatiClientiFornitoriModelImpl.CODICESOGGETTO_COLUMN_BITMASK |
			DatiClientiFornitoriModelImpl.TIPOSOGGETTO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CODICESOGGETTO = new FinderPath(DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiClientiFornitoriModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCodiceSoggetto",
			new String[] { String.class.getName(), Boolean.class.getName() });

	/**
	 * Returns the dati clienti fornitori where codiceSoggetto = &#63; and tipoSoggetto = &#63; or throws a {@link it.bysoftware.ct.NoSuchDatiClientiFornitoriException} if it could not be found.
	 *
	 * @param codiceSoggetto the codice soggetto
	 * @param tipoSoggetto the tipo soggetto
	 * @return the matching dati clienti fornitori
	 * @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a matching dati clienti fornitori could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiClientiFornitori findByCodiceSoggetto(String codiceSoggetto,
		boolean tipoSoggetto)
		throws NoSuchDatiClientiFornitoriException, SystemException {
		DatiClientiFornitori datiClientiFornitori = fetchByCodiceSoggetto(codiceSoggetto,
				tipoSoggetto);

		if (datiClientiFornitori == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("codiceSoggetto=");
			msg.append(codiceSoggetto);

			msg.append(", tipoSoggetto=");
			msg.append(tipoSoggetto);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchDatiClientiFornitoriException(msg.toString());
		}

		return datiClientiFornitori;
	}

	/**
	 * Returns the dati clienti fornitori where codiceSoggetto = &#63; and tipoSoggetto = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param codiceSoggetto the codice soggetto
	 * @param tipoSoggetto the tipo soggetto
	 * @return the matching dati clienti fornitori, or <code>null</code> if a matching dati clienti fornitori could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiClientiFornitori fetchByCodiceSoggetto(String codiceSoggetto,
		boolean tipoSoggetto) throws SystemException {
		return fetchByCodiceSoggetto(codiceSoggetto, tipoSoggetto, true);
	}

	/**
	 * Returns the dati clienti fornitori where codiceSoggetto = &#63; and tipoSoggetto = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param codiceSoggetto the codice soggetto
	 * @param tipoSoggetto the tipo soggetto
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching dati clienti fornitori, or <code>null</code> if a matching dati clienti fornitori could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiClientiFornitori fetchByCodiceSoggetto(String codiceSoggetto,
		boolean tipoSoggetto, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { codiceSoggetto, tipoSoggetto };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_CODICESOGGETTO,
					finderArgs, this);
		}

		if (result instanceof DatiClientiFornitori) {
			DatiClientiFornitori datiClientiFornitori = (DatiClientiFornitori)result;

			if (!Validator.equals(codiceSoggetto,
						datiClientiFornitori.getCodiceSoggetto()) ||
					(tipoSoggetto != datiClientiFornitori.getTipoSoggetto())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_DATICLIENTIFORNITORI_WHERE);

			boolean bindCodiceSoggetto = false;

			if (codiceSoggetto == null) {
				query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_1);
			}
			else if (codiceSoggetto.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_3);
			}
			else {
				bindCodiceSoggetto = true;

				query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_2);
			}

			query.append(_FINDER_COLUMN_CODICESOGGETTO_TIPOSOGGETTO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceSoggetto) {
					qPos.add(codiceSoggetto);
				}

				qPos.add(tipoSoggetto);

				List<DatiClientiFornitori> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODICESOGGETTO,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"DatiClientiFornitoriPersistenceImpl.fetchByCodiceSoggetto(String, boolean, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					DatiClientiFornitori datiClientiFornitori = list.get(0);

					result = datiClientiFornitori;

					cacheResult(datiClientiFornitori);

					if ((datiClientiFornitori.getCodiceSoggetto() == null) ||
							!datiClientiFornitori.getCodiceSoggetto()
													 .equals(codiceSoggetto) ||
							(datiClientiFornitori.getTipoSoggetto() != tipoSoggetto)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODICESOGGETTO,
							finderArgs, datiClientiFornitori);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CODICESOGGETTO,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (DatiClientiFornitori)result;
		}
	}

	/**
	 * Removes the dati clienti fornitori where codiceSoggetto = &#63; and tipoSoggetto = &#63; from the database.
	 *
	 * @param codiceSoggetto the codice soggetto
	 * @param tipoSoggetto the tipo soggetto
	 * @return the dati clienti fornitori that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiClientiFornitori removeByCodiceSoggetto(String codiceSoggetto,
		boolean tipoSoggetto)
		throws NoSuchDatiClientiFornitoriException, SystemException {
		DatiClientiFornitori datiClientiFornitori = findByCodiceSoggetto(codiceSoggetto,
				tipoSoggetto);

		return remove(datiClientiFornitori);
	}

	/**
	 * Returns the number of dati clienti fornitoris where codiceSoggetto = &#63; and tipoSoggetto = &#63;.
	 *
	 * @param codiceSoggetto the codice soggetto
	 * @param tipoSoggetto the tipo soggetto
	 * @return the number of matching dati clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCodiceSoggetto(String codiceSoggetto, boolean tipoSoggetto)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICESOGGETTO;

		Object[] finderArgs = new Object[] { codiceSoggetto, tipoSoggetto };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_DATICLIENTIFORNITORI_WHERE);

			boolean bindCodiceSoggetto = false;

			if (codiceSoggetto == null) {
				query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_1);
			}
			else if (codiceSoggetto.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_3);
			}
			else {
				bindCodiceSoggetto = true;

				query.append(_FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_2);
			}

			query.append(_FINDER_COLUMN_CODICESOGGETTO_TIPOSOGGETTO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceSoggetto) {
					qPos.add(codiceSoggetto);
				}

				qPos.add(tipoSoggetto);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_1 = "datiClientiFornitori.id.codiceSoggetto IS NULL AND ";
	private static final String _FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_2 = "datiClientiFornitori.id.codiceSoggetto = ? AND ";
	private static final String _FINDER_COLUMN_CODICESOGGETTO_CODICESOGGETTO_3 = "(datiClientiFornitori.id.codiceSoggetto IS NULL OR datiClientiFornitori.id.codiceSoggetto = '') AND ";
	private static final String _FINDER_COLUMN_CODICESOGGETTO_TIPOSOGGETTO_2 = "datiClientiFornitori.id.tipoSoggetto = ?";

	public DatiClientiFornitoriPersistenceImpl() {
		setModelClass(DatiClientiFornitori.class);
	}

	/**
	 * Caches the dati clienti fornitori in the entity cache if it is enabled.
	 *
	 * @param datiClientiFornitori the dati clienti fornitori
	 */
	@Override
	public void cacheResult(DatiClientiFornitori datiClientiFornitori) {
		EntityCacheUtil.putResult(DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiClientiFornitoriImpl.class,
			datiClientiFornitori.getPrimaryKey(), datiClientiFornitori);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODICESOGGETTO,
			new Object[] {
				datiClientiFornitori.getCodiceSoggetto(),
				datiClientiFornitori.getTipoSoggetto()
			}, datiClientiFornitori);

		datiClientiFornitori.resetOriginalValues();
	}

	/**
	 * Caches the dati clienti fornitoris in the entity cache if it is enabled.
	 *
	 * @param datiClientiFornitoris the dati clienti fornitoris
	 */
	@Override
	public void cacheResult(List<DatiClientiFornitori> datiClientiFornitoris) {
		for (DatiClientiFornitori datiClientiFornitori : datiClientiFornitoris) {
			if (EntityCacheUtil.getResult(
						DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
						DatiClientiFornitoriImpl.class,
						datiClientiFornitori.getPrimaryKey()) == null) {
				cacheResult(datiClientiFornitori);
			}
			else {
				datiClientiFornitori.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all dati clienti fornitoris.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(DatiClientiFornitoriImpl.class.getName());
		}

		EntityCacheUtil.clearCache(DatiClientiFornitoriImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the dati clienti fornitori.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(DatiClientiFornitori datiClientiFornitori) {
		EntityCacheUtil.removeResult(DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiClientiFornitoriImpl.class, datiClientiFornitori.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(datiClientiFornitori);
	}

	@Override
	public void clearCache(List<DatiClientiFornitori> datiClientiFornitoris) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (DatiClientiFornitori datiClientiFornitori : datiClientiFornitoris) {
			EntityCacheUtil.removeResult(DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
				DatiClientiFornitoriImpl.class,
				datiClientiFornitori.getPrimaryKey());

			clearUniqueFindersCache(datiClientiFornitori);
		}
	}

	protected void cacheUniqueFindersCache(
		DatiClientiFornitori datiClientiFornitori) {
		if (datiClientiFornitori.isNew()) {
			Object[] args = new Object[] {
					datiClientiFornitori.getCodiceSoggetto(),
					datiClientiFornitori.getTipoSoggetto()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CODICESOGGETTO,
				args, Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODICESOGGETTO,
				args, datiClientiFornitori);
		}
		else {
			DatiClientiFornitoriModelImpl datiClientiFornitoriModelImpl = (DatiClientiFornitoriModelImpl)datiClientiFornitori;

			if ((datiClientiFornitoriModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_CODICESOGGETTO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						datiClientiFornitori.getCodiceSoggetto(),
						datiClientiFornitori.getTipoSoggetto()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CODICESOGGETTO,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODICESOGGETTO,
					args, datiClientiFornitori);
			}
		}
	}

	protected void clearUniqueFindersCache(
		DatiClientiFornitori datiClientiFornitori) {
		DatiClientiFornitoriModelImpl datiClientiFornitoriModelImpl = (DatiClientiFornitoriModelImpl)datiClientiFornitori;

		Object[] args = new Object[] {
				datiClientiFornitori.getCodiceSoggetto(),
				datiClientiFornitori.getTipoSoggetto()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICESOGGETTO, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CODICESOGGETTO, args);

		if ((datiClientiFornitoriModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_CODICESOGGETTO.getColumnBitmask()) != 0) {
			args = new Object[] {
					datiClientiFornitoriModelImpl.getOriginalCodiceSoggetto(),
					datiClientiFornitoriModelImpl.getOriginalTipoSoggetto()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICESOGGETTO,
				args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CODICESOGGETTO,
				args);
		}
	}

	/**
	 * Creates a new dati clienti fornitori with the primary key. Does not add the dati clienti fornitori to the database.
	 *
	 * @param datiClientiFornitoriPK the primary key for the new dati clienti fornitori
	 * @return the new dati clienti fornitori
	 */
	@Override
	public DatiClientiFornitori create(
		DatiClientiFornitoriPK datiClientiFornitoriPK) {
		DatiClientiFornitori datiClientiFornitori = new DatiClientiFornitoriImpl();

		datiClientiFornitori.setNew(true);
		datiClientiFornitori.setPrimaryKey(datiClientiFornitoriPK);

		return datiClientiFornitori;
	}

	/**
	 * Removes the dati clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param datiClientiFornitoriPK the primary key of the dati clienti fornitori
	 * @return the dati clienti fornitori that was removed
	 * @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a dati clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiClientiFornitori remove(
		DatiClientiFornitoriPK datiClientiFornitoriPK)
		throws NoSuchDatiClientiFornitoriException, SystemException {
		return remove((Serializable)datiClientiFornitoriPK);
	}

	/**
	 * Removes the dati clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the dati clienti fornitori
	 * @return the dati clienti fornitori that was removed
	 * @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a dati clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiClientiFornitori remove(Serializable primaryKey)
		throws NoSuchDatiClientiFornitoriException, SystemException {
		Session session = null;

		try {
			session = openSession();

			DatiClientiFornitori datiClientiFornitori = (DatiClientiFornitori)session.get(DatiClientiFornitoriImpl.class,
					primaryKey);

			if (datiClientiFornitori == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDatiClientiFornitoriException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(datiClientiFornitori);
		}
		catch (NoSuchDatiClientiFornitoriException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected DatiClientiFornitori removeImpl(
		DatiClientiFornitori datiClientiFornitori) throws SystemException {
		datiClientiFornitori = toUnwrappedModel(datiClientiFornitori);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(datiClientiFornitori)) {
				datiClientiFornitori = (DatiClientiFornitori)session.get(DatiClientiFornitoriImpl.class,
						datiClientiFornitori.getPrimaryKeyObj());
			}

			if (datiClientiFornitori != null) {
				session.delete(datiClientiFornitori);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (datiClientiFornitori != null) {
			clearCache(datiClientiFornitori);
		}

		return datiClientiFornitori;
	}

	@Override
	public DatiClientiFornitori updateImpl(
		it.bysoftware.ct.model.DatiClientiFornitori datiClientiFornitori)
		throws SystemException {
		datiClientiFornitori = toUnwrappedModel(datiClientiFornitori);

		boolean isNew = datiClientiFornitori.isNew();

		DatiClientiFornitoriModelImpl datiClientiFornitoriModelImpl = (DatiClientiFornitoriModelImpl)datiClientiFornitori;

		Session session = null;

		try {
			session = openSession();

			if (datiClientiFornitori.isNew()) {
				session.save(datiClientiFornitori);

				datiClientiFornitori.setNew(false);
			}
			else {
				session.merge(datiClientiFornitori);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !DatiClientiFornitoriModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((datiClientiFornitoriModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEAGENTE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						datiClientiFornitoriModelImpl.getOriginalCodiceAgente(),
						datiClientiFornitoriModelImpl.getOriginalTipoSoggetto()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICEAGENTE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEAGENTE,
					args);

				args = new Object[] {
						datiClientiFornitoriModelImpl.getCodiceAgente(),
						datiClientiFornitoriModelImpl.getTipoSoggetto()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICEAGENTE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICEAGENTE,
					args);
			}
		}

		EntityCacheUtil.putResult(DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiClientiFornitoriImpl.class,
			datiClientiFornitori.getPrimaryKey(), datiClientiFornitori);

		clearUniqueFindersCache(datiClientiFornitori);
		cacheUniqueFindersCache(datiClientiFornitori);

		return datiClientiFornitori;
	}

	protected DatiClientiFornitori toUnwrappedModel(
		DatiClientiFornitori datiClientiFornitori) {
		if (datiClientiFornitori instanceof DatiClientiFornitoriImpl) {
			return datiClientiFornitori;
		}

		DatiClientiFornitoriImpl datiClientiFornitoriImpl = new DatiClientiFornitoriImpl();

		datiClientiFornitoriImpl.setNew(datiClientiFornitori.isNew());
		datiClientiFornitoriImpl.setPrimaryKey(datiClientiFornitori.getPrimaryKey());

		datiClientiFornitoriImpl.setTipoSoggetto(datiClientiFornitori.isTipoSoggetto());
		datiClientiFornitoriImpl.setCodiceSoggetto(datiClientiFornitori.getCodiceSoggetto());
		datiClientiFornitoriImpl.setCodiceBanca(datiClientiFornitori.getCodiceBanca());
		datiClientiFornitoriImpl.setCodiceAgenzia(datiClientiFornitori.getCodiceAgenzia());
		datiClientiFornitoriImpl.setCodiceIdBanca(datiClientiFornitori.getCodiceIdBanca());
		datiClientiFornitoriImpl.setCodicePaese(datiClientiFornitori.getCodicePaese());
		datiClientiFornitoriImpl.setCodiceCINInt(datiClientiFornitori.getCodiceCINInt());
		datiClientiFornitoriImpl.setCodiceCINNaz(datiClientiFornitori.getCodiceCINNaz());
		datiClientiFornitoriImpl.setNumeroContoCorrente(datiClientiFornitori.getNumeroContoCorrente());
		datiClientiFornitoriImpl.setIBAN(datiClientiFornitori.getIBAN());
		datiClientiFornitoriImpl.setCategoriaEconomica(datiClientiFornitori.getCategoriaEconomica());
		datiClientiFornitoriImpl.setCodiceEsenzioneIVA(datiClientiFornitori.getCodiceEsenzioneIVA());
		datiClientiFornitoriImpl.setCodiceDivisa(datiClientiFornitori.getCodiceDivisa());
		datiClientiFornitoriImpl.setCodicePagamento(datiClientiFornitori.getCodicePagamento());
		datiClientiFornitoriImpl.setCodiceZona(datiClientiFornitori.getCodiceZona());
		datiClientiFornitoriImpl.setCodiceAgente(datiClientiFornitori.getCodiceAgente());
		datiClientiFornitoriImpl.setCodiceGruppoAgente(datiClientiFornitori.getCodiceGruppoAgente());
		datiClientiFornitoriImpl.setCodiceCatProvv(datiClientiFornitori.getCodiceCatProvv());
		datiClientiFornitoriImpl.setCodiceSpedizione(datiClientiFornitori.getCodiceSpedizione());
		datiClientiFornitoriImpl.setCodicePorto(datiClientiFornitori.getCodicePorto());
		datiClientiFornitoriImpl.setCodiceVettore(datiClientiFornitori.getCodiceVettore());
		datiClientiFornitoriImpl.setCodiceDestDiversa(datiClientiFornitori.getCodiceDestDiversa());
		datiClientiFornitoriImpl.setCodiceListinoPrezzi(datiClientiFornitori.getCodiceListinoPrezzi());
		datiClientiFornitoriImpl.setCodiceScontoTotale(datiClientiFornitori.getCodiceScontoTotale());
		datiClientiFornitoriImpl.setCodiceScontoPreIVA(datiClientiFornitori.getCodiceScontoPreIVA());
		datiClientiFornitoriImpl.setScontoCat1(datiClientiFornitori.getScontoCat1());
		datiClientiFornitoriImpl.setScontoCat2(datiClientiFornitori.getScontoCat2());
		datiClientiFornitoriImpl.setCodiceLingua(datiClientiFornitori.getCodiceLingua());
		datiClientiFornitoriImpl.setCodiceLinguaEC(datiClientiFornitori.getCodiceLinguaEC());
		datiClientiFornitoriImpl.setAddebitoBolli(datiClientiFornitori.isAddebitoBolli());
		datiClientiFornitoriImpl.setAddebitoSpeseBanca(datiClientiFornitori.isAddebitoSpeseBanca());
		datiClientiFornitoriImpl.setRagruppaBolle(datiClientiFornitori.isRagruppaBolle());
		datiClientiFornitoriImpl.setSospensioneIVA(datiClientiFornitori.isSospensioneIVA());
		datiClientiFornitoriImpl.setRagruppaOrdini(datiClientiFornitori.getRagruppaOrdini());
		datiClientiFornitoriImpl.setRagruppaXDestinazione(datiClientiFornitori.isRagruppaXDestinazione());
		datiClientiFornitoriImpl.setRagruppaXPorto(datiClientiFornitori.isRagruppaXPorto());
		datiClientiFornitoriImpl.setPercSpeseTrasporto(datiClientiFornitori.getPercSpeseTrasporto());
		datiClientiFornitoriImpl.setPrezzoDaProporre(datiClientiFornitori.getPrezzoDaProporre());
		datiClientiFornitoriImpl.setSogettoFatturazione(datiClientiFornitori.getSogettoFatturazione());
		datiClientiFornitoriImpl.setCodiceRaggEffetti(datiClientiFornitori.getCodiceRaggEffetti());
		datiClientiFornitoriImpl.setRiportoRiferimenti(datiClientiFornitori.getRiportoRiferimenti());
		datiClientiFornitoriImpl.setStampaPrezzo(datiClientiFornitori.isStampaPrezzo());
		datiClientiFornitoriImpl.setBloccato(datiClientiFornitori.getBloccato());
		datiClientiFornitoriImpl.setMotivazione(datiClientiFornitori.getMotivazione());
		datiClientiFornitoriImpl.setLivelloBlocco(datiClientiFornitori.getLivelloBlocco());
		datiClientiFornitoriImpl.setAddebitoCONAI(datiClientiFornitori.isAddebitoCONAI());
		datiClientiFornitoriImpl.setLibStr1(datiClientiFornitori.getLibStr1());
		datiClientiFornitoriImpl.setLibStr2(datiClientiFornitori.getLibStr2());
		datiClientiFornitoriImpl.setLibStr3(datiClientiFornitori.getLibStr3());
		datiClientiFornitoriImpl.setLibStr4(datiClientiFornitori.getLibStr4());
		datiClientiFornitoriImpl.setLibStr5(datiClientiFornitori.getLibStr5());
		datiClientiFornitoriImpl.setLibDat1(datiClientiFornitori.getLibDat1());
		datiClientiFornitoriImpl.setLibDat2(datiClientiFornitori.getLibDat2());
		datiClientiFornitoriImpl.setLibDat3(datiClientiFornitori.getLibDat3());
		datiClientiFornitoriImpl.setLibDat4(datiClientiFornitori.getLibDat4());
		datiClientiFornitoriImpl.setLibDat5(datiClientiFornitori.getLibDat5());
		datiClientiFornitoriImpl.setLibLng1(datiClientiFornitori.getLibLng1());
		datiClientiFornitoriImpl.setLibLng2(datiClientiFornitori.getLibLng2());
		datiClientiFornitoriImpl.setLibLng3(datiClientiFornitori.getLibLng3());
		datiClientiFornitoriImpl.setLibLng4(datiClientiFornitori.getLibLng4());
		datiClientiFornitoriImpl.setLibLng5(datiClientiFornitori.getLibLng5());
		datiClientiFornitoriImpl.setLibDbl1(datiClientiFornitori.getLibDbl1());
		datiClientiFornitoriImpl.setLibDbl2(datiClientiFornitori.getLibDbl2());
		datiClientiFornitoriImpl.setLibDbl3(datiClientiFornitori.getLibDbl3());
		datiClientiFornitoriImpl.setLibDbl4(datiClientiFornitori.getLibDbl4());
		datiClientiFornitoriImpl.setLibDbl5(datiClientiFornitori.getLibDbl5());

		return datiClientiFornitoriImpl;
	}

	/**
	 * Returns the dati clienti fornitori with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the dati clienti fornitori
	 * @return the dati clienti fornitori
	 * @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a dati clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiClientiFornitori findByPrimaryKey(Serializable primaryKey)
		throws NoSuchDatiClientiFornitoriException, SystemException {
		DatiClientiFornitori datiClientiFornitori = fetchByPrimaryKey(primaryKey);

		if (datiClientiFornitori == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchDatiClientiFornitoriException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return datiClientiFornitori;
	}

	/**
	 * Returns the dati clienti fornitori with the primary key or throws a {@link it.bysoftware.ct.NoSuchDatiClientiFornitoriException} if it could not be found.
	 *
	 * @param datiClientiFornitoriPK the primary key of the dati clienti fornitori
	 * @return the dati clienti fornitori
	 * @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a dati clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiClientiFornitori findByPrimaryKey(
		DatiClientiFornitoriPK datiClientiFornitoriPK)
		throws NoSuchDatiClientiFornitoriException, SystemException {
		return findByPrimaryKey((Serializable)datiClientiFornitoriPK);
	}

	/**
	 * Returns the dati clienti fornitori with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the dati clienti fornitori
	 * @return the dati clienti fornitori, or <code>null</code> if a dati clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiClientiFornitori fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		DatiClientiFornitori datiClientiFornitori = (DatiClientiFornitori)EntityCacheUtil.getResult(DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
				DatiClientiFornitoriImpl.class, primaryKey);

		if (datiClientiFornitori == _nullDatiClientiFornitori) {
			return null;
		}

		if (datiClientiFornitori == null) {
			Session session = null;

			try {
				session = openSession();

				datiClientiFornitori = (DatiClientiFornitori)session.get(DatiClientiFornitoriImpl.class,
						primaryKey);

				if (datiClientiFornitori != null) {
					cacheResult(datiClientiFornitori);
				}
				else {
					EntityCacheUtil.putResult(DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
						DatiClientiFornitoriImpl.class, primaryKey,
						_nullDatiClientiFornitori);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(DatiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
					DatiClientiFornitoriImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return datiClientiFornitori;
	}

	/**
	 * Returns the dati clienti fornitori with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param datiClientiFornitoriPK the primary key of the dati clienti fornitori
	 * @return the dati clienti fornitori, or <code>null</code> if a dati clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiClientiFornitori fetchByPrimaryKey(
		DatiClientiFornitoriPK datiClientiFornitoriPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)datiClientiFornitoriPK);
	}

	/**
	 * Returns all the dati clienti fornitoris.
	 *
	 * @return the dati clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DatiClientiFornitori> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the dati clienti fornitoris.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of dati clienti fornitoris
	 * @param end the upper bound of the range of dati clienti fornitoris (not inclusive)
	 * @return the range of dati clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DatiClientiFornitori> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the dati clienti fornitoris.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of dati clienti fornitoris
	 * @param end the upper bound of the range of dati clienti fornitoris (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of dati clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DatiClientiFornitori> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<DatiClientiFornitori> list = (List<DatiClientiFornitori>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_DATICLIENTIFORNITORI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_DATICLIENTIFORNITORI;

				if (pagination) {
					sql = sql.concat(DatiClientiFornitoriModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<DatiClientiFornitori>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<DatiClientiFornitori>(list);
				}
				else {
					list = (List<DatiClientiFornitori>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the dati clienti fornitoris from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (DatiClientiFornitori datiClientiFornitori : findAll()) {
			remove(datiClientiFornitori);
		}
	}

	/**
	 * Returns the number of dati clienti fornitoris.
	 *
	 * @return the number of dati clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_DATICLIENTIFORNITORI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the dati clienti fornitori persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.DatiClientiFornitori")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<DatiClientiFornitori>> listenersList = new ArrayList<ModelListener<DatiClientiFornitori>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<DatiClientiFornitori>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(DatiClientiFornitoriImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_DATICLIENTIFORNITORI = "SELECT datiClientiFornitori FROM DatiClientiFornitori datiClientiFornitori";
	private static final String _SQL_SELECT_DATICLIENTIFORNITORI_WHERE = "SELECT datiClientiFornitori FROM DatiClientiFornitori datiClientiFornitori WHERE ";
	private static final String _SQL_COUNT_DATICLIENTIFORNITORI = "SELECT COUNT(datiClientiFornitori) FROM DatiClientiFornitori datiClientiFornitori";
	private static final String _SQL_COUNT_DATICLIENTIFORNITORI_WHERE = "SELECT COUNT(datiClientiFornitori) FROM DatiClientiFornitori datiClientiFornitori WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "datiClientiFornitori.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No DatiClientiFornitori exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No DatiClientiFornitori exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(DatiClientiFornitoriPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"tipoSoggetto", "codiceSoggetto", "codiceBanca", "codiceAgenzia",
				"codiceIdBanca", "codicePaese", "codiceCINInt", "codiceCINNaz",
				"numeroContoCorrente", "IBAN", "categoriaEconomica",
				"codiceEsenzioneIVA", "codiceDivisa", "codicePagamento",
				"codiceZona", "codiceAgente", "codiceGruppoAgente",
				"codiceCatProvv", "codiceSpedizione", "codicePorto",
				"codiceVettore", "codiceDestDiversa", "codiceListinoPrezzi",
				"codiceScontoTotale", "codiceScontoPreIVA", "scontoCat1",
				"scontoCat2", "codiceLingua", "codiceLinguaEC", "addebitoBolli",
				"addebitoSpeseBanca", "ragruppaBolle", "sospensioneIVA",
				"ragruppaOrdini", "ragruppaXDestinazione", "ragruppaXPorto",
				"percSpeseTrasporto", "prezzoDaProporre", "sogettoFatturazione",
				"codiceRaggEffetti", "riportoRiferimenti", "stampaPrezzo",
				"bloccato", "motivazione", "livelloBlocco", "addebitoCONAI",
				"libStr1", "libStr2", "libStr3", "libStr4", "libStr5", "libDat1",
				"libDat2", "libDat3", "libDat4", "libDat5", "libLng1", "libLng2",
				"libLng3", "libLng4", "libLng5", "libDbl1", "libDbl2", "libDbl3",
				"libDbl4", "libDbl5"
			});
	private static DatiClientiFornitori _nullDatiClientiFornitori = new DatiClientiFornitoriImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<DatiClientiFornitori> toCacheModel() {
				return _nullDatiClientiFornitoriCacheModel;
			}
		};

	private static CacheModel<DatiClientiFornitori> _nullDatiClientiFornitoriCacheModel =
		new CacheModel<DatiClientiFornitori>() {
			@Override
			public DatiClientiFornitori toEntityModel() {
				return _nullDatiClientiFornitori;
			}
		};
}