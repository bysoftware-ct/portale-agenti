/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException;
import it.bysoftware.ct.model.ListaMovimentiMagazzino;
import it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoImpl;
import it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the lista movimenti magazzino service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ListaMovimentiMagazzinoPersistence
 * @see ListaMovimentiMagazzinoUtil
 * @generated
 */
public class ListaMovimentiMagazzinoPersistenceImpl extends BasePersistenceImpl<ListaMovimentiMagazzino>
	implements ListaMovimentiMagazzinoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ListaMovimentiMagazzinoUtil} to access the lista movimenti magazzino persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ListaMovimentiMagazzinoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			ListaMovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED,
			ListaMovimentiMagazzinoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			ListaMovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED,
			ListaMovimentiMagazzinoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			ListaMovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTICOLOVARIANTE =
		new FinderPath(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			ListaMovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED,
			ListaMovimentiMagazzinoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByArticoloVariante",
			new String[] {
				String.class.getName(), String.class.getName(),
				Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTICOLOVARIANTE =
		new FinderPath(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			ListaMovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED,
			ListaMovimentiMagazzinoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByArticoloVariante",
			new String[] {
				String.class.getName(), String.class.getName(),
				Integer.class.getName()
			},
			ListaMovimentiMagazzinoModelImpl.CODICEARTICOLO_COLUMN_BITMASK |
			ListaMovimentiMagazzinoModelImpl.CODICEVARIANTE_COLUMN_BITMASK |
			ListaMovimentiMagazzinoModelImpl.SOLOVALORE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ARTICOLOVARIANTE = new FinderPath(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			ListaMovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByArticoloVariante",
			new String[] {
				String.class.getName(), String.class.getName(),
				Integer.class.getName()
			});

	/**
	 * Returns all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @return the matching lista movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListaMovimentiMagazzino> findByArticoloVariante(
		String codiceArticolo, String codiceVariante, int soloValore)
		throws SystemException {
		return findByArticoloVariante(codiceArticolo, codiceVariante,
			soloValore, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @param start the lower bound of the range of lista movimenti magazzinos
	 * @param end the upper bound of the range of lista movimenti magazzinos (not inclusive)
	 * @return the range of matching lista movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListaMovimentiMagazzino> findByArticoloVariante(
		String codiceArticolo, String codiceVariante, int soloValore,
		int start, int end) throws SystemException {
		return findByArticoloVariante(codiceArticolo, codiceVariante,
			soloValore, start, end, null);
	}

	/**
	 * Returns an ordered range of all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @param start the lower bound of the range of lista movimenti magazzinos
	 * @param end the upper bound of the range of lista movimenti magazzinos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching lista movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListaMovimentiMagazzino> findByArticoloVariante(
		String codiceArticolo, String codiceVariante, int soloValore,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTICOLOVARIANTE;
			finderArgs = new Object[] { codiceArticolo, codiceVariante, soloValore };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTICOLOVARIANTE;
			finderArgs = new Object[] {
					codiceArticolo, codiceVariante, soloValore,
					
					start, end, orderByComparator
				};
		}

		List<ListaMovimentiMagazzino> list = (List<ListaMovimentiMagazzino>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ListaMovimentiMagazzino listaMovimentiMagazzino : list) {
				if (!Validator.equals(codiceArticolo,
							listaMovimentiMagazzino.getCodiceArticolo()) ||
						!Validator.equals(codiceVariante,
							listaMovimentiMagazzino.getCodiceVariante()) ||
						(soloValore != listaMovimentiMagazzino.getSoloValore())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_LISTAMOVIMENTIMAGAZZINO_WHERE);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_2);
			}

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_SOLOVALORE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ListaMovimentiMagazzinoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				qPos.add(soloValore);

				if (!pagination) {
					list = (List<ListaMovimentiMagazzino>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ListaMovimentiMagazzino>(list);
				}
				else {
					list = (List<ListaMovimentiMagazzino>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lista movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a matching lista movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino findByArticoloVariante_First(
		String codiceArticolo, String codiceVariante, int soloValore,
		OrderByComparator orderByComparator)
		throws NoSuchListaMovimentiMagazzinoException, SystemException {
		ListaMovimentiMagazzino listaMovimentiMagazzino = fetchByArticoloVariante_First(codiceArticolo,
				codiceVariante, soloValore, orderByComparator);

		if (listaMovimentiMagazzino != null) {
			return listaMovimentiMagazzino;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(", soloValore=");
		msg.append(soloValore);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchListaMovimentiMagazzinoException(msg.toString());
	}

	/**
	 * Returns the first lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lista movimenti magazzino, or <code>null</code> if a matching lista movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino fetchByArticoloVariante_First(
		String codiceArticolo, String codiceVariante, int soloValore,
		OrderByComparator orderByComparator) throws SystemException {
		List<ListaMovimentiMagazzino> list = findByArticoloVariante(codiceArticolo,
				codiceVariante, soloValore, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lista movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a matching lista movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino findByArticoloVariante_Last(
		String codiceArticolo, String codiceVariante, int soloValore,
		OrderByComparator orderByComparator)
		throws NoSuchListaMovimentiMagazzinoException, SystemException {
		ListaMovimentiMagazzino listaMovimentiMagazzino = fetchByArticoloVariante_Last(codiceArticolo,
				codiceVariante, soloValore, orderByComparator);

		if (listaMovimentiMagazzino != null) {
			return listaMovimentiMagazzino;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(", soloValore=");
		msg.append(soloValore);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchListaMovimentiMagazzinoException(msg.toString());
	}

	/**
	 * Returns the last lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lista movimenti magazzino, or <code>null</code> if a matching lista movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino fetchByArticoloVariante_Last(
		String codiceArticolo, String codiceVariante, int soloValore,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByArticoloVariante(codiceArticolo, codiceVariante,
				soloValore);

		if (count == 0) {
			return null;
		}

		List<ListaMovimentiMagazzino> list = findByArticoloVariante(codiceArticolo,
				codiceVariante, soloValore, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the lista movimenti magazzinos before and after the current lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	 *
	 * @param ID the primary key of the current lista movimenti magazzino
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next lista movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a lista movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino[] findByArticoloVariante_PrevAndNext(
		String ID, String codiceArticolo, String codiceVariante,
		int soloValore, OrderByComparator orderByComparator)
		throws NoSuchListaMovimentiMagazzinoException, SystemException {
		ListaMovimentiMagazzino listaMovimentiMagazzino = findByPrimaryKey(ID);

		Session session = null;

		try {
			session = openSession();

			ListaMovimentiMagazzino[] array = new ListaMovimentiMagazzinoImpl[3];

			array[0] = getByArticoloVariante_PrevAndNext(session,
					listaMovimentiMagazzino, codiceArticolo, codiceVariante,
					soloValore, orderByComparator, true);

			array[1] = listaMovimentiMagazzino;

			array[2] = getByArticoloVariante_PrevAndNext(session,
					listaMovimentiMagazzino, codiceArticolo, codiceVariante,
					soloValore, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ListaMovimentiMagazzino getByArticoloVariante_PrevAndNext(
		Session session, ListaMovimentiMagazzino listaMovimentiMagazzino,
		String codiceArticolo, String codiceVariante, int soloValore,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LISTAMOVIMENTIMAGAZZINO_WHERE);

		boolean bindCodiceArticolo = false;

		if (codiceArticolo == null) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_1);
		}
		else if (codiceArticolo.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_3);
		}
		else {
			bindCodiceArticolo = true;

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_2);
		}

		boolean bindCodiceVariante = false;

		if (codiceVariante == null) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_1);
		}
		else if (codiceVariante.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_3);
		}
		else {
			bindCodiceVariante = true;

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_2);
		}

		query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_SOLOVALORE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ListaMovimentiMagazzinoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodiceArticolo) {
			qPos.add(codiceArticolo);
		}

		if (bindCodiceVariante) {
			qPos.add(codiceVariante);
		}

		qPos.add(soloValore);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(listaMovimentiMagazzino);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ListaMovimentiMagazzino> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63; from the database.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByArticoloVariante(String codiceArticolo,
		String codiceVariante, int soloValore) throws SystemException {
		for (ListaMovimentiMagazzino listaMovimentiMagazzino : findByArticoloVariante(
				codiceArticolo, codiceVariante, soloValore, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(listaMovimentiMagazzino);
		}
	}

	/**
	 * Returns the number of lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param soloValore the solo valore
	 * @return the number of matching lista movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByArticoloVariante(String codiceArticolo,
		String codiceVariante, int soloValore) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ARTICOLOVARIANTE;

		Object[] finderArgs = new Object[] {
				codiceArticolo, codiceVariante, soloValore
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_LISTAMOVIMENTIMAGAZZINO_WHERE);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_2);
			}

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTE_SOLOVALORE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				qPos.add(soloValore);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_1 =
		"listaMovimentiMagazzino.codiceArticolo IS NULL AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_2 =
		"listaMovimentiMagazzino.codiceArticolo = ? AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEARTICOLO_3 =
		"(listaMovimentiMagazzino.codiceArticolo IS NULL OR listaMovimentiMagazzino.codiceArticolo = '') AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_1 =
		"listaMovimentiMagazzino.codiceVariante IS NULL AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_2 =
		"listaMovimentiMagazzino.codiceVariante = ? AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_CODICEVARIANTE_3 =
		"(listaMovimentiMagazzino.codiceVariante IS NULL OR listaMovimentiMagazzino.codiceVariante = '') AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTE_SOLOVALORE_2 = "listaMovimentiMagazzino.soloValore = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTICOLOVARIANTECARICOSCARICO =
		new FinderPath(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			ListaMovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED,
			ListaMovimentiMagazzinoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByArticoloVarianteCaricoScarico",
			new String[] {
				String.class.getName(), String.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTICOLOVARIANTECARICOSCARICO =
		new FinderPath(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			ListaMovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED,
			ListaMovimentiMagazzinoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByArticoloVarianteCaricoScarico",
			new String[] {
				String.class.getName(), String.class.getName(),
				Integer.class.getName(), Integer.class.getName()
			},
			ListaMovimentiMagazzinoModelImpl.CODICEARTICOLO_COLUMN_BITMASK |
			ListaMovimentiMagazzinoModelImpl.CODICEVARIANTE_COLUMN_BITMASK |
			ListaMovimentiMagazzinoModelImpl.TESTCARICOSCARICO_COLUMN_BITMASK |
			ListaMovimentiMagazzinoModelImpl.SOLOVALORE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ARTICOLOVARIANTECARICOSCARICO =
		new FinderPath(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			ListaMovimentiMagazzinoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByArticoloVarianteCaricoScarico",
			new String[] {
				String.class.getName(), String.class.getName(),
				Integer.class.getName(), Integer.class.getName()
			});

	/**
	 * Returns all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @return the matching lista movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListaMovimentiMagazzino> findByArticoloVarianteCaricoScarico(
		String codiceArticolo, String codiceVariante, int testCaricoScarico,
		int soloValore) throws SystemException {
		return findByArticoloVarianteCaricoScarico(codiceArticolo,
			codiceVariante, testCaricoScarico, soloValore, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @param start the lower bound of the range of lista movimenti magazzinos
	 * @param end the upper bound of the range of lista movimenti magazzinos (not inclusive)
	 * @return the range of matching lista movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListaMovimentiMagazzino> findByArticoloVarianteCaricoScarico(
		String codiceArticolo, String codiceVariante, int testCaricoScarico,
		int soloValore, int start, int end) throws SystemException {
		return findByArticoloVarianteCaricoScarico(codiceArticolo,
			codiceVariante, testCaricoScarico, soloValore, start, end, null);
	}

	/**
	 * Returns an ordered range of all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @param start the lower bound of the range of lista movimenti magazzinos
	 * @param end the upper bound of the range of lista movimenti magazzinos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching lista movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListaMovimentiMagazzino> findByArticoloVarianteCaricoScarico(
		String codiceArticolo, String codiceVariante, int testCaricoScarico,
		int soloValore, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTICOLOVARIANTECARICOSCARICO;
			finderArgs = new Object[] {
					codiceArticolo, codiceVariante, testCaricoScarico,
					soloValore
				};
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTICOLOVARIANTECARICOSCARICO;
			finderArgs = new Object[] {
					codiceArticolo, codiceVariante, testCaricoScarico,
					soloValore,
					
					start, end, orderByComparator
				};
		}

		List<ListaMovimentiMagazzino> list = (List<ListaMovimentiMagazzino>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ListaMovimentiMagazzino listaMovimentiMagazzino : list) {
				if (!Validator.equals(codiceArticolo,
							listaMovimentiMagazzino.getCodiceArticolo()) ||
						!Validator.equals(codiceVariante,
							listaMovimentiMagazzino.getCodiceVariante()) ||
						(testCaricoScarico != listaMovimentiMagazzino.getTestCaricoScarico()) ||
						(soloValore != listaMovimentiMagazzino.getSoloValore())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(6 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(6);
			}

			query.append(_SQL_SELECT_LISTAMOVIMENTIMAGAZZINO_WHERE);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_2);
			}

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_TESTCARICOSCARICO_2);

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_SOLOVALORE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ListaMovimentiMagazzinoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				qPos.add(testCaricoScarico);

				qPos.add(soloValore);

				if (!pagination) {
					list = (List<ListaMovimentiMagazzino>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ListaMovimentiMagazzino>(list);
				}
				else {
					list = (List<ListaMovimentiMagazzino>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lista movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a matching lista movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino findByArticoloVarianteCaricoScarico_First(
		String codiceArticolo, String codiceVariante, int testCaricoScarico,
		int soloValore, OrderByComparator orderByComparator)
		throws NoSuchListaMovimentiMagazzinoException, SystemException {
		ListaMovimentiMagazzino listaMovimentiMagazzino = fetchByArticoloVarianteCaricoScarico_First(codiceArticolo,
				codiceVariante, testCaricoScarico, soloValore, orderByComparator);

		if (listaMovimentiMagazzino != null) {
			return listaMovimentiMagazzino;
		}

		StringBundler msg = new StringBundler(10);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(", testCaricoScarico=");
		msg.append(testCaricoScarico);

		msg.append(", soloValore=");
		msg.append(soloValore);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchListaMovimentiMagazzinoException(msg.toString());
	}

	/**
	 * Returns the first lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lista movimenti magazzino, or <code>null</code> if a matching lista movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino fetchByArticoloVarianteCaricoScarico_First(
		String codiceArticolo, String codiceVariante, int testCaricoScarico,
		int soloValore, OrderByComparator orderByComparator)
		throws SystemException {
		List<ListaMovimentiMagazzino> list = findByArticoloVarianteCaricoScarico(codiceArticolo,
				codiceVariante, testCaricoScarico, soloValore, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lista movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a matching lista movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino findByArticoloVarianteCaricoScarico_Last(
		String codiceArticolo, String codiceVariante, int testCaricoScarico,
		int soloValore, OrderByComparator orderByComparator)
		throws NoSuchListaMovimentiMagazzinoException, SystemException {
		ListaMovimentiMagazzino listaMovimentiMagazzino = fetchByArticoloVarianteCaricoScarico_Last(codiceArticolo,
				codiceVariante, testCaricoScarico, soloValore, orderByComparator);

		if (listaMovimentiMagazzino != null) {
			return listaMovimentiMagazzino;
		}

		StringBundler msg = new StringBundler(10);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(", testCaricoScarico=");
		msg.append(testCaricoScarico);

		msg.append(", soloValore=");
		msg.append(soloValore);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchListaMovimentiMagazzinoException(msg.toString());
	}

	/**
	 * Returns the last lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lista movimenti magazzino, or <code>null</code> if a matching lista movimenti magazzino could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino fetchByArticoloVarianteCaricoScarico_Last(
		String codiceArticolo, String codiceVariante, int testCaricoScarico,
		int soloValore, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByArticoloVarianteCaricoScarico(codiceArticolo,
				codiceVariante, testCaricoScarico, soloValore);

		if (count == 0) {
			return null;
		}

		List<ListaMovimentiMagazzino> list = findByArticoloVarianteCaricoScarico(codiceArticolo,
				codiceVariante, testCaricoScarico, soloValore, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the lista movimenti magazzinos before and after the current lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	 *
	 * @param ID the primary key of the current lista movimenti magazzino
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next lista movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a lista movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino[] findByArticoloVarianteCaricoScarico_PrevAndNext(
		String ID, String codiceArticolo, String codiceVariante,
		int testCaricoScarico, int soloValore,
		OrderByComparator orderByComparator)
		throws NoSuchListaMovimentiMagazzinoException, SystemException {
		ListaMovimentiMagazzino listaMovimentiMagazzino = findByPrimaryKey(ID);

		Session session = null;

		try {
			session = openSession();

			ListaMovimentiMagazzino[] array = new ListaMovimentiMagazzinoImpl[3];

			array[0] = getByArticoloVarianteCaricoScarico_PrevAndNext(session,
					listaMovimentiMagazzino, codiceArticolo, codiceVariante,
					testCaricoScarico, soloValore, orderByComparator, true);

			array[1] = listaMovimentiMagazzino;

			array[2] = getByArticoloVarianteCaricoScarico_PrevAndNext(session,
					listaMovimentiMagazzino, codiceArticolo, codiceVariante,
					testCaricoScarico, soloValore, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ListaMovimentiMagazzino getByArticoloVarianteCaricoScarico_PrevAndNext(
		Session session, ListaMovimentiMagazzino listaMovimentiMagazzino,
		String codiceArticolo, String codiceVariante, int testCaricoScarico,
		int soloValore, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LISTAMOVIMENTIMAGAZZINO_WHERE);

		boolean bindCodiceArticolo = false;

		if (codiceArticolo == null) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_1);
		}
		else if (codiceArticolo.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_3);
		}
		else {
			bindCodiceArticolo = true;

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_2);
		}

		boolean bindCodiceVariante = false;

		if (codiceVariante == null) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_1);
		}
		else if (codiceVariante.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_3);
		}
		else {
			bindCodiceVariante = true;

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_2);
		}

		query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_TESTCARICOSCARICO_2);

		query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_SOLOVALORE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ListaMovimentiMagazzinoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodiceArticolo) {
			qPos.add(codiceArticolo);
		}

		if (bindCodiceVariante) {
			qPos.add(codiceVariante);
		}

		qPos.add(testCaricoScarico);

		qPos.add(soloValore);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(listaMovimentiMagazzino);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ListaMovimentiMagazzino> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63; from the database.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByArticoloVarianteCaricoScarico(String codiceArticolo,
		String codiceVariante, int testCaricoScarico, int soloValore)
		throws SystemException {
		for (ListaMovimentiMagazzino listaMovimentiMagazzino : findByArticoloVarianteCaricoScarico(
				codiceArticolo, codiceVariante, testCaricoScarico, soloValore,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(listaMovimentiMagazzino);
		}
	}

	/**
	 * Returns the number of lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param testCaricoScarico the test carico scarico
	 * @param soloValore the solo valore
	 * @return the number of matching lista movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByArticoloVarianteCaricoScarico(String codiceArticolo,
		String codiceVariante, int testCaricoScarico, int soloValore)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ARTICOLOVARIANTECARICOSCARICO;

		Object[] finderArgs = new Object[] {
				codiceArticolo, codiceVariante, testCaricoScarico, soloValore
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(5);

			query.append(_SQL_COUNT_LISTAMOVIMENTIMAGAZZINO_WHERE);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_2);
			}

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_TESTCARICOSCARICO_2);

			query.append(_FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_SOLOVALORE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				qPos.add(testCaricoScarico);

				qPos.add(soloValore);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_1 =
		"listaMovimentiMagazzino.codiceArticolo IS NULL AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_2 =
		"listaMovimentiMagazzino.codiceArticolo = ? AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEARTICOLO_3 =
		"(listaMovimentiMagazzino.codiceArticolo IS NULL OR listaMovimentiMagazzino.codiceArticolo = '') AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_1 =
		"listaMovimentiMagazzino.codiceVariante IS NULL AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_2 =
		"listaMovimentiMagazzino.codiceVariante = ? AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_CODICEVARIANTE_3 =
		"(listaMovimentiMagazzino.codiceVariante IS NULL OR listaMovimentiMagazzino.codiceVariante = '') AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_TESTCARICOSCARICO_2 =
		"listaMovimentiMagazzino.testCaricoScarico = ? AND ";
	private static final String _FINDER_COLUMN_ARTICOLOVARIANTECARICOSCARICO_SOLOVALORE_2 =
		"listaMovimentiMagazzino.soloValore = ?";

	public ListaMovimentiMagazzinoPersistenceImpl() {
		setModelClass(ListaMovimentiMagazzino.class);
	}

	/**
	 * Caches the lista movimenti magazzino in the entity cache if it is enabled.
	 *
	 * @param listaMovimentiMagazzino the lista movimenti magazzino
	 */
	@Override
	public void cacheResult(ListaMovimentiMagazzino listaMovimentiMagazzino) {
		EntityCacheUtil.putResult(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			ListaMovimentiMagazzinoImpl.class,
			listaMovimentiMagazzino.getPrimaryKey(), listaMovimentiMagazzino);

		listaMovimentiMagazzino.resetOriginalValues();
	}

	/**
	 * Caches the lista movimenti magazzinos in the entity cache if it is enabled.
	 *
	 * @param listaMovimentiMagazzinos the lista movimenti magazzinos
	 */
	@Override
	public void cacheResult(
		List<ListaMovimentiMagazzino> listaMovimentiMagazzinos) {
		for (ListaMovimentiMagazzino listaMovimentiMagazzino : listaMovimentiMagazzinos) {
			if (EntityCacheUtil.getResult(
						ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
						ListaMovimentiMagazzinoImpl.class,
						listaMovimentiMagazzino.getPrimaryKey()) == null) {
				cacheResult(listaMovimentiMagazzino);
			}
			else {
				listaMovimentiMagazzino.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all lista movimenti magazzinos.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ListaMovimentiMagazzinoImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ListaMovimentiMagazzinoImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the lista movimenti magazzino.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ListaMovimentiMagazzino listaMovimentiMagazzino) {
		EntityCacheUtil.removeResult(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			ListaMovimentiMagazzinoImpl.class,
			listaMovimentiMagazzino.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(
		List<ListaMovimentiMagazzino> listaMovimentiMagazzinos) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ListaMovimentiMagazzino listaMovimentiMagazzino : listaMovimentiMagazzinos) {
			EntityCacheUtil.removeResult(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
				ListaMovimentiMagazzinoImpl.class,
				listaMovimentiMagazzino.getPrimaryKey());
		}
	}

	/**
	 * Creates a new lista movimenti magazzino with the primary key. Does not add the lista movimenti magazzino to the database.
	 *
	 * @param ID the primary key for the new lista movimenti magazzino
	 * @return the new lista movimenti magazzino
	 */
	@Override
	public ListaMovimentiMagazzino create(String ID) {
		ListaMovimentiMagazzino listaMovimentiMagazzino = new ListaMovimentiMagazzinoImpl();

		listaMovimentiMagazzino.setNew(true);
		listaMovimentiMagazzino.setPrimaryKey(ID);

		return listaMovimentiMagazzino;
	}

	/**
	 * Removes the lista movimenti magazzino with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ID the primary key of the lista movimenti magazzino
	 * @return the lista movimenti magazzino that was removed
	 * @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a lista movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino remove(String ID)
		throws NoSuchListaMovimentiMagazzinoException, SystemException {
		return remove((Serializable)ID);
	}

	/**
	 * Removes the lista movimenti magazzino with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the lista movimenti magazzino
	 * @return the lista movimenti magazzino that was removed
	 * @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a lista movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino remove(Serializable primaryKey)
		throws NoSuchListaMovimentiMagazzinoException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ListaMovimentiMagazzino listaMovimentiMagazzino = (ListaMovimentiMagazzino)session.get(ListaMovimentiMagazzinoImpl.class,
					primaryKey);

			if (listaMovimentiMagazzino == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchListaMovimentiMagazzinoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(listaMovimentiMagazzino);
		}
		catch (NoSuchListaMovimentiMagazzinoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ListaMovimentiMagazzino removeImpl(
		ListaMovimentiMagazzino listaMovimentiMagazzino)
		throws SystemException {
		listaMovimentiMagazzino = toUnwrappedModel(listaMovimentiMagazzino);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(listaMovimentiMagazzino)) {
				listaMovimentiMagazzino = (ListaMovimentiMagazzino)session.get(ListaMovimentiMagazzinoImpl.class,
						listaMovimentiMagazzino.getPrimaryKeyObj());
			}

			if (listaMovimentiMagazzino != null) {
				session.delete(listaMovimentiMagazzino);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (listaMovimentiMagazzino != null) {
			clearCache(listaMovimentiMagazzino);
		}

		return listaMovimentiMagazzino;
	}

	@Override
	public ListaMovimentiMagazzino updateImpl(
		it.bysoftware.ct.model.ListaMovimentiMagazzino listaMovimentiMagazzino)
		throws SystemException {
		listaMovimentiMagazzino = toUnwrappedModel(listaMovimentiMagazzino);

		boolean isNew = listaMovimentiMagazzino.isNew();

		ListaMovimentiMagazzinoModelImpl listaMovimentiMagazzinoModelImpl = (ListaMovimentiMagazzinoModelImpl)listaMovimentiMagazzino;

		Session session = null;

		try {
			session = openSession();

			if (listaMovimentiMagazzino.isNew()) {
				session.save(listaMovimentiMagazzino);

				listaMovimentiMagazzino.setNew(false);
			}
			else {
				session.merge(listaMovimentiMagazzino);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ListaMovimentiMagazzinoModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((listaMovimentiMagazzinoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTICOLOVARIANTE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						listaMovimentiMagazzinoModelImpl.getOriginalCodiceArticolo(),
						listaMovimentiMagazzinoModelImpl.getOriginalCodiceVariante(),
						listaMovimentiMagazzinoModelImpl.getOriginalSoloValore()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTICOLOVARIANTE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTICOLOVARIANTE,
					args);

				args = new Object[] {
						listaMovimentiMagazzinoModelImpl.getCodiceArticolo(),
						listaMovimentiMagazzinoModelImpl.getCodiceVariante(),
						listaMovimentiMagazzinoModelImpl.getSoloValore()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTICOLOVARIANTE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTICOLOVARIANTE,
					args);
			}

			if ((listaMovimentiMagazzinoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTICOLOVARIANTECARICOSCARICO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						listaMovimentiMagazzinoModelImpl.getOriginalCodiceArticolo(),
						listaMovimentiMagazzinoModelImpl.getOriginalCodiceVariante(),
						listaMovimentiMagazzinoModelImpl.getOriginalTestCaricoScarico(),
						listaMovimentiMagazzinoModelImpl.getOriginalSoloValore()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTICOLOVARIANTECARICOSCARICO,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTICOLOVARIANTECARICOSCARICO,
					args);

				args = new Object[] {
						listaMovimentiMagazzinoModelImpl.getCodiceArticolo(),
						listaMovimentiMagazzinoModelImpl.getCodiceVariante(),
						listaMovimentiMagazzinoModelImpl.getTestCaricoScarico(),
						listaMovimentiMagazzinoModelImpl.getSoloValore()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTICOLOVARIANTECARICOSCARICO,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTICOLOVARIANTECARICOSCARICO,
					args);
			}
		}

		EntityCacheUtil.putResult(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
			ListaMovimentiMagazzinoImpl.class,
			listaMovimentiMagazzino.getPrimaryKey(), listaMovimentiMagazzino);

		return listaMovimentiMagazzino;
	}

	protected ListaMovimentiMagazzino toUnwrappedModel(
		ListaMovimentiMagazzino listaMovimentiMagazzino) {
		if (listaMovimentiMagazzino instanceof ListaMovimentiMagazzinoImpl) {
			return listaMovimentiMagazzino;
		}

		ListaMovimentiMagazzinoImpl listaMovimentiMagazzinoImpl = new ListaMovimentiMagazzinoImpl();

		listaMovimentiMagazzinoImpl.setNew(listaMovimentiMagazzino.isNew());
		listaMovimentiMagazzinoImpl.setPrimaryKey(listaMovimentiMagazzino.getPrimaryKey());

		listaMovimentiMagazzinoImpl.setID(listaMovimentiMagazzino.getID());
		listaMovimentiMagazzinoImpl.setCodiceArticolo(listaMovimentiMagazzino.getCodiceArticolo());
		listaMovimentiMagazzinoImpl.setCodiceVariante(listaMovimentiMagazzino.getCodiceVariante());
		listaMovimentiMagazzinoImpl.setDescrizione(listaMovimentiMagazzino.getDescrizione());
		listaMovimentiMagazzinoImpl.setQuantita(listaMovimentiMagazzino.getQuantita());
		listaMovimentiMagazzinoImpl.setTestCaricoScarico(listaMovimentiMagazzino.getTestCaricoScarico());
		listaMovimentiMagazzinoImpl.setSoloValore(listaMovimentiMagazzino.getSoloValore());

		return listaMovimentiMagazzinoImpl;
	}

	/**
	 * Returns the lista movimenti magazzino with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the lista movimenti magazzino
	 * @return the lista movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a lista movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino findByPrimaryKey(Serializable primaryKey)
		throws NoSuchListaMovimentiMagazzinoException, SystemException {
		ListaMovimentiMagazzino listaMovimentiMagazzino = fetchByPrimaryKey(primaryKey);

		if (listaMovimentiMagazzino == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchListaMovimentiMagazzinoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return listaMovimentiMagazzino;
	}

	/**
	 * Returns the lista movimenti magazzino with the primary key or throws a {@link it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException} if it could not be found.
	 *
	 * @param ID the primary key of the lista movimenti magazzino
	 * @return the lista movimenti magazzino
	 * @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a lista movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino findByPrimaryKey(String ID)
		throws NoSuchListaMovimentiMagazzinoException, SystemException {
		return findByPrimaryKey((Serializable)ID);
	}

	/**
	 * Returns the lista movimenti magazzino with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the lista movimenti magazzino
	 * @return the lista movimenti magazzino, or <code>null</code> if a lista movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ListaMovimentiMagazzino listaMovimentiMagazzino = (ListaMovimentiMagazzino)EntityCacheUtil.getResult(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
				ListaMovimentiMagazzinoImpl.class, primaryKey);

		if (listaMovimentiMagazzino == _nullListaMovimentiMagazzino) {
			return null;
		}

		if (listaMovimentiMagazzino == null) {
			Session session = null;

			try {
				session = openSession();

				listaMovimentiMagazzino = (ListaMovimentiMagazzino)session.get(ListaMovimentiMagazzinoImpl.class,
						primaryKey);

				if (listaMovimentiMagazzino != null) {
					cacheResult(listaMovimentiMagazzino);
				}
				else {
					EntityCacheUtil.putResult(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
						ListaMovimentiMagazzinoImpl.class, primaryKey,
						_nullListaMovimentiMagazzino);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ListaMovimentiMagazzinoModelImpl.ENTITY_CACHE_ENABLED,
					ListaMovimentiMagazzinoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return listaMovimentiMagazzino;
	}

	/**
	 * Returns the lista movimenti magazzino with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ID the primary key of the lista movimenti magazzino
	 * @return the lista movimenti magazzino, or <code>null</code> if a lista movimenti magazzino with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListaMovimentiMagazzino fetchByPrimaryKey(String ID)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)ID);
	}

	/**
	 * Returns all the lista movimenti magazzinos.
	 *
	 * @return the lista movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListaMovimentiMagazzino> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lista movimenti magazzinos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of lista movimenti magazzinos
	 * @param end the upper bound of the range of lista movimenti magazzinos (not inclusive)
	 * @return the range of lista movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListaMovimentiMagazzino> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the lista movimenti magazzinos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of lista movimenti magazzinos
	 * @param end the upper bound of the range of lista movimenti magazzinos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of lista movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListaMovimentiMagazzino> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ListaMovimentiMagazzino> list = (List<ListaMovimentiMagazzino>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_LISTAMOVIMENTIMAGAZZINO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LISTAMOVIMENTIMAGAZZINO;

				if (pagination) {
					sql = sql.concat(ListaMovimentiMagazzinoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ListaMovimentiMagazzino>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ListaMovimentiMagazzino>(list);
				}
				else {
					list = (List<ListaMovimentiMagazzino>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the lista movimenti magazzinos from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ListaMovimentiMagazzino listaMovimentiMagazzino : findAll()) {
			remove(listaMovimentiMagazzino);
		}
	}

	/**
	 * Returns the number of lista movimenti magazzinos.
	 *
	 * @return the number of lista movimenti magazzinos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LISTAMOVIMENTIMAGAZZINO);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the lista movimenti magazzino persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.ListaMovimentiMagazzino")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ListaMovimentiMagazzino>> listenersList = new ArrayList<ModelListener<ListaMovimentiMagazzino>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ListaMovimentiMagazzino>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ListaMovimentiMagazzinoImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_LISTAMOVIMENTIMAGAZZINO = "SELECT listaMovimentiMagazzino FROM ListaMovimentiMagazzino listaMovimentiMagazzino";
	private static final String _SQL_SELECT_LISTAMOVIMENTIMAGAZZINO_WHERE = "SELECT listaMovimentiMagazzino FROM ListaMovimentiMagazzino listaMovimentiMagazzino WHERE ";
	private static final String _SQL_COUNT_LISTAMOVIMENTIMAGAZZINO = "SELECT COUNT(listaMovimentiMagazzino) FROM ListaMovimentiMagazzino listaMovimentiMagazzino";
	private static final String _SQL_COUNT_LISTAMOVIMENTIMAGAZZINO_WHERE = "SELECT COUNT(listaMovimentiMagazzino) FROM ListaMovimentiMagazzino listaMovimentiMagazzino WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "listaMovimentiMagazzino.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ListaMovimentiMagazzino exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ListaMovimentiMagazzino exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ListaMovimentiMagazzinoPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"codiceArticolo", "codiceVariante", "descrizione", "quantita",
				"testCaricoScarico", "soloValore"
			});
	private static ListaMovimentiMagazzino _nullListaMovimentiMagazzino = new ListaMovimentiMagazzinoImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ListaMovimentiMagazzino> toCacheModel() {
				return _nullListaMovimentiMagazzinoCacheModel;
			}
		};

	private static CacheModel<ListaMovimentiMagazzino> _nullListaMovimentiMagazzinoCacheModel =
		new CacheModel<ListaMovimentiMagazzino>() {
			@Override
			public ListaMovimentiMagazzino toEntityModel() {
				return _nullListaMovimentiMagazzino;
			}
		};
}