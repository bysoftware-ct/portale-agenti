/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException;
import it.bysoftware.ct.model.DatiCostantiClientiFornitori;
import it.bysoftware.ct.model.impl.DatiCostantiClientiFornitoriImpl;
import it.bysoftware.ct.model.impl.DatiCostantiClientiFornitoriModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the dati costanti clienti fornitori service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see DatiCostantiClientiFornitoriPersistence
 * @see DatiCostantiClientiFornitoriUtil
 * @generated
 */
public class DatiCostantiClientiFornitoriPersistenceImpl
	extends BasePersistenceImpl<DatiCostantiClientiFornitori>
	implements DatiCostantiClientiFornitoriPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link DatiCostantiClientiFornitoriUtil} to access the dati costanti clienti fornitori persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = DatiCostantiClientiFornitoriImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DatiCostantiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiCostantiClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
			DatiCostantiClientiFornitoriImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DatiCostantiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiCostantiClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
			DatiCostantiClientiFornitoriImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DatiCostantiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiCostantiClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);

	public DatiCostantiClientiFornitoriPersistenceImpl() {
		setModelClass(DatiCostantiClientiFornitori.class);
	}

	/**
	 * Caches the dati costanti clienti fornitori in the entity cache if it is enabled.
	 *
	 * @param datiCostantiClientiFornitori the dati costanti clienti fornitori
	 */
	@Override
	public void cacheResult(
		DatiCostantiClientiFornitori datiCostantiClientiFornitori) {
		EntityCacheUtil.putResult(DatiCostantiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiCostantiClientiFornitoriImpl.class,
			datiCostantiClientiFornitori.getPrimaryKey(),
			datiCostantiClientiFornitori);

		datiCostantiClientiFornitori.resetOriginalValues();
	}

	/**
	 * Caches the dati costanti clienti fornitoris in the entity cache if it is enabled.
	 *
	 * @param datiCostantiClientiFornitoris the dati costanti clienti fornitoris
	 */
	@Override
	public void cacheResult(
		List<DatiCostantiClientiFornitori> datiCostantiClientiFornitoris) {
		for (DatiCostantiClientiFornitori datiCostantiClientiFornitori : datiCostantiClientiFornitoris) {
			if (EntityCacheUtil.getResult(
						DatiCostantiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
						DatiCostantiClientiFornitoriImpl.class,
						datiCostantiClientiFornitori.getPrimaryKey()) == null) {
				cacheResult(datiCostantiClientiFornitori);
			}
			else {
				datiCostantiClientiFornitori.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all dati costanti clienti fornitoris.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(DatiCostantiClientiFornitoriImpl.class.getName());
		}

		EntityCacheUtil.clearCache(DatiCostantiClientiFornitoriImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the dati costanti clienti fornitori.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(
		DatiCostantiClientiFornitori datiCostantiClientiFornitori) {
		EntityCacheUtil.removeResult(DatiCostantiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiCostantiClientiFornitoriImpl.class,
			datiCostantiClientiFornitori.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(
		List<DatiCostantiClientiFornitori> datiCostantiClientiFornitoris) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (DatiCostantiClientiFornitori datiCostantiClientiFornitori : datiCostantiClientiFornitoris) {
			EntityCacheUtil.removeResult(DatiCostantiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
				DatiCostantiClientiFornitoriImpl.class,
				datiCostantiClientiFornitori.getPrimaryKey());
		}
	}

	/**
	 * Creates a new dati costanti clienti fornitori with the primary key. Does not add the dati costanti clienti fornitori to the database.
	 *
	 * @param datiCostantiClientiFornitoriPK the primary key for the new dati costanti clienti fornitori
	 * @return the new dati costanti clienti fornitori
	 */
	@Override
	public DatiCostantiClientiFornitori create(
		DatiCostantiClientiFornitoriPK datiCostantiClientiFornitoriPK) {
		DatiCostantiClientiFornitori datiCostantiClientiFornitori = new DatiCostantiClientiFornitoriImpl();

		datiCostantiClientiFornitori.setNew(true);
		datiCostantiClientiFornitori.setPrimaryKey(datiCostantiClientiFornitoriPK);

		return datiCostantiClientiFornitori;
	}

	/**
	 * Removes the dati costanti clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param datiCostantiClientiFornitoriPK the primary key of the dati costanti clienti fornitori
	 * @return the dati costanti clienti fornitori that was removed
	 * @throws it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException if a dati costanti clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiCostantiClientiFornitori remove(
		DatiCostantiClientiFornitoriPK datiCostantiClientiFornitoriPK)
		throws NoSuchDatiCostantiClientiFornitoriException, SystemException {
		return remove((Serializable)datiCostantiClientiFornitoriPK);
	}

	/**
	 * Removes the dati costanti clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the dati costanti clienti fornitori
	 * @return the dati costanti clienti fornitori that was removed
	 * @throws it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException if a dati costanti clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiCostantiClientiFornitori remove(Serializable primaryKey)
		throws NoSuchDatiCostantiClientiFornitoriException, SystemException {
		Session session = null;

		try {
			session = openSession();

			DatiCostantiClientiFornitori datiCostantiClientiFornitori = (DatiCostantiClientiFornitori)session.get(DatiCostantiClientiFornitoriImpl.class,
					primaryKey);

			if (datiCostantiClientiFornitori == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDatiCostantiClientiFornitoriException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(datiCostantiClientiFornitori);
		}
		catch (NoSuchDatiCostantiClientiFornitoriException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected DatiCostantiClientiFornitori removeImpl(
		DatiCostantiClientiFornitori datiCostantiClientiFornitori)
		throws SystemException {
		datiCostantiClientiFornitori = toUnwrappedModel(datiCostantiClientiFornitori);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(datiCostantiClientiFornitori)) {
				datiCostantiClientiFornitori = (DatiCostantiClientiFornitori)session.get(DatiCostantiClientiFornitoriImpl.class,
						datiCostantiClientiFornitori.getPrimaryKeyObj());
			}

			if (datiCostantiClientiFornitori != null) {
				session.delete(datiCostantiClientiFornitori);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (datiCostantiClientiFornitori != null) {
			clearCache(datiCostantiClientiFornitori);
		}

		return datiCostantiClientiFornitori;
	}

	@Override
	public DatiCostantiClientiFornitori updateImpl(
		it.bysoftware.ct.model.DatiCostantiClientiFornitori datiCostantiClientiFornitori)
		throws SystemException {
		datiCostantiClientiFornitori = toUnwrappedModel(datiCostantiClientiFornitori);

		boolean isNew = datiCostantiClientiFornitori.isNew();

		Session session = null;

		try {
			session = openSession();

			if (datiCostantiClientiFornitori.isNew()) {
				session.save(datiCostantiClientiFornitori);

				datiCostantiClientiFornitori.setNew(false);
			}
			else {
				session.merge(datiCostantiClientiFornitori);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(DatiCostantiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			DatiCostantiClientiFornitoriImpl.class,
			datiCostantiClientiFornitori.getPrimaryKey(),
			datiCostantiClientiFornitori);

		return datiCostantiClientiFornitori;
	}

	protected DatiCostantiClientiFornitori toUnwrappedModel(
		DatiCostantiClientiFornitori datiCostantiClientiFornitori) {
		if (datiCostantiClientiFornitori instanceof DatiCostantiClientiFornitoriImpl) {
			return datiCostantiClientiFornitori;
		}

		DatiCostantiClientiFornitoriImpl datiCostantiClientiFornitoriImpl = new DatiCostantiClientiFornitoriImpl();

		datiCostantiClientiFornitoriImpl.setNew(datiCostantiClientiFornitori.isNew());
		datiCostantiClientiFornitoriImpl.setPrimaryKey(datiCostantiClientiFornitori.getPrimaryKey());

		datiCostantiClientiFornitoriImpl.setTipoSoggetto(datiCostantiClientiFornitori.isTipoSoggetto());
		datiCostantiClientiFornitoriImpl.setCodiceSoggetto(datiCostantiClientiFornitori.getCodiceSoggetto());
		datiCostantiClientiFornitoriImpl.setCodiceSottoconto(datiCostantiClientiFornitori.getCodiceSottoconto());
		datiCostantiClientiFornitoriImpl.setMassimoScoperto(datiCostantiClientiFornitori.getMassimoScoperto());
		datiCostantiClientiFornitoriImpl.setGestioneEstrattoConto(datiCostantiClientiFornitori.isGestioneEstrattoConto());
		datiCostantiClientiFornitoriImpl.setCodicePagamento(datiCostantiClientiFornitori.getCodicePagamento());
		datiCostantiClientiFornitoriImpl.setCodiceBancaPagFor(datiCostantiClientiFornitori.getCodiceBancaPagFor());
		datiCostantiClientiFornitoriImpl.setCodiceAgenziaPagFor(datiCostantiClientiFornitori.getCodiceAgenziaPagFor());
		datiCostantiClientiFornitoriImpl.setCodiceContropartita(datiCostantiClientiFornitori.getCodiceContropartita());
		datiCostantiClientiFornitoriImpl.setCalcoloIntMora(datiCostantiClientiFornitori.isCalcoloIntMora());

		return datiCostantiClientiFornitoriImpl;
	}

	/**
	 * Returns the dati costanti clienti fornitori with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the dati costanti clienti fornitori
	 * @return the dati costanti clienti fornitori
	 * @throws it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException if a dati costanti clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiCostantiClientiFornitori findByPrimaryKey(
		Serializable primaryKey)
		throws NoSuchDatiCostantiClientiFornitoriException, SystemException {
		DatiCostantiClientiFornitori datiCostantiClientiFornitori = fetchByPrimaryKey(primaryKey);

		if (datiCostantiClientiFornitori == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchDatiCostantiClientiFornitoriException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return datiCostantiClientiFornitori;
	}

	/**
	 * Returns the dati costanti clienti fornitori with the primary key or throws a {@link it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException} if it could not be found.
	 *
	 * @param datiCostantiClientiFornitoriPK the primary key of the dati costanti clienti fornitori
	 * @return the dati costanti clienti fornitori
	 * @throws it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException if a dati costanti clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiCostantiClientiFornitori findByPrimaryKey(
		DatiCostantiClientiFornitoriPK datiCostantiClientiFornitoriPK)
		throws NoSuchDatiCostantiClientiFornitoriException, SystemException {
		return findByPrimaryKey((Serializable)datiCostantiClientiFornitoriPK);
	}

	/**
	 * Returns the dati costanti clienti fornitori with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the dati costanti clienti fornitori
	 * @return the dati costanti clienti fornitori, or <code>null</code> if a dati costanti clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiCostantiClientiFornitori fetchByPrimaryKey(
		Serializable primaryKey) throws SystemException {
		DatiCostantiClientiFornitori datiCostantiClientiFornitori = (DatiCostantiClientiFornitori)EntityCacheUtil.getResult(DatiCostantiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
				DatiCostantiClientiFornitoriImpl.class, primaryKey);

		if (datiCostantiClientiFornitori == _nullDatiCostantiClientiFornitori) {
			return null;
		}

		if (datiCostantiClientiFornitori == null) {
			Session session = null;

			try {
				session = openSession();

				datiCostantiClientiFornitori = (DatiCostantiClientiFornitori)session.get(DatiCostantiClientiFornitoriImpl.class,
						primaryKey);

				if (datiCostantiClientiFornitori != null) {
					cacheResult(datiCostantiClientiFornitori);
				}
				else {
					EntityCacheUtil.putResult(DatiCostantiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
						DatiCostantiClientiFornitoriImpl.class, primaryKey,
						_nullDatiCostantiClientiFornitori);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(DatiCostantiClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
					DatiCostantiClientiFornitoriImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return datiCostantiClientiFornitori;
	}

	/**
	 * Returns the dati costanti clienti fornitori with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param datiCostantiClientiFornitoriPK the primary key of the dati costanti clienti fornitori
	 * @return the dati costanti clienti fornitori, or <code>null</code> if a dati costanti clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DatiCostantiClientiFornitori fetchByPrimaryKey(
		DatiCostantiClientiFornitoriPK datiCostantiClientiFornitoriPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)datiCostantiClientiFornitoriPK);
	}

	/**
	 * Returns all the dati costanti clienti fornitoris.
	 *
	 * @return the dati costanti clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DatiCostantiClientiFornitori> findAll()
		throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the dati costanti clienti fornitoris.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiCostantiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of dati costanti clienti fornitoris
	 * @param end the upper bound of the range of dati costanti clienti fornitoris (not inclusive)
	 * @return the range of dati costanti clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DatiCostantiClientiFornitori> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the dati costanti clienti fornitoris.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiCostantiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of dati costanti clienti fornitoris
	 * @param end the upper bound of the range of dati costanti clienti fornitoris (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of dati costanti clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DatiCostantiClientiFornitori> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<DatiCostantiClientiFornitori> list = (List<DatiCostantiClientiFornitori>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_DATICOSTANTICLIENTIFORNITORI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_DATICOSTANTICLIENTIFORNITORI;

				if (pagination) {
					sql = sql.concat(DatiCostantiClientiFornitoriModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<DatiCostantiClientiFornitori>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<DatiCostantiClientiFornitori>(list);
				}
				else {
					list = (List<DatiCostantiClientiFornitori>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the dati costanti clienti fornitoris from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (DatiCostantiClientiFornitori datiCostantiClientiFornitori : findAll()) {
			remove(datiCostantiClientiFornitori);
		}
	}

	/**
	 * Returns the number of dati costanti clienti fornitoris.
	 *
	 * @return the number of dati costanti clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_DATICOSTANTICLIENTIFORNITORI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the dati costanti clienti fornitori persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.DatiCostantiClientiFornitori")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<DatiCostantiClientiFornitori>> listenersList = new ArrayList<ModelListener<DatiCostantiClientiFornitori>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<DatiCostantiClientiFornitori>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(DatiCostantiClientiFornitoriImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_DATICOSTANTICLIENTIFORNITORI = "SELECT datiCostantiClientiFornitori FROM DatiCostantiClientiFornitori datiCostantiClientiFornitori";
	private static final String _SQL_COUNT_DATICOSTANTICLIENTIFORNITORI = "SELECT COUNT(datiCostantiClientiFornitori) FROM DatiCostantiClientiFornitori datiCostantiClientiFornitori";
	private static final String _ORDER_BY_ENTITY_ALIAS = "datiCostantiClientiFornitori.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No DatiCostantiClientiFornitori exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(DatiCostantiClientiFornitoriPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"tipoSoggetto", "codiceSoggetto", "codiceSottoconto",
				"massimoScoperto", "gestioneEstrattoConto", "codicePagamento",
				"codiceBancaPagFor", "codiceAgenziaPagFor",
				"codiceContropartita", "calcoloIntMora"
			});
	private static DatiCostantiClientiFornitori _nullDatiCostantiClientiFornitori =
		new DatiCostantiClientiFornitoriImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<DatiCostantiClientiFornitori> toCacheModel() {
				return _nullDatiCostantiClientiFornitoriCacheModel;
			}
		};

	private static CacheModel<DatiCostantiClientiFornitori> _nullDatiCostantiClientiFornitoriCacheModel =
		new CacheModel<DatiCostantiClientiFornitori>() {
			@Override
			public DatiCostantiClientiFornitori toEntityModel() {
				return _nullDatiCostantiClientiFornitori;
			}
		};
}