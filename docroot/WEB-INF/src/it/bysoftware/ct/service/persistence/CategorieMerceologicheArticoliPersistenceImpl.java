/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException;
import it.bysoftware.ct.model.CategorieMerceologicheArticoli;
import it.bysoftware.ct.model.impl.CategorieMerceologicheArticoliImpl;
import it.bysoftware.ct.model.impl.CategorieMerceologicheArticoliModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the categorie merceologiche articoli service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CategorieMerceologicheArticoliPersistence
 * @see CategorieMerceologicheArticoliUtil
 * @generated
 */
public class CategorieMerceologicheArticoliPersistenceImpl
	extends BasePersistenceImpl<CategorieMerceologicheArticoli>
	implements CategorieMerceologicheArticoliPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CategorieMerceologicheArticoliUtil} to access the categorie merceologiche articoli persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CategorieMerceologicheArticoliImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CategorieMerceologicheArticoliModelImpl.ENTITY_CACHE_ENABLED,
			CategorieMerceologicheArticoliModelImpl.FINDER_CACHE_ENABLED,
			CategorieMerceologicheArticoliImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CategorieMerceologicheArticoliModelImpl.ENTITY_CACHE_ENABLED,
			CategorieMerceologicheArticoliModelImpl.FINDER_CACHE_ENABLED,
			CategorieMerceologicheArticoliImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CategorieMerceologicheArticoliModelImpl.ENTITY_CACHE_ENABLED,
			CategorieMerceologicheArticoliModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_CODICECATEGORIA = new FinderPath(CategorieMerceologicheArticoliModelImpl.ENTITY_CACHE_ENABLED,
			CategorieMerceologicheArticoliModelImpl.FINDER_CACHE_ENABLED,
			CategorieMerceologicheArticoliImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByCodiceCategoria", new String[] { String.class.getName() },
			CategorieMerceologicheArticoliModelImpl.CODICECATEGORIA_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CODICECATEGORIA = new FinderPath(CategorieMerceologicheArticoliModelImpl.ENTITY_CACHE_ENABLED,
			CategorieMerceologicheArticoliModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCodiceCategoria", new String[] { String.class.getName() });

	/**
	 * Returns the categorie merceologiche articoli where codiceCategoria = &#63; or throws a {@link it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException} if it could not be found.
	 *
	 * @param codiceCategoria the codice categoria
	 * @return the matching categorie merceologiche articoli
	 * @throws it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException if a matching categorie merceologiche articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CategorieMerceologicheArticoli findByCodiceCategoria(
		String codiceCategoria)
		throws NoSuchCategorieMerceologicheArticoliException, SystemException {
		CategorieMerceologicheArticoli categorieMerceologicheArticoli = fetchByCodiceCategoria(codiceCategoria);

		if (categorieMerceologicheArticoli == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("codiceCategoria=");
			msg.append(codiceCategoria);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchCategorieMerceologicheArticoliException(msg.toString());
		}

		return categorieMerceologicheArticoli;
	}

	/**
	 * Returns the categorie merceologiche articoli where codiceCategoria = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param codiceCategoria the codice categoria
	 * @return the matching categorie merceologiche articoli, or <code>null</code> if a matching categorie merceologiche articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CategorieMerceologicheArticoli fetchByCodiceCategoria(
		String codiceCategoria) throws SystemException {
		return fetchByCodiceCategoria(codiceCategoria, true);
	}

	/**
	 * Returns the categorie merceologiche articoli where codiceCategoria = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param codiceCategoria the codice categoria
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching categorie merceologiche articoli, or <code>null</code> if a matching categorie merceologiche articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CategorieMerceologicheArticoli fetchByCodiceCategoria(
		String codiceCategoria, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { codiceCategoria };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_CODICECATEGORIA,
					finderArgs, this);
		}

		if (result instanceof CategorieMerceologicheArticoli) {
			CategorieMerceologicheArticoli categorieMerceologicheArticoli = (CategorieMerceologicheArticoli)result;

			if (!Validator.equals(codiceCategoria,
						categorieMerceologicheArticoli.getCodiceCategoria())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_CATEGORIEMERCEOLOGICHEARTICOLI_WHERE);

			boolean bindCodiceCategoria = false;

			if (codiceCategoria == null) {
				query.append(_FINDER_COLUMN_CODICECATEGORIA_CODICECATEGORIA_1);
			}
			else if (codiceCategoria.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CODICECATEGORIA_CODICECATEGORIA_3);
			}
			else {
				bindCodiceCategoria = true;

				query.append(_FINDER_COLUMN_CODICECATEGORIA_CODICECATEGORIA_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceCategoria) {
					qPos.add(codiceCategoria);
				}

				List<CategorieMerceologicheArticoli> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODICECATEGORIA,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"CategorieMerceologicheArticoliPersistenceImpl.fetchByCodiceCategoria(String, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					CategorieMerceologicheArticoli categorieMerceologicheArticoli =
						list.get(0);

					result = categorieMerceologicheArticoli;

					cacheResult(categorieMerceologicheArticoli);

					if ((categorieMerceologicheArticoli.getCodiceCategoria() == null) ||
							!categorieMerceologicheArticoli.getCodiceCategoria()
															   .equals(codiceCategoria)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODICECATEGORIA,
							finderArgs, categorieMerceologicheArticoli);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CODICECATEGORIA,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (CategorieMerceologicheArticoli)result;
		}
	}

	/**
	 * Removes the categorie merceologiche articoli where codiceCategoria = &#63; from the database.
	 *
	 * @param codiceCategoria the codice categoria
	 * @return the categorie merceologiche articoli that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CategorieMerceologicheArticoli removeByCodiceCategoria(
		String codiceCategoria)
		throws NoSuchCategorieMerceologicheArticoliException, SystemException {
		CategorieMerceologicheArticoli categorieMerceologicheArticoli = findByCodiceCategoria(codiceCategoria);

		return remove(categorieMerceologicheArticoli);
	}

	/**
	 * Returns the number of categorie merceologiche articolis where codiceCategoria = &#63;.
	 *
	 * @param codiceCategoria the codice categoria
	 * @return the number of matching categorie merceologiche articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCodiceCategoria(String codiceCategoria)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICECATEGORIA;

		Object[] finderArgs = new Object[] { codiceCategoria };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CATEGORIEMERCEOLOGICHEARTICOLI_WHERE);

			boolean bindCodiceCategoria = false;

			if (codiceCategoria == null) {
				query.append(_FINDER_COLUMN_CODICECATEGORIA_CODICECATEGORIA_1);
			}
			else if (codiceCategoria.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CODICECATEGORIA_CODICECATEGORIA_3);
			}
			else {
				bindCodiceCategoria = true;

				query.append(_FINDER_COLUMN_CODICECATEGORIA_CODICECATEGORIA_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceCategoria) {
					qPos.add(codiceCategoria);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CODICECATEGORIA_CODICECATEGORIA_1 =
		"categorieMerceologicheArticoli.codiceCategoria IS NULL";
	private static final String _FINDER_COLUMN_CODICECATEGORIA_CODICECATEGORIA_2 =
		"categorieMerceologicheArticoli.codiceCategoria = ?";
	private static final String _FINDER_COLUMN_CODICECATEGORIA_CODICECATEGORIA_3 =
		"(categorieMerceologicheArticoli.codiceCategoria IS NULL OR categorieMerceologicheArticoli.codiceCategoria = '')";

	public CategorieMerceologicheArticoliPersistenceImpl() {
		setModelClass(CategorieMerceologicheArticoli.class);
	}

	/**
	 * Caches the categorie merceologiche articoli in the entity cache if it is enabled.
	 *
	 * @param categorieMerceologicheArticoli the categorie merceologiche articoli
	 */
	@Override
	public void cacheResult(
		CategorieMerceologicheArticoli categorieMerceologicheArticoli) {
		EntityCacheUtil.putResult(CategorieMerceologicheArticoliModelImpl.ENTITY_CACHE_ENABLED,
			CategorieMerceologicheArticoliImpl.class,
			categorieMerceologicheArticoli.getPrimaryKey(),
			categorieMerceologicheArticoli);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODICECATEGORIA,
			new Object[] { categorieMerceologicheArticoli.getCodiceCategoria() },
			categorieMerceologicheArticoli);

		categorieMerceologicheArticoli.resetOriginalValues();
	}

	/**
	 * Caches the categorie merceologiche articolis in the entity cache if it is enabled.
	 *
	 * @param categorieMerceologicheArticolis the categorie merceologiche articolis
	 */
	@Override
	public void cacheResult(
		List<CategorieMerceologicheArticoli> categorieMerceologicheArticolis) {
		for (CategorieMerceologicheArticoli categorieMerceologicheArticoli : categorieMerceologicheArticolis) {
			if (EntityCacheUtil.getResult(
						CategorieMerceologicheArticoliModelImpl.ENTITY_CACHE_ENABLED,
						CategorieMerceologicheArticoliImpl.class,
						categorieMerceologicheArticoli.getPrimaryKey()) == null) {
				cacheResult(categorieMerceologicheArticoli);
			}
			else {
				categorieMerceologicheArticoli.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all categorie merceologiche articolis.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CategorieMerceologicheArticoliImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CategorieMerceologicheArticoliImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the categorie merceologiche articoli.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(
		CategorieMerceologicheArticoli categorieMerceologicheArticoli) {
		EntityCacheUtil.removeResult(CategorieMerceologicheArticoliModelImpl.ENTITY_CACHE_ENABLED,
			CategorieMerceologicheArticoliImpl.class,
			categorieMerceologicheArticoli.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(categorieMerceologicheArticoli);
	}

	@Override
	public void clearCache(
		List<CategorieMerceologicheArticoli> categorieMerceologicheArticolis) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CategorieMerceologicheArticoli categorieMerceologicheArticoli : categorieMerceologicheArticolis) {
			EntityCacheUtil.removeResult(CategorieMerceologicheArticoliModelImpl.ENTITY_CACHE_ENABLED,
				CategorieMerceologicheArticoliImpl.class,
				categorieMerceologicheArticoli.getPrimaryKey());

			clearUniqueFindersCache(categorieMerceologicheArticoli);
		}
	}

	protected void cacheUniqueFindersCache(
		CategorieMerceologicheArticoli categorieMerceologicheArticoli) {
		if (categorieMerceologicheArticoli.isNew()) {
			Object[] args = new Object[] {
					categorieMerceologicheArticoli.getCodiceCategoria()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CODICECATEGORIA,
				args, Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODICECATEGORIA,
				args, categorieMerceologicheArticoli);
		}
		else {
			CategorieMerceologicheArticoliModelImpl categorieMerceologicheArticoliModelImpl =
				(CategorieMerceologicheArticoliModelImpl)categorieMerceologicheArticoli;

			if ((categorieMerceologicheArticoliModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_CODICECATEGORIA.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						categorieMerceologicheArticoli.getCodiceCategoria()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CODICECATEGORIA,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODICECATEGORIA,
					args, categorieMerceologicheArticoli);
			}
		}
	}

	protected void clearUniqueFindersCache(
		CategorieMerceologicheArticoli categorieMerceologicheArticoli) {
		CategorieMerceologicheArticoliModelImpl categorieMerceologicheArticoliModelImpl =
			(CategorieMerceologicheArticoliModelImpl)categorieMerceologicheArticoli;

		Object[] args = new Object[] {
				categorieMerceologicheArticoli.getCodiceCategoria()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICECATEGORIA, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CODICECATEGORIA, args);

		if ((categorieMerceologicheArticoliModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_CODICECATEGORIA.getColumnBitmask()) != 0) {
			args = new Object[] {
					categorieMerceologicheArticoliModelImpl.getOriginalCodiceCategoria()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICECATEGORIA,
				args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CODICECATEGORIA,
				args);
		}
	}

	/**
	 * Creates a new categorie merceologiche articoli with the primary key. Does not add the categorie merceologiche articoli to the database.
	 *
	 * @param codiceCategoria the primary key for the new categorie merceologiche articoli
	 * @return the new categorie merceologiche articoli
	 */
	@Override
	public CategorieMerceologicheArticoli create(String codiceCategoria) {
		CategorieMerceologicheArticoli categorieMerceologicheArticoli = new CategorieMerceologicheArticoliImpl();

		categorieMerceologicheArticoli.setNew(true);
		categorieMerceologicheArticoli.setPrimaryKey(codiceCategoria);

		return categorieMerceologicheArticoli;
	}

	/**
	 * Removes the categorie merceologiche articoli with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param codiceCategoria the primary key of the categorie merceologiche articoli
	 * @return the categorie merceologiche articoli that was removed
	 * @throws it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException if a categorie merceologiche articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CategorieMerceologicheArticoli remove(String codiceCategoria)
		throws NoSuchCategorieMerceologicheArticoliException, SystemException {
		return remove((Serializable)codiceCategoria);
	}

	/**
	 * Removes the categorie merceologiche articoli with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the categorie merceologiche articoli
	 * @return the categorie merceologiche articoli that was removed
	 * @throws it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException if a categorie merceologiche articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CategorieMerceologicheArticoli remove(Serializable primaryKey)
		throws NoSuchCategorieMerceologicheArticoliException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CategorieMerceologicheArticoli categorieMerceologicheArticoli = (CategorieMerceologicheArticoli)session.get(CategorieMerceologicheArticoliImpl.class,
					primaryKey);

			if (categorieMerceologicheArticoli == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCategorieMerceologicheArticoliException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(categorieMerceologicheArticoli);
		}
		catch (NoSuchCategorieMerceologicheArticoliException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CategorieMerceologicheArticoli removeImpl(
		CategorieMerceologicheArticoli categorieMerceologicheArticoli)
		throws SystemException {
		categorieMerceologicheArticoli = toUnwrappedModel(categorieMerceologicheArticoli);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(categorieMerceologicheArticoli)) {
				categorieMerceologicheArticoli = (CategorieMerceologicheArticoli)session.get(CategorieMerceologicheArticoliImpl.class,
						categorieMerceologicheArticoli.getPrimaryKeyObj());
			}

			if (categorieMerceologicheArticoli != null) {
				session.delete(categorieMerceologicheArticoli);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (categorieMerceologicheArticoli != null) {
			clearCache(categorieMerceologicheArticoli);
		}

		return categorieMerceologicheArticoli;
	}

	@Override
	public CategorieMerceologicheArticoli updateImpl(
		it.bysoftware.ct.model.CategorieMerceologicheArticoli categorieMerceologicheArticoli)
		throws SystemException {
		categorieMerceologicheArticoli = toUnwrappedModel(categorieMerceologicheArticoli);

		boolean isNew = categorieMerceologicheArticoli.isNew();

		Session session = null;

		try {
			session = openSession();

			if (categorieMerceologicheArticoli.isNew()) {
				session.save(categorieMerceologicheArticoli);

				categorieMerceologicheArticoli.setNew(false);
			}
			else {
				session.merge(categorieMerceologicheArticoli);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew ||
				!CategorieMerceologicheArticoliModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(CategorieMerceologicheArticoliModelImpl.ENTITY_CACHE_ENABLED,
			CategorieMerceologicheArticoliImpl.class,
			categorieMerceologicheArticoli.getPrimaryKey(),
			categorieMerceologicheArticoli);

		clearUniqueFindersCache(categorieMerceologicheArticoli);
		cacheUniqueFindersCache(categorieMerceologicheArticoli);

		return categorieMerceologicheArticoli;
	}

	protected CategorieMerceologicheArticoli toUnwrappedModel(
		CategorieMerceologicheArticoli categorieMerceologicheArticoli) {
		if (categorieMerceologicheArticoli instanceof CategorieMerceologicheArticoliImpl) {
			return categorieMerceologicheArticoli;
		}

		CategorieMerceologicheArticoliImpl categorieMerceologicheArticoliImpl = new CategorieMerceologicheArticoliImpl();

		categorieMerceologicheArticoliImpl.setNew(categorieMerceologicheArticoli.isNew());
		categorieMerceologicheArticoliImpl.setPrimaryKey(categorieMerceologicheArticoli.getPrimaryKey());

		categorieMerceologicheArticoliImpl.setCodiceCategoria(categorieMerceologicheArticoli.getCodiceCategoria());
		categorieMerceologicheArticoliImpl.setDescrizione(categorieMerceologicheArticoli.getDescrizione());

		return categorieMerceologicheArticoliImpl;
	}

	/**
	 * Returns the categorie merceologiche articoli with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the categorie merceologiche articoli
	 * @return the categorie merceologiche articoli
	 * @throws it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException if a categorie merceologiche articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CategorieMerceologicheArticoli findByPrimaryKey(
		Serializable primaryKey)
		throws NoSuchCategorieMerceologicheArticoliException, SystemException {
		CategorieMerceologicheArticoli categorieMerceologicheArticoli = fetchByPrimaryKey(primaryKey);

		if (categorieMerceologicheArticoli == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCategorieMerceologicheArticoliException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return categorieMerceologicheArticoli;
	}

	/**
	 * Returns the categorie merceologiche articoli with the primary key or throws a {@link it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException} if it could not be found.
	 *
	 * @param codiceCategoria the primary key of the categorie merceologiche articoli
	 * @return the categorie merceologiche articoli
	 * @throws it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException if a categorie merceologiche articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CategorieMerceologicheArticoli findByPrimaryKey(
		String codiceCategoria)
		throws NoSuchCategorieMerceologicheArticoliException, SystemException {
		return findByPrimaryKey((Serializable)codiceCategoria);
	}

	/**
	 * Returns the categorie merceologiche articoli with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the categorie merceologiche articoli
	 * @return the categorie merceologiche articoli, or <code>null</code> if a categorie merceologiche articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CategorieMerceologicheArticoli fetchByPrimaryKey(
		Serializable primaryKey) throws SystemException {
		CategorieMerceologicheArticoli categorieMerceologicheArticoli = (CategorieMerceologicheArticoli)EntityCacheUtil.getResult(CategorieMerceologicheArticoliModelImpl.ENTITY_CACHE_ENABLED,
				CategorieMerceologicheArticoliImpl.class, primaryKey);

		if (categorieMerceologicheArticoli == _nullCategorieMerceologicheArticoli) {
			return null;
		}

		if (categorieMerceologicheArticoli == null) {
			Session session = null;

			try {
				session = openSession();

				categorieMerceologicheArticoli = (CategorieMerceologicheArticoli)session.get(CategorieMerceologicheArticoliImpl.class,
						primaryKey);

				if (categorieMerceologicheArticoli != null) {
					cacheResult(categorieMerceologicheArticoli);
				}
				else {
					EntityCacheUtil.putResult(CategorieMerceologicheArticoliModelImpl.ENTITY_CACHE_ENABLED,
						CategorieMerceologicheArticoliImpl.class, primaryKey,
						_nullCategorieMerceologicheArticoli);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CategorieMerceologicheArticoliModelImpl.ENTITY_CACHE_ENABLED,
					CategorieMerceologicheArticoliImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return categorieMerceologicheArticoli;
	}

	/**
	 * Returns the categorie merceologiche articoli with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param codiceCategoria the primary key of the categorie merceologiche articoli
	 * @return the categorie merceologiche articoli, or <code>null</code> if a categorie merceologiche articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CategorieMerceologicheArticoli fetchByPrimaryKey(
		String codiceCategoria) throws SystemException {
		return fetchByPrimaryKey((Serializable)codiceCategoria);
	}

	/**
	 * Returns all the categorie merceologiche articolis.
	 *
	 * @return the categorie merceologiche articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CategorieMerceologicheArticoli> findAll()
		throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the categorie merceologiche articolis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieMerceologicheArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of categorie merceologiche articolis
	 * @param end the upper bound of the range of categorie merceologiche articolis (not inclusive)
	 * @return the range of categorie merceologiche articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CategorieMerceologicheArticoli> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the categorie merceologiche articolis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieMerceologicheArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of categorie merceologiche articolis
	 * @param end the upper bound of the range of categorie merceologiche articolis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of categorie merceologiche articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CategorieMerceologicheArticoli> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CategorieMerceologicheArticoli> list = (List<CategorieMerceologicheArticoli>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CATEGORIEMERCEOLOGICHEARTICOLI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CATEGORIEMERCEOLOGICHEARTICOLI;

				if (pagination) {
					sql = sql.concat(CategorieMerceologicheArticoliModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CategorieMerceologicheArticoli>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CategorieMerceologicheArticoli>(list);
				}
				else {
					list = (List<CategorieMerceologicheArticoli>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the categorie merceologiche articolis from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CategorieMerceologicheArticoli categorieMerceologicheArticoli : findAll()) {
			remove(categorieMerceologicheArticoli);
		}
	}

	/**
	 * Returns the number of categorie merceologiche articolis.
	 *
	 * @return the number of categorie merceologiche articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CATEGORIEMERCEOLOGICHEARTICOLI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the categorie merceologiche articoli persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.CategorieMerceologicheArticoli")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CategorieMerceologicheArticoli>> listenersList =
					new ArrayList<ModelListener<CategorieMerceologicheArticoli>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CategorieMerceologicheArticoli>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CategorieMerceologicheArticoliImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CATEGORIEMERCEOLOGICHEARTICOLI = "SELECT categorieMerceologicheArticoli FROM CategorieMerceologicheArticoli categorieMerceologicheArticoli";
	private static final String _SQL_SELECT_CATEGORIEMERCEOLOGICHEARTICOLI_WHERE =
		"SELECT categorieMerceologicheArticoli FROM CategorieMerceologicheArticoli categorieMerceologicheArticoli WHERE ";
	private static final String _SQL_COUNT_CATEGORIEMERCEOLOGICHEARTICOLI = "SELECT COUNT(categorieMerceologicheArticoli) FROM CategorieMerceologicheArticoli categorieMerceologicheArticoli";
	private static final String _SQL_COUNT_CATEGORIEMERCEOLOGICHEARTICOLI_WHERE = "SELECT COUNT(categorieMerceologicheArticoli) FROM CategorieMerceologicheArticoli categorieMerceologicheArticoli WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "categorieMerceologicheArticoli.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CategorieMerceologicheArticoli exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CategorieMerceologicheArticoli exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CategorieMerceologicheArticoliPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"codiceCategoria", "descrizione"
			});
	private static CategorieMerceologicheArticoli _nullCategorieMerceologicheArticoli =
		new CategorieMerceologicheArticoliImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CategorieMerceologicheArticoli> toCacheModel() {
				return _nullCategorieMerceologicheArticoliCacheModel;
			}
		};

	private static CacheModel<CategorieMerceologicheArticoli> _nullCategorieMerceologicheArticoliCacheModel =
		new CacheModel<CategorieMerceologicheArticoli>() {
			@Override
			public CategorieMerceologicheArticoli toEntityModel() {
				return _nullCategorieMerceologicheArticoli;
			}
		};
}