/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchRigheFattureVeditaException;
import it.bysoftware.ct.model.RigheFattureVedita;
import it.bysoftware.ct.model.impl.RigheFattureVeditaImpl;
import it.bysoftware.ct.model.impl.RigheFattureVeditaModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the righe fatture vedita service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see RigheFattureVeditaPersistence
 * @see RigheFattureVeditaUtil
 * @generated
 */
public class RigheFattureVeditaPersistenceImpl extends BasePersistenceImpl<RigheFattureVedita>
	implements RigheFattureVeditaPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link RigheFattureVeditaUtil} to access the righe fatture vedita persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = RigheFattureVeditaImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(RigheFattureVeditaModelImpl.ENTITY_CACHE_ENABLED,
			RigheFattureVeditaModelImpl.FINDER_CACHE_ENABLED,
			RigheFattureVeditaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(RigheFattureVeditaModelImpl.ENTITY_CACHE_ENABLED,
			RigheFattureVeditaModelImpl.FINDER_CACHE_ENABLED,
			RigheFattureVeditaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(RigheFattureVeditaModelImpl.ENTITY_CACHE_ENABLED,
			RigheFattureVeditaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TESTATAFATTURA =
		new FinderPath(RigheFattureVeditaModelImpl.ENTITY_CACHE_ENABLED,
			RigheFattureVeditaModelImpl.FINDER_CACHE_ENABLED,
			RigheFattureVeditaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTestataFattura",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				String.class.getName(), Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TESTATAFATTURA =
		new FinderPath(RigheFattureVeditaModelImpl.ENTITY_CACHE_ENABLED,
			RigheFattureVeditaModelImpl.FINDER_CACHE_ENABLED,
			RigheFattureVeditaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTestataFattura",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				String.class.getName(), Integer.class.getName()
			},
			RigheFattureVeditaModelImpl.ANNO_COLUMN_BITMASK |
			RigheFattureVeditaModelImpl.CODICEATTIVITA_COLUMN_BITMASK |
			RigheFattureVeditaModelImpl.CODICECENTRO_COLUMN_BITMASK |
			RigheFattureVeditaModelImpl.NUMEROPROTOCOLLO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TESTATAFATTURA = new FinderPath(RigheFattureVeditaModelImpl.ENTITY_CACHE_ENABLED,
			RigheFattureVeditaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTestataFattura",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				String.class.getName(), Integer.class.getName()
			});

	/**
	 * Returns all the righe fatture veditas where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param numeroProtocollo the numero protocollo
	 * @return the matching righe fatture veditas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<RigheFattureVedita> findByTestataFattura(int anno,
		String codiceAttivita, String codiceCentro, int numeroProtocollo)
		throws SystemException {
		return findByTestataFattura(anno, codiceAttivita, codiceCentro,
			numeroProtocollo, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the righe fatture veditas where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheFattureVeditaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param numeroProtocollo the numero protocollo
	 * @param start the lower bound of the range of righe fatture veditas
	 * @param end the upper bound of the range of righe fatture veditas (not inclusive)
	 * @return the range of matching righe fatture veditas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<RigheFattureVedita> findByTestataFattura(int anno,
		String codiceAttivita, String codiceCentro, int numeroProtocollo,
		int start, int end) throws SystemException {
		return findByTestataFattura(anno, codiceAttivita, codiceCentro,
			numeroProtocollo, start, end, null);
	}

	/**
	 * Returns an ordered range of all the righe fatture veditas where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheFattureVeditaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param numeroProtocollo the numero protocollo
	 * @param start the lower bound of the range of righe fatture veditas
	 * @param end the upper bound of the range of righe fatture veditas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching righe fatture veditas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<RigheFattureVedita> findByTestataFattura(int anno,
		String codiceAttivita, String codiceCentro, int numeroProtocollo,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TESTATAFATTURA;
			finderArgs = new Object[] {
					anno, codiceAttivita, codiceCentro, numeroProtocollo
				};
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TESTATAFATTURA;
			finderArgs = new Object[] {
					anno, codiceAttivita, codiceCentro, numeroProtocollo,
					
					start, end, orderByComparator
				};
		}

		List<RigheFattureVedita> list = (List<RigheFattureVedita>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (RigheFattureVedita righeFattureVedita : list) {
				if ((anno != righeFattureVedita.getAnno()) ||
						!Validator.equals(codiceAttivita,
							righeFattureVedita.getCodiceAttivita()) ||
						!Validator.equals(codiceCentro,
							righeFattureVedita.getCodiceCentro()) ||
						(numeroProtocollo != righeFattureVedita.getNumeroProtocollo())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(6 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(6);
			}

			query.append(_SQL_SELECT_RIGHEFATTUREVEDITA_WHERE);

			query.append(_FINDER_COLUMN_TESTATAFATTURA_ANNO_2);

			boolean bindCodiceAttivita = false;

			if (codiceAttivita == null) {
				query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICEATTIVITA_1);
			}
			else if (codiceAttivita.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICEATTIVITA_3);
			}
			else {
				bindCodiceAttivita = true;

				query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICEATTIVITA_2);
			}

			boolean bindCodiceCentro = false;

			if (codiceCentro == null) {
				query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICECENTRO_1);
			}
			else if (codiceCentro.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICECENTRO_3);
			}
			else {
				bindCodiceCentro = true;

				query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICECENTRO_2);
			}

			query.append(_FINDER_COLUMN_TESTATAFATTURA_NUMEROPROTOCOLLO_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(RigheFattureVeditaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(anno);

				if (bindCodiceAttivita) {
					qPos.add(codiceAttivita);
				}

				if (bindCodiceCentro) {
					qPos.add(codiceCentro);
				}

				qPos.add(numeroProtocollo);

				if (!pagination) {
					list = (List<RigheFattureVedita>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<RigheFattureVedita>(list);
				}
				else {
					list = (List<RigheFattureVedita>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first righe fatture vedita in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param numeroProtocollo the numero protocollo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching righe fatture vedita
	 * @throws it.bysoftware.ct.NoSuchRigheFattureVeditaException if a matching righe fatture vedita could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheFattureVedita findByTestataFattura_First(int anno,
		String codiceAttivita, String codiceCentro, int numeroProtocollo,
		OrderByComparator orderByComparator)
		throws NoSuchRigheFattureVeditaException, SystemException {
		RigheFattureVedita righeFattureVedita = fetchByTestataFattura_First(anno,
				codiceAttivita, codiceCentro, numeroProtocollo,
				orderByComparator);

		if (righeFattureVedita != null) {
			return righeFattureVedita;
		}

		StringBundler msg = new StringBundler(10);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("anno=");
		msg.append(anno);

		msg.append(", codiceAttivita=");
		msg.append(codiceAttivita);

		msg.append(", codiceCentro=");
		msg.append(codiceCentro);

		msg.append(", numeroProtocollo=");
		msg.append(numeroProtocollo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchRigheFattureVeditaException(msg.toString());
	}

	/**
	 * Returns the first righe fatture vedita in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param numeroProtocollo the numero protocollo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching righe fatture vedita, or <code>null</code> if a matching righe fatture vedita could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheFattureVedita fetchByTestataFattura_First(int anno,
		String codiceAttivita, String codiceCentro, int numeroProtocollo,
		OrderByComparator orderByComparator) throws SystemException {
		List<RigheFattureVedita> list = findByTestataFattura(anno,
				codiceAttivita, codiceCentro, numeroProtocollo, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last righe fatture vedita in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param numeroProtocollo the numero protocollo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching righe fatture vedita
	 * @throws it.bysoftware.ct.NoSuchRigheFattureVeditaException if a matching righe fatture vedita could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheFattureVedita findByTestataFattura_Last(int anno,
		String codiceAttivita, String codiceCentro, int numeroProtocollo,
		OrderByComparator orderByComparator)
		throws NoSuchRigheFattureVeditaException, SystemException {
		RigheFattureVedita righeFattureVedita = fetchByTestataFattura_Last(anno,
				codiceAttivita, codiceCentro, numeroProtocollo,
				orderByComparator);

		if (righeFattureVedita != null) {
			return righeFattureVedita;
		}

		StringBundler msg = new StringBundler(10);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("anno=");
		msg.append(anno);

		msg.append(", codiceAttivita=");
		msg.append(codiceAttivita);

		msg.append(", codiceCentro=");
		msg.append(codiceCentro);

		msg.append(", numeroProtocollo=");
		msg.append(numeroProtocollo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchRigheFattureVeditaException(msg.toString());
	}

	/**
	 * Returns the last righe fatture vedita in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param numeroProtocollo the numero protocollo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching righe fatture vedita, or <code>null</code> if a matching righe fatture vedita could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheFattureVedita fetchByTestataFattura_Last(int anno,
		String codiceAttivita, String codiceCentro, int numeroProtocollo,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByTestataFattura(anno, codiceAttivita, codiceCentro,
				numeroProtocollo);

		if (count == 0) {
			return null;
		}

		List<RigheFattureVedita> list = findByTestataFattura(anno,
				codiceAttivita, codiceCentro, numeroProtocollo, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the righe fatture veditas before and after the current righe fatture vedita in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	 *
	 * @param righeFattureVeditaPK the primary key of the current righe fatture vedita
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param numeroProtocollo the numero protocollo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next righe fatture vedita
	 * @throws it.bysoftware.ct.NoSuchRigheFattureVeditaException if a righe fatture vedita with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheFattureVedita[] findByTestataFattura_PrevAndNext(
		RigheFattureVeditaPK righeFattureVeditaPK, int anno,
		String codiceAttivita, String codiceCentro, int numeroProtocollo,
		OrderByComparator orderByComparator)
		throws NoSuchRigheFattureVeditaException, SystemException {
		RigheFattureVedita righeFattureVedita = findByPrimaryKey(righeFattureVeditaPK);

		Session session = null;

		try {
			session = openSession();

			RigheFattureVedita[] array = new RigheFattureVeditaImpl[3];

			array[0] = getByTestataFattura_PrevAndNext(session,
					righeFattureVedita, anno, codiceAttivita, codiceCentro,
					numeroProtocollo, orderByComparator, true);

			array[1] = righeFattureVedita;

			array[2] = getByTestataFattura_PrevAndNext(session,
					righeFattureVedita, anno, codiceAttivita, codiceCentro,
					numeroProtocollo, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected RigheFattureVedita getByTestataFattura_PrevAndNext(
		Session session, RigheFattureVedita righeFattureVedita, int anno,
		String codiceAttivita, String codiceCentro, int numeroProtocollo,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_RIGHEFATTUREVEDITA_WHERE);

		query.append(_FINDER_COLUMN_TESTATAFATTURA_ANNO_2);

		boolean bindCodiceAttivita = false;

		if (codiceAttivita == null) {
			query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICEATTIVITA_1);
		}
		else if (codiceAttivita.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICEATTIVITA_3);
		}
		else {
			bindCodiceAttivita = true;

			query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICEATTIVITA_2);
		}

		boolean bindCodiceCentro = false;

		if (codiceCentro == null) {
			query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICECENTRO_1);
		}
		else if (codiceCentro.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICECENTRO_3);
		}
		else {
			bindCodiceCentro = true;

			query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICECENTRO_2);
		}

		query.append(_FINDER_COLUMN_TESTATAFATTURA_NUMEROPROTOCOLLO_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(RigheFattureVeditaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(anno);

		if (bindCodiceAttivita) {
			qPos.add(codiceAttivita);
		}

		if (bindCodiceCentro) {
			qPos.add(codiceCentro);
		}

		qPos.add(numeroProtocollo);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(righeFattureVedita);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<RigheFattureVedita> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the righe fatture veditas where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; from the database.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param numeroProtocollo the numero protocollo
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByTestataFattura(int anno, String codiceAttivita,
		String codiceCentro, int numeroProtocollo) throws SystemException {
		for (RigheFattureVedita righeFattureVedita : findByTestataFattura(
				anno, codiceAttivita, codiceCentro, numeroProtocollo,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(righeFattureVedita);
		}
	}

	/**
	 * Returns the number of righe fatture veditas where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param numeroProtocollo the numero protocollo
	 * @return the number of matching righe fatture veditas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByTestataFattura(int anno, String codiceAttivita,
		String codiceCentro, int numeroProtocollo) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_TESTATAFATTURA;

		Object[] finderArgs = new Object[] {
				anno, codiceAttivita, codiceCentro, numeroProtocollo
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(5);

			query.append(_SQL_COUNT_RIGHEFATTUREVEDITA_WHERE);

			query.append(_FINDER_COLUMN_TESTATAFATTURA_ANNO_2);

			boolean bindCodiceAttivita = false;

			if (codiceAttivita == null) {
				query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICEATTIVITA_1);
			}
			else if (codiceAttivita.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICEATTIVITA_3);
			}
			else {
				bindCodiceAttivita = true;

				query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICEATTIVITA_2);
			}

			boolean bindCodiceCentro = false;

			if (codiceCentro == null) {
				query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICECENTRO_1);
			}
			else if (codiceCentro.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICECENTRO_3);
			}
			else {
				bindCodiceCentro = true;

				query.append(_FINDER_COLUMN_TESTATAFATTURA_CODICECENTRO_2);
			}

			query.append(_FINDER_COLUMN_TESTATAFATTURA_NUMEROPROTOCOLLO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(anno);

				if (bindCodiceAttivita) {
					qPos.add(codiceAttivita);
				}

				if (bindCodiceCentro) {
					qPos.add(codiceCentro);
				}

				qPos.add(numeroProtocollo);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_TESTATAFATTURA_ANNO_2 = "righeFattureVedita.id.anno = ? AND ";
	private static final String _FINDER_COLUMN_TESTATAFATTURA_CODICEATTIVITA_1 = "righeFattureVedita.id.codiceAttivita IS NULL AND ";
	private static final String _FINDER_COLUMN_TESTATAFATTURA_CODICEATTIVITA_2 = "righeFattureVedita.id.codiceAttivita = ? AND ";
	private static final String _FINDER_COLUMN_TESTATAFATTURA_CODICEATTIVITA_3 = "(righeFattureVedita.id.codiceAttivita IS NULL OR righeFattureVedita.id.codiceAttivita = '') AND ";
	private static final String _FINDER_COLUMN_TESTATAFATTURA_CODICECENTRO_1 = "righeFattureVedita.id.codiceCentro IS NULL AND ";
	private static final String _FINDER_COLUMN_TESTATAFATTURA_CODICECENTRO_2 = "righeFattureVedita.id.codiceCentro = ? AND ";
	private static final String _FINDER_COLUMN_TESTATAFATTURA_CODICECENTRO_3 = "(righeFattureVedita.id.codiceCentro IS NULL OR righeFattureVedita.id.codiceCentro = '') AND ";
	private static final String _FINDER_COLUMN_TESTATAFATTURA_NUMEROPROTOCOLLO_2 =
		"righeFattureVedita.id.numeroProtocollo = ?";

	public RigheFattureVeditaPersistenceImpl() {
		setModelClass(RigheFattureVedita.class);
	}

	/**
	 * Caches the righe fatture vedita in the entity cache if it is enabled.
	 *
	 * @param righeFattureVedita the righe fatture vedita
	 */
	@Override
	public void cacheResult(RigheFattureVedita righeFattureVedita) {
		EntityCacheUtil.putResult(RigheFattureVeditaModelImpl.ENTITY_CACHE_ENABLED,
			RigheFattureVeditaImpl.class, righeFattureVedita.getPrimaryKey(),
			righeFattureVedita);

		righeFattureVedita.resetOriginalValues();
	}

	/**
	 * Caches the righe fatture veditas in the entity cache if it is enabled.
	 *
	 * @param righeFattureVeditas the righe fatture veditas
	 */
	@Override
	public void cacheResult(List<RigheFattureVedita> righeFattureVeditas) {
		for (RigheFattureVedita righeFattureVedita : righeFattureVeditas) {
			if (EntityCacheUtil.getResult(
						RigheFattureVeditaModelImpl.ENTITY_CACHE_ENABLED,
						RigheFattureVeditaImpl.class,
						righeFattureVedita.getPrimaryKey()) == null) {
				cacheResult(righeFattureVedita);
			}
			else {
				righeFattureVedita.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all righe fatture veditas.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(RigheFattureVeditaImpl.class.getName());
		}

		EntityCacheUtil.clearCache(RigheFattureVeditaImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the righe fatture vedita.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(RigheFattureVedita righeFattureVedita) {
		EntityCacheUtil.removeResult(RigheFattureVeditaModelImpl.ENTITY_CACHE_ENABLED,
			RigheFattureVeditaImpl.class, righeFattureVedita.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<RigheFattureVedita> righeFattureVeditas) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (RigheFattureVedita righeFattureVedita : righeFattureVeditas) {
			EntityCacheUtil.removeResult(RigheFattureVeditaModelImpl.ENTITY_CACHE_ENABLED,
				RigheFattureVeditaImpl.class, righeFattureVedita.getPrimaryKey());
		}
	}

	/**
	 * Creates a new righe fatture vedita with the primary key. Does not add the righe fatture vedita to the database.
	 *
	 * @param righeFattureVeditaPK the primary key for the new righe fatture vedita
	 * @return the new righe fatture vedita
	 */
	@Override
	public RigheFattureVedita create(RigheFattureVeditaPK righeFattureVeditaPK) {
		RigheFattureVedita righeFattureVedita = new RigheFattureVeditaImpl();

		righeFattureVedita.setNew(true);
		righeFattureVedita.setPrimaryKey(righeFattureVeditaPK);

		return righeFattureVedita;
	}

	/**
	 * Removes the righe fatture vedita with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param righeFattureVeditaPK the primary key of the righe fatture vedita
	 * @return the righe fatture vedita that was removed
	 * @throws it.bysoftware.ct.NoSuchRigheFattureVeditaException if a righe fatture vedita with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheFattureVedita remove(RigheFattureVeditaPK righeFattureVeditaPK)
		throws NoSuchRigheFattureVeditaException, SystemException {
		return remove((Serializable)righeFattureVeditaPK);
	}

	/**
	 * Removes the righe fatture vedita with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the righe fatture vedita
	 * @return the righe fatture vedita that was removed
	 * @throws it.bysoftware.ct.NoSuchRigheFattureVeditaException if a righe fatture vedita with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheFattureVedita remove(Serializable primaryKey)
		throws NoSuchRigheFattureVeditaException, SystemException {
		Session session = null;

		try {
			session = openSession();

			RigheFattureVedita righeFattureVedita = (RigheFattureVedita)session.get(RigheFattureVeditaImpl.class,
					primaryKey);

			if (righeFattureVedita == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchRigheFattureVeditaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(righeFattureVedita);
		}
		catch (NoSuchRigheFattureVeditaException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected RigheFattureVedita removeImpl(
		RigheFattureVedita righeFattureVedita) throws SystemException {
		righeFattureVedita = toUnwrappedModel(righeFattureVedita);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(righeFattureVedita)) {
				righeFattureVedita = (RigheFattureVedita)session.get(RigheFattureVeditaImpl.class,
						righeFattureVedita.getPrimaryKeyObj());
			}

			if (righeFattureVedita != null) {
				session.delete(righeFattureVedita);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (righeFattureVedita != null) {
			clearCache(righeFattureVedita);
		}

		return righeFattureVedita;
	}

	@Override
	public RigheFattureVedita updateImpl(
		it.bysoftware.ct.model.RigheFattureVedita righeFattureVedita)
		throws SystemException {
		righeFattureVedita = toUnwrappedModel(righeFattureVedita);

		boolean isNew = righeFattureVedita.isNew();

		RigheFattureVeditaModelImpl righeFattureVeditaModelImpl = (RigheFattureVeditaModelImpl)righeFattureVedita;

		Session session = null;

		try {
			session = openSession();

			if (righeFattureVedita.isNew()) {
				session.save(righeFattureVedita);

				righeFattureVedita.setNew(false);
			}
			else {
				session.merge(righeFattureVedita);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !RigheFattureVeditaModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((righeFattureVeditaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TESTATAFATTURA.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						righeFattureVeditaModelImpl.getOriginalAnno(),
						righeFattureVeditaModelImpl.getOriginalCodiceAttivita(),
						righeFattureVeditaModelImpl.getOriginalCodiceCentro(),
						righeFattureVeditaModelImpl.getOriginalNumeroProtocollo()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TESTATAFATTURA,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TESTATAFATTURA,
					args);

				args = new Object[] {
						righeFattureVeditaModelImpl.getAnno(),
						righeFattureVeditaModelImpl.getCodiceAttivita(),
						righeFattureVeditaModelImpl.getCodiceCentro(),
						righeFattureVeditaModelImpl.getNumeroProtocollo()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TESTATAFATTURA,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TESTATAFATTURA,
					args);
			}
		}

		EntityCacheUtil.putResult(RigheFattureVeditaModelImpl.ENTITY_CACHE_ENABLED,
			RigheFattureVeditaImpl.class, righeFattureVedita.getPrimaryKey(),
			righeFattureVedita);

		return righeFattureVedita;
	}

	protected RigheFattureVedita toUnwrappedModel(
		RigheFattureVedita righeFattureVedita) {
		if (righeFattureVedita instanceof RigheFattureVeditaImpl) {
			return righeFattureVedita;
		}

		RigheFattureVeditaImpl righeFattureVeditaImpl = new RigheFattureVeditaImpl();

		righeFattureVeditaImpl.setNew(righeFattureVedita.isNew());
		righeFattureVeditaImpl.setPrimaryKey(righeFattureVedita.getPrimaryKey());

		righeFattureVeditaImpl.setAnno(righeFattureVedita.getAnno());
		righeFattureVeditaImpl.setCodiceAttivita(righeFattureVedita.getCodiceAttivita());
		righeFattureVeditaImpl.setCodiceCentro(righeFattureVedita.getCodiceCentro());
		righeFattureVeditaImpl.setNumeroProtocollo(righeFattureVedita.getNumeroProtocollo());
		righeFattureVeditaImpl.setNumeroRigo(righeFattureVedita.getNumeroRigo());
		righeFattureVeditaImpl.setStatoRigo(righeFattureVedita.isStatoRigo());
		righeFattureVeditaImpl.setAnnoBolla(righeFattureVedita.getAnnoBolla());
		righeFattureVeditaImpl.setTipoIdentificativoBolla(righeFattureVedita.getTipoIdentificativoBolla());
		righeFattureVeditaImpl.setTipoBolla(righeFattureVedita.getTipoBolla());
		righeFattureVeditaImpl.setCodiceDeposito(righeFattureVedita.getCodiceDeposito());
		righeFattureVeditaImpl.setNumeroBollettario(righeFattureVedita.getNumeroBollettario());
		righeFattureVeditaImpl.setNumeroBolla(righeFattureVedita.getNumeroBolla());
		righeFattureVeditaImpl.setDataBollaEvasa(righeFattureVedita.getDataBollaEvasa());
		righeFattureVeditaImpl.setRigoDDT(righeFattureVedita.getRigoDDT());
		righeFattureVeditaImpl.setTipoRigoDDT(righeFattureVedita.getTipoRigoDDT());
		righeFattureVeditaImpl.setCodiceCausaleMagazzino(righeFattureVedita.getCodiceCausaleMagazzino());
		righeFattureVeditaImpl.setCodiceDepositoMagazzino(righeFattureVedita.getCodiceDepositoMagazzino());
		righeFattureVeditaImpl.setCodiceArticolo(righeFattureVedita.getCodiceArticolo());
		righeFattureVeditaImpl.setCodiceVariante(righeFattureVedita.getCodiceVariante());
		righeFattureVeditaImpl.setCodiceTestoDescrizioni(righeFattureVedita.getCodiceTestoDescrizioni());
		righeFattureVeditaImpl.setDescrizione(righeFattureVedita.getDescrizione());
		righeFattureVeditaImpl.setUnitaMisura(righeFattureVedita.getUnitaMisura());
		righeFattureVeditaImpl.setNumeroDecimalliQuant(righeFattureVedita.getNumeroDecimalliQuant());
		righeFattureVeditaImpl.setQuantita1(righeFattureVedita.getQuantita1());
		righeFattureVeditaImpl.setQuantita2(righeFattureVedita.getQuantita2());
		righeFattureVeditaImpl.setQuantita3(righeFattureVedita.getQuantita3());
		righeFattureVeditaImpl.setQuantita(righeFattureVedita.getQuantita());
		righeFattureVeditaImpl.setUnitaMisura2(righeFattureVedita.getUnitaMisura2());
		righeFattureVeditaImpl.setQuantitaUnitMisSec(righeFattureVedita.getQuantitaUnitMisSec());
		righeFattureVeditaImpl.setNumeroDecimalliPrezzo(righeFattureVedita.getNumeroDecimalliPrezzo());
		righeFattureVeditaImpl.setPrezzo(righeFattureVedita.getPrezzo());
		righeFattureVeditaImpl.setPrezzoIVA(righeFattureVedita.getPrezzoIVA());
		righeFattureVeditaImpl.setImportoLordo(righeFattureVedita.getImportoLordo());
		righeFattureVeditaImpl.setImportoLordoIVA(righeFattureVedita.getImportoLordoIVA());
		righeFattureVeditaImpl.setSconto1(righeFattureVedita.getSconto1());
		righeFattureVeditaImpl.setSconto2(righeFattureVedita.getSconto2());
		righeFattureVeditaImpl.setSconto3(righeFattureVedita.getSconto3());
		righeFattureVeditaImpl.setImportoNettoRigo(righeFattureVedita.getImportoNettoRigo());
		righeFattureVeditaImpl.setImportoNettoRigoIVA(righeFattureVedita.getImportoNettoRigoIVA());
		righeFattureVeditaImpl.setImportoNetto(righeFattureVedita.getImportoNetto());
		righeFattureVeditaImpl.setImportoNettoIVA(righeFattureVedita.getImportoNettoIVA());
		righeFattureVeditaImpl.setImportoCONAIAddebitatoInt(righeFattureVedita.getImportoCONAIAddebitatoInt());
		righeFattureVeditaImpl.setImportoCONAIAddebitatoOrig(righeFattureVedita.getImportoCONAIAddebitatoOrig());
		righeFattureVeditaImpl.setCodiceIVAFatturazione(righeFattureVedita.getCodiceIVAFatturazione());
		righeFattureVeditaImpl.setNomenclaturaCombinata(righeFattureVedita.getNomenclaturaCombinata());
		righeFattureVeditaImpl.setTestStampaDisBase(righeFattureVedita.isTestStampaDisBase());
		righeFattureVeditaImpl.setTipoOrdineEvaso(righeFattureVedita.getTipoOrdineEvaso());
		righeFattureVeditaImpl.setAnnoOrdineEvaso(righeFattureVedita.getAnnoOrdineEvaso());
		righeFattureVeditaImpl.setNumeroOrdineEvaso(righeFattureVedita.getNumeroOrdineEvaso());
		righeFattureVeditaImpl.setNumeroRigoOrdineEvaso(righeFattureVedita.getNumeroRigoOrdineEvaso());
		righeFattureVeditaImpl.setTipoEvasione(righeFattureVedita.getTipoEvasione());
		righeFattureVeditaImpl.setDescrizioneRiferimento(righeFattureVedita.getDescrizioneRiferimento());
		righeFattureVeditaImpl.setNumeroPrimaNotaMagazzino(righeFattureVedita.getNumeroPrimaNotaMagazzino());
		righeFattureVeditaImpl.setNumeroMovimentoMagazzino(righeFattureVedita.getNumeroMovimentoMagazzino());
		righeFattureVeditaImpl.setTipoEsplosione(righeFattureVedita.getTipoEsplosione());
		righeFattureVeditaImpl.setLivelloMassimoEsplosione(righeFattureVedita.getLivelloMassimoEsplosione());
		righeFattureVeditaImpl.setLibStr1(righeFattureVedita.getLibStr1());
		righeFattureVeditaImpl.setLibStr2(righeFattureVedita.getLibStr2());
		righeFattureVeditaImpl.setLibStr3(righeFattureVedita.getLibStr3());
		righeFattureVeditaImpl.setLibDbl1(righeFattureVedita.getLibDbl1());
		righeFattureVeditaImpl.setLibDbl2(righeFattureVedita.getLibDbl2());
		righeFattureVeditaImpl.setLibDbl3(righeFattureVedita.getLibDbl3());
		righeFattureVeditaImpl.setLibLng1(righeFattureVedita.getLibLng1());
		righeFattureVeditaImpl.setLibLng2(righeFattureVedita.getLibLng2());
		righeFattureVeditaImpl.setLibLng3(righeFattureVedita.getLibLng3());
		righeFattureVeditaImpl.setLibDat1(righeFattureVedita.getLibDat1());
		righeFattureVeditaImpl.setLibDat2(righeFattureVedita.getLibDat2());
		righeFattureVeditaImpl.setLibDat3(righeFattureVedita.getLibDat3());
		righeFattureVeditaImpl.setTestFatturazioneAuto(righeFattureVedita.isTestFatturazioneAuto());

		return righeFattureVeditaImpl;
	}

	/**
	 * Returns the righe fatture vedita with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the righe fatture vedita
	 * @return the righe fatture vedita
	 * @throws it.bysoftware.ct.NoSuchRigheFattureVeditaException if a righe fatture vedita with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheFattureVedita findByPrimaryKey(Serializable primaryKey)
		throws NoSuchRigheFattureVeditaException, SystemException {
		RigheFattureVedita righeFattureVedita = fetchByPrimaryKey(primaryKey);

		if (righeFattureVedita == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchRigheFattureVeditaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return righeFattureVedita;
	}

	/**
	 * Returns the righe fatture vedita with the primary key or throws a {@link it.bysoftware.ct.NoSuchRigheFattureVeditaException} if it could not be found.
	 *
	 * @param righeFattureVeditaPK the primary key of the righe fatture vedita
	 * @return the righe fatture vedita
	 * @throws it.bysoftware.ct.NoSuchRigheFattureVeditaException if a righe fatture vedita with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheFattureVedita findByPrimaryKey(
		RigheFattureVeditaPK righeFattureVeditaPK)
		throws NoSuchRigheFattureVeditaException, SystemException {
		return findByPrimaryKey((Serializable)righeFattureVeditaPK);
	}

	/**
	 * Returns the righe fatture vedita with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the righe fatture vedita
	 * @return the righe fatture vedita, or <code>null</code> if a righe fatture vedita with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheFattureVedita fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		RigheFattureVedita righeFattureVedita = (RigheFattureVedita)EntityCacheUtil.getResult(RigheFattureVeditaModelImpl.ENTITY_CACHE_ENABLED,
				RigheFattureVeditaImpl.class, primaryKey);

		if (righeFattureVedita == _nullRigheFattureVedita) {
			return null;
		}

		if (righeFattureVedita == null) {
			Session session = null;

			try {
				session = openSession();

				righeFattureVedita = (RigheFattureVedita)session.get(RigheFattureVeditaImpl.class,
						primaryKey);

				if (righeFattureVedita != null) {
					cacheResult(righeFattureVedita);
				}
				else {
					EntityCacheUtil.putResult(RigheFattureVeditaModelImpl.ENTITY_CACHE_ENABLED,
						RigheFattureVeditaImpl.class, primaryKey,
						_nullRigheFattureVedita);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(RigheFattureVeditaModelImpl.ENTITY_CACHE_ENABLED,
					RigheFattureVeditaImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return righeFattureVedita;
	}

	/**
	 * Returns the righe fatture vedita with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param righeFattureVeditaPK the primary key of the righe fatture vedita
	 * @return the righe fatture vedita, or <code>null</code> if a righe fatture vedita with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public RigheFattureVedita fetchByPrimaryKey(
		RigheFattureVeditaPK righeFattureVeditaPK) throws SystemException {
		return fetchByPrimaryKey((Serializable)righeFattureVeditaPK);
	}

	/**
	 * Returns all the righe fatture veditas.
	 *
	 * @return the righe fatture veditas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<RigheFattureVedita> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the righe fatture veditas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheFattureVeditaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of righe fatture veditas
	 * @param end the upper bound of the range of righe fatture veditas (not inclusive)
	 * @return the range of righe fatture veditas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<RigheFattureVedita> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the righe fatture veditas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheFattureVeditaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of righe fatture veditas
	 * @param end the upper bound of the range of righe fatture veditas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of righe fatture veditas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<RigheFattureVedita> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<RigheFattureVedita> list = (List<RigheFattureVedita>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_RIGHEFATTUREVEDITA);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_RIGHEFATTUREVEDITA;

				if (pagination) {
					sql = sql.concat(RigheFattureVeditaModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<RigheFattureVedita>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<RigheFattureVedita>(list);
				}
				else {
					list = (List<RigheFattureVedita>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the righe fatture veditas from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (RigheFattureVedita righeFattureVedita : findAll()) {
			remove(righeFattureVedita);
		}
	}

	/**
	 * Returns the number of righe fatture veditas.
	 *
	 * @return the number of righe fatture veditas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_RIGHEFATTUREVEDITA);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the righe fatture vedita persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.RigheFattureVedita")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<RigheFattureVedita>> listenersList = new ArrayList<ModelListener<RigheFattureVedita>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<RigheFattureVedita>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(RigheFattureVeditaImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_RIGHEFATTUREVEDITA = "SELECT righeFattureVedita FROM RigheFattureVedita righeFattureVedita";
	private static final String _SQL_SELECT_RIGHEFATTUREVEDITA_WHERE = "SELECT righeFattureVedita FROM RigheFattureVedita righeFattureVedita WHERE ";
	private static final String _SQL_COUNT_RIGHEFATTUREVEDITA = "SELECT COUNT(righeFattureVedita) FROM RigheFattureVedita righeFattureVedita";
	private static final String _SQL_COUNT_RIGHEFATTUREVEDITA_WHERE = "SELECT COUNT(righeFattureVedita) FROM RigheFattureVedita righeFattureVedita WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "righeFattureVedita.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No RigheFattureVedita exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No RigheFattureVedita exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(RigheFattureVeditaPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"anno", "codiceAttivita", "codiceCentro", "numeroProtocollo",
				"numeroRigo", "statoRigo", "annoBolla",
				"tipoIdentificativoBolla", "tipoBolla", "codiceDeposito",
				"numeroBollettario", "numeroBolla", "dataBollaEvasa", "rigoDDT",
				"tipoRigoDDT", "codiceCausaleMagazzino",
				"codiceDepositoMagazzino", "codiceArticolo", "codiceVariante",
				"codiceTestoDescrizioni", "descrizione", "unitaMisura",
				"numeroDecimalliQuant", "quantita1", "quantita2", "quantita3",
				"quantita", "unitaMisura2", "quantitaUnitMisSec",
				"numeroDecimalliPrezzo", "prezzo", "prezzoIVA", "importoLordo",
				"importoLordoIVA", "sconto1", "sconto2", "sconto3",
				"importoNettoRigo", "importoNettoRigoIVA", "importoNetto",
				"importoNettoIVA", "importoCONAIAddebitatoInt",
				"importoCONAIAddebitatoOrig", "codiceIVAFatturazione",
				"nomenclaturaCombinata", "testStampaDisBase", "tipoOrdineEvaso",
				"annoOrdineEvaso", "numeroOrdineEvaso", "numeroRigoOrdineEvaso",
				"tipoEvasione", "descrizioneRiferimento",
				"numeroPrimaNotaMagazzino", "numeroMovimentoMagazzino",
				"tipoEsplosione", "livelloMassimoEsplosione", "libStr1",
				"libStr2", "libStr3", "libDbl1", "libDbl2", "libDbl3", "libLng1",
				"libLng2", "libLng3", "libDat1", "libDat2", "libDat3",
				"testFatturazioneAuto"
			});
	private static RigheFattureVedita _nullRigheFattureVedita = new RigheFattureVeditaImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<RigheFattureVedita> toCacheModel() {
				return _nullRigheFattureVeditaCacheModel;
			}
		};

	private static CacheModel<RigheFattureVedita> _nullRigheFattureVeditaCacheModel =
		new CacheModel<RigheFattureVedita>() {
			@Override
			public RigheFattureVedita toEntityModel() {
				return _nullRigheFattureVedita;
			}
		};
}