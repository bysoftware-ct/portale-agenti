/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchOrdiniClientiException;
import it.bysoftware.ct.model.OrdiniClienti;
import it.bysoftware.ct.model.impl.OrdiniClientiImpl;
import it.bysoftware.ct.model.impl.OrdiniClientiModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the ordini clienti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see OrdiniClientiPersistence
 * @see OrdiniClientiUtil
 * @generated
 */
public class OrdiniClientiPersistenceImpl extends BasePersistenceImpl<OrdiniClienti>
	implements OrdiniClientiPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link OrdiniClientiUtil} to access the ordini clienti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = OrdiniClientiImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			OrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			OrdiniClientiImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			OrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			OrdiniClientiImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			OrdiniClientiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICECLIENTE =
		new FinderPath(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			OrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			OrdiniClientiImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCodiceCliente",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTE =
		new FinderPath(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			OrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			OrdiniClientiImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByCodiceCliente", new String[] { String.class.getName() },
			OrdiniClientiModelImpl.CODICECLIENTE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CODICECLIENTE = new FinderPath(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			OrdiniClientiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCodiceCliente",
			new String[] { String.class.getName() });

	/**
	 * Returns all the ordini clientis where codiceCliente = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @return the matching ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrdiniClienti> findByCodiceCliente(String codiceCliente)
		throws SystemException {
		return findByCodiceCliente(codiceCliente, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ordini clientis where codiceCliente = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceCliente the codice cliente
	 * @param start the lower bound of the range of ordini clientis
	 * @param end the upper bound of the range of ordini clientis (not inclusive)
	 * @return the range of matching ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrdiniClienti> findByCodiceCliente(String codiceCliente,
		int start, int end) throws SystemException {
		return findByCodiceCliente(codiceCliente, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ordini clientis where codiceCliente = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceCliente the codice cliente
	 * @param start the lower bound of the range of ordini clientis
	 * @param end the upper bound of the range of ordini clientis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrdiniClienti> findByCodiceCliente(String codiceCliente,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTE;
			finderArgs = new Object[] { codiceCliente };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICECLIENTE;
			finderArgs = new Object[] {
					codiceCliente,
					
					start, end, orderByComparator
				};
		}

		List<OrdiniClienti> list = (List<OrdiniClienti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (OrdiniClienti ordiniClienti : list) {
				if (!Validator.equals(codiceCliente,
							ordiniClienti.getCodiceCliente())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ORDINICLIENTI_WHERE);

			boolean bindCodiceCliente = false;

			if (codiceCliente == null) {
				query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_1);
			}
			else if (codiceCliente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_3);
			}
			else {
				bindCodiceCliente = true;

				query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OrdiniClientiModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceCliente) {
					qPos.add(codiceCliente);
				}

				if (!pagination) {
					list = (List<OrdiniClienti>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<OrdiniClienti>(list);
				}
				else {
					list = (List<OrdiniClienti>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ordini clienti in the ordered set where codiceCliente = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ordini clienti
	 * @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a matching ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti findByCodiceCliente_First(String codiceCliente,
		OrderByComparator orderByComparator)
		throws NoSuchOrdiniClientiException, SystemException {
		OrdiniClienti ordiniClienti = fetchByCodiceCliente_First(codiceCliente,
				orderByComparator);

		if (ordiniClienti != null) {
			return ordiniClienti;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceCliente=");
		msg.append(codiceCliente);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrdiniClientiException(msg.toString());
	}

	/**
	 * Returns the first ordini clienti in the ordered set where codiceCliente = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ordini clienti, or <code>null</code> if a matching ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti fetchByCodiceCliente_First(String codiceCliente,
		OrderByComparator orderByComparator) throws SystemException {
		List<OrdiniClienti> list = findByCodiceCliente(codiceCliente, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ordini clienti in the ordered set where codiceCliente = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ordini clienti
	 * @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a matching ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti findByCodiceCliente_Last(String codiceCliente,
		OrderByComparator orderByComparator)
		throws NoSuchOrdiniClientiException, SystemException {
		OrdiniClienti ordiniClienti = fetchByCodiceCliente_Last(codiceCliente,
				orderByComparator);

		if (ordiniClienti != null) {
			return ordiniClienti;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceCliente=");
		msg.append(codiceCliente);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrdiniClientiException(msg.toString());
	}

	/**
	 * Returns the last ordini clienti in the ordered set where codiceCliente = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ordini clienti, or <code>null</code> if a matching ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti fetchByCodiceCliente_Last(String codiceCliente,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCodiceCliente(codiceCliente);

		if (count == 0) {
			return null;
		}

		List<OrdiniClienti> list = findByCodiceCliente(codiceCliente,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ordini clientis before and after the current ordini clienti in the ordered set where codiceCliente = &#63;.
	 *
	 * @param ordiniClientiPK the primary key of the current ordini clienti
	 * @param codiceCliente the codice cliente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ordini clienti
	 * @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti[] findByCodiceCliente_PrevAndNext(
		OrdiniClientiPK ordiniClientiPK, String codiceCliente,
		OrderByComparator orderByComparator)
		throws NoSuchOrdiniClientiException, SystemException {
		OrdiniClienti ordiniClienti = findByPrimaryKey(ordiniClientiPK);

		Session session = null;

		try {
			session = openSession();

			OrdiniClienti[] array = new OrdiniClientiImpl[3];

			array[0] = getByCodiceCliente_PrevAndNext(session, ordiniClienti,
					codiceCliente, orderByComparator, true);

			array[1] = ordiniClienti;

			array[2] = getByCodiceCliente_PrevAndNext(session, ordiniClienti,
					codiceCliente, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected OrdiniClienti getByCodiceCliente_PrevAndNext(Session session,
		OrdiniClienti ordiniClienti, String codiceCliente,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ORDINICLIENTI_WHERE);

		boolean bindCodiceCliente = false;

		if (codiceCliente == null) {
			query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_1);
		}
		else if (codiceCliente.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_3);
		}
		else {
			bindCodiceCliente = true;

			query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OrdiniClientiModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodiceCliente) {
			qPos.add(codiceCliente);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ordiniClienti);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<OrdiniClienti> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ordini clientis where codiceCliente = &#63; from the database.
	 *
	 * @param codiceCliente the codice cliente
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCodiceCliente(String codiceCliente)
		throws SystemException {
		for (OrdiniClienti ordiniClienti : findByCodiceCliente(codiceCliente,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(ordiniClienti);
		}
	}

	/**
	 * Returns the number of ordini clientis where codiceCliente = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @return the number of matching ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCodiceCliente(String codiceCliente)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICECLIENTE;

		Object[] finderArgs = new Object[] { codiceCliente };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ORDINICLIENTI_WHERE);

			boolean bindCodiceCliente = false;

			if (codiceCliente == null) {
				query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_1);
			}
			else if (codiceCliente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_3);
			}
			else {
				bindCodiceCliente = true;

				query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceCliente) {
					qPos.add(codiceCliente);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_1 = "ordiniClienti.codiceCliente IS NULL";
	private static final String _FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_2 = "ordiniClienti.codiceCliente = ?";
	private static final String _FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_3 = "(ordiniClienti.codiceCliente IS NULL OR ordiniClienti.codiceCliente = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICECLIENTEAGENTENUMPORTALE =
		new FinderPath(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			OrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			OrdiniClientiImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCodiceClienteAgenteNumPortale",
			new String[] {
				String.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTEAGENTENUMPORTALE =
		new FinderPath(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			OrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			OrdiniClientiImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByCodiceClienteAgenteNumPortale",
			new String[] { String.class.getName(), String.class.getName() },
			OrdiniClientiModelImpl.CODICECLIENTE_COLUMN_BITMASK |
			OrdiniClientiModelImpl.NOTE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CODICECLIENTEAGENTENUMPORTALE =
		new FinderPath(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			OrdiniClientiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCodiceClienteAgenteNumPortale",
			new String[] { String.class.getName(), String.class.getName() });

	/**
	 * Returns all the ordini clientis where codiceCliente = &#63; and note = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param note the note
	 * @return the matching ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrdiniClienti> findByCodiceClienteAgenteNumPortale(
		String codiceCliente, String note) throws SystemException {
		return findByCodiceClienteAgenteNumPortale(codiceCliente, note,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ordini clientis where codiceCliente = &#63; and note = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceCliente the codice cliente
	 * @param note the note
	 * @param start the lower bound of the range of ordini clientis
	 * @param end the upper bound of the range of ordini clientis (not inclusive)
	 * @return the range of matching ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrdiniClienti> findByCodiceClienteAgenteNumPortale(
		String codiceCliente, String note, int start, int end)
		throws SystemException {
		return findByCodiceClienteAgenteNumPortale(codiceCliente, note, start,
			end, null);
	}

	/**
	 * Returns an ordered range of all the ordini clientis where codiceCliente = &#63; and note = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceCliente the codice cliente
	 * @param note the note
	 * @param start the lower bound of the range of ordini clientis
	 * @param end the upper bound of the range of ordini clientis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrdiniClienti> findByCodiceClienteAgenteNumPortale(
		String codiceCliente, String note, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTEAGENTENUMPORTALE;
			finderArgs = new Object[] { codiceCliente, note };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICECLIENTEAGENTENUMPORTALE;
			finderArgs = new Object[] {
					codiceCliente, note,
					
					start, end, orderByComparator
				};
		}

		List<OrdiniClienti> list = (List<OrdiniClienti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (OrdiniClienti ordiniClienti : list) {
				if (!Validator.equals(codiceCliente,
							ordiniClienti.getCodiceCliente()) ||
						!Validator.equals(note, ordiniClienti.getNote())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ORDINICLIENTI_WHERE);

			boolean bindCodiceCliente = false;

			if (codiceCliente == null) {
				query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_CODICECLIENTE_1);
			}
			else if (codiceCliente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_CODICECLIENTE_3);
			}
			else {
				bindCodiceCliente = true;

				query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_CODICECLIENTE_2);
			}

			boolean bindNote = false;

			if (note == null) {
				query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_NOTE_1);
			}
			else if (note.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_NOTE_3);
			}
			else {
				bindNote = true;

				query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_NOTE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OrdiniClientiModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceCliente) {
					qPos.add(codiceCliente);
				}

				if (bindNote) {
					qPos.add(note);
				}

				if (!pagination) {
					list = (List<OrdiniClienti>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<OrdiniClienti>(list);
				}
				else {
					list = (List<OrdiniClienti>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ordini clienti in the ordered set where codiceCliente = &#63; and note = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param note the note
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ordini clienti
	 * @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a matching ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti findByCodiceClienteAgenteNumPortale_First(
		String codiceCliente, String note, OrderByComparator orderByComparator)
		throws NoSuchOrdiniClientiException, SystemException {
		OrdiniClienti ordiniClienti = fetchByCodiceClienteAgenteNumPortale_First(codiceCliente,
				note, orderByComparator);

		if (ordiniClienti != null) {
			return ordiniClienti;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceCliente=");
		msg.append(codiceCliente);

		msg.append(", note=");
		msg.append(note);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrdiniClientiException(msg.toString());
	}

	/**
	 * Returns the first ordini clienti in the ordered set where codiceCliente = &#63; and note = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param note the note
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ordini clienti, or <code>null</code> if a matching ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti fetchByCodiceClienteAgenteNumPortale_First(
		String codiceCliente, String note, OrderByComparator orderByComparator)
		throws SystemException {
		List<OrdiniClienti> list = findByCodiceClienteAgenteNumPortale(codiceCliente,
				note, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ordini clienti in the ordered set where codiceCliente = &#63; and note = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param note the note
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ordini clienti
	 * @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a matching ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti findByCodiceClienteAgenteNumPortale_Last(
		String codiceCliente, String note, OrderByComparator orderByComparator)
		throws NoSuchOrdiniClientiException, SystemException {
		OrdiniClienti ordiniClienti = fetchByCodiceClienteAgenteNumPortale_Last(codiceCliente,
				note, orderByComparator);

		if (ordiniClienti != null) {
			return ordiniClienti;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceCliente=");
		msg.append(codiceCliente);

		msg.append(", note=");
		msg.append(note);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrdiniClientiException(msg.toString());
	}

	/**
	 * Returns the last ordini clienti in the ordered set where codiceCliente = &#63; and note = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param note the note
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ordini clienti, or <code>null</code> if a matching ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti fetchByCodiceClienteAgenteNumPortale_Last(
		String codiceCliente, String note, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByCodiceClienteAgenteNumPortale(codiceCliente, note);

		if (count == 0) {
			return null;
		}

		List<OrdiniClienti> list = findByCodiceClienteAgenteNumPortale(codiceCliente,
				note, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ordini clientis before and after the current ordini clienti in the ordered set where codiceCliente = &#63; and note = &#63;.
	 *
	 * @param ordiniClientiPK the primary key of the current ordini clienti
	 * @param codiceCliente the codice cliente
	 * @param note the note
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ordini clienti
	 * @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti[] findByCodiceClienteAgenteNumPortale_PrevAndNext(
		OrdiniClientiPK ordiniClientiPK, String codiceCliente, String note,
		OrderByComparator orderByComparator)
		throws NoSuchOrdiniClientiException, SystemException {
		OrdiniClienti ordiniClienti = findByPrimaryKey(ordiniClientiPK);

		Session session = null;

		try {
			session = openSession();

			OrdiniClienti[] array = new OrdiniClientiImpl[3];

			array[0] = getByCodiceClienteAgenteNumPortale_PrevAndNext(session,
					ordiniClienti, codiceCliente, note, orderByComparator, true);

			array[1] = ordiniClienti;

			array[2] = getByCodiceClienteAgenteNumPortale_PrevAndNext(session,
					ordiniClienti, codiceCliente, note, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected OrdiniClienti getByCodiceClienteAgenteNumPortale_PrevAndNext(
		Session session, OrdiniClienti ordiniClienti, String codiceCliente,
		String note, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ORDINICLIENTI_WHERE);

		boolean bindCodiceCliente = false;

		if (codiceCliente == null) {
			query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_CODICECLIENTE_1);
		}
		else if (codiceCliente.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_CODICECLIENTE_3);
		}
		else {
			bindCodiceCliente = true;

			query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_CODICECLIENTE_2);
		}

		boolean bindNote = false;

		if (note == null) {
			query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_NOTE_1);
		}
		else if (note.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_NOTE_3);
		}
		else {
			bindNote = true;

			query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_NOTE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OrdiniClientiModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodiceCliente) {
			qPos.add(codiceCliente);
		}

		if (bindNote) {
			qPos.add(note);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ordiniClienti);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<OrdiniClienti> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ordini clientis where codiceCliente = &#63; and note = &#63; from the database.
	 *
	 * @param codiceCliente the codice cliente
	 * @param note the note
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCodiceClienteAgenteNumPortale(String codiceCliente,
		String note) throws SystemException {
		for (OrdiniClienti ordiniClienti : findByCodiceClienteAgenteNumPortale(
				codiceCliente, note, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(ordiniClienti);
		}
	}

	/**
	 * Returns the number of ordini clientis where codiceCliente = &#63; and note = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param note the note
	 * @return the number of matching ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCodiceClienteAgenteNumPortale(String codiceCliente,
		String note) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICECLIENTEAGENTENUMPORTALE;

		Object[] finderArgs = new Object[] { codiceCliente, note };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ORDINICLIENTI_WHERE);

			boolean bindCodiceCliente = false;

			if (codiceCliente == null) {
				query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_CODICECLIENTE_1);
			}
			else if (codiceCliente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_CODICECLIENTE_3);
			}
			else {
				bindCodiceCliente = true;

				query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_CODICECLIENTE_2);
			}

			boolean bindNote = false;

			if (note == null) {
				query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_NOTE_1);
			}
			else if (note.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_NOTE_3);
			}
			else {
				bindNote = true;

				query.append(_FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_NOTE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceCliente) {
					qPos.add(codiceCliente);
				}

				if (bindNote) {
					qPos.add(note);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_CODICECLIENTE_1 =
		"ordiniClienti.codiceCliente IS NULL AND ";
	private static final String _FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_CODICECLIENTE_2 =
		"ordiniClienti.codiceCliente = ? AND ";
	private static final String _FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_CODICECLIENTE_3 =
		"(ordiniClienti.codiceCliente IS NULL OR ordiniClienti.codiceCliente = '') AND ";
	private static final String _FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_NOTE_1 =
		"ordiniClienti.note IS NULL";
	private static final String _FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_NOTE_2 =
		"ordiniClienti.note = ?";
	private static final String _FINDER_COLUMN_CODICECLIENTEAGENTENUMPORTALE_NOTE_3 =
		"(ordiniClienti.note IS NULL OR ordiniClienti.note = '')";

	public OrdiniClientiPersistenceImpl() {
		setModelClass(OrdiniClienti.class);
	}

	/**
	 * Caches the ordini clienti in the entity cache if it is enabled.
	 *
	 * @param ordiniClienti the ordini clienti
	 */
	@Override
	public void cacheResult(OrdiniClienti ordiniClienti) {
		EntityCacheUtil.putResult(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			OrdiniClientiImpl.class, ordiniClienti.getPrimaryKey(),
			ordiniClienti);

		ordiniClienti.resetOriginalValues();
	}

	/**
	 * Caches the ordini clientis in the entity cache if it is enabled.
	 *
	 * @param ordiniClientis the ordini clientis
	 */
	@Override
	public void cacheResult(List<OrdiniClienti> ordiniClientis) {
		for (OrdiniClienti ordiniClienti : ordiniClientis) {
			if (EntityCacheUtil.getResult(
						OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
						OrdiniClientiImpl.class, ordiniClienti.getPrimaryKey()) == null) {
				cacheResult(ordiniClienti);
			}
			else {
				ordiniClienti.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all ordini clientis.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(OrdiniClientiImpl.class.getName());
		}

		EntityCacheUtil.clearCache(OrdiniClientiImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the ordini clienti.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(OrdiniClienti ordiniClienti) {
		EntityCacheUtil.removeResult(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			OrdiniClientiImpl.class, ordiniClienti.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<OrdiniClienti> ordiniClientis) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (OrdiniClienti ordiniClienti : ordiniClientis) {
			EntityCacheUtil.removeResult(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
				OrdiniClientiImpl.class, ordiniClienti.getPrimaryKey());
		}
	}

	/**
	 * Creates a new ordini clienti with the primary key. Does not add the ordini clienti to the database.
	 *
	 * @param ordiniClientiPK the primary key for the new ordini clienti
	 * @return the new ordini clienti
	 */
	@Override
	public OrdiniClienti create(OrdiniClientiPK ordiniClientiPK) {
		OrdiniClienti ordiniClienti = new OrdiniClientiImpl();

		ordiniClienti.setNew(true);
		ordiniClienti.setPrimaryKey(ordiniClientiPK);

		return ordiniClienti;
	}

	/**
	 * Removes the ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ordiniClientiPK the primary key of the ordini clienti
	 * @return the ordini clienti that was removed
	 * @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti remove(OrdiniClientiPK ordiniClientiPK)
		throws NoSuchOrdiniClientiException, SystemException {
		return remove((Serializable)ordiniClientiPK);
	}

	/**
	 * Removes the ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the ordini clienti
	 * @return the ordini clienti that was removed
	 * @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti remove(Serializable primaryKey)
		throws NoSuchOrdiniClientiException, SystemException {
		Session session = null;

		try {
			session = openSession();

			OrdiniClienti ordiniClienti = (OrdiniClienti)session.get(OrdiniClientiImpl.class,
					primaryKey);

			if (ordiniClienti == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchOrdiniClientiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ordiniClienti);
		}
		catch (NoSuchOrdiniClientiException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected OrdiniClienti removeImpl(OrdiniClienti ordiniClienti)
		throws SystemException {
		ordiniClienti = toUnwrappedModel(ordiniClienti);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ordiniClienti)) {
				ordiniClienti = (OrdiniClienti)session.get(OrdiniClientiImpl.class,
						ordiniClienti.getPrimaryKeyObj());
			}

			if (ordiniClienti != null) {
				session.delete(ordiniClienti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ordiniClienti != null) {
			clearCache(ordiniClienti);
		}

		return ordiniClienti;
	}

	@Override
	public OrdiniClienti updateImpl(
		it.bysoftware.ct.model.OrdiniClienti ordiniClienti)
		throws SystemException {
		ordiniClienti = toUnwrappedModel(ordiniClienti);

		boolean isNew = ordiniClienti.isNew();

		OrdiniClientiModelImpl ordiniClientiModelImpl = (OrdiniClientiModelImpl)ordiniClienti;

		Session session = null;

		try {
			session = openSession();

			if (ordiniClienti.isNew()) {
				session.save(ordiniClienti);

				ordiniClienti.setNew(false);
			}
			else {
				session.merge(ordiniClienti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !OrdiniClientiModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((ordiniClientiModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ordiniClientiModelImpl.getOriginalCodiceCliente()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICECLIENTE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTE,
					args);

				args = new Object[] { ordiniClientiModelImpl.getCodiceCliente() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICECLIENTE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTE,
					args);
			}

			if ((ordiniClientiModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTEAGENTENUMPORTALE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ordiniClientiModelImpl.getOriginalCodiceCliente(),
						ordiniClientiModelImpl.getOriginalNote()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICECLIENTEAGENTENUMPORTALE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTEAGENTENUMPORTALE,
					args);

				args = new Object[] {
						ordiniClientiModelImpl.getCodiceCliente(),
						ordiniClientiModelImpl.getNote()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICECLIENTEAGENTENUMPORTALE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTEAGENTENUMPORTALE,
					args);
			}
		}

		EntityCacheUtil.putResult(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			OrdiniClientiImpl.class, ordiniClienti.getPrimaryKey(),
			ordiniClienti);

		return ordiniClienti;
	}

	protected OrdiniClienti toUnwrappedModel(OrdiniClienti ordiniClienti) {
		if (ordiniClienti instanceof OrdiniClientiImpl) {
			return ordiniClienti;
		}

		OrdiniClientiImpl ordiniClientiImpl = new OrdiniClientiImpl();

		ordiniClientiImpl.setNew(ordiniClienti.isNew());
		ordiniClientiImpl.setPrimaryKey(ordiniClienti.getPrimaryKey());

		ordiniClientiImpl.setAnno(ordiniClienti.getAnno());
		ordiniClientiImpl.setCodiceAttivita(ordiniClienti.getCodiceAttivita());
		ordiniClientiImpl.setCodiceCentro(ordiniClienti.getCodiceCentro());
		ordiniClientiImpl.setCodiceDeposito(ordiniClienti.getCodiceDeposito());
		ordiniClientiImpl.setTipoOrdine(ordiniClienti.getTipoOrdine());
		ordiniClientiImpl.setNumeroOrdine(ordiniClienti.getNumeroOrdine());
		ordiniClientiImpl.setTipoDocumento(ordiniClienti.getTipoDocumento());
		ordiniClientiImpl.setCodiceContAnalitica(ordiniClienti.getCodiceContAnalitica());
		ordiniClientiImpl.setIdTipoDocumento(ordiniClienti.getIdTipoDocumento());
		ordiniClientiImpl.setStatoOrdine(ordiniClienti.isStatoOrdine());
		ordiniClientiImpl.setDescrEstremiDoc(ordiniClienti.getDescrEstremiDoc());
		ordiniClientiImpl.setTipoSoggetto(ordiniClienti.isTipoSoggetto());
		ordiniClientiImpl.setCodiceCliente(ordiniClienti.getCodiceCliente());
		ordiniClientiImpl.setNaturaTransazione(ordiniClienti.getNaturaTransazione());
		ordiniClientiImpl.setCodiceEsenzione(ordiniClienti.getCodiceEsenzione());
		ordiniClientiImpl.setCodiceDivisa(ordiniClienti.getCodiceDivisa());
		ordiniClientiImpl.setValoreCambio(ordiniClienti.getValoreCambio());
		ordiniClientiImpl.setDataValoreCambio(ordiniClienti.getDataValoreCambio());
		ordiniClientiImpl.setCodicePianoPag(ordiniClienti.getCodicePianoPag());
		ordiniClientiImpl.setInizioCalcoloPag(ordiniClienti.getInizioCalcoloPag());
		ordiniClientiImpl.setPercentualeScontoMaggiorazione(ordiniClienti.getPercentualeScontoMaggiorazione());
		ordiniClientiImpl.setPercentualeScontoProntaCassa(ordiniClienti.getPercentualeScontoProntaCassa());
		ordiniClientiImpl.setPercentualeProvvChiusura(ordiniClienti.getPercentualeProvvChiusura());
		ordiniClientiImpl.setDataDocumento(ordiniClienti.getDataDocumento());
		ordiniClientiImpl.setDataRegistrazione(ordiniClienti.getDataRegistrazione());
		ordiniClientiImpl.setCausaleEstrattoConto(ordiniClienti.getCausaleEstrattoConto());
		ordiniClientiImpl.setDataPrimaRata(ordiniClienti.getDataPrimaRata());
		ordiniClientiImpl.setDataUltimaRata(ordiniClienti.getDataUltimaRata());
		ordiniClientiImpl.setCodiceBanca(ordiniClienti.getCodiceBanca());
		ordiniClientiImpl.setCodiceAgenzia(ordiniClienti.getCodiceAgenzia());
		ordiniClientiImpl.setCodiceAgente(ordiniClienti.getCodiceAgente());
		ordiniClientiImpl.setCodiceGruppoAgenti(ordiniClienti.getCodiceGruppoAgenti());
		ordiniClientiImpl.setCodiceZona(ordiniClienti.getCodiceZona());
		ordiniClientiImpl.setCodiceSpedizione(ordiniClienti.getCodiceSpedizione());
		ordiniClientiImpl.setCodicePorto(ordiniClienti.getCodicePorto());
		ordiniClientiImpl.setCodiceDestinatario(ordiniClienti.getCodiceDestinatario());
		ordiniClientiImpl.setCodiceListino(ordiniClienti.getCodiceListino());
		ordiniClientiImpl.setCodiceLingua(ordiniClienti.getCodiceLingua());
		ordiniClientiImpl.setNumeroDecPrezzo(ordiniClienti.getNumeroDecPrezzo());
		ordiniClientiImpl.setNote(ordiniClienti.getNote());
		ordiniClientiImpl.setPercentualeSpeseTrasp(ordiniClienti.getPercentualeSpeseTrasp());
		ordiniClientiImpl.setSpeseTrasporto(ordiniClienti.getSpeseTrasporto());
		ordiniClientiImpl.setSpeseImballaggio(ordiniClienti.getSpeseImballaggio());
		ordiniClientiImpl.setSpeseVarie(ordiniClienti.getSpeseVarie());
		ordiniClientiImpl.setSpeseBanca(ordiniClienti.getSpeseBanca());
		ordiniClientiImpl.setCuraTrasporto(ordiniClienti.getCuraTrasporto());
		ordiniClientiImpl.setCausaleTrasporto(ordiniClienti.getCausaleTrasporto());
		ordiniClientiImpl.setAspettoEstriore(ordiniClienti.getAspettoEstriore());
		ordiniClientiImpl.setVettore1(ordiniClienti.getVettore1());
		ordiniClientiImpl.setVettore2(ordiniClienti.getVettore2());
		ordiniClientiImpl.setVettore3(ordiniClienti.getVettore3());
		ordiniClientiImpl.setNumeroColli(ordiniClienti.getNumeroColli());
		ordiniClientiImpl.setPesoLordo(ordiniClienti.getPesoLordo());
		ordiniClientiImpl.setPesoNetto(ordiniClienti.getPesoNetto());
		ordiniClientiImpl.setVolume(ordiniClienti.getVolume());
		ordiniClientiImpl.setNumeroCopie(ordiniClienti.getNumeroCopie());
		ordiniClientiImpl.setNumeroCopieStampate(ordiniClienti.getNumeroCopieStampate());
		ordiniClientiImpl.setInviatoEmail(ordiniClienti.isInviatoEmail());
		ordiniClientiImpl.setNomePDF(ordiniClienti.getNomePDF());
		ordiniClientiImpl.setRiferimentoOrdine(ordiniClienti.getRiferimentoOrdine());
		ordiniClientiImpl.setDataConferma(ordiniClienti.getDataConferma());
		ordiniClientiImpl.setConfermaStampata(ordiniClienti.isConfermaStampata());
		ordiniClientiImpl.setTotaleOrdine(ordiniClienti.getTotaleOrdine());
		ordiniClientiImpl.setCodiceIVATrasp(ordiniClienti.getCodiceIVATrasp());
		ordiniClientiImpl.setCodiceIVAImballo(ordiniClienti.getCodiceIVAImballo());
		ordiniClientiImpl.setCodiceIVAVarie(ordiniClienti.getCodiceIVAVarie());
		ordiniClientiImpl.setCodiceIVABanca(ordiniClienti.getCodiceIVABanca());
		ordiniClientiImpl.setLibStr1(ordiniClienti.getLibStr1());
		ordiniClientiImpl.setLibStr2(ordiniClienti.getLibStr2());
		ordiniClientiImpl.setLibStr3(ordiniClienti.getLibStr3());
		ordiniClientiImpl.setLibDbl1(ordiniClienti.getLibDbl1());
		ordiniClientiImpl.setLibDbl2(ordiniClienti.getLibDbl2());
		ordiniClientiImpl.setLibDbl3(ordiniClienti.getLibDbl3());
		ordiniClientiImpl.setLibDat1(ordiniClienti.getLibDat1());
		ordiniClientiImpl.setLibDat2(ordiniClienti.getLibDat2());
		ordiniClientiImpl.setLibDat3(ordiniClienti.getLibDat3());
		ordiniClientiImpl.setLibLng1(ordiniClienti.getLibLng1());
		ordiniClientiImpl.setLibLng2(ordiniClienti.getLibLng2());
		ordiniClientiImpl.setLibLng3(ordiniClienti.getLibLng3());

		return ordiniClientiImpl;
	}

	/**
	 * Returns the ordini clienti with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the ordini clienti
	 * @return the ordini clienti
	 * @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti findByPrimaryKey(Serializable primaryKey)
		throws NoSuchOrdiniClientiException, SystemException {
		OrdiniClienti ordiniClienti = fetchByPrimaryKey(primaryKey);

		if (ordiniClienti == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchOrdiniClientiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ordiniClienti;
	}

	/**
	 * Returns the ordini clienti with the primary key or throws a {@link it.bysoftware.ct.NoSuchOrdiniClientiException} if it could not be found.
	 *
	 * @param ordiniClientiPK the primary key of the ordini clienti
	 * @return the ordini clienti
	 * @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti findByPrimaryKey(OrdiniClientiPK ordiniClientiPK)
		throws NoSuchOrdiniClientiException, SystemException {
		return findByPrimaryKey((Serializable)ordiniClientiPK);
	}

	/**
	 * Returns the ordini clienti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the ordini clienti
	 * @return the ordini clienti, or <code>null</code> if a ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		OrdiniClienti ordiniClienti = (OrdiniClienti)EntityCacheUtil.getResult(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
				OrdiniClientiImpl.class, primaryKey);

		if (ordiniClienti == _nullOrdiniClienti) {
			return null;
		}

		if (ordiniClienti == null) {
			Session session = null;

			try {
				session = openSession();

				ordiniClienti = (OrdiniClienti)session.get(OrdiniClientiImpl.class,
						primaryKey);

				if (ordiniClienti != null) {
					cacheResult(ordiniClienti);
				}
				else {
					EntityCacheUtil.putResult(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
						OrdiniClientiImpl.class, primaryKey, _nullOrdiniClienti);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(OrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
					OrdiniClientiImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ordiniClienti;
	}

	/**
	 * Returns the ordini clienti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ordiniClientiPK the primary key of the ordini clienti
	 * @return the ordini clienti, or <code>null</code> if a ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrdiniClienti fetchByPrimaryKey(OrdiniClientiPK ordiniClientiPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)ordiniClientiPK);
	}

	/**
	 * Returns all the ordini clientis.
	 *
	 * @return the ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrdiniClienti> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ordini clientis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ordini clientis
	 * @param end the upper bound of the range of ordini clientis (not inclusive)
	 * @return the range of ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrdiniClienti> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the ordini clientis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ordini clientis
	 * @param end the upper bound of the range of ordini clientis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrdiniClienti> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<OrdiniClienti> list = (List<OrdiniClienti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ORDINICLIENTI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ORDINICLIENTI;

				if (pagination) {
					sql = sql.concat(OrdiniClientiModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<OrdiniClienti>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<OrdiniClienti>(list);
				}
				else {
					list = (List<OrdiniClienti>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the ordini clientis from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (OrdiniClienti ordiniClienti : findAll()) {
			remove(ordiniClienti);
		}
	}

	/**
	 * Returns the number of ordini clientis.
	 *
	 * @return the number of ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ORDINICLIENTI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the ordini clienti persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.OrdiniClienti")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<OrdiniClienti>> listenersList = new ArrayList<ModelListener<OrdiniClienti>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<OrdiniClienti>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(OrdiniClientiImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ORDINICLIENTI = "SELECT ordiniClienti FROM OrdiniClienti ordiniClienti";
	private static final String _SQL_SELECT_ORDINICLIENTI_WHERE = "SELECT ordiniClienti FROM OrdiniClienti ordiniClienti WHERE ";
	private static final String _SQL_COUNT_ORDINICLIENTI = "SELECT COUNT(ordiniClienti) FROM OrdiniClienti ordiniClienti";
	private static final String _SQL_COUNT_ORDINICLIENTI_WHERE = "SELECT COUNT(ordiniClienti) FROM OrdiniClienti ordiniClienti WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ordiniClienti.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No OrdiniClienti exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No OrdiniClienti exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(OrdiniClientiPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"anno", "codiceAttivita", "codiceCentro", "codiceDeposito",
				"tipoOrdine", "numeroOrdine", "tipoDocumento",
				"codiceContAnalitica", "idTipoDocumento", "statoOrdine",
				"descrEstremiDoc", "tipoSoggetto", "codiceCliente",
				"naturaTransazione", "codiceEsenzione", "codiceDivisa",
				"valoreCambio", "dataValoreCambio", "codicePianoPag",
				"inizioCalcoloPag", "percentualeScontoMaggiorazione",
				"percentualeScontoProntaCassa", "percentualeProvvChiusura",
				"dataDocumento", "dataRegistrazione", "causaleEstrattoConto",
				"dataPrimaRata", "dataUltimaRata", "codiceBanca",
				"codiceAgenzia", "codiceAgente", "codiceGruppoAgenti",
				"codiceZona", "codiceSpedizione", "codicePorto",
				"codiceDestinatario", "codiceListino", "codiceLingua",
				"numeroDecPrezzo", "note", "percentualeSpeseTrasp",
				"speseTrasporto", "speseImballaggio", "speseVarie", "speseBanca",
				"curaTrasporto", "causaleTrasporto", "aspettoEstriore",
				"vettore1", "vettore2", "vettore3", "numeroColli", "pesoLordo",
				"pesoNetto", "volume", "numeroCopie", "numeroCopieStampate",
				"inviatoEmail", "nomePDF", "riferimentoOrdine", "dataConferma",
				"confermaStampata", "totaleOrdine", "codiceIVATrasp",
				"codiceIVAImballo", "codiceIVAVarie", "codiceIVABanca",
				"libStr1", "libStr2", "libStr3", "libDbl1", "libDbl2", "libDbl3",
				"libDat1", "libDat2", "libDat3", "libLng1", "libLng2", "libLng3"
			});
	private static OrdiniClienti _nullOrdiniClienti = new OrdiniClientiImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<OrdiniClienti> toCacheModel() {
				return _nullOrdiniClientiCacheModel;
			}
		};

	private static CacheModel<OrdiniClienti> _nullOrdiniClientiCacheModel = new CacheModel<OrdiniClienti>() {
			@Override
			public OrdiniClienti toEntityModel() {
				return _nullOrdiniClienti;
			}
		};
}