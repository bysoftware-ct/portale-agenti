/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchEstrattoContoException;
import it.bysoftware.ct.model.EstrattoConto;
import it.bysoftware.ct.model.impl.EstrattoContoImpl;
import it.bysoftware.ct.model.impl.EstrattoContoModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the estratto conto service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see EstrattoContoPersistence
 * @see EstrattoContoUtil
 * @generated
 */
public class EstrattoContoPersistenceImpl extends BasePersistenceImpl<EstrattoConto>
	implements EstrattoContoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EstrattoContoUtil} to access the estratto conto persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EstrattoContoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
			EstrattoContoModelImpl.FINDER_CACHE_ENABLED,
			EstrattoContoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
			EstrattoContoModelImpl.FINDER_CACHE_ENABLED,
			EstrattoContoImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
			EstrattoContoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TIPOCODICESOGETTODATACHIUSA =
		new FinderPath(EstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
			EstrattoContoModelImpl.FINDER_CACHE_ENABLED,
			EstrattoContoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByTipoCodiceSogettoDataChiusa",
			new String[] {
				Boolean.class.getName(), String.class.getName(),
				Integer.class.getName(), Date.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_TIPOCODICESOGETTODATACHIUSA =
		new FinderPath(EstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
			EstrattoContoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"countByTipoCodiceSogettoDataChiusa",
			new String[] {
				Boolean.class.getName(), String.class.getName(),
				Integer.class.getName(), Date.class.getName()
			});

	/**
	 * Returns all the estratto contos where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceCliente the codice cliente
	 * @param stato the stato
	 * @param dataScadenza the data scadenza
	 * @return the matching estratto contos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EstrattoConto> findByTipoCodiceSogettoDataChiusa(
		boolean tipoSoggetto, String codiceCliente, int stato, Date dataScadenza)
		throws SystemException {
		return findByTipoCodiceSogettoDataChiusa(tipoSoggetto, codiceCliente,
			stato, dataScadenza, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the estratto contos where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceCliente the codice cliente
	 * @param stato the stato
	 * @param dataScadenza the data scadenza
	 * @param start the lower bound of the range of estratto contos
	 * @param end the upper bound of the range of estratto contos (not inclusive)
	 * @return the range of matching estratto contos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EstrattoConto> findByTipoCodiceSogettoDataChiusa(
		boolean tipoSoggetto, String codiceCliente, int stato,
		Date dataScadenza, int start, int end) throws SystemException {
		return findByTipoCodiceSogettoDataChiusa(tipoSoggetto, codiceCliente,
			stato, dataScadenza, start, end, null);
	}

	/**
	 * Returns an ordered range of all the estratto contos where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceCliente the codice cliente
	 * @param stato the stato
	 * @param dataScadenza the data scadenza
	 * @param start the lower bound of the range of estratto contos
	 * @param end the upper bound of the range of estratto contos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching estratto contos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EstrattoConto> findByTipoCodiceSogettoDataChiusa(
		boolean tipoSoggetto, String codiceCliente, int stato,
		Date dataScadenza, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TIPOCODICESOGETTODATACHIUSA;
		finderArgs = new Object[] {
				tipoSoggetto, codiceCliente, stato, dataScadenza,
				
				start, end, orderByComparator
			};

		List<EstrattoConto> list = (List<EstrattoConto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EstrattoConto estrattoConto : list) {
				if ((tipoSoggetto != estrattoConto.getTipoSoggetto()) ||
						!Validator.equals(codiceCliente,
							estrattoConto.getCodiceCliente()) ||
						(stato != estrattoConto.getStato()) ||
						(dataScadenza.getTime() <= estrattoConto.getDataScadenza()
																	.getTime())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(6 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(6);
			}

			query.append(_SQL_SELECT_ESTRATTOCONTO_WHERE);

			query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_TIPOSOGGETTO_2);

			boolean bindCodiceCliente = false;

			if (codiceCliente == null) {
				query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_CODICECLIENTE_1);
			}
			else if (codiceCliente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_CODICECLIENTE_3);
			}
			else {
				bindCodiceCliente = true;

				query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_CODICECLIENTE_2);
			}

			query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_STATO_2);

			boolean bindDataScadenza = false;

			if (dataScadenza == null) {
				query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_DATASCADENZA_1);
			}
			else {
				bindDataScadenza = true;

				query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_DATASCADENZA_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EstrattoContoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(tipoSoggetto);

				if (bindCodiceCliente) {
					qPos.add(codiceCliente);
				}

				qPos.add(stato);

				if (bindDataScadenza) {
					qPos.add(CalendarUtil.getTimestamp(dataScadenza));
				}

				if (!pagination) {
					list = (List<EstrattoConto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EstrattoConto>(list);
				}
				else {
					list = (List<EstrattoConto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first estratto conto in the ordered set where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceCliente the codice cliente
	 * @param stato the stato
	 * @param dataScadenza the data scadenza
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching estratto conto
	 * @throws it.bysoftware.ct.NoSuchEstrattoContoException if a matching estratto conto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EstrattoConto findByTipoCodiceSogettoDataChiusa_First(
		boolean tipoSoggetto, String codiceCliente, int stato,
		Date dataScadenza, OrderByComparator orderByComparator)
		throws NoSuchEstrattoContoException, SystemException {
		EstrattoConto estrattoConto = fetchByTipoCodiceSogettoDataChiusa_First(tipoSoggetto,
				codiceCliente, stato, dataScadenza, orderByComparator);

		if (estrattoConto != null) {
			return estrattoConto;
		}

		StringBundler msg = new StringBundler(10);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("tipoSoggetto=");
		msg.append(tipoSoggetto);

		msg.append(", codiceCliente=");
		msg.append(codiceCliente);

		msg.append(", stato=");
		msg.append(stato);

		msg.append(", dataScadenza=");
		msg.append(dataScadenza);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEstrattoContoException(msg.toString());
	}

	/**
	 * Returns the first estratto conto in the ordered set where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceCliente the codice cliente
	 * @param stato the stato
	 * @param dataScadenza the data scadenza
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching estratto conto, or <code>null</code> if a matching estratto conto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EstrattoConto fetchByTipoCodiceSogettoDataChiusa_First(
		boolean tipoSoggetto, String codiceCliente, int stato,
		Date dataScadenza, OrderByComparator orderByComparator)
		throws SystemException {
		List<EstrattoConto> list = findByTipoCodiceSogettoDataChiusa(tipoSoggetto,
				codiceCliente, stato, dataScadenza, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last estratto conto in the ordered set where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceCliente the codice cliente
	 * @param stato the stato
	 * @param dataScadenza the data scadenza
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching estratto conto
	 * @throws it.bysoftware.ct.NoSuchEstrattoContoException if a matching estratto conto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EstrattoConto findByTipoCodiceSogettoDataChiusa_Last(
		boolean tipoSoggetto, String codiceCliente, int stato,
		Date dataScadenza, OrderByComparator orderByComparator)
		throws NoSuchEstrattoContoException, SystemException {
		EstrattoConto estrattoConto = fetchByTipoCodiceSogettoDataChiusa_Last(tipoSoggetto,
				codiceCliente, stato, dataScadenza, orderByComparator);

		if (estrattoConto != null) {
			return estrattoConto;
		}

		StringBundler msg = new StringBundler(10);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("tipoSoggetto=");
		msg.append(tipoSoggetto);

		msg.append(", codiceCliente=");
		msg.append(codiceCliente);

		msg.append(", stato=");
		msg.append(stato);

		msg.append(", dataScadenza=");
		msg.append(dataScadenza);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEstrattoContoException(msg.toString());
	}

	/**
	 * Returns the last estratto conto in the ordered set where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceCliente the codice cliente
	 * @param stato the stato
	 * @param dataScadenza the data scadenza
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching estratto conto, or <code>null</code> if a matching estratto conto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EstrattoConto fetchByTipoCodiceSogettoDataChiusa_Last(
		boolean tipoSoggetto, String codiceCliente, int stato,
		Date dataScadenza, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByTipoCodiceSogettoDataChiusa(tipoSoggetto,
				codiceCliente, stato, dataScadenza);

		if (count == 0) {
			return null;
		}

		List<EstrattoConto> list = findByTipoCodiceSogettoDataChiusa(tipoSoggetto,
				codiceCliente, stato, dataScadenza, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the estratto contos before and after the current estratto conto in the ordered set where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	 *
	 * @param estrattoContoPK the primary key of the current estratto conto
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceCliente the codice cliente
	 * @param stato the stato
	 * @param dataScadenza the data scadenza
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next estratto conto
	 * @throws it.bysoftware.ct.NoSuchEstrattoContoException if a estratto conto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EstrattoConto[] findByTipoCodiceSogettoDataChiusa_PrevAndNext(
		EstrattoContoPK estrattoContoPK, boolean tipoSoggetto,
		String codiceCliente, int stato, Date dataScadenza,
		OrderByComparator orderByComparator)
		throws NoSuchEstrattoContoException, SystemException {
		EstrattoConto estrattoConto = findByPrimaryKey(estrattoContoPK);

		Session session = null;

		try {
			session = openSession();

			EstrattoConto[] array = new EstrattoContoImpl[3];

			array[0] = getByTipoCodiceSogettoDataChiusa_PrevAndNext(session,
					estrattoConto, tipoSoggetto, codiceCliente, stato,
					dataScadenza, orderByComparator, true);

			array[1] = estrattoConto;

			array[2] = getByTipoCodiceSogettoDataChiusa_PrevAndNext(session,
					estrattoConto, tipoSoggetto, codiceCliente, stato,
					dataScadenza, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EstrattoConto getByTipoCodiceSogettoDataChiusa_PrevAndNext(
		Session session, EstrattoConto estrattoConto, boolean tipoSoggetto,
		String codiceCliente, int stato, Date dataScadenza,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ESTRATTOCONTO_WHERE);

		query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_TIPOSOGGETTO_2);

		boolean bindCodiceCliente = false;

		if (codiceCliente == null) {
			query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_CODICECLIENTE_1);
		}
		else if (codiceCliente.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_CODICECLIENTE_3);
		}
		else {
			bindCodiceCliente = true;

			query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_CODICECLIENTE_2);
		}

		query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_STATO_2);

		boolean bindDataScadenza = false;

		if (dataScadenza == null) {
			query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_DATASCADENZA_1);
		}
		else {
			bindDataScadenza = true;

			query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_DATASCADENZA_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EstrattoContoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(tipoSoggetto);

		if (bindCodiceCliente) {
			qPos.add(codiceCliente);
		}

		qPos.add(stato);

		if (bindDataScadenza) {
			qPos.add(CalendarUtil.getTimestamp(dataScadenza));
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(estrattoConto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EstrattoConto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the estratto contos where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63; from the database.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceCliente the codice cliente
	 * @param stato the stato
	 * @param dataScadenza the data scadenza
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByTipoCodiceSogettoDataChiusa(boolean tipoSoggetto,
		String codiceCliente, int stato, Date dataScadenza)
		throws SystemException {
		for (EstrattoConto estrattoConto : findByTipoCodiceSogettoDataChiusa(
				tipoSoggetto, codiceCliente, stato, dataScadenza,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(estrattoConto);
		}
	}

	/**
	 * Returns the number of estratto contos where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	 *
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceCliente the codice cliente
	 * @param stato the stato
	 * @param dataScadenza the data scadenza
	 * @return the number of matching estratto contos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByTipoCodiceSogettoDataChiusa(boolean tipoSoggetto,
		String codiceCliente, int stato, Date dataScadenza)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_TIPOCODICESOGETTODATACHIUSA;

		Object[] finderArgs = new Object[] {
				tipoSoggetto, codiceCliente, stato, dataScadenza
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(5);

			query.append(_SQL_COUNT_ESTRATTOCONTO_WHERE);

			query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_TIPOSOGGETTO_2);

			boolean bindCodiceCliente = false;

			if (codiceCliente == null) {
				query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_CODICECLIENTE_1);
			}
			else if (codiceCliente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_CODICECLIENTE_3);
			}
			else {
				bindCodiceCliente = true;

				query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_CODICECLIENTE_2);
			}

			query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_STATO_2);

			boolean bindDataScadenza = false;

			if (dataScadenza == null) {
				query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_DATASCADENZA_1);
			}
			else {
				bindDataScadenza = true;

				query.append(_FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_DATASCADENZA_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(tipoSoggetto);

				if (bindCodiceCliente) {
					qPos.add(codiceCliente);
				}

				qPos.add(stato);

				if (bindDataScadenza) {
					qPos.add(CalendarUtil.getTimestamp(dataScadenza));
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_TIPOSOGGETTO_2 =
		"estrattoConto.id.tipoSoggetto = ? AND ";
	private static final String _FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_CODICECLIENTE_1 =
		"estrattoConto.id.codiceCliente IS NULL AND ";
	private static final String _FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_CODICECLIENTE_2 =
		"estrattoConto.id.codiceCliente = ? AND ";
	private static final String _FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_CODICECLIENTE_3 =
		"(estrattoConto.id.codiceCliente IS NULL OR estrattoConto.id.codiceCliente = '') AND ";
	private static final String _FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_STATO_2 =
		"estrattoConto.stato = ? AND ";
	private static final String _FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_DATASCADENZA_1 =
		"estrattoConto.dataScadenza < NULL";
	private static final String _FINDER_COLUMN_TIPOCODICESOGETTODATACHIUSA_DATASCADENZA_2 =
		"estrattoConto.dataScadenza < ?";

	public EstrattoContoPersistenceImpl() {
		setModelClass(EstrattoConto.class);
	}

	/**
	 * Caches the estratto conto in the entity cache if it is enabled.
	 *
	 * @param estrattoConto the estratto conto
	 */
	@Override
	public void cacheResult(EstrattoConto estrattoConto) {
		EntityCacheUtil.putResult(EstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
			EstrattoContoImpl.class, estrattoConto.getPrimaryKey(),
			estrattoConto);

		estrattoConto.resetOriginalValues();
	}

	/**
	 * Caches the estratto contos in the entity cache if it is enabled.
	 *
	 * @param estrattoContos the estratto contos
	 */
	@Override
	public void cacheResult(List<EstrattoConto> estrattoContos) {
		for (EstrattoConto estrattoConto : estrattoContos) {
			if (EntityCacheUtil.getResult(
						EstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
						EstrattoContoImpl.class, estrattoConto.getPrimaryKey()) == null) {
				cacheResult(estrattoConto);
			}
			else {
				estrattoConto.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all estratto contos.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(EstrattoContoImpl.class.getName());
		}

		EntityCacheUtil.clearCache(EstrattoContoImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the estratto conto.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EstrattoConto estrattoConto) {
		EntityCacheUtil.removeResult(EstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
			EstrattoContoImpl.class, estrattoConto.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<EstrattoConto> estrattoContos) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (EstrattoConto estrattoConto : estrattoContos) {
			EntityCacheUtil.removeResult(EstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
				EstrattoContoImpl.class, estrattoConto.getPrimaryKey());
		}
	}

	/**
	 * Creates a new estratto conto with the primary key. Does not add the estratto conto to the database.
	 *
	 * @param estrattoContoPK the primary key for the new estratto conto
	 * @return the new estratto conto
	 */
	@Override
	public EstrattoConto create(EstrattoContoPK estrattoContoPK) {
		EstrattoConto estrattoConto = new EstrattoContoImpl();

		estrattoConto.setNew(true);
		estrattoConto.setPrimaryKey(estrattoContoPK);

		return estrattoConto;
	}

	/**
	 * Removes the estratto conto with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param estrattoContoPK the primary key of the estratto conto
	 * @return the estratto conto that was removed
	 * @throws it.bysoftware.ct.NoSuchEstrattoContoException if a estratto conto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EstrattoConto remove(EstrattoContoPK estrattoContoPK)
		throws NoSuchEstrattoContoException, SystemException {
		return remove((Serializable)estrattoContoPK);
	}

	/**
	 * Removes the estratto conto with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the estratto conto
	 * @return the estratto conto that was removed
	 * @throws it.bysoftware.ct.NoSuchEstrattoContoException if a estratto conto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EstrattoConto remove(Serializable primaryKey)
		throws NoSuchEstrattoContoException, SystemException {
		Session session = null;

		try {
			session = openSession();

			EstrattoConto estrattoConto = (EstrattoConto)session.get(EstrattoContoImpl.class,
					primaryKey);

			if (estrattoConto == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEstrattoContoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(estrattoConto);
		}
		catch (NoSuchEstrattoContoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EstrattoConto removeImpl(EstrattoConto estrattoConto)
		throws SystemException {
		estrattoConto = toUnwrappedModel(estrattoConto);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(estrattoConto)) {
				estrattoConto = (EstrattoConto)session.get(EstrattoContoImpl.class,
						estrattoConto.getPrimaryKeyObj());
			}

			if (estrattoConto != null) {
				session.delete(estrattoConto);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (estrattoConto != null) {
			clearCache(estrattoConto);
		}

		return estrattoConto;
	}

	@Override
	public EstrattoConto updateImpl(
		it.bysoftware.ct.model.EstrattoConto estrattoConto)
		throws SystemException {
		estrattoConto = toUnwrappedModel(estrattoConto);

		boolean isNew = estrattoConto.isNew();

		Session session = null;

		try {
			session = openSession();

			if (estrattoConto.isNew()) {
				session.save(estrattoConto);

				estrattoConto.setNew(false);
			}
			else {
				session.merge(estrattoConto);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !EstrattoContoModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(EstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
			EstrattoContoImpl.class, estrattoConto.getPrimaryKey(),
			estrattoConto);

		return estrattoConto;
	}

	protected EstrattoConto toUnwrappedModel(EstrattoConto estrattoConto) {
		if (estrattoConto instanceof EstrattoContoImpl) {
			return estrattoConto;
		}

		EstrattoContoImpl estrattoContoImpl = new EstrattoContoImpl();

		estrattoContoImpl.setNew(estrattoConto.isNew());
		estrattoContoImpl.setPrimaryKey(estrattoConto.getPrimaryKey());

		estrattoContoImpl.setTipoSoggetto(estrattoConto.isTipoSoggetto());
		estrattoContoImpl.setCodiceCliente(estrattoConto.getCodiceCliente());
		estrattoContoImpl.setEsercizioRegistrazione(estrattoConto.getEsercizioRegistrazione());
		estrattoContoImpl.setNumeroPartita(estrattoConto.getNumeroPartita());
		estrattoContoImpl.setNumeroScadenza(estrattoConto.getNumeroScadenza());
		estrattoContoImpl.setDataScadenza(estrattoConto.getDataScadenza());
		estrattoContoImpl.setCodiceTipoPagam(estrattoConto.getCodiceTipoPagam());
		estrattoContoImpl.setStato(estrattoConto.getStato());
		estrattoContoImpl.setCodiceBanca(estrattoConto.getCodiceBanca());
		estrattoContoImpl.setCodiceContoCorrente(estrattoConto.getCodiceContoCorrente());
		estrattoContoImpl.setCodiceAgente(estrattoConto.getCodiceAgente());
		estrattoContoImpl.setTipoCausale(estrattoConto.getTipoCausale());
		estrattoContoImpl.setEsercizioDocumento(estrattoConto.getEsercizioDocumento());
		estrattoContoImpl.setProtocolloDocumento(estrattoConto.getProtocolloDocumento());
		estrattoContoImpl.setCodiceAttivita(estrattoConto.getCodiceAttivita());
		estrattoContoImpl.setCodiceCentro(estrattoConto.getCodiceCentro());
		estrattoContoImpl.setDataRegistrazione(estrattoConto.getDataRegistrazione());
		estrattoContoImpl.setDataOperazione(estrattoConto.getDataOperazione());
		estrattoContoImpl.setDataAggiornamento(estrattoConto.getDataAggiornamento());
		estrattoContoImpl.setDataChiusura(estrattoConto.getDataChiusura());
		estrattoContoImpl.setCodiceCausale(estrattoConto.getCodiceCausale());
		estrattoContoImpl.setDataDocumento(estrattoConto.getDataDocumento());
		estrattoContoImpl.setNumeroDocumento(estrattoConto.getNumeroDocumento());
		estrattoContoImpl.setCodiceTipoPagamOrig(estrattoConto.getCodiceTipoPagamOrig());
		estrattoContoImpl.setCodiceTipoPagamScad(estrattoConto.getCodiceTipoPagamScad());
		estrattoContoImpl.setCausaleEstrattoContoOrig(estrattoConto.getCausaleEstrattoContoOrig());
		estrattoContoImpl.setCodiceDivisaOrig(estrattoConto.getCodiceDivisaOrig());
		estrattoContoImpl.setImportoTotaleInt(estrattoConto.getImportoTotaleInt());
		estrattoContoImpl.setImportoPagatoInt(estrattoConto.getImportoPagatoInt());
		estrattoContoImpl.setImportoApertoInt(estrattoConto.getImportoApertoInt());
		estrattoContoImpl.setImportoEspostoInt(estrattoConto.getImportoEspostoInt());
		estrattoContoImpl.setImportoScadutoInt(estrattoConto.getImportoScadutoInt());
		estrattoContoImpl.setImportoInsolutoInt(estrattoConto.getImportoInsolutoInt());
		estrattoContoImpl.setImportoInContenziosoInt(estrattoConto.getImportoInContenziosoInt());
		estrattoContoImpl.setImportoTotale(estrattoConto.getImportoTotale());
		estrattoContoImpl.setImportoPagato(estrattoConto.getImportoPagato());
		estrattoContoImpl.setImportoAperto(estrattoConto.getImportoAperto());
		estrattoContoImpl.setImportoEsposto(estrattoConto.getImportoEsposto());
		estrattoContoImpl.setImportoScaduto(estrattoConto.getImportoScaduto());
		estrattoContoImpl.setImportoInsoluto(estrattoConto.getImportoInsoluto());
		estrattoContoImpl.setImportoInContenzioso(estrattoConto.getImportoInContenzioso());
		estrattoContoImpl.setNumeroLettereSollecito(estrattoConto.getNumeroLettereSollecito());
		estrattoContoImpl.setCodiceDivisaInterna(estrattoConto.getCodiceDivisaInterna());
		estrattoContoImpl.setCodiceEsercizioScadenzaConversione(estrattoConto.getCodiceEsercizioScadenzaConversione());
		estrattoContoImpl.setNumeroPartitaScadenzaCoversione(estrattoConto.getNumeroPartitaScadenzaCoversione());
		estrattoContoImpl.setNumeroScadenzaCoversione(estrattoConto.getNumeroScadenzaCoversione());
		estrattoContoImpl.setTipoCoversione(estrattoConto.getTipoCoversione());
		estrattoContoImpl.setCodiceEsercizioScritturaConversione(estrattoConto.getCodiceEsercizioScritturaConversione());
		estrattoContoImpl.setNumeroRegistrazioneScritturaConversione(estrattoConto.getNumeroRegistrazioneScritturaConversione());
		estrattoContoImpl.setDataRegistrazioneScritturaConversione(estrattoConto.getDataRegistrazioneScritturaConversione());
		estrattoContoImpl.setCodiceDivisaIntScadenzaConversione(estrattoConto.getCodiceDivisaIntScadenzaConversione());
		estrattoContoImpl.setCodiceDivisaScadenzaConversione(estrattoConto.getCodiceDivisaScadenzaConversione());
		estrattoContoImpl.setCambioScadenzaConversione(estrattoConto.getCambioScadenzaConversione());

		return estrattoContoImpl;
	}

	/**
	 * Returns the estratto conto with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the estratto conto
	 * @return the estratto conto
	 * @throws it.bysoftware.ct.NoSuchEstrattoContoException if a estratto conto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EstrattoConto findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEstrattoContoException, SystemException {
		EstrattoConto estrattoConto = fetchByPrimaryKey(primaryKey);

		if (estrattoConto == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEstrattoContoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return estrattoConto;
	}

	/**
	 * Returns the estratto conto with the primary key or throws a {@link it.bysoftware.ct.NoSuchEstrattoContoException} if it could not be found.
	 *
	 * @param estrattoContoPK the primary key of the estratto conto
	 * @return the estratto conto
	 * @throws it.bysoftware.ct.NoSuchEstrattoContoException if a estratto conto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EstrattoConto findByPrimaryKey(EstrattoContoPK estrattoContoPK)
		throws NoSuchEstrattoContoException, SystemException {
		return findByPrimaryKey((Serializable)estrattoContoPK);
	}

	/**
	 * Returns the estratto conto with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the estratto conto
	 * @return the estratto conto, or <code>null</code> if a estratto conto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EstrattoConto fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		EstrattoConto estrattoConto = (EstrattoConto)EntityCacheUtil.getResult(EstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
				EstrattoContoImpl.class, primaryKey);

		if (estrattoConto == _nullEstrattoConto) {
			return null;
		}

		if (estrattoConto == null) {
			Session session = null;

			try {
				session = openSession();

				estrattoConto = (EstrattoConto)session.get(EstrattoContoImpl.class,
						primaryKey);

				if (estrattoConto != null) {
					cacheResult(estrattoConto);
				}
				else {
					EntityCacheUtil.putResult(EstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
						EstrattoContoImpl.class, primaryKey, _nullEstrattoConto);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(EstrattoContoModelImpl.ENTITY_CACHE_ENABLED,
					EstrattoContoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return estrattoConto;
	}

	/**
	 * Returns the estratto conto with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param estrattoContoPK the primary key of the estratto conto
	 * @return the estratto conto, or <code>null</code> if a estratto conto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EstrattoConto fetchByPrimaryKey(EstrattoContoPK estrattoContoPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)estrattoContoPK);
	}

	/**
	 * Returns all the estratto contos.
	 *
	 * @return the estratto contos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EstrattoConto> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the estratto contos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of estratto contos
	 * @param end the upper bound of the range of estratto contos (not inclusive)
	 * @return the range of estratto contos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EstrattoConto> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the estratto contos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of estratto contos
	 * @param end the upper bound of the range of estratto contos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of estratto contos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EstrattoConto> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<EstrattoConto> list = (List<EstrattoConto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ESTRATTOCONTO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ESTRATTOCONTO;

				if (pagination) {
					sql = sql.concat(EstrattoContoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<EstrattoConto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EstrattoConto>(list);
				}
				else {
					list = (List<EstrattoConto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the estratto contos from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (EstrattoConto estrattoConto : findAll()) {
			remove(estrattoConto);
		}
	}

	/**
	 * Returns the number of estratto contos.
	 *
	 * @return the number of estratto contos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ESTRATTOCONTO);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the estratto conto persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.EstrattoConto")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<EstrattoConto>> listenersList = new ArrayList<ModelListener<EstrattoConto>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<EstrattoConto>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(EstrattoContoImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ESTRATTOCONTO = "SELECT estrattoConto FROM EstrattoConto estrattoConto";
	private static final String _SQL_SELECT_ESTRATTOCONTO_WHERE = "SELECT estrattoConto FROM EstrattoConto estrattoConto WHERE ";
	private static final String _SQL_COUNT_ESTRATTOCONTO = "SELECT COUNT(estrattoConto) FROM EstrattoConto estrattoConto";
	private static final String _SQL_COUNT_ESTRATTOCONTO_WHERE = "SELECT COUNT(estrattoConto) FROM EstrattoConto estrattoConto WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "estrattoConto.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No EstrattoConto exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No EstrattoConto exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(EstrattoContoPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"tipoSoggetto", "codiceCliente", "esercizioRegistrazione",
				"numeroPartita", "numeroScadenza", "dataScadenza",
				"codiceTipoPagam", "stato", "codiceBanca", "codiceContoCorrente",
				"codiceAgente", "tipoCausale", "esercizioDocumento",
				"protocolloDocumento", "codiceAttivita", "codiceCentro",
				"dataRegistrazione", "dataOperazione", "dataAggiornamento",
				"dataChiusura", "codiceCausale", "dataDocumento",
				"numeroDocumento", "codiceTipoPagamOrig", "codiceTipoPagamScad",
				"causaleEstrattoContoOrig", "codiceDivisaOrig",
				"importoTotaleInt", "importoPagatoInt", "importoApertoInt",
				"importoEspostoInt", "importoScadutoInt", "importoInsolutoInt",
				"importoInContenziosoInt", "importoTotale", "importoPagato",
				"importoAperto", "importoEsposto", "importoScaduto",
				"importoInsoluto", "importoInContenzioso",
				"numeroLettereSollecito", "codiceDivisaInterna",
				"codiceEsercizioScadenzaConversione",
				"numeroPartitaScadenzaCoversione", "numeroScadenzaCoversione",
				"tipoCoversione", "codiceEsercizioScritturaConversione",
				"numeroRegistrazioneScritturaConversione",
				"dataRegistrazioneScritturaConversione",
				"codiceDivisaIntScadenzaConversione",
				"codiceDivisaScadenzaConversione", "cambioScadenzaConversione"
			});
	private static EstrattoConto _nullEstrattoConto = new EstrattoContoImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<EstrattoConto> toCacheModel() {
				return _nullEstrattoContoCacheModel;
			}
		};

	private static CacheModel<EstrattoConto> _nullEstrattoContoCacheModel = new CacheModel<EstrattoConto>() {
			@Override
			public EstrattoConto toEntityModel() {
				return _nullEstrattoConto;
			}
		};
}