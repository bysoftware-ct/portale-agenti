/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException;
import it.bysoftware.ct.model.AnagraficaClientiFornitori;
import it.bysoftware.ct.model.impl.AnagraficaClientiFornitoriImpl;
import it.bysoftware.ct.model.impl.AnagraficaClientiFornitoriModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the anagrafica clienti fornitori service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see AnagraficaClientiFornitoriPersistence
 * @see AnagraficaClientiFornitoriUtil
 * @generated
 */
public class AnagraficaClientiFornitoriPersistenceImpl
	extends BasePersistenceImpl<AnagraficaClientiFornitori>
	implements AnagraficaClientiFornitoriPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AnagraficaClientiFornitoriUtil} to access the anagrafica clienti fornitori persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AnagraficaClientiFornitoriImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AnagraficaClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			AnagraficaClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
			AnagraficaClientiFornitoriImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AnagraficaClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			AnagraficaClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
			AnagraficaClientiFornitoriImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AnagraficaClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			AnagraficaClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_RAGIONESOCIALE =
		new FinderPath(AnagraficaClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			AnagraficaClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
			AnagraficaClientiFornitoriImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByRagioneSociale",
			new String[] {
				String.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_RAGIONESOCIALE =
		new FinderPath(AnagraficaClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			AnagraficaClientiFornitoriModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"countByRagioneSociale",
			new String[] { String.class.getName(), String.class.getName() });

	/**
	 * Returns all the anagrafica clienti fornitoris where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	 *
	 * @param comune the comune
	 * @param ragioneSociale the ragione sociale
	 * @return the matching anagrafica clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AnagraficaClientiFornitori> findByRagioneSociale(
		String comune, String ragioneSociale) throws SystemException {
		return findByRagioneSociale(comune, ragioneSociale, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the anagrafica clienti fornitoris where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficaClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param comune the comune
	 * @param ragioneSociale the ragione sociale
	 * @param start the lower bound of the range of anagrafica clienti fornitoris
	 * @param end the upper bound of the range of anagrafica clienti fornitoris (not inclusive)
	 * @return the range of matching anagrafica clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AnagraficaClientiFornitori> findByRagioneSociale(
		String comune, String ragioneSociale, int start, int end)
		throws SystemException {
		return findByRagioneSociale(comune, ragioneSociale, start, end, null);
	}

	/**
	 * Returns an ordered range of all the anagrafica clienti fornitoris where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficaClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param comune the comune
	 * @param ragioneSociale the ragione sociale
	 * @param start the lower bound of the range of anagrafica clienti fornitoris
	 * @param end the upper bound of the range of anagrafica clienti fornitoris (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching anagrafica clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AnagraficaClientiFornitori> findByRagioneSociale(
		String comune, String ragioneSociale, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_RAGIONESOCIALE;
		finderArgs = new Object[] {
				comune, ragioneSociale,
				
				start, end, orderByComparator
			};

		List<AnagraficaClientiFornitori> list = (List<AnagraficaClientiFornitori>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (AnagraficaClientiFornitori anagraficaClientiFornitori : list) {
				if (!StringUtil.wildcardMatches(
							anagraficaClientiFornitori.getComune(), comune,
							CharPool.UNDERLINE, CharPool.PERCENT,
							CharPool.BACK_SLASH, true) ||
						!StringUtil.wildcardMatches(
							anagraficaClientiFornitori.getRagioneSociale(),
							ragioneSociale, CharPool.UNDERLINE,
							CharPool.PERCENT, CharPool.BACK_SLASH, true)) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ANAGRAFICACLIENTIFORNITORI_WHERE);

			boolean bindComune = false;

			if (comune == null) {
				query.append(_FINDER_COLUMN_RAGIONESOCIALE_COMUNE_1);
			}
			else if (comune.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_RAGIONESOCIALE_COMUNE_3);
			}
			else {
				bindComune = true;

				query.append(_FINDER_COLUMN_RAGIONESOCIALE_COMUNE_2);
			}

			boolean bindRagioneSociale = false;

			if (ragioneSociale == null) {
				query.append(_FINDER_COLUMN_RAGIONESOCIALE_RAGIONESOCIALE_1);
			}
			else if (ragioneSociale.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_RAGIONESOCIALE_RAGIONESOCIALE_3);
			}
			else {
				bindRagioneSociale = true;

				query.append(_FINDER_COLUMN_RAGIONESOCIALE_RAGIONESOCIALE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(AnagraficaClientiFornitoriModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindComune) {
					qPos.add(comune);
				}

				if (bindRagioneSociale) {
					qPos.add(ragioneSociale);
				}

				if (!pagination) {
					list = (List<AnagraficaClientiFornitori>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<AnagraficaClientiFornitori>(list);
				}
				else {
					list = (List<AnagraficaClientiFornitori>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first anagrafica clienti fornitori in the ordered set where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	 *
	 * @param comune the comune
	 * @param ragioneSociale the ragione sociale
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching anagrafica clienti fornitori
	 * @throws it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException if a matching anagrafica clienti fornitori could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaClientiFornitori findByRagioneSociale_First(
		String comune, String ragioneSociale,
		OrderByComparator orderByComparator)
		throws NoSuchAnagraficaClientiFornitoriException, SystemException {
		AnagraficaClientiFornitori anagraficaClientiFornitori = fetchByRagioneSociale_First(comune,
				ragioneSociale, orderByComparator);

		if (anagraficaClientiFornitori != null) {
			return anagraficaClientiFornitori;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("comune=");
		msg.append(comune);

		msg.append(", ragioneSociale=");
		msg.append(ragioneSociale);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAnagraficaClientiFornitoriException(msg.toString());
	}

	/**
	 * Returns the first anagrafica clienti fornitori in the ordered set where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	 *
	 * @param comune the comune
	 * @param ragioneSociale the ragione sociale
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching anagrafica clienti fornitori, or <code>null</code> if a matching anagrafica clienti fornitori could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaClientiFornitori fetchByRagioneSociale_First(
		String comune, String ragioneSociale,
		OrderByComparator orderByComparator) throws SystemException {
		List<AnagraficaClientiFornitori> list = findByRagioneSociale(comune,
				ragioneSociale, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last anagrafica clienti fornitori in the ordered set where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	 *
	 * @param comune the comune
	 * @param ragioneSociale the ragione sociale
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching anagrafica clienti fornitori
	 * @throws it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException if a matching anagrafica clienti fornitori could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaClientiFornitori findByRagioneSociale_Last(String comune,
		String ragioneSociale, OrderByComparator orderByComparator)
		throws NoSuchAnagraficaClientiFornitoriException, SystemException {
		AnagraficaClientiFornitori anagraficaClientiFornitori = fetchByRagioneSociale_Last(comune,
				ragioneSociale, orderByComparator);

		if (anagraficaClientiFornitori != null) {
			return anagraficaClientiFornitori;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("comune=");
		msg.append(comune);

		msg.append(", ragioneSociale=");
		msg.append(ragioneSociale);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAnagraficaClientiFornitoriException(msg.toString());
	}

	/**
	 * Returns the last anagrafica clienti fornitori in the ordered set where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	 *
	 * @param comune the comune
	 * @param ragioneSociale the ragione sociale
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching anagrafica clienti fornitori, or <code>null</code> if a matching anagrafica clienti fornitori could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaClientiFornitori fetchByRagioneSociale_Last(
		String comune, String ragioneSociale,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByRagioneSociale(comune, ragioneSociale);

		if (count == 0) {
			return null;
		}

		List<AnagraficaClientiFornitori> list = findByRagioneSociale(comune,
				ragioneSociale, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the anagrafica clienti fornitoris before and after the current anagrafica clienti fornitori in the ordered set where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	 *
	 * @param codiceSoggetto the primary key of the current anagrafica clienti fornitori
	 * @param comune the comune
	 * @param ragioneSociale the ragione sociale
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next anagrafica clienti fornitori
	 * @throws it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException if a anagrafica clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaClientiFornitori[] findByRagioneSociale_PrevAndNext(
		String codiceSoggetto, String comune, String ragioneSociale,
		OrderByComparator orderByComparator)
		throws NoSuchAnagraficaClientiFornitoriException, SystemException {
		AnagraficaClientiFornitori anagraficaClientiFornitori = findByPrimaryKey(codiceSoggetto);

		Session session = null;

		try {
			session = openSession();

			AnagraficaClientiFornitori[] array = new AnagraficaClientiFornitoriImpl[3];

			array[0] = getByRagioneSociale_PrevAndNext(session,
					anagraficaClientiFornitori, comune, ragioneSociale,
					orderByComparator, true);

			array[1] = anagraficaClientiFornitori;

			array[2] = getByRagioneSociale_PrevAndNext(session,
					anagraficaClientiFornitori, comune, ragioneSociale,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AnagraficaClientiFornitori getByRagioneSociale_PrevAndNext(
		Session session, AnagraficaClientiFornitori anagraficaClientiFornitori,
		String comune, String ragioneSociale,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ANAGRAFICACLIENTIFORNITORI_WHERE);

		boolean bindComune = false;

		if (comune == null) {
			query.append(_FINDER_COLUMN_RAGIONESOCIALE_COMUNE_1);
		}
		else if (comune.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_RAGIONESOCIALE_COMUNE_3);
		}
		else {
			bindComune = true;

			query.append(_FINDER_COLUMN_RAGIONESOCIALE_COMUNE_2);
		}

		boolean bindRagioneSociale = false;

		if (ragioneSociale == null) {
			query.append(_FINDER_COLUMN_RAGIONESOCIALE_RAGIONESOCIALE_1);
		}
		else if (ragioneSociale.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_RAGIONESOCIALE_RAGIONESOCIALE_3);
		}
		else {
			bindRagioneSociale = true;

			query.append(_FINDER_COLUMN_RAGIONESOCIALE_RAGIONESOCIALE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(AnagraficaClientiFornitoriModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindComune) {
			qPos.add(comune);
		}

		if (bindRagioneSociale) {
			qPos.add(ragioneSociale);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(anagraficaClientiFornitori);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AnagraficaClientiFornitori> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the anagrafica clienti fornitoris where comune LIKE &#63; and ragioneSociale LIKE &#63; from the database.
	 *
	 * @param comune the comune
	 * @param ragioneSociale the ragione sociale
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByRagioneSociale(String comune, String ragioneSociale)
		throws SystemException {
		for (AnagraficaClientiFornitori anagraficaClientiFornitori : findByRagioneSociale(
				comune, ragioneSociale, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
				null)) {
			remove(anagraficaClientiFornitori);
		}
	}

	/**
	 * Returns the number of anagrafica clienti fornitoris where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	 *
	 * @param comune the comune
	 * @param ragioneSociale the ragione sociale
	 * @return the number of matching anagrafica clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByRagioneSociale(String comune, String ragioneSociale)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_RAGIONESOCIALE;

		Object[] finderArgs = new Object[] { comune, ragioneSociale };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ANAGRAFICACLIENTIFORNITORI_WHERE);

			boolean bindComune = false;

			if (comune == null) {
				query.append(_FINDER_COLUMN_RAGIONESOCIALE_COMUNE_1);
			}
			else if (comune.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_RAGIONESOCIALE_COMUNE_3);
			}
			else {
				bindComune = true;

				query.append(_FINDER_COLUMN_RAGIONESOCIALE_COMUNE_2);
			}

			boolean bindRagioneSociale = false;

			if (ragioneSociale == null) {
				query.append(_FINDER_COLUMN_RAGIONESOCIALE_RAGIONESOCIALE_1);
			}
			else if (ragioneSociale.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_RAGIONESOCIALE_RAGIONESOCIALE_3);
			}
			else {
				bindRagioneSociale = true;

				query.append(_FINDER_COLUMN_RAGIONESOCIALE_RAGIONESOCIALE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindComune) {
					qPos.add(comune);
				}

				if (bindRagioneSociale) {
					qPos.add(ragioneSociale);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_RAGIONESOCIALE_COMUNE_1 = "anagraficaClientiFornitori.comune LIKE NULL AND ";
	private static final String _FINDER_COLUMN_RAGIONESOCIALE_COMUNE_2 = "anagraficaClientiFornitori.comune LIKE ? AND ";
	private static final String _FINDER_COLUMN_RAGIONESOCIALE_COMUNE_3 = "(anagraficaClientiFornitori.comune IS NULL OR anagraficaClientiFornitori.comune LIKE '') AND ";
	private static final String _FINDER_COLUMN_RAGIONESOCIALE_RAGIONESOCIALE_1 = "anagraficaClientiFornitori.ragioneSociale LIKE NULL";
	private static final String _FINDER_COLUMN_RAGIONESOCIALE_RAGIONESOCIALE_2 = "anagraficaClientiFornitori.ragioneSociale LIKE ?";
	private static final String _FINDER_COLUMN_RAGIONESOCIALE_RAGIONESOCIALE_3 = "(anagraficaClientiFornitori.ragioneSociale IS NULL OR anagraficaClientiFornitori.ragioneSociale LIKE '')";

	public AnagraficaClientiFornitoriPersistenceImpl() {
		setModelClass(AnagraficaClientiFornitori.class);
	}

	/**
	 * Caches the anagrafica clienti fornitori in the entity cache if it is enabled.
	 *
	 * @param anagraficaClientiFornitori the anagrafica clienti fornitori
	 */
	@Override
	public void cacheResult(
		AnagraficaClientiFornitori anagraficaClientiFornitori) {
		EntityCacheUtil.putResult(AnagraficaClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			AnagraficaClientiFornitoriImpl.class,
			anagraficaClientiFornitori.getPrimaryKey(),
			anagraficaClientiFornitori);

		anagraficaClientiFornitori.resetOriginalValues();
	}

	/**
	 * Caches the anagrafica clienti fornitoris in the entity cache if it is enabled.
	 *
	 * @param anagraficaClientiFornitoris the anagrafica clienti fornitoris
	 */
	@Override
	public void cacheResult(
		List<AnagraficaClientiFornitori> anagraficaClientiFornitoris) {
		for (AnagraficaClientiFornitori anagraficaClientiFornitori : anagraficaClientiFornitoris) {
			if (EntityCacheUtil.getResult(
						AnagraficaClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
						AnagraficaClientiFornitoriImpl.class,
						anagraficaClientiFornitori.getPrimaryKey()) == null) {
				cacheResult(anagraficaClientiFornitori);
			}
			else {
				anagraficaClientiFornitori.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all anagrafica clienti fornitoris.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AnagraficaClientiFornitoriImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AnagraficaClientiFornitoriImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the anagrafica clienti fornitori.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(
		AnagraficaClientiFornitori anagraficaClientiFornitori) {
		EntityCacheUtil.removeResult(AnagraficaClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			AnagraficaClientiFornitoriImpl.class,
			anagraficaClientiFornitori.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(
		List<AnagraficaClientiFornitori> anagraficaClientiFornitoris) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (AnagraficaClientiFornitori anagraficaClientiFornitori : anagraficaClientiFornitoris) {
			EntityCacheUtil.removeResult(AnagraficaClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
				AnagraficaClientiFornitoriImpl.class,
				anagraficaClientiFornitori.getPrimaryKey());
		}
	}

	/**
	 * Creates a new anagrafica clienti fornitori with the primary key. Does not add the anagrafica clienti fornitori to the database.
	 *
	 * @param codiceSoggetto the primary key for the new anagrafica clienti fornitori
	 * @return the new anagrafica clienti fornitori
	 */
	@Override
	public AnagraficaClientiFornitori create(String codiceSoggetto) {
		AnagraficaClientiFornitori anagraficaClientiFornitori = new AnagraficaClientiFornitoriImpl();

		anagraficaClientiFornitori.setNew(true);
		anagraficaClientiFornitori.setPrimaryKey(codiceSoggetto);

		return anagraficaClientiFornitori;
	}

	/**
	 * Removes the anagrafica clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param codiceSoggetto the primary key of the anagrafica clienti fornitori
	 * @return the anagrafica clienti fornitori that was removed
	 * @throws it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException if a anagrafica clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaClientiFornitori remove(String codiceSoggetto)
		throws NoSuchAnagraficaClientiFornitoriException, SystemException {
		return remove((Serializable)codiceSoggetto);
	}

	/**
	 * Removes the anagrafica clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the anagrafica clienti fornitori
	 * @return the anagrafica clienti fornitori that was removed
	 * @throws it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException if a anagrafica clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaClientiFornitori remove(Serializable primaryKey)
		throws NoSuchAnagraficaClientiFornitoriException, SystemException {
		Session session = null;

		try {
			session = openSession();

			AnagraficaClientiFornitori anagraficaClientiFornitori = (AnagraficaClientiFornitori)session.get(AnagraficaClientiFornitoriImpl.class,
					primaryKey);

			if (anagraficaClientiFornitori == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAnagraficaClientiFornitoriException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(anagraficaClientiFornitori);
		}
		catch (NoSuchAnagraficaClientiFornitoriException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AnagraficaClientiFornitori removeImpl(
		AnagraficaClientiFornitori anagraficaClientiFornitori)
		throws SystemException {
		anagraficaClientiFornitori = toUnwrappedModel(anagraficaClientiFornitori);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(anagraficaClientiFornitori)) {
				anagraficaClientiFornitori = (AnagraficaClientiFornitori)session.get(AnagraficaClientiFornitoriImpl.class,
						anagraficaClientiFornitori.getPrimaryKeyObj());
			}

			if (anagraficaClientiFornitori != null) {
				session.delete(anagraficaClientiFornitori);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (anagraficaClientiFornitori != null) {
			clearCache(anagraficaClientiFornitori);
		}

		return anagraficaClientiFornitori;
	}

	@Override
	public AnagraficaClientiFornitori updateImpl(
		it.bysoftware.ct.model.AnagraficaClientiFornitori anagraficaClientiFornitori)
		throws SystemException {
		anagraficaClientiFornitori = toUnwrappedModel(anagraficaClientiFornitori);

		boolean isNew = anagraficaClientiFornitori.isNew();

		Session session = null;

		try {
			session = openSession();

			if (anagraficaClientiFornitori.isNew()) {
				session.save(anagraficaClientiFornitori);

				anagraficaClientiFornitori.setNew(false);
			}
			else {
				session.merge(anagraficaClientiFornitori);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew ||
				!AnagraficaClientiFornitoriModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(AnagraficaClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
			AnagraficaClientiFornitoriImpl.class,
			anagraficaClientiFornitori.getPrimaryKey(),
			anagraficaClientiFornitori);

		return anagraficaClientiFornitori;
	}

	protected AnagraficaClientiFornitori toUnwrappedModel(
		AnagraficaClientiFornitori anagraficaClientiFornitori) {
		if (anagraficaClientiFornitori instanceof AnagraficaClientiFornitoriImpl) {
			return anagraficaClientiFornitori;
		}

		AnagraficaClientiFornitoriImpl anagraficaClientiFornitoriImpl = new AnagraficaClientiFornitoriImpl();

		anagraficaClientiFornitoriImpl.setNew(anagraficaClientiFornitori.isNew());
		anagraficaClientiFornitoriImpl.setPrimaryKey(anagraficaClientiFornitori.getPrimaryKey());

		anagraficaClientiFornitoriImpl.setCodiceSoggetto(anagraficaClientiFornitori.getCodiceSoggetto());
		anagraficaClientiFornitoriImpl.setRagioneSociale(anagraficaClientiFornitori.getRagioneSociale());
		anagraficaClientiFornitoriImpl.setRagioneSocialeAggiuntiva(anagraficaClientiFornitori.getRagioneSocialeAggiuntiva());
		anagraficaClientiFornitoriImpl.setIndirizzo(anagraficaClientiFornitori.getIndirizzo());
		anagraficaClientiFornitoriImpl.setComune(anagraficaClientiFornitori.getComune());
		anagraficaClientiFornitoriImpl.setCAP(anagraficaClientiFornitori.getCAP());
		anagraficaClientiFornitoriImpl.setSiglaProvincia(anagraficaClientiFornitori.getSiglaProvincia());
		anagraficaClientiFornitoriImpl.setSiglaStato(anagraficaClientiFornitori.getSiglaStato());
		anagraficaClientiFornitoriImpl.setPartitaIVA(anagraficaClientiFornitori.getPartitaIVA());
		anagraficaClientiFornitoriImpl.setCodiceFiscale(anagraficaClientiFornitori.getCodiceFiscale());
		anagraficaClientiFornitoriImpl.setNote(anagraficaClientiFornitori.getNote());
		anagraficaClientiFornitoriImpl.setTipoSoggetto(anagraficaClientiFornitori.isTipoSoggetto());
		anagraficaClientiFornitoriImpl.setCodiceMnemonico(anagraficaClientiFornitori.getCodiceMnemonico());
		anagraficaClientiFornitoriImpl.setTipoSollecito(anagraficaClientiFornitori.getTipoSollecito());
		anagraficaClientiFornitoriImpl.setAttivoEC(anagraficaClientiFornitori.isAttivoEC());

		return anagraficaClientiFornitoriImpl;
	}

	/**
	 * Returns the anagrafica clienti fornitori with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the anagrafica clienti fornitori
	 * @return the anagrafica clienti fornitori
	 * @throws it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException if a anagrafica clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaClientiFornitori findByPrimaryKey(Serializable primaryKey)
		throws NoSuchAnagraficaClientiFornitoriException, SystemException {
		AnagraficaClientiFornitori anagraficaClientiFornitori = fetchByPrimaryKey(primaryKey);

		if (anagraficaClientiFornitori == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchAnagraficaClientiFornitoriException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return anagraficaClientiFornitori;
	}

	/**
	 * Returns the anagrafica clienti fornitori with the primary key or throws a {@link it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException} if it could not be found.
	 *
	 * @param codiceSoggetto the primary key of the anagrafica clienti fornitori
	 * @return the anagrafica clienti fornitori
	 * @throws it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException if a anagrafica clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaClientiFornitori findByPrimaryKey(String codiceSoggetto)
		throws NoSuchAnagraficaClientiFornitoriException, SystemException {
		return findByPrimaryKey((Serializable)codiceSoggetto);
	}

	/**
	 * Returns the anagrafica clienti fornitori with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the anagrafica clienti fornitori
	 * @return the anagrafica clienti fornitori, or <code>null</code> if a anagrafica clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaClientiFornitori fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		AnagraficaClientiFornitori anagraficaClientiFornitori = (AnagraficaClientiFornitori)EntityCacheUtil.getResult(AnagraficaClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
				AnagraficaClientiFornitoriImpl.class, primaryKey);

		if (anagraficaClientiFornitori == _nullAnagraficaClientiFornitori) {
			return null;
		}

		if (anagraficaClientiFornitori == null) {
			Session session = null;

			try {
				session = openSession();

				anagraficaClientiFornitori = (AnagraficaClientiFornitori)session.get(AnagraficaClientiFornitoriImpl.class,
						primaryKey);

				if (anagraficaClientiFornitori != null) {
					cacheResult(anagraficaClientiFornitori);
				}
				else {
					EntityCacheUtil.putResult(AnagraficaClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
						AnagraficaClientiFornitoriImpl.class, primaryKey,
						_nullAnagraficaClientiFornitori);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(AnagraficaClientiFornitoriModelImpl.ENTITY_CACHE_ENABLED,
					AnagraficaClientiFornitoriImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return anagraficaClientiFornitori;
	}

	/**
	 * Returns the anagrafica clienti fornitori with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param codiceSoggetto the primary key of the anagrafica clienti fornitori
	 * @return the anagrafica clienti fornitori, or <code>null</code> if a anagrafica clienti fornitori with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AnagraficaClientiFornitori fetchByPrimaryKey(String codiceSoggetto)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)codiceSoggetto);
	}

	/**
	 * Returns all the anagrafica clienti fornitoris.
	 *
	 * @return the anagrafica clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AnagraficaClientiFornitori> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the anagrafica clienti fornitoris.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficaClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of anagrafica clienti fornitoris
	 * @param end the upper bound of the range of anagrafica clienti fornitoris (not inclusive)
	 * @return the range of anagrafica clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AnagraficaClientiFornitori> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the anagrafica clienti fornitoris.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficaClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of anagrafica clienti fornitoris
	 * @param end the upper bound of the range of anagrafica clienti fornitoris (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of anagrafica clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AnagraficaClientiFornitori> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<AnagraficaClientiFornitori> list = (List<AnagraficaClientiFornitori>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ANAGRAFICACLIENTIFORNITORI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ANAGRAFICACLIENTIFORNITORI;

				if (pagination) {
					sql = sql.concat(AnagraficaClientiFornitoriModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<AnagraficaClientiFornitori>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<AnagraficaClientiFornitori>(list);
				}
				else {
					list = (List<AnagraficaClientiFornitori>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the anagrafica clienti fornitoris from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (AnagraficaClientiFornitori anagraficaClientiFornitori : findAll()) {
			remove(anagraficaClientiFornitori);
		}
	}

	/**
	 * Returns the number of anagrafica clienti fornitoris.
	 *
	 * @return the number of anagrafica clienti fornitoris
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ANAGRAFICACLIENTIFORNITORI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the anagrafica clienti fornitori persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.AnagraficaClientiFornitori")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<AnagraficaClientiFornitori>> listenersList = new ArrayList<ModelListener<AnagraficaClientiFornitori>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<AnagraficaClientiFornitori>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AnagraficaClientiFornitoriImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ANAGRAFICACLIENTIFORNITORI = "SELECT anagraficaClientiFornitori FROM AnagraficaClientiFornitori anagraficaClientiFornitori";
	private static final String _SQL_SELECT_ANAGRAFICACLIENTIFORNITORI_WHERE = "SELECT anagraficaClientiFornitori FROM AnagraficaClientiFornitori anagraficaClientiFornitori WHERE ";
	private static final String _SQL_COUNT_ANAGRAFICACLIENTIFORNITORI = "SELECT COUNT(anagraficaClientiFornitori) FROM AnagraficaClientiFornitori anagraficaClientiFornitori";
	private static final String _SQL_COUNT_ANAGRAFICACLIENTIFORNITORI_WHERE = "SELECT COUNT(anagraficaClientiFornitori) FROM AnagraficaClientiFornitori anagraficaClientiFornitori WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "anagraficaClientiFornitori.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AnagraficaClientiFornitori exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No AnagraficaClientiFornitori exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AnagraficaClientiFornitoriPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"codiceSoggetto", "ragioneSociale", "ragioneSocialeAggiuntiva",
				"indirizzo", "comune", "CAP", "siglaProvincia", "siglaStato",
				"partitaIVA", "codiceFiscale", "note", "tipoSoggetto",
				"codiceMnemonico", "tipoSollecito", "attivoEC"
			});
	private static AnagraficaClientiFornitori _nullAnagraficaClientiFornitori = new AnagraficaClientiFornitoriImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<AnagraficaClientiFornitori> toCacheModel() {
				return _nullAnagraficaClientiFornitoriCacheModel;
			}
		};

	private static CacheModel<AnagraficaClientiFornitori> _nullAnagraficaClientiFornitoriCacheModel =
		new CacheModel<AnagraficaClientiFornitori>() {
			@Override
			public AnagraficaClientiFornitori toEntityModel() {
				return _nullAnagraficaClientiFornitori;
			}
		};
}