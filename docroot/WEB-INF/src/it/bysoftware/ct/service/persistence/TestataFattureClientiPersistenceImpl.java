/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchTestataFattureClientiException;
import it.bysoftware.ct.model.TestataFattureClienti;
import it.bysoftware.ct.model.impl.TestataFattureClientiImpl;
import it.bysoftware.ct.model.impl.TestataFattureClientiModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the testata fatture clienti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see TestataFattureClientiPersistence
 * @see TestataFattureClientiUtil
 * @generated
 */
public class TestataFattureClientiPersistenceImpl extends BasePersistenceImpl<TestataFattureClienti>
	implements TestataFattureClientiPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link TestataFattureClientiUtil} to access the testata fatture clienti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = TestataFattureClientiImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TestataFattureClientiModelImpl.ENTITY_CACHE_ENABLED,
			TestataFattureClientiModelImpl.FINDER_CACHE_ENABLED,
			TestataFattureClientiImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TestataFattureClientiModelImpl.ENTITY_CACHE_ENABLED,
			TestataFattureClientiModelImpl.FINDER_CACHE_ENABLED,
			TestataFattureClientiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TestataFattureClientiModelImpl.ENTITY_CACHE_ENABLED,
			TestataFattureClientiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO =
		new FinderPath(TestataFattureClientiModelImpl.ENTITY_CACHE_ENABLED,
			TestataFattureClientiModelImpl.FINDER_CACHE_ENABLED,
			TestataFattureClientiImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByAnnoCodiceAttivitaCodiceCentroNumeroDocumento",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				String.class.getName(), Integer.class.getName(),
				Date.class.getName(), Boolean.class.getName(),
				String.class.getName()
			},
			TestataFattureClientiModelImpl.ANNO_COLUMN_BITMASK |
			TestataFattureClientiModelImpl.CODICEATTIVITA_COLUMN_BITMASK |
			TestataFattureClientiModelImpl.CODICECENTRO_COLUMN_BITMASK |
			TestataFattureClientiModelImpl.NUMERODOCUMENTO_COLUMN_BITMASK |
			TestataFattureClientiModelImpl.DATADOCUMENTO_COLUMN_BITMASK |
			TestataFattureClientiModelImpl.TIPOSOGGETTO_COLUMN_BITMASK |
			TestataFattureClientiModelImpl.CODICECLIENTE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO =
		new FinderPath(TestataFattureClientiModelImpl.ENTITY_CACHE_ENABLED,
			TestataFattureClientiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByAnnoCodiceAttivitaCodiceCentroNumeroDocumento",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				String.class.getName(), Integer.class.getName(),
				Date.class.getName(), Boolean.class.getName(),
				String.class.getName()
			});

	/**
	 * Returns the testata fatture clienti where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroDocumento = &#63; and dataDocumento = &#63; and tipoSoggetto = &#63; and codiceCliente = &#63; or throws a {@link it.bysoftware.ct.NoSuchTestataFattureClientiException} if it could not be found.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param numeroDocumento the numero documento
	 * @param dataDocumento the data documento
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceCliente the codice cliente
	 * @return the matching testata fatture clienti
	 * @throws it.bysoftware.ct.NoSuchTestataFattureClientiException if a matching testata fatture clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestataFattureClienti findByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(
		int anno, String codiceAttivita, String codiceCentro,
		int numeroDocumento, Date dataDocumento, boolean tipoSoggetto,
		String codiceCliente)
		throws NoSuchTestataFattureClientiException, SystemException {
		TestataFattureClienti testataFattureClienti = fetchByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(anno,
				codiceAttivita, codiceCentro, numeroDocumento, dataDocumento,
				tipoSoggetto, codiceCliente);

		if (testataFattureClienti == null) {
			StringBundler msg = new StringBundler(16);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("anno=");
			msg.append(anno);

			msg.append(", codiceAttivita=");
			msg.append(codiceAttivita);

			msg.append(", codiceCentro=");
			msg.append(codiceCentro);

			msg.append(", numeroDocumento=");
			msg.append(numeroDocumento);

			msg.append(", dataDocumento=");
			msg.append(dataDocumento);

			msg.append(", tipoSoggetto=");
			msg.append(tipoSoggetto);

			msg.append(", codiceCliente=");
			msg.append(codiceCliente);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchTestataFattureClientiException(msg.toString());
		}

		return testataFattureClienti;
	}

	/**
	 * Returns the testata fatture clienti where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroDocumento = &#63; and dataDocumento = &#63; and tipoSoggetto = &#63; and codiceCliente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param numeroDocumento the numero documento
	 * @param dataDocumento the data documento
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceCliente the codice cliente
	 * @return the matching testata fatture clienti, or <code>null</code> if a matching testata fatture clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestataFattureClienti fetchByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(
		int anno, String codiceAttivita, String codiceCentro,
		int numeroDocumento, Date dataDocumento, boolean tipoSoggetto,
		String codiceCliente) throws SystemException {
		return fetchByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(anno,
			codiceAttivita, codiceCentro, numeroDocumento, dataDocumento,
			tipoSoggetto, codiceCliente, true);
	}

	/**
	 * Returns the testata fatture clienti where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroDocumento = &#63; and dataDocumento = &#63; and tipoSoggetto = &#63; and codiceCliente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param numeroDocumento the numero documento
	 * @param dataDocumento the data documento
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceCliente the codice cliente
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching testata fatture clienti, or <code>null</code> if a matching testata fatture clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestataFattureClienti fetchByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(
		int anno, String codiceAttivita, String codiceCentro,
		int numeroDocumento, Date dataDocumento, boolean tipoSoggetto,
		String codiceCliente, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] {
				anno, codiceAttivita, codiceCentro, numeroDocumento,
				dataDocumento, tipoSoggetto, codiceCliente
			};

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO,
					finderArgs, this);
		}

		if (result instanceof TestataFattureClienti) {
			TestataFattureClienti testataFattureClienti = (TestataFattureClienti)result;

			if ((anno != testataFattureClienti.getAnno()) ||
					!Validator.equals(codiceAttivita,
						testataFattureClienti.getCodiceAttivita()) ||
					!Validator.equals(codiceCentro,
						testataFattureClienti.getCodiceCentro()) ||
					(numeroDocumento != testataFattureClienti.getNumeroDocumento()) ||
					!Validator.equals(dataDocumento,
						testataFattureClienti.getDataDocumento()) ||
					(tipoSoggetto != testataFattureClienti.getTipoSoggetto()) ||
					!Validator.equals(codiceCliente,
						testataFattureClienti.getCodiceCliente())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(9);

			query.append(_SQL_SELECT_TESTATAFATTURECLIENTI_WHERE);

			query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_ANNO_2);

			boolean bindCodiceAttivita = false;

			if (codiceAttivita == null) {
				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICEATTIVITA_1);
			}
			else if (codiceAttivita.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICEATTIVITA_3);
			}
			else {
				bindCodiceAttivita = true;

				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICEATTIVITA_2);
			}

			boolean bindCodiceCentro = false;

			if (codiceCentro == null) {
				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECENTRO_1);
			}
			else if (codiceCentro.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECENTRO_3);
			}
			else {
				bindCodiceCentro = true;

				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECENTRO_2);
			}

			query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_NUMERODOCUMENTO_2);

			boolean bindDataDocumento = false;

			if (dataDocumento == null) {
				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_DATADOCUMENTO_1);
			}
			else {
				bindDataDocumento = true;

				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_DATADOCUMENTO_2);
			}

			query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_TIPOSOGGETTO_2);

			boolean bindCodiceCliente = false;

			if (codiceCliente == null) {
				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECLIENTE_1);
			}
			else if (codiceCliente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECLIENTE_3);
			}
			else {
				bindCodiceCliente = true;

				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECLIENTE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(anno);

				if (bindCodiceAttivita) {
					qPos.add(codiceAttivita);
				}

				if (bindCodiceCentro) {
					qPos.add(codiceCentro);
				}

				qPos.add(numeroDocumento);

				if (bindDataDocumento) {
					qPos.add(CalendarUtil.getTimestamp(dataDocumento));
				}

				qPos.add(tipoSoggetto);

				if (bindCodiceCliente) {
					qPos.add(codiceCliente);
				}

				List<TestataFattureClienti> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"TestataFattureClientiPersistenceImpl.fetchByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(int, String, String, int, Date, boolean, String, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					TestataFattureClienti testataFattureClienti = list.get(0);

					result = testataFattureClienti;

					cacheResult(testataFattureClienti);

					if ((testataFattureClienti.getAnno() != anno) ||
							(testataFattureClienti.getCodiceAttivita() == null) ||
							!testataFattureClienti.getCodiceAttivita()
													  .equals(codiceAttivita) ||
							(testataFattureClienti.getCodiceCentro() == null) ||
							!testataFattureClienti.getCodiceCentro()
													  .equals(codiceCentro) ||
							(testataFattureClienti.getNumeroDocumento() != numeroDocumento) ||
							(testataFattureClienti.getDataDocumento() == null) ||
							!testataFattureClienti.getDataDocumento()
													  .equals(dataDocumento) ||
							(testataFattureClienti.getTipoSoggetto() != tipoSoggetto) ||
							(testataFattureClienti.getCodiceCliente() == null) ||
							!testataFattureClienti.getCodiceCliente()
													  .equals(codiceCliente)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO,
							finderArgs, testataFattureClienti);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (TestataFattureClienti)result;
		}
	}

	/**
	 * Removes the testata fatture clienti where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroDocumento = &#63; and dataDocumento = &#63; and tipoSoggetto = &#63; and codiceCliente = &#63; from the database.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param numeroDocumento the numero documento
	 * @param dataDocumento the data documento
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceCliente the codice cliente
	 * @return the testata fatture clienti that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestataFattureClienti removeByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(
		int anno, String codiceAttivita, String codiceCentro,
		int numeroDocumento, Date dataDocumento, boolean tipoSoggetto,
		String codiceCliente)
		throws NoSuchTestataFattureClientiException, SystemException {
		TestataFattureClienti testataFattureClienti = findByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(anno,
				codiceAttivita, codiceCentro, numeroDocumento, dataDocumento,
				tipoSoggetto, codiceCliente);

		return remove(testataFattureClienti);
	}

	/**
	 * Returns the number of testata fatture clientis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroDocumento = &#63; and dataDocumento = &#63; and tipoSoggetto = &#63; and codiceCliente = &#63;.
	 *
	 * @param anno the anno
	 * @param codiceAttivita the codice attivita
	 * @param codiceCentro the codice centro
	 * @param numeroDocumento the numero documento
	 * @param dataDocumento the data documento
	 * @param tipoSoggetto the tipo soggetto
	 * @param codiceCliente the codice cliente
	 * @return the number of matching testata fatture clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(int anno,
		String codiceAttivita, String codiceCentro, int numeroDocumento,
		Date dataDocumento, boolean tipoSoggetto, String codiceCliente)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO;

		Object[] finderArgs = new Object[] {
				anno, codiceAttivita, codiceCentro, numeroDocumento,
				dataDocumento, tipoSoggetto, codiceCliente
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(8);

			query.append(_SQL_COUNT_TESTATAFATTURECLIENTI_WHERE);

			query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_ANNO_2);

			boolean bindCodiceAttivita = false;

			if (codiceAttivita == null) {
				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICEATTIVITA_1);
			}
			else if (codiceAttivita.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICEATTIVITA_3);
			}
			else {
				bindCodiceAttivita = true;

				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICEATTIVITA_2);
			}

			boolean bindCodiceCentro = false;

			if (codiceCentro == null) {
				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECENTRO_1);
			}
			else if (codiceCentro.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECENTRO_3);
			}
			else {
				bindCodiceCentro = true;

				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECENTRO_2);
			}

			query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_NUMERODOCUMENTO_2);

			boolean bindDataDocumento = false;

			if (dataDocumento == null) {
				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_DATADOCUMENTO_1);
			}
			else {
				bindDataDocumento = true;

				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_DATADOCUMENTO_2);
			}

			query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_TIPOSOGGETTO_2);

			boolean bindCodiceCliente = false;

			if (codiceCliente == null) {
				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECLIENTE_1);
			}
			else if (codiceCliente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECLIENTE_3);
			}
			else {
				bindCodiceCliente = true;

				query.append(_FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECLIENTE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(anno);

				if (bindCodiceAttivita) {
					qPos.add(codiceAttivita);
				}

				if (bindCodiceCentro) {
					qPos.add(codiceCentro);
				}

				qPos.add(numeroDocumento);

				if (bindDataDocumento) {
					qPos.add(CalendarUtil.getTimestamp(dataDocumento));
				}

				qPos.add(tipoSoggetto);

				if (bindCodiceCliente) {
					qPos.add(codiceCliente);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_ANNO_2 =
		"testataFattureClienti.id.anno = ? AND ";
	private static final String _FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICEATTIVITA_1 =
		"testataFattureClienti.id.codiceAttivita IS NULL AND ";
	private static final String _FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICEATTIVITA_2 =
		"testataFattureClienti.id.codiceAttivita = ? AND ";
	private static final String _FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICEATTIVITA_3 =
		"(testataFattureClienti.id.codiceAttivita IS NULL OR testataFattureClienti.id.codiceAttivita = '') AND ";
	private static final String _FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECENTRO_1 =
		"testataFattureClienti.id.codiceCentro IS NULL AND ";
	private static final String _FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECENTRO_2 =
		"testataFattureClienti.id.codiceCentro = ? AND ";
	private static final String _FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECENTRO_3 =
		"(testataFattureClienti.id.codiceCentro IS NULL OR testataFattureClienti.id.codiceCentro = '') AND ";
	private static final String _FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_NUMERODOCUMENTO_2 =
		"testataFattureClienti.numeroDocumento = ? AND ";
	private static final String _FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_DATADOCUMENTO_1 =
		"testataFattureClienti.dataDocumento IS NULL AND ";
	private static final String _FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_DATADOCUMENTO_2 =
		"testataFattureClienti.dataDocumento = ? AND ";
	private static final String _FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_TIPOSOGGETTO_2 =
		"testataFattureClienti.tipoSoggetto = ? AND ";
	private static final String _FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECLIENTE_1 =
		"testataFattureClienti.codiceCliente IS NULL";
	private static final String _FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECLIENTE_2 =
		"testataFattureClienti.codiceCliente = ?";
	private static final String _FINDER_COLUMN_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO_CODICECLIENTE_3 =
		"(testataFattureClienti.codiceCliente IS NULL OR testataFattureClienti.codiceCliente = '')";

	public TestataFattureClientiPersistenceImpl() {
		setModelClass(TestataFattureClienti.class);
	}

	/**
	 * Caches the testata fatture clienti in the entity cache if it is enabled.
	 *
	 * @param testataFattureClienti the testata fatture clienti
	 */
	@Override
	public void cacheResult(TestataFattureClienti testataFattureClienti) {
		EntityCacheUtil.putResult(TestataFattureClientiModelImpl.ENTITY_CACHE_ENABLED,
			TestataFattureClientiImpl.class,
			testataFattureClienti.getPrimaryKey(), testataFattureClienti);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO,
			new Object[] {
				testataFattureClienti.getAnno(),
				testataFattureClienti.getCodiceAttivita(),
				testataFattureClienti.getCodiceCentro(),
				testataFattureClienti.getNumeroDocumento(),
				testataFattureClienti.getDataDocumento(),
				testataFattureClienti.getTipoSoggetto(),
				testataFattureClienti.getCodiceCliente()
			}, testataFattureClienti);

		testataFattureClienti.resetOriginalValues();
	}

	/**
	 * Caches the testata fatture clientis in the entity cache if it is enabled.
	 *
	 * @param testataFattureClientis the testata fatture clientis
	 */
	@Override
	public void cacheResult(List<TestataFattureClienti> testataFattureClientis) {
		for (TestataFattureClienti testataFattureClienti : testataFattureClientis) {
			if (EntityCacheUtil.getResult(
						TestataFattureClientiModelImpl.ENTITY_CACHE_ENABLED,
						TestataFattureClientiImpl.class,
						testataFattureClienti.getPrimaryKey()) == null) {
				cacheResult(testataFattureClienti);
			}
			else {
				testataFattureClienti.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all testata fatture clientis.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(TestataFattureClientiImpl.class.getName());
		}

		EntityCacheUtil.clearCache(TestataFattureClientiImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the testata fatture clienti.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(TestataFattureClienti testataFattureClienti) {
		EntityCacheUtil.removeResult(TestataFattureClientiModelImpl.ENTITY_CACHE_ENABLED,
			TestataFattureClientiImpl.class,
			testataFattureClienti.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(testataFattureClienti);
	}

	@Override
	public void clearCache(List<TestataFattureClienti> testataFattureClientis) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (TestataFattureClienti testataFattureClienti : testataFattureClientis) {
			EntityCacheUtil.removeResult(TestataFattureClientiModelImpl.ENTITY_CACHE_ENABLED,
				TestataFattureClientiImpl.class,
				testataFattureClienti.getPrimaryKey());

			clearUniqueFindersCache(testataFattureClienti);
		}
	}

	protected void cacheUniqueFindersCache(
		TestataFattureClienti testataFattureClienti) {
		if (testataFattureClienti.isNew()) {
			Object[] args = new Object[] {
					testataFattureClienti.getAnno(),
					testataFattureClienti.getCodiceAttivita(),
					testataFattureClienti.getCodiceCentro(),
					testataFattureClienti.getNumeroDocumento(),
					testataFattureClienti.getDataDocumento(),
					testataFattureClienti.getTipoSoggetto(),
					testataFattureClienti.getCodiceCliente()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO,
				args, Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO,
				args, testataFattureClienti);
		}
		else {
			TestataFattureClientiModelImpl testataFattureClientiModelImpl = (TestataFattureClientiModelImpl)testataFattureClienti;

			if ((testataFattureClientiModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						testataFattureClienti.getAnno(),
						testataFattureClienti.getCodiceAttivita(),
						testataFattureClienti.getCodiceCentro(),
						testataFattureClienti.getNumeroDocumento(),
						testataFattureClienti.getDataDocumento(),
						testataFattureClienti.getTipoSoggetto(),
						testataFattureClienti.getCodiceCliente()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO,
					args, testataFattureClienti);
			}
		}
	}

	protected void clearUniqueFindersCache(
		TestataFattureClienti testataFattureClienti) {
		TestataFattureClientiModelImpl testataFattureClientiModelImpl = (TestataFattureClientiModelImpl)testataFattureClienti;

		Object[] args = new Object[] {
				testataFattureClienti.getAnno(),
				testataFattureClienti.getCodiceAttivita(),
				testataFattureClienti.getCodiceCentro(),
				testataFattureClienti.getNumeroDocumento(),
				testataFattureClienti.getDataDocumento(),
				testataFattureClienti.getTipoSoggetto(),
				testataFattureClienti.getCodiceCliente()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO,
			args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO,
			args);

		if ((testataFattureClientiModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO.getColumnBitmask()) != 0) {
			args = new Object[] {
					testataFattureClientiModelImpl.getOriginalAnno(),
					testataFattureClientiModelImpl.getOriginalCodiceAttivita(),
					testataFattureClientiModelImpl.getOriginalCodiceCentro(),
					testataFattureClientiModelImpl.getOriginalNumeroDocumento(),
					testataFattureClientiModelImpl.getOriginalDataDocumento(),
					testataFattureClientiModelImpl.getOriginalTipoSoggetto(),
					testataFattureClientiModelImpl.getOriginalCodiceCliente()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO,
				args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ANNOCODICEATTIVITACODICECENTRONUMERODOCUMENTO,
				args);
		}
	}

	/**
	 * Creates a new testata fatture clienti with the primary key. Does not add the testata fatture clienti to the database.
	 *
	 * @param testataFattureClientiPK the primary key for the new testata fatture clienti
	 * @return the new testata fatture clienti
	 */
	@Override
	public TestataFattureClienti create(
		TestataFattureClientiPK testataFattureClientiPK) {
		TestataFattureClienti testataFattureClienti = new TestataFattureClientiImpl();

		testataFattureClienti.setNew(true);
		testataFattureClienti.setPrimaryKey(testataFattureClientiPK);

		return testataFattureClienti;
	}

	/**
	 * Removes the testata fatture clienti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param testataFattureClientiPK the primary key of the testata fatture clienti
	 * @return the testata fatture clienti that was removed
	 * @throws it.bysoftware.ct.NoSuchTestataFattureClientiException if a testata fatture clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestataFattureClienti remove(
		TestataFattureClientiPK testataFattureClientiPK)
		throws NoSuchTestataFattureClientiException, SystemException {
		return remove((Serializable)testataFattureClientiPK);
	}

	/**
	 * Removes the testata fatture clienti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the testata fatture clienti
	 * @return the testata fatture clienti that was removed
	 * @throws it.bysoftware.ct.NoSuchTestataFattureClientiException if a testata fatture clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestataFattureClienti remove(Serializable primaryKey)
		throws NoSuchTestataFattureClientiException, SystemException {
		Session session = null;

		try {
			session = openSession();

			TestataFattureClienti testataFattureClienti = (TestataFattureClienti)session.get(TestataFattureClientiImpl.class,
					primaryKey);

			if (testataFattureClienti == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTestataFattureClientiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(testataFattureClienti);
		}
		catch (NoSuchTestataFattureClientiException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected TestataFattureClienti removeImpl(
		TestataFattureClienti testataFattureClienti) throws SystemException {
		testataFattureClienti = toUnwrappedModel(testataFattureClienti);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(testataFattureClienti)) {
				testataFattureClienti = (TestataFattureClienti)session.get(TestataFattureClientiImpl.class,
						testataFattureClienti.getPrimaryKeyObj());
			}

			if (testataFattureClienti != null) {
				session.delete(testataFattureClienti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (testataFattureClienti != null) {
			clearCache(testataFattureClienti);
		}

		return testataFattureClienti;
	}

	@Override
	public TestataFattureClienti updateImpl(
		it.bysoftware.ct.model.TestataFattureClienti testataFattureClienti)
		throws SystemException {
		testataFattureClienti = toUnwrappedModel(testataFattureClienti);

		boolean isNew = testataFattureClienti.isNew();

		Session session = null;

		try {
			session = openSession();

			if (testataFattureClienti.isNew()) {
				session.save(testataFattureClienti);

				testataFattureClienti.setNew(false);
			}
			else {
				session.merge(testataFattureClienti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !TestataFattureClientiModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(TestataFattureClientiModelImpl.ENTITY_CACHE_ENABLED,
			TestataFattureClientiImpl.class,
			testataFattureClienti.getPrimaryKey(), testataFattureClienti);

		clearUniqueFindersCache(testataFattureClienti);
		cacheUniqueFindersCache(testataFattureClienti);

		return testataFattureClienti;
	}

	protected TestataFattureClienti toUnwrappedModel(
		TestataFattureClienti testataFattureClienti) {
		if (testataFattureClienti instanceof TestataFattureClientiImpl) {
			return testataFattureClienti;
		}

		TestataFattureClientiImpl testataFattureClientiImpl = new TestataFattureClientiImpl();

		testataFattureClientiImpl.setNew(testataFattureClienti.isNew());
		testataFattureClientiImpl.setPrimaryKey(testataFattureClienti.getPrimaryKey());

		testataFattureClientiImpl.setAnno(testataFattureClienti.getAnno());
		testataFattureClientiImpl.setCodiceAttivita(testataFattureClienti.getCodiceAttivita());
		testataFattureClientiImpl.setCodiceCentro(testataFattureClienti.getCodiceCentro());
		testataFattureClientiImpl.setNumeroProtocollo(testataFattureClienti.getNumeroProtocollo());
		testataFattureClientiImpl.setTipoDocumento(testataFattureClienti.getTipoDocumento());
		testataFattureClientiImpl.setCodiceCentroContAnalitica(testataFattureClienti.getCodiceCentroContAnalitica());
		testataFattureClientiImpl.setIdTipoDocumentoOrigine(testataFattureClienti.getIdTipoDocumentoOrigine());
		testataFattureClientiImpl.setStatoFattura(testataFattureClienti.isStatoFattura());
		testataFattureClientiImpl.setDataRegistrazione(testataFattureClienti.getDataRegistrazione());
		testataFattureClientiImpl.setDataOperazione(testataFattureClienti.getDataOperazione());
		testataFattureClientiImpl.setDataAnnotazione(testataFattureClienti.getDataAnnotazione());
		testataFattureClientiImpl.setDataDocumento(testataFattureClienti.getDataDocumento());
		testataFattureClientiImpl.setNumeroDocumento(testataFattureClienti.getNumeroDocumento());
		testataFattureClientiImpl.setRiferimentoAScontrino(testataFattureClienti.getRiferimentoAScontrino());
		testataFattureClientiImpl.setDescrizioneEstremiDocumento(testataFattureClienti.getDescrizioneEstremiDocumento());
		testataFattureClientiImpl.setTipoSoggetto(testataFattureClienti.isTipoSoggetto());
		testataFattureClientiImpl.setCodiceCliente(testataFattureClienti.getCodiceCliente());
		testataFattureClientiImpl.setDescrizioneAggiuntivaFattura(testataFattureClienti.getDescrizioneAggiuntivaFattura());
		testataFattureClientiImpl.setEstremiOrdine(testataFattureClienti.getEstremiOrdine());
		testataFattureClientiImpl.setEstremiBolla(testataFattureClienti.getEstremiBolla());
		testataFattureClientiImpl.setCodicePagamento(testataFattureClienti.getCodicePagamento());
		testataFattureClientiImpl.setCodiceAgente(testataFattureClienti.getCodiceAgente());
		testataFattureClientiImpl.setCodiceGruppoAgenti(testataFattureClienti.getCodiceGruppoAgenti());
		testataFattureClientiImpl.setAnnotazioni(testataFattureClienti.getAnnotazioni());
		testataFattureClientiImpl.setScontoChiusura(testataFattureClienti.getScontoChiusura());
		testataFattureClientiImpl.setScontoProntaCassa(testataFattureClienti.getScontoProntaCassa());
		testataFattureClientiImpl.setPertualeSpeseTrasp(testataFattureClienti.getPertualeSpeseTrasp());
		testataFattureClientiImpl.setImportoSpeseTrasp(testataFattureClienti.getImportoSpeseTrasp());
		testataFattureClientiImpl.setImportoSpeseImb(testataFattureClienti.getImportoSpeseImb());
		testataFattureClientiImpl.setImportoSpeseVarie(testataFattureClienti.getImportoSpeseVarie());
		testataFattureClientiImpl.setImportoSpeseBancarie(testataFattureClienti.getImportoSpeseBancarie());
		testataFattureClientiImpl.setCodiceIVATrasp(testataFattureClienti.getCodiceIVATrasp());
		testataFattureClientiImpl.setCodiceIVAImb(testataFattureClienti.getCodiceIVAImb());
		testataFattureClientiImpl.setCodiceIVAVarie(testataFattureClienti.getCodiceIVAVarie());
		testataFattureClientiImpl.setCodiceIVABancarie(testataFattureClienti.getCodiceIVABancarie());
		testataFattureClientiImpl.setTotaleScontiCorpo(testataFattureClienti.getTotaleScontiCorpo());
		testataFattureClientiImpl.setImponibile(testataFattureClienti.getImponibile());
		testataFattureClientiImpl.setIVA(testataFattureClienti.getIVA());
		testataFattureClientiImpl.setImportoFattura(testataFattureClienti.getImportoFattura());
		testataFattureClientiImpl.setLibStr1(testataFattureClienti.getLibStr1());
		testataFattureClientiImpl.setLibStr2(testataFattureClienti.getLibStr2());
		testataFattureClientiImpl.setLibStr3(testataFattureClienti.getLibStr3());
		testataFattureClientiImpl.setLibDbl1(testataFattureClienti.getLibDbl1());
		testataFattureClientiImpl.setLibDbl2(testataFattureClienti.getLibDbl2());
		testataFattureClientiImpl.setLibDbl3(testataFattureClienti.getLibDbl3());
		testataFattureClientiImpl.setLibLng1(testataFattureClienti.getLibLng1());
		testataFattureClientiImpl.setLibLng2(testataFattureClienti.getLibLng2());
		testataFattureClientiImpl.setLibLng3(testataFattureClienti.getLibLng3());
		testataFattureClientiImpl.setLibDat1(testataFattureClienti.getLibDat1());
		testataFattureClientiImpl.setLibDat2(testataFattureClienti.getLibDat2());
		testataFattureClientiImpl.setLibDat3(testataFattureClienti.getLibDat3());

		return testataFattureClientiImpl;
	}

	/**
	 * Returns the testata fatture clienti with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the testata fatture clienti
	 * @return the testata fatture clienti
	 * @throws it.bysoftware.ct.NoSuchTestataFattureClientiException if a testata fatture clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestataFattureClienti findByPrimaryKey(Serializable primaryKey)
		throws NoSuchTestataFattureClientiException, SystemException {
		TestataFattureClienti testataFattureClienti = fetchByPrimaryKey(primaryKey);

		if (testataFattureClienti == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTestataFattureClientiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return testataFattureClienti;
	}

	/**
	 * Returns the testata fatture clienti with the primary key or throws a {@link it.bysoftware.ct.NoSuchTestataFattureClientiException} if it could not be found.
	 *
	 * @param testataFattureClientiPK the primary key of the testata fatture clienti
	 * @return the testata fatture clienti
	 * @throws it.bysoftware.ct.NoSuchTestataFattureClientiException if a testata fatture clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestataFattureClienti findByPrimaryKey(
		TestataFattureClientiPK testataFattureClientiPK)
		throws NoSuchTestataFattureClientiException, SystemException {
		return findByPrimaryKey((Serializable)testataFattureClientiPK);
	}

	/**
	 * Returns the testata fatture clienti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the testata fatture clienti
	 * @return the testata fatture clienti, or <code>null</code> if a testata fatture clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestataFattureClienti fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		TestataFattureClienti testataFattureClienti = (TestataFattureClienti)EntityCacheUtil.getResult(TestataFattureClientiModelImpl.ENTITY_CACHE_ENABLED,
				TestataFattureClientiImpl.class, primaryKey);

		if (testataFattureClienti == _nullTestataFattureClienti) {
			return null;
		}

		if (testataFattureClienti == null) {
			Session session = null;

			try {
				session = openSession();

				testataFattureClienti = (TestataFattureClienti)session.get(TestataFattureClientiImpl.class,
						primaryKey);

				if (testataFattureClienti != null) {
					cacheResult(testataFattureClienti);
				}
				else {
					EntityCacheUtil.putResult(TestataFattureClientiModelImpl.ENTITY_CACHE_ENABLED,
						TestataFattureClientiImpl.class, primaryKey,
						_nullTestataFattureClienti);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(TestataFattureClientiModelImpl.ENTITY_CACHE_ENABLED,
					TestataFattureClientiImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return testataFattureClienti;
	}

	/**
	 * Returns the testata fatture clienti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param testataFattureClientiPK the primary key of the testata fatture clienti
	 * @return the testata fatture clienti, or <code>null</code> if a testata fatture clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TestataFattureClienti fetchByPrimaryKey(
		TestataFattureClientiPK testataFattureClientiPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)testataFattureClientiPK);
	}

	/**
	 * Returns all the testata fatture clientis.
	 *
	 * @return the testata fatture clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TestataFattureClienti> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the testata fatture clientis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataFattureClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of testata fatture clientis
	 * @param end the upper bound of the range of testata fatture clientis (not inclusive)
	 * @return the range of testata fatture clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TestataFattureClienti> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the testata fatture clientis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataFattureClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of testata fatture clientis
	 * @param end the upper bound of the range of testata fatture clientis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of testata fatture clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TestataFattureClienti> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<TestataFattureClienti> list = (List<TestataFattureClienti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_TESTATAFATTURECLIENTI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TESTATAFATTURECLIENTI;

				if (pagination) {
					sql = sql.concat(TestataFattureClientiModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<TestataFattureClienti>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<TestataFattureClienti>(list);
				}
				else {
					list = (List<TestataFattureClienti>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the testata fatture clientis from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (TestataFattureClienti testataFattureClienti : findAll()) {
			remove(testataFattureClienti);
		}
	}

	/**
	 * Returns the number of testata fatture clientis.
	 *
	 * @return the number of testata fatture clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TESTATAFATTURECLIENTI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the testata fatture clienti persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.TestataFattureClienti")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<TestataFattureClienti>> listenersList = new ArrayList<ModelListener<TestataFattureClienti>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<TestataFattureClienti>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(TestataFattureClientiImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_TESTATAFATTURECLIENTI = "SELECT testataFattureClienti FROM TestataFattureClienti testataFattureClienti";
	private static final String _SQL_SELECT_TESTATAFATTURECLIENTI_WHERE = "SELECT testataFattureClienti FROM TestataFattureClienti testataFattureClienti WHERE ";
	private static final String _SQL_COUNT_TESTATAFATTURECLIENTI = "SELECT COUNT(testataFattureClienti) FROM TestataFattureClienti testataFattureClienti";
	private static final String _SQL_COUNT_TESTATAFATTURECLIENTI_WHERE = "SELECT COUNT(testataFattureClienti) FROM TestataFattureClienti testataFattureClienti WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "testataFattureClienti.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TestataFattureClienti exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No TestataFattureClienti exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(TestataFattureClientiPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"anno", "codiceAttivita", "codiceCentro", "numeroProtocollo",
				"tipoDocumento", "codiceCentroContAnalitica",
				"idTipoDocumentoOrigine", "statoFattura", "dataRegistrazione",
				"dataOperazione", "dataAnnotazione", "dataDocumento",
				"numeroDocumento", "riferimentoAScontrino",
				"descrizioneEstremiDocumento", "tipoSoggetto", "codiceCliente",
				"descrizioneAggiuntivaFattura", "estremiOrdine", "estremiBolla",
				"codicePagamento", "codiceAgente", "codiceGruppoAgenti",
				"annotazioni", "scontoChiusura", "scontoProntaCassa",
				"pertualeSpeseTrasp", "importoSpeseTrasp", "importoSpeseImb",
				"importoSpeseVarie", "importoSpeseBancarie", "codiceIVATrasp",
				"codiceIVAImb", "codiceIVAVarie", "codiceIVABancarie",
				"totaleScontiCorpo", "imponibile", "IVA", "importoFattura",
				"libStr1", "libStr2", "libStr3", "libDbl1", "libDbl2", "libDbl3",
				"libLng1", "libLng2", "libLng3", "libDat1", "libDat2", "libDat3"
			});
	private static TestataFattureClienti _nullTestataFattureClienti = new TestataFattureClientiImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<TestataFattureClienti> toCacheModel() {
				return _nullTestataFattureClientiCacheModel;
			}
		};

	private static CacheModel<TestataFattureClienti> _nullTestataFattureClientiCacheModel =
		new CacheModel<TestataFattureClienti>() {
			@Override
			public TestataFattureClienti toEntityModel() {
				return _nullTestataFattureClienti;
			}
		};
}