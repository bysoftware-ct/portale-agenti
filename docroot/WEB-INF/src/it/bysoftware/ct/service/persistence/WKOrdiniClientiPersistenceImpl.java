/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchWKOrdiniClientiException;
import it.bysoftware.ct.model.WKOrdiniClienti;
import it.bysoftware.ct.model.impl.WKOrdiniClientiImpl;
import it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the w k ordini clienti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see WKOrdiniClientiPersistence
 * @see WKOrdiniClientiUtil
 * @generated
 */
public class WKOrdiniClientiPersistenceImpl extends BasePersistenceImpl<WKOrdiniClienti>
	implements WKOrdiniClientiPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link WKOrdiniClientiUtil} to access the w k ordini clienti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = WKOrdiniClientiImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			WKOrdiniClientiImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			WKOrdiniClientiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKOrdiniClientiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ANNOTIPOORDINE =
		new FinderPath(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			WKOrdiniClientiImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByAnnoTipoOrdine",
			new String[] {
				Integer.class.getName(), Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOTIPOORDINE =
		new FinderPath(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			WKOrdiniClientiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByAnnoTipoOrdine",
			new String[] { Integer.class.getName(), Integer.class.getName() },
			WKOrdiniClientiModelImpl.ANNO_COLUMN_BITMASK |
			WKOrdiniClientiModelImpl.TIPOORDINE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ANNOTIPOORDINE = new FinderPath(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKOrdiniClientiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByAnnoTipoOrdine",
			new String[] { Integer.class.getName(), Integer.class.getName() });

	/**
	 * Returns all the w k ordini clientis where anno = &#63; and tipoOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @return the matching w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKOrdiniClienti> findByAnnoTipoOrdine(int anno, int tipoOrdine)
		throws SystemException {
		return findByAnnoTipoOrdine(anno, tipoOrdine, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the w k ordini clientis where anno = &#63; and tipoOrdine = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param start the lower bound of the range of w k ordini clientis
	 * @param end the upper bound of the range of w k ordini clientis (not inclusive)
	 * @return the range of matching w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKOrdiniClienti> findByAnnoTipoOrdine(int anno, int tipoOrdine,
		int start, int end) throws SystemException {
		return findByAnnoTipoOrdine(anno, tipoOrdine, start, end, null);
	}

	/**
	 * Returns an ordered range of all the w k ordini clientis where anno = &#63; and tipoOrdine = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param start the lower bound of the range of w k ordini clientis
	 * @param end the upper bound of the range of w k ordini clientis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKOrdiniClienti> findByAnnoTipoOrdine(int anno, int tipoOrdine,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOTIPOORDINE;
			finderArgs = new Object[] { anno, tipoOrdine };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ANNOTIPOORDINE;
			finderArgs = new Object[] {
					anno, tipoOrdine,
					
					start, end, orderByComparator
				};
		}

		List<WKOrdiniClienti> list = (List<WKOrdiniClienti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (WKOrdiniClienti wkOrdiniClienti : list) {
				if ((anno != wkOrdiniClienti.getAnno()) ||
						(tipoOrdine != wkOrdiniClienti.getTipoOrdine())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_WKORDINICLIENTI_WHERE);

			query.append(_FINDER_COLUMN_ANNOTIPOORDINE_ANNO_2);

			query.append(_FINDER_COLUMN_ANNOTIPOORDINE_TIPOORDINE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(WKOrdiniClientiModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(anno);

				qPos.add(tipoOrdine);

				if (!pagination) {
					list = (List<WKOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<WKOrdiniClienti>(list);
				}
				else {
					list = (List<WKOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first w k ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching w k ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a matching w k ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti findByAnnoTipoOrdine_First(int anno, int tipoOrdine,
		OrderByComparator orderByComparator)
		throws NoSuchWKOrdiniClientiException, SystemException {
		WKOrdiniClienti wkOrdiniClienti = fetchByAnnoTipoOrdine_First(anno,
				tipoOrdine, orderByComparator);

		if (wkOrdiniClienti != null) {
			return wkOrdiniClienti;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("anno=");
		msg.append(anno);

		msg.append(", tipoOrdine=");
		msg.append(tipoOrdine);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWKOrdiniClientiException(msg.toString());
	}

	/**
	 * Returns the first w k ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching w k ordini clienti, or <code>null</code> if a matching w k ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti fetchByAnnoTipoOrdine_First(int anno,
		int tipoOrdine, OrderByComparator orderByComparator)
		throws SystemException {
		List<WKOrdiniClienti> list = findByAnnoTipoOrdine(anno, tipoOrdine, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last w k ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching w k ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a matching w k ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti findByAnnoTipoOrdine_Last(int anno, int tipoOrdine,
		OrderByComparator orderByComparator)
		throws NoSuchWKOrdiniClientiException, SystemException {
		WKOrdiniClienti wkOrdiniClienti = fetchByAnnoTipoOrdine_Last(anno,
				tipoOrdine, orderByComparator);

		if (wkOrdiniClienti != null) {
			return wkOrdiniClienti;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("anno=");
		msg.append(anno);

		msg.append(", tipoOrdine=");
		msg.append(tipoOrdine);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWKOrdiniClientiException(msg.toString());
	}

	/**
	 * Returns the last w k ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching w k ordini clienti, or <code>null</code> if a matching w k ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti fetchByAnnoTipoOrdine_Last(int anno, int tipoOrdine,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByAnnoTipoOrdine(anno, tipoOrdine);

		if (count == 0) {
			return null;
		}

		List<WKOrdiniClienti> list = findByAnnoTipoOrdine(anno, tipoOrdine,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the w k ordini clientis before and after the current w k ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63;.
	 *
	 * @param wkOrdiniClientiPK the primary key of the current w k ordini clienti
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next w k ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a w k ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti[] findByAnnoTipoOrdine_PrevAndNext(
		WKOrdiniClientiPK wkOrdiniClientiPK, int anno, int tipoOrdine,
		OrderByComparator orderByComparator)
		throws NoSuchWKOrdiniClientiException, SystemException {
		WKOrdiniClienti wkOrdiniClienti = findByPrimaryKey(wkOrdiniClientiPK);

		Session session = null;

		try {
			session = openSession();

			WKOrdiniClienti[] array = new WKOrdiniClientiImpl[3];

			array[0] = getByAnnoTipoOrdine_PrevAndNext(session,
					wkOrdiniClienti, anno, tipoOrdine, orderByComparator, true);

			array[1] = wkOrdiniClienti;

			array[2] = getByAnnoTipoOrdine_PrevAndNext(session,
					wkOrdiniClienti, anno, tipoOrdine, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected WKOrdiniClienti getByAnnoTipoOrdine_PrevAndNext(Session session,
		WKOrdiniClienti wkOrdiniClienti, int anno, int tipoOrdine,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_WKORDINICLIENTI_WHERE);

		query.append(_FINDER_COLUMN_ANNOTIPOORDINE_ANNO_2);

		query.append(_FINDER_COLUMN_ANNOTIPOORDINE_TIPOORDINE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(WKOrdiniClientiModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(anno);

		qPos.add(tipoOrdine);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(wkOrdiniClienti);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<WKOrdiniClienti> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the w k ordini clientis where anno = &#63; and tipoOrdine = &#63; from the database.
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByAnnoTipoOrdine(int anno, int tipoOrdine)
		throws SystemException {
		for (WKOrdiniClienti wkOrdiniClienti : findByAnnoTipoOrdine(anno,
				tipoOrdine, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(wkOrdiniClienti);
		}
	}

	/**
	 * Returns the number of w k ordini clientis where anno = &#63; and tipoOrdine = &#63;.
	 *
	 * @param anno the anno
	 * @param tipoOrdine the tipo ordine
	 * @return the number of matching w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByAnnoTipoOrdine(int anno, int tipoOrdine)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ANNOTIPOORDINE;

		Object[] finderArgs = new Object[] { anno, tipoOrdine };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_WKORDINICLIENTI_WHERE);

			query.append(_FINDER_COLUMN_ANNOTIPOORDINE_ANNO_2);

			query.append(_FINDER_COLUMN_ANNOTIPOORDINE_TIPOORDINE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(anno);

				qPos.add(tipoOrdine);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ANNOTIPOORDINE_ANNO_2 = "wkOrdiniClienti.id.anno = ? AND ";
	private static final String _FINDER_COLUMN_ANNOTIPOORDINE_TIPOORDINE_2 = "wkOrdiniClienti.id.tipoOrdine = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICECLIENTE =
		new FinderPath(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			WKOrdiniClientiImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCodiceCliente",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTE =
		new FinderPath(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			WKOrdiniClientiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCodiceCliente",
			new String[] { String.class.getName() },
			WKOrdiniClientiModelImpl.CODICECLIENTE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CODICECLIENTE = new FinderPath(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKOrdiniClientiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCodiceCliente",
			new String[] { String.class.getName() });

	/**
	 * Returns all the w k ordini clientis where codiceCliente = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @return the matching w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKOrdiniClienti> findByCodiceCliente(String codiceCliente)
		throws SystemException {
		return findByCodiceCliente(codiceCliente, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the w k ordini clientis where codiceCliente = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceCliente the codice cliente
	 * @param start the lower bound of the range of w k ordini clientis
	 * @param end the upper bound of the range of w k ordini clientis (not inclusive)
	 * @return the range of matching w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKOrdiniClienti> findByCodiceCliente(String codiceCliente,
		int start, int end) throws SystemException {
		return findByCodiceCliente(codiceCliente, start, end, null);
	}

	/**
	 * Returns an ordered range of all the w k ordini clientis where codiceCliente = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceCliente the codice cliente
	 * @param start the lower bound of the range of w k ordini clientis
	 * @param end the upper bound of the range of w k ordini clientis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKOrdiniClienti> findByCodiceCliente(String codiceCliente,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTE;
			finderArgs = new Object[] { codiceCliente };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CODICECLIENTE;
			finderArgs = new Object[] {
					codiceCliente,
					
					start, end, orderByComparator
				};
		}

		List<WKOrdiniClienti> list = (List<WKOrdiniClienti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (WKOrdiniClienti wkOrdiniClienti : list) {
				if (!Validator.equals(codiceCliente,
							wkOrdiniClienti.getCodiceCliente())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_WKORDINICLIENTI_WHERE);

			boolean bindCodiceCliente = false;

			if (codiceCliente == null) {
				query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_1);
			}
			else if (codiceCliente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_3);
			}
			else {
				bindCodiceCliente = true;

				query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(WKOrdiniClientiModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceCliente) {
					qPos.add(codiceCliente);
				}

				if (!pagination) {
					list = (List<WKOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<WKOrdiniClienti>(list);
				}
				else {
					list = (List<WKOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first w k ordini clienti in the ordered set where codiceCliente = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching w k ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a matching w k ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti findByCodiceCliente_First(String codiceCliente,
		OrderByComparator orderByComparator)
		throws NoSuchWKOrdiniClientiException, SystemException {
		WKOrdiniClienti wkOrdiniClienti = fetchByCodiceCliente_First(codiceCliente,
				orderByComparator);

		if (wkOrdiniClienti != null) {
			return wkOrdiniClienti;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceCliente=");
		msg.append(codiceCliente);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWKOrdiniClientiException(msg.toString());
	}

	/**
	 * Returns the first w k ordini clienti in the ordered set where codiceCliente = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching w k ordini clienti, or <code>null</code> if a matching w k ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti fetchByCodiceCliente_First(String codiceCliente,
		OrderByComparator orderByComparator) throws SystemException {
		List<WKOrdiniClienti> list = findByCodiceCliente(codiceCliente, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last w k ordini clienti in the ordered set where codiceCliente = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching w k ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a matching w k ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti findByCodiceCliente_Last(String codiceCliente,
		OrderByComparator orderByComparator)
		throws NoSuchWKOrdiniClientiException, SystemException {
		WKOrdiniClienti wkOrdiniClienti = fetchByCodiceCliente_Last(codiceCliente,
				orderByComparator);

		if (wkOrdiniClienti != null) {
			return wkOrdiniClienti;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceCliente=");
		msg.append(codiceCliente);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWKOrdiniClientiException(msg.toString());
	}

	/**
	 * Returns the last w k ordini clienti in the ordered set where codiceCliente = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching w k ordini clienti, or <code>null</code> if a matching w k ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti fetchByCodiceCliente_Last(String codiceCliente,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCodiceCliente(codiceCliente);

		if (count == 0) {
			return null;
		}

		List<WKOrdiniClienti> list = findByCodiceCliente(codiceCliente,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the w k ordini clientis before and after the current w k ordini clienti in the ordered set where codiceCliente = &#63;.
	 *
	 * @param wkOrdiniClientiPK the primary key of the current w k ordini clienti
	 * @param codiceCliente the codice cliente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next w k ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a w k ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti[] findByCodiceCliente_PrevAndNext(
		WKOrdiniClientiPK wkOrdiniClientiPK, String codiceCliente,
		OrderByComparator orderByComparator)
		throws NoSuchWKOrdiniClientiException, SystemException {
		WKOrdiniClienti wkOrdiniClienti = findByPrimaryKey(wkOrdiniClientiPK);

		Session session = null;

		try {
			session = openSession();

			WKOrdiniClienti[] array = new WKOrdiniClientiImpl[3];

			array[0] = getByCodiceCliente_PrevAndNext(session, wkOrdiniClienti,
					codiceCliente, orderByComparator, true);

			array[1] = wkOrdiniClienti;

			array[2] = getByCodiceCliente_PrevAndNext(session, wkOrdiniClienti,
					codiceCliente, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected WKOrdiniClienti getByCodiceCliente_PrevAndNext(Session session,
		WKOrdiniClienti wkOrdiniClienti, String codiceCliente,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_WKORDINICLIENTI_WHERE);

		boolean bindCodiceCliente = false;

		if (codiceCliente == null) {
			query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_1);
		}
		else if (codiceCliente.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_3);
		}
		else {
			bindCodiceCliente = true;

			query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(WKOrdiniClientiModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodiceCliente) {
			qPos.add(codiceCliente);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(wkOrdiniClienti);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<WKOrdiniClienti> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the w k ordini clientis where codiceCliente = &#63; from the database.
	 *
	 * @param codiceCliente the codice cliente
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCodiceCliente(String codiceCliente)
		throws SystemException {
		for (WKOrdiniClienti wkOrdiniClienti : findByCodiceCliente(
				codiceCliente, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(wkOrdiniClienti);
		}
	}

	/**
	 * Returns the number of w k ordini clientis where codiceCliente = &#63;.
	 *
	 * @param codiceCliente the codice cliente
	 * @return the number of matching w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCodiceCliente(String codiceCliente)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CODICECLIENTE;

		Object[] finderArgs = new Object[] { codiceCliente };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_WKORDINICLIENTI_WHERE);

			boolean bindCodiceCliente = false;

			if (codiceCliente == null) {
				query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_1);
			}
			else if (codiceCliente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_3);
			}
			else {
				bindCodiceCliente = true;

				query.append(_FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceCliente) {
					qPos.add(codiceCliente);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_1 = "wkOrdiniClienti.codiceCliente IS NULL";
	private static final String _FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_2 = "wkOrdiniClienti.codiceCliente = ?";
	private static final String _FINDER_COLUMN_CODICECLIENTE_CODICECLIENTE_3 = "(wkOrdiniClienti.codiceCliente IS NULL OR wkOrdiniClienti.codiceCliente = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STATOORDINE =
		new FinderPath(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			WKOrdiniClientiImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByStatoOrdine",
			new String[] {
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATOORDINE =
		new FinderPath(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKOrdiniClientiModelImpl.FINDER_CACHE_ENABLED,
			WKOrdiniClientiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByStatoOrdine",
			new String[] { Boolean.class.getName() },
			WKOrdiniClientiModelImpl.STATOORDINE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STATOORDINE = new FinderPath(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKOrdiniClientiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByStatoOrdine",
			new String[] { Boolean.class.getName() });

	/**
	 * Returns all the w k ordini clientis where statoOrdine = &#63;.
	 *
	 * @param statoOrdine the stato ordine
	 * @return the matching w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKOrdiniClienti> findByStatoOrdine(boolean statoOrdine)
		throws SystemException {
		return findByStatoOrdine(statoOrdine, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the w k ordini clientis where statoOrdine = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param statoOrdine the stato ordine
	 * @param start the lower bound of the range of w k ordini clientis
	 * @param end the upper bound of the range of w k ordini clientis (not inclusive)
	 * @return the range of matching w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKOrdiniClienti> findByStatoOrdine(boolean statoOrdine,
		int start, int end) throws SystemException {
		return findByStatoOrdine(statoOrdine, start, end, null);
	}

	/**
	 * Returns an ordered range of all the w k ordini clientis where statoOrdine = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param statoOrdine the stato ordine
	 * @param start the lower bound of the range of w k ordini clientis
	 * @param end the upper bound of the range of w k ordini clientis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKOrdiniClienti> findByStatoOrdine(boolean statoOrdine,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATOORDINE;
			finderArgs = new Object[] { statoOrdine };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STATOORDINE;
			finderArgs = new Object[] { statoOrdine, start, end, orderByComparator };
		}

		List<WKOrdiniClienti> list = (List<WKOrdiniClienti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (WKOrdiniClienti wkOrdiniClienti : list) {
				if ((statoOrdine != wkOrdiniClienti.getStatoOrdine())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_WKORDINICLIENTI_WHERE);

			query.append(_FINDER_COLUMN_STATOORDINE_STATOORDINE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(WKOrdiniClientiModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(statoOrdine);

				if (!pagination) {
					list = (List<WKOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<WKOrdiniClienti>(list);
				}
				else {
					list = (List<WKOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first w k ordini clienti in the ordered set where statoOrdine = &#63;.
	 *
	 * @param statoOrdine the stato ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching w k ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a matching w k ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti findByStatoOrdine_First(boolean statoOrdine,
		OrderByComparator orderByComparator)
		throws NoSuchWKOrdiniClientiException, SystemException {
		WKOrdiniClienti wkOrdiniClienti = fetchByStatoOrdine_First(statoOrdine,
				orderByComparator);

		if (wkOrdiniClienti != null) {
			return wkOrdiniClienti;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("statoOrdine=");
		msg.append(statoOrdine);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWKOrdiniClientiException(msg.toString());
	}

	/**
	 * Returns the first w k ordini clienti in the ordered set where statoOrdine = &#63;.
	 *
	 * @param statoOrdine the stato ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching w k ordini clienti, or <code>null</code> if a matching w k ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti fetchByStatoOrdine_First(boolean statoOrdine,
		OrderByComparator orderByComparator) throws SystemException {
		List<WKOrdiniClienti> list = findByStatoOrdine(statoOrdine, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last w k ordini clienti in the ordered set where statoOrdine = &#63;.
	 *
	 * @param statoOrdine the stato ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching w k ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a matching w k ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti findByStatoOrdine_Last(boolean statoOrdine,
		OrderByComparator orderByComparator)
		throws NoSuchWKOrdiniClientiException, SystemException {
		WKOrdiniClienti wkOrdiniClienti = fetchByStatoOrdine_Last(statoOrdine,
				orderByComparator);

		if (wkOrdiniClienti != null) {
			return wkOrdiniClienti;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("statoOrdine=");
		msg.append(statoOrdine);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWKOrdiniClientiException(msg.toString());
	}

	/**
	 * Returns the last w k ordini clienti in the ordered set where statoOrdine = &#63;.
	 *
	 * @param statoOrdine the stato ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching w k ordini clienti, or <code>null</code> if a matching w k ordini clienti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti fetchByStatoOrdine_Last(boolean statoOrdine,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByStatoOrdine(statoOrdine);

		if (count == 0) {
			return null;
		}

		List<WKOrdiniClienti> list = findByStatoOrdine(statoOrdine, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the w k ordini clientis before and after the current w k ordini clienti in the ordered set where statoOrdine = &#63;.
	 *
	 * @param wkOrdiniClientiPK the primary key of the current w k ordini clienti
	 * @param statoOrdine the stato ordine
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next w k ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a w k ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti[] findByStatoOrdine_PrevAndNext(
		WKOrdiniClientiPK wkOrdiniClientiPK, boolean statoOrdine,
		OrderByComparator orderByComparator)
		throws NoSuchWKOrdiniClientiException, SystemException {
		WKOrdiniClienti wkOrdiniClienti = findByPrimaryKey(wkOrdiniClientiPK);

		Session session = null;

		try {
			session = openSession();

			WKOrdiniClienti[] array = new WKOrdiniClientiImpl[3];

			array[0] = getByStatoOrdine_PrevAndNext(session, wkOrdiniClienti,
					statoOrdine, orderByComparator, true);

			array[1] = wkOrdiniClienti;

			array[2] = getByStatoOrdine_PrevAndNext(session, wkOrdiniClienti,
					statoOrdine, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected WKOrdiniClienti getByStatoOrdine_PrevAndNext(Session session,
		WKOrdiniClienti wkOrdiniClienti, boolean statoOrdine,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_WKORDINICLIENTI_WHERE);

		query.append(_FINDER_COLUMN_STATOORDINE_STATOORDINE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(WKOrdiniClientiModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(statoOrdine);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(wkOrdiniClienti);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<WKOrdiniClienti> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the w k ordini clientis where statoOrdine = &#63; from the database.
	 *
	 * @param statoOrdine the stato ordine
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByStatoOrdine(boolean statoOrdine)
		throws SystemException {
		for (WKOrdiniClienti wkOrdiniClienti : findByStatoOrdine(statoOrdine,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(wkOrdiniClienti);
		}
	}

	/**
	 * Returns the number of w k ordini clientis where statoOrdine = &#63;.
	 *
	 * @param statoOrdine the stato ordine
	 * @return the number of matching w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByStatoOrdine(boolean statoOrdine)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_STATOORDINE;

		Object[] finderArgs = new Object[] { statoOrdine };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_WKORDINICLIENTI_WHERE);

			query.append(_FINDER_COLUMN_STATOORDINE_STATOORDINE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(statoOrdine);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_STATOORDINE_STATOORDINE_2 = "wkOrdiniClienti.statoOrdine = ?";

	public WKOrdiniClientiPersistenceImpl() {
		setModelClass(WKOrdiniClienti.class);
	}

	/**
	 * Caches the w k ordini clienti in the entity cache if it is enabled.
	 *
	 * @param wkOrdiniClienti the w k ordini clienti
	 */
	@Override
	public void cacheResult(WKOrdiniClienti wkOrdiniClienti) {
		EntityCacheUtil.putResult(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKOrdiniClientiImpl.class, wkOrdiniClienti.getPrimaryKey(),
			wkOrdiniClienti);

		wkOrdiniClienti.resetOriginalValues();
	}

	/**
	 * Caches the w k ordini clientis in the entity cache if it is enabled.
	 *
	 * @param wkOrdiniClientis the w k ordini clientis
	 */
	@Override
	public void cacheResult(List<WKOrdiniClienti> wkOrdiniClientis) {
		for (WKOrdiniClienti wkOrdiniClienti : wkOrdiniClientis) {
			if (EntityCacheUtil.getResult(
						WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
						WKOrdiniClientiImpl.class,
						wkOrdiniClienti.getPrimaryKey()) == null) {
				cacheResult(wkOrdiniClienti);
			}
			else {
				wkOrdiniClienti.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all w k ordini clientis.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(WKOrdiniClientiImpl.class.getName());
		}

		EntityCacheUtil.clearCache(WKOrdiniClientiImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the w k ordini clienti.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(WKOrdiniClienti wkOrdiniClienti) {
		EntityCacheUtil.removeResult(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKOrdiniClientiImpl.class, wkOrdiniClienti.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<WKOrdiniClienti> wkOrdiniClientis) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (WKOrdiniClienti wkOrdiniClienti : wkOrdiniClientis) {
			EntityCacheUtil.removeResult(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
				WKOrdiniClientiImpl.class, wkOrdiniClienti.getPrimaryKey());
		}
	}

	/**
	 * Creates a new w k ordini clienti with the primary key. Does not add the w k ordini clienti to the database.
	 *
	 * @param wkOrdiniClientiPK the primary key for the new w k ordini clienti
	 * @return the new w k ordini clienti
	 */
	@Override
	public WKOrdiniClienti create(WKOrdiniClientiPK wkOrdiniClientiPK) {
		WKOrdiniClienti wkOrdiniClienti = new WKOrdiniClientiImpl();

		wkOrdiniClienti.setNew(true);
		wkOrdiniClienti.setPrimaryKey(wkOrdiniClientiPK);

		return wkOrdiniClienti;
	}

	/**
	 * Removes the w k ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param wkOrdiniClientiPK the primary key of the w k ordini clienti
	 * @return the w k ordini clienti that was removed
	 * @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a w k ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti remove(WKOrdiniClientiPK wkOrdiniClientiPK)
		throws NoSuchWKOrdiniClientiException, SystemException {
		return remove((Serializable)wkOrdiniClientiPK);
	}

	/**
	 * Removes the w k ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the w k ordini clienti
	 * @return the w k ordini clienti that was removed
	 * @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a w k ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti remove(Serializable primaryKey)
		throws NoSuchWKOrdiniClientiException, SystemException {
		Session session = null;

		try {
			session = openSession();

			WKOrdiniClienti wkOrdiniClienti = (WKOrdiniClienti)session.get(WKOrdiniClientiImpl.class,
					primaryKey);

			if (wkOrdiniClienti == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchWKOrdiniClientiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(wkOrdiniClienti);
		}
		catch (NoSuchWKOrdiniClientiException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected WKOrdiniClienti removeImpl(WKOrdiniClienti wkOrdiniClienti)
		throws SystemException {
		wkOrdiniClienti = toUnwrappedModel(wkOrdiniClienti);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(wkOrdiniClienti)) {
				wkOrdiniClienti = (WKOrdiniClienti)session.get(WKOrdiniClientiImpl.class,
						wkOrdiniClienti.getPrimaryKeyObj());
			}

			if (wkOrdiniClienti != null) {
				session.delete(wkOrdiniClienti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (wkOrdiniClienti != null) {
			clearCache(wkOrdiniClienti);
		}

		return wkOrdiniClienti;
	}

	@Override
	public WKOrdiniClienti updateImpl(
		it.bysoftware.ct.model.WKOrdiniClienti wkOrdiniClienti)
		throws SystemException {
		wkOrdiniClienti = toUnwrappedModel(wkOrdiniClienti);

		boolean isNew = wkOrdiniClienti.isNew();

		WKOrdiniClientiModelImpl wkOrdiniClientiModelImpl = (WKOrdiniClientiModelImpl)wkOrdiniClienti;

		Session session = null;

		try {
			session = openSession();

			if (wkOrdiniClienti.isNew()) {
				session.save(wkOrdiniClienti);

				wkOrdiniClienti.setNew(false);
			}
			else {
				session.merge(wkOrdiniClienti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !WKOrdiniClientiModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((wkOrdiniClientiModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOTIPOORDINE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						wkOrdiniClientiModelImpl.getOriginalAnno(),
						wkOrdiniClientiModelImpl.getOriginalTipoOrdine()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNOTIPOORDINE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOTIPOORDINE,
					args);

				args = new Object[] {
						wkOrdiniClientiModelImpl.getAnno(),
						wkOrdiniClientiModelImpl.getTipoOrdine()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANNOTIPOORDINE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANNOTIPOORDINE,
					args);
			}

			if ((wkOrdiniClientiModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						wkOrdiniClientiModelImpl.getOriginalCodiceCliente()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICECLIENTE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTE,
					args);

				args = new Object[] { wkOrdiniClientiModelImpl.getCodiceCliente() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODICECLIENTE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CODICECLIENTE,
					args);
			}

			if ((wkOrdiniClientiModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATOORDINE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						wkOrdiniClientiModelImpl.getOriginalStatoOrdine()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATOORDINE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATOORDINE,
					args);

				args = new Object[] { wkOrdiniClientiModelImpl.getStatoOrdine() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATOORDINE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATOORDINE,
					args);
			}
		}

		EntityCacheUtil.putResult(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
			WKOrdiniClientiImpl.class, wkOrdiniClienti.getPrimaryKey(),
			wkOrdiniClienti);

		return wkOrdiniClienti;
	}

	protected WKOrdiniClienti toUnwrappedModel(WKOrdiniClienti wkOrdiniClienti) {
		if (wkOrdiniClienti instanceof WKOrdiniClientiImpl) {
			return wkOrdiniClienti;
		}

		WKOrdiniClientiImpl wkOrdiniClientiImpl = new WKOrdiniClientiImpl();

		wkOrdiniClientiImpl.setNew(wkOrdiniClienti.isNew());
		wkOrdiniClientiImpl.setPrimaryKey(wkOrdiniClienti.getPrimaryKey());

		wkOrdiniClientiImpl.setAnno(wkOrdiniClienti.getAnno());
		wkOrdiniClientiImpl.setTipoOrdine(wkOrdiniClienti.getTipoOrdine());
		wkOrdiniClientiImpl.setNumeroOrdine(wkOrdiniClienti.getNumeroOrdine());
		wkOrdiniClientiImpl.setTipoDocumento(wkOrdiniClienti.getTipoDocumento());
		wkOrdiniClientiImpl.setIdTipoDocumento(wkOrdiniClienti.getIdTipoDocumento());
		wkOrdiniClientiImpl.setStatoOrdine(wkOrdiniClienti.isStatoOrdine());
		wkOrdiniClientiImpl.setTipoSoggetto(wkOrdiniClienti.isTipoSoggetto());
		wkOrdiniClientiImpl.setCodiceCliente(wkOrdiniClienti.getCodiceCliente());
		wkOrdiniClientiImpl.setCodiceEsenzione(wkOrdiniClienti.getCodiceEsenzione());
		wkOrdiniClientiImpl.setCodiceDivisa(wkOrdiniClienti.getCodiceDivisa());
		wkOrdiniClientiImpl.setValoreCambio(wkOrdiniClienti.getValoreCambio());
		wkOrdiniClientiImpl.setDataValoreCambio(wkOrdiniClienti.getDataValoreCambio());
		wkOrdiniClientiImpl.setCodicePianoPag(wkOrdiniClienti.getCodicePianoPag());
		wkOrdiniClientiImpl.setInizioCalcoloPag(wkOrdiniClienti.getInizioCalcoloPag());
		wkOrdiniClientiImpl.setPercentualeScontoMaggiorazione(wkOrdiniClienti.getPercentualeScontoMaggiorazione());
		wkOrdiniClientiImpl.setPercentualeScontoProntaCassa(wkOrdiniClienti.getPercentualeScontoProntaCassa());
		wkOrdiniClientiImpl.setDataDocumento(wkOrdiniClienti.getDataDocumento());
		wkOrdiniClientiImpl.setDataRegistrazione(wkOrdiniClienti.getDataRegistrazione());
		wkOrdiniClientiImpl.setCausaleEstrattoConto(wkOrdiniClienti.getCausaleEstrattoConto());
		wkOrdiniClientiImpl.setCodiceAgente(wkOrdiniClienti.getCodiceAgente());
		wkOrdiniClientiImpl.setCodiceGruppoAgenti(wkOrdiniClienti.getCodiceGruppoAgenti());
		wkOrdiniClientiImpl.setCodiceZona(wkOrdiniClienti.getCodiceZona());
		wkOrdiniClientiImpl.setCodiceDestinatario(wkOrdiniClienti.getCodiceDestinatario());
		wkOrdiniClientiImpl.setCodiceListino(wkOrdiniClienti.getCodiceListino());
		wkOrdiniClientiImpl.setNumeroDecPrezzo(wkOrdiniClienti.getNumeroDecPrezzo());
		wkOrdiniClientiImpl.setNote(wkOrdiniClienti.getNote());
		wkOrdiniClientiImpl.setInviatoEmail(wkOrdiniClienti.isInviatoEmail());
		wkOrdiniClientiImpl.setNomePDF(wkOrdiniClienti.getNomePDF());
		wkOrdiniClientiImpl.setRiferimentoOrdine(wkOrdiniClienti.getRiferimentoOrdine());
		wkOrdiniClientiImpl.setDataConferma(wkOrdiniClienti.getDataConferma());
		wkOrdiniClientiImpl.setConfermaStampata(wkOrdiniClienti.isConfermaStampata());
		wkOrdiniClientiImpl.setTotaleOrdine(wkOrdiniClienti.getTotaleOrdine());
		wkOrdiniClientiImpl.setLibStr1(wkOrdiniClienti.getLibStr1());
		wkOrdiniClientiImpl.setLibStr2(wkOrdiniClienti.getLibStr2());
		wkOrdiniClientiImpl.setLibStr3(wkOrdiniClienti.getLibStr3());
		wkOrdiniClientiImpl.setLibDbl1(wkOrdiniClienti.getLibDbl1());
		wkOrdiniClientiImpl.setLibDbl2(wkOrdiniClienti.getLibDbl2());
		wkOrdiniClientiImpl.setLibDbl3(wkOrdiniClienti.getLibDbl3());
		wkOrdiniClientiImpl.setLibDat1(wkOrdiniClienti.getLibDat1());
		wkOrdiniClientiImpl.setLibDat2(wkOrdiniClienti.getLibDat2());
		wkOrdiniClientiImpl.setLibDat3(wkOrdiniClienti.getLibDat3());
		wkOrdiniClientiImpl.setLibLng1(wkOrdiniClienti.getLibLng1());
		wkOrdiniClientiImpl.setLibLng2(wkOrdiniClienti.getLibLng2());
		wkOrdiniClientiImpl.setLibLng3(wkOrdiniClienti.getLibLng3());

		return wkOrdiniClientiImpl;
	}

	/**
	 * Returns the w k ordini clienti with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the w k ordini clienti
	 * @return the w k ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a w k ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti findByPrimaryKey(Serializable primaryKey)
		throws NoSuchWKOrdiniClientiException, SystemException {
		WKOrdiniClienti wkOrdiniClienti = fetchByPrimaryKey(primaryKey);

		if (wkOrdiniClienti == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchWKOrdiniClientiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return wkOrdiniClienti;
	}

	/**
	 * Returns the w k ordini clienti with the primary key or throws a {@link it.bysoftware.ct.NoSuchWKOrdiniClientiException} if it could not be found.
	 *
	 * @param wkOrdiniClientiPK the primary key of the w k ordini clienti
	 * @return the w k ordini clienti
	 * @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a w k ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti findByPrimaryKey(WKOrdiniClientiPK wkOrdiniClientiPK)
		throws NoSuchWKOrdiniClientiException, SystemException {
		return findByPrimaryKey((Serializable)wkOrdiniClientiPK);
	}

	/**
	 * Returns the w k ordini clienti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the w k ordini clienti
	 * @return the w k ordini clienti, or <code>null</code> if a w k ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		WKOrdiniClienti wkOrdiniClienti = (WKOrdiniClienti)EntityCacheUtil.getResult(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
				WKOrdiniClientiImpl.class, primaryKey);

		if (wkOrdiniClienti == _nullWKOrdiniClienti) {
			return null;
		}

		if (wkOrdiniClienti == null) {
			Session session = null;

			try {
				session = openSession();

				wkOrdiniClienti = (WKOrdiniClienti)session.get(WKOrdiniClientiImpl.class,
						primaryKey);

				if (wkOrdiniClienti != null) {
					cacheResult(wkOrdiniClienti);
				}
				else {
					EntityCacheUtil.putResult(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
						WKOrdiniClientiImpl.class, primaryKey,
						_nullWKOrdiniClienti);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(WKOrdiniClientiModelImpl.ENTITY_CACHE_ENABLED,
					WKOrdiniClientiImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return wkOrdiniClienti;
	}

	/**
	 * Returns the w k ordini clienti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param wkOrdiniClientiPK the primary key of the w k ordini clienti
	 * @return the w k ordini clienti, or <code>null</code> if a w k ordini clienti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WKOrdiniClienti fetchByPrimaryKey(
		WKOrdiniClientiPK wkOrdiniClientiPK) throws SystemException {
		return fetchByPrimaryKey((Serializable)wkOrdiniClientiPK);
	}

	/**
	 * Returns all the w k ordini clientis.
	 *
	 * @return the w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKOrdiniClienti> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the w k ordini clientis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of w k ordini clientis
	 * @param end the upper bound of the range of w k ordini clientis (not inclusive)
	 * @return the range of w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKOrdiniClienti> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the w k ordini clientis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of w k ordini clientis
	 * @param end the upper bound of the range of w k ordini clientis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WKOrdiniClienti> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<WKOrdiniClienti> list = (List<WKOrdiniClienti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_WKORDINICLIENTI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_WKORDINICLIENTI;

				if (pagination) {
					sql = sql.concat(WKOrdiniClientiModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<WKOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<WKOrdiniClienti>(list);
				}
				else {
					list = (List<WKOrdiniClienti>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the w k ordini clientis from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (WKOrdiniClienti wkOrdiniClienti : findAll()) {
			remove(wkOrdiniClienti);
		}
	}

	/**
	 * Returns the number of w k ordini clientis.
	 *
	 * @return the number of w k ordini clientis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_WKORDINICLIENTI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the w k ordini clienti persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.WKOrdiniClienti")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<WKOrdiniClienti>> listenersList = new ArrayList<ModelListener<WKOrdiniClienti>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<WKOrdiniClienti>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(WKOrdiniClientiImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_WKORDINICLIENTI = "SELECT wkOrdiniClienti FROM WKOrdiniClienti wkOrdiniClienti";
	private static final String _SQL_SELECT_WKORDINICLIENTI_WHERE = "SELECT wkOrdiniClienti FROM WKOrdiniClienti wkOrdiniClienti WHERE ";
	private static final String _SQL_COUNT_WKORDINICLIENTI = "SELECT COUNT(wkOrdiniClienti) FROM WKOrdiniClienti wkOrdiniClienti";
	private static final String _SQL_COUNT_WKORDINICLIENTI_WHERE = "SELECT COUNT(wkOrdiniClienti) FROM WKOrdiniClienti wkOrdiniClienti WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "wkOrdiniClienti.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No WKOrdiniClienti exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No WKOrdiniClienti exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(WKOrdiniClientiPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"anno", "tipoOrdine", "numeroOrdine", "tipoDocumento",
				"idTipoDocumento", "statoOrdine", "tipoSoggetto",
				"codiceCliente", "codiceEsenzione", "codiceDivisa",
				"valoreCambio", "dataValoreCambio", "codicePianoPag",
				"inizioCalcoloPag", "percentualeScontoMaggiorazione",
				"percentualeScontoProntaCassa", "dataDocumento",
				"dataRegistrazione", "causaleEstrattoConto", "codiceAgente",
				"codiceGruppoAgenti", "codiceZona", "codiceDestinatario",
				"codiceListino", "numeroDecPrezzo", "note", "inviatoEmail",
				"nomePDF", "riferimentoOrdine", "dataConferma",
				"confermaStampata", "totaleOrdine", "libStr1", "libStr2",
				"libStr3", "libDbl1", "libDbl2", "libDbl3", "libDat1", "libDat2",
				"libDat3", "libLng1", "libLng2", "libLng3"
			});
	private static WKOrdiniClienti _nullWKOrdiniClienti = new WKOrdiniClientiImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<WKOrdiniClienti> toCacheModel() {
				return _nullWKOrdiniClientiCacheModel;
			}
		};

	private static CacheModel<WKOrdiniClienti> _nullWKOrdiniClientiCacheModel = new CacheModel<WKOrdiniClienti>() {
			@Override
			public WKOrdiniClienti toEntityModel() {
				return _nullWKOrdiniClienti;
			}
		};
}