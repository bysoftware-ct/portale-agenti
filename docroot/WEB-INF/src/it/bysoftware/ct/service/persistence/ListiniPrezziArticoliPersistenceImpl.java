/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.bysoftware.ct.NoSuchListiniPrezziArticoliException;
import it.bysoftware.ct.model.ListiniPrezziArticoli;
import it.bysoftware.ct.model.impl.ListiniPrezziArticoliImpl;
import it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the listini prezzi articoli service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ListiniPrezziArticoliPersistence
 * @see ListiniPrezziArticoliUtil
 * @generated
 */
public class ListiniPrezziArticoliPersistenceImpl extends BasePersistenceImpl<ListiniPrezziArticoli>
	implements ListiniPrezziArticoliPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ListiniPrezziArticoliUtil} to access the listini prezzi articoli persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ListiniPrezziArticoliImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ListiniPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ListiniPrezziArticoliModelImpl.FINDER_CACHE_ENABLED,
			ListiniPrezziArticoliImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ListiniPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ListiniPrezziArticoliModelImpl.FINDER_CACHE_ENABLED,
			ListiniPrezziArticoliImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ListiniPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ListiniPrezziArticoliModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DEDICATOSOGGETTO =
		new FinderPath(ListiniPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ListiniPrezziArticoliModelImpl.FINDER_CACHE_ENABLED,
			ListiniPrezziArticoliImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByDedicatoSoggetto",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName(), Integer.class.getName(),
				Date.class.getName(), Double.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_DEDICATOSOGGETTO =
		new FinderPath(ListiniPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ListiniPrezziArticoliModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByDedicatoSoggetto",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName(), Integer.class.getName(),
				Date.class.getName(), Double.class.getName()
			});

	/**
	 * Returns all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param codiceSoggetto the codice soggetto
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @return the matching listini prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListiniPrezziArticoli> findByDedicatoSoggetto(
		String codiceArticolo, String codiceVariante, String codiceSoggetto,
		int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo) throws SystemException {
		return findByDedicatoSoggetto(codiceArticolo, codiceVariante,
			codiceSoggetto, tipoSoggetto, dataInizioValidita,
			quantInizioValiditaPrezzo, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param codiceSoggetto the codice soggetto
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @param start the lower bound of the range of listini prezzi articolis
	 * @param end the upper bound of the range of listini prezzi articolis (not inclusive)
	 * @return the range of matching listini prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListiniPrezziArticoli> findByDedicatoSoggetto(
		String codiceArticolo, String codiceVariante, String codiceSoggetto,
		int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo, int start, int end)
		throws SystemException {
		return findByDedicatoSoggetto(codiceArticolo, codiceVariante,
			codiceSoggetto, tipoSoggetto, dataInizioValidita,
			quantInizioValiditaPrezzo, start, end, null);
	}

	/**
	 * Returns an ordered range of all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param codiceSoggetto the codice soggetto
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @param start the lower bound of the range of listini prezzi articolis
	 * @param end the upper bound of the range of listini prezzi articolis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching listini prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListiniPrezziArticoli> findByDedicatoSoggetto(
		String codiceArticolo, String codiceVariante, String codiceSoggetto,
		int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DEDICATOSOGGETTO;
		finderArgs = new Object[] {
				codiceArticolo, codiceVariante, codiceSoggetto, tipoSoggetto,
				dataInizioValidita, quantInizioValiditaPrezzo,
				
				start, end, orderByComparator
			};

		List<ListiniPrezziArticoli> list = (List<ListiniPrezziArticoli>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ListiniPrezziArticoli listiniPrezziArticoli : list) {
				if (!Validator.equals(codiceArticolo,
							listiniPrezziArticoli.getCodiceArticolo()) ||
						!Validator.equals(codiceVariante,
							listiniPrezziArticoli.getCodiceVariante()) ||
						!Validator.equals(codiceSoggetto,
							listiniPrezziArticoli.getCodiceSoggetto()) ||
						(tipoSoggetto != listiniPrezziArticoli.getTipoSoggetto()) ||
						(dataInizioValidita.getTime() >= listiniPrezziArticoli.getDataInizioValidita()
																				  .getTime()) ||
						(quantInizioValiditaPrezzo >= listiniPrezziArticoli.getQuantInizioValiditaPrezzo())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(8 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(8);
			}

			query.append(_SQL_SELECT_LISTINIPREZZIARTICOLI_WHERE);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEVARIANTE_2);
			}

			boolean bindCodiceSoggetto = false;

			if (codiceSoggetto == null) {
				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICESOGGETTO_1);
			}
			else if (codiceSoggetto.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICESOGGETTO_3);
			}
			else {
				bindCodiceSoggetto = true;

				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICESOGGETTO_2);
			}

			query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_TIPOSOGGETTO_2);

			boolean bindDataInizioValidita = false;

			if (dataInizioValidita == null) {
				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_DATAINIZIOVALIDITA_1);
			}
			else {
				bindDataInizioValidita = true;

				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_DATAINIZIOVALIDITA_2);
			}

			query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_QUANTINIZIOVALIDITAPREZZO_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ListiniPrezziArticoliModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				if (bindCodiceSoggetto) {
					qPos.add(codiceSoggetto);
				}

				qPos.add(tipoSoggetto);

				if (bindDataInizioValidita) {
					qPos.add(CalendarUtil.getTimestamp(dataInizioValidita));
				}

				qPos.add(quantInizioValiditaPrezzo);

				if (!pagination) {
					list = (List<ListiniPrezziArticoli>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ListiniPrezziArticoli>(list);
				}
				else {
					list = (List<ListiniPrezziArticoli>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param codiceSoggetto the codice soggetto
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching listini prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a matching listini prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli findByDedicatoSoggetto_First(
		String codiceArticolo, String codiceVariante, String codiceSoggetto,
		int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo, OrderByComparator orderByComparator)
		throws NoSuchListiniPrezziArticoliException, SystemException {
		ListiniPrezziArticoli listiniPrezziArticoli = fetchByDedicatoSoggetto_First(codiceArticolo,
				codiceVariante, codiceSoggetto, tipoSoggetto,
				dataInizioValidita, quantInizioValiditaPrezzo, orderByComparator);

		if (listiniPrezziArticoli != null) {
			return listiniPrezziArticoli;
		}

		StringBundler msg = new StringBundler(14);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(", codiceSoggetto=");
		msg.append(codiceSoggetto);

		msg.append(", tipoSoggetto=");
		msg.append(tipoSoggetto);

		msg.append(", dataInizioValidita=");
		msg.append(dataInizioValidita);

		msg.append(", quantInizioValiditaPrezzo=");
		msg.append(quantInizioValiditaPrezzo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchListiniPrezziArticoliException(msg.toString());
	}

	/**
	 * Returns the first listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param codiceSoggetto the codice soggetto
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching listini prezzi articoli, or <code>null</code> if a matching listini prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli fetchByDedicatoSoggetto_First(
		String codiceArticolo, String codiceVariante, String codiceSoggetto,
		int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo, OrderByComparator orderByComparator)
		throws SystemException {
		List<ListiniPrezziArticoli> list = findByDedicatoSoggetto(codiceArticolo,
				codiceVariante, codiceSoggetto, tipoSoggetto,
				dataInizioValidita, quantInizioValiditaPrezzo, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param codiceSoggetto the codice soggetto
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching listini prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a matching listini prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli findByDedicatoSoggetto_Last(
		String codiceArticolo, String codiceVariante, String codiceSoggetto,
		int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo, OrderByComparator orderByComparator)
		throws NoSuchListiniPrezziArticoliException, SystemException {
		ListiniPrezziArticoli listiniPrezziArticoli = fetchByDedicatoSoggetto_Last(codiceArticolo,
				codiceVariante, codiceSoggetto, tipoSoggetto,
				dataInizioValidita, quantInizioValiditaPrezzo, orderByComparator);

		if (listiniPrezziArticoli != null) {
			return listiniPrezziArticoli;
		}

		StringBundler msg = new StringBundler(14);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(", codiceSoggetto=");
		msg.append(codiceSoggetto);

		msg.append(", tipoSoggetto=");
		msg.append(tipoSoggetto);

		msg.append(", dataInizioValidita=");
		msg.append(dataInizioValidita);

		msg.append(", quantInizioValiditaPrezzo=");
		msg.append(quantInizioValiditaPrezzo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchListiniPrezziArticoliException(msg.toString());
	}

	/**
	 * Returns the last listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param codiceSoggetto the codice soggetto
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching listini prezzi articoli, or <code>null</code> if a matching listini prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli fetchByDedicatoSoggetto_Last(
		String codiceArticolo, String codiceVariante, String codiceSoggetto,
		int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByDedicatoSoggetto(codiceArticolo, codiceVariante,
				codiceSoggetto, tipoSoggetto, dataInizioValidita,
				quantInizioValiditaPrezzo);

		if (count == 0) {
			return null;
		}

		List<ListiniPrezziArticoli> list = findByDedicatoSoggetto(codiceArticolo,
				codiceVariante, codiceSoggetto, tipoSoggetto,
				dataInizioValidita, quantInizioValiditaPrezzo, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the listini prezzi articolis before and after the current listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * @param listiniPrezziArticoliPK the primary key of the current listini prezzi articoli
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param codiceSoggetto the codice soggetto
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next listini prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a listini prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli[] findByDedicatoSoggetto_PrevAndNext(
		ListiniPrezziArticoliPK listiniPrezziArticoliPK, String codiceArticolo,
		String codiceVariante, String codiceSoggetto, int tipoSoggetto,
		Date dataInizioValidita, double quantInizioValiditaPrezzo,
		OrderByComparator orderByComparator)
		throws NoSuchListiniPrezziArticoliException, SystemException {
		ListiniPrezziArticoli listiniPrezziArticoli = findByPrimaryKey(listiniPrezziArticoliPK);

		Session session = null;

		try {
			session = openSession();

			ListiniPrezziArticoli[] array = new ListiniPrezziArticoliImpl[3];

			array[0] = getByDedicatoSoggetto_PrevAndNext(session,
					listiniPrezziArticoli, codiceArticolo, codiceVariante,
					codiceSoggetto, tipoSoggetto, dataInizioValidita,
					quantInizioValiditaPrezzo, orderByComparator, true);

			array[1] = listiniPrezziArticoli;

			array[2] = getByDedicatoSoggetto_PrevAndNext(session,
					listiniPrezziArticoli, codiceArticolo, codiceVariante,
					codiceSoggetto, tipoSoggetto, dataInizioValidita,
					quantInizioValiditaPrezzo, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ListiniPrezziArticoli getByDedicatoSoggetto_PrevAndNext(
		Session session, ListiniPrezziArticoli listiniPrezziArticoli,
		String codiceArticolo, String codiceVariante, String codiceSoggetto,
		int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LISTINIPREZZIARTICOLI_WHERE);

		boolean bindCodiceArticolo = false;

		if (codiceArticolo == null) {
			query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEARTICOLO_1);
		}
		else if (codiceArticolo.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEARTICOLO_3);
		}
		else {
			bindCodiceArticolo = true;

			query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEARTICOLO_2);
		}

		boolean bindCodiceVariante = false;

		if (codiceVariante == null) {
			query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEVARIANTE_1);
		}
		else if (codiceVariante.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEVARIANTE_3);
		}
		else {
			bindCodiceVariante = true;

			query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEVARIANTE_2);
		}

		boolean bindCodiceSoggetto = false;

		if (codiceSoggetto == null) {
			query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICESOGGETTO_1);
		}
		else if (codiceSoggetto.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICESOGGETTO_3);
		}
		else {
			bindCodiceSoggetto = true;

			query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICESOGGETTO_2);
		}

		query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_TIPOSOGGETTO_2);

		boolean bindDataInizioValidita = false;

		if (dataInizioValidita == null) {
			query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_DATAINIZIOVALIDITA_1);
		}
		else {
			bindDataInizioValidita = true;

			query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_DATAINIZIOVALIDITA_2);
		}

		query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_QUANTINIZIOVALIDITAPREZZO_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ListiniPrezziArticoliModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodiceArticolo) {
			qPos.add(codiceArticolo);
		}

		if (bindCodiceVariante) {
			qPos.add(codiceVariante);
		}

		if (bindCodiceSoggetto) {
			qPos.add(codiceSoggetto);
		}

		qPos.add(tipoSoggetto);

		if (bindDataInizioValidita) {
			qPos.add(CalendarUtil.getTimestamp(dataInizioValidita));
		}

		qPos.add(quantInizioValiditaPrezzo);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(listiniPrezziArticoli);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ListiniPrezziArticoli> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63; from the database.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param codiceSoggetto the codice soggetto
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByDedicatoSoggetto(String codiceArticolo,
		String codiceVariante, String codiceSoggetto, int tipoSoggetto,
		Date dataInizioValidita, double quantInizioValiditaPrezzo)
		throws SystemException {
		for (ListiniPrezziArticoli listiniPrezziArticoli : findByDedicatoSoggetto(
				codiceArticolo, codiceVariante, codiceSoggetto, tipoSoggetto,
				dataInizioValidita, quantInizioValiditaPrezzo,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(listiniPrezziArticoli);
		}
	}

	/**
	 * Returns the number of listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param codiceSoggetto the codice soggetto
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @return the number of matching listini prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByDedicatoSoggetto(String codiceArticolo,
		String codiceVariante, String codiceSoggetto, int tipoSoggetto,
		Date dataInizioValidita, double quantInizioValiditaPrezzo)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_DEDICATOSOGGETTO;

		Object[] finderArgs = new Object[] {
				codiceArticolo, codiceVariante, codiceSoggetto, tipoSoggetto,
				dataInizioValidita, quantInizioValiditaPrezzo
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(7);

			query.append(_SQL_COUNT_LISTINIPREZZIARTICOLI_WHERE);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICEVARIANTE_2);
			}

			boolean bindCodiceSoggetto = false;

			if (codiceSoggetto == null) {
				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICESOGGETTO_1);
			}
			else if (codiceSoggetto.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICESOGGETTO_3);
			}
			else {
				bindCodiceSoggetto = true;

				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_CODICESOGGETTO_2);
			}

			query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_TIPOSOGGETTO_2);

			boolean bindDataInizioValidita = false;

			if (dataInizioValidita == null) {
				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_DATAINIZIOVALIDITA_1);
			}
			else {
				bindDataInizioValidita = true;

				query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_DATAINIZIOVALIDITA_2);
			}

			query.append(_FINDER_COLUMN_DEDICATOSOGGETTO_QUANTINIZIOVALIDITAPREZZO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				if (bindCodiceSoggetto) {
					qPos.add(codiceSoggetto);
				}

				qPos.add(tipoSoggetto);

				if (bindDataInizioValidita) {
					qPos.add(CalendarUtil.getTimestamp(dataInizioValidita));
				}

				qPos.add(quantInizioValiditaPrezzo);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_DEDICATOSOGGETTO_CODICEARTICOLO_1 =
		"listiniPrezziArticoli.id.codiceArticolo IS NULL AND ";
	private static final String _FINDER_COLUMN_DEDICATOSOGGETTO_CODICEARTICOLO_2 =
		"listiniPrezziArticoli.id.codiceArticolo = ? AND ";
	private static final String _FINDER_COLUMN_DEDICATOSOGGETTO_CODICEARTICOLO_3 =
		"(listiniPrezziArticoli.id.codiceArticolo IS NULL OR listiniPrezziArticoli.id.codiceArticolo = '') AND ";
	private static final String _FINDER_COLUMN_DEDICATOSOGGETTO_CODICEVARIANTE_1 =
		"listiniPrezziArticoli.id.codiceVariante IS NULL AND ";
	private static final String _FINDER_COLUMN_DEDICATOSOGGETTO_CODICEVARIANTE_2 =
		"listiniPrezziArticoli.id.codiceVariante = ? AND ";
	private static final String _FINDER_COLUMN_DEDICATOSOGGETTO_CODICEVARIANTE_3 =
		"(listiniPrezziArticoli.id.codiceVariante IS NULL OR listiniPrezziArticoli.id.codiceVariante = '') AND ";
	private static final String _FINDER_COLUMN_DEDICATOSOGGETTO_CODICESOGGETTO_1 =
		"listiniPrezziArticoli.id.codiceSoggetto IS NULL AND ";
	private static final String _FINDER_COLUMN_DEDICATOSOGGETTO_CODICESOGGETTO_2 =
		"listiniPrezziArticoli.id.codiceSoggetto = ? AND ";
	private static final String _FINDER_COLUMN_DEDICATOSOGGETTO_CODICESOGGETTO_3 =
		"(listiniPrezziArticoli.id.codiceSoggetto IS NULL OR listiniPrezziArticoli.id.codiceSoggetto = '') AND ";
	private static final String _FINDER_COLUMN_DEDICATOSOGGETTO_TIPOSOGGETTO_2 = "listiniPrezziArticoli.id.tipoSoggetto = ? AND ";
	private static final String _FINDER_COLUMN_DEDICATOSOGGETTO_DATAINIZIOVALIDITA_1 =
		"listiniPrezziArticoli.id.dataInizioValidita > NULL AND ";
	private static final String _FINDER_COLUMN_DEDICATOSOGGETTO_DATAINIZIOVALIDITA_2 =
		"listiniPrezziArticoli.id.dataInizioValidita > ? AND ";
	private static final String _FINDER_COLUMN_DEDICATOSOGGETTO_QUANTINIZIOVALIDITAPREZZO_2 =
		"listiniPrezziArticoli.id.quantInizioValiditaPrezzo > ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GENERICO = new FinderPath(ListiniPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ListiniPrezziArticoliModelImpl.FINDER_CACHE_ENABLED,
			ListiniPrezziArticoliImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGenerico",
			new String[] {
				String.class.getName(), String.class.getName(),
				Integer.class.getName(), Date.class.getName(),
				Double.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_GENERICO =
		new FinderPath(ListiniPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ListiniPrezziArticoliModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByGenerico",
			new String[] {
				String.class.getName(), String.class.getName(),
				Integer.class.getName(), Date.class.getName(),
				Double.class.getName()
			});

	/**
	 * Returns all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @return the matching listini prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListiniPrezziArticoli> findByGenerico(String codiceArticolo,
		String codiceVariante, int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo) throws SystemException {
		return findByGenerico(codiceArticolo, codiceVariante, tipoSoggetto,
			dataInizioValidita, quantInizioValiditaPrezzo, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @param start the lower bound of the range of listini prezzi articolis
	 * @param end the upper bound of the range of listini prezzi articolis (not inclusive)
	 * @return the range of matching listini prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListiniPrezziArticoli> findByGenerico(String codiceArticolo,
		String codiceVariante, int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo, int start, int end)
		throws SystemException {
		return findByGenerico(codiceArticolo, codiceVariante, tipoSoggetto,
			dataInizioValidita, quantInizioValiditaPrezzo, start, end, null);
	}

	/**
	 * Returns an ordered range of all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @param start the lower bound of the range of listini prezzi articolis
	 * @param end the upper bound of the range of listini prezzi articolis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching listini prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListiniPrezziArticoli> findByGenerico(String codiceArticolo,
		String codiceVariante, int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GENERICO;
		finderArgs = new Object[] {
				codiceArticolo, codiceVariante, tipoSoggetto, dataInizioValidita,
				quantInizioValiditaPrezzo,
				
				start, end, orderByComparator
			};

		List<ListiniPrezziArticoli> list = (List<ListiniPrezziArticoli>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ListiniPrezziArticoli listiniPrezziArticoli : list) {
				if (!Validator.equals(codiceArticolo,
							listiniPrezziArticoli.getCodiceArticolo()) ||
						!Validator.equals(codiceVariante,
							listiniPrezziArticoli.getCodiceVariante()) ||
						(tipoSoggetto != listiniPrezziArticoli.getTipoSoggetto()) ||
						(dataInizioValidita.getTime() >= listiniPrezziArticoli.getDataInizioValidita()
																				  .getTime()) ||
						(quantInizioValiditaPrezzo >= listiniPrezziArticoli.getQuantInizioValiditaPrezzo())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(7 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(7);
			}

			query.append(_SQL_SELECT_LISTINIPREZZIARTICOLI_WHERE);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_GENERICO_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_GENERICO_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_GENERICO_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_GENERICO_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_GENERICO_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_GENERICO_CODICEVARIANTE_2);
			}

			query.append(_FINDER_COLUMN_GENERICO_TIPOSOGGETTO_2);

			boolean bindDataInizioValidita = false;

			if (dataInizioValidita == null) {
				query.append(_FINDER_COLUMN_GENERICO_DATAINIZIOVALIDITA_1);
			}
			else {
				bindDataInizioValidita = true;

				query.append(_FINDER_COLUMN_GENERICO_DATAINIZIOVALIDITA_2);
			}

			query.append(_FINDER_COLUMN_GENERICO_QUANTINIZIOVALIDITAPREZZO_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ListiniPrezziArticoliModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				qPos.add(tipoSoggetto);

				if (bindDataInizioValidita) {
					qPos.add(CalendarUtil.getTimestamp(dataInizioValidita));
				}

				qPos.add(quantInizioValiditaPrezzo);

				if (!pagination) {
					list = (List<ListiniPrezziArticoli>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ListiniPrezziArticoli>(list);
				}
				else {
					list = (List<ListiniPrezziArticoli>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching listini prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a matching listini prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli findByGenerico_First(String codiceArticolo,
		String codiceVariante, int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo, OrderByComparator orderByComparator)
		throws NoSuchListiniPrezziArticoliException, SystemException {
		ListiniPrezziArticoli listiniPrezziArticoli = fetchByGenerico_First(codiceArticolo,
				codiceVariante, tipoSoggetto, dataInizioValidita,
				quantInizioValiditaPrezzo, orderByComparator);

		if (listiniPrezziArticoli != null) {
			return listiniPrezziArticoli;
		}

		StringBundler msg = new StringBundler(12);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(", tipoSoggetto=");
		msg.append(tipoSoggetto);

		msg.append(", dataInizioValidita=");
		msg.append(dataInizioValidita);

		msg.append(", quantInizioValiditaPrezzo=");
		msg.append(quantInizioValiditaPrezzo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchListiniPrezziArticoliException(msg.toString());
	}

	/**
	 * Returns the first listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching listini prezzi articoli, or <code>null</code> if a matching listini prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli fetchByGenerico_First(String codiceArticolo,
		String codiceVariante, int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo, OrderByComparator orderByComparator)
		throws SystemException {
		List<ListiniPrezziArticoli> list = findByGenerico(codiceArticolo,
				codiceVariante, tipoSoggetto, dataInizioValidita,
				quantInizioValiditaPrezzo, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching listini prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a matching listini prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli findByGenerico_Last(String codiceArticolo,
		String codiceVariante, int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo, OrderByComparator orderByComparator)
		throws NoSuchListiniPrezziArticoliException, SystemException {
		ListiniPrezziArticoli listiniPrezziArticoli = fetchByGenerico_Last(codiceArticolo,
				codiceVariante, tipoSoggetto, dataInizioValidita,
				quantInizioValiditaPrezzo, orderByComparator);

		if (listiniPrezziArticoli != null) {
			return listiniPrezziArticoli;
		}

		StringBundler msg = new StringBundler(12);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codiceArticolo=");
		msg.append(codiceArticolo);

		msg.append(", codiceVariante=");
		msg.append(codiceVariante);

		msg.append(", tipoSoggetto=");
		msg.append(tipoSoggetto);

		msg.append(", dataInizioValidita=");
		msg.append(dataInizioValidita);

		msg.append(", quantInizioValiditaPrezzo=");
		msg.append(quantInizioValiditaPrezzo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchListiniPrezziArticoliException(msg.toString());
	}

	/**
	 * Returns the last listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching listini prezzi articoli, or <code>null</code> if a matching listini prezzi articoli could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli fetchByGenerico_Last(String codiceArticolo,
		String codiceVariante, int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByGenerico(codiceArticolo, codiceVariante,
				tipoSoggetto, dataInizioValidita, quantInizioValiditaPrezzo);

		if (count == 0) {
			return null;
		}

		List<ListiniPrezziArticoli> list = findByGenerico(codiceArticolo,
				codiceVariante, tipoSoggetto, dataInizioValidita,
				quantInizioValiditaPrezzo, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the listini prezzi articolis before and after the current listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * @param listiniPrezziArticoliPK the primary key of the current listini prezzi articoli
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next listini prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a listini prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli[] findByGenerico_PrevAndNext(
		ListiniPrezziArticoliPK listiniPrezziArticoliPK, String codiceArticolo,
		String codiceVariante, int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo, OrderByComparator orderByComparator)
		throws NoSuchListiniPrezziArticoliException, SystemException {
		ListiniPrezziArticoli listiniPrezziArticoli = findByPrimaryKey(listiniPrezziArticoliPK);

		Session session = null;

		try {
			session = openSession();

			ListiniPrezziArticoli[] array = new ListiniPrezziArticoliImpl[3];

			array[0] = getByGenerico_PrevAndNext(session,
					listiniPrezziArticoli, codiceArticolo, codiceVariante,
					tipoSoggetto, dataInizioValidita,
					quantInizioValiditaPrezzo, orderByComparator, true);

			array[1] = listiniPrezziArticoli;

			array[2] = getByGenerico_PrevAndNext(session,
					listiniPrezziArticoli, codiceArticolo, codiceVariante,
					tipoSoggetto, dataInizioValidita,
					quantInizioValiditaPrezzo, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ListiniPrezziArticoli getByGenerico_PrevAndNext(Session session,
		ListiniPrezziArticoli listiniPrezziArticoli, String codiceArticolo,
		String codiceVariante, int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LISTINIPREZZIARTICOLI_WHERE);

		boolean bindCodiceArticolo = false;

		if (codiceArticolo == null) {
			query.append(_FINDER_COLUMN_GENERICO_CODICEARTICOLO_1);
		}
		else if (codiceArticolo.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_GENERICO_CODICEARTICOLO_3);
		}
		else {
			bindCodiceArticolo = true;

			query.append(_FINDER_COLUMN_GENERICO_CODICEARTICOLO_2);
		}

		boolean bindCodiceVariante = false;

		if (codiceVariante == null) {
			query.append(_FINDER_COLUMN_GENERICO_CODICEVARIANTE_1);
		}
		else if (codiceVariante.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_GENERICO_CODICEVARIANTE_3);
		}
		else {
			bindCodiceVariante = true;

			query.append(_FINDER_COLUMN_GENERICO_CODICEVARIANTE_2);
		}

		query.append(_FINDER_COLUMN_GENERICO_TIPOSOGGETTO_2);

		boolean bindDataInizioValidita = false;

		if (dataInizioValidita == null) {
			query.append(_FINDER_COLUMN_GENERICO_DATAINIZIOVALIDITA_1);
		}
		else {
			bindDataInizioValidita = true;

			query.append(_FINDER_COLUMN_GENERICO_DATAINIZIOVALIDITA_2);
		}

		query.append(_FINDER_COLUMN_GENERICO_QUANTINIZIOVALIDITAPREZZO_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ListiniPrezziArticoliModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodiceArticolo) {
			qPos.add(codiceArticolo);
		}

		if (bindCodiceVariante) {
			qPos.add(codiceVariante);
		}

		qPos.add(tipoSoggetto);

		if (bindDataInizioValidita) {
			qPos.add(CalendarUtil.getTimestamp(dataInizioValidita));
		}

		qPos.add(quantInizioValiditaPrezzo);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(listiniPrezziArticoli);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ListiniPrezziArticoli> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63; from the database.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByGenerico(String codiceArticolo, String codiceVariante,
		int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo) throws SystemException {
		for (ListiniPrezziArticoli listiniPrezziArticoli : findByGenerico(
				codiceArticolo, codiceVariante, tipoSoggetto,
				dataInizioValidita, quantInizioValiditaPrezzo,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(listiniPrezziArticoli);
		}
	}

	/**
	 * Returns the number of listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	 *
	 * @param codiceArticolo the codice articolo
	 * @param codiceVariante the codice variante
	 * @param tipoSoggetto the tipo soggetto
	 * @param dataInizioValidita the data inizio validita
	 * @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	 * @return the number of matching listini prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByGenerico(String codiceArticolo, String codiceVariante,
		int tipoSoggetto, Date dataInizioValidita,
		double quantInizioValiditaPrezzo) throws SystemException {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_GENERICO;

		Object[] finderArgs = new Object[] {
				codiceArticolo, codiceVariante, tipoSoggetto, dataInizioValidita,
				quantInizioValiditaPrezzo
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(6);

			query.append(_SQL_COUNT_LISTINIPREZZIARTICOLI_WHERE);

			boolean bindCodiceArticolo = false;

			if (codiceArticolo == null) {
				query.append(_FINDER_COLUMN_GENERICO_CODICEARTICOLO_1);
			}
			else if (codiceArticolo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_GENERICO_CODICEARTICOLO_3);
			}
			else {
				bindCodiceArticolo = true;

				query.append(_FINDER_COLUMN_GENERICO_CODICEARTICOLO_2);
			}

			boolean bindCodiceVariante = false;

			if (codiceVariante == null) {
				query.append(_FINDER_COLUMN_GENERICO_CODICEVARIANTE_1);
			}
			else if (codiceVariante.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_GENERICO_CODICEVARIANTE_3);
			}
			else {
				bindCodiceVariante = true;

				query.append(_FINDER_COLUMN_GENERICO_CODICEVARIANTE_2);
			}

			query.append(_FINDER_COLUMN_GENERICO_TIPOSOGGETTO_2);

			boolean bindDataInizioValidita = false;

			if (dataInizioValidita == null) {
				query.append(_FINDER_COLUMN_GENERICO_DATAINIZIOVALIDITA_1);
			}
			else {
				bindDataInizioValidita = true;

				query.append(_FINDER_COLUMN_GENERICO_DATAINIZIOVALIDITA_2);
			}

			query.append(_FINDER_COLUMN_GENERICO_QUANTINIZIOVALIDITAPREZZO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodiceArticolo) {
					qPos.add(codiceArticolo);
				}

				if (bindCodiceVariante) {
					qPos.add(codiceVariante);
				}

				qPos.add(tipoSoggetto);

				if (bindDataInizioValidita) {
					qPos.add(CalendarUtil.getTimestamp(dataInizioValidita));
				}

				qPos.add(quantInizioValiditaPrezzo);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GENERICO_CODICEARTICOLO_1 = "listiniPrezziArticoli.id.codiceArticolo IS NULL AND ";
	private static final String _FINDER_COLUMN_GENERICO_CODICEARTICOLO_2 = "listiniPrezziArticoli.id.codiceArticolo = ? AND ";
	private static final String _FINDER_COLUMN_GENERICO_CODICEARTICOLO_3 = "(listiniPrezziArticoli.id.codiceArticolo IS NULL OR listiniPrezziArticoli.id.codiceArticolo = '') AND ";
	private static final String _FINDER_COLUMN_GENERICO_CODICEVARIANTE_1 = "listiniPrezziArticoli.id.codiceVariante IS NULL AND ";
	private static final String _FINDER_COLUMN_GENERICO_CODICEVARIANTE_2 = "listiniPrezziArticoli.id.codiceVariante = ? AND ";
	private static final String _FINDER_COLUMN_GENERICO_CODICEVARIANTE_3 = "(listiniPrezziArticoli.id.codiceVariante IS NULL OR listiniPrezziArticoli.id.codiceVariante = '') AND ";
	private static final String _FINDER_COLUMN_GENERICO_TIPOSOGGETTO_2 = "listiniPrezziArticoli.id.tipoSoggetto = ? AND ";
	private static final String _FINDER_COLUMN_GENERICO_DATAINIZIOVALIDITA_1 = "listiniPrezziArticoli.id.dataInizioValidita > NULL AND ";
	private static final String _FINDER_COLUMN_GENERICO_DATAINIZIOVALIDITA_2 = "listiniPrezziArticoli.id.dataInizioValidita > ? AND ";
	private static final String _FINDER_COLUMN_GENERICO_QUANTINIZIOVALIDITAPREZZO_2 =
		"listiniPrezziArticoli.id.quantInizioValiditaPrezzo > ?";

	public ListiniPrezziArticoliPersistenceImpl() {
		setModelClass(ListiniPrezziArticoli.class);
	}

	/**
	 * Caches the listini prezzi articoli in the entity cache if it is enabled.
	 *
	 * @param listiniPrezziArticoli the listini prezzi articoli
	 */
	@Override
	public void cacheResult(ListiniPrezziArticoli listiniPrezziArticoli) {
		EntityCacheUtil.putResult(ListiniPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ListiniPrezziArticoliImpl.class,
			listiniPrezziArticoli.getPrimaryKey(), listiniPrezziArticoli);

		listiniPrezziArticoli.resetOriginalValues();
	}

	/**
	 * Caches the listini prezzi articolis in the entity cache if it is enabled.
	 *
	 * @param listiniPrezziArticolis the listini prezzi articolis
	 */
	@Override
	public void cacheResult(List<ListiniPrezziArticoli> listiniPrezziArticolis) {
		for (ListiniPrezziArticoli listiniPrezziArticoli : listiniPrezziArticolis) {
			if (EntityCacheUtil.getResult(
						ListiniPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
						ListiniPrezziArticoliImpl.class,
						listiniPrezziArticoli.getPrimaryKey()) == null) {
				cacheResult(listiniPrezziArticoli);
			}
			else {
				listiniPrezziArticoli.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all listini prezzi articolis.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ListiniPrezziArticoliImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ListiniPrezziArticoliImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the listini prezzi articoli.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ListiniPrezziArticoli listiniPrezziArticoli) {
		EntityCacheUtil.removeResult(ListiniPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ListiniPrezziArticoliImpl.class,
			listiniPrezziArticoli.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ListiniPrezziArticoli> listiniPrezziArticolis) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ListiniPrezziArticoli listiniPrezziArticoli : listiniPrezziArticolis) {
			EntityCacheUtil.removeResult(ListiniPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
				ListiniPrezziArticoliImpl.class,
				listiniPrezziArticoli.getPrimaryKey());
		}
	}

	/**
	 * Creates a new listini prezzi articoli with the primary key. Does not add the listini prezzi articoli to the database.
	 *
	 * @param listiniPrezziArticoliPK the primary key for the new listini prezzi articoli
	 * @return the new listini prezzi articoli
	 */
	@Override
	public ListiniPrezziArticoli create(
		ListiniPrezziArticoliPK listiniPrezziArticoliPK) {
		ListiniPrezziArticoli listiniPrezziArticoli = new ListiniPrezziArticoliImpl();

		listiniPrezziArticoli.setNew(true);
		listiniPrezziArticoli.setPrimaryKey(listiniPrezziArticoliPK);

		return listiniPrezziArticoli;
	}

	/**
	 * Removes the listini prezzi articoli with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param listiniPrezziArticoliPK the primary key of the listini prezzi articoli
	 * @return the listini prezzi articoli that was removed
	 * @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a listini prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli remove(
		ListiniPrezziArticoliPK listiniPrezziArticoliPK)
		throws NoSuchListiniPrezziArticoliException, SystemException {
		return remove((Serializable)listiniPrezziArticoliPK);
	}

	/**
	 * Removes the listini prezzi articoli with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the listini prezzi articoli
	 * @return the listini prezzi articoli that was removed
	 * @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a listini prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli remove(Serializable primaryKey)
		throws NoSuchListiniPrezziArticoliException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ListiniPrezziArticoli listiniPrezziArticoli = (ListiniPrezziArticoli)session.get(ListiniPrezziArticoliImpl.class,
					primaryKey);

			if (listiniPrezziArticoli == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchListiniPrezziArticoliException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(listiniPrezziArticoli);
		}
		catch (NoSuchListiniPrezziArticoliException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ListiniPrezziArticoli removeImpl(
		ListiniPrezziArticoli listiniPrezziArticoli) throws SystemException {
		listiniPrezziArticoli = toUnwrappedModel(listiniPrezziArticoli);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(listiniPrezziArticoli)) {
				listiniPrezziArticoli = (ListiniPrezziArticoli)session.get(ListiniPrezziArticoliImpl.class,
						listiniPrezziArticoli.getPrimaryKeyObj());
			}

			if (listiniPrezziArticoli != null) {
				session.delete(listiniPrezziArticoli);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (listiniPrezziArticoli != null) {
			clearCache(listiniPrezziArticoli);
		}

		return listiniPrezziArticoli;
	}

	@Override
	public ListiniPrezziArticoli updateImpl(
		it.bysoftware.ct.model.ListiniPrezziArticoli listiniPrezziArticoli)
		throws SystemException {
		listiniPrezziArticoli = toUnwrappedModel(listiniPrezziArticoli);

		boolean isNew = listiniPrezziArticoli.isNew();

		Session session = null;

		try {
			session = openSession();

			if (listiniPrezziArticoli.isNew()) {
				session.save(listiniPrezziArticoli);

				listiniPrezziArticoli.setNew(false);
			}
			else {
				session.merge(listiniPrezziArticoli);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ListiniPrezziArticoliModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ListiniPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
			ListiniPrezziArticoliImpl.class,
			listiniPrezziArticoli.getPrimaryKey(), listiniPrezziArticoli);

		return listiniPrezziArticoli;
	}

	protected ListiniPrezziArticoli toUnwrappedModel(
		ListiniPrezziArticoli listiniPrezziArticoli) {
		if (listiniPrezziArticoli instanceof ListiniPrezziArticoliImpl) {
			return listiniPrezziArticoli;
		}

		ListiniPrezziArticoliImpl listiniPrezziArticoliImpl = new ListiniPrezziArticoliImpl();

		listiniPrezziArticoliImpl.setNew(listiniPrezziArticoli.isNew());
		listiniPrezziArticoliImpl.setPrimaryKey(listiniPrezziArticoli.getPrimaryKey());

		listiniPrezziArticoliImpl.setCodiceListino(listiniPrezziArticoli.getCodiceListino());
		listiniPrezziArticoliImpl.setCodiceDivisa(listiniPrezziArticoli.getCodiceDivisa());
		listiniPrezziArticoliImpl.setTipoSoggetto(listiniPrezziArticoli.getTipoSoggetto());
		listiniPrezziArticoliImpl.setCodiceSoggetto(listiniPrezziArticoli.getCodiceSoggetto());
		listiniPrezziArticoliImpl.setDataInizioValidita(listiniPrezziArticoli.getDataInizioValidita());
		listiniPrezziArticoliImpl.setCodiceArticolo(listiniPrezziArticoli.getCodiceArticolo());
		listiniPrezziArticoliImpl.setCodiceVariante(listiniPrezziArticoli.getCodiceVariante());
		listiniPrezziArticoliImpl.setCodiceLavorazione(listiniPrezziArticoli.getCodiceLavorazione());
		listiniPrezziArticoliImpl.setQuantInizioValiditaPrezzo(listiniPrezziArticoli.getQuantInizioValiditaPrezzo());
		listiniPrezziArticoliImpl.setDataFineValidita(listiniPrezziArticoli.getDataFineValidita());
		listiniPrezziArticoliImpl.setQuantFineValiditaPrezzo(listiniPrezziArticoli.getQuantFineValiditaPrezzo());
		listiniPrezziArticoliImpl.setPrezzoNettoIVA(listiniPrezziArticoli.getPrezzoNettoIVA());
		listiniPrezziArticoliImpl.setCodiceIVA(listiniPrezziArticoli.getCodiceIVA());
		listiniPrezziArticoliImpl.setUtilizzoSconti(listiniPrezziArticoli.isUtilizzoSconti());
		listiniPrezziArticoliImpl.setScontoListino1(listiniPrezziArticoli.getScontoListino1());
		listiniPrezziArticoliImpl.setScontoListino2(listiniPrezziArticoli.getScontoListino2());
		listiniPrezziArticoliImpl.setScontoListino3(listiniPrezziArticoli.getScontoListino3());

		return listiniPrezziArticoliImpl;
	}

	/**
	 * Returns the listini prezzi articoli with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the listini prezzi articoli
	 * @return the listini prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a listini prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli findByPrimaryKey(Serializable primaryKey)
		throws NoSuchListiniPrezziArticoliException, SystemException {
		ListiniPrezziArticoli listiniPrezziArticoli = fetchByPrimaryKey(primaryKey);

		if (listiniPrezziArticoli == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchListiniPrezziArticoliException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return listiniPrezziArticoli;
	}

	/**
	 * Returns the listini prezzi articoli with the primary key or throws a {@link it.bysoftware.ct.NoSuchListiniPrezziArticoliException} if it could not be found.
	 *
	 * @param listiniPrezziArticoliPK the primary key of the listini prezzi articoli
	 * @return the listini prezzi articoli
	 * @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a listini prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli findByPrimaryKey(
		ListiniPrezziArticoliPK listiniPrezziArticoliPK)
		throws NoSuchListiniPrezziArticoliException, SystemException {
		return findByPrimaryKey((Serializable)listiniPrezziArticoliPK);
	}

	/**
	 * Returns the listini prezzi articoli with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the listini prezzi articoli
	 * @return the listini prezzi articoli, or <code>null</code> if a listini prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ListiniPrezziArticoli listiniPrezziArticoli = (ListiniPrezziArticoli)EntityCacheUtil.getResult(ListiniPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
				ListiniPrezziArticoliImpl.class, primaryKey);

		if (listiniPrezziArticoli == _nullListiniPrezziArticoli) {
			return null;
		}

		if (listiniPrezziArticoli == null) {
			Session session = null;

			try {
				session = openSession();

				listiniPrezziArticoli = (ListiniPrezziArticoli)session.get(ListiniPrezziArticoliImpl.class,
						primaryKey);

				if (listiniPrezziArticoli != null) {
					cacheResult(listiniPrezziArticoli);
				}
				else {
					EntityCacheUtil.putResult(ListiniPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
						ListiniPrezziArticoliImpl.class, primaryKey,
						_nullListiniPrezziArticoli);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ListiniPrezziArticoliModelImpl.ENTITY_CACHE_ENABLED,
					ListiniPrezziArticoliImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return listiniPrezziArticoli;
	}

	/**
	 * Returns the listini prezzi articoli with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param listiniPrezziArticoliPK the primary key of the listini prezzi articoli
	 * @return the listini prezzi articoli, or <code>null</code> if a listini prezzi articoli with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ListiniPrezziArticoli fetchByPrimaryKey(
		ListiniPrezziArticoliPK listiniPrezziArticoliPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)listiniPrezziArticoliPK);
	}

	/**
	 * Returns all the listini prezzi articolis.
	 *
	 * @return the listini prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListiniPrezziArticoli> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the listini prezzi articolis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of listini prezzi articolis
	 * @param end the upper bound of the range of listini prezzi articolis (not inclusive)
	 * @return the range of listini prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListiniPrezziArticoli> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the listini prezzi articolis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of listini prezzi articolis
	 * @param end the upper bound of the range of listini prezzi articolis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of listini prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ListiniPrezziArticoli> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ListiniPrezziArticoli> list = (List<ListiniPrezziArticoli>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_LISTINIPREZZIARTICOLI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LISTINIPREZZIARTICOLI;

				if (pagination) {
					sql = sql.concat(ListiniPrezziArticoliModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ListiniPrezziArticoli>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ListiniPrezziArticoli>(list);
				}
				else {
					list = (List<ListiniPrezziArticoli>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the listini prezzi articolis from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ListiniPrezziArticoli listiniPrezziArticoli : findAll()) {
			remove(listiniPrezziArticoli);
		}
	}

	/**
	 * Returns the number of listini prezzi articolis.
	 *
	 * @return the number of listini prezzi articolis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LISTINIPREZZIARTICOLI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the listini prezzi articoli persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.bysoftware.ct.model.ListiniPrezziArticoli")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ListiniPrezziArticoli>> listenersList = new ArrayList<ModelListener<ListiniPrezziArticoli>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ListiniPrezziArticoli>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ListiniPrezziArticoliImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_LISTINIPREZZIARTICOLI = "SELECT listiniPrezziArticoli FROM ListiniPrezziArticoli listiniPrezziArticoli";
	private static final String _SQL_SELECT_LISTINIPREZZIARTICOLI_WHERE = "SELECT listiniPrezziArticoli FROM ListiniPrezziArticoli listiniPrezziArticoli WHERE ";
	private static final String _SQL_COUNT_LISTINIPREZZIARTICOLI = "SELECT COUNT(listiniPrezziArticoli) FROM ListiniPrezziArticoli listiniPrezziArticoli";
	private static final String _SQL_COUNT_LISTINIPREZZIARTICOLI_WHERE = "SELECT COUNT(listiniPrezziArticoli) FROM ListiniPrezziArticoli listiniPrezziArticoli WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "listiniPrezziArticoli.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ListiniPrezziArticoli exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ListiniPrezziArticoli exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ListiniPrezziArticoliPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"codiceListino", "codiceDivisa", "tipoSoggetto",
				"codiceSoggetto", "dataInizioValidita", "codiceArticolo",
				"codiceVariante", "codiceLavorazione",
				"quantInizioValiditaPrezzo", "dataFineValidita",
				"quantFineValiditaPrezzo", "prezzoNettoIVA", "codiceIVA",
				"utilizzoSconti", "scontoListino1", "scontoListino2",
				"scontoListino3"
			});
	private static ListiniPrezziArticoli _nullListiniPrezziArticoli = new ListiniPrezziArticoliImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ListiniPrezziArticoli> toCacheModel() {
				return _nullListiniPrezziArticoliCacheModel;
			}
		};

	private static CacheModel<ListiniPrezziArticoli> _nullListiniPrezziArticoliCacheModel =
		new CacheModel<ListiniPrezziArticoli>() {
			@Override
			public ListiniPrezziArticoli toEntityModel() {
				return _nullListiniPrezziArticoli;
			}
		};
}