/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.impl;

import java.sql.Date;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.bysoftware.ct.NoSuchTestataFattureClientiException;
import it.bysoftware.ct.model.TestataFattureClienti;
import it.bysoftware.ct.service.base.TestataFattureClientiLocalServiceBaseImpl;

/**
 * The implementation of the testata fatture clienti local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.TestataFattureClientiLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.TestataFattureClientiLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.TestataFattureClientiLocalServiceUtil
 */
public class TestataFattureClientiLocalServiceImpl extends
        TestataFattureClientiLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.TestataFattureClientiLocalServiceUtil} to access
     * the testata fatture clienti local service.
     */

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil
            .getLog(TestataFattureClientiLocalServiceImpl.class);

    @Override
    public final TestataFattureClienti getFattureByAnnoAttivitaDocumento(
            final int anno, final String codiceAttivita,
            final String codiceCentro, final int numeroDocumento,
            final Date dataDocumento, final boolean tipoSoggetto,
            final String codiceSoggetto) {
        try {
            this.logger.debug("Look for invoice: " + anno + " "
                    + codiceAttivita + " " + codiceCentro + " "
                    + numeroDocumento);
            return this.testataFattureClientiPersistence
                    .findByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(anno,
                            codiceAttivita, codiceCentro, numeroDocumento,
                            dataDocumento, tipoSoggetto, codiceSoggetto);
        } catch (SystemException | NoSuchTestataFattureClientiException e) {
            this.logger.error(e.getMessage());
            return null;
        }
    }
}
