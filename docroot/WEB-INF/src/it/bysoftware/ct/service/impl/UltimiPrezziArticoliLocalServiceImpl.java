/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Junction;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;

import it.bysoftware.ct.model.UltimiPrezziArticoli;
import it.bysoftware.ct.service.UltimiPrezziArticoliLocalServiceUtil;
import it.bysoftware.ct.service.base.UltimiPrezziArticoliLocalServiceBaseImpl;

/**
 * The implementation of the ultimi prezzi articoli local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods 
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.UltimiPrezziArticoliLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.UltimiPrezziArticoliLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.UltimiPrezziArticoliLocalServiceUtil
 */
public class UltimiPrezziArticoliLocalServiceImpl extends
	UltimiPrezziArticoliLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.UltimiPrezziArticoliLocalServiceUtil} to access
     * the ultimi prezzi articoli local service.
     */

    /**
     * Max number of record returned.
     */
    private static final int LIMIT = 3;

    /**
     * SQL wildcard character.
     */
    private static final String SQL_WILDCARD_CHAR = "%";

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.
	    getLog(UltimiPrezziArticoliLocalServiceImpl.class);

    @Override
    public final List<UltimiPrezziArticoli> getUltimeCondizioniACliente(
	    final String codiceSoggetto, final String codiceArticolo,
	    final String codiceVariante) {
	try {
	    if (codiceArticolo != null && !codiceArticolo.isEmpty()) {
		return this.ultimiPrezziArticoliPersistence.
			findByUltimeCondizioniArticolo(false, codiceSoggetto,
				codiceArticolo, codiceVariante, 0, LIMIT);
	    } else {
		return this.ultimiPrezziArticoliPersistence.
			findByUltimeCondizioni(false, codiceSoggetto);
	    }
	} catch (SystemException e) {
	    this.logger.error(e.getMessage());
	    return new ArrayList<UltimiPrezziArticoli>();
	}
    }

    @Override
    public final List<UltimiPrezziArticoli> getUltimeCondizioniDaFornitore(
	    final String codiceArticolo, final String codiceVariante) {
	try {
	    return this.ultimiPrezziArticoliPersistence.
		    findByUltimeCondizioniFornitori(true, codiceArticolo,
			    codiceVariante, 0, LIMIT);
	} catch (SystemException e) {
	    this.logger.error(e.getMessage());
	    return new ArrayList<UltimiPrezziArticoli>();
	}
    }

    @SuppressWarnings("unchecked")
    @Override
    public final List<UltimiPrezziArticoli> searchUltimeCondizioniACliente(
	    final String codiceSoggetto, final String codiceArticolo,
	    final String codiceVariante, final boolean andSearch,
	    final int start, final int end,
	    final OrderByComparator orderByComparator) {

	DynamicQuery dynamicQuery = this.buildUltimiPrezziArticoliDynamicQuery(
		false, codiceSoggetto, codiceArticolo, codiceVariante,
		andSearch);
	try {
	    return UltimiPrezziArticoliLocalServiceUtil.dynamicQuery(
		    dynamicQuery, start, end, orderByComparator);
	} catch (SystemException e) {
	    this.logger.error(e.getMessage());
	    return new ArrayList<UltimiPrezziArticoli>();
	}
    }

    /**
     * Returns the {@link DynamicQuery} using specified criteria.
     * 
     * @param tipoSoggetto
     *            customer type (<i>false</i> if 'customer',
     *            <i>false</i> if 'supplier'
     * @param codiceSoggetto
     *            the customer code
     * @param codiceArticolo
     *            item code
     * @param codiceVariante
     *            variant code
     * @param andSearch
     *            AND or OR filter
     * @return the {@link DynamicQuery} created.
     */
    private DynamicQuery buildUltimiPrezziArticoliDynamicQuery(
	    final boolean tipoSoggetto, final String codiceSoggetto,
	    final String codiceArticolo, final String codiceVariante,
	    final boolean andSearch) {
	Junction junction = null;
	if (andSearch) {
	    junction = RestrictionsFactoryUtil.conjunction();
	} else {
	    junction = RestrictionsFactoryUtil.disjunction();
	}

	Property p = PropertyFactoryUtil.forName("primaryKey.tipoSoggetto");
	junction.add(p.eq(tipoSoggetto));

	if (Validator.isNotNull(codiceSoggetto)) {
	    Property property = PropertyFactoryUtil.
		    forName("primaryKey.codiceSoggetto");
	    junction.add(property.eq(codiceSoggetto));
	}
	if (Validator.isNotNull(codiceArticolo)) {
	    Property property = PropertyFactoryUtil.
		    forName("primaryKey.codiceArticolo");
	    String value = (new StringBuilder(SQL_WILDCARD_CHAR))
		    .append(codiceArticolo).append(SQL_WILDCARD_CHAR)
		    .toString();
	    junction.add(property.like(value));
	}
	if (Validator.isNotNull(codiceVariante)) {
	    Property property = PropertyFactoryUtil.
		    forName("primaryKey.codiceVariante");
	    String value = (new StringBuilder(SQL_WILDCARD_CHAR)).
		    append(codiceVariante).append(SQL_WILDCARD_CHAR).
		    toString();
	    junction.add(property.like(value));
	}

	DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
		UltimiPrezziArticoli.class, getClassLoader());
	return dynamicQuery.add(junction);
    }
}