/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Junction;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;

import it.bysoftware.ct.model.WKOrdiniClienti;
import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.service.base.WKOrdiniClientiLocalServiceBaseImpl;

/**
 * The implementation of the w k ordini clienti local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.WKOrdiniClientiLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.WKOrdiniClientiLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.WKOrdiniClientiLocalServiceUtil
 */
public class WKOrdiniClientiLocalServiceImpl extends
	WKOrdiniClientiLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.WKOrdiniClientiLocalServiceUtil} to access the w
     * k ordini clienti local service.
     */

    /**
     * SQL wildcard character.
     */
    private static final String SQL_WILDCARD_CHAR = "%";

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.
	    getLog(WKOrdiniClientiLocalServiceImpl.class);

    @Override
    public final int getNumeroOrdine(final int anno, final int tipoOrdine) {
	try {
	    List<WKOrdiniClienti> ordini = this.wkOrdiniClientiPersistence.
		    findByAnnoTipoOrdine(anno, tipoOrdine);
	    if (ordini.size() > 0) {
		return ordini.get(0).getNumeroOrdine();
	    } else {
		return 0;
	    }
	} catch (SystemException e) {
	    this.logger.error(e.getMessage());
	    return -1;
	}
    }

    @Override
    public final List<WKOrdiniClienti> getOrdiniByCodiceCliente(
	    final String codiceCliente) {
	try {
	    return this.wkOrdiniClientiPersistence.
		    findByCodiceCliente(codiceCliente);
	} catch (SystemException e) {
	    this.logger.error(e.getMessage());
	    return new ArrayList<WKOrdiniClienti>();
	}
    }

    @Override
    public final int getOrdiniClientiAcquisitiCount() {
	return getOrdiniByStatoOrdine(true).size();
    }

    @Override
    public final int getOrdiniClientiDaAcquisireCount() {
	return getOrdiniByStatoOrdine(false).size();
    }

    @Override
    public final List<WKOrdiniClienti> getOrdiniByStatoOrdine(
	    final boolean stato) {
	try {
	    this.logger.debug("Look for: all documents...");
	    return this.wkOrdiniClientiPersistence.findByStatoOrdine(stato);
	} catch (SystemException e) {
	    this.logger.error(e.getMessage());
	    return new ArrayList<WKOrdiniClienti>();
	}
    }

    @SuppressWarnings("unchecked")
    @Override
    public final List<WKOrdiniClienti> searchOrdini(
	    final Boolean statoOrdine, final String codiceCliente,
	    final Date from, final Date to, final int start, final int end,
	    final OrderByComparator orderByComparator) {

	DynamicQuery dynamicQuery = this.buildOrdiniDynamicQuery(statoOrdine,
		codiceCliente, from, to);
	dynamicQuery.addOrder(OrderFactoryUtil.desc("primaryKey.anno"));
	dynamicQuery.addOrder(OrderFactoryUtil.asc("statoOrdine"));
	dynamicQuery.addOrder(OrderFactoryUtil.desc("primaryKey.numeroOrdine"));
	dynamicQuery.addOrder(OrderFactoryUtil.desc("dataDocumento"));
	try {
	    return ArticoliLocalServiceUtil.dynamicQuery(dynamicQuery, start,
		    end, orderByComparator);
	} catch (SystemException e) {
	    this.logger.error(e.getMessage());
	    return new ArrayList<WKOrdiniClienti>();
	}
    }

    /**
     * Returns the {@link DynamicQuery} using specified criteria.
     * 
     * @param statoOrdine
     *            order state, 'true' for acquired document
     * @param codiceCliente
     *            the customer code
     * @param from
     *            start date
     * @param to
     *            start date
     * @return the {@link DynamicQuery} created.
     */
    private DynamicQuery buildOrdiniDynamicQuery(final Boolean statoOrdine,
	    final String codiceCliente, final Date from, final Date to) {
	Junction junction = RestrictionsFactoryUtil.conjunction();

	if (Validator.isNotNull(statoOrdine)) {
	    Property p = PropertyFactoryUtil.forName("statoOrdine");
	    this.logger.debug("Look for: " + statoOrdine + " documents...");
	    junction.add(p.eq(statoOrdine));
	}

	if (Validator.isNotNull(codiceCliente)) {
	    this.logger.debug("Look for: " + codiceCliente
		    + " like documents...");
	    Property property = PropertyFactoryUtil.forName("codiceCliente");
	    String value = (new StringBuilder(SQL_WILDCARD_CHAR)).
		    append(codiceCliente).append(SQL_WILDCARD_CHAR).toString();
	    junction.add(property.like(value));
	}

	Criterion c = null;
	Property property = PropertyFactoryUtil.forName("dataDocumento");

	if (Validator.isNotNull(from) && Validator.isNotNull(to)) {
	    this.logger.debug("Look for documents between: " + from + " and: "
		    + to);
	    c = property.between(from, to);
	} else if (Validator.isNotNull(from)) {
	    this.logger.debug("Look for documents greater than: " + from);
	    c = property.gt(from);
	} else if (Validator.isNotNull(to)) {
	    this.logger.debug("Look for documents less than: " + to);
	    c = property.lt(to);
	}
	if (c != null) {
	    junction.add(c);
	}

	DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
		WKOrdiniClienti.class, getClassLoader());
	return dynamicQuery.add(junction);
    }
}
