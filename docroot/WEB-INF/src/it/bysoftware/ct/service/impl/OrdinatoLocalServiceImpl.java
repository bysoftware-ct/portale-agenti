/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.bysoftware.ct.NoSuchOrdinatoException;
import it.bysoftware.ct.model.Ordinato;
import it.bysoftware.ct.service.base.OrdinatoLocalServiceBaseImpl;

/**
 * The implementation of the ordinato local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods 
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.OrdinatoLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.OrdinatoLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.OrdinatoLocalServiceUtil
 */
public class OrdinatoLocalServiceImpl extends OrdinatoLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.OrdinatoLocalServiceUtil} to access the ordinato
     * local service.
     */

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(OrdinatoLocalServiceImpl.class);

    @Override
    public final Ordinato getQuantitaOridnata(final String codiceArticolo,
	    final String codiceVariante) {
	try {
	    return this.ordinatoPersistence.findByArticoloVariante(
		    codiceArticolo, codiceVariante);
	} catch (NoSuchOrdinatoException | SystemException e) {
	    e.printStackTrace();
	    this.logger.error(e.getMessage());
	    return null;
	}
    }
}