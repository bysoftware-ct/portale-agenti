/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.model.Articoli;
import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.service.base.ArticoliLocalServiceBaseImpl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Junction;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

/**
 * The implementation of the articoli local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods 
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.ArticoliLocalService} interface.
 *
 * <p> This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.ArticoliLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.ArticoliLocalServiceUtil
 */
public class ArticoliLocalServiceImpl extends ArticoliLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.ArticoliLocalServiceUtil} to access the articoli
     * local service.
     */
    
    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.getLog(ArticoliLocalServiceImpl.class);

    @Override
    public final List<Articoli> getItemWithItemSheet() {
        try {
            return this.articoliPersistence
                    .findByHaSchedaArticolo("");
        } catch (SystemException e) {
            this.logger.error(e.getMessage());
            return new ArrayList<Articoli>();
        }
    }
    
    @Override
    public final List<Articoli> getArticoliByCategoriaMerceologica(
	    final String categoriaMerceologica) {
	try {
	    return this.articoliPersistence.
		    findByCategoriaMerceologica(categoriaMerceologica);
	} catch (SystemException e) {
	    this.logger.error(e.getMessage());
	    return new ArrayList<Articoli>();
	}
    }

    @SuppressWarnings("unchecked")
    @Override
    public final List<Articoli> searchArticoliByCodice(
	    final String codiceArticolo, final String descrizione,
	    final String categoriaMerceologica, final boolean noObsolete,
	    final boolean andSearch, final int start, final int end,
	    final OrderByComparator orderByComparator) {

	DynamicQuery dynamicQuery = this.buildClientiDynamicQuery(
		codiceArticolo, descrizione, categoriaMerceologica, noObsolete,
		andSearch);
	try {
	    return ArticoliLocalServiceUtil.dynamicQuery(dynamicQuery, start,
		    end, orderByComparator);
	} catch (SystemException e) {
	    this.logger.error(e.getMessage());
	    return new ArrayList<Articoli>();
	}
    }


    /**
     * Returns {@link DynamicQuery} passing the required filters.
     * 
     * @param codiceArticolo
     *            item code
     * @param descrizione
     *            item description
     * @param categoriaMerceologica
     *            item category
     * @param noObsolete 
     *            true to exclude obsolete items, falsse otherwise
     * @param andSearch
     *            if filter is AND or OR
     * 
     * @return the {@link DynamicQuery} result
     */
    private DynamicQuery buildClientiDynamicQuery(
	    final String codiceArticolo, final String descrizione,
	    final String categoriaMerceologica, final boolean noObsolete,
	    final boolean andSearch) {

	Junction junction = null;
        if (andSearch) {
            junction = RestrictionsFactoryUtil.conjunction();
        } else {
            junction = RestrictionsFactoryUtil.disjunction();
        }

	if (Validator.isNotNull(codiceArticolo)) {
	    Property property = PropertyFactoryUtil.forName("codiceArticolo");
	    String value = (new StringBuilder(StringPool.PERCENT))
		    .append(codiceArticolo).append(StringPool.PERCENT)
		    .toString();
	    junction.add(property.like(value));
	}
	if (Validator.isNotNull(descrizione)) {
	    Property property = PropertyFactoryUtil.forName("descrizione");
	    String value = (new StringBuilder(StringPool.PERCENT))
		    .append(descrizione).append(StringPool.PERCENT).toString();
	    junction.add(property.like(value));
	}
	if (Validator.isNotNull(categoriaMerceologica)
		&& !"all".equals(categoriaMerceologica)) {
	    Property property = PropertyFactoryUtil
    		.forName("categoriaMerceologica");
	    String value = (new StringBuilder()).append(
		    categoriaMerceologica).append(StringPool.PERCENT).toString();
	    junction.add(property.like(value));
	}
	
	if (noObsolete) {
            Property property = PropertyFactoryUtil.forName("obsoleto");
            junction.add(property.ne(noObsolete));
        }
	
	DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
		Articoli.class, getClassLoader());
	return dynamicQuery.add(junction);

    }

}