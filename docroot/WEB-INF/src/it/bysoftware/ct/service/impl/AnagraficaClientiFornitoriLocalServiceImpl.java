/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.util.StringUtil;

import it.bysoftware.ct.model.AnagraficaClientiFornitori;
import it.bysoftware.ct.service.AnagraficaClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.base.AnagraficaClientiFornitoriLocalServiceBaseImpl;

/**
 * The implementation of the anagrafica clienti fornitori local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods 
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.AnagraficaClientiFornitoriLocalService}
 * interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.
 *      AnagraficaClientiFornitoriLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.AnagraficaClientiFornitoriLocalServiceUtil
 */
public class AnagraficaClientiFornitoriLocalServiceImpl extends
	AnagraficaClientiFornitoriLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.AnagraficaClientiFornitoriLocalServiceUtil} to
     * access the anagrafica clienti fornitori local service.
     */

    /**
     * Returns all agent's clients.
     * 
     * @param codiceAgente
     *            agent code
     * @param comune
     *            city filter
     * @param ragSociale
     *            client name filter
     * @param codiceSoggetto client code
     * @return {@link List} of aget's clients
     */
    public final List<AnagraficaClientiFornitori> getClientiByCodiceAgente(
	    final String codiceAgente, final String comune,
	    final String ragSociale, final String codiceSoggetto) {
	Session session = this.anagraficaAgentiPersistence.openSession();
	SQLQuery query = session
		.createSQLQuery("SELECT ANCLFO.RacCodana, ANCLFO.RacRagso1,"
			+ " ANCLFO.RacComune, CODVEA.RveAgente, CODVEA.RveTipo"
			+ " FROM ANCLFO INNER JOIN CODVEA ON ANCLFO.RacCodana ="
			+ " CODVEA.RveCodclf WHERE (CODVEA.RveTipo = 0) AND"
			+ " CODVEA.RveAgente = ?");
	QueryPos pos = QueryPos.getInstance(query);
	pos.add(codiceAgente);
	List<AnagraficaClientiFornitori> clienti =
		new ArrayList<AnagraficaClientiFornitori>();
	@SuppressWarnings("unchecked")
	List<Object[]> list = query.list();
	for (Object[] objects : list) {
	    AnagraficaClientiFornitori cliente =
		    AnagraficaClientiFornitoriLocalServiceUtil
		    .createAnagraficaClientiFornitori((String) objects[0]);
	    cliente.setRagioneSociale((String) objects[1]);
	    cliente.setComune((String) objects[2]);
	    boolean add1 = true;
	    boolean add2 = true;
	    boolean add3 = true;
	    if (comune != null && !comune.isEmpty()) {
		add1 = StringUtil.matchesIgnoreCase(cliente.getComune(),
		        comune);
	    }
	    if (ragSociale != null && !ragSociale.isEmpty()) {
		add2 = StringUtil.matchesIgnoreCase(cliente.getRagioneSociale(),
			ragSociale);
	    }
	    if (codiceSoggetto != null && !codiceSoggetto.isEmpty()) {
                add3 = StringUtil.matchesIgnoreCase(cliente.getCodiceSoggetto(),
                        codiceSoggetto);
            }
	    if (add1 && add2 && add3) {
		clienti.add(cliente);
	    }
	}
	return clienti;
    }
}
