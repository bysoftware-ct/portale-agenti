/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.model.EstrattoConto;
import it.bysoftware.ct.service.EstrattoContoLocalServiceUtil;
import it.bysoftware.ct.service.base.EstrattoContoLocalServiceBaseImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Junction;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;

/**
 * The implementation of the estratto conto local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods 
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.EstrattoContoLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.EstrattoContoLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.EstrattoContoLocalServiceUtil
 */
public class EstrattoContoLocalServiceImpl extends
        EstrattoContoLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.EstrattoContoLocalServiceUtil} to access the
     * estratto conto local service.
     */

    /**
     * Opened entry constant.
     */
    private static final int OPENED = 1;
    
    /**
     * Exposed entry constant.
     */
    private static final int EXPOSED = 2;
    
    
    /**
     * Expired entry constant.
     */
    private static final int EXPIRED = 3;
    
    /**
     * Unpaid entry constant.
     */
    private static final int UNPAID = 4;
    
    /**
     * In contentions entry constant.
     */
    private static final int CONTENTIONS = 5;
    
    /**
     * Represents the state of the entry.
     * 
     * @author Mario Torrisi torrisi.mario@gmail.com
     *
     */
    private enum EntryState {
        /**
         * Open entry.
         */
        APERTA(1),
        /**
         * Exposed entry.
         */
        ESPOSTA(2),
        /**
         * Expired entry.
         */
        SCADUTA(3),
        /**
         * Unsolved entry.
         */
        INSOLUTA(4),
        /**
         * Protested entry.
         */
        IN_CONTENZIOSO(5),
        /**
         * Closed entry.
         */
        CHIUSA(6),
        /**
         * Replaced entry.
         */
        SOSTITUITA(7);

        /**
         * Entry value.
         */
        private final int val;

        /**
         * Creates a entry using its state.
         * 
         * @param v
         *            real state of the entry
         */
        EntryState(final int v) {
            this.val = v;
        }

        /**
         * Returns the entry state.
         * 
         * @return the entry state
         */
        public int getVal() {
            return val;
        }
    }

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil
            .getLog(EstrattoContoLocalServiceImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public final List<EstrattoConto> getPartiteCliente(
            final String codiceSoggetto, final boolean chiuse, final Date from,
            final Date to, final int start, final int end,
            final OrderByComparator orderByComparator) {
        DynamicQuery dynamicQuery = buildDynamicQuery(codiceSoggetto, chiuse,
                from, to);

        dynamicQuery.addOrder(OrderFactoryUtil.asc("stato"));
        dynamicQuery.addOrder(OrderFactoryUtil.desc("dataScadenza"));

        try {
            return EstrattoContoLocalServiceUtil.dynamicQuery(dynamicQuery,
                    start, end, orderByComparator);
        } catch (SystemException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return new ArrayList<EstrattoConto>();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public final double getTotalePartiteAperte(final String codiceSoggetto,
            final boolean chiuse, final Date from, final Date to,
            final int start, final int end) {

        DynamicQuery dynamicQuery = buildDynamicQuery(codiceSoggetto, false,
                from, to);

        List<EstrattoConto> list = new ArrayList<EstrattoConto>();
        try {
            list = EstrattoContoLocalServiceUtil.dynamicQuery(dynamicQuery, 0,
                    end);
        } catch (SystemException e) {
             logger.error(e.getMessage());
            e.printStackTrace();
        }

        double result = 0;
        for (EstrattoConto partita : list) {
            switch (partita.getStato()) {
                case OPENED:
                    this.logger.debug("Opened state!!");
                    result += partita.getImportoAperto();
                    break;
                case EXPOSED:
                    this.logger.debug("Exposed state!!");
                    result += partita.getImportoEsposto();
                    break;
                case EXPIRED:
                    this.logger.debug("Expired state!!");
                    result += partita.getImportoScaduto();
                    break;
                case UNPAID:
                    this.logger.debug("Unpaied state!!");
                    result += partita.getImportoInsoluto();
                    break;
                case CONTENTIONS:
                    this.logger.debug("Contentious state!!");
                    result += partita.getImportoInContenzioso();
                    break;
                default:
                    this.logger.warn("Unvalid entry state!!");
                    break;
            }

        }
        return result;
    }

    /**
     * Returns the {@link DynamicQuery} using specified criteria.
     * 
     * @param codiceSoggetto
     *            the customer code
     * @param chiuse
     *            if only open entries should be shown
     * @param from
     *            start date
     * @param to
     *            end date
     * @return the {@link DynamicQuery} created.
     */
    private DynamicQuery buildDynamicQuery(final String codiceSoggetto,
            final boolean chiuse, final Date from, final Date to) {

        Junction junction = RestrictionsFactoryUtil.conjunction();

        if (!chiuse) {
            this.logger.debug("Look for open entries...");
            Property property = PropertyFactoryUtil.forName("stato");
            junction.add(property.ne(EntryState.CHIUSA.getVal()));
            junction.add(property.ne(EntryState.SOSTITUITA.getVal()));
            // To consider the entries closed in other way, Company does
            // something to consider that closed.
            Property pTotal =
                    PropertyFactoryUtil.forName("importoTotale");
            Property pTotalPaied =
                    PropertyFactoryUtil.forName("importoPagato");
            junction.add(pTotal.neProperty(pTotalPaied));
        } else {
            this.logger.debug("Look for all entries...");
        }

        if (Validator.isNotNull(codiceSoggetto)) {
            this.logger.debug("Look for: " + codiceSoggetto + "'s entries...");
            Property property = PropertyFactoryUtil
                    .forName("primaryKey.codiceCliente");
            junction.add(property.eq(codiceSoggetto));
        }

        Criterion c = null;
        Property property = PropertyFactoryUtil.forName("dataScadenza");
        if (Validator.isNotNull(from) && Validator.isNotNull(to)) {
            this.logger.debug("Look for entries between: " + from + " and: "
                    + to);
            c = property.between(from, to);
        } else if (Validator.isNotNull(from)) {
            this.logger.debug("Look for documents greater than: " + from);
            c = property.gt(from);
        } else if (Validator.isNotNull(to)) {
            this.logger.debug("Look for documents less than: " + to);
            c = property.lt(to);
        }

        if (c != null) {
            junction.add(c);
        }

        DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
                EstrattoConto.class, getClassLoader());
        return dynamicQuery.add(junction);
    }

}
