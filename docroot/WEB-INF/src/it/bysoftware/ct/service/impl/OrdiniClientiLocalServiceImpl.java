/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.impl;

import it.bysoftware.ct.model.OrdiniClienti;
import it.bysoftware.ct.service.OrdiniClientiLocalServiceUtil;
import it.bysoftware.ct.service.RigheOrdiniClientiLocalServiceUtil;
import it.bysoftware.ct.service.base.OrdiniClientiLocalServiceBaseImpl;
import it.bysoftware.ct.utils.Utils;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Junction;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;

/**
 * The implementation of the ordini clienti local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods 
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.OrdiniClientiLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.OrdiniClientiLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.OrdiniClientiLocalServiceUtil
 */
public class OrdiniClientiLocalServiceImpl extends
	OrdiniClientiLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.OrdiniClientiLocalServiceUtil} to access the
     * ordini clienti local service.
     */

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil.
	    getLog(OrdiniClientiLocalServiceImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public final List<OrdiniClienti> getOrdiniByCodiceCliente(
	    final String codiceCliente, final boolean closed, final int start,
	    final int end, final Date from, final Date to,
	    final OrderByComparator orderByComparator) {
	int yearFrom = Utils.getYear() - 1;
	int yearTo = Utils.getYear();

	DynamicQuery dynamicQuery = buildDynamicQuery(codiceCliente, null,
		yearFrom, yearTo, from, to);

	try {
	    List<OrdiniClienti> list = OrdiniClientiLocalServiceUtil
		    .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	    if (closed) {
		return list;
	    } else {
		return getOpenOrders(list);
	    }
	} catch (SystemException e) {
	    e.printStackTrace();
	    this.logger.error(e.getMessage());
	    return new ArrayList<OrdiniClienti>();
	}
    }

    @SuppressWarnings("unchecked")
    @Override
    public final List<OrdiniClienti> getOrdiniByCodiceClienteNumPortale(
	    final String codiceCliente, final boolean closed,
	    final String note, final int start, final int end, final Date from,
	    final Date to, final OrderByComparator orderByComparator) {

	int yearFrom = Utils.getYear() - 1;
	int yearTo = Utils.getYear();

	DynamicQuery dynamicQuery = buildDynamicQuery(codiceCliente, note,
		yearFrom, yearTo, from, to);
	try {
	    List<OrdiniClienti> list = OrdiniClientiLocalServiceUtil
		    .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	    if (closed) {
		this.logger.debug("Returning all orders...");
		return list;
	    } else {
		this.logger.debug("Returning only opened orders...");
		return getOpenOrders(list);
	    }
	} catch (SystemException e) {
	    e.printStackTrace();
	    this.logger.error(e.getMessage());
	    return new ArrayList<OrdiniClienti>();
	}
    }

    /**
     * Returns only the open orders.
     * 
     * @param list
     *            {@link List} of {@link OrdiniClienti}
     * @return {@link List} of open {@link OrdiniClienti}
     */
    private List<OrdiniClienti> getOpenOrders(final List<OrdiniClienti> list) {
	List<OrdiniClienti> openOrders = new ArrayList<OrdiniClienti>();
	for (OrdiniClienti ordine : list) {
	    if (RigheOrdiniClientiLocalServiceUtil.checkRigheOrdineByTestata(
		    ordine.getAnno(), ordine.getCodiceAttivita(),
		    ordine.getCodiceCentro(), ordine.getCodiceDeposito(),
		    ordine.getTipoOrdine(), ordine.getNumeroOrdine())) {
		openOrders.add(ordine);
	    }
	}
	return openOrders;
    }

    /**
     * Returns the {@link DynamicQuery} using specified criteria.
     * 
     * @param codiceCliente
     *            the customer code
     * @param note
     *            portal order id
     * @param yearFrom
     *            start date
     * @param yearTo
     *            end date
     * @param from
     *            start date
     * @param to
     *            end date
     * @return the {@link DynamicQuery} created.
     */
    private DynamicQuery buildDynamicQuery(final String codiceCliente,
	    final String note, final int yearFrom, final int yearTo,
	    final Date from, final Date to) {

	Junction junction = RestrictionsFactoryUtil.conjunction();

	if (Validator.isNotNull(codiceCliente)) {
	    this.logger.debug("Look for order from: " + codiceCliente);
	    Property property = PropertyFactoryUtil.forName("codiceCliente");
	    junction.add(property.eq(codiceCliente));
	}

	if (Validator.isNotNull(note)) {
	    this.logger.debug("Look for order: " + note);
	    Property property = PropertyFactoryUtil.forName("note");
	    junction.add(property.eq(note));
	}

	Property property = PropertyFactoryUtil.forName("primaryKey.anno");
	Criterion c = property.between(yearFrom, yearTo);

	junction.add(c);

	property = PropertyFactoryUtil.forName("dataDocumento");
	if (Validator.isNotNull(from) && Validator.isNotNull(to)) {
	    this.logger.debug("Look for orders between: " + from + " and: "
		    + to);
	    c = property.between(from, to);
	} else if (Validator.isNotNull(from)) {
	    this.logger.debug("Look for orders after than: " + from);
	    c = property.gt(from);
	} else if (Validator.isNotNull(to)) {
	    this.logger.debug("Look for orders before than: " + to);
	    c = property.lt(to);
	}
	
	junction.add(c);
	DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
		OrdiniClienti.class, getClassLoader());
	return dynamicQuery.add(junction);
    }
}