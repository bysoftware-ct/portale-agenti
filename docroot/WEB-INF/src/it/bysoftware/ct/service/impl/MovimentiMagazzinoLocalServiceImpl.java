/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Junction;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

import it.bysoftware.ct.model.MovimentiMagazzino;
import it.bysoftware.ct.service.MovimentiMagazzinoLocalServiceUtil;
import it.bysoftware.ct.service.base.MovimentiMagazzinoLocalServiceBaseImpl;
import it.bysoftware.ct.utils.Utils;

/**
 * The implementation of the movimenti magazzino local service.
 *
 * <p>All custom service methods should be put in this class. Whenever methods 
 * are added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.bysoftware.ct.service.MovimentiMagazzinoLocalService} interface.
 *
 * <p>This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.base.MovimentiMagazzinoLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.MovimentiMagazzinoLocalServiceUtil
 */
public class MovimentiMagazzinoLocalServiceImpl extends
	MovimentiMagazzinoLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     * 
     * Never reference this interface directly. Always use {@link
     * it.bysoftware.ct.service.MovimentiMagazzinoLocalServiceUtil} to access
     * the movimenti magazzino local service.
     */

    /**
     * Logger object. Based on Liferay logger
     * {@link com.liferay.portal.kernel.log.Log} commons logging.
     */
    private Log logger = LogFactoryUtil
	    .getLog(MovimentiMagazzinoLocalServiceImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public final List<MovimentiMagazzino> getMovimentiMagazzino(
	    final String codiceArticolo, final String codiceVariante,
	    final int testScarico, final Date dateFrom, final Date dateTo) {
	int yearTo = Utils.getYear();
	int yearFrom = yearTo - 1;
	DynamicQuery dynamicQuery = buildDynamicQuery(codiceArticolo,
		codiceVariante, yearFrom, yearTo, testScarico, dateFrom,
		dateTo);

	try {
	    List<MovimentiMagazzino> list = MovimentiMagazzinoLocalServiceUtil
		    .dynamicQuery(dynamicQuery); // , start, end,
						 // orderByComparator);
	    return list;
	} catch (SystemException e) {
	    e.printStackTrace();
	    this.logger.error(e.getMessage());
	    return new ArrayList<MovimentiMagazzino>();
	}
    }

    /**
     * Returns the {@link DynamicQuery} using specified criteria.
     * 
     * @param codiceArticolo
     *            item code
     * @param codiceVariante
     *            item variant code
     * @param yearFrom
     *            year from
     * @param yearTo
     *            year to
     * @param testScarico
     *            look for load or unload handle
     * @param from
     *            start date
     * @param to
     *            to date
     * @return the {@link DynamicQuery} created
     */
    private DynamicQuery buildDynamicQuery(final String codiceArticolo,
	    final String codiceVariante, final int yearFrom, final int yearTo,
	    final int testScarico, final Date from, final Date to) {
	Junction junction = RestrictionsFactoryUtil.conjunction();

	if (testScarico > 0) {
	    this.logger.debug("Look for type: " + testScarico + "handles ...");
	    Property property = PropertyFactoryUtil
		    .forName("testCaricoScarico");
	    junction.add(property.eq(testScarico));
	}

	if (Validator.isNotNull(codiceArticolo)) {
	    this.logger.debug("Look for handle of: " + codiceArticolo + "...");
	    Property property = PropertyFactoryUtil.forName("codiceArticolo");
	    junction.add(property.eq(codiceArticolo));
	}

	if (Validator.isNotNull(codiceVariante)) {
	    this.logger.debug("Look for variant code: " + codiceVariante
		    + "...");
	    Property property = PropertyFactoryUtil.forName("codiceVariante");
	    junction.add(property.eq(codiceVariante));
	}

	Property property = PropertyFactoryUtil.forName("anno");
	Criterion c = property.between(yearFrom, yearTo);

	junction.add(c);

	property = PropertyFactoryUtil.forName("dataRegistrazione");
	if (Validator.isNotNull(from) && Validator.isNotNull(to)) {
	    this.logger.debug("Look for handles between: " + from + " and: "
		    + to);
	    c = property.between(from, to);
	} else if (Validator.isNotNull(from)) {
	    this.logger.debug("Look for handles after: " + from);
	    c = property.gt(from);
	} else if (Validator.isNotNull(to)) {
	    this.logger.debug("Look for handles before: " + to);
	    c = property.lt(to);
	}

	junction.add(c);

	DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
		MovimentiMagazzino.class, getClassLoader());
	return dynamicQuery.add(junction);
    }

}
