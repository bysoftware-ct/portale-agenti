/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for OrdiniClienti. This utility wraps
 * {@link it.bysoftware.ct.service.impl.OrdiniClientiLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Mario Torrisi
 * @see OrdiniClientiLocalService
 * @see it.bysoftware.ct.service.base.OrdiniClientiLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.impl.OrdiniClientiLocalServiceImpl
 * @generated
 */
public class OrdiniClientiLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.bysoftware.ct.service.impl.OrdiniClientiLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the ordini clienti to the database. Also notifies the appropriate model listeners.
	*
	* @param ordiniClienti the ordini clienti
	* @return the ordini clienti that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.OrdiniClienti addOrdiniClienti(
		it.bysoftware.ct.model.OrdiniClienti ordiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addOrdiniClienti(ordiniClienti);
	}

	/**
	* Creates a new ordini clienti with the primary key. Does not add the ordini clienti to the database.
	*
	* @param ordiniClientiPK the primary key for the new ordini clienti
	* @return the new ordini clienti
	*/
	public static it.bysoftware.ct.model.OrdiniClienti createOrdiniClienti(
		it.bysoftware.ct.service.persistence.OrdiniClientiPK ordiniClientiPK) {
		return getService().createOrdiniClienti(ordiniClientiPK);
	}

	/**
	* Deletes the ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ordiniClientiPK the primary key of the ordini clienti
	* @return the ordini clienti that was removed
	* @throws PortalException if a ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.OrdiniClienti deleteOrdiniClienti(
		it.bysoftware.ct.service.persistence.OrdiniClientiPK ordiniClientiPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteOrdiniClienti(ordiniClientiPK);
	}

	/**
	* Deletes the ordini clienti from the database. Also notifies the appropriate model listeners.
	*
	* @param ordiniClienti the ordini clienti
	* @return the ordini clienti that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.OrdiniClienti deleteOrdiniClienti(
		it.bysoftware.ct.model.OrdiniClienti ordiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteOrdiniClienti(ordiniClienti);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.bysoftware.ct.model.OrdiniClienti fetchOrdiniClienti(
		it.bysoftware.ct.service.persistence.OrdiniClientiPK ordiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchOrdiniClienti(ordiniClientiPK);
	}

	/**
	* Returns the ordini clienti with the primary key.
	*
	* @param ordiniClientiPK the primary key of the ordini clienti
	* @return the ordini clienti
	* @throws PortalException if a ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.OrdiniClienti getOrdiniClienti(
		it.bysoftware.ct.service.persistence.OrdiniClientiPK ordiniClientiPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getOrdiniClienti(ordiniClientiPK);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the ordini clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ordini clientis
	* @param end the upper bound of the range of ordini clientis (not inclusive)
	* @return the range of ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.OrdiniClienti> getOrdiniClientis(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getOrdiniClientis(start, end);
	}

	/**
	* Returns the number of ordini clientis.
	*
	* @return the number of ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public static int getOrdiniClientisCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getOrdiniClientisCount();
	}

	/**
	* Updates the ordini clienti in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ordiniClienti the ordini clienti
	* @return the ordini clienti that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.OrdiniClienti updateOrdiniClienti(
		it.bysoftware.ct.model.OrdiniClienti ordiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateOrdiniClienti(ordiniClienti);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<it.bysoftware.ct.model.OrdiniClienti> getOrdiniByCodiceCliente(
		java.lang.String codiceCliente, boolean closed, int start, int end,
		java.sql.Date from, java.sql.Date to,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator) {
		return getService()
				   .getOrdiniByCodiceCliente(codiceCliente, closed, start, end,
			from, to, orderByComparator);
	}

	public static java.util.List<it.bysoftware.ct.model.OrdiniClienti> getOrdiniByCodiceClienteNumPortale(
		java.lang.String codiceCliente, boolean closed, java.lang.String note,
		int start, int end, java.sql.Date from, java.sql.Date to,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator) {
		return getService()
				   .getOrdiniByCodiceClienteNumPortale(codiceCliente, closed,
			note, start, end, from, to, orderByComparator);
	}

	public static void clearService() {
		_service = null;
	}

	public static OrdiniClientiLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					OrdiniClientiLocalService.class.getName());

			if (invokableLocalService instanceof OrdiniClientiLocalService) {
				_service = (OrdiniClientiLocalService)invokableLocalService;
			}
			else {
				_service = new OrdiniClientiLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(OrdiniClientiLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(OrdiniClientiLocalService service) {
	}

	private static OrdiniClientiLocalService _service;
}