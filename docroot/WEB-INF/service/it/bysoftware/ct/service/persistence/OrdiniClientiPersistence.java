/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.OrdiniClienti;

/**
 * The persistence interface for the ordini clienti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see OrdiniClientiPersistenceImpl
 * @see OrdiniClientiUtil
 * @generated
 */
public interface OrdiniClientiPersistence extends BasePersistence<OrdiniClienti> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link OrdiniClientiUtil} to access the ordini clienti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the ordini clientis where codiceCliente = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @return the matching ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.OrdiniClienti> findByCodiceCliente(
		java.lang.String codiceCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ordini clientis where codiceCliente = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceCliente the codice cliente
	* @param start the lower bound of the range of ordini clientis
	* @param end the upper bound of the range of ordini clientis (not inclusive)
	* @return the range of matching ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.OrdiniClienti> findByCodiceCliente(
		java.lang.String codiceCliente, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ordini clientis where codiceCliente = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceCliente the codice cliente
	* @param start the lower bound of the range of ordini clientis
	* @param end the upper bound of the range of ordini clientis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.OrdiniClienti> findByCodiceCliente(
		java.lang.String codiceCliente, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ordini clienti in the ordered set where codiceCliente = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ordini clienti
	* @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a matching ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.OrdiniClienti findByCodiceCliente_First(
		java.lang.String codiceCliente,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchOrdiniClientiException;

	/**
	* Returns the first ordini clienti in the ordered set where codiceCliente = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ordini clienti, or <code>null</code> if a matching ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.OrdiniClienti fetchByCodiceCliente_First(
		java.lang.String codiceCliente,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ordini clienti in the ordered set where codiceCliente = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ordini clienti
	* @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a matching ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.OrdiniClienti findByCodiceCliente_Last(
		java.lang.String codiceCliente,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchOrdiniClientiException;

	/**
	* Returns the last ordini clienti in the ordered set where codiceCliente = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ordini clienti, or <code>null</code> if a matching ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.OrdiniClienti fetchByCodiceCliente_Last(
		java.lang.String codiceCliente,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ordini clientis before and after the current ordini clienti in the ordered set where codiceCliente = &#63;.
	*
	* @param ordiniClientiPK the primary key of the current ordini clienti
	* @param codiceCliente the codice cliente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ordini clienti
	* @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.OrdiniClienti[] findByCodiceCliente_PrevAndNext(
		it.bysoftware.ct.service.persistence.OrdiniClientiPK ordiniClientiPK,
		java.lang.String codiceCliente,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchOrdiniClientiException;

	/**
	* Removes all the ordini clientis where codiceCliente = &#63; from the database.
	*
	* @param codiceCliente the codice cliente
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCodiceCliente(java.lang.String codiceCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ordini clientis where codiceCliente = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @return the number of matching ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public int countByCodiceCliente(java.lang.String codiceCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ordini clientis where codiceCliente = &#63; and note = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param note the note
	* @return the matching ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.OrdiniClienti> findByCodiceClienteAgenteNumPortale(
		java.lang.String codiceCliente, java.lang.String note)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ordini clientis where codiceCliente = &#63; and note = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceCliente the codice cliente
	* @param note the note
	* @param start the lower bound of the range of ordini clientis
	* @param end the upper bound of the range of ordini clientis (not inclusive)
	* @return the range of matching ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.OrdiniClienti> findByCodiceClienteAgenteNumPortale(
		java.lang.String codiceCliente, java.lang.String note, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ordini clientis where codiceCliente = &#63; and note = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceCliente the codice cliente
	* @param note the note
	* @param start the lower bound of the range of ordini clientis
	* @param end the upper bound of the range of ordini clientis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.OrdiniClienti> findByCodiceClienteAgenteNumPortale(
		java.lang.String codiceCliente, java.lang.String note, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ordini clienti in the ordered set where codiceCliente = &#63; and note = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param note the note
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ordini clienti
	* @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a matching ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.OrdiniClienti findByCodiceClienteAgenteNumPortale_First(
		java.lang.String codiceCliente, java.lang.String note,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchOrdiniClientiException;

	/**
	* Returns the first ordini clienti in the ordered set where codiceCliente = &#63; and note = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param note the note
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ordini clienti, or <code>null</code> if a matching ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.OrdiniClienti fetchByCodiceClienteAgenteNumPortale_First(
		java.lang.String codiceCliente, java.lang.String note,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ordini clienti in the ordered set where codiceCliente = &#63; and note = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param note the note
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ordini clienti
	* @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a matching ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.OrdiniClienti findByCodiceClienteAgenteNumPortale_Last(
		java.lang.String codiceCliente, java.lang.String note,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchOrdiniClientiException;

	/**
	* Returns the last ordini clienti in the ordered set where codiceCliente = &#63; and note = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param note the note
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ordini clienti, or <code>null</code> if a matching ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.OrdiniClienti fetchByCodiceClienteAgenteNumPortale_Last(
		java.lang.String codiceCliente, java.lang.String note,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ordini clientis before and after the current ordini clienti in the ordered set where codiceCliente = &#63; and note = &#63;.
	*
	* @param ordiniClientiPK the primary key of the current ordini clienti
	* @param codiceCliente the codice cliente
	* @param note the note
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ordini clienti
	* @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.OrdiniClienti[] findByCodiceClienteAgenteNumPortale_PrevAndNext(
		it.bysoftware.ct.service.persistence.OrdiniClientiPK ordiniClientiPK,
		java.lang.String codiceCliente, java.lang.String note,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchOrdiniClientiException;

	/**
	* Removes all the ordini clientis where codiceCliente = &#63; and note = &#63; from the database.
	*
	* @param codiceCliente the codice cliente
	* @param note the note
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCodiceClienteAgenteNumPortale(
		java.lang.String codiceCliente, java.lang.String note)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ordini clientis where codiceCliente = &#63; and note = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param note the note
	* @return the number of matching ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public int countByCodiceClienteAgenteNumPortale(
		java.lang.String codiceCliente, java.lang.String note)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the ordini clienti in the entity cache if it is enabled.
	*
	* @param ordiniClienti the ordini clienti
	*/
	public void cacheResult(it.bysoftware.ct.model.OrdiniClienti ordiniClienti);

	/**
	* Caches the ordini clientis in the entity cache if it is enabled.
	*
	* @param ordiniClientis the ordini clientis
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.OrdiniClienti> ordiniClientis);

	/**
	* Creates a new ordini clienti with the primary key. Does not add the ordini clienti to the database.
	*
	* @param ordiniClientiPK the primary key for the new ordini clienti
	* @return the new ordini clienti
	*/
	public it.bysoftware.ct.model.OrdiniClienti create(
		it.bysoftware.ct.service.persistence.OrdiniClientiPK ordiniClientiPK);

	/**
	* Removes the ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ordiniClientiPK the primary key of the ordini clienti
	* @return the ordini clienti that was removed
	* @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.OrdiniClienti remove(
		it.bysoftware.ct.service.persistence.OrdiniClientiPK ordiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchOrdiniClientiException;

	public it.bysoftware.ct.model.OrdiniClienti updateImpl(
		it.bysoftware.ct.model.OrdiniClienti ordiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ordini clienti with the primary key or throws a {@link it.bysoftware.ct.NoSuchOrdiniClientiException} if it could not be found.
	*
	* @param ordiniClientiPK the primary key of the ordini clienti
	* @return the ordini clienti
	* @throws it.bysoftware.ct.NoSuchOrdiniClientiException if a ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.OrdiniClienti findByPrimaryKey(
		it.bysoftware.ct.service.persistence.OrdiniClientiPK ordiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchOrdiniClientiException;

	/**
	* Returns the ordini clienti with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ordiniClientiPK the primary key of the ordini clienti
	* @return the ordini clienti, or <code>null</code> if a ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.OrdiniClienti fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.OrdiniClientiPK ordiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ordini clientis.
	*
	* @return the ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.OrdiniClienti> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ordini clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ordini clientis
	* @param end the upper bound of the range of ordini clientis (not inclusive)
	* @return the range of ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.OrdiniClienti> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ordini clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ordini clientis
	* @param end the upper bound of the range of ordini clientis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.OrdiniClienti> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the ordini clientis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ordini clientis.
	*
	* @return the number of ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}