/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.RigheFattureVedita;

/**
 * The persistence interface for the righe fatture vedita service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see RigheFattureVeditaPersistenceImpl
 * @see RigheFattureVeditaUtil
 * @generated
 */
public interface RigheFattureVeditaPersistence extends BasePersistence<RigheFattureVedita> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link RigheFattureVeditaUtil} to access the righe fatture vedita persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the righe fatture veditas where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroProtocollo the numero protocollo
	* @return the matching righe fatture veditas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.RigheFattureVedita> findByTestataFattura(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroProtocollo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the righe fatture veditas where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheFattureVeditaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroProtocollo the numero protocollo
	* @param start the lower bound of the range of righe fatture veditas
	* @param end the upper bound of the range of righe fatture veditas (not inclusive)
	* @return the range of matching righe fatture veditas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.RigheFattureVedita> findByTestataFattura(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroProtocollo, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the righe fatture veditas where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheFattureVeditaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroProtocollo the numero protocollo
	* @param start the lower bound of the range of righe fatture veditas
	* @param end the upper bound of the range of righe fatture veditas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching righe fatture veditas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.RigheFattureVedita> findByTestataFattura(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroProtocollo, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first righe fatture vedita in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroProtocollo the numero protocollo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching righe fatture vedita
	* @throws it.bysoftware.ct.NoSuchRigheFattureVeditaException if a matching righe fatture vedita could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheFattureVedita findByTestataFattura_First(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroProtocollo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchRigheFattureVeditaException;

	/**
	* Returns the first righe fatture vedita in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroProtocollo the numero protocollo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching righe fatture vedita, or <code>null</code> if a matching righe fatture vedita could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheFattureVedita fetchByTestataFattura_First(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroProtocollo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last righe fatture vedita in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroProtocollo the numero protocollo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching righe fatture vedita
	* @throws it.bysoftware.ct.NoSuchRigheFattureVeditaException if a matching righe fatture vedita could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheFattureVedita findByTestataFattura_Last(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroProtocollo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchRigheFattureVeditaException;

	/**
	* Returns the last righe fatture vedita in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroProtocollo the numero protocollo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching righe fatture vedita, or <code>null</code> if a matching righe fatture vedita could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheFattureVedita fetchByTestataFattura_Last(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroProtocollo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the righe fatture veditas before and after the current righe fatture vedita in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	*
	* @param righeFattureVeditaPK the primary key of the current righe fatture vedita
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroProtocollo the numero protocollo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next righe fatture vedita
	* @throws it.bysoftware.ct.NoSuchRigheFattureVeditaException if a righe fatture vedita with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheFattureVedita[] findByTestataFattura_PrevAndNext(
		it.bysoftware.ct.service.persistence.RigheFattureVeditaPK righeFattureVeditaPK,
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroProtocollo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchRigheFattureVeditaException;

	/**
	* Removes all the righe fatture veditas where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63; from the database.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroProtocollo the numero protocollo
	* @throws SystemException if a system exception occurred
	*/
	public void removeByTestataFattura(int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		int numeroProtocollo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of righe fatture veditas where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroProtocollo = &#63;.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroProtocollo the numero protocollo
	* @return the number of matching righe fatture veditas
	* @throws SystemException if a system exception occurred
	*/
	public int countByTestataFattura(int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroProtocollo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the righe fatture vedita in the entity cache if it is enabled.
	*
	* @param righeFattureVedita the righe fatture vedita
	*/
	public void cacheResult(
		it.bysoftware.ct.model.RigheFattureVedita righeFattureVedita);

	/**
	* Caches the righe fatture veditas in the entity cache if it is enabled.
	*
	* @param righeFattureVeditas the righe fatture veditas
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.RigheFattureVedita> righeFattureVeditas);

	/**
	* Creates a new righe fatture vedita with the primary key. Does not add the righe fatture vedita to the database.
	*
	* @param righeFattureVeditaPK the primary key for the new righe fatture vedita
	* @return the new righe fatture vedita
	*/
	public it.bysoftware.ct.model.RigheFattureVedita create(
		it.bysoftware.ct.service.persistence.RigheFattureVeditaPK righeFattureVeditaPK);

	/**
	* Removes the righe fatture vedita with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param righeFattureVeditaPK the primary key of the righe fatture vedita
	* @return the righe fatture vedita that was removed
	* @throws it.bysoftware.ct.NoSuchRigheFattureVeditaException if a righe fatture vedita with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheFattureVedita remove(
		it.bysoftware.ct.service.persistence.RigheFattureVeditaPK righeFattureVeditaPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchRigheFattureVeditaException;

	public it.bysoftware.ct.model.RigheFattureVedita updateImpl(
		it.bysoftware.ct.model.RigheFattureVedita righeFattureVedita)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the righe fatture vedita with the primary key or throws a {@link it.bysoftware.ct.NoSuchRigheFattureVeditaException} if it could not be found.
	*
	* @param righeFattureVeditaPK the primary key of the righe fatture vedita
	* @return the righe fatture vedita
	* @throws it.bysoftware.ct.NoSuchRigheFattureVeditaException if a righe fatture vedita with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheFattureVedita findByPrimaryKey(
		it.bysoftware.ct.service.persistence.RigheFattureVeditaPK righeFattureVeditaPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchRigheFattureVeditaException;

	/**
	* Returns the righe fatture vedita with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param righeFattureVeditaPK the primary key of the righe fatture vedita
	* @return the righe fatture vedita, or <code>null</code> if a righe fatture vedita with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheFattureVedita fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.RigheFattureVeditaPK righeFattureVeditaPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the righe fatture veditas.
	*
	* @return the righe fatture veditas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.RigheFattureVedita> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the righe fatture veditas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheFattureVeditaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of righe fatture veditas
	* @param end the upper bound of the range of righe fatture veditas (not inclusive)
	* @return the range of righe fatture veditas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.RigheFattureVedita> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the righe fatture veditas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheFattureVeditaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of righe fatture veditas
	* @param end the upper bound of the range of righe fatture veditas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of righe fatture veditas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.RigheFattureVedita> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the righe fatture veditas from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of righe fatture veditas.
	*
	* @return the number of righe fatture veditas
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}