/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class WKRigheOrdiniClientiPK implements Comparable<WKRigheOrdiniClientiPK>,
	Serializable {
	public int anno;
	public int tipoOrdine;
	public int numeroOrdine;
	public int numeroRigo;

	public WKRigheOrdiniClientiPK() {
	}

	public WKRigheOrdiniClientiPK(int anno, int tipoOrdine, int numeroOrdine,
		int numeroRigo) {
		this.anno = anno;
		this.tipoOrdine = tipoOrdine;
		this.numeroOrdine = numeroOrdine;
		this.numeroRigo = numeroRigo;
	}

	public int getAnno() {
		return anno;
	}

	public void setAnno(int anno) {
		this.anno = anno;
	}

	public int getTipoOrdine() {
		return tipoOrdine;
	}

	public void setTipoOrdine(int tipoOrdine) {
		this.tipoOrdine = tipoOrdine;
	}

	public int getNumeroOrdine() {
		return numeroOrdine;
	}

	public void setNumeroOrdine(int numeroOrdine) {
		this.numeroOrdine = numeroOrdine;
	}

	public int getNumeroRigo() {
		return numeroRigo;
	}

	public void setNumeroRigo(int numeroRigo) {
		this.numeroRigo = numeroRigo;
	}

	@Override
	public int compareTo(WKRigheOrdiniClientiPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (anno < pk.anno) {
			value = -1;
		}
		else if (anno > pk.anno) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (tipoOrdine < pk.tipoOrdine) {
			value = -1;
		}
		else if (tipoOrdine > pk.tipoOrdine) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (numeroOrdine < pk.numeroOrdine) {
			value = -1;
		}
		else if (numeroOrdine > pk.numeroOrdine) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (numeroRigo < pk.numeroRigo) {
			value = -1;
		}
		else if (numeroRigo > pk.numeroRigo) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof WKRigheOrdiniClientiPK)) {
			return false;
		}

		WKRigheOrdiniClientiPK pk = (WKRigheOrdiniClientiPK)obj;

		if ((anno == pk.anno) && (tipoOrdine == pk.tipoOrdine) &&
				(numeroOrdine == pk.numeroOrdine) &&
				(numeroRigo == pk.numeroRigo)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(anno) + String.valueOf(tipoOrdine) +
		String.valueOf(numeroOrdine) + String.valueOf(numeroRigo)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(20);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("anno");
		sb.append(StringPool.EQUAL);
		sb.append(anno);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("tipoOrdine");
		sb.append(StringPool.EQUAL);
		sb.append(tipoOrdine);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("numeroOrdine");
		sb.append(StringPool.EQUAL);
		sb.append(numeroOrdine);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("numeroRigo");
		sb.append(StringPool.EQUAL);
		sb.append(numeroRigo);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}