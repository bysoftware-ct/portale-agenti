/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.AnagraficaClientiFornitori;

/**
 * The persistence interface for the anagrafica clienti fornitori service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see AnagraficaClientiFornitoriPersistenceImpl
 * @see AnagraficaClientiFornitoriUtil
 * @generated
 */
public interface AnagraficaClientiFornitoriPersistence extends BasePersistence<AnagraficaClientiFornitori> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AnagraficaClientiFornitoriUtil} to access the anagrafica clienti fornitori persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the anagrafica clienti fornitoris where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	*
	* @param comune the comune
	* @param ragioneSociale the ragione sociale
	* @return the matching anagrafica clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.AnagraficaClientiFornitori> findByRagioneSociale(
		java.lang.String comune, java.lang.String ragioneSociale)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the anagrafica clienti fornitoris where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficaClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param comune the comune
	* @param ragioneSociale the ragione sociale
	* @param start the lower bound of the range of anagrafica clienti fornitoris
	* @param end the upper bound of the range of anagrafica clienti fornitoris (not inclusive)
	* @return the range of matching anagrafica clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.AnagraficaClientiFornitori> findByRagioneSociale(
		java.lang.String comune, java.lang.String ragioneSociale, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the anagrafica clienti fornitoris where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficaClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param comune the comune
	* @param ragioneSociale the ragione sociale
	* @param start the lower bound of the range of anagrafica clienti fornitoris
	* @param end the upper bound of the range of anagrafica clienti fornitoris (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching anagrafica clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.AnagraficaClientiFornitori> findByRagioneSociale(
		java.lang.String comune, java.lang.String ragioneSociale, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first anagrafica clienti fornitori in the ordered set where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	*
	* @param comune the comune
	* @param ragioneSociale the ragione sociale
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching anagrafica clienti fornitori
	* @throws it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException if a matching anagrafica clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.AnagraficaClientiFornitori findByRagioneSociale_First(
		java.lang.String comune, java.lang.String ragioneSociale,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException;

	/**
	* Returns the first anagrafica clienti fornitori in the ordered set where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	*
	* @param comune the comune
	* @param ragioneSociale the ragione sociale
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching anagrafica clienti fornitori, or <code>null</code> if a matching anagrafica clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.AnagraficaClientiFornitori fetchByRagioneSociale_First(
		java.lang.String comune, java.lang.String ragioneSociale,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last anagrafica clienti fornitori in the ordered set where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	*
	* @param comune the comune
	* @param ragioneSociale the ragione sociale
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching anagrafica clienti fornitori
	* @throws it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException if a matching anagrafica clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.AnagraficaClientiFornitori findByRagioneSociale_Last(
		java.lang.String comune, java.lang.String ragioneSociale,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException;

	/**
	* Returns the last anagrafica clienti fornitori in the ordered set where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	*
	* @param comune the comune
	* @param ragioneSociale the ragione sociale
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching anagrafica clienti fornitori, or <code>null</code> if a matching anagrafica clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.AnagraficaClientiFornitori fetchByRagioneSociale_Last(
		java.lang.String comune, java.lang.String ragioneSociale,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the anagrafica clienti fornitoris before and after the current anagrafica clienti fornitori in the ordered set where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	*
	* @param codiceSoggetto the primary key of the current anagrafica clienti fornitori
	* @param comune the comune
	* @param ragioneSociale the ragione sociale
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next anagrafica clienti fornitori
	* @throws it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException if a anagrafica clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.AnagraficaClientiFornitori[] findByRagioneSociale_PrevAndNext(
		java.lang.String codiceSoggetto, java.lang.String comune,
		java.lang.String ragioneSociale,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException;

	/**
	* Removes all the anagrafica clienti fornitoris where comune LIKE &#63; and ragioneSociale LIKE &#63; from the database.
	*
	* @param comune the comune
	* @param ragioneSociale the ragione sociale
	* @throws SystemException if a system exception occurred
	*/
	public void removeByRagioneSociale(java.lang.String comune,
		java.lang.String ragioneSociale)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of anagrafica clienti fornitoris where comune LIKE &#63; and ragioneSociale LIKE &#63;.
	*
	* @param comune the comune
	* @param ragioneSociale the ragione sociale
	* @return the number of matching anagrafica clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public int countByRagioneSociale(java.lang.String comune,
		java.lang.String ragioneSociale)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the anagrafica clienti fornitori in the entity cache if it is enabled.
	*
	* @param anagraficaClientiFornitori the anagrafica clienti fornitori
	*/
	public void cacheResult(
		it.bysoftware.ct.model.AnagraficaClientiFornitori anagraficaClientiFornitori);

	/**
	* Caches the anagrafica clienti fornitoris in the entity cache if it is enabled.
	*
	* @param anagraficaClientiFornitoris the anagrafica clienti fornitoris
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.AnagraficaClientiFornitori> anagraficaClientiFornitoris);

	/**
	* Creates a new anagrafica clienti fornitori with the primary key. Does not add the anagrafica clienti fornitori to the database.
	*
	* @param codiceSoggetto the primary key for the new anagrafica clienti fornitori
	* @return the new anagrafica clienti fornitori
	*/
	public it.bysoftware.ct.model.AnagraficaClientiFornitori create(
		java.lang.String codiceSoggetto);

	/**
	* Removes the anagrafica clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param codiceSoggetto the primary key of the anagrafica clienti fornitori
	* @return the anagrafica clienti fornitori that was removed
	* @throws it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException if a anagrafica clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.AnagraficaClientiFornitori remove(
		java.lang.String codiceSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException;

	public it.bysoftware.ct.model.AnagraficaClientiFornitori updateImpl(
		it.bysoftware.ct.model.AnagraficaClientiFornitori anagraficaClientiFornitori)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the anagrafica clienti fornitori with the primary key or throws a {@link it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException} if it could not be found.
	*
	* @param codiceSoggetto the primary key of the anagrafica clienti fornitori
	* @return the anagrafica clienti fornitori
	* @throws it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException if a anagrafica clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.AnagraficaClientiFornitori findByPrimaryKey(
		java.lang.String codiceSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException;

	/**
	* Returns the anagrafica clienti fornitori with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param codiceSoggetto the primary key of the anagrafica clienti fornitori
	* @return the anagrafica clienti fornitori, or <code>null</code> if a anagrafica clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.AnagraficaClientiFornitori fetchByPrimaryKey(
		java.lang.String codiceSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the anagrafica clienti fornitoris.
	*
	* @return the anagrafica clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.AnagraficaClientiFornitori> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the anagrafica clienti fornitoris.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficaClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of anagrafica clienti fornitoris
	* @param end the upper bound of the range of anagrafica clienti fornitoris (not inclusive)
	* @return the range of anagrafica clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.AnagraficaClientiFornitori> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the anagrafica clienti fornitoris.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficaClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of anagrafica clienti fornitoris
	* @param end the upper bound of the range of anagrafica clienti fornitoris (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of anagrafica clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.AnagraficaClientiFornitori> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the anagrafica clienti fornitoris from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of anagrafica clienti fornitoris.
	*
	* @return the number of anagrafica clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}