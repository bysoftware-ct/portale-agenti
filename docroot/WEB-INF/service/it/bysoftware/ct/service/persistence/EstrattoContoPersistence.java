/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.EstrattoConto;

/**
 * The persistence interface for the estratto conto service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see EstrattoContoPersistenceImpl
 * @see EstrattoContoUtil
 * @generated
 */
public interface EstrattoContoPersistence extends BasePersistence<EstrattoConto> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EstrattoContoUtil} to access the estratto conto persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the estratto contos where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @return the matching estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.EstrattoConto> findByTipoCodiceSogettoDataChiusa(
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the estratto contos where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @param start the lower bound of the range of estratto contos
	* @param end the upper bound of the range of estratto contos (not inclusive)
	* @return the range of matching estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.EstrattoConto> findByTipoCodiceSogettoDataChiusa(
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the estratto contos where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @param start the lower bound of the range of estratto contos
	* @param end the upper bound of the range of estratto contos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.EstrattoConto> findByTipoCodiceSogettoDataChiusa(
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first estratto conto in the ordered set where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching estratto conto
	* @throws it.bysoftware.ct.NoSuchEstrattoContoException if a matching estratto conto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EstrattoConto findByTipoCodiceSogettoDataChiusa_First(
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEstrattoContoException;

	/**
	* Returns the first estratto conto in the ordered set where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching estratto conto, or <code>null</code> if a matching estratto conto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EstrattoConto fetchByTipoCodiceSogettoDataChiusa_First(
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last estratto conto in the ordered set where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching estratto conto
	* @throws it.bysoftware.ct.NoSuchEstrattoContoException if a matching estratto conto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EstrattoConto findByTipoCodiceSogettoDataChiusa_Last(
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEstrattoContoException;

	/**
	* Returns the last estratto conto in the ordered set where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching estratto conto, or <code>null</code> if a matching estratto conto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EstrattoConto fetchByTipoCodiceSogettoDataChiusa_Last(
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the estratto contos before and after the current estratto conto in the ordered set where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* @param estrattoContoPK the primary key of the current estratto conto
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next estratto conto
	* @throws it.bysoftware.ct.NoSuchEstrattoContoException if a estratto conto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EstrattoConto[] findByTipoCodiceSogettoDataChiusa_PrevAndNext(
		it.bysoftware.ct.service.persistence.EstrattoContoPK estrattoContoPK,
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEstrattoContoException;

	/**
	* Removes all the estratto contos where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63; from the database.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @throws SystemException if a system exception occurred
	*/
	public void removeByTipoCodiceSogettoDataChiusa(boolean tipoSoggetto,
		java.lang.String codiceCliente, int stato, java.util.Date dataScadenza)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of estratto contos where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @return the number of matching estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public int countByTipoCodiceSogettoDataChiusa(boolean tipoSoggetto,
		java.lang.String codiceCliente, int stato, java.util.Date dataScadenza)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the estratto conto in the entity cache if it is enabled.
	*
	* @param estrattoConto the estratto conto
	*/
	public void cacheResult(it.bysoftware.ct.model.EstrattoConto estrattoConto);

	/**
	* Caches the estratto contos in the entity cache if it is enabled.
	*
	* @param estrattoContos the estratto contos
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.EstrattoConto> estrattoContos);

	/**
	* Creates a new estratto conto with the primary key. Does not add the estratto conto to the database.
	*
	* @param estrattoContoPK the primary key for the new estratto conto
	* @return the new estratto conto
	*/
	public it.bysoftware.ct.model.EstrattoConto create(
		it.bysoftware.ct.service.persistence.EstrattoContoPK estrattoContoPK);

	/**
	* Removes the estratto conto with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param estrattoContoPK the primary key of the estratto conto
	* @return the estratto conto that was removed
	* @throws it.bysoftware.ct.NoSuchEstrattoContoException if a estratto conto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EstrattoConto remove(
		it.bysoftware.ct.service.persistence.EstrattoContoPK estrattoContoPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEstrattoContoException;

	public it.bysoftware.ct.model.EstrattoConto updateImpl(
		it.bysoftware.ct.model.EstrattoConto estrattoConto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the estratto conto with the primary key or throws a {@link it.bysoftware.ct.NoSuchEstrattoContoException} if it could not be found.
	*
	* @param estrattoContoPK the primary key of the estratto conto
	* @return the estratto conto
	* @throws it.bysoftware.ct.NoSuchEstrattoContoException if a estratto conto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EstrattoConto findByPrimaryKey(
		it.bysoftware.ct.service.persistence.EstrattoContoPK estrattoContoPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEstrattoContoException;

	/**
	* Returns the estratto conto with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param estrattoContoPK the primary key of the estratto conto
	* @return the estratto conto, or <code>null</code> if a estratto conto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EstrattoConto fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.EstrattoContoPK estrattoContoPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the estratto contos.
	*
	* @return the estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.EstrattoConto> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the estratto contos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of estratto contos
	* @param end the upper bound of the range of estratto contos (not inclusive)
	* @return the range of estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.EstrattoConto> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the estratto contos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of estratto contos
	* @param end the upper bound of the range of estratto contos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.EstrattoConto> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the estratto contos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of estratto contos.
	*
	* @return the number of estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}