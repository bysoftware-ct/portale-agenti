/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.DatiCostantiClientiFornitori;

import java.util.List;

/**
 * The persistence utility for the dati costanti clienti fornitori service. This utility wraps {@link DatiCostantiClientiFornitoriPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see DatiCostantiClientiFornitoriPersistence
 * @see DatiCostantiClientiFornitoriPersistenceImpl
 * @generated
 */
public class DatiCostantiClientiFornitoriUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(
		DatiCostantiClientiFornitori datiCostantiClientiFornitori) {
		getPersistence().clearCache(datiCostantiClientiFornitori);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<DatiCostantiClientiFornitori> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<DatiCostantiClientiFornitori> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<DatiCostantiClientiFornitori> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static DatiCostantiClientiFornitori update(
		DatiCostantiClientiFornitori datiCostantiClientiFornitori)
		throws SystemException {
		return getPersistence().update(datiCostantiClientiFornitori);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static DatiCostantiClientiFornitori update(
		DatiCostantiClientiFornitori datiCostantiClientiFornitori,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence()
				   .update(datiCostantiClientiFornitori, serviceContext);
	}

	/**
	* Caches the dati costanti clienti fornitori in the entity cache if it is enabled.
	*
	* @param datiCostantiClientiFornitori the dati costanti clienti fornitori
	*/
	public static void cacheResult(
		it.bysoftware.ct.model.DatiCostantiClientiFornitori datiCostantiClientiFornitori) {
		getPersistence().cacheResult(datiCostantiClientiFornitori);
	}

	/**
	* Caches the dati costanti clienti fornitoris in the entity cache if it is enabled.
	*
	* @param datiCostantiClientiFornitoris the dati costanti clienti fornitoris
	*/
	public static void cacheResult(
		java.util.List<it.bysoftware.ct.model.DatiCostantiClientiFornitori> datiCostantiClientiFornitoris) {
		getPersistence().cacheResult(datiCostantiClientiFornitoris);
	}

	/**
	* Creates a new dati costanti clienti fornitori with the primary key. Does not add the dati costanti clienti fornitori to the database.
	*
	* @param datiCostantiClientiFornitoriPK the primary key for the new dati costanti clienti fornitori
	* @return the new dati costanti clienti fornitori
	*/
	public static it.bysoftware.ct.model.DatiCostantiClientiFornitori create(
		it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK datiCostantiClientiFornitoriPK) {
		return getPersistence().create(datiCostantiClientiFornitoriPK);
	}

	/**
	* Removes the dati costanti clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param datiCostantiClientiFornitoriPK the primary key of the dati costanti clienti fornitori
	* @return the dati costanti clienti fornitori that was removed
	* @throws it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException if a dati costanti clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.DatiCostantiClientiFornitori remove(
		it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK datiCostantiClientiFornitoriPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException {
		return getPersistence().remove(datiCostantiClientiFornitoriPK);
	}

	public static it.bysoftware.ct.model.DatiCostantiClientiFornitori updateImpl(
		it.bysoftware.ct.model.DatiCostantiClientiFornitori datiCostantiClientiFornitori)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(datiCostantiClientiFornitori);
	}

	/**
	* Returns the dati costanti clienti fornitori with the primary key or throws a {@link it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException} if it could not be found.
	*
	* @param datiCostantiClientiFornitoriPK the primary key of the dati costanti clienti fornitori
	* @return the dati costanti clienti fornitori
	* @throws it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException if a dati costanti clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.DatiCostantiClientiFornitori findByPrimaryKey(
		it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK datiCostantiClientiFornitoriPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException {
		return getPersistence().findByPrimaryKey(datiCostantiClientiFornitoriPK);
	}

	/**
	* Returns the dati costanti clienti fornitori with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param datiCostantiClientiFornitoriPK the primary key of the dati costanti clienti fornitori
	* @return the dati costanti clienti fornitori, or <code>null</code> if a dati costanti clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.DatiCostantiClientiFornitori fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK datiCostantiClientiFornitoriPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(datiCostantiClientiFornitoriPK);
	}

	/**
	* Returns all the dati costanti clienti fornitoris.
	*
	* @return the dati costanti clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.DatiCostantiClientiFornitori> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the dati costanti clienti fornitoris.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiCostantiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dati costanti clienti fornitoris
	* @param end the upper bound of the range of dati costanti clienti fornitoris (not inclusive)
	* @return the range of dati costanti clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.DatiCostantiClientiFornitori> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the dati costanti clienti fornitoris.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiCostantiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dati costanti clienti fornitoris
	* @param end the upper bound of the range of dati costanti clienti fornitoris (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of dati costanti clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.DatiCostantiClientiFornitori> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the dati costanti clienti fornitoris from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of dati costanti clienti fornitoris.
	*
	* @return the number of dati costanti clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static DatiCostantiClientiFornitoriPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (DatiCostantiClientiFornitoriPersistence)PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
					DatiCostantiClientiFornitoriPersistence.class.getName());

			ReferenceRegistry.registerReference(DatiCostantiClientiFornitoriUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(
		DatiCostantiClientiFornitoriPersistence persistence) {
	}

	private static DatiCostantiClientiFornitoriPersistence _persistence;
}