/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.TestataFattureClienti;

/**
 * The persistence interface for the testata fatture clienti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see TestataFattureClientiPersistenceImpl
 * @see TestataFattureClientiUtil
 * @generated
 */
public interface TestataFattureClientiPersistence extends BasePersistence<TestataFattureClienti> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TestataFattureClientiUtil} to access the testata fatture clienti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the testata fatture clienti where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroDocumento = &#63; and dataDocumento = &#63; and tipoSoggetto = &#63; and codiceCliente = &#63; or throws a {@link it.bysoftware.ct.NoSuchTestataFattureClientiException} if it could not be found.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroDocumento the numero documento
	* @param dataDocumento the data documento
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @return the matching testata fatture clienti
	* @throws it.bysoftware.ct.NoSuchTestataFattureClientiException if a matching testata fatture clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.TestataFattureClienti findByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroDocumento,
		java.util.Date dataDocumento, boolean tipoSoggetto,
		java.lang.String codiceCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchTestataFattureClientiException;

	/**
	* Returns the testata fatture clienti where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroDocumento = &#63; and dataDocumento = &#63; and tipoSoggetto = &#63; and codiceCliente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroDocumento the numero documento
	* @param dataDocumento the data documento
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @return the matching testata fatture clienti, or <code>null</code> if a matching testata fatture clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.TestataFattureClienti fetchByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroDocumento,
		java.util.Date dataDocumento, boolean tipoSoggetto,
		java.lang.String codiceCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the testata fatture clienti where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroDocumento = &#63; and dataDocumento = &#63; and tipoSoggetto = &#63; and codiceCliente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroDocumento the numero documento
	* @param dataDocumento the data documento
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching testata fatture clienti, or <code>null</code> if a matching testata fatture clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.TestataFattureClienti fetchByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroDocumento,
		java.util.Date dataDocumento, boolean tipoSoggetto,
		java.lang.String codiceCliente, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the testata fatture clienti where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroDocumento = &#63; and dataDocumento = &#63; and tipoSoggetto = &#63; and codiceCliente = &#63; from the database.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroDocumento the numero documento
	* @param dataDocumento the data documento
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @return the testata fatture clienti that was removed
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.TestataFattureClienti removeByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroDocumento,
		java.util.Date dataDocumento, boolean tipoSoggetto,
		java.lang.String codiceCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchTestataFattureClientiException;

	/**
	* Returns the number of testata fatture clientis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroDocumento = &#63; and dataDocumento = &#63; and tipoSoggetto = &#63; and codiceCliente = &#63;.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroDocumento the numero documento
	* @param dataDocumento the data documento
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @return the number of matching testata fatture clientis
	* @throws SystemException if a system exception occurred
	*/
	public int countByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		int numeroDocumento, java.util.Date dataDocumento,
		boolean tipoSoggetto, java.lang.String codiceCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the testata fatture clienti in the entity cache if it is enabled.
	*
	* @param testataFattureClienti the testata fatture clienti
	*/
	public void cacheResult(
		it.bysoftware.ct.model.TestataFattureClienti testataFattureClienti);

	/**
	* Caches the testata fatture clientis in the entity cache if it is enabled.
	*
	* @param testataFattureClientis the testata fatture clientis
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.TestataFattureClienti> testataFattureClientis);

	/**
	* Creates a new testata fatture clienti with the primary key. Does not add the testata fatture clienti to the database.
	*
	* @param testataFattureClientiPK the primary key for the new testata fatture clienti
	* @return the new testata fatture clienti
	*/
	public it.bysoftware.ct.model.TestataFattureClienti create(
		it.bysoftware.ct.service.persistence.TestataFattureClientiPK testataFattureClientiPK);

	/**
	* Removes the testata fatture clienti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param testataFattureClientiPK the primary key of the testata fatture clienti
	* @return the testata fatture clienti that was removed
	* @throws it.bysoftware.ct.NoSuchTestataFattureClientiException if a testata fatture clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.TestataFattureClienti remove(
		it.bysoftware.ct.service.persistence.TestataFattureClientiPK testataFattureClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchTestataFattureClientiException;

	public it.bysoftware.ct.model.TestataFattureClienti updateImpl(
		it.bysoftware.ct.model.TestataFattureClienti testataFattureClienti)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the testata fatture clienti with the primary key or throws a {@link it.bysoftware.ct.NoSuchTestataFattureClientiException} if it could not be found.
	*
	* @param testataFattureClientiPK the primary key of the testata fatture clienti
	* @return the testata fatture clienti
	* @throws it.bysoftware.ct.NoSuchTestataFattureClientiException if a testata fatture clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.TestataFattureClienti findByPrimaryKey(
		it.bysoftware.ct.service.persistence.TestataFattureClientiPK testataFattureClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchTestataFattureClientiException;

	/**
	* Returns the testata fatture clienti with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param testataFattureClientiPK the primary key of the testata fatture clienti
	* @return the testata fatture clienti, or <code>null</code> if a testata fatture clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.TestataFattureClienti fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.TestataFattureClientiPK testataFattureClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the testata fatture clientis.
	*
	* @return the testata fatture clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.TestataFattureClienti> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the testata fatture clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataFattureClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of testata fatture clientis
	* @param end the upper bound of the range of testata fatture clientis (not inclusive)
	* @return the range of testata fatture clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.TestataFattureClienti> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the testata fatture clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataFattureClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of testata fatture clientis
	* @param end the upper bound of the range of testata fatture clientis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of testata fatture clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.TestataFattureClienti> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the testata fatture clientis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of testata fatture clientis.
	*
	* @return the number of testata fatture clientis
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}