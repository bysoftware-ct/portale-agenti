/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class EstrattoContoPK implements Comparable<EstrattoContoPK>,
	Serializable {
	public boolean tipoSoggetto;
	public String codiceCliente;
	public int esercizioRegistrazione;
	public int numeroPartita;
	public int numeroScadenza;

	public EstrattoContoPK() {
	}

	public EstrattoContoPK(boolean tipoSoggetto, String codiceCliente,
		int esercizioRegistrazione, int numeroPartita, int numeroScadenza) {
		this.tipoSoggetto = tipoSoggetto;
		this.codiceCliente = codiceCliente;
		this.esercizioRegistrazione = esercizioRegistrazione;
		this.numeroPartita = numeroPartita;
		this.numeroScadenza = numeroScadenza;
	}

	public boolean getTipoSoggetto() {
		return tipoSoggetto;
	}

	public boolean isTipoSoggetto() {
		return tipoSoggetto;
	}

	public void setTipoSoggetto(boolean tipoSoggetto) {
		this.tipoSoggetto = tipoSoggetto;
	}

	public String getCodiceCliente() {
		return codiceCliente;
	}

	public void setCodiceCliente(String codiceCliente) {
		this.codiceCliente = codiceCliente;
	}

	public int getEsercizioRegistrazione() {
		return esercizioRegistrazione;
	}

	public void setEsercizioRegistrazione(int esercizioRegistrazione) {
		this.esercizioRegistrazione = esercizioRegistrazione;
	}

	public int getNumeroPartita() {
		return numeroPartita;
	}

	public void setNumeroPartita(int numeroPartita) {
		this.numeroPartita = numeroPartita;
	}

	public int getNumeroScadenza() {
		return numeroScadenza;
	}

	public void setNumeroScadenza(int numeroScadenza) {
		this.numeroScadenza = numeroScadenza;
	}

	@Override
	public int compareTo(EstrattoContoPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (!tipoSoggetto && pk.tipoSoggetto) {
			value = -1;
		}
		else if (tipoSoggetto && !pk.tipoSoggetto) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		value = codiceCliente.compareTo(pk.codiceCliente);

		if (value != 0) {
			return value;
		}

		if (esercizioRegistrazione < pk.esercizioRegistrazione) {
			value = -1;
		}
		else if (esercizioRegistrazione > pk.esercizioRegistrazione) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (numeroPartita < pk.numeroPartita) {
			value = -1;
		}
		else if (numeroPartita > pk.numeroPartita) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (numeroScadenza < pk.numeroScadenza) {
			value = -1;
		}
		else if (numeroScadenza > pk.numeroScadenza) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EstrattoContoPK)) {
			return false;
		}

		EstrattoContoPK pk = (EstrattoContoPK)obj;

		if ((tipoSoggetto == pk.tipoSoggetto) &&
				(codiceCliente.equals(pk.codiceCliente)) &&
				(esercizioRegistrazione == pk.esercizioRegistrazione) &&
				(numeroPartita == pk.numeroPartita) &&
				(numeroScadenza == pk.numeroScadenza)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(tipoSoggetto) + String.valueOf(codiceCliente) +
		String.valueOf(esercizioRegistrazione) + String.valueOf(numeroPartita) +
		String.valueOf(numeroScadenza)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("tipoSoggetto");
		sb.append(StringPool.EQUAL);
		sb.append(tipoSoggetto);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codiceCliente");
		sb.append(StringPool.EQUAL);
		sb.append(codiceCliente);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("esercizioRegistrazione");
		sb.append(StringPool.EQUAL);
		sb.append(esercizioRegistrazione);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("numeroPartita");
		sb.append(StringPool.EQUAL);
		sb.append(numeroPartita);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("numeroScadenza");
		sb.append(StringPool.EQUAL);
		sb.append(numeroScadenza);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}