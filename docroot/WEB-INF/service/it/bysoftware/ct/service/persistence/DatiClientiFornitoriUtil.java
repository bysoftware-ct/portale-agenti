/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.DatiClientiFornitori;

import java.util.List;

/**
 * The persistence utility for the dati clienti fornitori service. This utility wraps {@link DatiClientiFornitoriPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see DatiClientiFornitoriPersistence
 * @see DatiClientiFornitoriPersistenceImpl
 * @generated
 */
public class DatiClientiFornitoriUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(DatiClientiFornitori datiClientiFornitori) {
		getPersistence().clearCache(datiClientiFornitori);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<DatiClientiFornitori> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<DatiClientiFornitori> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<DatiClientiFornitori> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static DatiClientiFornitori update(
		DatiClientiFornitori datiClientiFornitori) throws SystemException {
		return getPersistence().update(datiClientiFornitori);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static DatiClientiFornitori update(
		DatiClientiFornitori datiClientiFornitori, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(datiClientiFornitori, serviceContext);
	}

	/**
	* Returns all the dati clienti fornitoris where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @return the matching dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.DatiClientiFornitori> findByCodiceAgente(
		java.lang.String codiceAgente, boolean tipoSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCodiceAgente(codiceAgente, tipoSoggetto);
	}

	/**
	* Returns a range of all the dati clienti fornitoris where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @param start the lower bound of the range of dati clienti fornitoris
	* @param end the upper bound of the range of dati clienti fornitoris (not inclusive)
	* @return the range of matching dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.DatiClientiFornitori> findByCodiceAgente(
		java.lang.String codiceAgente, boolean tipoSoggetto, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCodiceAgente(codiceAgente, tipoSoggetto, start, end);
	}

	/**
	* Returns an ordered range of all the dati clienti fornitoris where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @param start the lower bound of the range of dati clienti fornitoris
	* @param end the upper bound of the range of dati clienti fornitoris (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.DatiClientiFornitori> findByCodiceAgente(
		java.lang.String codiceAgente, boolean tipoSoggetto, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCodiceAgente(codiceAgente, tipoSoggetto, start, end,
			orderByComparator);
	}

	/**
	* Returns the first dati clienti fornitori in the ordered set where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dati clienti fornitori
	* @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a matching dati clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.DatiClientiFornitori findByCodiceAgente_First(
		java.lang.String codiceAgente, boolean tipoSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiClientiFornitoriException {
		return getPersistence()
				   .findByCodiceAgente_First(codiceAgente, tipoSoggetto,
			orderByComparator);
	}

	/**
	* Returns the first dati clienti fornitori in the ordered set where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dati clienti fornitori, or <code>null</code> if a matching dati clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.DatiClientiFornitori fetchByCodiceAgente_First(
		java.lang.String codiceAgente, boolean tipoSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCodiceAgente_First(codiceAgente, tipoSoggetto,
			orderByComparator);
	}

	/**
	* Returns the last dati clienti fornitori in the ordered set where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dati clienti fornitori
	* @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a matching dati clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.DatiClientiFornitori findByCodiceAgente_Last(
		java.lang.String codiceAgente, boolean tipoSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiClientiFornitoriException {
		return getPersistence()
				   .findByCodiceAgente_Last(codiceAgente, tipoSoggetto,
			orderByComparator);
	}

	/**
	* Returns the last dati clienti fornitori in the ordered set where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dati clienti fornitori, or <code>null</code> if a matching dati clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.DatiClientiFornitori fetchByCodiceAgente_Last(
		java.lang.String codiceAgente, boolean tipoSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCodiceAgente_Last(codiceAgente, tipoSoggetto,
			orderByComparator);
	}

	/**
	* Returns the dati clienti fornitoris before and after the current dati clienti fornitori in the ordered set where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* @param datiClientiFornitoriPK the primary key of the current dati clienti fornitori
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next dati clienti fornitori
	* @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a dati clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.DatiClientiFornitori[] findByCodiceAgente_PrevAndNext(
		it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK datiClientiFornitoriPK,
		java.lang.String codiceAgente, boolean tipoSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiClientiFornitoriException {
		return getPersistence()
				   .findByCodiceAgente_PrevAndNext(datiClientiFornitoriPK,
			codiceAgente, tipoSoggetto, orderByComparator);
	}

	/**
	* Removes all the dati clienti fornitoris where codiceAgente = &#63; and tipoSoggetto = &#63; from the database.
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCodiceAgente(java.lang.String codiceAgente,
		boolean tipoSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCodiceAgente(codiceAgente, tipoSoggetto);
	}

	/**
	* Returns the number of dati clienti fornitoris where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @return the number of matching dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCodiceAgente(java.lang.String codiceAgente,
		boolean tipoSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCodiceAgente(codiceAgente, tipoSoggetto);
	}

	/**
	* Returns the dati clienti fornitori where codiceSoggetto = &#63; and tipoSoggetto = &#63; or throws a {@link it.bysoftware.ct.NoSuchDatiClientiFornitoriException} if it could not be found.
	*
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @return the matching dati clienti fornitori
	* @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a matching dati clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.DatiClientiFornitori findByCodiceSoggetto(
		java.lang.String codiceSoggetto, boolean tipoSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiClientiFornitoriException {
		return getPersistence()
				   .findByCodiceSoggetto(codiceSoggetto, tipoSoggetto);
	}

	/**
	* Returns the dati clienti fornitori where codiceSoggetto = &#63; and tipoSoggetto = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @return the matching dati clienti fornitori, or <code>null</code> if a matching dati clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.DatiClientiFornitori fetchByCodiceSoggetto(
		java.lang.String codiceSoggetto, boolean tipoSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCodiceSoggetto(codiceSoggetto, tipoSoggetto);
	}

	/**
	* Returns the dati clienti fornitori where codiceSoggetto = &#63; and tipoSoggetto = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching dati clienti fornitori, or <code>null</code> if a matching dati clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.DatiClientiFornitori fetchByCodiceSoggetto(
		java.lang.String codiceSoggetto, boolean tipoSoggetto,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCodiceSoggetto(codiceSoggetto, tipoSoggetto,
			retrieveFromCache);
	}

	/**
	* Removes the dati clienti fornitori where codiceSoggetto = &#63; and tipoSoggetto = &#63; from the database.
	*
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @return the dati clienti fornitori that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.DatiClientiFornitori removeByCodiceSoggetto(
		java.lang.String codiceSoggetto, boolean tipoSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiClientiFornitoriException {
		return getPersistence()
				   .removeByCodiceSoggetto(codiceSoggetto, tipoSoggetto);
	}

	/**
	* Returns the number of dati clienti fornitoris where codiceSoggetto = &#63; and tipoSoggetto = &#63;.
	*
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @return the number of matching dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCodiceSoggetto(java.lang.String codiceSoggetto,
		boolean tipoSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByCodiceSoggetto(codiceSoggetto, tipoSoggetto);
	}

	/**
	* Caches the dati clienti fornitori in the entity cache if it is enabled.
	*
	* @param datiClientiFornitori the dati clienti fornitori
	*/
	public static void cacheResult(
		it.bysoftware.ct.model.DatiClientiFornitori datiClientiFornitori) {
		getPersistence().cacheResult(datiClientiFornitori);
	}

	/**
	* Caches the dati clienti fornitoris in the entity cache if it is enabled.
	*
	* @param datiClientiFornitoris the dati clienti fornitoris
	*/
	public static void cacheResult(
		java.util.List<it.bysoftware.ct.model.DatiClientiFornitori> datiClientiFornitoris) {
		getPersistence().cacheResult(datiClientiFornitoris);
	}

	/**
	* Creates a new dati clienti fornitori with the primary key. Does not add the dati clienti fornitori to the database.
	*
	* @param datiClientiFornitoriPK the primary key for the new dati clienti fornitori
	* @return the new dati clienti fornitori
	*/
	public static it.bysoftware.ct.model.DatiClientiFornitori create(
		it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK datiClientiFornitoriPK) {
		return getPersistence().create(datiClientiFornitoriPK);
	}

	/**
	* Removes the dati clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param datiClientiFornitoriPK the primary key of the dati clienti fornitori
	* @return the dati clienti fornitori that was removed
	* @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a dati clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.DatiClientiFornitori remove(
		it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK datiClientiFornitoriPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiClientiFornitoriException {
		return getPersistence().remove(datiClientiFornitoriPK);
	}

	public static it.bysoftware.ct.model.DatiClientiFornitori updateImpl(
		it.bysoftware.ct.model.DatiClientiFornitori datiClientiFornitori)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(datiClientiFornitori);
	}

	/**
	* Returns the dati clienti fornitori with the primary key or throws a {@link it.bysoftware.ct.NoSuchDatiClientiFornitoriException} if it could not be found.
	*
	* @param datiClientiFornitoriPK the primary key of the dati clienti fornitori
	* @return the dati clienti fornitori
	* @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a dati clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.DatiClientiFornitori findByPrimaryKey(
		it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK datiClientiFornitoriPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiClientiFornitoriException {
		return getPersistence().findByPrimaryKey(datiClientiFornitoriPK);
	}

	/**
	* Returns the dati clienti fornitori with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param datiClientiFornitoriPK the primary key of the dati clienti fornitori
	* @return the dati clienti fornitori, or <code>null</code> if a dati clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.DatiClientiFornitori fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK datiClientiFornitoriPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(datiClientiFornitoriPK);
	}

	/**
	* Returns all the dati clienti fornitoris.
	*
	* @return the dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.DatiClientiFornitori> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the dati clienti fornitoris.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dati clienti fornitoris
	* @param end the upper bound of the range of dati clienti fornitoris (not inclusive)
	* @return the range of dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.DatiClientiFornitori> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the dati clienti fornitoris.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dati clienti fornitoris
	* @param end the upper bound of the range of dati clienti fornitoris (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.DatiClientiFornitori> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the dati clienti fornitoris from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of dati clienti fornitoris.
	*
	* @return the number of dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static DatiClientiFornitoriPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (DatiClientiFornitoriPersistence)PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
					DatiClientiFornitoriPersistence.class.getName());

			ReferenceRegistry.registerReference(DatiClientiFornitoriUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(DatiClientiFornitoriPersistence persistence) {
	}

	private static DatiClientiFornitoriPersistence _persistence;
}