/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.EvasioniOrdini;

/**
 * The persistence interface for the evasioni ordini service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see EvasioniOrdiniPersistenceImpl
 * @see EvasioniOrdiniUtil
 * @generated
 */
public interface EvasioniOrdiniPersistence extends BasePersistence<EvasioniOrdini> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EvasioniOrdiniUtil} to access the evasioni ordini persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the evasioni ordinis where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @return the matching evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.EvasioniOrdini> findByClienteAndOrdine(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the evasioni ordinis where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @param start the lower bound of the range of evasioni ordinis
	* @param end the upper bound of the range of evasioni ordinis (not inclusive)
	* @return the range of matching evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.EvasioniOrdini> findByClienteAndOrdine(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the evasioni ordinis where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @param start the lower bound of the range of evasioni ordinis
	* @param end the upper bound of the range of evasioni ordinis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.EvasioniOrdini> findByClienteAndOrdine(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first evasioni ordini in the ordered set where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evasioni ordini
	* @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a matching evasioni ordini could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EvasioniOrdini findByClienteAndOrdine_First(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEvasioniOrdiniException;

	/**
	* Returns the first evasioni ordini in the ordered set where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evasioni ordini, or <code>null</code> if a matching evasioni ordini could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EvasioniOrdini fetchByClienteAndOrdine_First(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last evasioni ordini in the ordered set where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evasioni ordini
	* @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a matching evasioni ordini could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EvasioniOrdini findByClienteAndOrdine_Last(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEvasioniOrdiniException;

	/**
	* Returns the last evasioni ordini in the ordered set where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evasioni ordini, or <code>null</code> if a matching evasioni ordini could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EvasioniOrdini fetchByClienteAndOrdine_Last(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the evasioni ordinis before and after the current evasioni ordini in the ordered set where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* @param ID the primary key of the current evasioni ordini
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next evasioni ordini
	* @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a evasioni ordini with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EvasioniOrdini[] findByClienteAndOrdine_PrevAndNext(
		java.lang.String ID, java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEvasioniOrdiniException;

	/**
	* Removes all the evasioni ordinis where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63; from the database.
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @throws SystemException if a system exception occurred
	*/
	public void removeByClienteAndOrdine(java.lang.String codiceCliente,
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, java.lang.String codiceDeposito,
		int tipoOrdine, int numeroOrdine, java.lang.String codiceArticolo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of evasioni ordinis where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @return the number of matching evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public int countByClienteAndOrdine(java.lang.String codiceCliente,
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, java.lang.String codiceDeposito,
		int tipoOrdine, int numeroOrdine, java.lang.String codiceArticolo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the evasioni ordini in the entity cache if it is enabled.
	*
	* @param evasioniOrdini the evasioni ordini
	*/
	public void cacheResult(
		it.bysoftware.ct.model.EvasioniOrdini evasioniOrdini);

	/**
	* Caches the evasioni ordinis in the entity cache if it is enabled.
	*
	* @param evasioniOrdinis the evasioni ordinis
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.EvasioniOrdini> evasioniOrdinis);

	/**
	* Creates a new evasioni ordini with the primary key. Does not add the evasioni ordini to the database.
	*
	* @param ID the primary key for the new evasioni ordini
	* @return the new evasioni ordini
	*/
	public it.bysoftware.ct.model.EvasioniOrdini create(java.lang.String ID);

	/**
	* Removes the evasioni ordini with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the evasioni ordini
	* @return the evasioni ordini that was removed
	* @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a evasioni ordini with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EvasioniOrdini remove(java.lang.String ID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEvasioniOrdiniException;

	public it.bysoftware.ct.model.EvasioniOrdini updateImpl(
		it.bysoftware.ct.model.EvasioniOrdini evasioniOrdini)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the evasioni ordini with the primary key or throws a {@link it.bysoftware.ct.NoSuchEvasioniOrdiniException} if it could not be found.
	*
	* @param ID the primary key of the evasioni ordini
	* @return the evasioni ordini
	* @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a evasioni ordini with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EvasioniOrdini findByPrimaryKey(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEvasioniOrdiniException;

	/**
	* Returns the evasioni ordini with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ID the primary key of the evasioni ordini
	* @return the evasioni ordini, or <code>null</code> if a evasioni ordini with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.EvasioniOrdini fetchByPrimaryKey(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the evasioni ordinis.
	*
	* @return the evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.EvasioniOrdini> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the evasioni ordinis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of evasioni ordinis
	* @param end the upper bound of the range of evasioni ordinis (not inclusive)
	* @return the range of evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.EvasioniOrdini> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the evasioni ordinis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of evasioni ordinis
	* @param end the upper bound of the range of evasioni ordinis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.EvasioniOrdini> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the evasioni ordinis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of evasioni ordinis.
	*
	* @return the number of evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}