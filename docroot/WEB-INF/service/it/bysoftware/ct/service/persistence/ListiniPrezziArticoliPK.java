/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

import java.util.Date;

/**
 * @author Mario Torrisi
 * @generated
 */
public class ListiniPrezziArticoliPK implements Comparable<ListiniPrezziArticoliPK>,
	Serializable {
	public String codiceListino;
	public String codiceDivisa;
	public int tipoSoggetto;
	public String codiceSoggetto;
	public Date dataInizioValidita;
	public String codiceArticolo;
	public String codiceVariante;
	public String codiceLavorazione;
	public double quantInizioValiditaPrezzo;

	public ListiniPrezziArticoliPK() {
	}

	public ListiniPrezziArticoliPK(String codiceListino, String codiceDivisa,
		int tipoSoggetto, String codiceSoggetto, Date dataInizioValidita,
		String codiceArticolo, String codiceVariante, String codiceLavorazione,
		double quantInizioValiditaPrezzo) {
		this.codiceListino = codiceListino;
		this.codiceDivisa = codiceDivisa;
		this.tipoSoggetto = tipoSoggetto;
		this.codiceSoggetto = codiceSoggetto;
		this.dataInizioValidita = dataInizioValidita;
		this.codiceArticolo = codiceArticolo;
		this.codiceVariante = codiceVariante;
		this.codiceLavorazione = codiceLavorazione;
		this.quantInizioValiditaPrezzo = quantInizioValiditaPrezzo;
	}

	public String getCodiceListino() {
		return codiceListino;
	}

	public void setCodiceListino(String codiceListino) {
		this.codiceListino = codiceListino;
	}

	public String getCodiceDivisa() {
		return codiceDivisa;
	}

	public void setCodiceDivisa(String codiceDivisa) {
		this.codiceDivisa = codiceDivisa;
	}

	public int getTipoSoggetto() {
		return tipoSoggetto;
	}

	public void setTipoSoggetto(int tipoSoggetto) {
		this.tipoSoggetto = tipoSoggetto;
	}

	public String getCodiceSoggetto() {
		return codiceSoggetto;
	}

	public void setCodiceSoggetto(String codiceSoggetto) {
		this.codiceSoggetto = codiceSoggetto;
	}

	public Date getDataInizioValidita() {
		return dataInizioValidita;
	}

	public void setDataInizioValidita(Date dataInizioValidita) {
		this.dataInizioValidita = dataInizioValidita;
	}

	public String getCodiceArticolo() {
		return codiceArticolo;
	}

	public void setCodiceArticolo(String codiceArticolo) {
		this.codiceArticolo = codiceArticolo;
	}

	public String getCodiceVariante() {
		return codiceVariante;
	}

	public void setCodiceVariante(String codiceVariante) {
		this.codiceVariante = codiceVariante;
	}

	public String getCodiceLavorazione() {
		return codiceLavorazione;
	}

	public void setCodiceLavorazione(String codiceLavorazione) {
		this.codiceLavorazione = codiceLavorazione;
	}

	public double getQuantInizioValiditaPrezzo() {
		return quantInizioValiditaPrezzo;
	}

	public void setQuantInizioValiditaPrezzo(double quantInizioValiditaPrezzo) {
		this.quantInizioValiditaPrezzo = quantInizioValiditaPrezzo;
	}

	@Override
	public int compareTo(ListiniPrezziArticoliPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		value = codiceListino.compareTo(pk.codiceListino);

		if (value != 0) {
			return value;
		}

		value = codiceDivisa.compareTo(pk.codiceDivisa);

		if (value != 0) {
			return value;
		}

		if (tipoSoggetto < pk.tipoSoggetto) {
			value = -1;
		}
		else if (tipoSoggetto > pk.tipoSoggetto) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		value = codiceSoggetto.compareTo(pk.codiceSoggetto);

		if (value != 0) {
			return value;
		}

		value = DateUtil.compareTo(dataInizioValidita, pk.dataInizioValidita);

		if (value != 0) {
			return value;
		}

		value = codiceArticolo.compareTo(pk.codiceArticolo);

		if (value != 0) {
			return value;
		}

		value = codiceVariante.compareTo(pk.codiceVariante);

		if (value != 0) {
			return value;
		}

		value = codiceLavorazione.compareTo(pk.codiceLavorazione);

		if (value != 0) {
			return value;
		}

		if (quantInizioValiditaPrezzo < pk.quantInizioValiditaPrezzo) {
			value = -1;
		}
		else if (quantInizioValiditaPrezzo > pk.quantInizioValiditaPrezzo) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ListiniPrezziArticoliPK)) {
			return false;
		}

		ListiniPrezziArticoliPK pk = (ListiniPrezziArticoliPK)obj;

		if ((codiceListino.equals(pk.codiceListino)) &&
				(codiceDivisa.equals(pk.codiceDivisa)) &&
				(tipoSoggetto == pk.tipoSoggetto) &&
				(codiceSoggetto.equals(pk.codiceSoggetto)) &&
				(dataInizioValidita.equals(pk.dataInizioValidita)) &&
				(codiceArticolo.equals(pk.codiceArticolo)) &&
				(codiceVariante.equals(pk.codiceVariante)) &&
				(codiceLavorazione.equals(pk.codiceLavorazione)) &&
				(quantInizioValiditaPrezzo == pk.quantInizioValiditaPrezzo)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(codiceListino) + String.valueOf(codiceDivisa) +
		String.valueOf(tipoSoggetto) + String.valueOf(codiceSoggetto) +
		dataInizioValidita.toString() + String.valueOf(codiceArticolo) +
		String.valueOf(codiceVariante) + String.valueOf(codiceLavorazione) +
		String.valueOf(quantInizioValiditaPrezzo)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(45);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("codiceListino");
		sb.append(StringPool.EQUAL);
		sb.append(codiceListino);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codiceDivisa");
		sb.append(StringPool.EQUAL);
		sb.append(codiceDivisa);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("tipoSoggetto");
		sb.append(StringPool.EQUAL);
		sb.append(tipoSoggetto);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codiceSoggetto");
		sb.append(StringPool.EQUAL);
		sb.append(codiceSoggetto);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("dataInizioValidita");
		sb.append(StringPool.EQUAL);
		sb.append(dataInizioValidita);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codiceArticolo");
		sb.append(StringPool.EQUAL);
		sb.append(codiceArticolo);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codiceVariante");
		sb.append(StringPool.EQUAL);
		sb.append(codiceVariante);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codiceLavorazione");
		sb.append(StringPool.EQUAL);
		sb.append(codiceLavorazione);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("quantInizioValiditaPrezzo");
		sb.append(StringPool.EQUAL);
		sb.append(quantInizioValiditaPrezzo);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}