/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.ScontiArticoli;

/**
 * The persistence interface for the sconti articoli service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ScontiArticoliPersistenceImpl
 * @see ScontiArticoliUtil
 * @generated
 */
public interface ScontiArticoliPersistence extends BasePersistence<ScontiArticoli> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ScontiArticoliUtil} to access the sconti articoli persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the sconti articoli in the entity cache if it is enabled.
	*
	* @param scontiArticoli the sconti articoli
	*/
	public void cacheResult(
		it.bysoftware.ct.model.ScontiArticoli scontiArticoli);

	/**
	* Caches the sconti articolis in the entity cache if it is enabled.
	*
	* @param scontiArticolis the sconti articolis
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.ScontiArticoli> scontiArticolis);

	/**
	* Creates a new sconti articoli with the primary key. Does not add the sconti articoli to the database.
	*
	* @param scontiArticoliPK the primary key for the new sconti articoli
	* @return the new sconti articoli
	*/
	public it.bysoftware.ct.model.ScontiArticoli create(
		it.bysoftware.ct.service.persistence.ScontiArticoliPK scontiArticoliPK);

	/**
	* Removes the sconti articoli with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param scontiArticoliPK the primary key of the sconti articoli
	* @return the sconti articoli that was removed
	* @throws it.bysoftware.ct.NoSuchScontiArticoliException if a sconti articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ScontiArticoli remove(
		it.bysoftware.ct.service.persistence.ScontiArticoliPK scontiArticoliPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchScontiArticoliException;

	public it.bysoftware.ct.model.ScontiArticoli updateImpl(
		it.bysoftware.ct.model.ScontiArticoli scontiArticoli)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the sconti articoli with the primary key or throws a {@link it.bysoftware.ct.NoSuchScontiArticoliException} if it could not be found.
	*
	* @param scontiArticoliPK the primary key of the sconti articoli
	* @return the sconti articoli
	* @throws it.bysoftware.ct.NoSuchScontiArticoliException if a sconti articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ScontiArticoli findByPrimaryKey(
		it.bysoftware.ct.service.persistence.ScontiArticoliPK scontiArticoliPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchScontiArticoliException;

	/**
	* Returns the sconti articoli with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param scontiArticoliPK the primary key of the sconti articoli
	* @return the sconti articoli, or <code>null</code> if a sconti articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ScontiArticoli fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.ScontiArticoliPK scontiArticoliPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the sconti articolis.
	*
	* @return the sconti articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ScontiArticoli> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the sconti articolis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScontiArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sconti articolis
	* @param end the upper bound of the range of sconti articolis (not inclusive)
	* @return the range of sconti articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ScontiArticoli> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the sconti articolis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ScontiArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sconti articolis
	* @param end the upper bound of the range of sconti articolis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of sconti articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ScontiArticoli> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the sconti articolis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of sconti articolis.
	*
	* @return the number of sconti articolis
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}