/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.EvasioniOrdini;

import java.util.List;

/**
 * The persistence utility for the evasioni ordini service. This utility wraps {@link EvasioniOrdiniPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see EvasioniOrdiniPersistence
 * @see EvasioniOrdiniPersistenceImpl
 * @generated
 */
public class EvasioniOrdiniUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(EvasioniOrdini evasioniOrdini) {
		getPersistence().clearCache(evasioniOrdini);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<EvasioniOrdini> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<EvasioniOrdini> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<EvasioniOrdini> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static EvasioniOrdini update(EvasioniOrdini evasioniOrdini)
		throws SystemException {
		return getPersistence().update(evasioniOrdini);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static EvasioniOrdini update(EvasioniOrdini evasioniOrdini,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(evasioniOrdini, serviceContext);
	}

	/**
	* Returns all the evasioni ordinis where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @return the matching evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.EvasioniOrdini> findByClienteAndOrdine(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByClienteAndOrdine(codiceCliente, anno, codiceAttivita,
			codiceCentro, codiceDeposito, tipoOrdine, numeroOrdine,
			codiceArticolo);
	}

	/**
	* Returns a range of all the evasioni ordinis where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @param start the lower bound of the range of evasioni ordinis
	* @param end the upper bound of the range of evasioni ordinis (not inclusive)
	* @return the range of matching evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.EvasioniOrdini> findByClienteAndOrdine(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByClienteAndOrdine(codiceCliente, anno, codiceAttivita,
			codiceCentro, codiceDeposito, tipoOrdine, numeroOrdine,
			codiceArticolo, start, end);
	}

	/**
	* Returns an ordered range of all the evasioni ordinis where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @param start the lower bound of the range of evasioni ordinis
	* @param end the upper bound of the range of evasioni ordinis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.EvasioniOrdini> findByClienteAndOrdine(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByClienteAndOrdine(codiceCliente, anno, codiceAttivita,
			codiceCentro, codiceDeposito, tipoOrdine, numeroOrdine,
			codiceArticolo, start, end, orderByComparator);
	}

	/**
	* Returns the first evasioni ordini in the ordered set where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evasioni ordini
	* @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a matching evasioni ordini could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EvasioniOrdini findByClienteAndOrdine_First(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEvasioniOrdiniException {
		return getPersistence()
				   .findByClienteAndOrdine_First(codiceCliente, anno,
			codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
			numeroOrdine, codiceArticolo, orderByComparator);
	}

	/**
	* Returns the first evasioni ordini in the ordered set where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evasioni ordini, or <code>null</code> if a matching evasioni ordini could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EvasioniOrdini fetchByClienteAndOrdine_First(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByClienteAndOrdine_First(codiceCliente, anno,
			codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
			numeroOrdine, codiceArticolo, orderByComparator);
	}

	/**
	* Returns the last evasioni ordini in the ordered set where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evasioni ordini
	* @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a matching evasioni ordini could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EvasioniOrdini findByClienteAndOrdine_Last(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEvasioniOrdiniException {
		return getPersistence()
				   .findByClienteAndOrdine_Last(codiceCliente, anno,
			codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
			numeroOrdine, codiceArticolo, orderByComparator);
	}

	/**
	* Returns the last evasioni ordini in the ordered set where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evasioni ordini, or <code>null</code> if a matching evasioni ordini could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EvasioniOrdini fetchByClienteAndOrdine_Last(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByClienteAndOrdine_Last(codiceCliente, anno,
			codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
			numeroOrdine, codiceArticolo, orderByComparator);
	}

	/**
	* Returns the evasioni ordinis before and after the current evasioni ordini in the ordered set where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* @param ID the primary key of the current evasioni ordini
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next evasioni ordini
	* @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a evasioni ordini with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EvasioniOrdini[] findByClienteAndOrdine_PrevAndNext(
		java.lang.String ID, java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEvasioniOrdiniException {
		return getPersistence()
				   .findByClienteAndOrdine_PrevAndNext(ID, codiceCliente, anno,
			codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
			numeroOrdine, codiceArticolo, orderByComparator);
	}

	/**
	* Removes all the evasioni ordinis where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63; from the database.
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByClienteAndOrdine(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine,
		java.lang.String codiceArticolo)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByClienteAndOrdine(codiceCliente, anno, codiceAttivita,
			codiceCentro, codiceDeposito, tipoOrdine, numeroOrdine,
			codiceArticolo);
	}

	/**
	* Returns the number of evasioni ordinis where codiceCliente = &#63; and anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; and codiceArticolo &ne; &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param codiceArticolo the codice articolo
	* @return the number of matching evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public static int countByClienteAndOrdine(java.lang.String codiceCliente,
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, java.lang.String codiceDeposito,
		int tipoOrdine, int numeroOrdine, java.lang.String codiceArticolo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByClienteAndOrdine(codiceCliente, anno,
			codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
			numeroOrdine, codiceArticolo);
	}

	/**
	* Caches the evasioni ordini in the entity cache if it is enabled.
	*
	* @param evasioniOrdini the evasioni ordini
	*/
	public static void cacheResult(
		it.bysoftware.ct.model.EvasioniOrdini evasioniOrdini) {
		getPersistence().cacheResult(evasioniOrdini);
	}

	/**
	* Caches the evasioni ordinis in the entity cache if it is enabled.
	*
	* @param evasioniOrdinis the evasioni ordinis
	*/
	public static void cacheResult(
		java.util.List<it.bysoftware.ct.model.EvasioniOrdini> evasioniOrdinis) {
		getPersistence().cacheResult(evasioniOrdinis);
	}

	/**
	* Creates a new evasioni ordini with the primary key. Does not add the evasioni ordini to the database.
	*
	* @param ID the primary key for the new evasioni ordini
	* @return the new evasioni ordini
	*/
	public static it.bysoftware.ct.model.EvasioniOrdini create(
		java.lang.String ID) {
		return getPersistence().create(ID);
	}

	/**
	* Removes the evasioni ordini with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the evasioni ordini
	* @return the evasioni ordini that was removed
	* @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a evasioni ordini with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EvasioniOrdini remove(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEvasioniOrdiniException {
		return getPersistence().remove(ID);
	}

	public static it.bysoftware.ct.model.EvasioniOrdini updateImpl(
		it.bysoftware.ct.model.EvasioniOrdini evasioniOrdini)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(evasioniOrdini);
	}

	/**
	* Returns the evasioni ordini with the primary key or throws a {@link it.bysoftware.ct.NoSuchEvasioniOrdiniException} if it could not be found.
	*
	* @param ID the primary key of the evasioni ordini
	* @return the evasioni ordini
	* @throws it.bysoftware.ct.NoSuchEvasioniOrdiniException if a evasioni ordini with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EvasioniOrdini findByPrimaryKey(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEvasioniOrdiniException {
		return getPersistence().findByPrimaryKey(ID);
	}

	/**
	* Returns the evasioni ordini with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ID the primary key of the evasioni ordini
	* @return the evasioni ordini, or <code>null</code> if a evasioni ordini with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EvasioniOrdini fetchByPrimaryKey(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(ID);
	}

	/**
	* Returns all the evasioni ordinis.
	*
	* @return the evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.EvasioniOrdini> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the evasioni ordinis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of evasioni ordinis
	* @param end the upper bound of the range of evasioni ordinis (not inclusive)
	* @return the range of evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.EvasioniOrdini> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the evasioni ordinis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of evasioni ordinis
	* @param end the upper bound of the range of evasioni ordinis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.EvasioniOrdini> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the evasioni ordinis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of evasioni ordinis.
	*
	* @return the number of evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static EvasioniOrdiniPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (EvasioniOrdiniPersistence)PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
					EvasioniOrdiniPersistence.class.getName());

			ReferenceRegistry.registerReference(EvasioniOrdiniUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(EvasioniOrdiniPersistence persistence) {
	}

	private static EvasioniOrdiniPersistence _persistence;
}