/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.PianoPagamenti;

import java.util.List;

/**
 * The persistence utility for the piano pagamenti service. This utility wraps {@link PianoPagamentiPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see PianoPagamentiPersistence
 * @see PianoPagamentiPersistenceImpl
 * @generated
 */
public class PianoPagamentiUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(PianoPagamenti pianoPagamenti) {
		getPersistence().clearCache(pianoPagamenti);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<PianoPagamenti> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<PianoPagamenti> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<PianoPagamenti> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static PianoPagamenti update(PianoPagamenti pianoPagamenti)
		throws SystemException {
		return getPersistence().update(pianoPagamenti);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static PianoPagamenti update(PianoPagamenti pianoPagamenti,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(pianoPagamenti, serviceContext);
	}

	/**
	* Caches the piano pagamenti in the entity cache if it is enabled.
	*
	* @param pianoPagamenti the piano pagamenti
	*/
	public static void cacheResult(
		it.bysoftware.ct.model.PianoPagamenti pianoPagamenti) {
		getPersistence().cacheResult(pianoPagamenti);
	}

	/**
	* Caches the piano pagamentis in the entity cache if it is enabled.
	*
	* @param pianoPagamentis the piano pagamentis
	*/
	public static void cacheResult(
		java.util.List<it.bysoftware.ct.model.PianoPagamenti> pianoPagamentis) {
		getPersistence().cacheResult(pianoPagamentis);
	}

	/**
	* Creates a new piano pagamenti with the primary key. Does not add the piano pagamenti to the database.
	*
	* @param codicePianoPagamento the primary key for the new piano pagamenti
	* @return the new piano pagamenti
	*/
	public static it.bysoftware.ct.model.PianoPagamenti create(
		java.lang.String codicePianoPagamento) {
		return getPersistence().create(codicePianoPagamento);
	}

	/**
	* Removes the piano pagamenti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param codicePianoPagamento the primary key of the piano pagamenti
	* @return the piano pagamenti that was removed
	* @throws it.bysoftware.ct.NoSuchPianoPagamentiException if a piano pagamenti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.PianoPagamenti remove(
		java.lang.String codicePianoPagamento)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchPianoPagamentiException {
		return getPersistence().remove(codicePianoPagamento);
	}

	public static it.bysoftware.ct.model.PianoPagamenti updateImpl(
		it.bysoftware.ct.model.PianoPagamenti pianoPagamenti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(pianoPagamenti);
	}

	/**
	* Returns the piano pagamenti with the primary key or throws a {@link it.bysoftware.ct.NoSuchPianoPagamentiException} if it could not be found.
	*
	* @param codicePianoPagamento the primary key of the piano pagamenti
	* @return the piano pagamenti
	* @throws it.bysoftware.ct.NoSuchPianoPagamentiException if a piano pagamenti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.PianoPagamenti findByPrimaryKey(
		java.lang.String codicePianoPagamento)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchPianoPagamentiException {
		return getPersistence().findByPrimaryKey(codicePianoPagamento);
	}

	/**
	* Returns the piano pagamenti with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param codicePianoPagamento the primary key of the piano pagamenti
	* @return the piano pagamenti, or <code>null</code> if a piano pagamenti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.PianoPagamenti fetchByPrimaryKey(
		java.lang.String codicePianoPagamento)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(codicePianoPagamento);
	}

	/**
	* Returns all the piano pagamentis.
	*
	* @return the piano pagamentis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.PianoPagamenti> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the piano pagamentis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianoPagamentiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of piano pagamentis
	* @param end the upper bound of the range of piano pagamentis (not inclusive)
	* @return the range of piano pagamentis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.PianoPagamenti> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the piano pagamentis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianoPagamentiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of piano pagamentis
	* @param end the upper bound of the range of piano pagamentis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of piano pagamentis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.PianoPagamenti> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the piano pagamentis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of piano pagamentis.
	*
	* @return the number of piano pagamentis
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static PianoPagamentiPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (PianoPagamentiPersistence)PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
					PianoPagamentiPersistence.class.getName());

			ReferenceRegistry.registerReference(PianoPagamentiUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(PianoPagamentiPersistence persistence) {
	}

	private static PianoPagamentiPersistence _persistence;
}