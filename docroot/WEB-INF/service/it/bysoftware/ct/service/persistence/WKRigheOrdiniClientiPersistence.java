/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.WKRigheOrdiniClienti;

/**
 * The persistence interface for the w k righe ordini clienti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see WKRigheOrdiniClientiPersistenceImpl
 * @see WKRigheOrdiniClientiUtil
 * @generated
 */
public interface WKRigheOrdiniClientiPersistence extends BasePersistence<WKRigheOrdiniClienti> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link WKRigheOrdiniClientiUtil} to access the w k righe ordini clienti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the w k righe ordini clientis where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @return the matching w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKRigheOrdiniClienti> findByAnnoTipoOrdineNumeroOrdine(
		int anno, int tipoOrdine, int numeroOrdine)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the w k righe ordini clientis where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKRigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param start the lower bound of the range of w k righe ordini clientis
	* @param end the upper bound of the range of w k righe ordini clientis (not inclusive)
	* @return the range of matching w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKRigheOrdiniClienti> findByAnnoTipoOrdineNumeroOrdine(
		int anno, int tipoOrdine, int numeroOrdine, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the w k righe ordini clientis where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKRigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param start the lower bound of the range of w k righe ordini clientis
	* @param end the upper bound of the range of w k righe ordini clientis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKRigheOrdiniClienti> findByAnnoTipoOrdineNumeroOrdine(
		int anno, int tipoOrdine, int numeroOrdine, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first w k righe ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching w k righe ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a matching w k righe ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKRigheOrdiniClienti findByAnnoTipoOrdineNumeroOrdine_First(
		int anno, int tipoOrdine, int numeroOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException;

	/**
	* Returns the first w k righe ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching w k righe ordini clienti, or <code>null</code> if a matching w k righe ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKRigheOrdiniClienti fetchByAnnoTipoOrdineNumeroOrdine_First(
		int anno, int tipoOrdine, int numeroOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last w k righe ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching w k righe ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a matching w k righe ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKRigheOrdiniClienti findByAnnoTipoOrdineNumeroOrdine_Last(
		int anno, int tipoOrdine, int numeroOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException;

	/**
	* Returns the last w k righe ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching w k righe ordini clienti, or <code>null</code> if a matching w k righe ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKRigheOrdiniClienti fetchByAnnoTipoOrdineNumeroOrdine_Last(
		int anno, int tipoOrdine, int numeroOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the w k righe ordini clientis before and after the current w k righe ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param wkRigheOrdiniClientiPK the primary key of the current w k righe ordini clienti
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next w k righe ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a w k righe ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKRigheOrdiniClienti[] findByAnnoTipoOrdineNumeroOrdine_PrevAndNext(
		it.bysoftware.ct.service.persistence.WKRigheOrdiniClientiPK wkRigheOrdiniClientiPK,
		int anno, int tipoOrdine, int numeroOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException;

	/**
	* Removes all the w k righe ordini clientis where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; from the database.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @throws SystemException if a system exception occurred
	*/
	public void removeByAnnoTipoOrdineNumeroOrdine(int anno, int tipoOrdine,
		int numeroOrdine)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of w k righe ordini clientis where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @return the number of matching w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public int countByAnnoTipoOrdineNumeroOrdine(int anno, int tipoOrdine,
		int numeroOrdine)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the w k righe ordini clienti in the entity cache if it is enabled.
	*
	* @param wkRigheOrdiniClienti the w k righe ordini clienti
	*/
	public void cacheResult(
		it.bysoftware.ct.model.WKRigheOrdiniClienti wkRigheOrdiniClienti);

	/**
	* Caches the w k righe ordini clientis in the entity cache if it is enabled.
	*
	* @param wkRigheOrdiniClientis the w k righe ordini clientis
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.WKRigheOrdiniClienti> wkRigheOrdiniClientis);

	/**
	* Creates a new w k righe ordini clienti with the primary key. Does not add the w k righe ordini clienti to the database.
	*
	* @param wkRigheOrdiniClientiPK the primary key for the new w k righe ordini clienti
	* @return the new w k righe ordini clienti
	*/
	public it.bysoftware.ct.model.WKRigheOrdiniClienti create(
		it.bysoftware.ct.service.persistence.WKRigheOrdiniClientiPK wkRigheOrdiniClientiPK);

	/**
	* Removes the w k righe ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param wkRigheOrdiniClientiPK the primary key of the w k righe ordini clienti
	* @return the w k righe ordini clienti that was removed
	* @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a w k righe ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKRigheOrdiniClienti remove(
		it.bysoftware.ct.service.persistence.WKRigheOrdiniClientiPK wkRigheOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException;

	public it.bysoftware.ct.model.WKRigheOrdiniClienti updateImpl(
		it.bysoftware.ct.model.WKRigheOrdiniClienti wkRigheOrdiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the w k righe ordini clienti with the primary key or throws a {@link it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException} if it could not be found.
	*
	* @param wkRigheOrdiniClientiPK the primary key of the w k righe ordini clienti
	* @return the w k righe ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a w k righe ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKRigheOrdiniClienti findByPrimaryKey(
		it.bysoftware.ct.service.persistence.WKRigheOrdiniClientiPK wkRigheOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException;

	/**
	* Returns the w k righe ordini clienti with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param wkRigheOrdiniClientiPK the primary key of the w k righe ordini clienti
	* @return the w k righe ordini clienti, or <code>null</code> if a w k righe ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKRigheOrdiniClienti fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.WKRigheOrdiniClientiPK wkRigheOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the w k righe ordini clientis.
	*
	* @return the w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKRigheOrdiniClienti> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the w k righe ordini clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKRigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of w k righe ordini clientis
	* @param end the upper bound of the range of w k righe ordini clientis (not inclusive)
	* @return the range of w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKRigheOrdiniClienti> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the w k righe ordini clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKRigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of w k righe ordini clientis
	* @param end the upper bound of the range of w k righe ordini clientis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKRigheOrdiniClienti> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the w k righe ordini clientis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of w k righe ordini clientis.
	*
	* @return the number of w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}