/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.DatiCostantiClientiFornitori;

/**
 * The persistence interface for the dati costanti clienti fornitori service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see DatiCostantiClientiFornitoriPersistenceImpl
 * @see DatiCostantiClientiFornitoriUtil
 * @generated
 */
public interface DatiCostantiClientiFornitoriPersistence extends BasePersistence<DatiCostantiClientiFornitori> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DatiCostantiClientiFornitoriUtil} to access the dati costanti clienti fornitori persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the dati costanti clienti fornitori in the entity cache if it is enabled.
	*
	* @param datiCostantiClientiFornitori the dati costanti clienti fornitori
	*/
	public void cacheResult(
		it.bysoftware.ct.model.DatiCostantiClientiFornitori datiCostantiClientiFornitori);

	/**
	* Caches the dati costanti clienti fornitoris in the entity cache if it is enabled.
	*
	* @param datiCostantiClientiFornitoris the dati costanti clienti fornitoris
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.DatiCostantiClientiFornitori> datiCostantiClientiFornitoris);

	/**
	* Creates a new dati costanti clienti fornitori with the primary key. Does not add the dati costanti clienti fornitori to the database.
	*
	* @param datiCostantiClientiFornitoriPK the primary key for the new dati costanti clienti fornitori
	* @return the new dati costanti clienti fornitori
	*/
	public it.bysoftware.ct.model.DatiCostantiClientiFornitori create(
		it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK datiCostantiClientiFornitoriPK);

	/**
	* Removes the dati costanti clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param datiCostantiClientiFornitoriPK the primary key of the dati costanti clienti fornitori
	* @return the dati costanti clienti fornitori that was removed
	* @throws it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException if a dati costanti clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.DatiCostantiClientiFornitori remove(
		it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK datiCostantiClientiFornitoriPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException;

	public it.bysoftware.ct.model.DatiCostantiClientiFornitori updateImpl(
		it.bysoftware.ct.model.DatiCostantiClientiFornitori datiCostantiClientiFornitori)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the dati costanti clienti fornitori with the primary key or throws a {@link it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException} if it could not be found.
	*
	* @param datiCostantiClientiFornitoriPK the primary key of the dati costanti clienti fornitori
	* @return the dati costanti clienti fornitori
	* @throws it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException if a dati costanti clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.DatiCostantiClientiFornitori findByPrimaryKey(
		it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK datiCostantiClientiFornitoriPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException;

	/**
	* Returns the dati costanti clienti fornitori with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param datiCostantiClientiFornitoriPK the primary key of the dati costanti clienti fornitori
	* @return the dati costanti clienti fornitori, or <code>null</code> if a dati costanti clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.DatiCostantiClientiFornitori fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK datiCostantiClientiFornitoriPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the dati costanti clienti fornitoris.
	*
	* @return the dati costanti clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.DatiCostantiClientiFornitori> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the dati costanti clienti fornitoris.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiCostantiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dati costanti clienti fornitoris
	* @param end the upper bound of the range of dati costanti clienti fornitoris (not inclusive)
	* @return the range of dati costanti clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.DatiCostantiClientiFornitori> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the dati costanti clienti fornitoris.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiCostantiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dati costanti clienti fornitoris
	* @param end the upper bound of the range of dati costanti clienti fornitoris (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of dati costanti clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.DatiCostantiClientiFornitori> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the dati costanti clienti fornitoris from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of dati costanti clienti fornitoris.
	*
	* @return the number of dati costanti clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}