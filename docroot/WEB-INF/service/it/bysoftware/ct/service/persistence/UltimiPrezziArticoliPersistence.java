/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.UltimiPrezziArticoli;

/**
 * The persistence interface for the ultimi prezzi articoli service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see UltimiPrezziArticoliPersistenceImpl
 * @see UltimiPrezziArticoliUtil
 * @generated
 */
public interface UltimiPrezziArticoliPersistence extends BasePersistence<UltimiPrezziArticoli> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UltimiPrezziArticoliUtil} to access the ultimi prezzi articoli persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @return the matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioni(
		boolean tipoSoggetto, java.lang.String codiceSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @return the range of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioni(
		boolean tipoSoggetto, java.lang.String codiceSoggetto, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioni(
		boolean tipoSoggetto, java.lang.String codiceSoggetto, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli findByUltimeCondizioni_First(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException;

	/**
	* Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli fetchByUltimeCondizioni_First(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli findByUltimeCondizioni_Last(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException;

	/**
	* Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli fetchByUltimeCondizioni_Last(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ultimi prezzi articolis before and after the current ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* @param ultimiPrezziArticoliPK the primary key of the current ultimi prezzi articoli
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli[] findByUltimeCondizioni_PrevAndNext(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK,
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException;

	/**
	* Removes all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; from the database.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUltimeCondizioni(boolean tipoSoggetto,
		java.lang.String codiceSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @return the number of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public int countByUltimeCondizioni(boolean tipoSoggetto,
		java.lang.String codiceSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @return the matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioniArticolo(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @return the range of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioniArticolo(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioniArticolo(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli findByUltimeCondizioniArticolo_First(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException;

	/**
	* Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli fetchByUltimeCondizioniArticolo_First(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli findByUltimeCondizioniArticolo_Last(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException;

	/**
	* Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli fetchByUltimeCondizioniArticolo_Last(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ultimi prezzi articolis before and after the current ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param ultimiPrezziArticoliPK the primary key of the current ultimi prezzi articoli
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli[] findByUltimeCondizioniArticolo_PrevAndNext(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK,
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException;

	/**
	* Removes all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; from the database.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUltimeCondizioniArticolo(boolean tipoSoggetto,
		java.lang.String codiceSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @return the number of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public int countByUltimeCondizioniArticolo(boolean tipoSoggetto,
		java.lang.String codiceSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @return the matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioniFornitori(
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @return the range of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioniFornitori(
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioniFornitori(
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli findByUltimeCondizioniFornitori_First(
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException;

	/**
	* Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli fetchByUltimeCondizioniFornitori_First(
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli findByUltimeCondizioniFornitori_Last(
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException;

	/**
	* Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli fetchByUltimeCondizioniFornitori_Last(
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ultimi prezzi articolis before and after the current ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param ultimiPrezziArticoliPK the primary key of the current ultimi prezzi articoli
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli[] findByUltimeCondizioniFornitori_PrevAndNext(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK,
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException;

	/**
	* Removes all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; from the database.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUltimeCondizioniFornitori(boolean tipoSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ultimi prezzi articolis where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @return the number of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public int countByUltimeCondizioniFornitori(boolean tipoSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the ultimi prezzi articoli in the entity cache if it is enabled.
	*
	* @param ultimiPrezziArticoli the ultimi prezzi articoli
	*/
	public void cacheResult(
		it.bysoftware.ct.model.UltimiPrezziArticoli ultimiPrezziArticoli);

	/**
	* Caches the ultimi prezzi articolis in the entity cache if it is enabled.
	*
	* @param ultimiPrezziArticolis the ultimi prezzi articolis
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> ultimiPrezziArticolis);

	/**
	* Creates a new ultimi prezzi articoli with the primary key. Does not add the ultimi prezzi articoli to the database.
	*
	* @param ultimiPrezziArticoliPK the primary key for the new ultimi prezzi articoli
	* @return the new ultimi prezzi articoli
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli create(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK);

	/**
	* Removes the ultimi prezzi articoli with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ultimiPrezziArticoliPK the primary key of the ultimi prezzi articoli
	* @return the ultimi prezzi articoli that was removed
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli remove(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException;

	public it.bysoftware.ct.model.UltimiPrezziArticoli updateImpl(
		it.bysoftware.ct.model.UltimiPrezziArticoli ultimiPrezziArticoli)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ultimi prezzi articoli with the primary key or throws a {@link it.bysoftware.ct.NoSuchUltimiPrezziArticoliException} if it could not be found.
	*
	* @param ultimiPrezziArticoliPK the primary key of the ultimi prezzi articoli
	* @return the ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli findByPrimaryKey(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException;

	/**
	* Returns the ultimi prezzi articoli with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ultimiPrezziArticoliPK the primary key of the ultimi prezzi articoli
	* @return the ultimi prezzi articoli, or <code>null</code> if a ultimi prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.UltimiPrezziArticoli fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ultimi prezzi articolis.
	*
	* @return the ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ultimi prezzi articolis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @return the range of ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ultimi prezzi articolis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the ultimi prezzi articolis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ultimi prezzi articolis.
	*
	* @return the number of ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}