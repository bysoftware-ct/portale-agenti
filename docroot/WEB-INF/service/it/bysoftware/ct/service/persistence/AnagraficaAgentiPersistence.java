/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.AnagraficaAgenti;

/**
 * The persistence interface for the anagrafica agenti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see AnagraficaAgentiPersistenceImpl
 * @see AnagraficaAgentiUtil
 * @generated
 */
public interface AnagraficaAgentiPersistence extends BasePersistence<AnagraficaAgenti> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AnagraficaAgentiUtil} to access the anagrafica agenti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the anagrafica agenti in the entity cache if it is enabled.
	*
	* @param anagraficaAgenti the anagrafica agenti
	*/
	public void cacheResult(
		it.bysoftware.ct.model.AnagraficaAgenti anagraficaAgenti);

	/**
	* Caches the anagrafica agentis in the entity cache if it is enabled.
	*
	* @param anagraficaAgentis the anagrafica agentis
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.AnagraficaAgenti> anagraficaAgentis);

	/**
	* Creates a new anagrafica agenti with the primary key. Does not add the anagrafica agenti to the database.
	*
	* @param codiceAgente the primary key for the new anagrafica agenti
	* @return the new anagrafica agenti
	*/
	public it.bysoftware.ct.model.AnagraficaAgenti create(
		java.lang.String codiceAgente);

	/**
	* Removes the anagrafica agenti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param codiceAgente the primary key of the anagrafica agenti
	* @return the anagrafica agenti that was removed
	* @throws it.bysoftware.ct.NoSuchAnagraficaAgentiException if a anagrafica agenti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.AnagraficaAgenti remove(
		java.lang.String codiceAgente)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchAnagraficaAgentiException;

	public it.bysoftware.ct.model.AnagraficaAgenti updateImpl(
		it.bysoftware.ct.model.AnagraficaAgenti anagraficaAgenti)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the anagrafica agenti with the primary key or throws a {@link it.bysoftware.ct.NoSuchAnagraficaAgentiException} if it could not be found.
	*
	* @param codiceAgente the primary key of the anagrafica agenti
	* @return the anagrafica agenti
	* @throws it.bysoftware.ct.NoSuchAnagraficaAgentiException if a anagrafica agenti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.AnagraficaAgenti findByPrimaryKey(
		java.lang.String codiceAgente)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchAnagraficaAgentiException;

	/**
	* Returns the anagrafica agenti with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param codiceAgente the primary key of the anagrafica agenti
	* @return the anagrafica agenti, or <code>null</code> if a anagrafica agenti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.AnagraficaAgenti fetchByPrimaryKey(
		java.lang.String codiceAgente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the anagrafica agentis.
	*
	* @return the anagrafica agentis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.AnagraficaAgenti> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the anagrafica agentis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficaAgentiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of anagrafica agentis
	* @param end the upper bound of the range of anagrafica agentis (not inclusive)
	* @return the range of anagrafica agentis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.AnagraficaAgenti> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the anagrafica agentis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficaAgentiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of anagrafica agentis
	* @param end the upper bound of the range of anagrafica agentis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of anagrafica agentis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.AnagraficaAgenti> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the anagrafica agentis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of anagrafica agentis.
	*
	* @return the number of anagrafica agentis
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}