/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.RigheOrdiniClienti;

/**
 * The persistence interface for the righe ordini clienti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see RigheOrdiniClientiPersistenceImpl
 * @see RigheOrdiniClientiUtil
 * @generated
 */
public interface RigheOrdiniClientiPersistence extends BasePersistence<RigheOrdiniClienti> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link RigheOrdiniClientiUtil} to access the righe ordini clienti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the righe ordini clientis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @return the matching righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.RigheOrdiniClienti> findByTestataOrdine(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, java.lang.String codiceDeposito,
		int tipoOrdine, int numeroOrdine)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the righe ordini clientis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param start the lower bound of the range of righe ordini clientis
	* @param end the upper bound of the range of righe ordini clientis (not inclusive)
	* @return the range of matching righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.RigheOrdiniClienti> findByTestataOrdine(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, java.lang.String codiceDeposito,
		int tipoOrdine, int numeroOrdine, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the righe ordini clientis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param start the lower bound of the range of righe ordini clientis
	* @param end the upper bound of the range of righe ordini clientis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.RigheOrdiniClienti> findByTestataOrdine(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, java.lang.String codiceDeposito,
		int tipoOrdine, int numeroOrdine, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first righe ordini clienti in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching righe ordini clienti
	* @throws it.bysoftware.ct.NoSuchRigheOrdiniClientiException if a matching righe ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheOrdiniClienti findByTestataOrdine_First(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, java.lang.String codiceDeposito,
		int tipoOrdine, int numeroOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchRigheOrdiniClientiException;

	/**
	* Returns the first righe ordini clienti in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching righe ordini clienti, or <code>null</code> if a matching righe ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheOrdiniClienti fetchByTestataOrdine_First(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, java.lang.String codiceDeposito,
		int tipoOrdine, int numeroOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last righe ordini clienti in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching righe ordini clienti
	* @throws it.bysoftware.ct.NoSuchRigheOrdiniClientiException if a matching righe ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheOrdiniClienti findByTestataOrdine_Last(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, java.lang.String codiceDeposito,
		int tipoOrdine, int numeroOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchRigheOrdiniClientiException;

	/**
	* Returns the last righe ordini clienti in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching righe ordini clienti, or <code>null</code> if a matching righe ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheOrdiniClienti fetchByTestataOrdine_Last(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, java.lang.String codiceDeposito,
		int tipoOrdine, int numeroOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the righe ordini clientis before and after the current righe ordini clienti in the ordered set where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param righeOrdiniClientiPK the primary key of the current righe ordini clienti
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next righe ordini clienti
	* @throws it.bysoftware.ct.NoSuchRigheOrdiniClientiException if a righe ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheOrdiniClienti[] findByTestataOrdine_PrevAndNext(
		it.bysoftware.ct.service.persistence.RigheOrdiniClientiPK righeOrdiniClientiPK,
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, java.lang.String codiceDeposito,
		int tipoOrdine, int numeroOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchRigheOrdiniClientiException;

	/**
	* Removes all the righe ordini clientis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; from the database.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @throws SystemException if a system exception occurred
	*/
	public void removeByTestataOrdine(int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of righe ordini clientis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and codiceDeposito = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param codiceDeposito the codice deposito
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @return the number of matching righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public int countByTestataOrdine(int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, java.lang.String codiceDeposito,
		int tipoOrdine, int numeroOrdine)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the righe ordini clienti in the entity cache if it is enabled.
	*
	* @param righeOrdiniClienti the righe ordini clienti
	*/
	public void cacheResult(
		it.bysoftware.ct.model.RigheOrdiniClienti righeOrdiniClienti);

	/**
	* Caches the righe ordini clientis in the entity cache if it is enabled.
	*
	* @param righeOrdiniClientis the righe ordini clientis
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.RigheOrdiniClienti> righeOrdiniClientis);

	/**
	* Creates a new righe ordini clienti with the primary key. Does not add the righe ordini clienti to the database.
	*
	* @param righeOrdiniClientiPK the primary key for the new righe ordini clienti
	* @return the new righe ordini clienti
	*/
	public it.bysoftware.ct.model.RigheOrdiniClienti create(
		it.bysoftware.ct.service.persistence.RigheOrdiniClientiPK righeOrdiniClientiPK);

	/**
	* Removes the righe ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param righeOrdiniClientiPK the primary key of the righe ordini clienti
	* @return the righe ordini clienti that was removed
	* @throws it.bysoftware.ct.NoSuchRigheOrdiniClientiException if a righe ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheOrdiniClienti remove(
		it.bysoftware.ct.service.persistence.RigheOrdiniClientiPK righeOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchRigheOrdiniClientiException;

	public it.bysoftware.ct.model.RigheOrdiniClienti updateImpl(
		it.bysoftware.ct.model.RigheOrdiniClienti righeOrdiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the righe ordini clienti with the primary key or throws a {@link it.bysoftware.ct.NoSuchRigheOrdiniClientiException} if it could not be found.
	*
	* @param righeOrdiniClientiPK the primary key of the righe ordini clienti
	* @return the righe ordini clienti
	* @throws it.bysoftware.ct.NoSuchRigheOrdiniClientiException if a righe ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheOrdiniClienti findByPrimaryKey(
		it.bysoftware.ct.service.persistence.RigheOrdiniClientiPK righeOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchRigheOrdiniClientiException;

	/**
	* Returns the righe ordini clienti with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param righeOrdiniClientiPK the primary key of the righe ordini clienti
	* @return the righe ordini clienti, or <code>null</code> if a righe ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.RigheOrdiniClienti fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.RigheOrdiniClientiPK righeOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the righe ordini clientis.
	*
	* @return the righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.RigheOrdiniClienti> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the righe ordini clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of righe ordini clientis
	* @param end the upper bound of the range of righe ordini clientis (not inclusive)
	* @return the range of righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.RigheOrdiniClienti> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the righe ordini clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of righe ordini clientis
	* @param end the upper bound of the range of righe ordini clientis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.RigheOrdiniClienti> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the righe ordini clientis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of righe ordini clientis.
	*
	* @return the number of righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}