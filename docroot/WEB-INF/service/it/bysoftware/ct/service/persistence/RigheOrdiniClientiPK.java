/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class RigheOrdiniClientiPK implements Comparable<RigheOrdiniClientiPK>,
	Serializable {
	public int anno;
	public String codiceAttivita;
	public String codiceCentro;
	public String codiceDeposito;
	public int tipoOrdine;
	public int numeroOrdine;
	public int numeroRigo;

	public RigheOrdiniClientiPK() {
	}

	public RigheOrdiniClientiPK(int anno, String codiceAttivita,
		String codiceCentro, String codiceDeposito, int tipoOrdine,
		int numeroOrdine, int numeroRigo) {
		this.anno = anno;
		this.codiceAttivita = codiceAttivita;
		this.codiceCentro = codiceCentro;
		this.codiceDeposito = codiceDeposito;
		this.tipoOrdine = tipoOrdine;
		this.numeroOrdine = numeroOrdine;
		this.numeroRigo = numeroRigo;
	}

	public int getAnno() {
		return anno;
	}

	public void setAnno(int anno) {
		this.anno = anno;
	}

	public String getCodiceAttivita() {
		return codiceAttivita;
	}

	public void setCodiceAttivita(String codiceAttivita) {
		this.codiceAttivita = codiceAttivita;
	}

	public String getCodiceCentro() {
		return codiceCentro;
	}

	public void setCodiceCentro(String codiceCentro) {
		this.codiceCentro = codiceCentro;
	}

	public String getCodiceDeposito() {
		return codiceDeposito;
	}

	public void setCodiceDeposito(String codiceDeposito) {
		this.codiceDeposito = codiceDeposito;
	}

	public int getTipoOrdine() {
		return tipoOrdine;
	}

	public void setTipoOrdine(int tipoOrdine) {
		this.tipoOrdine = tipoOrdine;
	}

	public int getNumeroOrdine() {
		return numeroOrdine;
	}

	public void setNumeroOrdine(int numeroOrdine) {
		this.numeroOrdine = numeroOrdine;
	}

	public int getNumeroRigo() {
		return numeroRigo;
	}

	public void setNumeroRigo(int numeroRigo) {
		this.numeroRigo = numeroRigo;
	}

	@Override
	public int compareTo(RigheOrdiniClientiPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (anno < pk.anno) {
			value = -1;
		}
		else if (anno > pk.anno) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		value = codiceAttivita.compareTo(pk.codiceAttivita);

		if (value != 0) {
			return value;
		}

		value = codiceCentro.compareTo(pk.codiceCentro);

		if (value != 0) {
			return value;
		}

		value = codiceDeposito.compareTo(pk.codiceDeposito);

		if (value != 0) {
			return value;
		}

		if (tipoOrdine < pk.tipoOrdine) {
			value = -1;
		}
		else if (tipoOrdine > pk.tipoOrdine) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (numeroOrdine < pk.numeroOrdine) {
			value = -1;
		}
		else if (numeroOrdine > pk.numeroOrdine) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (numeroRigo < pk.numeroRigo) {
			value = -1;
		}
		else if (numeroRigo > pk.numeroRigo) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof RigheOrdiniClientiPK)) {
			return false;
		}

		RigheOrdiniClientiPK pk = (RigheOrdiniClientiPK)obj;

		if ((anno == pk.anno) && (codiceAttivita.equals(pk.codiceAttivita)) &&
				(codiceCentro.equals(pk.codiceCentro)) &&
				(codiceDeposito.equals(pk.codiceDeposito)) &&
				(tipoOrdine == pk.tipoOrdine) &&
				(numeroOrdine == pk.numeroOrdine) &&
				(numeroRigo == pk.numeroRigo)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(anno) + String.valueOf(codiceAttivita) +
		String.valueOf(codiceCentro) + String.valueOf(codiceDeposito) +
		String.valueOf(tipoOrdine) + String.valueOf(numeroOrdine) +
		String.valueOf(numeroRigo)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(35);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("anno");
		sb.append(StringPool.EQUAL);
		sb.append(anno);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codiceAttivita");
		sb.append(StringPool.EQUAL);
		sb.append(codiceAttivita);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codiceCentro");
		sb.append(StringPool.EQUAL);
		sb.append(codiceCentro);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codiceDeposito");
		sb.append(StringPool.EQUAL);
		sb.append(codiceDeposito);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("tipoOrdine");
		sb.append(StringPool.EQUAL);
		sb.append(tipoOrdine);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("numeroOrdine");
		sb.append(StringPool.EQUAL);
		sb.append(numeroOrdine);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("numeroRigo");
		sb.append(StringPool.EQUAL);
		sb.append(numeroRigo);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}