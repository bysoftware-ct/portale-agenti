/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.PianoPagamenti;

/**
 * The persistence interface for the piano pagamenti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see PianoPagamentiPersistenceImpl
 * @see PianoPagamentiUtil
 * @generated
 */
public interface PianoPagamentiPersistence extends BasePersistence<PianoPagamenti> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link PianoPagamentiUtil} to access the piano pagamenti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the piano pagamenti in the entity cache if it is enabled.
	*
	* @param pianoPagamenti the piano pagamenti
	*/
	public void cacheResult(
		it.bysoftware.ct.model.PianoPagamenti pianoPagamenti);

	/**
	* Caches the piano pagamentis in the entity cache if it is enabled.
	*
	* @param pianoPagamentis the piano pagamentis
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.PianoPagamenti> pianoPagamentis);

	/**
	* Creates a new piano pagamenti with the primary key. Does not add the piano pagamenti to the database.
	*
	* @param codicePianoPagamento the primary key for the new piano pagamenti
	* @return the new piano pagamenti
	*/
	public it.bysoftware.ct.model.PianoPagamenti create(
		java.lang.String codicePianoPagamento);

	/**
	* Removes the piano pagamenti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param codicePianoPagamento the primary key of the piano pagamenti
	* @return the piano pagamenti that was removed
	* @throws it.bysoftware.ct.NoSuchPianoPagamentiException if a piano pagamenti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.PianoPagamenti remove(
		java.lang.String codicePianoPagamento)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchPianoPagamentiException;

	public it.bysoftware.ct.model.PianoPagamenti updateImpl(
		it.bysoftware.ct.model.PianoPagamenti pianoPagamenti)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the piano pagamenti with the primary key or throws a {@link it.bysoftware.ct.NoSuchPianoPagamentiException} if it could not be found.
	*
	* @param codicePianoPagamento the primary key of the piano pagamenti
	* @return the piano pagamenti
	* @throws it.bysoftware.ct.NoSuchPianoPagamentiException if a piano pagamenti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.PianoPagamenti findByPrimaryKey(
		java.lang.String codicePianoPagamento)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchPianoPagamentiException;

	/**
	* Returns the piano pagamenti with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param codicePianoPagamento the primary key of the piano pagamenti
	* @return the piano pagamenti, or <code>null</code> if a piano pagamenti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.PianoPagamenti fetchByPrimaryKey(
		java.lang.String codicePianoPagamento)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the piano pagamentis.
	*
	* @return the piano pagamentis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.PianoPagamenti> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the piano pagamentis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianoPagamentiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of piano pagamentis
	* @param end the upper bound of the range of piano pagamentis (not inclusive)
	* @return the range of piano pagamentis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.PianoPagamenti> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the piano pagamentis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.PianoPagamentiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of piano pagamentis
	* @param end the upper bound of the range of piano pagamentis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of piano pagamentis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.PianoPagamenti> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the piano pagamentis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of piano pagamentis.
	*
	* @return the number of piano pagamentis
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}