/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.WKOrdiniClienti;

/**
 * The persistence interface for the w k ordini clienti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see WKOrdiniClientiPersistenceImpl
 * @see WKOrdiniClientiUtil
 * @generated
 */
public interface WKOrdiniClientiPersistence extends BasePersistence<WKOrdiniClienti> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link WKOrdiniClientiUtil} to access the w k ordini clienti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the w k ordini clientis where anno = &#63; and tipoOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @return the matching w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> findByAnnoTipoOrdine(
		int anno, int tipoOrdine)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the w k ordini clientis where anno = &#63; and tipoOrdine = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param start the lower bound of the range of w k ordini clientis
	* @param end the upper bound of the range of w k ordini clientis (not inclusive)
	* @return the range of matching w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> findByAnnoTipoOrdine(
		int anno, int tipoOrdine, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the w k ordini clientis where anno = &#63; and tipoOrdine = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param start the lower bound of the range of w k ordini clientis
	* @param end the upper bound of the range of w k ordini clientis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> findByAnnoTipoOrdine(
		int anno, int tipoOrdine, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first w k ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching w k ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a matching w k ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti findByAnnoTipoOrdine_First(
		int anno, int tipoOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKOrdiniClientiException;

	/**
	* Returns the first w k ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching w k ordini clienti, or <code>null</code> if a matching w k ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti fetchByAnnoTipoOrdine_First(
		int anno, int tipoOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last w k ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching w k ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a matching w k ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti findByAnnoTipoOrdine_Last(
		int anno, int tipoOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKOrdiniClientiException;

	/**
	* Returns the last w k ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching w k ordini clienti, or <code>null</code> if a matching w k ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti fetchByAnnoTipoOrdine_Last(
		int anno, int tipoOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the w k ordini clientis before and after the current w k ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63;.
	*
	* @param wkOrdiniClientiPK the primary key of the current w k ordini clienti
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next w k ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a w k ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti[] findByAnnoTipoOrdine_PrevAndNext(
		it.bysoftware.ct.service.persistence.WKOrdiniClientiPK wkOrdiniClientiPK,
		int anno, int tipoOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKOrdiniClientiException;

	/**
	* Removes all the w k ordini clientis where anno = &#63; and tipoOrdine = &#63; from the database.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @throws SystemException if a system exception occurred
	*/
	public void removeByAnnoTipoOrdine(int anno, int tipoOrdine)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of w k ordini clientis where anno = &#63; and tipoOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @return the number of matching w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public int countByAnnoTipoOrdine(int anno, int tipoOrdine)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the w k ordini clientis where codiceCliente = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @return the matching w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> findByCodiceCliente(
		java.lang.String codiceCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the w k ordini clientis where codiceCliente = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceCliente the codice cliente
	* @param start the lower bound of the range of w k ordini clientis
	* @param end the upper bound of the range of w k ordini clientis (not inclusive)
	* @return the range of matching w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> findByCodiceCliente(
		java.lang.String codiceCliente, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the w k ordini clientis where codiceCliente = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceCliente the codice cliente
	* @param start the lower bound of the range of w k ordini clientis
	* @param end the upper bound of the range of w k ordini clientis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> findByCodiceCliente(
		java.lang.String codiceCliente, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first w k ordini clienti in the ordered set where codiceCliente = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching w k ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a matching w k ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti findByCodiceCliente_First(
		java.lang.String codiceCliente,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKOrdiniClientiException;

	/**
	* Returns the first w k ordini clienti in the ordered set where codiceCliente = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching w k ordini clienti, or <code>null</code> if a matching w k ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti fetchByCodiceCliente_First(
		java.lang.String codiceCliente,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last w k ordini clienti in the ordered set where codiceCliente = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching w k ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a matching w k ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti findByCodiceCliente_Last(
		java.lang.String codiceCliente,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKOrdiniClientiException;

	/**
	* Returns the last w k ordini clienti in the ordered set where codiceCliente = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching w k ordini clienti, or <code>null</code> if a matching w k ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti fetchByCodiceCliente_Last(
		java.lang.String codiceCliente,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the w k ordini clientis before and after the current w k ordini clienti in the ordered set where codiceCliente = &#63;.
	*
	* @param wkOrdiniClientiPK the primary key of the current w k ordini clienti
	* @param codiceCliente the codice cliente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next w k ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a w k ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti[] findByCodiceCliente_PrevAndNext(
		it.bysoftware.ct.service.persistence.WKOrdiniClientiPK wkOrdiniClientiPK,
		java.lang.String codiceCliente,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKOrdiniClientiException;

	/**
	* Removes all the w k ordini clientis where codiceCliente = &#63; from the database.
	*
	* @param codiceCliente the codice cliente
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCodiceCliente(java.lang.String codiceCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of w k ordini clientis where codiceCliente = &#63;.
	*
	* @param codiceCliente the codice cliente
	* @return the number of matching w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public int countByCodiceCliente(java.lang.String codiceCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the w k ordini clientis where statoOrdine = &#63;.
	*
	* @param statoOrdine the stato ordine
	* @return the matching w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> findByStatoOrdine(
		boolean statoOrdine)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the w k ordini clientis where statoOrdine = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param statoOrdine the stato ordine
	* @param start the lower bound of the range of w k ordini clientis
	* @param end the upper bound of the range of w k ordini clientis (not inclusive)
	* @return the range of matching w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> findByStatoOrdine(
		boolean statoOrdine, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the w k ordini clientis where statoOrdine = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param statoOrdine the stato ordine
	* @param start the lower bound of the range of w k ordini clientis
	* @param end the upper bound of the range of w k ordini clientis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> findByStatoOrdine(
		boolean statoOrdine, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first w k ordini clienti in the ordered set where statoOrdine = &#63;.
	*
	* @param statoOrdine the stato ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching w k ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a matching w k ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti findByStatoOrdine_First(
		boolean statoOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKOrdiniClientiException;

	/**
	* Returns the first w k ordini clienti in the ordered set where statoOrdine = &#63;.
	*
	* @param statoOrdine the stato ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching w k ordini clienti, or <code>null</code> if a matching w k ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti fetchByStatoOrdine_First(
		boolean statoOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last w k ordini clienti in the ordered set where statoOrdine = &#63;.
	*
	* @param statoOrdine the stato ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching w k ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a matching w k ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti findByStatoOrdine_Last(
		boolean statoOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKOrdiniClientiException;

	/**
	* Returns the last w k ordini clienti in the ordered set where statoOrdine = &#63;.
	*
	* @param statoOrdine the stato ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching w k ordini clienti, or <code>null</code> if a matching w k ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti fetchByStatoOrdine_Last(
		boolean statoOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the w k ordini clientis before and after the current w k ordini clienti in the ordered set where statoOrdine = &#63;.
	*
	* @param wkOrdiniClientiPK the primary key of the current w k ordini clienti
	* @param statoOrdine the stato ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next w k ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a w k ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti[] findByStatoOrdine_PrevAndNext(
		it.bysoftware.ct.service.persistence.WKOrdiniClientiPK wkOrdiniClientiPK,
		boolean statoOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKOrdiniClientiException;

	/**
	* Removes all the w k ordini clientis where statoOrdine = &#63; from the database.
	*
	* @param statoOrdine the stato ordine
	* @throws SystemException if a system exception occurred
	*/
	public void removeByStatoOrdine(boolean statoOrdine)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of w k ordini clientis where statoOrdine = &#63;.
	*
	* @param statoOrdine the stato ordine
	* @return the number of matching w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public int countByStatoOrdine(boolean statoOrdine)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the w k ordini clienti in the entity cache if it is enabled.
	*
	* @param wkOrdiniClienti the w k ordini clienti
	*/
	public void cacheResult(
		it.bysoftware.ct.model.WKOrdiniClienti wkOrdiniClienti);

	/**
	* Caches the w k ordini clientis in the entity cache if it is enabled.
	*
	* @param wkOrdiniClientis the w k ordini clientis
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> wkOrdiniClientis);

	/**
	* Creates a new w k ordini clienti with the primary key. Does not add the w k ordini clienti to the database.
	*
	* @param wkOrdiniClientiPK the primary key for the new w k ordini clienti
	* @return the new w k ordini clienti
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti create(
		it.bysoftware.ct.service.persistence.WKOrdiniClientiPK wkOrdiniClientiPK);

	/**
	* Removes the w k ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param wkOrdiniClientiPK the primary key of the w k ordini clienti
	* @return the w k ordini clienti that was removed
	* @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a w k ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti remove(
		it.bysoftware.ct.service.persistence.WKOrdiniClientiPK wkOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKOrdiniClientiException;

	public it.bysoftware.ct.model.WKOrdiniClienti updateImpl(
		it.bysoftware.ct.model.WKOrdiniClienti wkOrdiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the w k ordini clienti with the primary key or throws a {@link it.bysoftware.ct.NoSuchWKOrdiniClientiException} if it could not be found.
	*
	* @param wkOrdiniClientiPK the primary key of the w k ordini clienti
	* @return the w k ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKOrdiniClientiException if a w k ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti findByPrimaryKey(
		it.bysoftware.ct.service.persistence.WKOrdiniClientiPK wkOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKOrdiniClientiException;

	/**
	* Returns the w k ordini clienti with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param wkOrdiniClientiPK the primary key of the w k ordini clienti
	* @return the w k ordini clienti, or <code>null</code> if a w k ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.WKOrdiniClienti fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.WKOrdiniClientiPK wkOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the w k ordini clientis.
	*
	* @return the w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the w k ordini clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of w k ordini clientis
	* @param end the upper bound of the range of w k ordini clientis (not inclusive)
	* @return the range of w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the w k ordini clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of w k ordini clientis
	* @param end the upper bound of the range of w k ordini clientis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the w k ordini clientis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of w k ordini clientis.
	*
	* @return the number of w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}