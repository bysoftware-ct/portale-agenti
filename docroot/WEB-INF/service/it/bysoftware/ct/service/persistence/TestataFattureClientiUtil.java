/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.TestataFattureClienti;

import java.util.List;

/**
 * The persistence utility for the testata fatture clienti service. This utility wraps {@link TestataFattureClientiPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see TestataFattureClientiPersistence
 * @see TestataFattureClientiPersistenceImpl
 * @generated
 */
public class TestataFattureClientiUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(TestataFattureClienti testataFattureClienti) {
		getPersistence().clearCache(testataFattureClienti);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<TestataFattureClienti> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<TestataFattureClienti> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<TestataFattureClienti> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static TestataFattureClienti update(
		TestataFattureClienti testataFattureClienti) throws SystemException {
		return getPersistence().update(testataFattureClienti);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static TestataFattureClienti update(
		TestataFattureClienti testataFattureClienti,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(testataFattureClienti, serviceContext);
	}

	/**
	* Returns the testata fatture clienti where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroDocumento = &#63; and dataDocumento = &#63; and tipoSoggetto = &#63; and codiceCliente = &#63; or throws a {@link it.bysoftware.ct.NoSuchTestataFattureClientiException} if it could not be found.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroDocumento the numero documento
	* @param dataDocumento the data documento
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @return the matching testata fatture clienti
	* @throws it.bysoftware.ct.NoSuchTestataFattureClientiException if a matching testata fatture clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.TestataFattureClienti findByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroDocumento,
		java.util.Date dataDocumento, boolean tipoSoggetto,
		java.lang.String codiceCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchTestataFattureClientiException {
		return getPersistence()
				   .findByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(anno,
			codiceAttivita, codiceCentro, numeroDocumento, dataDocumento,
			tipoSoggetto, codiceCliente);
	}

	/**
	* Returns the testata fatture clienti where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroDocumento = &#63; and dataDocumento = &#63; and tipoSoggetto = &#63; and codiceCliente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroDocumento the numero documento
	* @param dataDocumento the data documento
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @return the matching testata fatture clienti, or <code>null</code> if a matching testata fatture clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.TestataFattureClienti fetchByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroDocumento,
		java.util.Date dataDocumento, boolean tipoSoggetto,
		java.lang.String codiceCliente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(anno,
			codiceAttivita, codiceCentro, numeroDocumento, dataDocumento,
			tipoSoggetto, codiceCliente);
	}

	/**
	* Returns the testata fatture clienti where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroDocumento = &#63; and dataDocumento = &#63; and tipoSoggetto = &#63; and codiceCliente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroDocumento the numero documento
	* @param dataDocumento the data documento
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching testata fatture clienti, or <code>null</code> if a matching testata fatture clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.TestataFattureClienti fetchByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroDocumento,
		java.util.Date dataDocumento, boolean tipoSoggetto,
		java.lang.String codiceCliente, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(anno,
			codiceAttivita, codiceCentro, numeroDocumento, dataDocumento,
			tipoSoggetto, codiceCliente, retrieveFromCache);
	}

	/**
	* Removes the testata fatture clienti where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroDocumento = &#63; and dataDocumento = &#63; and tipoSoggetto = &#63; and codiceCliente = &#63; from the database.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroDocumento the numero documento
	* @param dataDocumento the data documento
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @return the testata fatture clienti that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.TestataFattureClienti removeByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroDocumento,
		java.util.Date dataDocumento, boolean tipoSoggetto,
		java.lang.String codiceCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchTestataFattureClientiException {
		return getPersistence()
				   .removeByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(anno,
			codiceAttivita, codiceCentro, numeroDocumento, dataDocumento,
			tipoSoggetto, codiceCliente);
	}

	/**
	* Returns the number of testata fatture clientis where anno = &#63; and codiceAttivita = &#63; and codiceCentro = &#63; and numeroDocumento = &#63; and dataDocumento = &#63; and tipoSoggetto = &#63; and codiceCliente = &#63;.
	*
	* @param anno the anno
	* @param codiceAttivita the codice attivita
	* @param codiceCentro the codice centro
	* @param numeroDocumento the numero documento
	* @param dataDocumento the data documento
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @return the number of matching testata fatture clientis
	* @throws SystemException if a system exception occurred
	*/
	public static int countByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroDocumento,
		java.util.Date dataDocumento, boolean tipoSoggetto,
		java.lang.String codiceCliente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByAnnoCodiceAttivitaCodiceCentroNumeroDocumento(anno,
			codiceAttivita, codiceCentro, numeroDocumento, dataDocumento,
			tipoSoggetto, codiceCliente);
	}

	/**
	* Caches the testata fatture clienti in the entity cache if it is enabled.
	*
	* @param testataFattureClienti the testata fatture clienti
	*/
	public static void cacheResult(
		it.bysoftware.ct.model.TestataFattureClienti testataFattureClienti) {
		getPersistence().cacheResult(testataFattureClienti);
	}

	/**
	* Caches the testata fatture clientis in the entity cache if it is enabled.
	*
	* @param testataFattureClientis the testata fatture clientis
	*/
	public static void cacheResult(
		java.util.List<it.bysoftware.ct.model.TestataFattureClienti> testataFattureClientis) {
		getPersistence().cacheResult(testataFattureClientis);
	}

	/**
	* Creates a new testata fatture clienti with the primary key. Does not add the testata fatture clienti to the database.
	*
	* @param testataFattureClientiPK the primary key for the new testata fatture clienti
	* @return the new testata fatture clienti
	*/
	public static it.bysoftware.ct.model.TestataFattureClienti create(
		it.bysoftware.ct.service.persistence.TestataFattureClientiPK testataFattureClientiPK) {
		return getPersistence().create(testataFattureClientiPK);
	}

	/**
	* Removes the testata fatture clienti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param testataFattureClientiPK the primary key of the testata fatture clienti
	* @return the testata fatture clienti that was removed
	* @throws it.bysoftware.ct.NoSuchTestataFattureClientiException if a testata fatture clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.TestataFattureClienti remove(
		it.bysoftware.ct.service.persistence.TestataFattureClientiPK testataFattureClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchTestataFattureClientiException {
		return getPersistence().remove(testataFattureClientiPK);
	}

	public static it.bysoftware.ct.model.TestataFattureClienti updateImpl(
		it.bysoftware.ct.model.TestataFattureClienti testataFattureClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(testataFattureClienti);
	}

	/**
	* Returns the testata fatture clienti with the primary key or throws a {@link it.bysoftware.ct.NoSuchTestataFattureClientiException} if it could not be found.
	*
	* @param testataFattureClientiPK the primary key of the testata fatture clienti
	* @return the testata fatture clienti
	* @throws it.bysoftware.ct.NoSuchTestataFattureClientiException if a testata fatture clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.TestataFattureClienti findByPrimaryKey(
		it.bysoftware.ct.service.persistence.TestataFattureClientiPK testataFattureClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchTestataFattureClientiException {
		return getPersistence().findByPrimaryKey(testataFattureClientiPK);
	}

	/**
	* Returns the testata fatture clienti with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param testataFattureClientiPK the primary key of the testata fatture clienti
	* @return the testata fatture clienti, or <code>null</code> if a testata fatture clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.TestataFattureClienti fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.TestataFattureClientiPK testataFattureClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(testataFattureClientiPK);
	}

	/**
	* Returns all the testata fatture clientis.
	*
	* @return the testata fatture clientis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.TestataFattureClienti> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the testata fatture clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataFattureClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of testata fatture clientis
	* @param end the upper bound of the range of testata fatture clientis (not inclusive)
	* @return the range of testata fatture clientis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.TestataFattureClienti> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the testata fatture clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataFattureClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of testata fatture clientis
	* @param end the upper bound of the range of testata fatture clientis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of testata fatture clientis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.TestataFattureClienti> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the testata fatture clientis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of testata fatture clientis.
	*
	* @return the number of testata fatture clientis
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static TestataFattureClientiPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (TestataFattureClientiPersistence)PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
					TestataFattureClientiPersistence.class.getName());

			ReferenceRegistry.registerReference(TestataFattureClientiUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(TestataFattureClientiPersistence persistence) {
	}

	private static TestataFattureClientiPersistence _persistence;
}