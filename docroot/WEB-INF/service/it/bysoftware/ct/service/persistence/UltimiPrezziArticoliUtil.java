/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.UltimiPrezziArticoli;

import java.util.List;

/**
 * The persistence utility for the ultimi prezzi articoli service. This utility wraps {@link UltimiPrezziArticoliPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see UltimiPrezziArticoliPersistence
 * @see UltimiPrezziArticoliPersistenceImpl
 * @generated
 */
public class UltimiPrezziArticoliUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(UltimiPrezziArticoli ultimiPrezziArticoli) {
		getPersistence().clearCache(ultimiPrezziArticoli);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<UltimiPrezziArticoli> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<UltimiPrezziArticoli> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<UltimiPrezziArticoli> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static UltimiPrezziArticoli update(
		UltimiPrezziArticoli ultimiPrezziArticoli) throws SystemException {
		return getPersistence().update(ultimiPrezziArticoli);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static UltimiPrezziArticoli update(
		UltimiPrezziArticoli ultimiPrezziArticoli, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(ultimiPrezziArticoli, serviceContext);
	}

	/**
	* Returns all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @return the matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioni(
		boolean tipoSoggetto, java.lang.String codiceSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUltimeCondizioni(tipoSoggetto, codiceSoggetto);
	}

	/**
	* Returns a range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @return the range of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioni(
		boolean tipoSoggetto, java.lang.String codiceSoggetto, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUltimeCondizioni(tipoSoggetto, codiceSoggetto, start,
			end);
	}

	/**
	* Returns an ordered range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioni(
		boolean tipoSoggetto, java.lang.String codiceSoggetto, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUltimeCondizioni(tipoSoggetto, codiceSoggetto, start,
			end, orderByComparator);
	}

	/**
	* Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli findByUltimeCondizioni_First(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException {
		return getPersistence()
				   .findByUltimeCondizioni_First(tipoSoggetto, codiceSoggetto,
			orderByComparator);
	}

	/**
	* Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli fetchByUltimeCondizioni_First(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUltimeCondizioni_First(tipoSoggetto, codiceSoggetto,
			orderByComparator);
	}

	/**
	* Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli findByUltimeCondizioni_Last(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException {
		return getPersistence()
				   .findByUltimeCondizioni_Last(tipoSoggetto, codiceSoggetto,
			orderByComparator);
	}

	/**
	* Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli fetchByUltimeCondizioni_Last(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUltimeCondizioni_Last(tipoSoggetto, codiceSoggetto,
			orderByComparator);
	}

	/**
	* Returns the ultimi prezzi articolis before and after the current ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* @param ultimiPrezziArticoliPK the primary key of the current ultimi prezzi articoli
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli[] findByUltimeCondizioni_PrevAndNext(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK,
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException {
		return getPersistence()
				   .findByUltimeCondizioni_PrevAndNext(ultimiPrezziArticoliPK,
			tipoSoggetto, codiceSoggetto, orderByComparator);
	}

	/**
	* Removes all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; from the database.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUltimeCondizioni(boolean tipoSoggetto,
		java.lang.String codiceSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUltimeCondizioni(tipoSoggetto, codiceSoggetto);
	}

	/**
	* Returns the number of ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @return the number of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUltimeCondizioni(boolean tipoSoggetto,
		java.lang.String codiceSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByUltimeCondizioni(tipoSoggetto, codiceSoggetto);
	}

	/**
	* Returns all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @return the matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioniArticolo(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUltimeCondizioniArticolo(tipoSoggetto,
			codiceSoggetto, codiceArticolo, codiceVariante);
	}

	/**
	* Returns a range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @return the range of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioniArticolo(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUltimeCondizioniArticolo(tipoSoggetto,
			codiceSoggetto, codiceArticolo, codiceVariante, start, end);
	}

	/**
	* Returns an ordered range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioniArticolo(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUltimeCondizioniArticolo(tipoSoggetto,
			codiceSoggetto, codiceArticolo, codiceVariante, start, end,
			orderByComparator);
	}

	/**
	* Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli findByUltimeCondizioniArticolo_First(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException {
		return getPersistence()
				   .findByUltimeCondizioniArticolo_First(tipoSoggetto,
			codiceSoggetto, codiceArticolo, codiceVariante, orderByComparator);
	}

	/**
	* Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli fetchByUltimeCondizioniArticolo_First(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUltimeCondizioniArticolo_First(tipoSoggetto,
			codiceSoggetto, codiceArticolo, codiceVariante, orderByComparator);
	}

	/**
	* Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli findByUltimeCondizioniArticolo_Last(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException {
		return getPersistence()
				   .findByUltimeCondizioniArticolo_Last(tipoSoggetto,
			codiceSoggetto, codiceArticolo, codiceVariante, orderByComparator);
	}

	/**
	* Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli fetchByUltimeCondizioniArticolo_Last(
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUltimeCondizioniArticolo_Last(tipoSoggetto,
			codiceSoggetto, codiceArticolo, codiceVariante, orderByComparator);
	}

	/**
	* Returns the ultimi prezzi articolis before and after the current ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param ultimiPrezziArticoliPK the primary key of the current ultimi prezzi articoli
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli[] findByUltimeCondizioniArticolo_PrevAndNext(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK,
		boolean tipoSoggetto, java.lang.String codiceSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException {
		return getPersistence()
				   .findByUltimeCondizioniArticolo_PrevAndNext(ultimiPrezziArticoliPK,
			tipoSoggetto, codiceSoggetto, codiceArticolo, codiceVariante,
			orderByComparator);
	}

	/**
	* Removes all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; from the database.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUltimeCondizioniArticolo(boolean tipoSoggetto,
		java.lang.String codiceSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByUltimeCondizioniArticolo(tipoSoggetto, codiceSoggetto,
			codiceArticolo, codiceVariante);
	}

	/**
	* Returns the number of ultimi prezzi articolis where tipoSoggetto = &#63; and codiceSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceSoggetto the codice soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @return the number of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUltimeCondizioniArticolo(boolean tipoSoggetto,
		java.lang.String codiceSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByUltimeCondizioniArticolo(tipoSoggetto,
			codiceSoggetto, codiceArticolo, codiceVariante);
	}

	/**
	* Returns all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @return the matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioniFornitori(
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUltimeCondizioniFornitori(tipoSoggetto,
			codiceArticolo, codiceVariante);
	}

	/**
	* Returns a range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @return the range of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioniFornitori(
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUltimeCondizioniFornitori(tipoSoggetto,
			codiceArticolo, codiceVariante, start, end);
	}

	/**
	* Returns an ordered range of all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findByUltimeCondizioniFornitori(
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUltimeCondizioniFornitori(tipoSoggetto,
			codiceArticolo, codiceVariante, start, end, orderByComparator);
	}

	/**
	* Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli findByUltimeCondizioniFornitori_First(
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException {
		return getPersistence()
				   .findByUltimeCondizioniFornitori_First(tipoSoggetto,
			codiceArticolo, codiceVariante, orderByComparator);
	}

	/**
	* Returns the first ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli fetchByUltimeCondizioniFornitori_First(
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUltimeCondizioniFornitori_First(tipoSoggetto,
			codiceArticolo, codiceVariante, orderByComparator);
	}

	/**
	* Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli findByUltimeCondizioniFornitori_Last(
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException {
		return getPersistence()
				   .findByUltimeCondizioniFornitori_Last(tipoSoggetto,
			codiceArticolo, codiceVariante, orderByComparator);
	}

	/**
	* Returns the last ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ultimi prezzi articoli, or <code>null</code> if a matching ultimi prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli fetchByUltimeCondizioniFornitori_Last(
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUltimeCondizioniFornitori_Last(tipoSoggetto,
			codiceArticolo, codiceVariante, orderByComparator);
	}

	/**
	* Returns the ultimi prezzi articolis before and after the current ultimi prezzi articoli in the ordered set where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param ultimiPrezziArticoliPK the primary key of the current ultimi prezzi articoli
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli[] findByUltimeCondizioniFornitori_PrevAndNext(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK,
		boolean tipoSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException {
		return getPersistence()
				   .findByUltimeCondizioniFornitori_PrevAndNext(ultimiPrezziArticoliPK,
			tipoSoggetto, codiceArticolo, codiceVariante, orderByComparator);
	}

	/**
	* Removes all the ultimi prezzi articolis where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; from the database.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUltimeCondizioniFornitori(boolean tipoSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByUltimeCondizioniFornitori(tipoSoggetto, codiceArticolo,
			codiceVariante);
	}

	/**
	* Returns the number of ultimi prezzi articolis where tipoSoggetto = &#63; and codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @return the number of matching ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUltimeCondizioniFornitori(boolean tipoSoggetto,
		java.lang.String codiceArticolo, java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByUltimeCondizioniFornitori(tipoSoggetto,
			codiceArticolo, codiceVariante);
	}

	/**
	* Caches the ultimi prezzi articoli in the entity cache if it is enabled.
	*
	* @param ultimiPrezziArticoli the ultimi prezzi articoli
	*/
	public static void cacheResult(
		it.bysoftware.ct.model.UltimiPrezziArticoli ultimiPrezziArticoli) {
		getPersistence().cacheResult(ultimiPrezziArticoli);
	}

	/**
	* Caches the ultimi prezzi articolis in the entity cache if it is enabled.
	*
	* @param ultimiPrezziArticolis the ultimi prezzi articolis
	*/
	public static void cacheResult(
		java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> ultimiPrezziArticolis) {
		getPersistence().cacheResult(ultimiPrezziArticolis);
	}

	/**
	* Creates a new ultimi prezzi articoli with the primary key. Does not add the ultimi prezzi articoli to the database.
	*
	* @param ultimiPrezziArticoliPK the primary key for the new ultimi prezzi articoli
	* @return the new ultimi prezzi articoli
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli create(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK) {
		return getPersistence().create(ultimiPrezziArticoliPK);
	}

	/**
	* Removes the ultimi prezzi articoli with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ultimiPrezziArticoliPK the primary key of the ultimi prezzi articoli
	* @return the ultimi prezzi articoli that was removed
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli remove(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException {
		return getPersistence().remove(ultimiPrezziArticoliPK);
	}

	public static it.bysoftware.ct.model.UltimiPrezziArticoli updateImpl(
		it.bysoftware.ct.model.UltimiPrezziArticoli ultimiPrezziArticoli)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(ultimiPrezziArticoli);
	}

	/**
	* Returns the ultimi prezzi articoli with the primary key or throws a {@link it.bysoftware.ct.NoSuchUltimiPrezziArticoliException} if it could not be found.
	*
	* @param ultimiPrezziArticoliPK the primary key of the ultimi prezzi articoli
	* @return the ultimi prezzi articoli
	* @throws it.bysoftware.ct.NoSuchUltimiPrezziArticoliException if a ultimi prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli findByPrimaryKey(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchUltimiPrezziArticoliException {
		return getPersistence().findByPrimaryKey(ultimiPrezziArticoliPK);
	}

	/**
	* Returns the ultimi prezzi articoli with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ultimiPrezziArticoliPK the primary key of the ultimi prezzi articoli
	* @return the ultimi prezzi articoli, or <code>null</code> if a ultimi prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.UltimiPrezziArticoli fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(ultimiPrezziArticoliPK);
	}

	/**
	* Returns all the ultimi prezzi articolis.
	*
	* @return the ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the ultimi prezzi articolis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @return the range of ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the ultimi prezzi articolis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the ultimi prezzi articolis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of ultimi prezzi articolis.
	*
	* @return the number of ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static UltimiPrezziArticoliPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (UltimiPrezziArticoliPersistence)PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
					UltimiPrezziArticoliPersistence.class.getName());

			ReferenceRegistry.registerReference(UltimiPrezziArticoliUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(UltimiPrezziArticoliPersistence persistence) {
	}

	private static UltimiPrezziArticoliPersistence _persistence;
}