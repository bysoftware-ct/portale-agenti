/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.EstrattoConto;

import java.util.List;

/**
 * The persistence utility for the estratto conto service. This utility wraps {@link EstrattoContoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see EstrattoContoPersistence
 * @see EstrattoContoPersistenceImpl
 * @generated
 */
public class EstrattoContoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(EstrattoConto estrattoConto) {
		getPersistence().clearCache(estrattoConto);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<EstrattoConto> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<EstrattoConto> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<EstrattoConto> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static EstrattoConto update(EstrattoConto estrattoConto)
		throws SystemException {
		return getPersistence().update(estrattoConto);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static EstrattoConto update(EstrattoConto estrattoConto,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(estrattoConto, serviceContext);
	}

	/**
	* Returns all the estratto contos where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @return the matching estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.EstrattoConto> findByTipoCodiceSogettoDataChiusa(
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByTipoCodiceSogettoDataChiusa(tipoSoggetto,
			codiceCliente, stato, dataScadenza);
	}

	/**
	* Returns a range of all the estratto contos where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @param start the lower bound of the range of estratto contos
	* @param end the upper bound of the range of estratto contos (not inclusive)
	* @return the range of matching estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.EstrattoConto> findByTipoCodiceSogettoDataChiusa(
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByTipoCodiceSogettoDataChiusa(tipoSoggetto,
			codiceCliente, stato, dataScadenza, start, end);
	}

	/**
	* Returns an ordered range of all the estratto contos where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @param start the lower bound of the range of estratto contos
	* @param end the upper bound of the range of estratto contos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.EstrattoConto> findByTipoCodiceSogettoDataChiusa(
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByTipoCodiceSogettoDataChiusa(tipoSoggetto,
			codiceCliente, stato, dataScadenza, start, end, orderByComparator);
	}

	/**
	* Returns the first estratto conto in the ordered set where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching estratto conto
	* @throws it.bysoftware.ct.NoSuchEstrattoContoException if a matching estratto conto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EstrattoConto findByTipoCodiceSogettoDataChiusa_First(
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEstrattoContoException {
		return getPersistence()
				   .findByTipoCodiceSogettoDataChiusa_First(tipoSoggetto,
			codiceCliente, stato, dataScadenza, orderByComparator);
	}

	/**
	* Returns the first estratto conto in the ordered set where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching estratto conto, or <code>null</code> if a matching estratto conto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EstrattoConto fetchByTipoCodiceSogettoDataChiusa_First(
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByTipoCodiceSogettoDataChiusa_First(tipoSoggetto,
			codiceCliente, stato, dataScadenza, orderByComparator);
	}

	/**
	* Returns the last estratto conto in the ordered set where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching estratto conto
	* @throws it.bysoftware.ct.NoSuchEstrattoContoException if a matching estratto conto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EstrattoConto findByTipoCodiceSogettoDataChiusa_Last(
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEstrattoContoException {
		return getPersistence()
				   .findByTipoCodiceSogettoDataChiusa_Last(tipoSoggetto,
			codiceCliente, stato, dataScadenza, orderByComparator);
	}

	/**
	* Returns the last estratto conto in the ordered set where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching estratto conto, or <code>null</code> if a matching estratto conto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EstrattoConto fetchByTipoCodiceSogettoDataChiusa_Last(
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByTipoCodiceSogettoDataChiusa_Last(tipoSoggetto,
			codiceCliente, stato, dataScadenza, orderByComparator);
	}

	/**
	* Returns the estratto contos before and after the current estratto conto in the ordered set where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* @param estrattoContoPK the primary key of the current estratto conto
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next estratto conto
	* @throws it.bysoftware.ct.NoSuchEstrattoContoException if a estratto conto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EstrattoConto[] findByTipoCodiceSogettoDataChiusa_PrevAndNext(
		it.bysoftware.ct.service.persistence.EstrattoContoPK estrattoContoPK,
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEstrattoContoException {
		return getPersistence()
				   .findByTipoCodiceSogettoDataChiusa_PrevAndNext(estrattoContoPK,
			tipoSoggetto, codiceCliente, stato, dataScadenza, orderByComparator);
	}

	/**
	* Removes all the estratto contos where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63; from the database.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByTipoCodiceSogettoDataChiusa(
		boolean tipoSoggetto, java.lang.String codiceCliente, int stato,
		java.util.Date dataScadenza)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByTipoCodiceSogettoDataChiusa(tipoSoggetto, codiceCliente,
			stato, dataScadenza);
	}

	/**
	* Returns the number of estratto contos where tipoSoggetto = &#63; and codiceCliente = &#63; and stato = &#63; and dataScadenza &lt; &#63;.
	*
	* @param tipoSoggetto the tipo soggetto
	* @param codiceCliente the codice cliente
	* @param stato the stato
	* @param dataScadenza the data scadenza
	* @return the number of matching estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public static int countByTipoCodiceSogettoDataChiusa(boolean tipoSoggetto,
		java.lang.String codiceCliente, int stato, java.util.Date dataScadenza)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByTipoCodiceSogettoDataChiusa(tipoSoggetto,
			codiceCliente, stato, dataScadenza);
	}

	/**
	* Caches the estratto conto in the entity cache if it is enabled.
	*
	* @param estrattoConto the estratto conto
	*/
	public static void cacheResult(
		it.bysoftware.ct.model.EstrattoConto estrattoConto) {
		getPersistence().cacheResult(estrattoConto);
	}

	/**
	* Caches the estratto contos in the entity cache if it is enabled.
	*
	* @param estrattoContos the estratto contos
	*/
	public static void cacheResult(
		java.util.List<it.bysoftware.ct.model.EstrattoConto> estrattoContos) {
		getPersistence().cacheResult(estrattoContos);
	}

	/**
	* Creates a new estratto conto with the primary key. Does not add the estratto conto to the database.
	*
	* @param estrattoContoPK the primary key for the new estratto conto
	* @return the new estratto conto
	*/
	public static it.bysoftware.ct.model.EstrattoConto create(
		it.bysoftware.ct.service.persistence.EstrattoContoPK estrattoContoPK) {
		return getPersistence().create(estrattoContoPK);
	}

	/**
	* Removes the estratto conto with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param estrattoContoPK the primary key of the estratto conto
	* @return the estratto conto that was removed
	* @throws it.bysoftware.ct.NoSuchEstrattoContoException if a estratto conto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EstrattoConto remove(
		it.bysoftware.ct.service.persistence.EstrattoContoPK estrattoContoPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEstrattoContoException {
		return getPersistence().remove(estrattoContoPK);
	}

	public static it.bysoftware.ct.model.EstrattoConto updateImpl(
		it.bysoftware.ct.model.EstrattoConto estrattoConto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(estrattoConto);
	}

	/**
	* Returns the estratto conto with the primary key or throws a {@link it.bysoftware.ct.NoSuchEstrattoContoException} if it could not be found.
	*
	* @param estrattoContoPK the primary key of the estratto conto
	* @return the estratto conto
	* @throws it.bysoftware.ct.NoSuchEstrattoContoException if a estratto conto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EstrattoConto findByPrimaryKey(
		it.bysoftware.ct.service.persistence.EstrattoContoPK estrattoContoPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchEstrattoContoException {
		return getPersistence().findByPrimaryKey(estrattoContoPK);
	}

	/**
	* Returns the estratto conto with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param estrattoContoPK the primary key of the estratto conto
	* @return the estratto conto, or <code>null</code> if a estratto conto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EstrattoConto fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.EstrattoContoPK estrattoContoPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(estrattoContoPK);
	}

	/**
	* Returns all the estratto contos.
	*
	* @return the estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.EstrattoConto> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the estratto contos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of estratto contos
	* @param end the upper bound of the range of estratto contos (not inclusive)
	* @return the range of estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.EstrattoConto> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the estratto contos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EstrattoContoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of estratto contos
	* @param end the upper bound of the range of estratto contos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.EstrattoConto> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the estratto contos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of estratto contos.
	*
	* @return the number of estratto contos
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static EstrattoContoPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (EstrattoContoPersistence)PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
					EstrattoContoPersistence.class.getName());

			ReferenceRegistry.registerReference(EstrattoContoUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(EstrattoContoPersistence persistence) {
	}

	private static EstrattoContoPersistence _persistence;
}