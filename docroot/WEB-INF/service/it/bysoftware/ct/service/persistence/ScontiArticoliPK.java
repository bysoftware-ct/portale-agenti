/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class ScontiArticoliPK implements Comparable<ScontiArticoliPK>,
	Serializable {
	public String codScontoCliente;
	public String codScontoArticolo;

	public ScontiArticoliPK() {
	}

	public ScontiArticoliPK(String codScontoCliente, String codScontoArticolo) {
		this.codScontoCliente = codScontoCliente;
		this.codScontoArticolo = codScontoArticolo;
	}

	public String getCodScontoCliente() {
		return codScontoCliente;
	}

	public void setCodScontoCliente(String codScontoCliente) {
		this.codScontoCliente = codScontoCliente;
	}

	public String getCodScontoArticolo() {
		return codScontoArticolo;
	}

	public void setCodScontoArticolo(String codScontoArticolo) {
		this.codScontoArticolo = codScontoArticolo;
	}

	@Override
	public int compareTo(ScontiArticoliPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		value = codScontoCliente.compareTo(pk.codScontoCliente);

		if (value != 0) {
			return value;
		}

		value = codScontoArticolo.compareTo(pk.codScontoArticolo);

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ScontiArticoliPK)) {
			return false;
		}

		ScontiArticoliPK pk = (ScontiArticoliPK)obj;

		if ((codScontoCliente.equals(pk.codScontoCliente)) &&
				(codScontoArticolo.equals(pk.codScontoArticolo))) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(codScontoCliente) +
		String.valueOf(codScontoArticolo)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("codScontoCliente");
		sb.append(StringPool.EQUAL);
		sb.append(codScontoCliente);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codScontoArticolo");
		sb.append(StringPool.EQUAL);
		sb.append(codScontoArticolo);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}