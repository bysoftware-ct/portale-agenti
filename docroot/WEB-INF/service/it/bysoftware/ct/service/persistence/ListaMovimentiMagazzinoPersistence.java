/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.ListaMovimentiMagazzino;

/**
 * The persistence interface for the lista movimenti magazzino service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ListaMovimentiMagazzinoPersistenceImpl
 * @see ListaMovimentiMagazzinoUtil
 * @generated
 */
public interface ListaMovimentiMagazzinoPersistence extends BasePersistence<ListaMovimentiMagazzino> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ListaMovimentiMagazzinoUtil} to access the lista movimenti magazzino persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @return the matching lista movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListaMovimentiMagazzino> findByArticoloVariante(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int soloValore)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @param start the lower bound of the range of lista movimenti magazzinos
	* @param end the upper bound of the range of lista movimenti magazzinos (not inclusive)
	* @return the range of matching lista movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListaMovimentiMagazzino> findByArticoloVariante(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int soloValore, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @param start the lower bound of the range of lista movimenti magazzinos
	* @param end the upper bound of the range of lista movimenti magazzinos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lista movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListaMovimentiMagazzino> findByArticoloVariante(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int soloValore, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lista movimenti magazzino
	* @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a matching lista movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListaMovimentiMagazzino findByArticoloVariante_First(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException;

	/**
	* Returns the first lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lista movimenti magazzino, or <code>null</code> if a matching lista movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListaMovimentiMagazzino fetchByArticoloVariante_First(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lista movimenti magazzino
	* @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a matching lista movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListaMovimentiMagazzino findByArticoloVariante_Last(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException;

	/**
	* Returns the last lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lista movimenti magazzino, or <code>null</code> if a matching lista movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListaMovimentiMagazzino fetchByArticoloVariante_Last(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lista movimenti magazzinos before and after the current lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	*
	* @param ID the primary key of the current lista movimenti magazzino
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lista movimenti magazzino
	* @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a lista movimenti magazzino with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListaMovimentiMagazzino[] findByArticoloVariante_PrevAndNext(
		java.lang.String ID, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException;

	/**
	* Removes all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63; from the database.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @throws SystemException if a system exception occurred
	*/
	public void removeByArticoloVariante(java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int soloValore)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and soloValore = &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @return the number of matching lista movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public int countByArticoloVariante(java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int soloValore)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @return the matching lista movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListaMovimentiMagazzino> findByArticoloVarianteCaricoScarico(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int testCaricoScarico, int soloValore)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @param start the lower bound of the range of lista movimenti magazzinos
	* @param end the upper bound of the range of lista movimenti magazzinos (not inclusive)
	* @return the range of matching lista movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListaMovimentiMagazzino> findByArticoloVarianteCaricoScarico(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int testCaricoScarico, int soloValore, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @param start the lower bound of the range of lista movimenti magazzinos
	* @param end the upper bound of the range of lista movimenti magazzinos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lista movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListaMovimentiMagazzino> findByArticoloVarianteCaricoScarico(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int testCaricoScarico, int soloValore, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lista movimenti magazzino
	* @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a matching lista movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListaMovimentiMagazzino findByArticoloVarianteCaricoScarico_First(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int testCaricoScarico, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException;

	/**
	* Returns the first lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lista movimenti magazzino, or <code>null</code> if a matching lista movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListaMovimentiMagazzino fetchByArticoloVarianteCaricoScarico_First(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int testCaricoScarico, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lista movimenti magazzino
	* @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a matching lista movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListaMovimentiMagazzino findByArticoloVarianteCaricoScarico_Last(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int testCaricoScarico, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException;

	/**
	* Returns the last lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lista movimenti magazzino, or <code>null</code> if a matching lista movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListaMovimentiMagazzino fetchByArticoloVarianteCaricoScarico_Last(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int testCaricoScarico, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lista movimenti magazzinos before and after the current lista movimenti magazzino in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	*
	* @param ID the primary key of the current lista movimenti magazzino
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lista movimenti magazzino
	* @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a lista movimenti magazzino with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListaMovimentiMagazzino[] findByArticoloVarianteCaricoScarico_PrevAndNext(
		java.lang.String ID, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int testCaricoScarico, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException;

	/**
	* Removes all the lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63; from the database.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @throws SystemException if a system exception occurred
	*/
	public void removeByArticoloVarianteCaricoScarico(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int testCaricoScarico, int soloValore)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lista movimenti magazzinos where codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore = &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @return the number of matching lista movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public int countByArticoloVarianteCaricoScarico(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int testCaricoScarico, int soloValore)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the lista movimenti magazzino in the entity cache if it is enabled.
	*
	* @param listaMovimentiMagazzino the lista movimenti magazzino
	*/
	public void cacheResult(
		it.bysoftware.ct.model.ListaMovimentiMagazzino listaMovimentiMagazzino);

	/**
	* Caches the lista movimenti magazzinos in the entity cache if it is enabled.
	*
	* @param listaMovimentiMagazzinos the lista movimenti magazzinos
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.ListaMovimentiMagazzino> listaMovimentiMagazzinos);

	/**
	* Creates a new lista movimenti magazzino with the primary key. Does not add the lista movimenti magazzino to the database.
	*
	* @param ID the primary key for the new lista movimenti magazzino
	* @return the new lista movimenti magazzino
	*/
	public it.bysoftware.ct.model.ListaMovimentiMagazzino create(
		java.lang.String ID);

	/**
	* Removes the lista movimenti magazzino with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the lista movimenti magazzino
	* @return the lista movimenti magazzino that was removed
	* @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a lista movimenti magazzino with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListaMovimentiMagazzino remove(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException;

	public it.bysoftware.ct.model.ListaMovimentiMagazzino updateImpl(
		it.bysoftware.ct.model.ListaMovimentiMagazzino listaMovimentiMagazzino)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lista movimenti magazzino with the primary key or throws a {@link it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException} if it could not be found.
	*
	* @param ID the primary key of the lista movimenti magazzino
	* @return the lista movimenti magazzino
	* @throws it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException if a lista movimenti magazzino with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListaMovimentiMagazzino findByPrimaryKey(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListaMovimentiMagazzinoException;

	/**
	* Returns the lista movimenti magazzino with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ID the primary key of the lista movimenti magazzino
	* @return the lista movimenti magazzino, or <code>null</code> if a lista movimenti magazzino with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListaMovimentiMagazzino fetchByPrimaryKey(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the lista movimenti magazzinos.
	*
	* @return the lista movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListaMovimentiMagazzino> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the lista movimenti magazzinos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lista movimenti magazzinos
	* @param end the upper bound of the range of lista movimenti magazzinos (not inclusive)
	* @return the range of lista movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListaMovimentiMagazzino> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the lista movimenti magazzinos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lista movimenti magazzinos
	* @param end the upper bound of the range of lista movimenti magazzinos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of lista movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListaMovimentiMagazzino> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the lista movimenti magazzinos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lista movimenti magazzinos.
	*
	* @return the number of lista movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}