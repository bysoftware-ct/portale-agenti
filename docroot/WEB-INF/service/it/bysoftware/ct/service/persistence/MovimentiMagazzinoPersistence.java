/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.MovimentiMagazzino;

/**
 * The persistence interface for the movimenti magazzino service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see MovimentiMagazzinoPersistenceImpl
 * @see MovimentiMagazzinoUtil
 * @generated
 */
public interface MovimentiMagazzinoPersistence extends BasePersistence<MovimentiMagazzino> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link MovimentiMagazzinoUtil} to access the movimenti magazzino persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @return the matching movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.MovimentiMagazzino> findByArticoloVariante(
		int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int soloValore)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @param start the lower bound of the range of movimenti magazzinos
	* @param end the upper bound of the range of movimenti magazzinos (not inclusive)
	* @return the range of matching movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.MovimentiMagazzino> findByArticoloVariante(
		int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int soloValore, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @param start the lower bound of the range of movimenti magazzinos
	* @param end the upper bound of the range of movimenti magazzinos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.MovimentiMagazzino> findByArticoloVariante(
		int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int soloValore, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching movimenti magazzino
	* @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a matching movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.MovimentiMagazzino findByArticoloVariante_First(
		int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchMovimentiMagazzinoException;

	/**
	* Returns the first movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching movimenti magazzino, or <code>null</code> if a matching movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.MovimentiMagazzino fetchByArticoloVariante_First(
		int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching movimenti magazzino
	* @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a matching movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.MovimentiMagazzino findByArticoloVariante_Last(
		int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchMovimentiMagazzinoException;

	/**
	* Returns the last movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching movimenti magazzino, or <code>null</code> if a matching movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.MovimentiMagazzino fetchByArticoloVariante_Last(
		int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the movimenti magazzinos before and after the current movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	*
	* @param ID the primary key of the current movimenti magazzino
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next movimenti magazzino
	* @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a movimenti magazzino with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.MovimentiMagazzino[] findByArticoloVariante_PrevAndNext(
		java.lang.String ID, int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchMovimentiMagazzinoException;

	/**
	* Removes all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63; from the database.
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @throws SystemException if a system exception occurred
	*/
	public void removeByArticoloVariante(int anno,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int soloValore)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and soloValore &ne; &#63;.
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param soloValore the solo valore
	* @return the number of matching movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public int countByArticoloVariante(int anno,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int soloValore)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @return the matching movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.MovimentiMagazzino> findByArticoloVarianteCaricoScarico(
		int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int testCaricoScarico, int soloValore)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @param start the lower bound of the range of movimenti magazzinos
	* @param end the upper bound of the range of movimenti magazzinos (not inclusive)
	* @return the range of matching movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.MovimentiMagazzino> findByArticoloVarianteCaricoScarico(
		int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int testCaricoScarico, int soloValore,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @param start the lower bound of the range of movimenti magazzinos
	* @param end the upper bound of the range of movimenti magazzinos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.MovimentiMagazzino> findByArticoloVarianteCaricoScarico(
		int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int testCaricoScarico, int soloValore,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching movimenti magazzino
	* @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a matching movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.MovimentiMagazzino findByArticoloVarianteCaricoScarico_First(
		int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int testCaricoScarico, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchMovimentiMagazzinoException;

	/**
	* Returns the first movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching movimenti magazzino, or <code>null</code> if a matching movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.MovimentiMagazzino fetchByArticoloVarianteCaricoScarico_First(
		int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int testCaricoScarico, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching movimenti magazzino
	* @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a matching movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.MovimentiMagazzino findByArticoloVarianteCaricoScarico_Last(
		int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int testCaricoScarico, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchMovimentiMagazzinoException;

	/**
	* Returns the last movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching movimenti magazzino, or <code>null</code> if a matching movimenti magazzino could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.MovimentiMagazzino fetchByArticoloVarianteCaricoScarico_Last(
		int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int testCaricoScarico, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the movimenti magazzinos before and after the current movimenti magazzino in the ordered set where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	*
	* @param ID the primary key of the current movimenti magazzino
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next movimenti magazzino
	* @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a movimenti magazzino with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.MovimentiMagazzino[] findByArticoloVarianteCaricoScarico_PrevAndNext(
		java.lang.String ID, int anno, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int testCaricoScarico, int soloValore,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchMovimentiMagazzinoException;

	/**
	* Removes all the movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63; from the database.
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @throws SystemException if a system exception occurred
	*/
	public void removeByArticoloVarianteCaricoScarico(int anno,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int testCaricoScarico, int soloValore)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of movimenti magazzinos where anno = &#63; and codiceArticolo = &#63; and codiceVariante = &#63; and testCaricoScarico = &#63; and soloValore &ne; &#63;.
	*
	* @param anno the anno
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param testCaricoScarico the test carico scarico
	* @param soloValore the solo valore
	* @return the number of matching movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public int countByArticoloVarianteCaricoScarico(int anno,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int testCaricoScarico, int soloValore)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the movimenti magazzino in the entity cache if it is enabled.
	*
	* @param movimentiMagazzino the movimenti magazzino
	*/
	public void cacheResult(
		it.bysoftware.ct.model.MovimentiMagazzino movimentiMagazzino);

	/**
	* Caches the movimenti magazzinos in the entity cache if it is enabled.
	*
	* @param movimentiMagazzinos the movimenti magazzinos
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.MovimentiMagazzino> movimentiMagazzinos);

	/**
	* Creates a new movimenti magazzino with the primary key. Does not add the movimenti magazzino to the database.
	*
	* @param ID the primary key for the new movimenti magazzino
	* @return the new movimenti magazzino
	*/
	public it.bysoftware.ct.model.MovimentiMagazzino create(java.lang.String ID);

	/**
	* Removes the movimenti magazzino with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the movimenti magazzino
	* @return the movimenti magazzino that was removed
	* @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a movimenti magazzino with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.MovimentiMagazzino remove(java.lang.String ID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchMovimentiMagazzinoException;

	public it.bysoftware.ct.model.MovimentiMagazzino updateImpl(
		it.bysoftware.ct.model.MovimentiMagazzino movimentiMagazzino)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the movimenti magazzino with the primary key or throws a {@link it.bysoftware.ct.NoSuchMovimentiMagazzinoException} if it could not be found.
	*
	* @param ID the primary key of the movimenti magazzino
	* @return the movimenti magazzino
	* @throws it.bysoftware.ct.NoSuchMovimentiMagazzinoException if a movimenti magazzino with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.MovimentiMagazzino findByPrimaryKey(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchMovimentiMagazzinoException;

	/**
	* Returns the movimenti magazzino with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ID the primary key of the movimenti magazzino
	* @return the movimenti magazzino, or <code>null</code> if a movimenti magazzino with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.MovimentiMagazzino fetchByPrimaryKey(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the movimenti magazzinos.
	*
	* @return the movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.MovimentiMagazzino> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the movimenti magazzinos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of movimenti magazzinos
	* @param end the upper bound of the range of movimenti magazzinos (not inclusive)
	* @return the range of movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.MovimentiMagazzino> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the movimenti magazzinos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.MovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of movimenti magazzinos
	* @param end the upper bound of the range of movimenti magazzinos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.MovimentiMagazzino> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the movimenti magazzinos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of movimenti magazzinos.
	*
	* @return the number of movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}