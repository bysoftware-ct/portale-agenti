/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.ListiniPrezziArticoli;

/**
 * The persistence interface for the listini prezzi articoli service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see ListiniPrezziArticoliPersistenceImpl
 * @see ListiniPrezziArticoliUtil
 * @generated
 */
public interface ListiniPrezziArticoliPersistence extends BasePersistence<ListiniPrezziArticoli> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ListiniPrezziArticoliUtil} to access the listini prezzi articoli persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @return the matching listini prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListiniPrezziArticoli> findByDedicatoSoggetto(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		java.lang.String codiceSoggetto, int tipoSoggetto,
		java.util.Date dataInizioValidita, double quantInizioValiditaPrezzo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @param start the lower bound of the range of listini prezzi articolis
	* @param end the upper bound of the range of listini prezzi articolis (not inclusive)
	* @return the range of matching listini prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListiniPrezziArticoli> findByDedicatoSoggetto(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		java.lang.String codiceSoggetto, int tipoSoggetto,
		java.util.Date dataInizioValidita, double quantInizioValiditaPrezzo,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @param start the lower bound of the range of listini prezzi articolis
	* @param end the upper bound of the range of listini prezzi articolis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching listini prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListiniPrezziArticoli> findByDedicatoSoggetto(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		java.lang.String codiceSoggetto, int tipoSoggetto,
		java.util.Date dataInizioValidita, double quantInizioValiditaPrezzo,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching listini prezzi articoli
	* @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a matching listini prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListiniPrezziArticoli findByDedicatoSoggetto_First(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		java.lang.String codiceSoggetto, int tipoSoggetto,
		java.util.Date dataInizioValidita, double quantInizioValiditaPrezzo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListiniPrezziArticoliException;

	/**
	* Returns the first listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching listini prezzi articoli, or <code>null</code> if a matching listini prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListiniPrezziArticoli fetchByDedicatoSoggetto_First(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		java.lang.String codiceSoggetto, int tipoSoggetto,
		java.util.Date dataInizioValidita, double quantInizioValiditaPrezzo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching listini prezzi articoli
	* @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a matching listini prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListiniPrezziArticoli findByDedicatoSoggetto_Last(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		java.lang.String codiceSoggetto, int tipoSoggetto,
		java.util.Date dataInizioValidita, double quantInizioValiditaPrezzo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListiniPrezziArticoliException;

	/**
	* Returns the last listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching listini prezzi articoli, or <code>null</code> if a matching listini prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListiniPrezziArticoli fetchByDedicatoSoggetto_Last(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		java.lang.String codiceSoggetto, int tipoSoggetto,
		java.util.Date dataInizioValidita, double quantInizioValiditaPrezzo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the listini prezzi articolis before and after the current listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* @param listiniPrezziArticoliPK the primary key of the current listini prezzi articoli
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next listini prezzi articoli
	* @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a listini prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListiniPrezziArticoli[] findByDedicatoSoggetto_PrevAndNext(
		it.bysoftware.ct.service.persistence.ListiniPrezziArticoliPK listiniPrezziArticoliPK,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		java.lang.String codiceSoggetto, int tipoSoggetto,
		java.util.Date dataInizioValidita, double quantInizioValiditaPrezzo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListiniPrezziArticoliException;

	/**
	* Removes all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63; from the database.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @throws SystemException if a system exception occurred
	*/
	public void removeByDedicatoSoggetto(java.lang.String codiceArticolo,
		java.lang.String codiceVariante, java.lang.String codiceSoggetto,
		int tipoSoggetto, java.util.Date dataInizioValidita,
		double quantInizioValiditaPrezzo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and codiceSoggetto = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @return the number of matching listini prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public int countByDedicatoSoggetto(java.lang.String codiceArticolo,
		java.lang.String codiceVariante, java.lang.String codiceSoggetto,
		int tipoSoggetto, java.util.Date dataInizioValidita,
		double quantInizioValiditaPrezzo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @return the matching listini prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListiniPrezziArticoli> findByGenerico(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int tipoSoggetto, java.util.Date dataInizioValidita,
		double quantInizioValiditaPrezzo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @param start the lower bound of the range of listini prezzi articolis
	* @param end the upper bound of the range of listini prezzi articolis (not inclusive)
	* @return the range of matching listini prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListiniPrezziArticoli> findByGenerico(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int tipoSoggetto, java.util.Date dataInizioValidita,
		double quantInizioValiditaPrezzo, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @param start the lower bound of the range of listini prezzi articolis
	* @param end the upper bound of the range of listini prezzi articolis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching listini prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListiniPrezziArticoli> findByGenerico(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int tipoSoggetto, java.util.Date dataInizioValidita,
		double quantInizioValiditaPrezzo, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching listini prezzi articoli
	* @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a matching listini prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListiniPrezziArticoli findByGenerico_First(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int tipoSoggetto, java.util.Date dataInizioValidita,
		double quantInizioValiditaPrezzo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListiniPrezziArticoliException;

	/**
	* Returns the first listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching listini prezzi articoli, or <code>null</code> if a matching listini prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListiniPrezziArticoli fetchByGenerico_First(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int tipoSoggetto, java.util.Date dataInizioValidita,
		double quantInizioValiditaPrezzo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching listini prezzi articoli
	* @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a matching listini prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListiniPrezziArticoli findByGenerico_Last(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int tipoSoggetto, java.util.Date dataInizioValidita,
		double quantInizioValiditaPrezzo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListiniPrezziArticoliException;

	/**
	* Returns the last listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching listini prezzi articoli, or <code>null</code> if a matching listini prezzi articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListiniPrezziArticoli fetchByGenerico_Last(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int tipoSoggetto, java.util.Date dataInizioValidita,
		double quantInizioValiditaPrezzo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the listini prezzi articolis before and after the current listini prezzi articoli in the ordered set where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* @param listiniPrezziArticoliPK the primary key of the current listini prezzi articoli
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next listini prezzi articoli
	* @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a listini prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListiniPrezziArticoli[] findByGenerico_PrevAndNext(
		it.bysoftware.ct.service.persistence.ListiniPrezziArticoliPK listiniPrezziArticoliPK,
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		int tipoSoggetto, java.util.Date dataInizioValidita,
		double quantInizioValiditaPrezzo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListiniPrezziArticoliException;

	/**
	* Removes all the listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63; from the database.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @throws SystemException if a system exception occurred
	*/
	public void removeByGenerico(java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int tipoSoggetto,
		java.util.Date dataInizioValidita, double quantInizioValiditaPrezzo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of listini prezzi articolis where codiceArticolo = &#63; and codiceVariante = &#63; and tipoSoggetto = &#63; and dataInizioValidita &gt; &#63; and quantInizioValiditaPrezzo &gt; &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param tipoSoggetto the tipo soggetto
	* @param dataInizioValidita the data inizio validita
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo
	* @return the number of matching listini prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public int countByGenerico(java.lang.String codiceArticolo,
		java.lang.String codiceVariante, int tipoSoggetto,
		java.util.Date dataInizioValidita, double quantInizioValiditaPrezzo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the listini prezzi articoli in the entity cache if it is enabled.
	*
	* @param listiniPrezziArticoli the listini prezzi articoli
	*/
	public void cacheResult(
		it.bysoftware.ct.model.ListiniPrezziArticoli listiniPrezziArticoli);

	/**
	* Caches the listini prezzi articolis in the entity cache if it is enabled.
	*
	* @param listiniPrezziArticolis the listini prezzi articolis
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.ListiniPrezziArticoli> listiniPrezziArticolis);

	/**
	* Creates a new listini prezzi articoli with the primary key. Does not add the listini prezzi articoli to the database.
	*
	* @param listiniPrezziArticoliPK the primary key for the new listini prezzi articoli
	* @return the new listini prezzi articoli
	*/
	public it.bysoftware.ct.model.ListiniPrezziArticoli create(
		it.bysoftware.ct.service.persistence.ListiniPrezziArticoliPK listiniPrezziArticoliPK);

	/**
	* Removes the listini prezzi articoli with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param listiniPrezziArticoliPK the primary key of the listini prezzi articoli
	* @return the listini prezzi articoli that was removed
	* @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a listini prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListiniPrezziArticoli remove(
		it.bysoftware.ct.service.persistence.ListiniPrezziArticoliPK listiniPrezziArticoliPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListiniPrezziArticoliException;

	public it.bysoftware.ct.model.ListiniPrezziArticoli updateImpl(
		it.bysoftware.ct.model.ListiniPrezziArticoli listiniPrezziArticoli)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the listini prezzi articoli with the primary key or throws a {@link it.bysoftware.ct.NoSuchListiniPrezziArticoliException} if it could not be found.
	*
	* @param listiniPrezziArticoliPK the primary key of the listini prezzi articoli
	* @return the listini prezzi articoli
	* @throws it.bysoftware.ct.NoSuchListiniPrezziArticoliException if a listini prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListiniPrezziArticoli findByPrimaryKey(
		it.bysoftware.ct.service.persistence.ListiniPrezziArticoliPK listiniPrezziArticoliPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchListiniPrezziArticoliException;

	/**
	* Returns the listini prezzi articoli with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param listiniPrezziArticoliPK the primary key of the listini prezzi articoli
	* @return the listini prezzi articoli, or <code>null</code> if a listini prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.ListiniPrezziArticoli fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.ListiniPrezziArticoliPK listiniPrezziArticoliPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the listini prezzi articolis.
	*
	* @return the listini prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListiniPrezziArticoli> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the listini prezzi articolis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of listini prezzi articolis
	* @param end the upper bound of the range of listini prezzi articolis (not inclusive)
	* @return the range of listini prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListiniPrezziArticoli> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the listini prezzi articolis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of listini prezzi articolis
	* @param end the upper bound of the range of listini prezzi articolis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of listini prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.ListiniPrezziArticoli> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the listini prezzi articolis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of listini prezzi articolis.
	*
	* @return the number of listini prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}