/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class UltimiPrezziArticoliPK implements Comparable<UltimiPrezziArticoliPK>,
	Serializable {
	public boolean tipoSoggetto;
	public String codiceSoggetto;
	public String codiceArticolo;
	public String codiceVariante;

	public UltimiPrezziArticoliPK() {
	}

	public UltimiPrezziArticoliPK(boolean tipoSoggetto, String codiceSoggetto,
		String codiceArticolo, String codiceVariante) {
		this.tipoSoggetto = tipoSoggetto;
		this.codiceSoggetto = codiceSoggetto;
		this.codiceArticolo = codiceArticolo;
		this.codiceVariante = codiceVariante;
	}

	public boolean getTipoSoggetto() {
		return tipoSoggetto;
	}

	public boolean isTipoSoggetto() {
		return tipoSoggetto;
	}

	public void setTipoSoggetto(boolean tipoSoggetto) {
		this.tipoSoggetto = tipoSoggetto;
	}

	public String getCodiceSoggetto() {
		return codiceSoggetto;
	}

	public void setCodiceSoggetto(String codiceSoggetto) {
		this.codiceSoggetto = codiceSoggetto;
	}

	public String getCodiceArticolo() {
		return codiceArticolo;
	}

	public void setCodiceArticolo(String codiceArticolo) {
		this.codiceArticolo = codiceArticolo;
	}

	public String getCodiceVariante() {
		return codiceVariante;
	}

	public void setCodiceVariante(String codiceVariante) {
		this.codiceVariante = codiceVariante;
	}

	@Override
	public int compareTo(UltimiPrezziArticoliPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (!tipoSoggetto && pk.tipoSoggetto) {
			value = -1;
		}
		else if (tipoSoggetto && !pk.tipoSoggetto) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		value = codiceSoggetto.compareTo(pk.codiceSoggetto);

		if (value != 0) {
			return value;
		}

		value = codiceArticolo.compareTo(pk.codiceArticolo);

		if (value != 0) {
			return value;
		}

		value = codiceVariante.compareTo(pk.codiceVariante);

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UltimiPrezziArticoliPK)) {
			return false;
		}

		UltimiPrezziArticoliPK pk = (UltimiPrezziArticoliPK)obj;

		if ((tipoSoggetto == pk.tipoSoggetto) &&
				(codiceSoggetto.equals(pk.codiceSoggetto)) &&
				(codiceArticolo.equals(pk.codiceArticolo)) &&
				(codiceVariante.equals(pk.codiceVariante))) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(tipoSoggetto) + String.valueOf(codiceSoggetto) +
		String.valueOf(codiceArticolo) + String.valueOf(codiceVariante)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(20);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("tipoSoggetto");
		sb.append(StringPool.EQUAL);
		sb.append(tipoSoggetto);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codiceSoggetto");
		sb.append(StringPool.EQUAL);
		sb.append(codiceSoggetto);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codiceArticolo");
		sb.append(StringPool.EQUAL);
		sb.append(codiceArticolo);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codiceVariante");
		sb.append(StringPool.EQUAL);
		sb.append(codiceVariante);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}