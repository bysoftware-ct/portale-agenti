/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.DatiClientiFornitori;

/**
 * The persistence interface for the dati clienti fornitori service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see DatiClientiFornitoriPersistenceImpl
 * @see DatiClientiFornitoriUtil
 * @generated
 */
public interface DatiClientiFornitoriPersistence extends BasePersistence<DatiClientiFornitori> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DatiClientiFornitoriUtil} to access the dati clienti fornitori persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the dati clienti fornitoris where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @return the matching dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.DatiClientiFornitori> findByCodiceAgente(
		java.lang.String codiceAgente, boolean tipoSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the dati clienti fornitoris where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @param start the lower bound of the range of dati clienti fornitoris
	* @param end the upper bound of the range of dati clienti fornitoris (not inclusive)
	* @return the range of matching dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.DatiClientiFornitori> findByCodiceAgente(
		java.lang.String codiceAgente, boolean tipoSoggetto, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the dati clienti fornitoris where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @param start the lower bound of the range of dati clienti fornitoris
	* @param end the upper bound of the range of dati clienti fornitoris (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.DatiClientiFornitori> findByCodiceAgente(
		java.lang.String codiceAgente, boolean tipoSoggetto, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first dati clienti fornitori in the ordered set where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dati clienti fornitori
	* @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a matching dati clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.DatiClientiFornitori findByCodiceAgente_First(
		java.lang.String codiceAgente, boolean tipoSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiClientiFornitoriException;

	/**
	* Returns the first dati clienti fornitori in the ordered set where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dati clienti fornitori, or <code>null</code> if a matching dati clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.DatiClientiFornitori fetchByCodiceAgente_First(
		java.lang.String codiceAgente, boolean tipoSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last dati clienti fornitori in the ordered set where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dati clienti fornitori
	* @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a matching dati clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.DatiClientiFornitori findByCodiceAgente_Last(
		java.lang.String codiceAgente, boolean tipoSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiClientiFornitoriException;

	/**
	* Returns the last dati clienti fornitori in the ordered set where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dati clienti fornitori, or <code>null</code> if a matching dati clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.DatiClientiFornitori fetchByCodiceAgente_Last(
		java.lang.String codiceAgente, boolean tipoSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the dati clienti fornitoris before and after the current dati clienti fornitori in the ordered set where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* @param datiClientiFornitoriPK the primary key of the current dati clienti fornitori
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next dati clienti fornitori
	* @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a dati clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.DatiClientiFornitori[] findByCodiceAgente_PrevAndNext(
		it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK datiClientiFornitoriPK,
		java.lang.String codiceAgente, boolean tipoSoggetto,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiClientiFornitoriException;

	/**
	* Removes all the dati clienti fornitoris where codiceAgente = &#63; and tipoSoggetto = &#63; from the database.
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCodiceAgente(java.lang.String codiceAgente,
		boolean tipoSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of dati clienti fornitoris where codiceAgente = &#63; and tipoSoggetto = &#63;.
	*
	* @param codiceAgente the codice agente
	* @param tipoSoggetto the tipo soggetto
	* @return the number of matching dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public int countByCodiceAgente(java.lang.String codiceAgente,
		boolean tipoSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the dati clienti fornitori where codiceSoggetto = &#63; and tipoSoggetto = &#63; or throws a {@link it.bysoftware.ct.NoSuchDatiClientiFornitoriException} if it could not be found.
	*
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @return the matching dati clienti fornitori
	* @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a matching dati clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.DatiClientiFornitori findByCodiceSoggetto(
		java.lang.String codiceSoggetto, boolean tipoSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiClientiFornitoriException;

	/**
	* Returns the dati clienti fornitori where codiceSoggetto = &#63; and tipoSoggetto = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @return the matching dati clienti fornitori, or <code>null</code> if a matching dati clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.DatiClientiFornitori fetchByCodiceSoggetto(
		java.lang.String codiceSoggetto, boolean tipoSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the dati clienti fornitori where codiceSoggetto = &#63; and tipoSoggetto = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching dati clienti fornitori, or <code>null</code> if a matching dati clienti fornitori could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.DatiClientiFornitori fetchByCodiceSoggetto(
		java.lang.String codiceSoggetto, boolean tipoSoggetto,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the dati clienti fornitori where codiceSoggetto = &#63; and tipoSoggetto = &#63; from the database.
	*
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @return the dati clienti fornitori that was removed
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.DatiClientiFornitori removeByCodiceSoggetto(
		java.lang.String codiceSoggetto, boolean tipoSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiClientiFornitoriException;

	/**
	* Returns the number of dati clienti fornitoris where codiceSoggetto = &#63; and tipoSoggetto = &#63;.
	*
	* @param codiceSoggetto the codice soggetto
	* @param tipoSoggetto the tipo soggetto
	* @return the number of matching dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public int countByCodiceSoggetto(java.lang.String codiceSoggetto,
		boolean tipoSoggetto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the dati clienti fornitori in the entity cache if it is enabled.
	*
	* @param datiClientiFornitori the dati clienti fornitori
	*/
	public void cacheResult(
		it.bysoftware.ct.model.DatiClientiFornitori datiClientiFornitori);

	/**
	* Caches the dati clienti fornitoris in the entity cache if it is enabled.
	*
	* @param datiClientiFornitoris the dati clienti fornitoris
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.DatiClientiFornitori> datiClientiFornitoris);

	/**
	* Creates a new dati clienti fornitori with the primary key. Does not add the dati clienti fornitori to the database.
	*
	* @param datiClientiFornitoriPK the primary key for the new dati clienti fornitori
	* @return the new dati clienti fornitori
	*/
	public it.bysoftware.ct.model.DatiClientiFornitori create(
		it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK datiClientiFornitoriPK);

	/**
	* Removes the dati clienti fornitori with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param datiClientiFornitoriPK the primary key of the dati clienti fornitori
	* @return the dati clienti fornitori that was removed
	* @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a dati clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.DatiClientiFornitori remove(
		it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK datiClientiFornitoriPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiClientiFornitoriException;

	public it.bysoftware.ct.model.DatiClientiFornitori updateImpl(
		it.bysoftware.ct.model.DatiClientiFornitori datiClientiFornitori)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the dati clienti fornitori with the primary key or throws a {@link it.bysoftware.ct.NoSuchDatiClientiFornitoriException} if it could not be found.
	*
	* @param datiClientiFornitoriPK the primary key of the dati clienti fornitori
	* @return the dati clienti fornitori
	* @throws it.bysoftware.ct.NoSuchDatiClientiFornitoriException if a dati clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.DatiClientiFornitori findByPrimaryKey(
		it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK datiClientiFornitoriPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchDatiClientiFornitoriException;

	/**
	* Returns the dati clienti fornitori with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param datiClientiFornitoriPK the primary key of the dati clienti fornitori
	* @return the dati clienti fornitori, or <code>null</code> if a dati clienti fornitori with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.DatiClientiFornitori fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK datiClientiFornitoriPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the dati clienti fornitoris.
	*
	* @return the dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.DatiClientiFornitori> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the dati clienti fornitoris.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dati clienti fornitoris
	* @param end the upper bound of the range of dati clienti fornitoris (not inclusive)
	* @return the range of dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.DatiClientiFornitori> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the dati clienti fornitoris.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.DatiClientiFornitoriModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dati clienti fornitoris
	* @param end the upper bound of the range of dati clienti fornitoris (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.DatiClientiFornitori> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the dati clienti fornitoris from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of dati clienti fornitoris.
	*
	* @return the number of dati clienti fornitoris
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}