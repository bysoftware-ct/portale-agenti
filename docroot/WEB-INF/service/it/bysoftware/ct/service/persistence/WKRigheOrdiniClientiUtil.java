/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.WKRigheOrdiniClienti;

import java.util.List;

/**
 * The persistence utility for the w k righe ordini clienti service. This utility wraps {@link WKRigheOrdiniClientiPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see WKRigheOrdiniClientiPersistence
 * @see WKRigheOrdiniClientiPersistenceImpl
 * @generated
 */
public class WKRigheOrdiniClientiUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(WKRigheOrdiniClienti wkRigheOrdiniClienti) {
		getPersistence().clearCache(wkRigheOrdiniClienti);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<WKRigheOrdiniClienti> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<WKRigheOrdiniClienti> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<WKRigheOrdiniClienti> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static WKRigheOrdiniClienti update(
		WKRigheOrdiniClienti wkRigheOrdiniClienti) throws SystemException {
		return getPersistence().update(wkRigheOrdiniClienti);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static WKRigheOrdiniClienti update(
		WKRigheOrdiniClienti wkRigheOrdiniClienti, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(wkRigheOrdiniClienti, serviceContext);
	}

	/**
	* Returns all the w k righe ordini clientis where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @return the matching w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.WKRigheOrdiniClienti> findByAnnoTipoOrdineNumeroOrdine(
		int anno, int tipoOrdine, int numeroOrdine)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByAnnoTipoOrdineNumeroOrdine(anno, tipoOrdine,
			numeroOrdine);
	}

	/**
	* Returns a range of all the w k righe ordini clientis where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKRigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param start the lower bound of the range of w k righe ordini clientis
	* @param end the upper bound of the range of w k righe ordini clientis (not inclusive)
	* @return the range of matching w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.WKRigheOrdiniClienti> findByAnnoTipoOrdineNumeroOrdine(
		int anno, int tipoOrdine, int numeroOrdine, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByAnnoTipoOrdineNumeroOrdine(anno, tipoOrdine,
			numeroOrdine, start, end);
	}

	/**
	* Returns an ordered range of all the w k righe ordini clientis where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKRigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param start the lower bound of the range of w k righe ordini clientis
	* @param end the upper bound of the range of w k righe ordini clientis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.WKRigheOrdiniClienti> findByAnnoTipoOrdineNumeroOrdine(
		int anno, int tipoOrdine, int numeroOrdine, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByAnnoTipoOrdineNumeroOrdine(anno, tipoOrdine,
			numeroOrdine, start, end, orderByComparator);
	}

	/**
	* Returns the first w k righe ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching w k righe ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a matching w k righe ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.WKRigheOrdiniClienti findByAnnoTipoOrdineNumeroOrdine_First(
		int anno, int tipoOrdine, int numeroOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException {
		return getPersistence()
				   .findByAnnoTipoOrdineNumeroOrdine_First(anno, tipoOrdine,
			numeroOrdine, orderByComparator);
	}

	/**
	* Returns the first w k righe ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching w k righe ordini clienti, or <code>null</code> if a matching w k righe ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.WKRigheOrdiniClienti fetchByAnnoTipoOrdineNumeroOrdine_First(
		int anno, int tipoOrdine, int numeroOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByAnnoTipoOrdineNumeroOrdine_First(anno, tipoOrdine,
			numeroOrdine, orderByComparator);
	}

	/**
	* Returns the last w k righe ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching w k righe ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a matching w k righe ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.WKRigheOrdiniClienti findByAnnoTipoOrdineNumeroOrdine_Last(
		int anno, int tipoOrdine, int numeroOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException {
		return getPersistence()
				   .findByAnnoTipoOrdineNumeroOrdine_Last(anno, tipoOrdine,
			numeroOrdine, orderByComparator);
	}

	/**
	* Returns the last w k righe ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching w k righe ordini clienti, or <code>null</code> if a matching w k righe ordini clienti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.WKRigheOrdiniClienti fetchByAnnoTipoOrdineNumeroOrdine_Last(
		int anno, int tipoOrdine, int numeroOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByAnnoTipoOrdineNumeroOrdine_Last(anno, tipoOrdine,
			numeroOrdine, orderByComparator);
	}

	/**
	* Returns the w k righe ordini clientis before and after the current w k righe ordini clienti in the ordered set where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param wkRigheOrdiniClientiPK the primary key of the current w k righe ordini clienti
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next w k righe ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a w k righe ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.WKRigheOrdiniClienti[] findByAnnoTipoOrdineNumeroOrdine_PrevAndNext(
		it.bysoftware.ct.service.persistence.WKRigheOrdiniClientiPK wkRigheOrdiniClientiPK,
		int anno, int tipoOrdine, int numeroOrdine,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException {
		return getPersistence()
				   .findByAnnoTipoOrdineNumeroOrdine_PrevAndNext(wkRigheOrdiniClientiPK,
			anno, tipoOrdine, numeroOrdine, orderByComparator);
	}

	/**
	* Removes all the w k righe ordini clientis where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63; from the database.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByAnnoTipoOrdineNumeroOrdine(int anno,
		int tipoOrdine, int numeroOrdine)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByAnnoTipoOrdineNumeroOrdine(anno, tipoOrdine, numeroOrdine);
	}

	/**
	* Returns the number of w k righe ordini clientis where anno = &#63; and tipoOrdine = &#63; and numeroOrdine = &#63;.
	*
	* @param anno the anno
	* @param tipoOrdine the tipo ordine
	* @param numeroOrdine the numero ordine
	* @return the number of matching w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public static int countByAnnoTipoOrdineNumeroOrdine(int anno,
		int tipoOrdine, int numeroOrdine)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByAnnoTipoOrdineNumeroOrdine(anno, tipoOrdine,
			numeroOrdine);
	}

	/**
	* Caches the w k righe ordini clienti in the entity cache if it is enabled.
	*
	* @param wkRigheOrdiniClienti the w k righe ordini clienti
	*/
	public static void cacheResult(
		it.bysoftware.ct.model.WKRigheOrdiniClienti wkRigheOrdiniClienti) {
		getPersistence().cacheResult(wkRigheOrdiniClienti);
	}

	/**
	* Caches the w k righe ordini clientis in the entity cache if it is enabled.
	*
	* @param wkRigheOrdiniClientis the w k righe ordini clientis
	*/
	public static void cacheResult(
		java.util.List<it.bysoftware.ct.model.WKRigheOrdiniClienti> wkRigheOrdiniClientis) {
		getPersistence().cacheResult(wkRigheOrdiniClientis);
	}

	/**
	* Creates a new w k righe ordini clienti with the primary key. Does not add the w k righe ordini clienti to the database.
	*
	* @param wkRigheOrdiniClientiPK the primary key for the new w k righe ordini clienti
	* @return the new w k righe ordini clienti
	*/
	public static it.bysoftware.ct.model.WKRigheOrdiniClienti create(
		it.bysoftware.ct.service.persistence.WKRigheOrdiniClientiPK wkRigheOrdiniClientiPK) {
		return getPersistence().create(wkRigheOrdiniClientiPK);
	}

	/**
	* Removes the w k righe ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param wkRigheOrdiniClientiPK the primary key of the w k righe ordini clienti
	* @return the w k righe ordini clienti that was removed
	* @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a w k righe ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.WKRigheOrdiniClienti remove(
		it.bysoftware.ct.service.persistence.WKRigheOrdiniClientiPK wkRigheOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException {
		return getPersistence().remove(wkRigheOrdiniClientiPK);
	}

	public static it.bysoftware.ct.model.WKRigheOrdiniClienti updateImpl(
		it.bysoftware.ct.model.WKRigheOrdiniClienti wkRigheOrdiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(wkRigheOrdiniClienti);
	}

	/**
	* Returns the w k righe ordini clienti with the primary key or throws a {@link it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException} if it could not be found.
	*
	* @param wkRigheOrdiniClientiPK the primary key of the w k righe ordini clienti
	* @return the w k righe ordini clienti
	* @throws it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException if a w k righe ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.WKRigheOrdiniClienti findByPrimaryKey(
		it.bysoftware.ct.service.persistence.WKRigheOrdiniClientiPK wkRigheOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException {
		return getPersistence().findByPrimaryKey(wkRigheOrdiniClientiPK);
	}

	/**
	* Returns the w k righe ordini clienti with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param wkRigheOrdiniClientiPK the primary key of the w k righe ordini clienti
	* @return the w k righe ordini clienti, or <code>null</code> if a w k righe ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.WKRigheOrdiniClienti fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.WKRigheOrdiniClientiPK wkRigheOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(wkRigheOrdiniClientiPK);
	}

	/**
	* Returns all the w k righe ordini clientis.
	*
	* @return the w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.WKRigheOrdiniClienti> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the w k righe ordini clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKRigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of w k righe ordini clientis
	* @param end the upper bound of the range of w k righe ordini clientis (not inclusive)
	* @return the range of w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.WKRigheOrdiniClienti> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the w k righe ordini clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKRigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of w k righe ordini clientis
	* @param end the upper bound of the range of w k righe ordini clientis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.WKRigheOrdiniClienti> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the w k righe ordini clientis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of w k righe ordini clientis.
	*
	* @return the number of w k righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static WKRigheOrdiniClientiPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (WKRigheOrdiniClientiPersistence)PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
					WKRigheOrdiniClientiPersistence.class.getName());

			ReferenceRegistry.registerReference(WKRigheOrdiniClientiUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(WKRigheOrdiniClientiPersistence persistence) {
	}

	private static WKRigheOrdiniClientiPersistence _persistence;
}