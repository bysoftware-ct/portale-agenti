/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.bysoftware.ct.model.Ordinato;

/**
 * The persistence interface for the ordinato service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see OrdinatoPersistenceImpl
 * @see OrdinatoUtil
 * @generated
 */
public interface OrdinatoPersistence extends BasePersistence<Ordinato> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link OrdinatoUtil} to access the ordinato persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the ordinato where codiceArticolo = &#63; and codiceVariante = &#63; or throws a {@link it.bysoftware.ct.NoSuchOrdinatoException} if it could not be found.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @return the matching ordinato
	* @throws it.bysoftware.ct.NoSuchOrdinatoException if a matching ordinato could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.Ordinato findByArticoloVariante(
		java.lang.String codiceArticolo, java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchOrdinatoException;

	/**
	* Returns the ordinato where codiceArticolo = &#63; and codiceVariante = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @return the matching ordinato, or <code>null</code> if a matching ordinato could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.Ordinato fetchByArticoloVariante(
		java.lang.String codiceArticolo, java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ordinato where codiceArticolo = &#63; and codiceVariante = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching ordinato, or <code>null</code> if a matching ordinato could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.Ordinato fetchByArticoloVariante(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the ordinato where codiceArticolo = &#63; and codiceVariante = &#63; from the database.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @return the ordinato that was removed
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.Ordinato removeByArticoloVariante(
		java.lang.String codiceArticolo, java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchOrdinatoException;

	/**
	* Returns the number of ordinatos where codiceArticolo = &#63; and codiceVariante = &#63;.
	*
	* @param codiceArticolo the codice articolo
	* @param codiceVariante the codice variante
	* @return the number of matching ordinatos
	* @throws SystemException if a system exception occurred
	*/
	public int countByArticoloVariante(java.lang.String codiceArticolo,
		java.lang.String codiceVariante)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the ordinato in the entity cache if it is enabled.
	*
	* @param ordinato the ordinato
	*/
	public void cacheResult(it.bysoftware.ct.model.Ordinato ordinato);

	/**
	* Caches the ordinatos in the entity cache if it is enabled.
	*
	* @param ordinatos the ordinatos
	*/
	public void cacheResult(
		java.util.List<it.bysoftware.ct.model.Ordinato> ordinatos);

	/**
	* Creates a new ordinato with the primary key. Does not add the ordinato to the database.
	*
	* @param ordinatoPK the primary key for the new ordinato
	* @return the new ordinato
	*/
	public it.bysoftware.ct.model.Ordinato create(
		it.bysoftware.ct.service.persistence.OrdinatoPK ordinatoPK);

	/**
	* Removes the ordinato with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ordinatoPK the primary key of the ordinato
	* @return the ordinato that was removed
	* @throws it.bysoftware.ct.NoSuchOrdinatoException if a ordinato with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.Ordinato remove(
		it.bysoftware.ct.service.persistence.OrdinatoPK ordinatoPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchOrdinatoException;

	public it.bysoftware.ct.model.Ordinato updateImpl(
		it.bysoftware.ct.model.Ordinato ordinato)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ordinato with the primary key or throws a {@link it.bysoftware.ct.NoSuchOrdinatoException} if it could not be found.
	*
	* @param ordinatoPK the primary key of the ordinato
	* @return the ordinato
	* @throws it.bysoftware.ct.NoSuchOrdinatoException if a ordinato with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.Ordinato findByPrimaryKey(
		it.bysoftware.ct.service.persistence.OrdinatoPK ordinatoPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchOrdinatoException;

	/**
	* Returns the ordinato with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ordinatoPK the primary key of the ordinato
	* @return the ordinato, or <code>null</code> if a ordinato with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.bysoftware.ct.model.Ordinato fetchByPrimaryKey(
		it.bysoftware.ct.service.persistence.OrdinatoPK ordinatoPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ordinatos.
	*
	* @return the ordinatos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.Ordinato> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ordinatos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdinatoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ordinatos
	* @param end the upper bound of the range of ordinatos (not inclusive)
	* @return the range of ordinatos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.Ordinato> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ordinatos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdinatoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ordinatos
	* @param end the upper bound of the range of ordinatos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ordinatos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.bysoftware.ct.model.Ordinato> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the ordinatos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ordinatos.
	*
	* @return the number of ordinatos
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}