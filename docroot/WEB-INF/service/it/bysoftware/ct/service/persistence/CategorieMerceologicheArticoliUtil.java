/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.CategorieMerceologicheArticoli;

import java.util.List;

/**
 * The persistence utility for the categorie merceologiche articoli service. This utility wraps {@link CategorieMerceologicheArticoliPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see CategorieMerceologicheArticoliPersistence
 * @see CategorieMerceologicheArticoliPersistenceImpl
 * @generated
 */
public class CategorieMerceologicheArticoliUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(
		CategorieMerceologicheArticoli categorieMerceologicheArticoli) {
		getPersistence().clearCache(categorieMerceologicheArticoli);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CategorieMerceologicheArticoli> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CategorieMerceologicheArticoli> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CategorieMerceologicheArticoli> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static CategorieMerceologicheArticoli update(
		CategorieMerceologicheArticoli categorieMerceologicheArticoli)
		throws SystemException {
		return getPersistence().update(categorieMerceologicheArticoli);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static CategorieMerceologicheArticoli update(
		CategorieMerceologicheArticoli categorieMerceologicheArticoli,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence()
				   .update(categorieMerceologicheArticoli, serviceContext);
	}

	/**
	* Returns the categorie merceologiche articoli where codiceCategoria = &#63; or throws a {@link it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException} if it could not be found.
	*
	* @param codiceCategoria the codice categoria
	* @return the matching categorie merceologiche articoli
	* @throws it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException if a matching categorie merceologiche articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.CategorieMerceologicheArticoli findByCodiceCategoria(
		java.lang.String codiceCategoria)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException {
		return getPersistence().findByCodiceCategoria(codiceCategoria);
	}

	/**
	* Returns the categorie merceologiche articoli where codiceCategoria = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codiceCategoria the codice categoria
	* @return the matching categorie merceologiche articoli, or <code>null</code> if a matching categorie merceologiche articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.CategorieMerceologicheArticoli fetchByCodiceCategoria(
		java.lang.String codiceCategoria)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCodiceCategoria(codiceCategoria);
	}

	/**
	* Returns the categorie merceologiche articoli where codiceCategoria = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codiceCategoria the codice categoria
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching categorie merceologiche articoli, or <code>null</code> if a matching categorie merceologiche articoli could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.CategorieMerceologicheArticoli fetchByCodiceCategoria(
		java.lang.String codiceCategoria, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCodiceCategoria(codiceCategoria, retrieveFromCache);
	}

	/**
	* Removes the categorie merceologiche articoli where codiceCategoria = &#63; from the database.
	*
	* @param codiceCategoria the codice categoria
	* @return the categorie merceologiche articoli that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.CategorieMerceologicheArticoli removeByCodiceCategoria(
		java.lang.String codiceCategoria)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException {
		return getPersistence().removeByCodiceCategoria(codiceCategoria);
	}

	/**
	* Returns the number of categorie merceologiche articolis where codiceCategoria = &#63;.
	*
	* @param codiceCategoria the codice categoria
	* @return the number of matching categorie merceologiche articolis
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCodiceCategoria(java.lang.String codiceCategoria)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCodiceCategoria(codiceCategoria);
	}

	/**
	* Caches the categorie merceologiche articoli in the entity cache if it is enabled.
	*
	* @param categorieMerceologicheArticoli the categorie merceologiche articoli
	*/
	public static void cacheResult(
		it.bysoftware.ct.model.CategorieMerceologicheArticoli categorieMerceologicheArticoli) {
		getPersistence().cacheResult(categorieMerceologicheArticoli);
	}

	/**
	* Caches the categorie merceologiche articolis in the entity cache if it is enabled.
	*
	* @param categorieMerceologicheArticolis the categorie merceologiche articolis
	*/
	public static void cacheResult(
		java.util.List<it.bysoftware.ct.model.CategorieMerceologicheArticoli> categorieMerceologicheArticolis) {
		getPersistence().cacheResult(categorieMerceologicheArticolis);
	}

	/**
	* Creates a new categorie merceologiche articoli with the primary key. Does not add the categorie merceologiche articoli to the database.
	*
	* @param codiceCategoria the primary key for the new categorie merceologiche articoli
	* @return the new categorie merceologiche articoli
	*/
	public static it.bysoftware.ct.model.CategorieMerceologicheArticoli create(
		java.lang.String codiceCategoria) {
		return getPersistence().create(codiceCategoria);
	}

	/**
	* Removes the categorie merceologiche articoli with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param codiceCategoria the primary key of the categorie merceologiche articoli
	* @return the categorie merceologiche articoli that was removed
	* @throws it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException if a categorie merceologiche articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.CategorieMerceologicheArticoli remove(
		java.lang.String codiceCategoria)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException {
		return getPersistence().remove(codiceCategoria);
	}

	public static it.bysoftware.ct.model.CategorieMerceologicheArticoli updateImpl(
		it.bysoftware.ct.model.CategorieMerceologicheArticoli categorieMerceologicheArticoli)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(categorieMerceologicheArticoli);
	}

	/**
	* Returns the categorie merceologiche articoli with the primary key or throws a {@link it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException} if it could not be found.
	*
	* @param codiceCategoria the primary key of the categorie merceologiche articoli
	* @return the categorie merceologiche articoli
	* @throws it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException if a categorie merceologiche articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.CategorieMerceologicheArticoli findByPrimaryKey(
		java.lang.String codiceCategoria)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException {
		return getPersistence().findByPrimaryKey(codiceCategoria);
	}

	/**
	* Returns the categorie merceologiche articoli with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param codiceCategoria the primary key of the categorie merceologiche articoli
	* @return the categorie merceologiche articoli, or <code>null</code> if a categorie merceologiche articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.CategorieMerceologicheArticoli fetchByPrimaryKey(
		java.lang.String codiceCategoria)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(codiceCategoria);
	}

	/**
	* Returns all the categorie merceologiche articolis.
	*
	* @return the categorie merceologiche articolis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.CategorieMerceologicheArticoli> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the categorie merceologiche articolis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieMerceologicheArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of categorie merceologiche articolis
	* @param end the upper bound of the range of categorie merceologiche articolis (not inclusive)
	* @return the range of categorie merceologiche articolis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.CategorieMerceologicheArticoli> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the categorie merceologiche articolis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieMerceologicheArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of categorie merceologiche articolis
	* @param end the upper bound of the range of categorie merceologiche articolis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of categorie merceologiche articolis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.CategorieMerceologicheArticoli> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the categorie merceologiche articolis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of categorie merceologiche articolis.
	*
	* @return the number of categorie merceologiche articolis
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CategorieMerceologicheArticoliPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CategorieMerceologicheArticoliPersistence)PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
					CategorieMerceologicheArticoliPersistence.class.getName());

			ReferenceRegistry.registerReference(CategorieMerceologicheArticoliUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(
		CategorieMerceologicheArticoliPersistence persistence) {
	}

	private static CategorieMerceologicheArticoliPersistence _persistence;
}