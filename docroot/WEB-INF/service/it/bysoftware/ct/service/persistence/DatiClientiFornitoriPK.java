/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Mario Torrisi
 * @generated
 */
public class DatiClientiFornitoriPK implements Comparable<DatiClientiFornitoriPK>,
	Serializable {
	public boolean tipoSoggetto;
	public String codiceSoggetto;

	public DatiClientiFornitoriPK() {
	}

	public DatiClientiFornitoriPK(boolean tipoSoggetto, String codiceSoggetto) {
		this.tipoSoggetto = tipoSoggetto;
		this.codiceSoggetto = codiceSoggetto;
	}

	public boolean getTipoSoggetto() {
		return tipoSoggetto;
	}

	public boolean isTipoSoggetto() {
		return tipoSoggetto;
	}

	public void setTipoSoggetto(boolean tipoSoggetto) {
		this.tipoSoggetto = tipoSoggetto;
	}

	public String getCodiceSoggetto() {
		return codiceSoggetto;
	}

	public void setCodiceSoggetto(String codiceSoggetto) {
		this.codiceSoggetto = codiceSoggetto;
	}

	@Override
	public int compareTo(DatiClientiFornitoriPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (!tipoSoggetto && pk.tipoSoggetto) {
			value = -1;
		}
		else if (tipoSoggetto && !pk.tipoSoggetto) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		value = codiceSoggetto.compareTo(pk.codiceSoggetto);

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DatiClientiFornitoriPK)) {
			return false;
		}

		DatiClientiFornitoriPK pk = (DatiClientiFornitoriPK)obj;

		if ((tipoSoggetto == pk.tipoSoggetto) &&
				(codiceSoggetto.equals(pk.codiceSoggetto))) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(tipoSoggetto) + String.valueOf(codiceSoggetto)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("tipoSoggetto");
		sb.append(StringPool.EQUAL);
		sb.append(tipoSoggetto);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("codiceSoggetto");
		sb.append(StringPool.EQUAL);
		sb.append(codiceSoggetto);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}