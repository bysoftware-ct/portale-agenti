/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.bysoftware.ct.model.AnagraficaAgenti;

import java.util.List;

/**
 * The persistence utility for the anagrafica agenti service. This utility wraps {@link AnagraficaAgentiPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Mario Torrisi
 * @see AnagraficaAgentiPersistence
 * @see AnagraficaAgentiPersistenceImpl
 * @generated
 */
public class AnagraficaAgentiUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(AnagraficaAgenti anagraficaAgenti) {
		getPersistence().clearCache(anagraficaAgenti);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<AnagraficaAgenti> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<AnagraficaAgenti> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<AnagraficaAgenti> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static AnagraficaAgenti update(AnagraficaAgenti anagraficaAgenti)
		throws SystemException {
		return getPersistence().update(anagraficaAgenti);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static AnagraficaAgenti update(AnagraficaAgenti anagraficaAgenti,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(anagraficaAgenti, serviceContext);
	}

	/**
	* Caches the anagrafica agenti in the entity cache if it is enabled.
	*
	* @param anagraficaAgenti the anagrafica agenti
	*/
	public static void cacheResult(
		it.bysoftware.ct.model.AnagraficaAgenti anagraficaAgenti) {
		getPersistence().cacheResult(anagraficaAgenti);
	}

	/**
	* Caches the anagrafica agentis in the entity cache if it is enabled.
	*
	* @param anagraficaAgentis the anagrafica agentis
	*/
	public static void cacheResult(
		java.util.List<it.bysoftware.ct.model.AnagraficaAgenti> anagraficaAgentis) {
		getPersistence().cacheResult(anagraficaAgentis);
	}

	/**
	* Creates a new anagrafica agenti with the primary key. Does not add the anagrafica agenti to the database.
	*
	* @param codiceAgente the primary key for the new anagrafica agenti
	* @return the new anagrafica agenti
	*/
	public static it.bysoftware.ct.model.AnagraficaAgenti create(
		java.lang.String codiceAgente) {
		return getPersistence().create(codiceAgente);
	}

	/**
	* Removes the anagrafica agenti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param codiceAgente the primary key of the anagrafica agenti
	* @return the anagrafica agenti that was removed
	* @throws it.bysoftware.ct.NoSuchAnagraficaAgentiException if a anagrafica agenti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.AnagraficaAgenti remove(
		java.lang.String codiceAgente)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchAnagraficaAgentiException {
		return getPersistence().remove(codiceAgente);
	}

	public static it.bysoftware.ct.model.AnagraficaAgenti updateImpl(
		it.bysoftware.ct.model.AnagraficaAgenti anagraficaAgenti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(anagraficaAgenti);
	}

	/**
	* Returns the anagrafica agenti with the primary key or throws a {@link it.bysoftware.ct.NoSuchAnagraficaAgentiException} if it could not be found.
	*
	* @param codiceAgente the primary key of the anagrafica agenti
	* @return the anagrafica agenti
	* @throws it.bysoftware.ct.NoSuchAnagraficaAgentiException if a anagrafica agenti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.AnagraficaAgenti findByPrimaryKey(
		java.lang.String codiceAgente)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.bysoftware.ct.NoSuchAnagraficaAgentiException {
		return getPersistence().findByPrimaryKey(codiceAgente);
	}

	/**
	* Returns the anagrafica agenti with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param codiceAgente the primary key of the anagrafica agenti
	* @return the anagrafica agenti, or <code>null</code> if a anagrafica agenti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.AnagraficaAgenti fetchByPrimaryKey(
		java.lang.String codiceAgente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(codiceAgente);
	}

	/**
	* Returns all the anagrafica agentis.
	*
	* @return the anagrafica agentis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.AnagraficaAgenti> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the anagrafica agentis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficaAgentiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of anagrafica agentis
	* @param end the upper bound of the range of anagrafica agentis (not inclusive)
	* @return the range of anagrafica agentis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.AnagraficaAgenti> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the anagrafica agentis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.AnagraficaAgentiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of anagrafica agentis
	* @param end the upper bound of the range of anagrafica agentis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of anagrafica agentis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.AnagraficaAgenti> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the anagrafica agentis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of anagrafica agentis.
	*
	* @return the number of anagrafica agentis
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static AnagraficaAgentiPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (AnagraficaAgentiPersistence)PortletBeanLocatorUtil.locate(it.bysoftware.ct.service.ClpSerializer.getServletContextName(),
					AnagraficaAgentiPersistence.class.getName());

			ReferenceRegistry.registerReference(AnagraficaAgentiUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(AnagraficaAgentiPersistence persistence) {
	}

	private static AnagraficaAgentiPersistence _persistence;
}