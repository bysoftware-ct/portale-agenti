/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ListiniPrezziArticoliLocalService}.
 *
 * @author Mario Torrisi
 * @see ListiniPrezziArticoliLocalService
 * @generated
 */
public class ListiniPrezziArticoliLocalServiceWrapper
	implements ListiniPrezziArticoliLocalService,
		ServiceWrapper<ListiniPrezziArticoliLocalService> {
	public ListiniPrezziArticoliLocalServiceWrapper(
		ListiniPrezziArticoliLocalService listiniPrezziArticoliLocalService) {
		_listiniPrezziArticoliLocalService = listiniPrezziArticoliLocalService;
	}

	/**
	* Adds the listini prezzi articoli to the database. Also notifies the appropriate model listeners.
	*
	* @param listiniPrezziArticoli the listini prezzi articoli
	* @return the listini prezzi articoli that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.ListiniPrezziArticoli addListiniPrezziArticoli(
		it.bysoftware.ct.model.ListiniPrezziArticoli listiniPrezziArticoli)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listiniPrezziArticoliLocalService.addListiniPrezziArticoli(listiniPrezziArticoli);
	}

	/**
	* Creates a new listini prezzi articoli with the primary key. Does not add the listini prezzi articoli to the database.
	*
	* @param listiniPrezziArticoliPK the primary key for the new listini prezzi articoli
	* @return the new listini prezzi articoli
	*/
	@Override
	public it.bysoftware.ct.model.ListiniPrezziArticoli createListiniPrezziArticoli(
		it.bysoftware.ct.service.persistence.ListiniPrezziArticoliPK listiniPrezziArticoliPK) {
		return _listiniPrezziArticoliLocalService.createListiniPrezziArticoli(listiniPrezziArticoliPK);
	}

	/**
	* Deletes the listini prezzi articoli with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param listiniPrezziArticoliPK the primary key of the listini prezzi articoli
	* @return the listini prezzi articoli that was removed
	* @throws PortalException if a listini prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.ListiniPrezziArticoli deleteListiniPrezziArticoli(
		it.bysoftware.ct.service.persistence.ListiniPrezziArticoliPK listiniPrezziArticoliPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _listiniPrezziArticoliLocalService.deleteListiniPrezziArticoli(listiniPrezziArticoliPK);
	}

	/**
	* Deletes the listini prezzi articoli from the database. Also notifies the appropriate model listeners.
	*
	* @param listiniPrezziArticoli the listini prezzi articoli
	* @return the listini prezzi articoli that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.ListiniPrezziArticoli deleteListiniPrezziArticoli(
		it.bysoftware.ct.model.ListiniPrezziArticoli listiniPrezziArticoli)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listiniPrezziArticoliLocalService.deleteListiniPrezziArticoli(listiniPrezziArticoli);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _listiniPrezziArticoliLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listiniPrezziArticoliLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _listiniPrezziArticoliLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listiniPrezziArticoliLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listiniPrezziArticoliLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listiniPrezziArticoliLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.bysoftware.ct.model.ListiniPrezziArticoli fetchListiniPrezziArticoli(
		it.bysoftware.ct.service.persistence.ListiniPrezziArticoliPK listiniPrezziArticoliPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listiniPrezziArticoliLocalService.fetchListiniPrezziArticoli(listiniPrezziArticoliPK);
	}

	/**
	* Returns the listini prezzi articoli with the primary key.
	*
	* @param listiniPrezziArticoliPK the primary key of the listini prezzi articoli
	* @return the listini prezzi articoli
	* @throws PortalException if a listini prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.ListiniPrezziArticoli getListiniPrezziArticoli(
		it.bysoftware.ct.service.persistence.ListiniPrezziArticoliPK listiniPrezziArticoliPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _listiniPrezziArticoliLocalService.getListiniPrezziArticoli(listiniPrezziArticoliPK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _listiniPrezziArticoliLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the listini prezzi articolis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListiniPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of listini prezzi articolis
	* @param end the upper bound of the range of listini prezzi articolis (not inclusive)
	* @return the range of listini prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.bysoftware.ct.model.ListiniPrezziArticoli> getListiniPrezziArticolis(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listiniPrezziArticoliLocalService.getListiniPrezziArticolis(start,
			end);
	}

	/**
	* Returns the number of listini prezzi articolis.
	*
	* @return the number of listini prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getListiniPrezziArticolisCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listiniPrezziArticoliLocalService.getListiniPrezziArticolisCount();
	}

	/**
	* Updates the listini prezzi articoli in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param listiniPrezziArticoli the listini prezzi articoli
	* @return the listini prezzi articoli that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.ListiniPrezziArticoli updateListiniPrezziArticoli(
		it.bysoftware.ct.model.ListiniPrezziArticoli listiniPrezziArticoli)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listiniPrezziArticoliLocalService.updateListiniPrezziArticoli(listiniPrezziArticoli);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _listiniPrezziArticoliLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_listiniPrezziArticoliLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _listiniPrezziArticoliLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	@Override
	public it.bysoftware.ct.model.ListiniPrezziArticoli getListinoDedicato(
		java.lang.String codiceArticolo, java.lang.String codiceVariante,
		java.lang.String codiceSoggetto) {
		return _listiniPrezziArticoliLocalService.getListinoDedicato(codiceArticolo,
			codiceVariante, codiceSoggetto);
	}

	@Override
	public it.bysoftware.ct.model.ListiniPrezziArticoli getListinoGenerico(
		java.lang.String codiceArticolo, java.lang.String codiceVariante) {
		return _listiniPrezziArticoliLocalService.getListinoGenerico(codiceArticolo,
			codiceVariante);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ListiniPrezziArticoliLocalService getWrappedListiniPrezziArticoliLocalService() {
		return _listiniPrezziArticoliLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedListiniPrezziArticoliLocalService(
		ListiniPrezziArticoliLocalService listiniPrezziArticoliLocalService) {
		_listiniPrezziArticoliLocalService = listiniPrezziArticoliLocalService;
	}

	@Override
	public ListiniPrezziArticoliLocalService getWrappedService() {
		return _listiniPrezziArticoliLocalService;
	}

	@Override
	public void setWrappedService(
		ListiniPrezziArticoliLocalService listiniPrezziArticoliLocalService) {
		_listiniPrezziArticoliLocalService = listiniPrezziArticoliLocalService;
	}

	private ListiniPrezziArticoliLocalService _listiniPrezziArticoliLocalService;
}