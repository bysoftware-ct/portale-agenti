/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link RigheOrdiniClientiLocalService}.
 *
 * @author Mario Torrisi
 * @see RigheOrdiniClientiLocalService
 * @generated
 */
public class RigheOrdiniClientiLocalServiceWrapper
	implements RigheOrdiniClientiLocalService,
		ServiceWrapper<RigheOrdiniClientiLocalService> {
	public RigheOrdiniClientiLocalServiceWrapper(
		RigheOrdiniClientiLocalService righeOrdiniClientiLocalService) {
		_righeOrdiniClientiLocalService = righeOrdiniClientiLocalService;
	}

	/**
	* Adds the righe ordini clienti to the database. Also notifies the appropriate model listeners.
	*
	* @param righeOrdiniClienti the righe ordini clienti
	* @return the righe ordini clienti that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.RigheOrdiniClienti addRigheOrdiniClienti(
		it.bysoftware.ct.model.RigheOrdiniClienti righeOrdiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _righeOrdiniClientiLocalService.addRigheOrdiniClienti(righeOrdiniClienti);
	}

	/**
	* Creates a new righe ordini clienti with the primary key. Does not add the righe ordini clienti to the database.
	*
	* @param righeOrdiniClientiPK the primary key for the new righe ordini clienti
	* @return the new righe ordini clienti
	*/
	@Override
	public it.bysoftware.ct.model.RigheOrdiniClienti createRigheOrdiniClienti(
		it.bysoftware.ct.service.persistence.RigheOrdiniClientiPK righeOrdiniClientiPK) {
		return _righeOrdiniClientiLocalService.createRigheOrdiniClienti(righeOrdiniClientiPK);
	}

	/**
	* Deletes the righe ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param righeOrdiniClientiPK the primary key of the righe ordini clienti
	* @return the righe ordini clienti that was removed
	* @throws PortalException if a righe ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.RigheOrdiniClienti deleteRigheOrdiniClienti(
		it.bysoftware.ct.service.persistence.RigheOrdiniClientiPK righeOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _righeOrdiniClientiLocalService.deleteRigheOrdiniClienti(righeOrdiniClientiPK);
	}

	/**
	* Deletes the righe ordini clienti from the database. Also notifies the appropriate model listeners.
	*
	* @param righeOrdiniClienti the righe ordini clienti
	* @return the righe ordini clienti that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.RigheOrdiniClienti deleteRigheOrdiniClienti(
		it.bysoftware.ct.model.RigheOrdiniClienti righeOrdiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _righeOrdiniClientiLocalService.deleteRigheOrdiniClienti(righeOrdiniClienti);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _righeOrdiniClientiLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _righeOrdiniClientiLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _righeOrdiniClientiLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _righeOrdiniClientiLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _righeOrdiniClientiLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _righeOrdiniClientiLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.bysoftware.ct.model.RigheOrdiniClienti fetchRigheOrdiniClienti(
		it.bysoftware.ct.service.persistence.RigheOrdiniClientiPK righeOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _righeOrdiniClientiLocalService.fetchRigheOrdiniClienti(righeOrdiniClientiPK);
	}

	/**
	* Returns the righe ordini clienti with the primary key.
	*
	* @param righeOrdiniClientiPK the primary key of the righe ordini clienti
	* @return the righe ordini clienti
	* @throws PortalException if a righe ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.RigheOrdiniClienti getRigheOrdiniClienti(
		it.bysoftware.ct.service.persistence.RigheOrdiniClientiPK righeOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _righeOrdiniClientiLocalService.getRigheOrdiniClienti(righeOrdiniClientiPK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _righeOrdiniClientiLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the righe ordini clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.RigheOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of righe ordini clientis
	* @param end the upper bound of the range of righe ordini clientis (not inclusive)
	* @return the range of righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.bysoftware.ct.model.RigheOrdiniClienti> getRigheOrdiniClientis(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _righeOrdiniClientiLocalService.getRigheOrdiniClientis(start, end);
	}

	/**
	* Returns the number of righe ordini clientis.
	*
	* @return the number of righe ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getRigheOrdiniClientisCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _righeOrdiniClientiLocalService.getRigheOrdiniClientisCount();
	}

	/**
	* Updates the righe ordini clienti in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param righeOrdiniClienti the righe ordini clienti
	* @return the righe ordini clienti that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.RigheOrdiniClienti updateRigheOrdiniClienti(
		it.bysoftware.ct.model.RigheOrdiniClienti righeOrdiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _righeOrdiniClientiLocalService.updateRigheOrdiniClienti(righeOrdiniClienti);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _righeOrdiniClientiLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_righeOrdiniClientiLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _righeOrdiniClientiLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	@Override
	public java.util.List<it.bysoftware.ct.model.RigheOrdiniClienti> getRigheOrdineByTestata(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, java.lang.String codiceDeposito,
		int tipoOrdine, int numeroOrdine) {
		return _righeOrdiniClientiLocalService.getRigheOrdineByTestata(anno,
			codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
			numeroOrdine);
	}

	@Override
	public boolean checkRigheOrdineByTestata(int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine) {
		return _righeOrdiniClientiLocalService.checkRigheOrdineByTestata(anno,
			codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
			numeroOrdine);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public RigheOrdiniClientiLocalService getWrappedRigheOrdiniClientiLocalService() {
		return _righeOrdiniClientiLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedRigheOrdiniClientiLocalService(
		RigheOrdiniClientiLocalService righeOrdiniClientiLocalService) {
		_righeOrdiniClientiLocalService = righeOrdiniClientiLocalService;
	}

	@Override
	public RigheOrdiniClientiLocalService getWrappedService() {
		return _righeOrdiniClientiLocalService;
	}

	@Override
	public void setWrappedService(
		RigheOrdiniClientiLocalService righeOrdiniClientiLocalService) {
		_righeOrdiniClientiLocalService = righeOrdiniClientiLocalService;
	}

	private RigheOrdiniClientiLocalService _righeOrdiniClientiLocalService;
}