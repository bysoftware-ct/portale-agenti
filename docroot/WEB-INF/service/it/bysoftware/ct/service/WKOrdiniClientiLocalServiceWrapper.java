/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link WKOrdiniClientiLocalService}.
 *
 * @author Mario Torrisi
 * @see WKOrdiniClientiLocalService
 * @generated
 */
public class WKOrdiniClientiLocalServiceWrapper
	implements WKOrdiniClientiLocalService,
		ServiceWrapper<WKOrdiniClientiLocalService> {
	public WKOrdiniClientiLocalServiceWrapper(
		WKOrdiniClientiLocalService wkOrdiniClientiLocalService) {
		_wkOrdiniClientiLocalService = wkOrdiniClientiLocalService;
	}

	/**
	* Adds the w k ordini clienti to the database. Also notifies the appropriate model listeners.
	*
	* @param wkOrdiniClienti the w k ordini clienti
	* @return the w k ordini clienti that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.WKOrdiniClienti addWKOrdiniClienti(
		it.bysoftware.ct.model.WKOrdiniClienti wkOrdiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _wkOrdiniClientiLocalService.addWKOrdiniClienti(wkOrdiniClienti);
	}

	/**
	* Creates a new w k ordini clienti with the primary key. Does not add the w k ordini clienti to the database.
	*
	* @param wkOrdiniClientiPK the primary key for the new w k ordini clienti
	* @return the new w k ordini clienti
	*/
	@Override
	public it.bysoftware.ct.model.WKOrdiniClienti createWKOrdiniClienti(
		it.bysoftware.ct.service.persistence.WKOrdiniClientiPK wkOrdiniClientiPK) {
		return _wkOrdiniClientiLocalService.createWKOrdiniClienti(wkOrdiniClientiPK);
	}

	/**
	* Deletes the w k ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param wkOrdiniClientiPK the primary key of the w k ordini clienti
	* @return the w k ordini clienti that was removed
	* @throws PortalException if a w k ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.WKOrdiniClienti deleteWKOrdiniClienti(
		it.bysoftware.ct.service.persistence.WKOrdiniClientiPK wkOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _wkOrdiniClientiLocalService.deleteWKOrdiniClienti(wkOrdiniClientiPK);
	}

	/**
	* Deletes the w k ordini clienti from the database. Also notifies the appropriate model listeners.
	*
	* @param wkOrdiniClienti the w k ordini clienti
	* @return the w k ordini clienti that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.WKOrdiniClienti deleteWKOrdiniClienti(
		it.bysoftware.ct.model.WKOrdiniClienti wkOrdiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _wkOrdiniClientiLocalService.deleteWKOrdiniClienti(wkOrdiniClienti);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _wkOrdiniClientiLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _wkOrdiniClientiLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _wkOrdiniClientiLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _wkOrdiniClientiLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _wkOrdiniClientiLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _wkOrdiniClientiLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.bysoftware.ct.model.WKOrdiniClienti fetchWKOrdiniClienti(
		it.bysoftware.ct.service.persistence.WKOrdiniClientiPK wkOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _wkOrdiniClientiLocalService.fetchWKOrdiniClienti(wkOrdiniClientiPK);
	}

	/**
	* Returns the w k ordini clienti with the primary key.
	*
	* @param wkOrdiniClientiPK the primary key of the w k ordini clienti
	* @return the w k ordini clienti
	* @throws PortalException if a w k ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.WKOrdiniClienti getWKOrdiniClienti(
		it.bysoftware.ct.service.persistence.WKOrdiniClientiPK wkOrdiniClientiPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _wkOrdiniClientiLocalService.getWKOrdiniClienti(wkOrdiniClientiPK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _wkOrdiniClientiLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the w k ordini clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.WKOrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of w k ordini clientis
	* @param end the upper bound of the range of w k ordini clientis (not inclusive)
	* @return the range of w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> getWKOrdiniClientis(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _wkOrdiniClientiLocalService.getWKOrdiniClientis(start, end);
	}

	/**
	* Returns the number of w k ordini clientis.
	*
	* @return the number of w k ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getWKOrdiniClientisCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _wkOrdiniClientiLocalService.getWKOrdiniClientisCount();
	}

	/**
	* Updates the w k ordini clienti in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param wkOrdiniClienti the w k ordini clienti
	* @return the w k ordini clienti that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.WKOrdiniClienti updateWKOrdiniClienti(
		it.bysoftware.ct.model.WKOrdiniClienti wkOrdiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _wkOrdiniClientiLocalService.updateWKOrdiniClienti(wkOrdiniClienti);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _wkOrdiniClientiLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_wkOrdiniClientiLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _wkOrdiniClientiLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public int getNumeroOrdine(int anno, int tipoOrdine) {
		return _wkOrdiniClientiLocalService.getNumeroOrdine(anno, tipoOrdine);
	}

	@Override
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> getOrdiniByCodiceCliente(
		java.lang.String codiceCliente) {
		return _wkOrdiniClientiLocalService.getOrdiniByCodiceCliente(codiceCliente);
	}

	@Override
	public int getOrdiniClientiAcquisitiCount() {
		return _wkOrdiniClientiLocalService.getOrdiniClientiAcquisitiCount();
	}

	@Override
	public int getOrdiniClientiDaAcquisireCount() {
		return _wkOrdiniClientiLocalService.getOrdiniClientiDaAcquisireCount();
	}

	@Override
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> getOrdiniByStatoOrdine(
		boolean stato) {
		return _wkOrdiniClientiLocalService.getOrdiniByStatoOrdine(stato);
	}

	@Override
	public java.util.List<it.bysoftware.ct.model.WKOrdiniClienti> searchOrdini(
		java.lang.Boolean statoOrdine, java.lang.String codiceCliente,
		java.sql.Date from, java.sql.Date to, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator) {
		return _wkOrdiniClientiLocalService.searchOrdini(statoOrdine,
			codiceCliente, from, to, start, end, orderByComparator);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public WKOrdiniClientiLocalService getWrappedWKOrdiniClientiLocalService() {
		return _wkOrdiniClientiLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedWKOrdiniClientiLocalService(
		WKOrdiniClientiLocalService wkOrdiniClientiLocalService) {
		_wkOrdiniClientiLocalService = wkOrdiniClientiLocalService;
	}

	@Override
	public WKOrdiniClientiLocalService getWrappedService() {
		return _wkOrdiniClientiLocalService;
	}

	@Override
	public void setWrappedService(
		WKOrdiniClientiLocalService wkOrdiniClientiLocalService) {
		_wkOrdiniClientiLocalService = wkOrdiniClientiLocalService;
	}

	private WKOrdiniClientiLocalService _wkOrdiniClientiLocalService;
}