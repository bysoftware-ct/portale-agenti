/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service.messaging;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;

import it.bysoftware.ct.service.AnagraficaAgentiLocalServiceUtil;
import it.bysoftware.ct.service.AnagraficaAgentiServiceUtil;
import it.bysoftware.ct.service.AnagraficaClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.AnagraficaClientiFornitoriServiceUtil;
import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.service.ArticoliServiceUtil;
import it.bysoftware.ct.service.CategorieMerceologicheArticoliLocalServiceUtil;
import it.bysoftware.ct.service.CategorieMerceologicheArticoliServiceUtil;
import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.DatiClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.DatiClientiFornitoriServiceUtil;
import it.bysoftware.ct.service.DatiCostantiClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.DatiCostantiClientiFornitoriServiceUtil;
import it.bysoftware.ct.service.EstrattoContoLocalServiceUtil;
import it.bysoftware.ct.service.EstrattoContoServiceUtil;
import it.bysoftware.ct.service.EvasioniOrdiniLocalServiceUtil;
import it.bysoftware.ct.service.EvasioniOrdiniServiceUtil;
import it.bysoftware.ct.service.ListiniPrezziArticoliLocalServiceUtil;
import it.bysoftware.ct.service.ListiniPrezziArticoliServiceUtil;
import it.bysoftware.ct.service.MovimentiMagazzinoLocalServiceUtil;
import it.bysoftware.ct.service.MovimentiMagazzinoServiceUtil;
import it.bysoftware.ct.service.OrdinatoLocalServiceUtil;
import it.bysoftware.ct.service.OrdinatoServiceUtil;
import it.bysoftware.ct.service.OrdiniClientiLocalServiceUtil;
import it.bysoftware.ct.service.OrdiniClientiServiceUtil;
import it.bysoftware.ct.service.PianoPagamentiLocalServiceUtil;
import it.bysoftware.ct.service.PianoPagamentiServiceUtil;
import it.bysoftware.ct.service.RigheFattureVeditaLocalServiceUtil;
import it.bysoftware.ct.service.RigheFattureVeditaServiceUtil;
import it.bysoftware.ct.service.RigheOrdiniClientiLocalServiceUtil;
import it.bysoftware.ct.service.RigheOrdiniClientiServiceUtil;
import it.bysoftware.ct.service.ScontiArticoliLocalServiceUtil;
import it.bysoftware.ct.service.ScontiArticoliServiceUtil;
import it.bysoftware.ct.service.TestataFattureClientiLocalServiceUtil;
import it.bysoftware.ct.service.TestataFattureClientiServiceUtil;
import it.bysoftware.ct.service.UltimiPrezziArticoliLocalServiceUtil;
import it.bysoftware.ct.service.UltimiPrezziArticoliServiceUtil;
import it.bysoftware.ct.service.VociIvaLocalServiceUtil;
import it.bysoftware.ct.service.VociIvaServiceUtil;
import it.bysoftware.ct.service.WKOrdiniClientiLocalServiceUtil;
import it.bysoftware.ct.service.WKOrdiniClientiServiceUtil;
import it.bysoftware.ct.service.WKRigheOrdiniClientiLocalServiceUtil;
import it.bysoftware.ct.service.WKRigheOrdiniClientiServiceUtil;

/**
 * @author Mario Torrisi
 */
public class ClpMessageListener extends BaseMessageListener {
	public static String getServletContextName() {
		return ClpSerializer.getServletContextName();
	}

	@Override
	protected void doReceive(Message message) throws Exception {
		String command = message.getString("command");
		String servletContextName = message.getString("servletContextName");

		if (command.equals("undeploy") &&
				servletContextName.equals(getServletContextName())) {
			AnagraficaAgentiLocalServiceUtil.clearService();

			AnagraficaAgentiServiceUtil.clearService();
			AnagraficaClientiFornitoriLocalServiceUtil.clearService();

			AnagraficaClientiFornitoriServiceUtil.clearService();
			ArticoliLocalServiceUtil.clearService();

			ArticoliServiceUtil.clearService();
			CategorieMerceologicheArticoliLocalServiceUtil.clearService();

			CategorieMerceologicheArticoliServiceUtil.clearService();
			DatiClientiFornitoriLocalServiceUtil.clearService();

			DatiClientiFornitoriServiceUtil.clearService();
			DatiCostantiClientiFornitoriLocalServiceUtil.clearService();

			DatiCostantiClientiFornitoriServiceUtil.clearService();
			EstrattoContoLocalServiceUtil.clearService();

			EstrattoContoServiceUtil.clearService();
			EvasioniOrdiniLocalServiceUtil.clearService();

			EvasioniOrdiniServiceUtil.clearService();
			ListiniPrezziArticoliLocalServiceUtil.clearService();

			ListiniPrezziArticoliServiceUtil.clearService();
			MovimentiMagazzinoLocalServiceUtil.clearService();

			MovimentiMagazzinoServiceUtil.clearService();
			OrdinatoLocalServiceUtil.clearService();

			OrdinatoServiceUtil.clearService();
			OrdiniClientiLocalServiceUtil.clearService();

			OrdiniClientiServiceUtil.clearService();
			PianoPagamentiLocalServiceUtil.clearService();

			PianoPagamentiServiceUtil.clearService();
			RigheFattureVeditaLocalServiceUtil.clearService();

			RigheFattureVeditaServiceUtil.clearService();
			RigheOrdiniClientiLocalServiceUtil.clearService();

			RigheOrdiniClientiServiceUtil.clearService();
			ScontiArticoliLocalServiceUtil.clearService();

			ScontiArticoliServiceUtil.clearService();
			TestataFattureClientiLocalServiceUtil.clearService();

			TestataFattureClientiServiceUtil.clearService();
			UltimiPrezziArticoliLocalServiceUtil.clearService();

			UltimiPrezziArticoliServiceUtil.clearService();
			VociIvaLocalServiceUtil.clearService();

			VociIvaServiceUtil.clearService();
			WKOrdiniClientiLocalServiceUtil.clearService();

			WKOrdiniClientiServiceUtil.clearService();
			WKRigheOrdiniClientiLocalServiceUtil.clearService();

			WKRigheOrdiniClientiServiceUtil.clearService();
		}
	}
}