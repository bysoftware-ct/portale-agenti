/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UltimiPrezziArticoliLocalService}.
 *
 * @author Mario Torrisi
 * @see UltimiPrezziArticoliLocalService
 * @generated
 */
public class UltimiPrezziArticoliLocalServiceWrapper
	implements UltimiPrezziArticoliLocalService,
		ServiceWrapper<UltimiPrezziArticoliLocalService> {
	public UltimiPrezziArticoliLocalServiceWrapper(
		UltimiPrezziArticoliLocalService ultimiPrezziArticoliLocalService) {
		_ultimiPrezziArticoliLocalService = ultimiPrezziArticoliLocalService;
	}

	/**
	* Adds the ultimi prezzi articoli to the database. Also notifies the appropriate model listeners.
	*
	* @param ultimiPrezziArticoli the ultimi prezzi articoli
	* @return the ultimi prezzi articoli that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.UltimiPrezziArticoli addUltimiPrezziArticoli(
		it.bysoftware.ct.model.UltimiPrezziArticoli ultimiPrezziArticoli)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ultimiPrezziArticoliLocalService.addUltimiPrezziArticoli(ultimiPrezziArticoli);
	}

	/**
	* Creates a new ultimi prezzi articoli with the primary key. Does not add the ultimi prezzi articoli to the database.
	*
	* @param ultimiPrezziArticoliPK the primary key for the new ultimi prezzi articoli
	* @return the new ultimi prezzi articoli
	*/
	@Override
	public it.bysoftware.ct.model.UltimiPrezziArticoli createUltimiPrezziArticoli(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK) {
		return _ultimiPrezziArticoliLocalService.createUltimiPrezziArticoli(ultimiPrezziArticoliPK);
	}

	/**
	* Deletes the ultimi prezzi articoli with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ultimiPrezziArticoliPK the primary key of the ultimi prezzi articoli
	* @return the ultimi prezzi articoli that was removed
	* @throws PortalException if a ultimi prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.UltimiPrezziArticoli deleteUltimiPrezziArticoli(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ultimiPrezziArticoliLocalService.deleteUltimiPrezziArticoli(ultimiPrezziArticoliPK);
	}

	/**
	* Deletes the ultimi prezzi articoli from the database. Also notifies the appropriate model listeners.
	*
	* @param ultimiPrezziArticoli the ultimi prezzi articoli
	* @return the ultimi prezzi articoli that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.UltimiPrezziArticoli deleteUltimiPrezziArticoli(
		it.bysoftware.ct.model.UltimiPrezziArticoli ultimiPrezziArticoli)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ultimiPrezziArticoliLocalService.deleteUltimiPrezziArticoli(ultimiPrezziArticoli);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _ultimiPrezziArticoliLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ultimiPrezziArticoliLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _ultimiPrezziArticoliLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ultimiPrezziArticoliLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ultimiPrezziArticoliLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ultimiPrezziArticoliLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.bysoftware.ct.model.UltimiPrezziArticoli fetchUltimiPrezziArticoli(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ultimiPrezziArticoliLocalService.fetchUltimiPrezziArticoli(ultimiPrezziArticoliPK);
	}

	/**
	* Returns the ultimi prezzi articoli with the primary key.
	*
	* @param ultimiPrezziArticoliPK the primary key of the ultimi prezzi articoli
	* @return the ultimi prezzi articoli
	* @throws PortalException if a ultimi prezzi articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.UltimiPrezziArticoli getUltimiPrezziArticoli(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK ultimiPrezziArticoliPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ultimiPrezziArticoliLocalService.getUltimiPrezziArticoli(ultimiPrezziArticoliPK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ultimiPrezziArticoliLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the ultimi prezzi articolis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.UltimiPrezziArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ultimi prezzi articolis
	* @param end the upper bound of the range of ultimi prezzi articolis (not inclusive)
	* @return the range of ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> getUltimiPrezziArticolis(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ultimiPrezziArticoliLocalService.getUltimiPrezziArticolis(start,
			end);
	}

	/**
	* Returns the number of ultimi prezzi articolis.
	*
	* @return the number of ultimi prezzi articolis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getUltimiPrezziArticolisCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ultimiPrezziArticoliLocalService.getUltimiPrezziArticolisCount();
	}

	/**
	* Updates the ultimi prezzi articoli in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ultimiPrezziArticoli the ultimi prezzi articoli
	* @return the ultimi prezzi articoli that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.UltimiPrezziArticoli updateUltimiPrezziArticoli(
		it.bysoftware.ct.model.UltimiPrezziArticoli ultimiPrezziArticoli)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ultimiPrezziArticoliLocalService.updateUltimiPrezziArticoli(ultimiPrezziArticoli);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _ultimiPrezziArticoliLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_ultimiPrezziArticoliLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _ultimiPrezziArticoliLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	@Override
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> getUltimeCondizioniACliente(
		java.lang.String codiceSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante) {
		return _ultimiPrezziArticoliLocalService.getUltimeCondizioniACliente(codiceSoggetto,
			codiceArticolo, codiceVariante);
	}

	@Override
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> getUltimeCondizioniDaFornitore(
		java.lang.String codiceArticolo, java.lang.String codiceVariante) {
		return _ultimiPrezziArticoliLocalService.getUltimeCondizioniDaFornitore(codiceArticolo,
			codiceVariante);
	}

	@Override
	public java.util.List<it.bysoftware.ct.model.UltimiPrezziArticoli> searchUltimeCondizioniACliente(
		java.lang.String codiceSoggetto, java.lang.String codiceArticolo,
		java.lang.String codiceVariante, boolean andSearch, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator) {
		return _ultimiPrezziArticoliLocalService.searchUltimeCondizioniACliente(codiceSoggetto,
			codiceArticolo, codiceVariante, andSearch, start, end,
			orderByComparator);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public UltimiPrezziArticoliLocalService getWrappedUltimiPrezziArticoliLocalService() {
		return _ultimiPrezziArticoliLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedUltimiPrezziArticoliLocalService(
		UltimiPrezziArticoliLocalService ultimiPrezziArticoliLocalService) {
		_ultimiPrezziArticoliLocalService = ultimiPrezziArticoliLocalService;
	}

	@Override
	public UltimiPrezziArticoliLocalService getWrappedService() {
		return _ultimiPrezziArticoliLocalService;
	}

	@Override
	public void setWrappedService(
		UltimiPrezziArticoliLocalService ultimiPrezziArticoliLocalService) {
		_ultimiPrezziArticoliLocalService = ultimiPrezziArticoliLocalService;
	}

	private UltimiPrezziArticoliLocalService _ultimiPrezziArticoliLocalService;
}