/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link EvasioniOrdiniLocalService}.
 *
 * @author Mario Torrisi
 * @see EvasioniOrdiniLocalService
 * @generated
 */
public class EvasioniOrdiniLocalServiceWrapper
	implements EvasioniOrdiniLocalService,
		ServiceWrapper<EvasioniOrdiniLocalService> {
	public EvasioniOrdiniLocalServiceWrapper(
		EvasioniOrdiniLocalService evasioniOrdiniLocalService) {
		_evasioniOrdiniLocalService = evasioniOrdiniLocalService;
	}

	/**
	* Adds the evasioni ordini to the database. Also notifies the appropriate model listeners.
	*
	* @param evasioniOrdini the evasioni ordini
	* @return the evasioni ordini that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.EvasioniOrdini addEvasioniOrdini(
		it.bysoftware.ct.model.EvasioniOrdini evasioniOrdini)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _evasioniOrdiniLocalService.addEvasioniOrdini(evasioniOrdini);
	}

	/**
	* Creates a new evasioni ordini with the primary key. Does not add the evasioni ordini to the database.
	*
	* @param ID the primary key for the new evasioni ordini
	* @return the new evasioni ordini
	*/
	@Override
	public it.bysoftware.ct.model.EvasioniOrdini createEvasioniOrdini(
		java.lang.String ID) {
		return _evasioniOrdiniLocalService.createEvasioniOrdini(ID);
	}

	/**
	* Deletes the evasioni ordini with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the evasioni ordini
	* @return the evasioni ordini that was removed
	* @throws PortalException if a evasioni ordini with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.EvasioniOrdini deleteEvasioniOrdini(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _evasioniOrdiniLocalService.deleteEvasioniOrdini(ID);
	}

	/**
	* Deletes the evasioni ordini from the database. Also notifies the appropriate model listeners.
	*
	* @param evasioniOrdini the evasioni ordini
	* @return the evasioni ordini that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.EvasioniOrdini deleteEvasioniOrdini(
		it.bysoftware.ct.model.EvasioniOrdini evasioniOrdini)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _evasioniOrdiniLocalService.deleteEvasioniOrdini(evasioniOrdini);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _evasioniOrdiniLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _evasioniOrdiniLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _evasioniOrdiniLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _evasioniOrdiniLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _evasioniOrdiniLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _evasioniOrdiniLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.bysoftware.ct.model.EvasioniOrdini fetchEvasioniOrdini(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _evasioniOrdiniLocalService.fetchEvasioniOrdini(ID);
	}

	/**
	* Returns the evasioni ordini with the primary key.
	*
	* @param ID the primary key of the evasioni ordini
	* @return the evasioni ordini
	* @throws PortalException if a evasioni ordini with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.EvasioniOrdini getEvasioniOrdini(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _evasioniOrdiniLocalService.getEvasioniOrdini(ID);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _evasioniOrdiniLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the evasioni ordinis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of evasioni ordinis
	* @param end the upper bound of the range of evasioni ordinis (not inclusive)
	* @return the range of evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.bysoftware.ct.model.EvasioniOrdini> getEvasioniOrdinis(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _evasioniOrdiniLocalService.getEvasioniOrdinis(start, end);
	}

	/**
	* Returns the number of evasioni ordinis.
	*
	* @return the number of evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getEvasioniOrdinisCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _evasioniOrdiniLocalService.getEvasioniOrdinisCount();
	}

	/**
	* Updates the evasioni ordini in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param evasioniOrdini the evasioni ordini
	* @return the evasioni ordini that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.EvasioniOrdini updateEvasioniOrdini(
		it.bysoftware.ct.model.EvasioniOrdini evasioniOrdini)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _evasioniOrdiniLocalService.updateEvasioniOrdini(evasioniOrdini);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _evasioniOrdiniLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_evasioniOrdiniLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _evasioniOrdiniLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	* Returns all process order for the specified client.
	*
	* @param codiceCliente
	* @return {@link List} of process order for the specified client
	*/
	@Override
	public java.util.List<it.bysoftware.ct.model.EvasioniOrdini> getEvasioniByCodiceCliente(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine) {
		return _evasioniOrdiniLocalService.getEvasioniByCodiceCliente(codiceCliente,
			anno, codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
			numeroOrdine);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public EvasioniOrdiniLocalService getWrappedEvasioniOrdiniLocalService() {
		return _evasioniOrdiniLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedEvasioniOrdiniLocalService(
		EvasioniOrdiniLocalService evasioniOrdiniLocalService) {
		_evasioniOrdiniLocalService = evasioniOrdiniLocalService;
	}

	@Override
	public EvasioniOrdiniLocalService getWrappedService() {
		return _evasioniOrdiniLocalService;
	}

	@Override
	public void setWrappedService(
		EvasioniOrdiniLocalService evasioniOrdiniLocalService) {
		_evasioniOrdiniLocalService = evasioniOrdiniLocalService;
	}

	private EvasioniOrdiniLocalService _evasioniOrdiniLocalService;
}