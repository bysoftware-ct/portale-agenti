/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for EvasioniOrdini. This utility wraps
 * {@link it.bysoftware.ct.service.impl.EvasioniOrdiniLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Mario Torrisi
 * @see EvasioniOrdiniLocalService
 * @see it.bysoftware.ct.service.base.EvasioniOrdiniLocalServiceBaseImpl
 * @see it.bysoftware.ct.service.impl.EvasioniOrdiniLocalServiceImpl
 * @generated
 */
public class EvasioniOrdiniLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.bysoftware.ct.service.impl.EvasioniOrdiniLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the evasioni ordini to the database. Also notifies the appropriate model listeners.
	*
	* @param evasioniOrdini the evasioni ordini
	* @return the evasioni ordini that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EvasioniOrdini addEvasioniOrdini(
		it.bysoftware.ct.model.EvasioniOrdini evasioniOrdini)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addEvasioniOrdini(evasioniOrdini);
	}

	/**
	* Creates a new evasioni ordini with the primary key. Does not add the evasioni ordini to the database.
	*
	* @param ID the primary key for the new evasioni ordini
	* @return the new evasioni ordini
	*/
	public static it.bysoftware.ct.model.EvasioniOrdini createEvasioniOrdini(
		java.lang.String ID) {
		return getService().createEvasioniOrdini(ID);
	}

	/**
	* Deletes the evasioni ordini with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the evasioni ordini
	* @return the evasioni ordini that was removed
	* @throws PortalException if a evasioni ordini with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EvasioniOrdini deleteEvasioniOrdini(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteEvasioniOrdini(ID);
	}

	/**
	* Deletes the evasioni ordini from the database. Also notifies the appropriate model listeners.
	*
	* @param evasioniOrdini the evasioni ordini
	* @return the evasioni ordini that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EvasioniOrdini deleteEvasioniOrdini(
		it.bysoftware.ct.model.EvasioniOrdini evasioniOrdini)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteEvasioniOrdini(evasioniOrdini);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.bysoftware.ct.model.EvasioniOrdini fetchEvasioniOrdini(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchEvasioniOrdini(ID);
	}

	/**
	* Returns the evasioni ordini with the primary key.
	*
	* @param ID the primary key of the evasioni ordini
	* @return the evasioni ordini
	* @throws PortalException if a evasioni ordini with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EvasioniOrdini getEvasioniOrdini(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getEvasioniOrdini(ID);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the evasioni ordinis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.EvasioniOrdiniModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of evasioni ordinis
	* @param end the upper bound of the range of evasioni ordinis (not inclusive)
	* @return the range of evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.bysoftware.ct.model.EvasioniOrdini> getEvasioniOrdinis(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getEvasioniOrdinis(start, end);
	}

	/**
	* Returns the number of evasioni ordinis.
	*
	* @return the number of evasioni ordinis
	* @throws SystemException if a system exception occurred
	*/
	public static int getEvasioniOrdinisCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getEvasioniOrdinisCount();
	}

	/**
	* Updates the evasioni ordini in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param evasioniOrdini the evasioni ordini
	* @return the evasioni ordini that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.bysoftware.ct.model.EvasioniOrdini updateEvasioniOrdini(
		it.bysoftware.ct.model.EvasioniOrdini evasioniOrdini)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateEvasioniOrdini(evasioniOrdini);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	/**
	* Returns all process order for the specified client.
	*
	* @param codiceCliente
	* @return {@link List} of process order for the specified client
	*/
	public static java.util.List<it.bysoftware.ct.model.EvasioniOrdini> getEvasioniByCodiceCliente(
		java.lang.String codiceCliente, int anno,
		java.lang.String codiceAttivita, java.lang.String codiceCentro,
		java.lang.String codiceDeposito, int tipoOrdine, int numeroOrdine) {
		return getService()
				   .getEvasioniByCodiceCliente(codiceCliente, anno,
			codiceAttivita, codiceCentro, codiceDeposito, tipoOrdine,
			numeroOrdine);
	}

	public static void clearService() {
		_service = null;
	}

	public static EvasioniOrdiniLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					EvasioniOrdiniLocalService.class.getName());

			if (invokableLocalService instanceof EvasioniOrdiniLocalService) {
				_service = (EvasioniOrdiniLocalService)invokableLocalService;
			}
			else {
				_service = new EvasioniOrdiniLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(EvasioniOrdiniLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(EvasioniOrdiniLocalService service) {
	}

	private static EvasioniOrdiniLocalService _service;
}