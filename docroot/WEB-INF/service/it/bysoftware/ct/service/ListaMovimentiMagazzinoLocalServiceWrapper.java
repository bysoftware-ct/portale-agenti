/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ListaMovimentiMagazzinoLocalService}.
 *
 * @author Mario Torrisi
 * @see ListaMovimentiMagazzinoLocalService
 * @generated
 */
public class ListaMovimentiMagazzinoLocalServiceWrapper
	implements ListaMovimentiMagazzinoLocalService,
		ServiceWrapper<ListaMovimentiMagazzinoLocalService> {
	public ListaMovimentiMagazzinoLocalServiceWrapper(
		ListaMovimentiMagazzinoLocalService listaMovimentiMagazzinoLocalService) {
		_listaMovimentiMagazzinoLocalService = listaMovimentiMagazzinoLocalService;
	}

	/**
	* Adds the lista movimenti magazzino to the database. Also notifies the appropriate model listeners.
	*
	* @param listaMovimentiMagazzino the lista movimenti magazzino
	* @return the lista movimenti magazzino that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.ListaMovimentiMagazzino addListaMovimentiMagazzino(
		it.bysoftware.ct.model.ListaMovimentiMagazzino listaMovimentiMagazzino)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listaMovimentiMagazzinoLocalService.addListaMovimentiMagazzino(listaMovimentiMagazzino);
	}

	/**
	* Creates a new lista movimenti magazzino with the primary key. Does not add the lista movimenti magazzino to the database.
	*
	* @param ID the primary key for the new lista movimenti magazzino
	* @return the new lista movimenti magazzino
	*/
	@Override
	public it.bysoftware.ct.model.ListaMovimentiMagazzino createListaMovimentiMagazzino(
		java.lang.String ID) {
		return _listaMovimentiMagazzinoLocalService.createListaMovimentiMagazzino(ID);
	}

	/**
	* Deletes the lista movimenti magazzino with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the lista movimenti magazzino
	* @return the lista movimenti magazzino that was removed
	* @throws PortalException if a lista movimenti magazzino with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.ListaMovimentiMagazzino deleteListaMovimentiMagazzino(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _listaMovimentiMagazzinoLocalService.deleteListaMovimentiMagazzino(ID);
	}

	/**
	* Deletes the lista movimenti magazzino from the database. Also notifies the appropriate model listeners.
	*
	* @param listaMovimentiMagazzino the lista movimenti magazzino
	* @return the lista movimenti magazzino that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.ListaMovimentiMagazzino deleteListaMovimentiMagazzino(
		it.bysoftware.ct.model.ListaMovimentiMagazzino listaMovimentiMagazzino)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listaMovimentiMagazzinoLocalService.deleteListaMovimentiMagazzino(listaMovimentiMagazzino);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _listaMovimentiMagazzinoLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listaMovimentiMagazzinoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _listaMovimentiMagazzinoLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listaMovimentiMagazzinoLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listaMovimentiMagazzinoLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listaMovimentiMagazzinoLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.bysoftware.ct.model.ListaMovimentiMagazzino fetchListaMovimentiMagazzino(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listaMovimentiMagazzinoLocalService.fetchListaMovimentiMagazzino(ID);
	}

	/**
	* Returns the lista movimenti magazzino with the primary key.
	*
	* @param ID the primary key of the lista movimenti magazzino
	* @return the lista movimenti magazzino
	* @throws PortalException if a lista movimenti magazzino with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.ListaMovimentiMagazzino getListaMovimentiMagazzino(
		java.lang.String ID)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _listaMovimentiMagazzinoLocalService.getListaMovimentiMagazzino(ID);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _listaMovimentiMagazzinoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the lista movimenti magazzinos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.ListaMovimentiMagazzinoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of lista movimenti magazzinos
	* @param end the upper bound of the range of lista movimenti magazzinos (not inclusive)
	* @return the range of lista movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.bysoftware.ct.model.ListaMovimentiMagazzino> getListaMovimentiMagazzinos(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listaMovimentiMagazzinoLocalService.getListaMovimentiMagazzinos(start,
			end);
	}

	/**
	* Returns the number of lista movimenti magazzinos.
	*
	* @return the number of lista movimenti magazzinos
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getListaMovimentiMagazzinosCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listaMovimentiMagazzinoLocalService.getListaMovimentiMagazzinosCount();
	}

	/**
	* Updates the lista movimenti magazzino in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param listaMovimentiMagazzino the lista movimenti magazzino
	* @return the lista movimenti magazzino that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.ListaMovimentiMagazzino updateListaMovimentiMagazzino(
		it.bysoftware.ct.model.ListaMovimentiMagazzino listaMovimentiMagazzino)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _listaMovimentiMagazzinoLocalService.updateListaMovimentiMagazzino(listaMovimentiMagazzino);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _listaMovimentiMagazzinoLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_listaMovimentiMagazzinoLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _listaMovimentiMagazzinoLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ListaMovimentiMagazzinoLocalService getWrappedListaMovimentiMagazzinoLocalService() {
		return _listaMovimentiMagazzinoLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedListaMovimentiMagazzinoLocalService(
		ListaMovimentiMagazzinoLocalService listaMovimentiMagazzinoLocalService) {
		_listaMovimentiMagazzinoLocalService = listaMovimentiMagazzinoLocalService;
	}

	@Override
	public ListaMovimentiMagazzinoLocalService getWrappedService() {
		return _listaMovimentiMagazzinoLocalService;
	}

	@Override
	public void setWrappedService(
		ListaMovimentiMagazzinoLocalService listaMovimentiMagazzinoLocalService) {
		_listaMovimentiMagazzinoLocalService = listaMovimentiMagazzinoLocalService;
	}

	private ListaMovimentiMagazzinoLocalService _listaMovimentiMagazzinoLocalService;
}