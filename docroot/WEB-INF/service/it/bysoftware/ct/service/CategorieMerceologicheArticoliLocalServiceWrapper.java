/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CategorieMerceologicheArticoliLocalService}.
 *
 * @author Mario Torrisi
 * @see CategorieMerceologicheArticoliLocalService
 * @generated
 */
public class CategorieMerceologicheArticoliLocalServiceWrapper
	implements CategorieMerceologicheArticoliLocalService,
		ServiceWrapper<CategorieMerceologicheArticoliLocalService> {
	public CategorieMerceologicheArticoliLocalServiceWrapper(
		CategorieMerceologicheArticoliLocalService categorieMerceologicheArticoliLocalService) {
		_categorieMerceologicheArticoliLocalService = categorieMerceologicheArticoliLocalService;
	}

	/**
	* Adds the categorie merceologiche articoli to the database. Also notifies the appropriate model listeners.
	*
	* @param categorieMerceologicheArticoli the categorie merceologiche articoli
	* @return the categorie merceologiche articoli that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.CategorieMerceologicheArticoli addCategorieMerceologicheArticoli(
		it.bysoftware.ct.model.CategorieMerceologicheArticoli categorieMerceologicheArticoli)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categorieMerceologicheArticoliLocalService.addCategorieMerceologicheArticoli(categorieMerceologicheArticoli);
	}

	/**
	* Creates a new categorie merceologiche articoli with the primary key. Does not add the categorie merceologiche articoli to the database.
	*
	* @param codiceCategoria the primary key for the new categorie merceologiche articoli
	* @return the new categorie merceologiche articoli
	*/
	@Override
	public it.bysoftware.ct.model.CategorieMerceologicheArticoli createCategorieMerceologicheArticoli(
		java.lang.String codiceCategoria) {
		return _categorieMerceologicheArticoliLocalService.createCategorieMerceologicheArticoli(codiceCategoria);
	}

	/**
	* Deletes the categorie merceologiche articoli with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param codiceCategoria the primary key of the categorie merceologiche articoli
	* @return the categorie merceologiche articoli that was removed
	* @throws PortalException if a categorie merceologiche articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.CategorieMerceologicheArticoli deleteCategorieMerceologicheArticoli(
		java.lang.String codiceCategoria)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _categorieMerceologicheArticoliLocalService.deleteCategorieMerceologicheArticoli(codiceCategoria);
	}

	/**
	* Deletes the categorie merceologiche articoli from the database. Also notifies the appropriate model listeners.
	*
	* @param categorieMerceologicheArticoli the categorie merceologiche articoli
	* @return the categorie merceologiche articoli that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.CategorieMerceologicheArticoli deleteCategorieMerceologicheArticoli(
		it.bysoftware.ct.model.CategorieMerceologicheArticoli categorieMerceologicheArticoli)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categorieMerceologicheArticoliLocalService.deleteCategorieMerceologicheArticoli(categorieMerceologicheArticoli);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _categorieMerceologicheArticoliLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categorieMerceologicheArticoliLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieMerceologicheArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _categorieMerceologicheArticoliLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieMerceologicheArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categorieMerceologicheArticoliLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categorieMerceologicheArticoliLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categorieMerceologicheArticoliLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.bysoftware.ct.model.CategorieMerceologicheArticoli fetchCategorieMerceologicheArticoli(
		java.lang.String codiceCategoria)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categorieMerceologicheArticoliLocalService.fetchCategorieMerceologicheArticoli(codiceCategoria);
	}

	/**
	* Returns the categorie merceologiche articoli with the primary key.
	*
	* @param codiceCategoria the primary key of the categorie merceologiche articoli
	* @return the categorie merceologiche articoli
	* @throws PortalException if a categorie merceologiche articoli with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.CategorieMerceologicheArticoli getCategorieMerceologicheArticoli(
		java.lang.String codiceCategoria)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _categorieMerceologicheArticoliLocalService.getCategorieMerceologicheArticoli(codiceCategoria);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _categorieMerceologicheArticoliLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the categorie merceologiche articolis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.CategorieMerceologicheArticoliModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of categorie merceologiche articolis
	* @param end the upper bound of the range of categorie merceologiche articolis (not inclusive)
	* @return the range of categorie merceologiche articolis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.bysoftware.ct.model.CategorieMerceologicheArticoli> getCategorieMerceologicheArticolis(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categorieMerceologicheArticoliLocalService.getCategorieMerceologicheArticolis(start,
			end);
	}

	/**
	* Returns the number of categorie merceologiche articolis.
	*
	* @return the number of categorie merceologiche articolis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getCategorieMerceologicheArticolisCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categorieMerceologicheArticoliLocalService.getCategorieMerceologicheArticolisCount();
	}

	/**
	* Updates the categorie merceologiche articoli in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param categorieMerceologicheArticoli the categorie merceologiche articoli
	* @return the categorie merceologiche articoli that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.CategorieMerceologicheArticoli updateCategorieMerceologicheArticoli(
		it.bysoftware.ct.model.CategorieMerceologicheArticoli categorieMerceologicheArticoli)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categorieMerceologicheArticoliLocalService.updateCategorieMerceologicheArticoli(categorieMerceologicheArticoli);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _categorieMerceologicheArticoliLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_categorieMerceologicheArticoliLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _categorieMerceologicheArticoliLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	@Override
	public it.bysoftware.ct.model.CategorieMerceologicheArticoli getDescrizioneCategoria(
		java.lang.String codiceCategoria) {
		return _categorieMerceologicheArticoliLocalService.getDescrizioneCategoria(codiceCategoria);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CategorieMerceologicheArticoliLocalService getWrappedCategorieMerceologicheArticoliLocalService() {
		return _categorieMerceologicheArticoliLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCategorieMerceologicheArticoliLocalService(
		CategorieMerceologicheArticoliLocalService categorieMerceologicheArticoliLocalService) {
		_categorieMerceologicheArticoliLocalService = categorieMerceologicheArticoliLocalService;
	}

	@Override
	public CategorieMerceologicheArticoliLocalService getWrappedService() {
		return _categorieMerceologicheArticoliLocalService;
	}

	@Override
	public void setWrappedService(
		CategorieMerceologicheArticoliLocalService categorieMerceologicheArticoliLocalService) {
		_categorieMerceologicheArticoliLocalService = categorieMerceologicheArticoliLocalService;
	}

	private CategorieMerceologicheArticoliLocalService _categorieMerceologicheArticoliLocalService;
}