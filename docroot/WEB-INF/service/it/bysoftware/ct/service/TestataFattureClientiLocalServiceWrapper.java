/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link TestataFattureClientiLocalService}.
 *
 * @author Mario Torrisi
 * @see TestataFattureClientiLocalService
 * @generated
 */
public class TestataFattureClientiLocalServiceWrapper
	implements TestataFattureClientiLocalService,
		ServiceWrapper<TestataFattureClientiLocalService> {
	public TestataFattureClientiLocalServiceWrapper(
		TestataFattureClientiLocalService testataFattureClientiLocalService) {
		_testataFattureClientiLocalService = testataFattureClientiLocalService;
	}

	/**
	* Adds the testata fatture clienti to the database. Also notifies the appropriate model listeners.
	*
	* @param testataFattureClienti the testata fatture clienti
	* @return the testata fatture clienti that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.TestataFattureClienti addTestataFattureClienti(
		it.bysoftware.ct.model.TestataFattureClienti testataFattureClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testataFattureClientiLocalService.addTestataFattureClienti(testataFattureClienti);
	}

	/**
	* Creates a new testata fatture clienti with the primary key. Does not add the testata fatture clienti to the database.
	*
	* @param testataFattureClientiPK the primary key for the new testata fatture clienti
	* @return the new testata fatture clienti
	*/
	@Override
	public it.bysoftware.ct.model.TestataFattureClienti createTestataFattureClienti(
		it.bysoftware.ct.service.persistence.TestataFattureClientiPK testataFattureClientiPK) {
		return _testataFattureClientiLocalService.createTestataFattureClienti(testataFattureClientiPK);
	}

	/**
	* Deletes the testata fatture clienti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param testataFattureClientiPK the primary key of the testata fatture clienti
	* @return the testata fatture clienti that was removed
	* @throws PortalException if a testata fatture clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.TestataFattureClienti deleteTestataFattureClienti(
		it.bysoftware.ct.service.persistence.TestataFattureClientiPK testataFattureClientiPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _testataFattureClientiLocalService.deleteTestataFattureClienti(testataFattureClientiPK);
	}

	/**
	* Deletes the testata fatture clienti from the database. Also notifies the appropriate model listeners.
	*
	* @param testataFattureClienti the testata fatture clienti
	* @return the testata fatture clienti that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.TestataFattureClienti deleteTestataFattureClienti(
		it.bysoftware.ct.model.TestataFattureClienti testataFattureClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testataFattureClientiLocalService.deleteTestataFattureClienti(testataFattureClienti);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _testataFattureClientiLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testataFattureClientiLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataFattureClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _testataFattureClientiLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataFattureClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testataFattureClientiLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testataFattureClientiLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testataFattureClientiLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.bysoftware.ct.model.TestataFattureClienti fetchTestataFattureClienti(
		it.bysoftware.ct.service.persistence.TestataFattureClientiPK testataFattureClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testataFattureClientiLocalService.fetchTestataFattureClienti(testataFattureClientiPK);
	}

	/**
	* Returns the testata fatture clienti with the primary key.
	*
	* @param testataFattureClientiPK the primary key of the testata fatture clienti
	* @return the testata fatture clienti
	* @throws PortalException if a testata fatture clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.TestataFattureClienti getTestataFattureClienti(
		it.bysoftware.ct.service.persistence.TestataFattureClientiPK testataFattureClientiPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _testataFattureClientiLocalService.getTestataFattureClienti(testataFattureClientiPK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _testataFattureClientiLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the testata fatture clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.TestataFattureClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of testata fatture clientis
	* @param end the upper bound of the range of testata fatture clientis (not inclusive)
	* @return the range of testata fatture clientis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.bysoftware.ct.model.TestataFattureClienti> getTestataFattureClientis(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testataFattureClientiLocalService.getTestataFattureClientis(start,
			end);
	}

	/**
	* Returns the number of testata fatture clientis.
	*
	* @return the number of testata fatture clientis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getTestataFattureClientisCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testataFattureClientiLocalService.getTestataFattureClientisCount();
	}

	/**
	* Updates the testata fatture clienti in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param testataFattureClienti the testata fatture clienti
	* @return the testata fatture clienti that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.TestataFattureClienti updateTestataFattureClienti(
		it.bysoftware.ct.model.TestataFattureClienti testataFattureClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _testataFattureClientiLocalService.updateTestataFattureClienti(testataFattureClienti);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _testataFattureClientiLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_testataFattureClientiLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _testataFattureClientiLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	@Override
	public it.bysoftware.ct.model.TestataFattureClienti getFattureByAnnoAttivitaDocumento(
		int anno, java.lang.String codiceAttivita,
		java.lang.String codiceCentro, int numeroDocumento,
		java.sql.Date dataDocumento, boolean tipoSoggetto,
		java.lang.String codiceSoggetto) {
		return _testataFattureClientiLocalService.getFattureByAnnoAttivitaDocumento(anno,
			codiceAttivita, codiceCentro, numeroDocumento, dataDocumento,
			tipoSoggetto, codiceSoggetto);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public TestataFattureClientiLocalService getWrappedTestataFattureClientiLocalService() {
		return _testataFattureClientiLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedTestataFattureClientiLocalService(
		TestataFattureClientiLocalService testataFattureClientiLocalService) {
		_testataFattureClientiLocalService = testataFattureClientiLocalService;
	}

	@Override
	public TestataFattureClientiLocalService getWrappedService() {
		return _testataFattureClientiLocalService;
	}

	@Override
	public void setWrappedService(
		TestataFattureClientiLocalService testataFattureClientiLocalService) {
		_testataFattureClientiLocalService = testataFattureClientiLocalService;
	}

	private TestataFattureClientiLocalService _testataFattureClientiLocalService;
}