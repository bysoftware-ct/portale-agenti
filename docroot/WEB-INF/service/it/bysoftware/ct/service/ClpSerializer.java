/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import it.bysoftware.ct.model.AnagraficaAgentiClp;
import it.bysoftware.ct.model.AnagraficaClientiFornitoriClp;
import it.bysoftware.ct.model.ArticoliClp;
import it.bysoftware.ct.model.CategorieMerceologicheArticoliClp;
import it.bysoftware.ct.model.DatiClientiFornitoriClp;
import it.bysoftware.ct.model.DatiCostantiClientiFornitoriClp;
import it.bysoftware.ct.model.EstrattoContoClp;
import it.bysoftware.ct.model.EvasioniOrdiniClp;
import it.bysoftware.ct.model.ListiniPrezziArticoliClp;
import it.bysoftware.ct.model.MovimentiMagazzinoClp;
import it.bysoftware.ct.model.OrdinatoClp;
import it.bysoftware.ct.model.OrdiniClientiClp;
import it.bysoftware.ct.model.PianoPagamentiClp;
import it.bysoftware.ct.model.RigheFattureVeditaClp;
import it.bysoftware.ct.model.RigheOrdiniClientiClp;
import it.bysoftware.ct.model.ScontiArticoliClp;
import it.bysoftware.ct.model.TestataFattureClientiClp;
import it.bysoftware.ct.model.UltimiPrezziArticoliClp;
import it.bysoftware.ct.model.VociIvaClp;
import it.bysoftware.ct.model.WKOrdiniClientiClp;
import it.bysoftware.ct.model.WKRigheOrdiniClientiClp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mario Torrisi
 */
public class ClpSerializer {
	public static String getServletContextName() {
		if (Validator.isNotNull(_servletContextName)) {
			return _servletContextName;
		}

		synchronized (ClpSerializer.class) {
			if (Validator.isNotNull(_servletContextName)) {
				return _servletContextName;
			}

			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Class<?> portletPropsClass = classLoader.loadClass(
						"com.liferay.util.portlet.PortletProps");

				Method getMethod = portletPropsClass.getMethod("get",
						new Class<?>[] { String.class });

				String portletPropsServletContextName = (String)getMethod.invoke(null,
						"portale-agenti-portlet-deployment-context");

				if (Validator.isNotNull(portletPropsServletContextName)) {
					_servletContextName = portletPropsServletContextName;
				}
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info(
						"Unable to locate deployment context from portlet properties");
				}
			}

			if (Validator.isNull(_servletContextName)) {
				try {
					String propsUtilServletContextName = PropsUtil.get(
							"portale-agenti-portlet-deployment-context");

					if (Validator.isNotNull(propsUtilServletContextName)) {
						_servletContextName = propsUtilServletContextName;
					}
				}
				catch (Throwable t) {
					if (_log.isInfoEnabled()) {
						_log.info(
							"Unable to locate deployment context from portal properties");
					}
				}
			}

			if (Validator.isNull(_servletContextName)) {
				_servletContextName = "portale-agenti-portlet";
			}

			return _servletContextName;
		}
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(AnagraficaAgentiClp.class.getName())) {
			return translateInputAnagraficaAgenti(oldModel);
		}

		if (oldModelClassName.equals(
					AnagraficaClientiFornitoriClp.class.getName())) {
			return translateInputAnagraficaClientiFornitori(oldModel);
		}

		if (oldModelClassName.equals(ArticoliClp.class.getName())) {
			return translateInputArticoli(oldModel);
		}

		if (oldModelClassName.equals(
					CategorieMerceologicheArticoliClp.class.getName())) {
			return translateInputCategorieMerceologicheArticoli(oldModel);
		}

		if (oldModelClassName.equals(DatiClientiFornitoriClp.class.getName())) {
			return translateInputDatiClientiFornitori(oldModel);
		}

		if (oldModelClassName.equals(
					DatiCostantiClientiFornitoriClp.class.getName())) {
			return translateInputDatiCostantiClientiFornitori(oldModel);
		}

		if (oldModelClassName.equals(EstrattoContoClp.class.getName())) {
			return translateInputEstrattoConto(oldModel);
		}

		if (oldModelClassName.equals(EvasioniOrdiniClp.class.getName())) {
			return translateInputEvasioniOrdini(oldModel);
		}

		if (oldModelClassName.equals(ListiniPrezziArticoliClp.class.getName())) {
			return translateInputListiniPrezziArticoli(oldModel);
		}

		if (oldModelClassName.equals(MovimentiMagazzinoClp.class.getName())) {
			return translateInputMovimentiMagazzino(oldModel);
		}

		if (oldModelClassName.equals(OrdinatoClp.class.getName())) {
			return translateInputOrdinato(oldModel);
		}

		if (oldModelClassName.equals(OrdiniClientiClp.class.getName())) {
			return translateInputOrdiniClienti(oldModel);
		}

		if (oldModelClassName.equals(PianoPagamentiClp.class.getName())) {
			return translateInputPianoPagamenti(oldModel);
		}

		if (oldModelClassName.equals(RigheFattureVeditaClp.class.getName())) {
			return translateInputRigheFattureVedita(oldModel);
		}

		if (oldModelClassName.equals(RigheOrdiniClientiClp.class.getName())) {
			return translateInputRigheOrdiniClienti(oldModel);
		}

		if (oldModelClassName.equals(ScontiArticoliClp.class.getName())) {
			return translateInputScontiArticoli(oldModel);
		}

		if (oldModelClassName.equals(TestataFattureClientiClp.class.getName())) {
			return translateInputTestataFattureClienti(oldModel);
		}

		if (oldModelClassName.equals(UltimiPrezziArticoliClp.class.getName())) {
			return translateInputUltimiPrezziArticoli(oldModel);
		}

		if (oldModelClassName.equals(VociIvaClp.class.getName())) {
			return translateInputVociIva(oldModel);
		}

		if (oldModelClassName.equals(WKOrdiniClientiClp.class.getName())) {
			return translateInputWKOrdiniClienti(oldModel);
		}

		if (oldModelClassName.equals(WKRigheOrdiniClientiClp.class.getName())) {
			return translateInputWKRigheOrdiniClienti(oldModel);
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInputAnagraficaAgenti(BaseModel<?> oldModel) {
		AnagraficaAgentiClp oldClpModel = (AnagraficaAgentiClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAnagraficaAgentiRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputAnagraficaClientiFornitori(
		BaseModel<?> oldModel) {
		AnagraficaClientiFornitoriClp oldClpModel = (AnagraficaClientiFornitoriClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAnagraficaClientiFornitoriRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputArticoli(BaseModel<?> oldModel) {
		ArticoliClp oldClpModel = (ArticoliClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getArticoliRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCategorieMerceologicheArticoli(
		BaseModel<?> oldModel) {
		CategorieMerceologicheArticoliClp oldClpModel = (CategorieMerceologicheArticoliClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCategorieMerceologicheArticoliRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputDatiClientiFornitori(
		BaseModel<?> oldModel) {
		DatiClientiFornitoriClp oldClpModel = (DatiClientiFornitoriClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getDatiClientiFornitoriRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputDatiCostantiClientiFornitori(
		BaseModel<?> oldModel) {
		DatiCostantiClientiFornitoriClp oldClpModel = (DatiCostantiClientiFornitoriClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getDatiCostantiClientiFornitoriRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEstrattoConto(BaseModel<?> oldModel) {
		EstrattoContoClp oldClpModel = (EstrattoContoClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEstrattoContoRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEvasioniOrdini(BaseModel<?> oldModel) {
		EvasioniOrdiniClp oldClpModel = (EvasioniOrdiniClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEvasioniOrdiniRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputListiniPrezziArticoli(
		BaseModel<?> oldModel) {
		ListiniPrezziArticoliClp oldClpModel = (ListiniPrezziArticoliClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getListiniPrezziArticoliRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputMovimentiMagazzino(BaseModel<?> oldModel) {
		MovimentiMagazzinoClp oldClpModel = (MovimentiMagazzinoClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getMovimentiMagazzinoRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputOrdinato(BaseModel<?> oldModel) {
		OrdinatoClp oldClpModel = (OrdinatoClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getOrdinatoRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputOrdiniClienti(BaseModel<?> oldModel) {
		OrdiniClientiClp oldClpModel = (OrdiniClientiClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getOrdiniClientiRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputPianoPagamenti(BaseModel<?> oldModel) {
		PianoPagamentiClp oldClpModel = (PianoPagamentiClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getPianoPagamentiRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputRigheFattureVedita(BaseModel<?> oldModel) {
		RigheFattureVeditaClp oldClpModel = (RigheFattureVeditaClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getRigheFattureVeditaRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputRigheOrdiniClienti(BaseModel<?> oldModel) {
		RigheOrdiniClientiClp oldClpModel = (RigheOrdiniClientiClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getRigheOrdiniClientiRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputScontiArticoli(BaseModel<?> oldModel) {
		ScontiArticoliClp oldClpModel = (ScontiArticoliClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getScontiArticoliRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputTestataFattureClienti(
		BaseModel<?> oldModel) {
		TestataFattureClientiClp oldClpModel = (TestataFattureClientiClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getTestataFattureClientiRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputUltimiPrezziArticoli(
		BaseModel<?> oldModel) {
		UltimiPrezziArticoliClp oldClpModel = (UltimiPrezziArticoliClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getUltimiPrezziArticoliRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputVociIva(BaseModel<?> oldModel) {
		VociIvaClp oldClpModel = (VociIvaClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getVociIvaRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputWKOrdiniClienti(BaseModel<?> oldModel) {
		WKOrdiniClientiClp oldClpModel = (WKOrdiniClientiClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getWKOrdiniClientiRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputWKRigheOrdiniClienti(
		BaseModel<?> oldModel) {
		WKRigheOrdiniClientiClp oldClpModel = (WKRigheOrdiniClientiClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getWKRigheOrdiniClientiRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.AnagraficaAgentiImpl")) {
			return translateOutputAnagraficaAgenti(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.AnagraficaClientiFornitoriImpl")) {
			return translateOutputAnagraficaClientiFornitori(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("it.bysoftware.ct.model.impl.ArticoliImpl")) {
			return translateOutputArticoli(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.CategorieMerceologicheArticoliImpl")) {
			return translateOutputCategorieMerceologicheArticoli(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.DatiClientiFornitoriImpl")) {
			return translateOutputDatiClientiFornitori(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.DatiCostantiClientiFornitoriImpl")) {
			return translateOutputDatiCostantiClientiFornitori(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.EstrattoContoImpl")) {
			return translateOutputEstrattoConto(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.EvasioniOrdiniImpl")) {
			return translateOutputEvasioniOrdini(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.ListiniPrezziArticoliImpl")) {
			return translateOutputListiniPrezziArticoli(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.MovimentiMagazzinoImpl")) {
			return translateOutputMovimentiMagazzino(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("it.bysoftware.ct.model.impl.OrdinatoImpl")) {
			return translateOutputOrdinato(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.OrdiniClientiImpl")) {
			return translateOutputOrdiniClienti(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.PianoPagamentiImpl")) {
			return translateOutputPianoPagamenti(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.RigheFattureVeditaImpl")) {
			return translateOutputRigheFattureVedita(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.RigheOrdiniClientiImpl")) {
			return translateOutputRigheOrdiniClienti(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.ScontiArticoliImpl")) {
			return translateOutputScontiArticoli(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.TestataFattureClientiImpl")) {
			return translateOutputTestataFattureClienti(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.UltimiPrezziArticoliImpl")) {
			return translateOutputUltimiPrezziArticoli(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("it.bysoftware.ct.model.impl.VociIvaImpl")) {
			return translateOutputVociIva(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.WKOrdiniClientiImpl")) {
			return translateOutputWKOrdiniClienti(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.bysoftware.ct.model.impl.WKRigheOrdiniClientiImpl")) {
			return translateOutputWKRigheOrdiniClienti(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Throwable translateThrowable(Throwable throwable) {
		if (_useReflectionToTranslateThrowable) {
			try {
				UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

				objectOutputStream.writeObject(throwable);

				objectOutputStream.flush();
				objectOutputStream.close();

				UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
						0, unsyncByteArrayOutputStream.size());

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader = currentThread.getContextClassLoader();

				ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
						contextClassLoader);

				throwable = (Throwable)objectInputStream.readObject();

				objectInputStream.close();

				return throwable;
			}
			catch (SecurityException se) {
				if (_log.isInfoEnabled()) {
					_log.info("Do not use reflection to translate throwable");
				}

				_useReflectionToTranslateThrowable = false;
			}
			catch (Throwable throwable2) {
				_log.error(throwable2, throwable2);

				return throwable2;
			}
		}

		Class<?> clazz = throwable.getClass();

		String className = clazz.getName();

		if (className.equals(PortalException.class.getName())) {
			return new PortalException();
		}

		if (className.equals(SystemException.class.getName())) {
			return new SystemException();
		}

		if (className.equals("it.bysoftware.ct.NoSuchAnagraficaAgentiException")) {
			return new it.bysoftware.ct.NoSuchAnagraficaAgentiException();
		}

		if (className.equals(
					"it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException")) {
			return new it.bysoftware.ct.NoSuchAnagraficaClientiFornitoriException();
		}

		if (className.equals("it.bysoftware.ct.NoSuchArticoliException")) {
			return new it.bysoftware.ct.NoSuchArticoliException();
		}

		if (className.equals(
					"it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException")) {
			return new it.bysoftware.ct.NoSuchCategorieMerceologicheArticoliException();
		}

		if (className.equals(
					"it.bysoftware.ct.NoSuchDatiClientiFornitoriException")) {
			return new it.bysoftware.ct.NoSuchDatiClientiFornitoriException();
		}

		if (className.equals(
					"it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException")) {
			return new it.bysoftware.ct.NoSuchDatiCostantiClientiFornitoriException();
		}

		if (className.equals("it.bysoftware.ct.NoSuchEstrattoContoException")) {
			return new it.bysoftware.ct.NoSuchEstrattoContoException();
		}

		if (className.equals("it.bysoftware.ct.NoSuchEvasioniOrdiniException")) {
			return new it.bysoftware.ct.NoSuchEvasioniOrdiniException();
		}

		if (className.equals(
					"it.bysoftware.ct.NoSuchListiniPrezziArticoliException")) {
			return new it.bysoftware.ct.NoSuchListiniPrezziArticoliException();
		}

		if (className.equals(
					"it.bysoftware.ct.NoSuchMovimentiMagazzinoException")) {
			return new it.bysoftware.ct.NoSuchMovimentiMagazzinoException();
		}

		if (className.equals("it.bysoftware.ct.NoSuchOrdinatoException")) {
			return new it.bysoftware.ct.NoSuchOrdinatoException();
		}

		if (className.equals("it.bysoftware.ct.NoSuchOrdiniClientiException")) {
			return new it.bysoftware.ct.NoSuchOrdiniClientiException();
		}

		if (className.equals("it.bysoftware.ct.NoSuchPianoPagamentiException")) {
			return new it.bysoftware.ct.NoSuchPianoPagamentiException();
		}

		if (className.equals(
					"it.bysoftware.ct.NoSuchRigheFattureVeditaException")) {
			return new it.bysoftware.ct.NoSuchRigheFattureVeditaException();
		}

		if (className.equals(
					"it.bysoftware.ct.NoSuchRigheOrdiniClientiException")) {
			return new it.bysoftware.ct.NoSuchRigheOrdiniClientiException();
		}

		if (className.equals("it.bysoftware.ct.NoSuchScontiArticoliException")) {
			return new it.bysoftware.ct.NoSuchScontiArticoliException();
		}

		if (className.equals(
					"it.bysoftware.ct.NoSuchTestataFattureClientiException")) {
			return new it.bysoftware.ct.NoSuchTestataFattureClientiException();
		}

		if (className.equals(
					"it.bysoftware.ct.NoSuchUltimiPrezziArticoliException")) {
			return new it.bysoftware.ct.NoSuchUltimiPrezziArticoliException();
		}

		if (className.equals("it.bysoftware.ct.NoSuchVociIvaException")) {
			return new it.bysoftware.ct.NoSuchVociIvaException();
		}

		if (className.equals("it.bysoftware.ct.NoSuchWKOrdiniClientiException")) {
			return new it.bysoftware.ct.NoSuchWKOrdiniClientiException();
		}

		if (className.equals(
					"it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException")) {
			return new it.bysoftware.ct.NoSuchWKRigheOrdiniClientiException();
		}

		return throwable;
	}

	public static Object translateOutputAnagraficaAgenti(BaseModel<?> oldModel) {
		AnagraficaAgentiClp newModel = new AnagraficaAgentiClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAnagraficaAgentiRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputAnagraficaClientiFornitori(
		BaseModel<?> oldModel) {
		AnagraficaClientiFornitoriClp newModel = new AnagraficaClientiFornitoriClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAnagraficaClientiFornitoriRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputArticoli(BaseModel<?> oldModel) {
		ArticoliClp newModel = new ArticoliClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setArticoliRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCategorieMerceologicheArticoli(
		BaseModel<?> oldModel) {
		CategorieMerceologicheArticoliClp newModel = new CategorieMerceologicheArticoliClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCategorieMerceologicheArticoliRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputDatiClientiFornitori(
		BaseModel<?> oldModel) {
		DatiClientiFornitoriClp newModel = new DatiClientiFornitoriClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setDatiClientiFornitoriRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputDatiCostantiClientiFornitori(
		BaseModel<?> oldModel) {
		DatiCostantiClientiFornitoriClp newModel = new DatiCostantiClientiFornitoriClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setDatiCostantiClientiFornitoriRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEstrattoConto(BaseModel<?> oldModel) {
		EstrattoContoClp newModel = new EstrattoContoClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEstrattoContoRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEvasioniOrdini(BaseModel<?> oldModel) {
		EvasioniOrdiniClp newModel = new EvasioniOrdiniClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEvasioniOrdiniRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputListiniPrezziArticoli(
		BaseModel<?> oldModel) {
		ListiniPrezziArticoliClp newModel = new ListiniPrezziArticoliClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setListiniPrezziArticoliRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputMovimentiMagazzino(
		BaseModel<?> oldModel) {
		MovimentiMagazzinoClp newModel = new MovimentiMagazzinoClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setMovimentiMagazzinoRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputOrdinato(BaseModel<?> oldModel) {
		OrdinatoClp newModel = new OrdinatoClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setOrdinatoRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputOrdiniClienti(BaseModel<?> oldModel) {
		OrdiniClientiClp newModel = new OrdiniClientiClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setOrdiniClientiRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputPianoPagamenti(BaseModel<?> oldModel) {
		PianoPagamentiClp newModel = new PianoPagamentiClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setPianoPagamentiRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputRigheFattureVedita(
		BaseModel<?> oldModel) {
		RigheFattureVeditaClp newModel = new RigheFattureVeditaClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setRigheFattureVeditaRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputRigheOrdiniClienti(
		BaseModel<?> oldModel) {
		RigheOrdiniClientiClp newModel = new RigheOrdiniClientiClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setRigheOrdiniClientiRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputScontiArticoli(BaseModel<?> oldModel) {
		ScontiArticoliClp newModel = new ScontiArticoliClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setScontiArticoliRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputTestataFattureClienti(
		BaseModel<?> oldModel) {
		TestataFattureClientiClp newModel = new TestataFattureClientiClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setTestataFattureClientiRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputUltimiPrezziArticoli(
		BaseModel<?> oldModel) {
		UltimiPrezziArticoliClp newModel = new UltimiPrezziArticoliClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setUltimiPrezziArticoliRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputVociIva(BaseModel<?> oldModel) {
		VociIvaClp newModel = new VociIvaClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setVociIvaRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputWKOrdiniClienti(BaseModel<?> oldModel) {
		WKOrdiniClientiClp newModel = new WKOrdiniClientiClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setWKOrdiniClientiRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputWKRigheOrdiniClienti(
		BaseModel<?> oldModel) {
		WKRigheOrdiniClientiClp newModel = new WKRigheOrdiniClientiClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setWKRigheOrdiniClientiRemoteModel(oldModel);

		return newModel;
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static String _servletContextName;
	private static boolean _useReflectionToTranslateThrowable = true;
}