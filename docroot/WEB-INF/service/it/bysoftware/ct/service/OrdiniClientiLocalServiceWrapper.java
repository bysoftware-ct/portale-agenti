/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link OrdiniClientiLocalService}.
 *
 * @author Mario Torrisi
 * @see OrdiniClientiLocalService
 * @generated
 */
public class OrdiniClientiLocalServiceWrapper
	implements OrdiniClientiLocalService,
		ServiceWrapper<OrdiniClientiLocalService> {
	public OrdiniClientiLocalServiceWrapper(
		OrdiniClientiLocalService ordiniClientiLocalService) {
		_ordiniClientiLocalService = ordiniClientiLocalService;
	}

	/**
	* Adds the ordini clienti to the database. Also notifies the appropriate model listeners.
	*
	* @param ordiniClienti the ordini clienti
	* @return the ordini clienti that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.OrdiniClienti addOrdiniClienti(
		it.bysoftware.ct.model.OrdiniClienti ordiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ordiniClientiLocalService.addOrdiniClienti(ordiniClienti);
	}

	/**
	* Creates a new ordini clienti with the primary key. Does not add the ordini clienti to the database.
	*
	* @param ordiniClientiPK the primary key for the new ordini clienti
	* @return the new ordini clienti
	*/
	@Override
	public it.bysoftware.ct.model.OrdiniClienti createOrdiniClienti(
		it.bysoftware.ct.service.persistence.OrdiniClientiPK ordiniClientiPK) {
		return _ordiniClientiLocalService.createOrdiniClienti(ordiniClientiPK);
	}

	/**
	* Deletes the ordini clienti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ordiniClientiPK the primary key of the ordini clienti
	* @return the ordini clienti that was removed
	* @throws PortalException if a ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.OrdiniClienti deleteOrdiniClienti(
		it.bysoftware.ct.service.persistence.OrdiniClientiPK ordiniClientiPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ordiniClientiLocalService.deleteOrdiniClienti(ordiniClientiPK);
	}

	/**
	* Deletes the ordini clienti from the database. Also notifies the appropriate model listeners.
	*
	* @param ordiniClienti the ordini clienti
	* @return the ordini clienti that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.OrdiniClienti deleteOrdiniClienti(
		it.bysoftware.ct.model.OrdiniClienti ordiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ordiniClientiLocalService.deleteOrdiniClienti(ordiniClienti);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _ordiniClientiLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ordiniClientiLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _ordiniClientiLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ordiniClientiLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ordiniClientiLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ordiniClientiLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.bysoftware.ct.model.OrdiniClienti fetchOrdiniClienti(
		it.bysoftware.ct.service.persistence.OrdiniClientiPK ordiniClientiPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ordiniClientiLocalService.fetchOrdiniClienti(ordiniClientiPK);
	}

	/**
	* Returns the ordini clienti with the primary key.
	*
	* @param ordiniClientiPK the primary key of the ordini clienti
	* @return the ordini clienti
	* @throws PortalException if a ordini clienti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.OrdiniClienti getOrdiniClienti(
		it.bysoftware.ct.service.persistence.OrdiniClientiPK ordiniClientiPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ordiniClientiLocalService.getOrdiniClienti(ordiniClientiPK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ordiniClientiLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the ordini clientis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.bysoftware.ct.model.impl.OrdiniClientiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ordini clientis
	* @param end the upper bound of the range of ordini clientis (not inclusive)
	* @return the range of ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.bysoftware.ct.model.OrdiniClienti> getOrdiniClientis(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ordiniClientiLocalService.getOrdiniClientis(start, end);
	}

	/**
	* Returns the number of ordini clientis.
	*
	* @return the number of ordini clientis
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getOrdiniClientisCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ordiniClientiLocalService.getOrdiniClientisCount();
	}

	/**
	* Updates the ordini clienti in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ordiniClienti the ordini clienti
	* @return the ordini clienti that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.bysoftware.ct.model.OrdiniClienti updateOrdiniClienti(
		it.bysoftware.ct.model.OrdiniClienti ordiniClienti)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ordiniClientiLocalService.updateOrdiniClienti(ordiniClienti);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _ordiniClientiLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_ordiniClientiLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _ordiniClientiLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public java.util.List<it.bysoftware.ct.model.OrdiniClienti> getOrdiniByCodiceCliente(
		java.lang.String codiceCliente, boolean closed, int start, int end,
		java.sql.Date from, java.sql.Date to,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator) {
		return _ordiniClientiLocalService.getOrdiniByCodiceCliente(codiceCliente,
			closed, start, end, from, to, orderByComparator);
	}

	@Override
	public java.util.List<it.bysoftware.ct.model.OrdiniClienti> getOrdiniByCodiceClienteNumPortale(
		java.lang.String codiceCliente, boolean closed, java.lang.String note,
		int start, int end, java.sql.Date from, java.sql.Date to,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator) {
		return _ordiniClientiLocalService.getOrdiniByCodiceClienteNumPortale(codiceCliente,
			closed, note, start, end, from, to, orderByComparator);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public OrdiniClientiLocalService getWrappedOrdiniClientiLocalService() {
		return _ordiniClientiLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedOrdiniClientiLocalService(
		OrdiniClientiLocalService ordiniClientiLocalService) {
		_ordiniClientiLocalService = ordiniClientiLocalService;
	}

	@Override
	public OrdiniClientiLocalService getWrappedService() {
		return _ordiniClientiLocalService;
	}

	@Override
	public void setWrappedService(
		OrdiniClientiLocalService ordiniClientiLocalService) {
		_ordiniClientiLocalService = ordiniClientiLocalService;
	}

	private OrdiniClientiLocalService _ordiniClientiLocalService;
}