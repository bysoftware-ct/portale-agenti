/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the DatiClientiFornitori service. Represents a row in the &quot;CODVEA&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link it.bysoftware.ct.model.impl.DatiClientiFornitoriModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link it.bysoftware.ct.model.impl.DatiClientiFornitoriImpl}.
 * </p>
 *
 * @author Mario Torrisi
 * @see DatiClientiFornitori
 * @see it.bysoftware.ct.model.impl.DatiClientiFornitoriImpl
 * @see it.bysoftware.ct.model.impl.DatiClientiFornitoriModelImpl
 * @generated
 */
public interface DatiClientiFornitoriModel extends BaseModel<DatiClientiFornitori> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a dati clienti fornitori model instance should use the {@link DatiClientiFornitori} interface instead.
	 */

	/**
	 * Returns the primary key of this dati clienti fornitori.
	 *
	 * @return the primary key of this dati clienti fornitori
	 */
	public DatiClientiFornitoriPK getPrimaryKey();

	/**
	 * Sets the primary key of this dati clienti fornitori.
	 *
	 * @param primaryKey the primary key of this dati clienti fornitori
	 */
	public void setPrimaryKey(DatiClientiFornitoriPK primaryKey);

	/**
	 * Returns the tipo soggetto of this dati clienti fornitori.
	 *
	 * @return the tipo soggetto of this dati clienti fornitori
	 */
	public boolean getTipoSoggetto();

	/**
	 * Returns <code>true</code> if this dati clienti fornitori is tipo soggetto.
	 *
	 * @return <code>true</code> if this dati clienti fornitori is tipo soggetto; <code>false</code> otherwise
	 */
	public boolean isTipoSoggetto();

	/**
	 * Sets whether this dati clienti fornitori is tipo soggetto.
	 *
	 * @param tipoSoggetto the tipo soggetto of this dati clienti fornitori
	 */
	public void setTipoSoggetto(boolean tipoSoggetto);

	/**
	 * Returns the codice soggetto of this dati clienti fornitori.
	 *
	 * @return the codice soggetto of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceSoggetto();

	/**
	 * Sets the codice soggetto of this dati clienti fornitori.
	 *
	 * @param codiceSoggetto the codice soggetto of this dati clienti fornitori
	 */
	public void setCodiceSoggetto(String codiceSoggetto);

	/**
	 * Returns the codice banca of this dati clienti fornitori.
	 *
	 * @return the codice banca of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceBanca();

	/**
	 * Sets the codice banca of this dati clienti fornitori.
	 *
	 * @param codiceBanca the codice banca of this dati clienti fornitori
	 */
	public void setCodiceBanca(String codiceBanca);

	/**
	 * Returns the codice agenzia of this dati clienti fornitori.
	 *
	 * @return the codice agenzia of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceAgenzia();

	/**
	 * Sets the codice agenzia of this dati clienti fornitori.
	 *
	 * @param codiceAgenzia the codice agenzia of this dati clienti fornitori
	 */
	public void setCodiceAgenzia(String codiceAgenzia);

	/**
	 * Returns the codice ID banca of this dati clienti fornitori.
	 *
	 * @return the codice ID banca of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceIdBanca();

	/**
	 * Sets the codice ID banca of this dati clienti fornitori.
	 *
	 * @param codiceIdBanca the codice ID banca of this dati clienti fornitori
	 */
	public void setCodiceIdBanca(String codiceIdBanca);

	/**
	 * Returns the codice paese of this dati clienti fornitori.
	 *
	 * @return the codice paese of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodicePaese();

	/**
	 * Sets the codice paese of this dati clienti fornitori.
	 *
	 * @param codicePaese the codice paese of this dati clienti fornitori
	 */
	public void setCodicePaese(String codicePaese);

	/**
	 * Returns the codice c i n int of this dati clienti fornitori.
	 *
	 * @return the codice c i n int of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceCINInt();

	/**
	 * Sets the codice c i n int of this dati clienti fornitori.
	 *
	 * @param codiceCINInt the codice c i n int of this dati clienti fornitori
	 */
	public void setCodiceCINInt(String codiceCINInt);

	/**
	 * Returns the codice c i n naz of this dati clienti fornitori.
	 *
	 * @return the codice c i n naz of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceCINNaz();

	/**
	 * Sets the codice c i n naz of this dati clienti fornitori.
	 *
	 * @param codiceCINNaz the codice c i n naz of this dati clienti fornitori
	 */
	public void setCodiceCINNaz(String codiceCINNaz);

	/**
	 * Returns the numero conto corrente of this dati clienti fornitori.
	 *
	 * @return the numero conto corrente of this dati clienti fornitori
	 */
	@AutoEscape
	public String getNumeroContoCorrente();

	/**
	 * Sets the numero conto corrente of this dati clienti fornitori.
	 *
	 * @param numeroContoCorrente the numero conto corrente of this dati clienti fornitori
	 */
	public void setNumeroContoCorrente(String numeroContoCorrente);

	/**
	 * Returns the i b a n of this dati clienti fornitori.
	 *
	 * @return the i b a n of this dati clienti fornitori
	 */
	@AutoEscape
	public String getIBAN();

	/**
	 * Sets the i b a n of this dati clienti fornitori.
	 *
	 * @param IBAN the i b a n of this dati clienti fornitori
	 */
	public void setIBAN(String IBAN);

	/**
	 * Returns the categoria economica of this dati clienti fornitori.
	 *
	 * @return the categoria economica of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCategoriaEconomica();

	/**
	 * Sets the categoria economica of this dati clienti fornitori.
	 *
	 * @param categoriaEconomica the categoria economica of this dati clienti fornitori
	 */
	public void setCategoriaEconomica(String categoriaEconomica);

	/**
	 * Returns the codice esenzione i v a of this dati clienti fornitori.
	 *
	 * @return the codice esenzione i v a of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceEsenzioneIVA();

	/**
	 * Sets the codice esenzione i v a of this dati clienti fornitori.
	 *
	 * @param codiceEsenzioneIVA the codice esenzione i v a of this dati clienti fornitori
	 */
	public void setCodiceEsenzioneIVA(String codiceEsenzioneIVA);

	/**
	 * Returns the codice divisa of this dati clienti fornitori.
	 *
	 * @return the codice divisa of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceDivisa();

	/**
	 * Sets the codice divisa of this dati clienti fornitori.
	 *
	 * @param codiceDivisa the codice divisa of this dati clienti fornitori
	 */
	public void setCodiceDivisa(String codiceDivisa);

	/**
	 * Returns the codice pagamento of this dati clienti fornitori.
	 *
	 * @return the codice pagamento of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodicePagamento();

	/**
	 * Sets the codice pagamento of this dati clienti fornitori.
	 *
	 * @param codicePagamento the codice pagamento of this dati clienti fornitori
	 */
	public void setCodicePagamento(String codicePagamento);

	/**
	 * Returns the codice zona of this dati clienti fornitori.
	 *
	 * @return the codice zona of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceZona();

	/**
	 * Sets the codice zona of this dati clienti fornitori.
	 *
	 * @param codiceZona the codice zona of this dati clienti fornitori
	 */
	public void setCodiceZona(String codiceZona);

	/**
	 * Returns the codice agente of this dati clienti fornitori.
	 *
	 * @return the codice agente of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceAgente();

	/**
	 * Sets the codice agente of this dati clienti fornitori.
	 *
	 * @param codiceAgente the codice agente of this dati clienti fornitori
	 */
	public void setCodiceAgente(String codiceAgente);

	/**
	 * Returns the codice gruppo agente of this dati clienti fornitori.
	 *
	 * @return the codice gruppo agente of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceGruppoAgente();

	/**
	 * Sets the codice gruppo agente of this dati clienti fornitori.
	 *
	 * @param codiceGruppoAgente the codice gruppo agente of this dati clienti fornitori
	 */
	public void setCodiceGruppoAgente(String codiceGruppoAgente);

	/**
	 * Returns the codice cat provv of this dati clienti fornitori.
	 *
	 * @return the codice cat provv of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceCatProvv();

	/**
	 * Sets the codice cat provv of this dati clienti fornitori.
	 *
	 * @param codiceCatProvv the codice cat provv of this dati clienti fornitori
	 */
	public void setCodiceCatProvv(String codiceCatProvv);

	/**
	 * Returns the codice spedizione of this dati clienti fornitori.
	 *
	 * @return the codice spedizione of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceSpedizione();

	/**
	 * Sets the codice spedizione of this dati clienti fornitori.
	 *
	 * @param codiceSpedizione the codice spedizione of this dati clienti fornitori
	 */
	public void setCodiceSpedizione(String codiceSpedizione);

	/**
	 * Returns the codice porto of this dati clienti fornitori.
	 *
	 * @return the codice porto of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodicePorto();

	/**
	 * Sets the codice porto of this dati clienti fornitori.
	 *
	 * @param codicePorto the codice porto of this dati clienti fornitori
	 */
	public void setCodicePorto(String codicePorto);

	/**
	 * Returns the codice vettore of this dati clienti fornitori.
	 *
	 * @return the codice vettore of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceVettore();

	/**
	 * Sets the codice vettore of this dati clienti fornitori.
	 *
	 * @param codiceVettore the codice vettore of this dati clienti fornitori
	 */
	public void setCodiceVettore(String codiceVettore);

	/**
	 * Returns the codice dest diversa of this dati clienti fornitori.
	 *
	 * @return the codice dest diversa of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceDestDiversa();

	/**
	 * Sets the codice dest diversa of this dati clienti fornitori.
	 *
	 * @param codiceDestDiversa the codice dest diversa of this dati clienti fornitori
	 */
	public void setCodiceDestDiversa(String codiceDestDiversa);

	/**
	 * Returns the codice listino prezzi of this dati clienti fornitori.
	 *
	 * @return the codice listino prezzi of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceListinoPrezzi();

	/**
	 * Sets the codice listino prezzi of this dati clienti fornitori.
	 *
	 * @param codiceListinoPrezzi the codice listino prezzi of this dati clienti fornitori
	 */
	public void setCodiceListinoPrezzi(String codiceListinoPrezzi);

	/**
	 * Returns the codice sconto totale of this dati clienti fornitori.
	 *
	 * @return the codice sconto totale of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceScontoTotale();

	/**
	 * Sets the codice sconto totale of this dati clienti fornitori.
	 *
	 * @param codiceScontoTotale the codice sconto totale of this dati clienti fornitori
	 */
	public void setCodiceScontoTotale(String codiceScontoTotale);

	/**
	 * Returns the codice sconto pre i v a of this dati clienti fornitori.
	 *
	 * @return the codice sconto pre i v a of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceScontoPreIVA();

	/**
	 * Sets the codice sconto pre i v a of this dati clienti fornitori.
	 *
	 * @param codiceScontoPreIVA the codice sconto pre i v a of this dati clienti fornitori
	 */
	public void setCodiceScontoPreIVA(String codiceScontoPreIVA);

	/**
	 * Returns the sconto cat1 of this dati clienti fornitori.
	 *
	 * @return the sconto cat1 of this dati clienti fornitori
	 */
	@AutoEscape
	public String getScontoCat1();

	/**
	 * Sets the sconto cat1 of this dati clienti fornitori.
	 *
	 * @param scontoCat1 the sconto cat1 of this dati clienti fornitori
	 */
	public void setScontoCat1(String scontoCat1);

	/**
	 * Returns the sconto cat2 of this dati clienti fornitori.
	 *
	 * @return the sconto cat2 of this dati clienti fornitori
	 */
	@AutoEscape
	public String getScontoCat2();

	/**
	 * Sets the sconto cat2 of this dati clienti fornitori.
	 *
	 * @param scontoCat2 the sconto cat2 of this dati clienti fornitori
	 */
	public void setScontoCat2(String scontoCat2);

	/**
	 * Returns the codice lingua of this dati clienti fornitori.
	 *
	 * @return the codice lingua of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceLingua();

	/**
	 * Sets the codice lingua of this dati clienti fornitori.
	 *
	 * @param codiceLingua the codice lingua of this dati clienti fornitori
	 */
	public void setCodiceLingua(String codiceLingua);

	/**
	 * Returns the codice lingua e c of this dati clienti fornitori.
	 *
	 * @return the codice lingua e c of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceLinguaEC();

	/**
	 * Sets the codice lingua e c of this dati clienti fornitori.
	 *
	 * @param codiceLinguaEC the codice lingua e c of this dati clienti fornitori
	 */
	public void setCodiceLinguaEC(String codiceLinguaEC);

	/**
	 * Returns the addebito bolli of this dati clienti fornitori.
	 *
	 * @return the addebito bolli of this dati clienti fornitori
	 */
	public boolean getAddebitoBolli();

	/**
	 * Returns <code>true</code> if this dati clienti fornitori is addebito bolli.
	 *
	 * @return <code>true</code> if this dati clienti fornitori is addebito bolli; <code>false</code> otherwise
	 */
	public boolean isAddebitoBolli();

	/**
	 * Sets whether this dati clienti fornitori is addebito bolli.
	 *
	 * @param addebitoBolli the addebito bolli of this dati clienti fornitori
	 */
	public void setAddebitoBolli(boolean addebitoBolli);

	/**
	 * Returns the addebito spese banca of this dati clienti fornitori.
	 *
	 * @return the addebito spese banca of this dati clienti fornitori
	 */
	public boolean getAddebitoSpeseBanca();

	/**
	 * Returns <code>true</code> if this dati clienti fornitori is addebito spese banca.
	 *
	 * @return <code>true</code> if this dati clienti fornitori is addebito spese banca; <code>false</code> otherwise
	 */
	public boolean isAddebitoSpeseBanca();

	/**
	 * Sets whether this dati clienti fornitori is addebito spese banca.
	 *
	 * @param addebitoSpeseBanca the addebito spese banca of this dati clienti fornitori
	 */
	public void setAddebitoSpeseBanca(boolean addebitoSpeseBanca);

	/**
	 * Returns the ragruppa bolle of this dati clienti fornitori.
	 *
	 * @return the ragruppa bolle of this dati clienti fornitori
	 */
	public boolean getRagruppaBolle();

	/**
	 * Returns <code>true</code> if this dati clienti fornitori is ragruppa bolle.
	 *
	 * @return <code>true</code> if this dati clienti fornitori is ragruppa bolle; <code>false</code> otherwise
	 */
	public boolean isRagruppaBolle();

	/**
	 * Sets whether this dati clienti fornitori is ragruppa bolle.
	 *
	 * @param ragruppaBolle the ragruppa bolle of this dati clienti fornitori
	 */
	public void setRagruppaBolle(boolean ragruppaBolle);

	/**
	 * Returns the sospensione i v a of this dati clienti fornitori.
	 *
	 * @return the sospensione i v a of this dati clienti fornitori
	 */
	public boolean getSospensioneIVA();

	/**
	 * Returns <code>true</code> if this dati clienti fornitori is sospensione i v a.
	 *
	 * @return <code>true</code> if this dati clienti fornitori is sospensione i v a; <code>false</code> otherwise
	 */
	public boolean isSospensioneIVA();

	/**
	 * Sets whether this dati clienti fornitori is sospensione i v a.
	 *
	 * @param sospensioneIVA the sospensione i v a of this dati clienti fornitori
	 */
	public void setSospensioneIVA(boolean sospensioneIVA);

	/**
	 * Returns the ragruppa ordini of this dati clienti fornitori.
	 *
	 * @return the ragruppa ordini of this dati clienti fornitori
	 */
	public int getRagruppaOrdini();

	/**
	 * Sets the ragruppa ordini of this dati clienti fornitori.
	 *
	 * @param ragruppaOrdini the ragruppa ordini of this dati clienti fornitori
	 */
	public void setRagruppaOrdini(int ragruppaOrdini);

	/**
	 * Returns the ragruppa x destinazione of this dati clienti fornitori.
	 *
	 * @return the ragruppa x destinazione of this dati clienti fornitori
	 */
	public boolean getRagruppaXDestinazione();

	/**
	 * Returns <code>true</code> if this dati clienti fornitori is ragruppa x destinazione.
	 *
	 * @return <code>true</code> if this dati clienti fornitori is ragruppa x destinazione; <code>false</code> otherwise
	 */
	public boolean isRagruppaXDestinazione();

	/**
	 * Sets whether this dati clienti fornitori is ragruppa x destinazione.
	 *
	 * @param ragruppaXDestinazione the ragruppa x destinazione of this dati clienti fornitori
	 */
	public void setRagruppaXDestinazione(boolean ragruppaXDestinazione);

	/**
	 * Returns the ragruppa x porto of this dati clienti fornitori.
	 *
	 * @return the ragruppa x porto of this dati clienti fornitori
	 */
	public boolean getRagruppaXPorto();

	/**
	 * Returns <code>true</code> if this dati clienti fornitori is ragruppa x porto.
	 *
	 * @return <code>true</code> if this dati clienti fornitori is ragruppa x porto; <code>false</code> otherwise
	 */
	public boolean isRagruppaXPorto();

	/**
	 * Sets whether this dati clienti fornitori is ragruppa x porto.
	 *
	 * @param ragruppaXPorto the ragruppa x porto of this dati clienti fornitori
	 */
	public void setRagruppaXPorto(boolean ragruppaXPorto);

	/**
	 * Returns the perc spese trasporto of this dati clienti fornitori.
	 *
	 * @return the perc spese trasporto of this dati clienti fornitori
	 */
	public double getPercSpeseTrasporto();

	/**
	 * Sets the perc spese trasporto of this dati clienti fornitori.
	 *
	 * @param percSpeseTrasporto the perc spese trasporto of this dati clienti fornitori
	 */
	public void setPercSpeseTrasporto(double percSpeseTrasporto);

	/**
	 * Returns the prezzo da proporre of this dati clienti fornitori.
	 *
	 * @return the prezzo da proporre of this dati clienti fornitori
	 */
	public int getPrezzoDaProporre();

	/**
	 * Sets the prezzo da proporre of this dati clienti fornitori.
	 *
	 * @param prezzoDaProporre the prezzo da proporre of this dati clienti fornitori
	 */
	public void setPrezzoDaProporre(int prezzoDaProporre);

	/**
	 * Returns the sogetto fatturazione of this dati clienti fornitori.
	 *
	 * @return the sogetto fatturazione of this dati clienti fornitori
	 */
	@AutoEscape
	public String getSogettoFatturazione();

	/**
	 * Sets the sogetto fatturazione of this dati clienti fornitori.
	 *
	 * @param sogettoFatturazione the sogetto fatturazione of this dati clienti fornitori
	 */
	public void setSogettoFatturazione(String sogettoFatturazione);

	/**
	 * Returns the codice ragg effetti of this dati clienti fornitori.
	 *
	 * @return the codice ragg effetti of this dati clienti fornitori
	 */
	@AutoEscape
	public String getCodiceRaggEffetti();

	/**
	 * Sets the codice ragg effetti of this dati clienti fornitori.
	 *
	 * @param codiceRaggEffetti the codice ragg effetti of this dati clienti fornitori
	 */
	public void setCodiceRaggEffetti(String codiceRaggEffetti);

	/**
	 * Returns the riporto riferimenti of this dati clienti fornitori.
	 *
	 * @return the riporto riferimenti of this dati clienti fornitori
	 */
	public int getRiportoRiferimenti();

	/**
	 * Sets the riporto riferimenti of this dati clienti fornitori.
	 *
	 * @param riportoRiferimenti the riporto riferimenti of this dati clienti fornitori
	 */
	public void setRiportoRiferimenti(int riportoRiferimenti);

	/**
	 * Returns the stampa prezzo of this dati clienti fornitori.
	 *
	 * @return the stampa prezzo of this dati clienti fornitori
	 */
	public boolean getStampaPrezzo();

	/**
	 * Returns <code>true</code> if this dati clienti fornitori is stampa prezzo.
	 *
	 * @return <code>true</code> if this dati clienti fornitori is stampa prezzo; <code>false</code> otherwise
	 */
	public boolean isStampaPrezzo();

	/**
	 * Sets whether this dati clienti fornitori is stampa prezzo.
	 *
	 * @param stampaPrezzo the stampa prezzo of this dati clienti fornitori
	 */
	public void setStampaPrezzo(boolean stampaPrezzo);

	/**
	 * Returns the bloccato of this dati clienti fornitori.
	 *
	 * @return the bloccato of this dati clienti fornitori
	 */
	public int getBloccato();

	/**
	 * Sets the bloccato of this dati clienti fornitori.
	 *
	 * @param bloccato the bloccato of this dati clienti fornitori
	 */
	public void setBloccato(int bloccato);

	/**
	 * Returns the motivazione of this dati clienti fornitori.
	 *
	 * @return the motivazione of this dati clienti fornitori
	 */
	@AutoEscape
	public String getMotivazione();

	/**
	 * Sets the motivazione of this dati clienti fornitori.
	 *
	 * @param motivazione the motivazione of this dati clienti fornitori
	 */
	public void setMotivazione(String motivazione);

	/**
	 * Returns the livello blocco of this dati clienti fornitori.
	 *
	 * @return the livello blocco of this dati clienti fornitori
	 */
	public int getLivelloBlocco();

	/**
	 * Sets the livello blocco of this dati clienti fornitori.
	 *
	 * @param livelloBlocco the livello blocco of this dati clienti fornitori
	 */
	public void setLivelloBlocco(int livelloBlocco);

	/**
	 * Returns the addebito c o n a i of this dati clienti fornitori.
	 *
	 * @return the addebito c o n a i of this dati clienti fornitori
	 */
	public boolean getAddebitoCONAI();

	/**
	 * Returns <code>true</code> if this dati clienti fornitori is addebito c o n a i.
	 *
	 * @return <code>true</code> if this dati clienti fornitori is addebito c o n a i; <code>false</code> otherwise
	 */
	public boolean isAddebitoCONAI();

	/**
	 * Sets whether this dati clienti fornitori is addebito c o n a i.
	 *
	 * @param addebitoCONAI the addebito c o n a i of this dati clienti fornitori
	 */
	public void setAddebitoCONAI(boolean addebitoCONAI);

	/**
	 * Returns the lib str1 of this dati clienti fornitori.
	 *
	 * @return the lib str1 of this dati clienti fornitori
	 */
	@AutoEscape
	public String getLibStr1();

	/**
	 * Sets the lib str1 of this dati clienti fornitori.
	 *
	 * @param libStr1 the lib str1 of this dati clienti fornitori
	 */
	public void setLibStr1(String libStr1);

	/**
	 * Returns the lib str2 of this dati clienti fornitori.
	 *
	 * @return the lib str2 of this dati clienti fornitori
	 */
	@AutoEscape
	public String getLibStr2();

	/**
	 * Sets the lib str2 of this dati clienti fornitori.
	 *
	 * @param libStr2 the lib str2 of this dati clienti fornitori
	 */
	public void setLibStr2(String libStr2);

	/**
	 * Returns the lib str3 of this dati clienti fornitori.
	 *
	 * @return the lib str3 of this dati clienti fornitori
	 */
	@AutoEscape
	public String getLibStr3();

	/**
	 * Sets the lib str3 of this dati clienti fornitori.
	 *
	 * @param libStr3 the lib str3 of this dati clienti fornitori
	 */
	public void setLibStr3(String libStr3);

	/**
	 * Returns the lib str4 of this dati clienti fornitori.
	 *
	 * @return the lib str4 of this dati clienti fornitori
	 */
	@AutoEscape
	public String getLibStr4();

	/**
	 * Sets the lib str4 of this dati clienti fornitori.
	 *
	 * @param libStr4 the lib str4 of this dati clienti fornitori
	 */
	public void setLibStr4(String libStr4);

	/**
	 * Returns the lib str5 of this dati clienti fornitori.
	 *
	 * @return the lib str5 of this dati clienti fornitori
	 */
	@AutoEscape
	public String getLibStr5();

	/**
	 * Sets the lib str5 of this dati clienti fornitori.
	 *
	 * @param libStr5 the lib str5 of this dati clienti fornitori
	 */
	public void setLibStr5(String libStr5);

	/**
	 * Returns the lib dat1 of this dati clienti fornitori.
	 *
	 * @return the lib dat1 of this dati clienti fornitori
	 */
	public Date getLibDat1();

	/**
	 * Sets the lib dat1 of this dati clienti fornitori.
	 *
	 * @param libDat1 the lib dat1 of this dati clienti fornitori
	 */
	public void setLibDat1(Date libDat1);

	/**
	 * Returns the lib dat2 of this dati clienti fornitori.
	 *
	 * @return the lib dat2 of this dati clienti fornitori
	 */
	public Date getLibDat2();

	/**
	 * Sets the lib dat2 of this dati clienti fornitori.
	 *
	 * @param libDat2 the lib dat2 of this dati clienti fornitori
	 */
	public void setLibDat2(Date libDat2);

	/**
	 * Returns the lib dat3 of this dati clienti fornitori.
	 *
	 * @return the lib dat3 of this dati clienti fornitori
	 */
	public Date getLibDat3();

	/**
	 * Sets the lib dat3 of this dati clienti fornitori.
	 *
	 * @param libDat3 the lib dat3 of this dati clienti fornitori
	 */
	public void setLibDat3(Date libDat3);

	/**
	 * Returns the lib dat4 of this dati clienti fornitori.
	 *
	 * @return the lib dat4 of this dati clienti fornitori
	 */
	public Date getLibDat4();

	/**
	 * Sets the lib dat4 of this dati clienti fornitori.
	 *
	 * @param libDat4 the lib dat4 of this dati clienti fornitori
	 */
	public void setLibDat4(Date libDat4);

	/**
	 * Returns the lib dat5 of this dati clienti fornitori.
	 *
	 * @return the lib dat5 of this dati clienti fornitori
	 */
	public Date getLibDat5();

	/**
	 * Sets the lib dat5 of this dati clienti fornitori.
	 *
	 * @param libDat5 the lib dat5 of this dati clienti fornitori
	 */
	public void setLibDat5(Date libDat5);

	/**
	 * Returns the lib lng1 of this dati clienti fornitori.
	 *
	 * @return the lib lng1 of this dati clienti fornitori
	 */
	public long getLibLng1();

	/**
	 * Sets the lib lng1 of this dati clienti fornitori.
	 *
	 * @param libLng1 the lib lng1 of this dati clienti fornitori
	 */
	public void setLibLng1(long libLng1);

	/**
	 * Returns the lib lng2 of this dati clienti fornitori.
	 *
	 * @return the lib lng2 of this dati clienti fornitori
	 */
	public long getLibLng2();

	/**
	 * Sets the lib lng2 of this dati clienti fornitori.
	 *
	 * @param libLng2 the lib lng2 of this dati clienti fornitori
	 */
	public void setLibLng2(long libLng2);

	/**
	 * Returns the lib lng3 of this dati clienti fornitori.
	 *
	 * @return the lib lng3 of this dati clienti fornitori
	 */
	public long getLibLng3();

	/**
	 * Sets the lib lng3 of this dati clienti fornitori.
	 *
	 * @param libLng3 the lib lng3 of this dati clienti fornitori
	 */
	public void setLibLng3(long libLng3);

	/**
	 * Returns the lib lng4 of this dati clienti fornitori.
	 *
	 * @return the lib lng4 of this dati clienti fornitori
	 */
	public long getLibLng4();

	/**
	 * Sets the lib lng4 of this dati clienti fornitori.
	 *
	 * @param libLng4 the lib lng4 of this dati clienti fornitori
	 */
	public void setLibLng4(long libLng4);

	/**
	 * Returns the lib lng5 of this dati clienti fornitori.
	 *
	 * @return the lib lng5 of this dati clienti fornitori
	 */
	public long getLibLng5();

	/**
	 * Sets the lib lng5 of this dati clienti fornitori.
	 *
	 * @param libLng5 the lib lng5 of this dati clienti fornitori
	 */
	public void setLibLng5(long libLng5);

	/**
	 * Returns the lib dbl1 of this dati clienti fornitori.
	 *
	 * @return the lib dbl1 of this dati clienti fornitori
	 */
	public double getLibDbl1();

	/**
	 * Sets the lib dbl1 of this dati clienti fornitori.
	 *
	 * @param libDbl1 the lib dbl1 of this dati clienti fornitori
	 */
	public void setLibDbl1(double libDbl1);

	/**
	 * Returns the lib dbl2 of this dati clienti fornitori.
	 *
	 * @return the lib dbl2 of this dati clienti fornitori
	 */
	public double getLibDbl2();

	/**
	 * Sets the lib dbl2 of this dati clienti fornitori.
	 *
	 * @param libDbl2 the lib dbl2 of this dati clienti fornitori
	 */
	public void setLibDbl2(double libDbl2);

	/**
	 * Returns the lib dbl3 of this dati clienti fornitori.
	 *
	 * @return the lib dbl3 of this dati clienti fornitori
	 */
	public double getLibDbl3();

	/**
	 * Sets the lib dbl3 of this dati clienti fornitori.
	 *
	 * @param libDbl3 the lib dbl3 of this dati clienti fornitori
	 */
	public void setLibDbl3(double libDbl3);

	/**
	 * Returns the lib dbl4 of this dati clienti fornitori.
	 *
	 * @return the lib dbl4 of this dati clienti fornitori
	 */
	public double getLibDbl4();

	/**
	 * Sets the lib dbl4 of this dati clienti fornitori.
	 *
	 * @param libDbl4 the lib dbl4 of this dati clienti fornitori
	 */
	public void setLibDbl4(double libDbl4);

	/**
	 * Returns the lib dbl5 of this dati clienti fornitori.
	 *
	 * @return the lib dbl5 of this dati clienti fornitori
	 */
	public double getLibDbl5();

	/**
	 * Sets the lib dbl5 of this dati clienti fornitori.
	 *
	 * @param libDbl5 the lib dbl5 of this dati clienti fornitori
	 */
	public void setLibDbl5(double libDbl5);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(
		it.bysoftware.ct.model.DatiClientiFornitori datiClientiFornitori);

	@Override
	public int hashCode();

	@Override
	public CacheModel<it.bysoftware.ct.model.DatiClientiFornitori> toCacheModel();

	@Override
	public it.bysoftware.ct.model.DatiClientiFornitori toEscapedModel();

	@Override
	public it.bysoftware.ct.model.DatiClientiFornitori toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}