/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.ListiniPrezziArticoliLocalServiceUtil;
import it.bysoftware.ct.service.persistence.ListiniPrezziArticoliPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class ListiniPrezziArticoliClp extends BaseModelImpl<ListiniPrezziArticoli>
	implements ListiniPrezziArticoli {
	public ListiniPrezziArticoliClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ListiniPrezziArticoli.class;
	}

	@Override
	public String getModelClassName() {
		return ListiniPrezziArticoli.class.getName();
	}

	@Override
	public ListiniPrezziArticoliPK getPrimaryKey() {
		return new ListiniPrezziArticoliPK(_codiceListino, _codiceDivisa,
			_tipoSoggetto, _codiceSoggetto, _dataInizioValidita,
			_codiceArticolo, _codiceVariante, _codiceLavorazione,
			_quantInizioValiditaPrezzo);
	}

	@Override
	public void setPrimaryKey(ListiniPrezziArticoliPK primaryKey) {
		setCodiceListino(primaryKey.codiceListino);
		setCodiceDivisa(primaryKey.codiceDivisa);
		setTipoSoggetto(primaryKey.tipoSoggetto);
		setCodiceSoggetto(primaryKey.codiceSoggetto);
		setDataInizioValidita(primaryKey.dataInizioValidita);
		setCodiceArticolo(primaryKey.codiceArticolo);
		setCodiceVariante(primaryKey.codiceVariante);
		setCodiceLavorazione(primaryKey.codiceLavorazione);
		setQuantInizioValiditaPrezzo(primaryKey.quantInizioValiditaPrezzo);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new ListiniPrezziArticoliPK(_codiceListino, _codiceDivisa,
			_tipoSoggetto, _codiceSoggetto, _dataInizioValidita,
			_codiceArticolo, _codiceVariante, _codiceLavorazione,
			_quantInizioValiditaPrezzo);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((ListiniPrezziArticoliPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codiceListino", getCodiceListino());
		attributes.put("codiceDivisa", getCodiceDivisa());
		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceSoggetto", getCodiceSoggetto());
		attributes.put("dataInizioValidita", getDataInizioValidita());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("codiceLavorazione", getCodiceLavorazione());
		attributes.put("quantInizioValiditaPrezzo",
			getQuantInizioValiditaPrezzo());
		attributes.put("dataFineValidita", getDataFineValidita());
		attributes.put("quantFineValiditaPrezzo", getQuantFineValiditaPrezzo());
		attributes.put("prezzoNettoIVA", getPrezzoNettoIVA());
		attributes.put("codiceIVA", getCodiceIVA());
		attributes.put("utilizzoSconti", getUtilizzoSconti());
		attributes.put("scontoListino1", getScontoListino1());
		attributes.put("scontoListino2", getScontoListino2());
		attributes.put("scontoListino3", getScontoListino3());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codiceListino = (String)attributes.get("codiceListino");

		if (codiceListino != null) {
			setCodiceListino(codiceListino);
		}

		String codiceDivisa = (String)attributes.get("codiceDivisa");

		if (codiceDivisa != null) {
			setCodiceDivisa(codiceDivisa);
		}

		Integer tipoSoggetto = (Integer)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceSoggetto = (String)attributes.get("codiceSoggetto");

		if (codiceSoggetto != null) {
			setCodiceSoggetto(codiceSoggetto);
		}

		Date dataInizioValidita = (Date)attributes.get("dataInizioValidita");

		if (dataInizioValidita != null) {
			setDataInizioValidita(dataInizioValidita);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		String codiceLavorazione = (String)attributes.get("codiceLavorazione");

		if (codiceLavorazione != null) {
			setCodiceLavorazione(codiceLavorazione);
		}

		Double quantInizioValiditaPrezzo = (Double)attributes.get(
				"quantInizioValiditaPrezzo");

		if (quantInizioValiditaPrezzo != null) {
			setQuantInizioValiditaPrezzo(quantInizioValiditaPrezzo);
		}

		Date dataFineValidita = (Date)attributes.get("dataFineValidita");

		if (dataFineValidita != null) {
			setDataFineValidita(dataFineValidita);
		}

		Double quantFineValiditaPrezzo = (Double)attributes.get(
				"quantFineValiditaPrezzo");

		if (quantFineValiditaPrezzo != null) {
			setQuantFineValiditaPrezzo(quantFineValiditaPrezzo);
		}

		Double prezzoNettoIVA = (Double)attributes.get("prezzoNettoIVA");

		if (prezzoNettoIVA != null) {
			setPrezzoNettoIVA(prezzoNettoIVA);
		}

		String codiceIVA = (String)attributes.get("codiceIVA");

		if (codiceIVA != null) {
			setCodiceIVA(codiceIVA);
		}

		Boolean utilizzoSconti = (Boolean)attributes.get("utilizzoSconti");

		if (utilizzoSconti != null) {
			setUtilizzoSconti(utilizzoSconti);
		}

		Double scontoListino1 = (Double)attributes.get("scontoListino1");

		if (scontoListino1 != null) {
			setScontoListino1(scontoListino1);
		}

		Double scontoListino2 = (Double)attributes.get("scontoListino2");

		if (scontoListino2 != null) {
			setScontoListino2(scontoListino2);
		}

		Double scontoListino3 = (Double)attributes.get("scontoListino3");

		if (scontoListino3 != null) {
			setScontoListino3(scontoListino3);
		}
	}

	@Override
	public String getCodiceListino() {
		return _codiceListino;
	}

	@Override
	public void setCodiceListino(String codiceListino) {
		_codiceListino = codiceListino;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceListino", String.class);

				method.invoke(_listiniPrezziArticoliRemoteModel, codiceListino);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDivisa() {
		return _codiceDivisa;
	}

	@Override
	public void setCodiceDivisa(String codiceDivisa) {
		_codiceDivisa = codiceDivisa;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDivisa", String.class);

				method.invoke(_listiniPrezziArticoliRemoteModel, codiceDivisa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public void setTipoSoggetto(int tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoSoggetto", int.class);

				method.invoke(_listiniPrezziArticoliRemoteModel, tipoSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceSoggetto() {
		return _codiceSoggetto;
	}

	@Override
	public void setCodiceSoggetto(String codiceSoggetto) {
		_codiceSoggetto = codiceSoggetto;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceSoggetto",
						String.class);

				method.invoke(_listiniPrezziArticoliRemoteModel, codiceSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataInizioValidita() {
		return _dataInizioValidita;
	}

	@Override
	public void setDataInizioValidita(Date dataInizioValidita) {
		_dataInizioValidita = dataInizioValidita;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setDataInizioValidita",
						Date.class);

				method.invoke(_listiniPrezziArticoliRemoteModel,
					dataInizioValidita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	@Override
	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceArticolo",
						String.class);

				method.invoke(_listiniPrezziArticoliRemoteModel, codiceArticolo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceVariante() {
		return _codiceVariante;
	}

	@Override
	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceVariante",
						String.class);

				method.invoke(_listiniPrezziArticoliRemoteModel, codiceVariante);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceLavorazione() {
		return _codiceLavorazione;
	}

	@Override
	public void setCodiceLavorazione(String codiceLavorazione) {
		_codiceLavorazione = codiceLavorazione;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceLavorazione",
						String.class);

				method.invoke(_listiniPrezziArticoliRemoteModel,
					codiceLavorazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantInizioValiditaPrezzo() {
		return _quantInizioValiditaPrezzo;
	}

	@Override
	public void setQuantInizioValiditaPrezzo(double quantInizioValiditaPrezzo) {
		_quantInizioValiditaPrezzo = quantInizioValiditaPrezzo;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantInizioValiditaPrezzo",
						double.class);

				method.invoke(_listiniPrezziArticoliRemoteModel,
					quantInizioValiditaPrezzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataFineValidita() {
		return _dataFineValidita;
	}

	@Override
	public void setDataFineValidita(Date dataFineValidita) {
		_dataFineValidita = dataFineValidita;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setDataFineValidita",
						Date.class);

				method.invoke(_listiniPrezziArticoliRemoteModel,
					dataFineValidita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantFineValiditaPrezzo() {
		return _quantFineValiditaPrezzo;
	}

	@Override
	public void setQuantFineValiditaPrezzo(double quantFineValiditaPrezzo) {
		_quantFineValiditaPrezzo = quantFineValiditaPrezzo;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantFineValiditaPrezzo",
						double.class);

				method.invoke(_listiniPrezziArticoliRemoteModel,
					quantFineValiditaPrezzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPrezzoNettoIVA() {
		return _prezzoNettoIVA;
	}

	@Override
	public void setPrezzoNettoIVA(double prezzoNettoIVA) {
		_prezzoNettoIVA = prezzoNettoIVA;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setPrezzoNettoIVA",
						double.class);

				method.invoke(_listiniPrezziArticoliRemoteModel, prezzoNettoIVA);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVA() {
		return _codiceIVA;
	}

	@Override
	public void setCodiceIVA(String codiceIVA) {
		_codiceIVA = codiceIVA;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVA", String.class);

				method.invoke(_listiniPrezziArticoliRemoteModel, codiceIVA);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getUtilizzoSconti() {
		return _utilizzoSconti;
	}

	@Override
	public boolean isUtilizzoSconti() {
		return _utilizzoSconti;
	}

	@Override
	public void setUtilizzoSconti(boolean utilizzoSconti) {
		_utilizzoSconti = utilizzoSconti;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setUtilizzoSconti",
						boolean.class);

				method.invoke(_listiniPrezziArticoliRemoteModel, utilizzoSconti);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getScontoListino1() {
		return _scontoListino1;
	}

	@Override
	public void setScontoListino1(double scontoListino1) {
		_scontoListino1 = scontoListino1;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setScontoListino1",
						double.class);

				method.invoke(_listiniPrezziArticoliRemoteModel, scontoListino1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getScontoListino2() {
		return _scontoListino2;
	}

	@Override
	public void setScontoListino2(double scontoListino2) {
		_scontoListino2 = scontoListino2;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setScontoListino2",
						double.class);

				method.invoke(_listiniPrezziArticoliRemoteModel, scontoListino2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getScontoListino3() {
		return _scontoListino3;
	}

	@Override
	public void setScontoListino3(double scontoListino3) {
		_scontoListino3 = scontoListino3;

		if (_listiniPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _listiniPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setScontoListino3",
						double.class);

				method.invoke(_listiniPrezziArticoliRemoteModel, scontoListino3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getListiniPrezziArticoliRemoteModel() {
		return _listiniPrezziArticoliRemoteModel;
	}

	public void setListiniPrezziArticoliRemoteModel(
		BaseModel<?> listiniPrezziArticoliRemoteModel) {
		_listiniPrezziArticoliRemoteModel = listiniPrezziArticoliRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _listiniPrezziArticoliRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_listiniPrezziArticoliRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ListiniPrezziArticoliLocalServiceUtil.addListiniPrezziArticoli(this);
		}
		else {
			ListiniPrezziArticoliLocalServiceUtil.updateListiniPrezziArticoli(this);
		}
	}

	@Override
	public ListiniPrezziArticoli toEscapedModel() {
		return (ListiniPrezziArticoli)ProxyUtil.newProxyInstance(ListiniPrezziArticoli.class.getClassLoader(),
			new Class[] { ListiniPrezziArticoli.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ListiniPrezziArticoliClp clone = new ListiniPrezziArticoliClp();

		clone.setCodiceListino(getCodiceListino());
		clone.setCodiceDivisa(getCodiceDivisa());
		clone.setTipoSoggetto(getTipoSoggetto());
		clone.setCodiceSoggetto(getCodiceSoggetto());
		clone.setDataInizioValidita(getDataInizioValidita());
		clone.setCodiceArticolo(getCodiceArticolo());
		clone.setCodiceVariante(getCodiceVariante());
		clone.setCodiceLavorazione(getCodiceLavorazione());
		clone.setQuantInizioValiditaPrezzo(getQuantInizioValiditaPrezzo());
		clone.setDataFineValidita(getDataFineValidita());
		clone.setQuantFineValiditaPrezzo(getQuantFineValiditaPrezzo());
		clone.setPrezzoNettoIVA(getPrezzoNettoIVA());
		clone.setCodiceIVA(getCodiceIVA());
		clone.setUtilizzoSconti(getUtilizzoSconti());
		clone.setScontoListino1(getScontoListino1());
		clone.setScontoListino2(getScontoListino2());
		clone.setScontoListino3(getScontoListino3());

		return clone;
	}

	@Override
	public int compareTo(ListiniPrezziArticoli listiniPrezziArticoli) {
		ListiniPrezziArticoliPK primaryKey = listiniPrezziArticoli.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ListiniPrezziArticoliClp)) {
			return false;
		}

		ListiniPrezziArticoliClp listiniPrezziArticoli = (ListiniPrezziArticoliClp)obj;

		ListiniPrezziArticoliPK primaryKey = listiniPrezziArticoli.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(35);

		sb.append("{codiceListino=");
		sb.append(getCodiceListino());
		sb.append(", codiceDivisa=");
		sb.append(getCodiceDivisa());
		sb.append(", tipoSoggetto=");
		sb.append(getTipoSoggetto());
		sb.append(", codiceSoggetto=");
		sb.append(getCodiceSoggetto());
		sb.append(", dataInizioValidita=");
		sb.append(getDataInizioValidita());
		sb.append(", codiceArticolo=");
		sb.append(getCodiceArticolo());
		sb.append(", codiceVariante=");
		sb.append(getCodiceVariante());
		sb.append(", codiceLavorazione=");
		sb.append(getCodiceLavorazione());
		sb.append(", quantInizioValiditaPrezzo=");
		sb.append(getQuantInizioValiditaPrezzo());
		sb.append(", dataFineValidita=");
		sb.append(getDataFineValidita());
		sb.append(", quantFineValiditaPrezzo=");
		sb.append(getQuantFineValiditaPrezzo());
		sb.append(", prezzoNettoIVA=");
		sb.append(getPrezzoNettoIVA());
		sb.append(", codiceIVA=");
		sb.append(getCodiceIVA());
		sb.append(", utilizzoSconti=");
		sb.append(getUtilizzoSconti());
		sb.append(", scontoListino1=");
		sb.append(getScontoListino1());
		sb.append(", scontoListino2=");
		sb.append(getScontoListino2());
		sb.append(", scontoListino3=");
		sb.append(getScontoListino3());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(55);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.ListiniPrezziArticoli");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>codiceListino</column-name><column-value><![CDATA[");
		sb.append(getCodiceListino());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDivisa</column-name><column-value><![CDATA[");
		sb.append(getCodiceDivisa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoSoggetto</column-name><column-value><![CDATA[");
		sb.append(getTipoSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceSoggetto</column-name><column-value><![CDATA[");
		sb.append(getCodiceSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataInizioValidita</column-name><column-value><![CDATA[");
		sb.append(getDataInizioValidita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceArticolo</column-name><column-value><![CDATA[");
		sb.append(getCodiceArticolo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceVariante</column-name><column-value><![CDATA[");
		sb.append(getCodiceVariante());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceLavorazione</column-name><column-value><![CDATA[");
		sb.append(getCodiceLavorazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantInizioValiditaPrezzo</column-name><column-value><![CDATA[");
		sb.append(getQuantInizioValiditaPrezzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataFineValidita</column-name><column-value><![CDATA[");
		sb.append(getDataFineValidita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantFineValiditaPrezzo</column-name><column-value><![CDATA[");
		sb.append(getQuantFineValiditaPrezzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>prezzoNettoIVA</column-name><column-value><![CDATA[");
		sb.append(getPrezzoNettoIVA());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVA</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVA());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>utilizzoSconti</column-name><column-value><![CDATA[");
		sb.append(getUtilizzoSconti());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>scontoListino1</column-name><column-value><![CDATA[");
		sb.append(getScontoListino1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>scontoListino2</column-name><column-value><![CDATA[");
		sb.append(getScontoListino2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>scontoListino3</column-name><column-value><![CDATA[");
		sb.append(getScontoListino3());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _codiceListino;
	private String _codiceDivisa;
	private int _tipoSoggetto;
	private String _codiceSoggetto;
	private Date _dataInizioValidita;
	private String _codiceArticolo;
	private String _codiceVariante;
	private String _codiceLavorazione;
	private double _quantInizioValiditaPrezzo;
	private Date _dataFineValidita;
	private double _quantFineValiditaPrezzo;
	private double _prezzoNettoIVA;
	private String _codiceIVA;
	private boolean _utilizzoSconti;
	private double _scontoListino1;
	private double _scontoListino2;
	private double _scontoListino3;
	private BaseModel<?> _listiniPrezziArticoliRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}