/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.AnagraficaAgentiServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.AnagraficaAgentiServiceSoap
 * @generated
 */
public class AnagraficaAgentiSoap implements Serializable {
	public static AnagraficaAgentiSoap toSoapModel(AnagraficaAgenti model) {
		AnagraficaAgentiSoap soapModel = new AnagraficaAgentiSoap();

		soapModel.setCodiceAgente(model.getCodiceAgente());
		soapModel.setNome(model.getNome());
		soapModel.setCodiceZona(model.getCodiceZona());
		soapModel.setCodiceProvvRigo(model.getCodiceProvvRigo());
		soapModel.setCodiceProvvChiusura(model.getCodiceProvvChiusura());
		soapModel.setBudget(model.getBudget());
		soapModel.setTipoLiquidazione(model.getTipoLiquidazione());
		soapModel.setGiorniToll(model.getGiorniToll());
		soapModel.setTipoDivisa(model.getTipoDivisa());

		return soapModel;
	}

	public static AnagraficaAgentiSoap[] toSoapModels(AnagraficaAgenti[] models) {
		AnagraficaAgentiSoap[] soapModels = new AnagraficaAgentiSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AnagraficaAgentiSoap[][] toSoapModels(
		AnagraficaAgenti[][] models) {
		AnagraficaAgentiSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AnagraficaAgentiSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AnagraficaAgentiSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AnagraficaAgentiSoap[] toSoapModels(
		List<AnagraficaAgenti> models) {
		List<AnagraficaAgentiSoap> soapModels = new ArrayList<AnagraficaAgentiSoap>(models.size());

		for (AnagraficaAgenti model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AnagraficaAgentiSoap[soapModels.size()]);
	}

	public AnagraficaAgentiSoap() {
	}

	public String getPrimaryKey() {
		return _codiceAgente;
	}

	public void setPrimaryKey(String pk) {
		setCodiceAgente(pk);
	}

	public String getCodiceAgente() {
		return _codiceAgente;
	}

	public void setCodiceAgente(String codiceAgente) {
		_codiceAgente = codiceAgente;
	}

	public String getNome() {
		return _nome;
	}

	public void setNome(String nome) {
		_nome = nome;
	}

	public String getCodiceZona() {
		return _codiceZona;
	}

	public void setCodiceZona(String codiceZona) {
		_codiceZona = codiceZona;
	}

	public String getCodiceProvvRigo() {
		return _codiceProvvRigo;
	}

	public void setCodiceProvvRigo(String codiceProvvRigo) {
		_codiceProvvRigo = codiceProvvRigo;
	}

	public String getCodiceProvvChiusura() {
		return _codiceProvvChiusura;
	}

	public void setCodiceProvvChiusura(String codiceProvvChiusura) {
		_codiceProvvChiusura = codiceProvvChiusura;
	}

	public double getBudget() {
		return _budget;
	}

	public void setBudget(double budget) {
		_budget = budget;
	}

	public int getTipoLiquidazione() {
		return _tipoLiquidazione;
	}

	public void setTipoLiquidazione(int tipoLiquidazione) {
		_tipoLiquidazione = tipoLiquidazione;
	}

	public int getGiorniToll() {
		return _giorniToll;
	}

	public void setGiorniToll(int giorniToll) {
		_giorniToll = giorniToll;
	}

	public boolean getTipoDivisa() {
		return _tipoDivisa;
	}

	public boolean isTipoDivisa() {
		return _tipoDivisa;
	}

	public void setTipoDivisa(boolean tipoDivisa) {
		_tipoDivisa = tipoDivisa;
	}

	private String _codiceAgente;
	private String _nome;
	private String _codiceZona;
	private String _codiceProvvRigo;
	private String _codiceProvvChiusura;
	private double _budget;
	private int _tipoLiquidazione;
	private int _giorniToll;
	private boolean _tipoDivisa;
}