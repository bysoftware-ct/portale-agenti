/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AnagraficaAgenti}.
 * </p>
 *
 * @author Mario Torrisi
 * @see AnagraficaAgenti
 * @generated
 */
public class AnagraficaAgentiWrapper implements AnagraficaAgenti,
	ModelWrapper<AnagraficaAgenti> {
	public AnagraficaAgentiWrapper(AnagraficaAgenti anagraficaAgenti) {
		_anagraficaAgenti = anagraficaAgenti;
	}

	@Override
	public Class<?> getModelClass() {
		return AnagraficaAgenti.class;
	}

	@Override
	public String getModelClassName() {
		return AnagraficaAgenti.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codiceAgente", getCodiceAgente());
		attributes.put("nome", getNome());
		attributes.put("codiceZona", getCodiceZona());
		attributes.put("codiceProvvRigo", getCodiceProvvRigo());
		attributes.put("codiceProvvChiusura", getCodiceProvvChiusura());
		attributes.put("budget", getBudget());
		attributes.put("tipoLiquidazione", getTipoLiquidazione());
		attributes.put("giorniToll", getGiorniToll());
		attributes.put("tipoDivisa", getTipoDivisa());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codiceAgente = (String)attributes.get("codiceAgente");

		if (codiceAgente != null) {
			setCodiceAgente(codiceAgente);
		}

		String nome = (String)attributes.get("nome");

		if (nome != null) {
			setNome(nome);
		}

		String codiceZona = (String)attributes.get("codiceZona");

		if (codiceZona != null) {
			setCodiceZona(codiceZona);
		}

		String codiceProvvRigo = (String)attributes.get("codiceProvvRigo");

		if (codiceProvvRigo != null) {
			setCodiceProvvRigo(codiceProvvRigo);
		}

		String codiceProvvChiusura = (String)attributes.get(
				"codiceProvvChiusura");

		if (codiceProvvChiusura != null) {
			setCodiceProvvChiusura(codiceProvvChiusura);
		}

		Double budget = (Double)attributes.get("budget");

		if (budget != null) {
			setBudget(budget);
		}

		Integer tipoLiquidazione = (Integer)attributes.get("tipoLiquidazione");

		if (tipoLiquidazione != null) {
			setTipoLiquidazione(tipoLiquidazione);
		}

		Integer giorniToll = (Integer)attributes.get("giorniToll");

		if (giorniToll != null) {
			setGiorniToll(giorniToll);
		}

		Boolean tipoDivisa = (Boolean)attributes.get("tipoDivisa");

		if (tipoDivisa != null) {
			setTipoDivisa(tipoDivisa);
		}
	}

	/**
	* Returns the primary key of this anagrafica agenti.
	*
	* @return the primary key of this anagrafica agenti
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _anagraficaAgenti.getPrimaryKey();
	}

	/**
	* Sets the primary key of this anagrafica agenti.
	*
	* @param primaryKey the primary key of this anagrafica agenti
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_anagraficaAgenti.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the codice agente of this anagrafica agenti.
	*
	* @return the codice agente of this anagrafica agenti
	*/
	@Override
	public java.lang.String getCodiceAgente() {
		return _anagraficaAgenti.getCodiceAgente();
	}

	/**
	* Sets the codice agente of this anagrafica agenti.
	*
	* @param codiceAgente the codice agente of this anagrafica agenti
	*/
	@Override
	public void setCodiceAgente(java.lang.String codiceAgente) {
		_anagraficaAgenti.setCodiceAgente(codiceAgente);
	}

	/**
	* Returns the nome of this anagrafica agenti.
	*
	* @return the nome of this anagrafica agenti
	*/
	@Override
	public java.lang.String getNome() {
		return _anagraficaAgenti.getNome();
	}

	/**
	* Sets the nome of this anagrafica agenti.
	*
	* @param nome the nome of this anagrafica agenti
	*/
	@Override
	public void setNome(java.lang.String nome) {
		_anagraficaAgenti.setNome(nome);
	}

	/**
	* Returns the codice zona of this anagrafica agenti.
	*
	* @return the codice zona of this anagrafica agenti
	*/
	@Override
	public java.lang.String getCodiceZona() {
		return _anagraficaAgenti.getCodiceZona();
	}

	/**
	* Sets the codice zona of this anagrafica agenti.
	*
	* @param codiceZona the codice zona of this anagrafica agenti
	*/
	@Override
	public void setCodiceZona(java.lang.String codiceZona) {
		_anagraficaAgenti.setCodiceZona(codiceZona);
	}

	/**
	* Returns the codice provv rigo of this anagrafica agenti.
	*
	* @return the codice provv rigo of this anagrafica agenti
	*/
	@Override
	public java.lang.String getCodiceProvvRigo() {
		return _anagraficaAgenti.getCodiceProvvRigo();
	}

	/**
	* Sets the codice provv rigo of this anagrafica agenti.
	*
	* @param codiceProvvRigo the codice provv rigo of this anagrafica agenti
	*/
	@Override
	public void setCodiceProvvRigo(java.lang.String codiceProvvRigo) {
		_anagraficaAgenti.setCodiceProvvRigo(codiceProvvRigo);
	}

	/**
	* Returns the codice provv chiusura of this anagrafica agenti.
	*
	* @return the codice provv chiusura of this anagrafica agenti
	*/
	@Override
	public java.lang.String getCodiceProvvChiusura() {
		return _anagraficaAgenti.getCodiceProvvChiusura();
	}

	/**
	* Sets the codice provv chiusura of this anagrafica agenti.
	*
	* @param codiceProvvChiusura the codice provv chiusura of this anagrafica agenti
	*/
	@Override
	public void setCodiceProvvChiusura(java.lang.String codiceProvvChiusura) {
		_anagraficaAgenti.setCodiceProvvChiusura(codiceProvvChiusura);
	}

	/**
	* Returns the budget of this anagrafica agenti.
	*
	* @return the budget of this anagrafica agenti
	*/
	@Override
	public double getBudget() {
		return _anagraficaAgenti.getBudget();
	}

	/**
	* Sets the budget of this anagrafica agenti.
	*
	* @param budget the budget of this anagrafica agenti
	*/
	@Override
	public void setBudget(double budget) {
		_anagraficaAgenti.setBudget(budget);
	}

	/**
	* Returns the tipo liquidazione of this anagrafica agenti.
	*
	* @return the tipo liquidazione of this anagrafica agenti
	*/
	@Override
	public int getTipoLiquidazione() {
		return _anagraficaAgenti.getTipoLiquidazione();
	}

	/**
	* Sets the tipo liquidazione of this anagrafica agenti.
	*
	* @param tipoLiquidazione the tipo liquidazione of this anagrafica agenti
	*/
	@Override
	public void setTipoLiquidazione(int tipoLiquidazione) {
		_anagraficaAgenti.setTipoLiquidazione(tipoLiquidazione);
	}

	/**
	* Returns the giorni toll of this anagrafica agenti.
	*
	* @return the giorni toll of this anagrafica agenti
	*/
	@Override
	public int getGiorniToll() {
		return _anagraficaAgenti.getGiorniToll();
	}

	/**
	* Sets the giorni toll of this anagrafica agenti.
	*
	* @param giorniToll the giorni toll of this anagrafica agenti
	*/
	@Override
	public void setGiorniToll(int giorniToll) {
		_anagraficaAgenti.setGiorniToll(giorniToll);
	}

	/**
	* Returns the tipo divisa of this anagrafica agenti.
	*
	* @return the tipo divisa of this anagrafica agenti
	*/
	@Override
	public boolean getTipoDivisa() {
		return _anagraficaAgenti.getTipoDivisa();
	}

	/**
	* Returns <code>true</code> if this anagrafica agenti is tipo divisa.
	*
	* @return <code>true</code> if this anagrafica agenti is tipo divisa; <code>false</code> otherwise
	*/
	@Override
	public boolean isTipoDivisa() {
		return _anagraficaAgenti.isTipoDivisa();
	}

	/**
	* Sets whether this anagrafica agenti is tipo divisa.
	*
	* @param tipoDivisa the tipo divisa of this anagrafica agenti
	*/
	@Override
	public void setTipoDivisa(boolean tipoDivisa) {
		_anagraficaAgenti.setTipoDivisa(tipoDivisa);
	}

	@Override
	public boolean isNew() {
		return _anagraficaAgenti.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_anagraficaAgenti.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _anagraficaAgenti.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_anagraficaAgenti.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _anagraficaAgenti.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _anagraficaAgenti.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_anagraficaAgenti.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _anagraficaAgenti.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_anagraficaAgenti.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_anagraficaAgenti.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_anagraficaAgenti.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AnagraficaAgentiWrapper((AnagraficaAgenti)_anagraficaAgenti.clone());
	}

	@Override
	public int compareTo(
		it.bysoftware.ct.model.AnagraficaAgenti anagraficaAgenti) {
		return _anagraficaAgenti.compareTo(anagraficaAgenti);
	}

	@Override
	public int hashCode() {
		return _anagraficaAgenti.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.AnagraficaAgenti> toCacheModel() {
		return _anagraficaAgenti.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.AnagraficaAgenti toEscapedModel() {
		return new AnagraficaAgentiWrapper(_anagraficaAgenti.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.AnagraficaAgenti toUnescapedModel() {
		return new AnagraficaAgentiWrapper(_anagraficaAgenti.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _anagraficaAgenti.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _anagraficaAgenti.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_anagraficaAgenti.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AnagraficaAgentiWrapper)) {
			return false;
		}

		AnagraficaAgentiWrapper anagraficaAgentiWrapper = (AnagraficaAgentiWrapper)obj;

		if (Validator.equals(_anagraficaAgenti,
					anagraficaAgentiWrapper._anagraficaAgenti)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public AnagraficaAgenti getWrappedAnagraficaAgenti() {
		return _anagraficaAgenti;
	}

	@Override
	public AnagraficaAgenti getWrappedModel() {
		return _anagraficaAgenti;
	}

	@Override
	public void resetOriginalValues() {
		_anagraficaAgenti.resetOriginalValues();
	}

	private AnagraficaAgenti _anagraficaAgenti;
}