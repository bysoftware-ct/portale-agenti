/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link WKOrdiniClienti}.
 * </p>
 *
 * @author Mario Torrisi
 * @see WKOrdiniClienti
 * @generated
 */
public class WKOrdiniClientiWrapper implements WKOrdiniClienti,
	ModelWrapper<WKOrdiniClienti> {
	public WKOrdiniClientiWrapper(WKOrdiniClienti wkOrdiniClienti) {
		_wkOrdiniClienti = wkOrdiniClienti;
	}

	@Override
	public Class<?> getModelClass() {
		return WKOrdiniClienti.class;
	}

	@Override
	public String getModelClassName() {
		return WKOrdiniClienti.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("anno", getAnno());
		attributes.put("tipoOrdine", getTipoOrdine());
		attributes.put("numeroOrdine", getNumeroOrdine());
		attributes.put("tipoDocumento", getTipoDocumento());
		attributes.put("idTipoDocumento", getIdTipoDocumento());
		attributes.put("statoOrdine", getStatoOrdine());
		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceCliente", getCodiceCliente());
		attributes.put("codiceEsenzione", getCodiceEsenzione());
		attributes.put("codiceDivisa", getCodiceDivisa());
		attributes.put("valoreCambio", getValoreCambio());
		attributes.put("dataValoreCambio", getDataValoreCambio());
		attributes.put("codicePianoPag", getCodicePianoPag());
		attributes.put("inizioCalcoloPag", getInizioCalcoloPag());
		attributes.put("percentualeScontoMaggiorazione",
			getPercentualeScontoMaggiorazione());
		attributes.put("percentualeScontoProntaCassa",
			getPercentualeScontoProntaCassa());
		attributes.put("dataDocumento", getDataDocumento());
		attributes.put("dataRegistrazione", getDataRegistrazione());
		attributes.put("causaleEstrattoConto", getCausaleEstrattoConto());
		attributes.put("codiceAgente", getCodiceAgente());
		attributes.put("codiceGruppoAgenti", getCodiceGruppoAgenti());
		attributes.put("codiceZona", getCodiceZona());
		attributes.put("codiceDestinatario", getCodiceDestinatario());
		attributes.put("codiceListino", getCodiceListino());
		attributes.put("numeroDecPrezzo", getNumeroDecPrezzo());
		attributes.put("note", getNote());
		attributes.put("inviatoEmail", getInviatoEmail());
		attributes.put("nomePDF", getNomePDF());
		attributes.put("riferimentoOrdine", getRiferimentoOrdine());
		attributes.put("dataConferma", getDataConferma());
		attributes.put("confermaStampata", getConfermaStampata());
		attributes.put("totaleOrdine", getTotaleOrdine());
		attributes.put("libStr1", getLibStr1());
		attributes.put("libStr2", getLibStr2());
		attributes.put("libStr3", getLibStr3());
		attributes.put("libDbl1", getLibDbl1());
		attributes.put("libDbl2", getLibDbl2());
		attributes.put("libDbl3", getLibDbl3());
		attributes.put("libDat1", getLibDat1());
		attributes.put("libDat2", getLibDat2());
		attributes.put("libDat3", getLibDat3());
		attributes.put("libLng1", getLibLng1());
		attributes.put("libLng2", getLibLng2());
		attributes.put("libLng3", getLibLng3());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer anno = (Integer)attributes.get("anno");

		if (anno != null) {
			setAnno(anno);
		}

		Integer tipoOrdine = (Integer)attributes.get("tipoOrdine");

		if (tipoOrdine != null) {
			setTipoOrdine(tipoOrdine);
		}

		Integer numeroOrdine = (Integer)attributes.get("numeroOrdine");

		if (numeroOrdine != null) {
			setNumeroOrdine(numeroOrdine);
		}

		String tipoDocumento = (String)attributes.get("tipoDocumento");

		if (tipoDocumento != null) {
			setTipoDocumento(tipoDocumento);
		}

		Integer idTipoDocumento = (Integer)attributes.get("idTipoDocumento");

		if (idTipoDocumento != null) {
			setIdTipoDocumento(idTipoDocumento);
		}

		Boolean statoOrdine = (Boolean)attributes.get("statoOrdine");

		if (statoOrdine != null) {
			setStatoOrdine(statoOrdine);
		}

		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceCliente = (String)attributes.get("codiceCliente");

		if (codiceCliente != null) {
			setCodiceCliente(codiceCliente);
		}

		String codiceEsenzione = (String)attributes.get("codiceEsenzione");

		if (codiceEsenzione != null) {
			setCodiceEsenzione(codiceEsenzione);
		}

		String codiceDivisa = (String)attributes.get("codiceDivisa");

		if (codiceDivisa != null) {
			setCodiceDivisa(codiceDivisa);
		}

		Double valoreCambio = (Double)attributes.get("valoreCambio");

		if (valoreCambio != null) {
			setValoreCambio(valoreCambio);
		}

		Date dataValoreCambio = (Date)attributes.get("dataValoreCambio");

		if (dataValoreCambio != null) {
			setDataValoreCambio(dataValoreCambio);
		}

		String codicePianoPag = (String)attributes.get("codicePianoPag");

		if (codicePianoPag != null) {
			setCodicePianoPag(codicePianoPag);
		}

		Date inizioCalcoloPag = (Date)attributes.get("inizioCalcoloPag");

		if (inizioCalcoloPag != null) {
			setInizioCalcoloPag(inizioCalcoloPag);
		}

		Double percentualeScontoMaggiorazione = (Double)attributes.get(
				"percentualeScontoMaggiorazione");

		if (percentualeScontoMaggiorazione != null) {
			setPercentualeScontoMaggiorazione(percentualeScontoMaggiorazione);
		}

		Double percentualeScontoProntaCassa = (Double)attributes.get(
				"percentualeScontoProntaCassa");

		if (percentualeScontoProntaCassa != null) {
			setPercentualeScontoProntaCassa(percentualeScontoProntaCassa);
		}

		Date dataDocumento = (Date)attributes.get("dataDocumento");

		if (dataDocumento != null) {
			setDataDocumento(dataDocumento);
		}

		Date dataRegistrazione = (Date)attributes.get("dataRegistrazione");

		if (dataRegistrazione != null) {
			setDataRegistrazione(dataRegistrazione);
		}

		String causaleEstrattoConto = (String)attributes.get(
				"causaleEstrattoConto");

		if (causaleEstrattoConto != null) {
			setCausaleEstrattoConto(causaleEstrattoConto);
		}

		String codiceAgente = (String)attributes.get("codiceAgente");

		if (codiceAgente != null) {
			setCodiceAgente(codiceAgente);
		}

		String codiceGruppoAgenti = (String)attributes.get("codiceGruppoAgenti");

		if (codiceGruppoAgenti != null) {
			setCodiceGruppoAgenti(codiceGruppoAgenti);
		}

		String codiceZona = (String)attributes.get("codiceZona");

		if (codiceZona != null) {
			setCodiceZona(codiceZona);
		}

		String codiceDestinatario = (String)attributes.get("codiceDestinatario");

		if (codiceDestinatario != null) {
			setCodiceDestinatario(codiceDestinatario);
		}

		String codiceListino = (String)attributes.get("codiceListino");

		if (codiceListino != null) {
			setCodiceListino(codiceListino);
		}

		Integer numeroDecPrezzo = (Integer)attributes.get("numeroDecPrezzo");

		if (numeroDecPrezzo != null) {
			setNumeroDecPrezzo(numeroDecPrezzo);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}

		Boolean inviatoEmail = (Boolean)attributes.get("inviatoEmail");

		if (inviatoEmail != null) {
			setInviatoEmail(inviatoEmail);
		}

		String nomePDF = (String)attributes.get("nomePDF");

		if (nomePDF != null) {
			setNomePDF(nomePDF);
		}

		String riferimentoOrdine = (String)attributes.get("riferimentoOrdine");

		if (riferimentoOrdine != null) {
			setRiferimentoOrdine(riferimentoOrdine);
		}

		Date dataConferma = (Date)attributes.get("dataConferma");

		if (dataConferma != null) {
			setDataConferma(dataConferma);
		}

		Boolean confermaStampata = (Boolean)attributes.get("confermaStampata");

		if (confermaStampata != null) {
			setConfermaStampata(confermaStampata);
		}

		Double totaleOrdine = (Double)attributes.get("totaleOrdine");

		if (totaleOrdine != null) {
			setTotaleOrdine(totaleOrdine);
		}

		String libStr1 = (String)attributes.get("libStr1");

		if (libStr1 != null) {
			setLibStr1(libStr1);
		}

		String libStr2 = (String)attributes.get("libStr2");

		if (libStr2 != null) {
			setLibStr2(libStr2);
		}

		String libStr3 = (String)attributes.get("libStr3");

		if (libStr3 != null) {
			setLibStr3(libStr3);
		}

		Double libDbl1 = (Double)attributes.get("libDbl1");

		if (libDbl1 != null) {
			setLibDbl1(libDbl1);
		}

		Double libDbl2 = (Double)attributes.get("libDbl2");

		if (libDbl2 != null) {
			setLibDbl2(libDbl2);
		}

		Double libDbl3 = (Double)attributes.get("libDbl3");

		if (libDbl3 != null) {
			setLibDbl3(libDbl3);
		}

		Date libDat1 = (Date)attributes.get("libDat1");

		if (libDat1 != null) {
			setLibDat1(libDat1);
		}

		Date libDat2 = (Date)attributes.get("libDat2");

		if (libDat2 != null) {
			setLibDat2(libDat2);
		}

		Date libDat3 = (Date)attributes.get("libDat3");

		if (libDat3 != null) {
			setLibDat3(libDat3);
		}

		Long libLng1 = (Long)attributes.get("libLng1");

		if (libLng1 != null) {
			setLibLng1(libLng1);
		}

		Long libLng2 = (Long)attributes.get("libLng2");

		if (libLng2 != null) {
			setLibLng2(libLng2);
		}

		Long libLng3 = (Long)attributes.get("libLng3");

		if (libLng3 != null) {
			setLibLng3(libLng3);
		}
	}

	/**
	* Returns the primary key of this w k ordini clienti.
	*
	* @return the primary key of this w k ordini clienti
	*/
	@Override
	public it.bysoftware.ct.service.persistence.WKOrdiniClientiPK getPrimaryKey() {
		return _wkOrdiniClienti.getPrimaryKey();
	}

	/**
	* Sets the primary key of this w k ordini clienti.
	*
	* @param primaryKey the primary key of this w k ordini clienti
	*/
	@Override
	public void setPrimaryKey(
		it.bysoftware.ct.service.persistence.WKOrdiniClientiPK primaryKey) {
		_wkOrdiniClienti.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the anno of this w k ordini clienti.
	*
	* @return the anno of this w k ordini clienti
	*/
	@Override
	public int getAnno() {
		return _wkOrdiniClienti.getAnno();
	}

	/**
	* Sets the anno of this w k ordini clienti.
	*
	* @param anno the anno of this w k ordini clienti
	*/
	@Override
	public void setAnno(int anno) {
		_wkOrdiniClienti.setAnno(anno);
	}

	/**
	* Returns the tipo ordine of this w k ordini clienti.
	*
	* @return the tipo ordine of this w k ordini clienti
	*/
	@Override
	public int getTipoOrdine() {
		return _wkOrdiniClienti.getTipoOrdine();
	}

	/**
	* Sets the tipo ordine of this w k ordini clienti.
	*
	* @param tipoOrdine the tipo ordine of this w k ordini clienti
	*/
	@Override
	public void setTipoOrdine(int tipoOrdine) {
		_wkOrdiniClienti.setTipoOrdine(tipoOrdine);
	}

	/**
	* Returns the numero ordine of this w k ordini clienti.
	*
	* @return the numero ordine of this w k ordini clienti
	*/
	@Override
	public int getNumeroOrdine() {
		return _wkOrdiniClienti.getNumeroOrdine();
	}

	/**
	* Sets the numero ordine of this w k ordini clienti.
	*
	* @param numeroOrdine the numero ordine of this w k ordini clienti
	*/
	@Override
	public void setNumeroOrdine(int numeroOrdine) {
		_wkOrdiniClienti.setNumeroOrdine(numeroOrdine);
	}

	/**
	* Returns the tipo documento of this w k ordini clienti.
	*
	* @return the tipo documento of this w k ordini clienti
	*/
	@Override
	public java.lang.String getTipoDocumento() {
		return _wkOrdiniClienti.getTipoDocumento();
	}

	/**
	* Sets the tipo documento of this w k ordini clienti.
	*
	* @param tipoDocumento the tipo documento of this w k ordini clienti
	*/
	@Override
	public void setTipoDocumento(java.lang.String tipoDocumento) {
		_wkOrdiniClienti.setTipoDocumento(tipoDocumento);
	}

	/**
	* Returns the id tipo documento of this w k ordini clienti.
	*
	* @return the id tipo documento of this w k ordini clienti
	*/
	@Override
	public int getIdTipoDocumento() {
		return _wkOrdiniClienti.getIdTipoDocumento();
	}

	/**
	* Sets the id tipo documento of this w k ordini clienti.
	*
	* @param idTipoDocumento the id tipo documento of this w k ordini clienti
	*/
	@Override
	public void setIdTipoDocumento(int idTipoDocumento) {
		_wkOrdiniClienti.setIdTipoDocumento(idTipoDocumento);
	}

	/**
	* Returns the stato ordine of this w k ordini clienti.
	*
	* @return the stato ordine of this w k ordini clienti
	*/
	@Override
	public boolean getStatoOrdine() {
		return _wkOrdiniClienti.getStatoOrdine();
	}

	/**
	* Returns <code>true</code> if this w k ordini clienti is stato ordine.
	*
	* @return <code>true</code> if this w k ordini clienti is stato ordine; <code>false</code> otherwise
	*/
	@Override
	public boolean isStatoOrdine() {
		return _wkOrdiniClienti.isStatoOrdine();
	}

	/**
	* Sets whether this w k ordini clienti is stato ordine.
	*
	* @param statoOrdine the stato ordine of this w k ordini clienti
	*/
	@Override
	public void setStatoOrdine(boolean statoOrdine) {
		_wkOrdiniClienti.setStatoOrdine(statoOrdine);
	}

	/**
	* Returns the tipo soggetto of this w k ordini clienti.
	*
	* @return the tipo soggetto of this w k ordini clienti
	*/
	@Override
	public boolean getTipoSoggetto() {
		return _wkOrdiniClienti.getTipoSoggetto();
	}

	/**
	* Returns <code>true</code> if this w k ordini clienti is tipo soggetto.
	*
	* @return <code>true</code> if this w k ordini clienti is tipo soggetto; <code>false</code> otherwise
	*/
	@Override
	public boolean isTipoSoggetto() {
		return _wkOrdiniClienti.isTipoSoggetto();
	}

	/**
	* Sets whether this w k ordini clienti is tipo soggetto.
	*
	* @param tipoSoggetto the tipo soggetto of this w k ordini clienti
	*/
	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_wkOrdiniClienti.setTipoSoggetto(tipoSoggetto);
	}

	/**
	* Returns the codice cliente of this w k ordini clienti.
	*
	* @return the codice cliente of this w k ordini clienti
	*/
	@Override
	public java.lang.String getCodiceCliente() {
		return _wkOrdiniClienti.getCodiceCliente();
	}

	/**
	* Sets the codice cliente of this w k ordini clienti.
	*
	* @param codiceCliente the codice cliente of this w k ordini clienti
	*/
	@Override
	public void setCodiceCliente(java.lang.String codiceCliente) {
		_wkOrdiniClienti.setCodiceCliente(codiceCliente);
	}

	/**
	* Returns the codice esenzione of this w k ordini clienti.
	*
	* @return the codice esenzione of this w k ordini clienti
	*/
	@Override
	public java.lang.String getCodiceEsenzione() {
		return _wkOrdiniClienti.getCodiceEsenzione();
	}

	/**
	* Sets the codice esenzione of this w k ordini clienti.
	*
	* @param codiceEsenzione the codice esenzione of this w k ordini clienti
	*/
	@Override
	public void setCodiceEsenzione(java.lang.String codiceEsenzione) {
		_wkOrdiniClienti.setCodiceEsenzione(codiceEsenzione);
	}

	/**
	* Returns the codice divisa of this w k ordini clienti.
	*
	* @return the codice divisa of this w k ordini clienti
	*/
	@Override
	public java.lang.String getCodiceDivisa() {
		return _wkOrdiniClienti.getCodiceDivisa();
	}

	/**
	* Sets the codice divisa of this w k ordini clienti.
	*
	* @param codiceDivisa the codice divisa of this w k ordini clienti
	*/
	@Override
	public void setCodiceDivisa(java.lang.String codiceDivisa) {
		_wkOrdiniClienti.setCodiceDivisa(codiceDivisa);
	}

	/**
	* Returns the valore cambio of this w k ordini clienti.
	*
	* @return the valore cambio of this w k ordini clienti
	*/
	@Override
	public double getValoreCambio() {
		return _wkOrdiniClienti.getValoreCambio();
	}

	/**
	* Sets the valore cambio of this w k ordini clienti.
	*
	* @param valoreCambio the valore cambio of this w k ordini clienti
	*/
	@Override
	public void setValoreCambio(double valoreCambio) {
		_wkOrdiniClienti.setValoreCambio(valoreCambio);
	}

	/**
	* Returns the data valore cambio of this w k ordini clienti.
	*
	* @return the data valore cambio of this w k ordini clienti
	*/
	@Override
	public java.util.Date getDataValoreCambio() {
		return _wkOrdiniClienti.getDataValoreCambio();
	}

	/**
	* Sets the data valore cambio of this w k ordini clienti.
	*
	* @param dataValoreCambio the data valore cambio of this w k ordini clienti
	*/
	@Override
	public void setDataValoreCambio(java.util.Date dataValoreCambio) {
		_wkOrdiniClienti.setDataValoreCambio(dataValoreCambio);
	}

	/**
	* Returns the codice piano pag of this w k ordini clienti.
	*
	* @return the codice piano pag of this w k ordini clienti
	*/
	@Override
	public java.lang.String getCodicePianoPag() {
		return _wkOrdiniClienti.getCodicePianoPag();
	}

	/**
	* Sets the codice piano pag of this w k ordini clienti.
	*
	* @param codicePianoPag the codice piano pag of this w k ordini clienti
	*/
	@Override
	public void setCodicePianoPag(java.lang.String codicePianoPag) {
		_wkOrdiniClienti.setCodicePianoPag(codicePianoPag);
	}

	/**
	* Returns the inizio calcolo pag of this w k ordini clienti.
	*
	* @return the inizio calcolo pag of this w k ordini clienti
	*/
	@Override
	public java.util.Date getInizioCalcoloPag() {
		return _wkOrdiniClienti.getInizioCalcoloPag();
	}

	/**
	* Sets the inizio calcolo pag of this w k ordini clienti.
	*
	* @param inizioCalcoloPag the inizio calcolo pag of this w k ordini clienti
	*/
	@Override
	public void setInizioCalcoloPag(java.util.Date inizioCalcoloPag) {
		_wkOrdiniClienti.setInizioCalcoloPag(inizioCalcoloPag);
	}

	/**
	* Returns the percentuale sconto maggiorazione of this w k ordini clienti.
	*
	* @return the percentuale sconto maggiorazione of this w k ordini clienti
	*/
	@Override
	public double getPercentualeScontoMaggiorazione() {
		return _wkOrdiniClienti.getPercentualeScontoMaggiorazione();
	}

	/**
	* Sets the percentuale sconto maggiorazione of this w k ordini clienti.
	*
	* @param percentualeScontoMaggiorazione the percentuale sconto maggiorazione of this w k ordini clienti
	*/
	@Override
	public void setPercentualeScontoMaggiorazione(
		double percentualeScontoMaggiorazione) {
		_wkOrdiniClienti.setPercentualeScontoMaggiorazione(percentualeScontoMaggiorazione);
	}

	/**
	* Returns the percentuale sconto pronta cassa of this w k ordini clienti.
	*
	* @return the percentuale sconto pronta cassa of this w k ordini clienti
	*/
	@Override
	public double getPercentualeScontoProntaCassa() {
		return _wkOrdiniClienti.getPercentualeScontoProntaCassa();
	}

	/**
	* Sets the percentuale sconto pronta cassa of this w k ordini clienti.
	*
	* @param percentualeScontoProntaCassa the percentuale sconto pronta cassa of this w k ordini clienti
	*/
	@Override
	public void setPercentualeScontoProntaCassa(
		double percentualeScontoProntaCassa) {
		_wkOrdiniClienti.setPercentualeScontoProntaCassa(percentualeScontoProntaCassa);
	}

	/**
	* Returns the data documento of this w k ordini clienti.
	*
	* @return the data documento of this w k ordini clienti
	*/
	@Override
	public java.util.Date getDataDocumento() {
		return _wkOrdiniClienti.getDataDocumento();
	}

	/**
	* Sets the data documento of this w k ordini clienti.
	*
	* @param dataDocumento the data documento of this w k ordini clienti
	*/
	@Override
	public void setDataDocumento(java.util.Date dataDocumento) {
		_wkOrdiniClienti.setDataDocumento(dataDocumento);
	}

	/**
	* Returns the data registrazione of this w k ordini clienti.
	*
	* @return the data registrazione of this w k ordini clienti
	*/
	@Override
	public java.util.Date getDataRegistrazione() {
		return _wkOrdiniClienti.getDataRegistrazione();
	}

	/**
	* Sets the data registrazione of this w k ordini clienti.
	*
	* @param dataRegistrazione the data registrazione of this w k ordini clienti
	*/
	@Override
	public void setDataRegistrazione(java.util.Date dataRegistrazione) {
		_wkOrdiniClienti.setDataRegistrazione(dataRegistrazione);
	}

	/**
	* Returns the causale estratto conto of this w k ordini clienti.
	*
	* @return the causale estratto conto of this w k ordini clienti
	*/
	@Override
	public java.lang.String getCausaleEstrattoConto() {
		return _wkOrdiniClienti.getCausaleEstrattoConto();
	}

	/**
	* Sets the causale estratto conto of this w k ordini clienti.
	*
	* @param causaleEstrattoConto the causale estratto conto of this w k ordini clienti
	*/
	@Override
	public void setCausaleEstrattoConto(java.lang.String causaleEstrattoConto) {
		_wkOrdiniClienti.setCausaleEstrattoConto(causaleEstrattoConto);
	}

	/**
	* Returns the codice agente of this w k ordini clienti.
	*
	* @return the codice agente of this w k ordini clienti
	*/
	@Override
	public java.lang.String getCodiceAgente() {
		return _wkOrdiniClienti.getCodiceAgente();
	}

	/**
	* Sets the codice agente of this w k ordini clienti.
	*
	* @param codiceAgente the codice agente of this w k ordini clienti
	*/
	@Override
	public void setCodiceAgente(java.lang.String codiceAgente) {
		_wkOrdiniClienti.setCodiceAgente(codiceAgente);
	}

	/**
	* Returns the codice gruppo agenti of this w k ordini clienti.
	*
	* @return the codice gruppo agenti of this w k ordini clienti
	*/
	@Override
	public java.lang.String getCodiceGruppoAgenti() {
		return _wkOrdiniClienti.getCodiceGruppoAgenti();
	}

	/**
	* Sets the codice gruppo agenti of this w k ordini clienti.
	*
	* @param codiceGruppoAgenti the codice gruppo agenti of this w k ordini clienti
	*/
	@Override
	public void setCodiceGruppoAgenti(java.lang.String codiceGruppoAgenti) {
		_wkOrdiniClienti.setCodiceGruppoAgenti(codiceGruppoAgenti);
	}

	/**
	* Returns the codice zona of this w k ordini clienti.
	*
	* @return the codice zona of this w k ordini clienti
	*/
	@Override
	public java.lang.String getCodiceZona() {
		return _wkOrdiniClienti.getCodiceZona();
	}

	/**
	* Sets the codice zona of this w k ordini clienti.
	*
	* @param codiceZona the codice zona of this w k ordini clienti
	*/
	@Override
	public void setCodiceZona(java.lang.String codiceZona) {
		_wkOrdiniClienti.setCodiceZona(codiceZona);
	}

	/**
	* Returns the codice destinatario of this w k ordini clienti.
	*
	* @return the codice destinatario of this w k ordini clienti
	*/
	@Override
	public java.lang.String getCodiceDestinatario() {
		return _wkOrdiniClienti.getCodiceDestinatario();
	}

	/**
	* Sets the codice destinatario of this w k ordini clienti.
	*
	* @param codiceDestinatario the codice destinatario of this w k ordini clienti
	*/
	@Override
	public void setCodiceDestinatario(java.lang.String codiceDestinatario) {
		_wkOrdiniClienti.setCodiceDestinatario(codiceDestinatario);
	}

	/**
	* Returns the codice listino of this w k ordini clienti.
	*
	* @return the codice listino of this w k ordini clienti
	*/
	@Override
	public java.lang.String getCodiceListino() {
		return _wkOrdiniClienti.getCodiceListino();
	}

	/**
	* Sets the codice listino of this w k ordini clienti.
	*
	* @param codiceListino the codice listino of this w k ordini clienti
	*/
	@Override
	public void setCodiceListino(java.lang.String codiceListino) {
		_wkOrdiniClienti.setCodiceListino(codiceListino);
	}

	/**
	* Returns the numero dec prezzo of this w k ordini clienti.
	*
	* @return the numero dec prezzo of this w k ordini clienti
	*/
	@Override
	public int getNumeroDecPrezzo() {
		return _wkOrdiniClienti.getNumeroDecPrezzo();
	}

	/**
	* Sets the numero dec prezzo of this w k ordini clienti.
	*
	* @param numeroDecPrezzo the numero dec prezzo of this w k ordini clienti
	*/
	@Override
	public void setNumeroDecPrezzo(int numeroDecPrezzo) {
		_wkOrdiniClienti.setNumeroDecPrezzo(numeroDecPrezzo);
	}

	/**
	* Returns the note of this w k ordini clienti.
	*
	* @return the note of this w k ordini clienti
	*/
	@Override
	public java.lang.String getNote() {
		return _wkOrdiniClienti.getNote();
	}

	/**
	* Sets the note of this w k ordini clienti.
	*
	* @param note the note of this w k ordini clienti
	*/
	@Override
	public void setNote(java.lang.String note) {
		_wkOrdiniClienti.setNote(note);
	}

	/**
	* Returns the inviato email of this w k ordini clienti.
	*
	* @return the inviato email of this w k ordini clienti
	*/
	@Override
	public boolean getInviatoEmail() {
		return _wkOrdiniClienti.getInviatoEmail();
	}

	/**
	* Returns <code>true</code> if this w k ordini clienti is inviato email.
	*
	* @return <code>true</code> if this w k ordini clienti is inviato email; <code>false</code> otherwise
	*/
	@Override
	public boolean isInviatoEmail() {
		return _wkOrdiniClienti.isInviatoEmail();
	}

	/**
	* Sets whether this w k ordini clienti is inviato email.
	*
	* @param inviatoEmail the inviato email of this w k ordini clienti
	*/
	@Override
	public void setInviatoEmail(boolean inviatoEmail) {
		_wkOrdiniClienti.setInviatoEmail(inviatoEmail);
	}

	/**
	* Returns the nome p d f of this w k ordini clienti.
	*
	* @return the nome p d f of this w k ordini clienti
	*/
	@Override
	public java.lang.String getNomePDF() {
		return _wkOrdiniClienti.getNomePDF();
	}

	/**
	* Sets the nome p d f of this w k ordini clienti.
	*
	* @param nomePDF the nome p d f of this w k ordini clienti
	*/
	@Override
	public void setNomePDF(java.lang.String nomePDF) {
		_wkOrdiniClienti.setNomePDF(nomePDF);
	}

	/**
	* Returns the riferimento ordine of this w k ordini clienti.
	*
	* @return the riferimento ordine of this w k ordini clienti
	*/
	@Override
	public java.lang.String getRiferimentoOrdine() {
		return _wkOrdiniClienti.getRiferimentoOrdine();
	}

	/**
	* Sets the riferimento ordine of this w k ordini clienti.
	*
	* @param riferimentoOrdine the riferimento ordine of this w k ordini clienti
	*/
	@Override
	public void setRiferimentoOrdine(java.lang.String riferimentoOrdine) {
		_wkOrdiniClienti.setRiferimentoOrdine(riferimentoOrdine);
	}

	/**
	* Returns the data conferma of this w k ordini clienti.
	*
	* @return the data conferma of this w k ordini clienti
	*/
	@Override
	public java.util.Date getDataConferma() {
		return _wkOrdiniClienti.getDataConferma();
	}

	/**
	* Sets the data conferma of this w k ordini clienti.
	*
	* @param dataConferma the data conferma of this w k ordini clienti
	*/
	@Override
	public void setDataConferma(java.util.Date dataConferma) {
		_wkOrdiniClienti.setDataConferma(dataConferma);
	}

	/**
	* Returns the conferma stampata of this w k ordini clienti.
	*
	* @return the conferma stampata of this w k ordini clienti
	*/
	@Override
	public boolean getConfermaStampata() {
		return _wkOrdiniClienti.getConfermaStampata();
	}

	/**
	* Returns <code>true</code> if this w k ordini clienti is conferma stampata.
	*
	* @return <code>true</code> if this w k ordini clienti is conferma stampata; <code>false</code> otherwise
	*/
	@Override
	public boolean isConfermaStampata() {
		return _wkOrdiniClienti.isConfermaStampata();
	}

	/**
	* Sets whether this w k ordini clienti is conferma stampata.
	*
	* @param confermaStampata the conferma stampata of this w k ordini clienti
	*/
	@Override
	public void setConfermaStampata(boolean confermaStampata) {
		_wkOrdiniClienti.setConfermaStampata(confermaStampata);
	}

	/**
	* Returns the totale ordine of this w k ordini clienti.
	*
	* @return the totale ordine of this w k ordini clienti
	*/
	@Override
	public double getTotaleOrdine() {
		return _wkOrdiniClienti.getTotaleOrdine();
	}

	/**
	* Sets the totale ordine of this w k ordini clienti.
	*
	* @param totaleOrdine the totale ordine of this w k ordini clienti
	*/
	@Override
	public void setTotaleOrdine(double totaleOrdine) {
		_wkOrdiniClienti.setTotaleOrdine(totaleOrdine);
	}

	/**
	* Returns the lib str1 of this w k ordini clienti.
	*
	* @return the lib str1 of this w k ordini clienti
	*/
	@Override
	public java.lang.String getLibStr1() {
		return _wkOrdiniClienti.getLibStr1();
	}

	/**
	* Sets the lib str1 of this w k ordini clienti.
	*
	* @param libStr1 the lib str1 of this w k ordini clienti
	*/
	@Override
	public void setLibStr1(java.lang.String libStr1) {
		_wkOrdiniClienti.setLibStr1(libStr1);
	}

	/**
	* Returns the lib str2 of this w k ordini clienti.
	*
	* @return the lib str2 of this w k ordini clienti
	*/
	@Override
	public java.lang.String getLibStr2() {
		return _wkOrdiniClienti.getLibStr2();
	}

	/**
	* Sets the lib str2 of this w k ordini clienti.
	*
	* @param libStr2 the lib str2 of this w k ordini clienti
	*/
	@Override
	public void setLibStr2(java.lang.String libStr2) {
		_wkOrdiniClienti.setLibStr2(libStr2);
	}

	/**
	* Returns the lib str3 of this w k ordini clienti.
	*
	* @return the lib str3 of this w k ordini clienti
	*/
	@Override
	public java.lang.String getLibStr3() {
		return _wkOrdiniClienti.getLibStr3();
	}

	/**
	* Sets the lib str3 of this w k ordini clienti.
	*
	* @param libStr3 the lib str3 of this w k ordini clienti
	*/
	@Override
	public void setLibStr3(java.lang.String libStr3) {
		_wkOrdiniClienti.setLibStr3(libStr3);
	}

	/**
	* Returns the lib dbl1 of this w k ordini clienti.
	*
	* @return the lib dbl1 of this w k ordini clienti
	*/
	@Override
	public double getLibDbl1() {
		return _wkOrdiniClienti.getLibDbl1();
	}

	/**
	* Sets the lib dbl1 of this w k ordini clienti.
	*
	* @param libDbl1 the lib dbl1 of this w k ordini clienti
	*/
	@Override
	public void setLibDbl1(double libDbl1) {
		_wkOrdiniClienti.setLibDbl1(libDbl1);
	}

	/**
	* Returns the lib dbl2 of this w k ordini clienti.
	*
	* @return the lib dbl2 of this w k ordini clienti
	*/
	@Override
	public double getLibDbl2() {
		return _wkOrdiniClienti.getLibDbl2();
	}

	/**
	* Sets the lib dbl2 of this w k ordini clienti.
	*
	* @param libDbl2 the lib dbl2 of this w k ordini clienti
	*/
	@Override
	public void setLibDbl2(double libDbl2) {
		_wkOrdiniClienti.setLibDbl2(libDbl2);
	}

	/**
	* Returns the lib dbl3 of this w k ordini clienti.
	*
	* @return the lib dbl3 of this w k ordini clienti
	*/
	@Override
	public double getLibDbl3() {
		return _wkOrdiniClienti.getLibDbl3();
	}

	/**
	* Sets the lib dbl3 of this w k ordini clienti.
	*
	* @param libDbl3 the lib dbl3 of this w k ordini clienti
	*/
	@Override
	public void setLibDbl3(double libDbl3) {
		_wkOrdiniClienti.setLibDbl3(libDbl3);
	}

	/**
	* Returns the lib dat1 of this w k ordini clienti.
	*
	* @return the lib dat1 of this w k ordini clienti
	*/
	@Override
	public java.util.Date getLibDat1() {
		return _wkOrdiniClienti.getLibDat1();
	}

	/**
	* Sets the lib dat1 of this w k ordini clienti.
	*
	* @param libDat1 the lib dat1 of this w k ordini clienti
	*/
	@Override
	public void setLibDat1(java.util.Date libDat1) {
		_wkOrdiniClienti.setLibDat1(libDat1);
	}

	/**
	* Returns the lib dat2 of this w k ordini clienti.
	*
	* @return the lib dat2 of this w k ordini clienti
	*/
	@Override
	public java.util.Date getLibDat2() {
		return _wkOrdiniClienti.getLibDat2();
	}

	/**
	* Sets the lib dat2 of this w k ordini clienti.
	*
	* @param libDat2 the lib dat2 of this w k ordini clienti
	*/
	@Override
	public void setLibDat2(java.util.Date libDat2) {
		_wkOrdiniClienti.setLibDat2(libDat2);
	}

	/**
	* Returns the lib dat3 of this w k ordini clienti.
	*
	* @return the lib dat3 of this w k ordini clienti
	*/
	@Override
	public java.util.Date getLibDat3() {
		return _wkOrdiniClienti.getLibDat3();
	}

	/**
	* Sets the lib dat3 of this w k ordini clienti.
	*
	* @param libDat3 the lib dat3 of this w k ordini clienti
	*/
	@Override
	public void setLibDat3(java.util.Date libDat3) {
		_wkOrdiniClienti.setLibDat3(libDat3);
	}

	/**
	* Returns the lib lng1 of this w k ordini clienti.
	*
	* @return the lib lng1 of this w k ordini clienti
	*/
	@Override
	public long getLibLng1() {
		return _wkOrdiniClienti.getLibLng1();
	}

	/**
	* Sets the lib lng1 of this w k ordini clienti.
	*
	* @param libLng1 the lib lng1 of this w k ordini clienti
	*/
	@Override
	public void setLibLng1(long libLng1) {
		_wkOrdiniClienti.setLibLng1(libLng1);
	}

	/**
	* Returns the lib lng2 of this w k ordini clienti.
	*
	* @return the lib lng2 of this w k ordini clienti
	*/
	@Override
	public long getLibLng2() {
		return _wkOrdiniClienti.getLibLng2();
	}

	/**
	* Sets the lib lng2 of this w k ordini clienti.
	*
	* @param libLng2 the lib lng2 of this w k ordini clienti
	*/
	@Override
	public void setLibLng2(long libLng2) {
		_wkOrdiniClienti.setLibLng2(libLng2);
	}

	/**
	* Returns the lib lng3 of this w k ordini clienti.
	*
	* @return the lib lng3 of this w k ordini clienti
	*/
	@Override
	public long getLibLng3() {
		return _wkOrdiniClienti.getLibLng3();
	}

	/**
	* Sets the lib lng3 of this w k ordini clienti.
	*
	* @param libLng3 the lib lng3 of this w k ordini clienti
	*/
	@Override
	public void setLibLng3(long libLng3) {
		_wkOrdiniClienti.setLibLng3(libLng3);
	}

	@Override
	public boolean isNew() {
		return _wkOrdiniClienti.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_wkOrdiniClienti.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _wkOrdiniClienti.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_wkOrdiniClienti.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _wkOrdiniClienti.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _wkOrdiniClienti.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_wkOrdiniClienti.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _wkOrdiniClienti.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_wkOrdiniClienti.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_wkOrdiniClienti.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_wkOrdiniClienti.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new WKOrdiniClientiWrapper((WKOrdiniClienti)_wkOrdiniClienti.clone());
	}

	@Override
	public int compareTo(it.bysoftware.ct.model.WKOrdiniClienti wkOrdiniClienti) {
		return _wkOrdiniClienti.compareTo(wkOrdiniClienti);
	}

	@Override
	public int hashCode() {
		return _wkOrdiniClienti.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.WKOrdiniClienti> toCacheModel() {
		return _wkOrdiniClienti.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.WKOrdiniClienti toEscapedModel() {
		return new WKOrdiniClientiWrapper(_wkOrdiniClienti.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.WKOrdiniClienti toUnescapedModel() {
		return new WKOrdiniClientiWrapper(_wkOrdiniClienti.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _wkOrdiniClienti.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _wkOrdiniClienti.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_wkOrdiniClienti.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof WKOrdiniClientiWrapper)) {
			return false;
		}

		WKOrdiniClientiWrapper wkOrdiniClientiWrapper = (WKOrdiniClientiWrapper)obj;

		if (Validator.equals(_wkOrdiniClienti,
					wkOrdiniClientiWrapper._wkOrdiniClienti)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public WKOrdiniClienti getWrappedWKOrdiniClienti() {
		return _wkOrdiniClienti;
	}

	@Override
	public WKOrdiniClienti getWrappedModel() {
		return _wkOrdiniClienti;
	}

	@Override
	public void resetOriginalValues() {
		_wkOrdiniClienti.resetOriginalValues();
	}

	private WKOrdiniClienti _wkOrdiniClienti;
}