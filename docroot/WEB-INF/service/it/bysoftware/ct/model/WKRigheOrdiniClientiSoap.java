/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.WKRigheOrdiniClientiPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.WKRigheOrdiniClientiServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.WKRigheOrdiniClientiServiceSoap
 * @generated
 */
public class WKRigheOrdiniClientiSoap implements Serializable {
	public static WKRigheOrdiniClientiSoap toSoapModel(
		WKRigheOrdiniClienti model) {
		WKRigheOrdiniClientiSoap soapModel = new WKRigheOrdiniClientiSoap();

		soapModel.setAnno(model.getAnno());
		soapModel.setTipoOrdine(model.getTipoOrdine());
		soapModel.setNumeroOrdine(model.getNumeroOrdine());
		soapModel.setNumeroRigo(model.getNumeroRigo());
		soapModel.setStatoRigo(model.getStatoRigo());
		soapModel.setTipoRigo(model.getTipoRigo());
		soapModel.setCodiceDepositoMov(model.getCodiceDepositoMov());
		soapModel.setCodiceArticolo(model.getCodiceArticolo());
		soapModel.setCodiceVariante(model.getCodiceVariante());
		soapModel.setDescrizione(model.getDescrizione());
		soapModel.setCodiceUnitMis(model.getCodiceUnitMis());
		soapModel.setDecimaliQuant(model.getDecimaliQuant());
		soapModel.setQuantita1(model.getQuantita1());
		soapModel.setQuantita2(model.getQuantita2());
		soapModel.setQuantita3(model.getQuantita3());
		soapModel.setQuantita(model.getQuantita());
		soapModel.setCodiceUnitMis2(model.getCodiceUnitMis2());
		soapModel.setQuantitaUnitMis2(model.getQuantitaUnitMis2());
		soapModel.setDecimaliPrezzo(model.getDecimaliPrezzo());
		soapModel.setPrezzo(model.getPrezzo());
		soapModel.setImportoLordo(model.getImportoLordo());
		soapModel.setSconto1(model.getSconto1());
		soapModel.setSconto2(model.getSconto2());
		soapModel.setSconto3(model.getSconto3());
		soapModel.setImportoNetto(model.getImportoNetto());
		soapModel.setImporto(model.getImporto());
		soapModel.setCodiceIVAFatturazione(model.getCodiceIVAFatturazione());
		soapModel.setCodiceCliente(model.getCodiceCliente());
		soapModel.setRiferimentoOrdineCliente(model.getRiferimentoOrdineCliente());
		soapModel.setDataOridine(model.getDataOridine());
		soapModel.setStatoEvasione(model.getStatoEvasione());
		soapModel.setDataPrevistaConsegna(model.getDataPrevistaConsegna());
		soapModel.setDataRegistrazioneOrdine(model.getDataRegistrazioneOrdine());
		soapModel.setLibStr1(model.getLibStr1());
		soapModel.setLibStr2(model.getLibStr2());
		soapModel.setLibStr3(model.getLibStr3());
		soapModel.setLibDbl1(model.getLibDbl1());
		soapModel.setLibDbl2(model.getLibDbl2());
		soapModel.setLibDbl3(model.getLibDbl3());
		soapModel.setLibDat1(model.getLibDat1());
		soapModel.setLibDat2(model.getLibDat2());
		soapModel.setLibDat3(model.getLibDat3());
		soapModel.setLibLng1(model.getLibLng1());
		soapModel.setLibLng2(model.getLibLng2());
		soapModel.setLibLng3(model.getLibLng3());

		return soapModel;
	}

	public static WKRigheOrdiniClientiSoap[] toSoapModels(
		WKRigheOrdiniClienti[] models) {
		WKRigheOrdiniClientiSoap[] soapModels = new WKRigheOrdiniClientiSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static WKRigheOrdiniClientiSoap[][] toSoapModels(
		WKRigheOrdiniClienti[][] models) {
		WKRigheOrdiniClientiSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new WKRigheOrdiniClientiSoap[models.length][models[0].length];
		}
		else {
			soapModels = new WKRigheOrdiniClientiSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static WKRigheOrdiniClientiSoap[] toSoapModels(
		List<WKRigheOrdiniClienti> models) {
		List<WKRigheOrdiniClientiSoap> soapModels = new ArrayList<WKRigheOrdiniClientiSoap>(models.size());

		for (WKRigheOrdiniClienti model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new WKRigheOrdiniClientiSoap[soapModels.size()]);
	}

	public WKRigheOrdiniClientiSoap() {
	}

	public WKRigheOrdiniClientiPK getPrimaryKey() {
		return new WKRigheOrdiniClientiPK(_anno, _tipoOrdine, _numeroOrdine,
			_numeroRigo);
	}

	public void setPrimaryKey(WKRigheOrdiniClientiPK pk) {
		setAnno(pk.anno);
		setTipoOrdine(pk.tipoOrdine);
		setNumeroOrdine(pk.numeroOrdine);
		setNumeroRigo(pk.numeroRigo);
	}

	public int getAnno() {
		return _anno;
	}

	public void setAnno(int anno) {
		_anno = anno;
	}

	public int getTipoOrdine() {
		return _tipoOrdine;
	}

	public void setTipoOrdine(int tipoOrdine) {
		_tipoOrdine = tipoOrdine;
	}

	public int getNumeroOrdine() {
		return _numeroOrdine;
	}

	public void setNumeroOrdine(int numeroOrdine) {
		_numeroOrdine = numeroOrdine;
	}

	public int getNumeroRigo() {
		return _numeroRigo;
	}

	public void setNumeroRigo(int numeroRigo) {
		_numeroRigo = numeroRigo;
	}

	public boolean getStatoRigo() {
		return _statoRigo;
	}

	public boolean isStatoRigo() {
		return _statoRigo;
	}

	public void setStatoRigo(boolean statoRigo) {
		_statoRigo = statoRigo;
	}

	public int getTipoRigo() {
		return _tipoRigo;
	}

	public void setTipoRigo(int tipoRigo) {
		_tipoRigo = tipoRigo;
	}

	public String getCodiceDepositoMov() {
		return _codiceDepositoMov;
	}

	public void setCodiceDepositoMov(String codiceDepositoMov) {
		_codiceDepositoMov = codiceDepositoMov;
	}

	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;
	}

	public String getCodiceVariante() {
		return _codiceVariante;
	}

	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;
	}

	public String getDescrizione() {
		return _descrizione;
	}

	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;
	}

	public String getCodiceUnitMis() {
		return _codiceUnitMis;
	}

	public void setCodiceUnitMis(String codiceUnitMis) {
		_codiceUnitMis = codiceUnitMis;
	}

	public int getDecimaliQuant() {
		return _decimaliQuant;
	}

	public void setDecimaliQuant(int decimaliQuant) {
		_decimaliQuant = decimaliQuant;
	}

	public double getQuantita1() {
		return _quantita1;
	}

	public void setQuantita1(double quantita1) {
		_quantita1 = quantita1;
	}

	public double getQuantita2() {
		return _quantita2;
	}

	public void setQuantita2(double quantita2) {
		_quantita2 = quantita2;
	}

	public double getQuantita3() {
		return _quantita3;
	}

	public void setQuantita3(double quantita3) {
		_quantita3 = quantita3;
	}

	public double getQuantita() {
		return _quantita;
	}

	public void setQuantita(double quantita) {
		_quantita = quantita;
	}

	public String getCodiceUnitMis2() {
		return _codiceUnitMis2;
	}

	public void setCodiceUnitMis2(String codiceUnitMis2) {
		_codiceUnitMis2 = codiceUnitMis2;
	}

	public double getQuantitaUnitMis2() {
		return _quantitaUnitMis2;
	}

	public void setQuantitaUnitMis2(double quantitaUnitMis2) {
		_quantitaUnitMis2 = quantitaUnitMis2;
	}

	public int getDecimaliPrezzo() {
		return _decimaliPrezzo;
	}

	public void setDecimaliPrezzo(int decimaliPrezzo) {
		_decimaliPrezzo = decimaliPrezzo;
	}

	public double getPrezzo() {
		return _prezzo;
	}

	public void setPrezzo(double prezzo) {
		_prezzo = prezzo;
	}

	public double getImportoLordo() {
		return _importoLordo;
	}

	public void setImportoLordo(double importoLordo) {
		_importoLordo = importoLordo;
	}

	public double getSconto1() {
		return _sconto1;
	}

	public void setSconto1(double sconto1) {
		_sconto1 = sconto1;
	}

	public double getSconto2() {
		return _sconto2;
	}

	public void setSconto2(double sconto2) {
		_sconto2 = sconto2;
	}

	public double getSconto3() {
		return _sconto3;
	}

	public void setSconto3(double sconto3) {
		_sconto3 = sconto3;
	}

	public double getImportoNetto() {
		return _importoNetto;
	}

	public void setImportoNetto(double importoNetto) {
		_importoNetto = importoNetto;
	}

	public double getImporto() {
		return _importo;
	}

	public void setImporto(double importo) {
		_importo = importo;
	}

	public String getCodiceIVAFatturazione() {
		return _codiceIVAFatturazione;
	}

	public void setCodiceIVAFatturazione(String codiceIVAFatturazione) {
		_codiceIVAFatturazione = codiceIVAFatturazione;
	}

	public String getCodiceCliente() {
		return _codiceCliente;
	}

	public void setCodiceCliente(String codiceCliente) {
		_codiceCliente = codiceCliente;
	}

	public String getRiferimentoOrdineCliente() {
		return _riferimentoOrdineCliente;
	}

	public void setRiferimentoOrdineCliente(String riferimentoOrdineCliente) {
		_riferimentoOrdineCliente = riferimentoOrdineCliente;
	}

	public Date getDataOridine() {
		return _dataOridine;
	}

	public void setDataOridine(Date dataOridine) {
		_dataOridine = dataOridine;
	}

	public boolean getStatoEvasione() {
		return _statoEvasione;
	}

	public boolean isStatoEvasione() {
		return _statoEvasione;
	}

	public void setStatoEvasione(boolean statoEvasione) {
		_statoEvasione = statoEvasione;
	}

	public Date getDataPrevistaConsegna() {
		return _dataPrevistaConsegna;
	}

	public void setDataPrevistaConsegna(Date dataPrevistaConsegna) {
		_dataPrevistaConsegna = dataPrevistaConsegna;
	}

	public Date getDataRegistrazioneOrdine() {
		return _dataRegistrazioneOrdine;
	}

	public void setDataRegistrazioneOrdine(Date dataRegistrazioneOrdine) {
		_dataRegistrazioneOrdine = dataRegistrazioneOrdine;
	}

	public String getLibStr1() {
		return _libStr1;
	}

	public void setLibStr1(String libStr1) {
		_libStr1 = libStr1;
	}

	public String getLibStr2() {
		return _libStr2;
	}

	public void setLibStr2(String libStr2) {
		_libStr2 = libStr2;
	}

	public String getLibStr3() {
		return _libStr3;
	}

	public void setLibStr3(String libStr3) {
		_libStr3 = libStr3;
	}

	public double getLibDbl1() {
		return _libDbl1;
	}

	public void setLibDbl1(double libDbl1) {
		_libDbl1 = libDbl1;
	}

	public double getLibDbl2() {
		return _libDbl2;
	}

	public void setLibDbl2(double libDbl2) {
		_libDbl2 = libDbl2;
	}

	public double getLibDbl3() {
		return _libDbl3;
	}

	public void setLibDbl3(double libDbl3) {
		_libDbl3 = libDbl3;
	}

	public Date getLibDat1() {
		return _libDat1;
	}

	public void setLibDat1(Date libDat1) {
		_libDat1 = libDat1;
	}

	public Date getLibDat2() {
		return _libDat2;
	}

	public void setLibDat2(Date libDat2) {
		_libDat2 = libDat2;
	}

	public Date getLibDat3() {
		return _libDat3;
	}

	public void setLibDat3(Date libDat3) {
		_libDat3 = libDat3;
	}

	public long getLibLng1() {
		return _libLng1;
	}

	public void setLibLng1(long libLng1) {
		_libLng1 = libLng1;
	}

	public long getLibLng2() {
		return _libLng2;
	}

	public void setLibLng2(long libLng2) {
		_libLng2 = libLng2;
	}

	public long getLibLng3() {
		return _libLng3;
	}

	public void setLibLng3(long libLng3) {
		_libLng3 = libLng3;
	}

	private int _anno;
	private int _tipoOrdine;
	private int _numeroOrdine;
	private int _numeroRigo;
	private boolean _statoRigo;
	private int _tipoRigo;
	private String _codiceDepositoMov;
	private String _codiceArticolo;
	private String _codiceVariante;
	private String _descrizione;
	private String _codiceUnitMis;
	private int _decimaliQuant;
	private double _quantita1;
	private double _quantita2;
	private double _quantita3;
	private double _quantita;
	private String _codiceUnitMis2;
	private double _quantitaUnitMis2;
	private int _decimaliPrezzo;
	private double _prezzo;
	private double _importoLordo;
	private double _sconto1;
	private double _sconto2;
	private double _sconto3;
	private double _importoNetto;
	private double _importo;
	private String _codiceIVAFatturazione;
	private String _codiceCliente;
	private String _riferimentoOrdineCliente;
	private Date _dataOridine;
	private boolean _statoEvasione;
	private Date _dataPrevistaConsegna;
	private Date _dataRegistrazioneOrdine;
	private String _libStr1;
	private String _libStr2;
	private String _libStr3;
	private double _libDbl1;
	private double _libDbl2;
	private double _libDbl3;
	private Date _libDat1;
	private Date _libDat2;
	private Date _libDat3;
	private long _libLng1;
	private long _libLng2;
	private long _libLng3;
}