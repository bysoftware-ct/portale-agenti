/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link RigheFattureVedita}.
 * </p>
 *
 * @author Mario Torrisi
 * @see RigheFattureVedita
 * @generated
 */
public class RigheFattureVeditaWrapper implements RigheFattureVedita,
	ModelWrapper<RigheFattureVedita> {
	public RigheFattureVeditaWrapper(RigheFattureVedita righeFattureVedita) {
		_righeFattureVedita = righeFattureVedita;
	}

	@Override
	public Class<?> getModelClass() {
		return RigheFattureVedita.class;
	}

	@Override
	public String getModelClassName() {
		return RigheFattureVedita.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("anno", getAnno());
		attributes.put("codiceAttivita", getCodiceAttivita());
		attributes.put("codiceCentro", getCodiceCentro());
		attributes.put("numeroProtocollo", getNumeroProtocollo());
		attributes.put("numeroRigo", getNumeroRigo());
		attributes.put("statoRigo", getStatoRigo());
		attributes.put("annoBolla", getAnnoBolla());
		attributes.put("tipoIdentificativoBolla", getTipoIdentificativoBolla());
		attributes.put("tipoBolla", getTipoBolla());
		attributes.put("codiceDeposito", getCodiceDeposito());
		attributes.put("numeroBollettario", getNumeroBollettario());
		attributes.put("numeroBolla", getNumeroBolla());
		attributes.put("dataBollaEvasa", getDataBollaEvasa());
		attributes.put("rigoDDT", getRigoDDT());
		attributes.put("tipoRigoDDT", getTipoRigoDDT());
		attributes.put("codiceCausaleMagazzino", getCodiceCausaleMagazzino());
		attributes.put("codiceDepositoMagazzino", getCodiceDepositoMagazzino());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("codiceTestoDescrizioni", getCodiceTestoDescrizioni());
		attributes.put("descrizione", getDescrizione());
		attributes.put("unitaMisura", getUnitaMisura());
		attributes.put("numeroDecimalliQuant", getNumeroDecimalliQuant());
		attributes.put("quantita1", getQuantita1());
		attributes.put("quantita2", getQuantita2());
		attributes.put("quantita3", getQuantita3());
		attributes.put("quantita", getQuantita());
		attributes.put("unitaMisura2", getUnitaMisura2());
		attributes.put("quantitaUnitMisSec", getQuantitaUnitMisSec());
		attributes.put("numeroDecimalliPrezzo", getNumeroDecimalliPrezzo());
		attributes.put("prezzo", getPrezzo());
		attributes.put("prezzoIVA", getPrezzoIVA());
		attributes.put("importoLordo", getImportoLordo());
		attributes.put("importoLordoIVA", getImportoLordoIVA());
		attributes.put("sconto1", getSconto1());
		attributes.put("sconto2", getSconto2());
		attributes.put("sconto3", getSconto3());
		attributes.put("importoNettoRigo", getImportoNettoRigo());
		attributes.put("importoNettoRigoIVA", getImportoNettoRigoIVA());
		attributes.put("importoNetto", getImportoNetto());
		attributes.put("importoNettoIVA", getImportoNettoIVA());
		attributes.put("importoCONAIAddebitatoInt",
			getImportoCONAIAddebitatoInt());
		attributes.put("importoCONAIAddebitatoOrig",
			getImportoCONAIAddebitatoOrig());
		attributes.put("codiceIVAFatturazione", getCodiceIVAFatturazione());
		attributes.put("nomenclaturaCombinata", getNomenclaturaCombinata());
		attributes.put("testStampaDisBase", getTestStampaDisBase());
		attributes.put("tipoOrdineEvaso", getTipoOrdineEvaso());
		attributes.put("annoOrdineEvaso", getAnnoOrdineEvaso());
		attributes.put("numeroOrdineEvaso", getNumeroOrdineEvaso());
		attributes.put("numeroRigoOrdineEvaso", getNumeroRigoOrdineEvaso());
		attributes.put("tipoEvasione", getTipoEvasione());
		attributes.put("descrizioneRiferimento", getDescrizioneRiferimento());
		attributes.put("numeroPrimaNotaMagazzino", getNumeroPrimaNotaMagazzino());
		attributes.put("numeroMovimentoMagazzino", getNumeroMovimentoMagazzino());
		attributes.put("tipoEsplosione", getTipoEsplosione());
		attributes.put("livelloMassimoEsplosione", getLivelloMassimoEsplosione());
		attributes.put("libStr1", getLibStr1());
		attributes.put("libStr2", getLibStr2());
		attributes.put("libStr3", getLibStr3());
		attributes.put("libDbl1", getLibDbl1());
		attributes.put("libDbl2", getLibDbl2());
		attributes.put("libDbl3", getLibDbl3());
		attributes.put("libLng1", getLibLng1());
		attributes.put("libLng2", getLibLng2());
		attributes.put("libLng3", getLibLng3());
		attributes.put("libDat1", getLibDat1());
		attributes.put("libDat2", getLibDat2());
		attributes.put("libDat3", getLibDat3());
		attributes.put("testFatturazioneAuto", getTestFatturazioneAuto());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer anno = (Integer)attributes.get("anno");

		if (anno != null) {
			setAnno(anno);
		}

		String codiceAttivita = (String)attributes.get("codiceAttivita");

		if (codiceAttivita != null) {
			setCodiceAttivita(codiceAttivita);
		}

		String codiceCentro = (String)attributes.get("codiceCentro");

		if (codiceCentro != null) {
			setCodiceCentro(codiceCentro);
		}

		Integer numeroProtocollo = (Integer)attributes.get("numeroProtocollo");

		if (numeroProtocollo != null) {
			setNumeroProtocollo(numeroProtocollo);
		}

		Integer numeroRigo = (Integer)attributes.get("numeroRigo");

		if (numeroRigo != null) {
			setNumeroRigo(numeroRigo);
		}

		Boolean statoRigo = (Boolean)attributes.get("statoRigo");

		if (statoRigo != null) {
			setStatoRigo(statoRigo);
		}

		Integer annoBolla = (Integer)attributes.get("annoBolla");

		if (annoBolla != null) {
			setAnnoBolla(annoBolla);
		}

		Integer tipoIdentificativoBolla = (Integer)attributes.get(
				"tipoIdentificativoBolla");

		if (tipoIdentificativoBolla != null) {
			setTipoIdentificativoBolla(tipoIdentificativoBolla);
		}

		String tipoBolla = (String)attributes.get("tipoBolla");

		if (tipoBolla != null) {
			setTipoBolla(tipoBolla);
		}

		String codiceDeposito = (String)attributes.get("codiceDeposito");

		if (codiceDeposito != null) {
			setCodiceDeposito(codiceDeposito);
		}

		Integer numeroBollettario = (Integer)attributes.get("numeroBollettario");

		if (numeroBollettario != null) {
			setNumeroBollettario(numeroBollettario);
		}

		Integer numeroBolla = (Integer)attributes.get("numeroBolla");

		if (numeroBolla != null) {
			setNumeroBolla(numeroBolla);
		}

		Date dataBollaEvasa = (Date)attributes.get("dataBollaEvasa");

		if (dataBollaEvasa != null) {
			setDataBollaEvasa(dataBollaEvasa);
		}

		Integer rigoDDT = (Integer)attributes.get("rigoDDT");

		if (rigoDDT != null) {
			setRigoDDT(rigoDDT);
		}

		Integer tipoRigoDDT = (Integer)attributes.get("tipoRigoDDT");

		if (tipoRigoDDT != null) {
			setTipoRigoDDT(tipoRigoDDT);
		}

		String codiceCausaleMagazzino = (String)attributes.get(
				"codiceCausaleMagazzino");

		if (codiceCausaleMagazzino != null) {
			setCodiceCausaleMagazzino(codiceCausaleMagazzino);
		}

		String codiceDepositoMagazzino = (String)attributes.get(
				"codiceDepositoMagazzino");

		if (codiceDepositoMagazzino != null) {
			setCodiceDepositoMagazzino(codiceDepositoMagazzino);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		String codiceTestoDescrizioni = (String)attributes.get(
				"codiceTestoDescrizioni");

		if (codiceTestoDescrizioni != null) {
			setCodiceTestoDescrizioni(codiceTestoDescrizioni);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		String unitaMisura = (String)attributes.get("unitaMisura");

		if (unitaMisura != null) {
			setUnitaMisura(unitaMisura);
		}

		Integer numeroDecimalliQuant = (Integer)attributes.get(
				"numeroDecimalliQuant");

		if (numeroDecimalliQuant != null) {
			setNumeroDecimalliQuant(numeroDecimalliQuant);
		}

		Double quantita1 = (Double)attributes.get("quantita1");

		if (quantita1 != null) {
			setQuantita1(quantita1);
		}

		Double quantita2 = (Double)attributes.get("quantita2");

		if (quantita2 != null) {
			setQuantita2(quantita2);
		}

		Double quantita3 = (Double)attributes.get("quantita3");

		if (quantita3 != null) {
			setQuantita3(quantita3);
		}

		Double quantita = (Double)attributes.get("quantita");

		if (quantita != null) {
			setQuantita(quantita);
		}

		String unitaMisura2 = (String)attributes.get("unitaMisura2");

		if (unitaMisura2 != null) {
			setUnitaMisura2(unitaMisura2);
		}

		Double quantitaUnitMisSec = (Double)attributes.get("quantitaUnitMisSec");

		if (quantitaUnitMisSec != null) {
			setQuantitaUnitMisSec(quantitaUnitMisSec);
		}

		Integer numeroDecimalliPrezzo = (Integer)attributes.get(
				"numeroDecimalliPrezzo");

		if (numeroDecimalliPrezzo != null) {
			setNumeroDecimalliPrezzo(numeroDecimalliPrezzo);
		}

		Double prezzo = (Double)attributes.get("prezzo");

		if (prezzo != null) {
			setPrezzo(prezzo);
		}

		Double prezzoIVA = (Double)attributes.get("prezzoIVA");

		if (prezzoIVA != null) {
			setPrezzoIVA(prezzoIVA);
		}

		Double importoLordo = (Double)attributes.get("importoLordo");

		if (importoLordo != null) {
			setImportoLordo(importoLordo);
		}

		Double importoLordoIVA = (Double)attributes.get("importoLordoIVA");

		if (importoLordoIVA != null) {
			setImportoLordoIVA(importoLordoIVA);
		}

		Double sconto1 = (Double)attributes.get("sconto1");

		if (sconto1 != null) {
			setSconto1(sconto1);
		}

		Double sconto2 = (Double)attributes.get("sconto2");

		if (sconto2 != null) {
			setSconto2(sconto2);
		}

		Double sconto3 = (Double)attributes.get("sconto3");

		if (sconto3 != null) {
			setSconto3(sconto3);
		}

		Double importoNettoRigo = (Double)attributes.get("importoNettoRigo");

		if (importoNettoRigo != null) {
			setImportoNettoRigo(importoNettoRigo);
		}

		Double importoNettoRigoIVA = (Double)attributes.get(
				"importoNettoRigoIVA");

		if (importoNettoRigoIVA != null) {
			setImportoNettoRigoIVA(importoNettoRigoIVA);
		}

		Double importoNetto = (Double)attributes.get("importoNetto");

		if (importoNetto != null) {
			setImportoNetto(importoNetto);
		}

		Double importoNettoIVA = (Double)attributes.get("importoNettoIVA");

		if (importoNettoIVA != null) {
			setImportoNettoIVA(importoNettoIVA);
		}

		Double importoCONAIAddebitatoInt = (Double)attributes.get(
				"importoCONAIAddebitatoInt");

		if (importoCONAIAddebitatoInt != null) {
			setImportoCONAIAddebitatoInt(importoCONAIAddebitatoInt);
		}

		Double importoCONAIAddebitatoOrig = (Double)attributes.get(
				"importoCONAIAddebitatoOrig");

		if (importoCONAIAddebitatoOrig != null) {
			setImportoCONAIAddebitatoOrig(importoCONAIAddebitatoOrig);
		}

		String codiceIVAFatturazione = (String)attributes.get(
				"codiceIVAFatturazione");

		if (codiceIVAFatturazione != null) {
			setCodiceIVAFatturazione(codiceIVAFatturazione);
		}

		Integer nomenclaturaCombinata = (Integer)attributes.get(
				"nomenclaturaCombinata");

		if (nomenclaturaCombinata != null) {
			setNomenclaturaCombinata(nomenclaturaCombinata);
		}

		Boolean testStampaDisBase = (Boolean)attributes.get("testStampaDisBase");

		if (testStampaDisBase != null) {
			setTestStampaDisBase(testStampaDisBase);
		}

		Integer tipoOrdineEvaso = (Integer)attributes.get("tipoOrdineEvaso");

		if (tipoOrdineEvaso != null) {
			setTipoOrdineEvaso(tipoOrdineEvaso);
		}

		Integer annoOrdineEvaso = (Integer)attributes.get("annoOrdineEvaso");

		if (annoOrdineEvaso != null) {
			setAnnoOrdineEvaso(annoOrdineEvaso);
		}

		Integer numeroOrdineEvaso = (Integer)attributes.get("numeroOrdineEvaso");

		if (numeroOrdineEvaso != null) {
			setNumeroOrdineEvaso(numeroOrdineEvaso);
		}

		Integer numeroRigoOrdineEvaso = (Integer)attributes.get(
				"numeroRigoOrdineEvaso");

		if (numeroRigoOrdineEvaso != null) {
			setNumeroRigoOrdineEvaso(numeroRigoOrdineEvaso);
		}

		String tipoEvasione = (String)attributes.get("tipoEvasione");

		if (tipoEvasione != null) {
			setTipoEvasione(tipoEvasione);
		}

		String descrizioneRiferimento = (String)attributes.get(
				"descrizioneRiferimento");

		if (descrizioneRiferimento != null) {
			setDescrizioneRiferimento(descrizioneRiferimento);
		}

		Integer numeroPrimaNotaMagazzino = (Integer)attributes.get(
				"numeroPrimaNotaMagazzino");

		if (numeroPrimaNotaMagazzino != null) {
			setNumeroPrimaNotaMagazzino(numeroPrimaNotaMagazzino);
		}

		Integer numeroMovimentoMagazzino = (Integer)attributes.get(
				"numeroMovimentoMagazzino");

		if (numeroMovimentoMagazzino != null) {
			setNumeroMovimentoMagazzino(numeroMovimentoMagazzino);
		}

		Integer tipoEsplosione = (Integer)attributes.get("tipoEsplosione");

		if (tipoEsplosione != null) {
			setTipoEsplosione(tipoEsplosione);
		}

		Integer livelloMassimoEsplosione = (Integer)attributes.get(
				"livelloMassimoEsplosione");

		if (livelloMassimoEsplosione != null) {
			setLivelloMassimoEsplosione(livelloMassimoEsplosione);
		}

		String libStr1 = (String)attributes.get("libStr1");

		if (libStr1 != null) {
			setLibStr1(libStr1);
		}

		String libStr2 = (String)attributes.get("libStr2");

		if (libStr2 != null) {
			setLibStr2(libStr2);
		}

		String libStr3 = (String)attributes.get("libStr3");

		if (libStr3 != null) {
			setLibStr3(libStr3);
		}

		Double libDbl1 = (Double)attributes.get("libDbl1");

		if (libDbl1 != null) {
			setLibDbl1(libDbl1);
		}

		Double libDbl2 = (Double)attributes.get("libDbl2");

		if (libDbl2 != null) {
			setLibDbl2(libDbl2);
		}

		Double libDbl3 = (Double)attributes.get("libDbl3");

		if (libDbl3 != null) {
			setLibDbl3(libDbl3);
		}

		Long libLng1 = (Long)attributes.get("libLng1");

		if (libLng1 != null) {
			setLibLng1(libLng1);
		}

		Long libLng2 = (Long)attributes.get("libLng2");

		if (libLng2 != null) {
			setLibLng2(libLng2);
		}

		Long libLng3 = (Long)attributes.get("libLng3");

		if (libLng3 != null) {
			setLibLng3(libLng3);
		}

		Date libDat1 = (Date)attributes.get("libDat1");

		if (libDat1 != null) {
			setLibDat1(libDat1);
		}

		Date libDat2 = (Date)attributes.get("libDat2");

		if (libDat2 != null) {
			setLibDat2(libDat2);
		}

		Date libDat3 = (Date)attributes.get("libDat3");

		if (libDat3 != null) {
			setLibDat3(libDat3);
		}

		Boolean testFatturazioneAuto = (Boolean)attributes.get(
				"testFatturazioneAuto");

		if (testFatturazioneAuto != null) {
			setTestFatturazioneAuto(testFatturazioneAuto);
		}
	}

	/**
	* Returns the primary key of this righe fatture vedita.
	*
	* @return the primary key of this righe fatture vedita
	*/
	@Override
	public it.bysoftware.ct.service.persistence.RigheFattureVeditaPK getPrimaryKey() {
		return _righeFattureVedita.getPrimaryKey();
	}

	/**
	* Sets the primary key of this righe fatture vedita.
	*
	* @param primaryKey the primary key of this righe fatture vedita
	*/
	@Override
	public void setPrimaryKey(
		it.bysoftware.ct.service.persistence.RigheFattureVeditaPK primaryKey) {
		_righeFattureVedita.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the anno of this righe fatture vedita.
	*
	* @return the anno of this righe fatture vedita
	*/
	@Override
	public int getAnno() {
		return _righeFattureVedita.getAnno();
	}

	/**
	* Sets the anno of this righe fatture vedita.
	*
	* @param anno the anno of this righe fatture vedita
	*/
	@Override
	public void setAnno(int anno) {
		_righeFattureVedita.setAnno(anno);
	}

	/**
	* Returns the codice attivita of this righe fatture vedita.
	*
	* @return the codice attivita of this righe fatture vedita
	*/
	@Override
	public java.lang.String getCodiceAttivita() {
		return _righeFattureVedita.getCodiceAttivita();
	}

	/**
	* Sets the codice attivita of this righe fatture vedita.
	*
	* @param codiceAttivita the codice attivita of this righe fatture vedita
	*/
	@Override
	public void setCodiceAttivita(java.lang.String codiceAttivita) {
		_righeFattureVedita.setCodiceAttivita(codiceAttivita);
	}

	/**
	* Returns the codice centro of this righe fatture vedita.
	*
	* @return the codice centro of this righe fatture vedita
	*/
	@Override
	public java.lang.String getCodiceCentro() {
		return _righeFattureVedita.getCodiceCentro();
	}

	/**
	* Sets the codice centro of this righe fatture vedita.
	*
	* @param codiceCentro the codice centro of this righe fatture vedita
	*/
	@Override
	public void setCodiceCentro(java.lang.String codiceCentro) {
		_righeFattureVedita.setCodiceCentro(codiceCentro);
	}

	/**
	* Returns the numero protocollo of this righe fatture vedita.
	*
	* @return the numero protocollo of this righe fatture vedita
	*/
	@Override
	public int getNumeroProtocollo() {
		return _righeFattureVedita.getNumeroProtocollo();
	}

	/**
	* Sets the numero protocollo of this righe fatture vedita.
	*
	* @param numeroProtocollo the numero protocollo of this righe fatture vedita
	*/
	@Override
	public void setNumeroProtocollo(int numeroProtocollo) {
		_righeFattureVedita.setNumeroProtocollo(numeroProtocollo);
	}

	/**
	* Returns the numero rigo of this righe fatture vedita.
	*
	* @return the numero rigo of this righe fatture vedita
	*/
	@Override
	public int getNumeroRigo() {
		return _righeFattureVedita.getNumeroRigo();
	}

	/**
	* Sets the numero rigo of this righe fatture vedita.
	*
	* @param numeroRigo the numero rigo of this righe fatture vedita
	*/
	@Override
	public void setNumeroRigo(int numeroRigo) {
		_righeFattureVedita.setNumeroRigo(numeroRigo);
	}

	/**
	* Returns the stato rigo of this righe fatture vedita.
	*
	* @return the stato rigo of this righe fatture vedita
	*/
	@Override
	public boolean getStatoRigo() {
		return _righeFattureVedita.getStatoRigo();
	}

	/**
	* Returns <code>true</code> if this righe fatture vedita is stato rigo.
	*
	* @return <code>true</code> if this righe fatture vedita is stato rigo; <code>false</code> otherwise
	*/
	@Override
	public boolean isStatoRigo() {
		return _righeFattureVedita.isStatoRigo();
	}

	/**
	* Sets whether this righe fatture vedita is stato rigo.
	*
	* @param statoRigo the stato rigo of this righe fatture vedita
	*/
	@Override
	public void setStatoRigo(boolean statoRigo) {
		_righeFattureVedita.setStatoRigo(statoRigo);
	}

	/**
	* Returns the anno bolla of this righe fatture vedita.
	*
	* @return the anno bolla of this righe fatture vedita
	*/
	@Override
	public int getAnnoBolla() {
		return _righeFattureVedita.getAnnoBolla();
	}

	/**
	* Sets the anno bolla of this righe fatture vedita.
	*
	* @param annoBolla the anno bolla of this righe fatture vedita
	*/
	@Override
	public void setAnnoBolla(int annoBolla) {
		_righeFattureVedita.setAnnoBolla(annoBolla);
	}

	/**
	* Returns the tipo identificativo bolla of this righe fatture vedita.
	*
	* @return the tipo identificativo bolla of this righe fatture vedita
	*/
	@Override
	public int getTipoIdentificativoBolla() {
		return _righeFattureVedita.getTipoIdentificativoBolla();
	}

	/**
	* Sets the tipo identificativo bolla of this righe fatture vedita.
	*
	* @param tipoIdentificativoBolla the tipo identificativo bolla of this righe fatture vedita
	*/
	@Override
	public void setTipoIdentificativoBolla(int tipoIdentificativoBolla) {
		_righeFattureVedita.setTipoIdentificativoBolla(tipoIdentificativoBolla);
	}

	/**
	* Returns the tipo bolla of this righe fatture vedita.
	*
	* @return the tipo bolla of this righe fatture vedita
	*/
	@Override
	public java.lang.String getTipoBolla() {
		return _righeFattureVedita.getTipoBolla();
	}

	/**
	* Sets the tipo bolla of this righe fatture vedita.
	*
	* @param tipoBolla the tipo bolla of this righe fatture vedita
	*/
	@Override
	public void setTipoBolla(java.lang.String tipoBolla) {
		_righeFattureVedita.setTipoBolla(tipoBolla);
	}

	/**
	* Returns the codice deposito of this righe fatture vedita.
	*
	* @return the codice deposito of this righe fatture vedita
	*/
	@Override
	public java.lang.String getCodiceDeposito() {
		return _righeFattureVedita.getCodiceDeposito();
	}

	/**
	* Sets the codice deposito of this righe fatture vedita.
	*
	* @param codiceDeposito the codice deposito of this righe fatture vedita
	*/
	@Override
	public void setCodiceDeposito(java.lang.String codiceDeposito) {
		_righeFattureVedita.setCodiceDeposito(codiceDeposito);
	}

	/**
	* Returns the numero bollettario of this righe fatture vedita.
	*
	* @return the numero bollettario of this righe fatture vedita
	*/
	@Override
	public int getNumeroBollettario() {
		return _righeFattureVedita.getNumeroBollettario();
	}

	/**
	* Sets the numero bollettario of this righe fatture vedita.
	*
	* @param numeroBollettario the numero bollettario of this righe fatture vedita
	*/
	@Override
	public void setNumeroBollettario(int numeroBollettario) {
		_righeFattureVedita.setNumeroBollettario(numeroBollettario);
	}

	/**
	* Returns the numero bolla of this righe fatture vedita.
	*
	* @return the numero bolla of this righe fatture vedita
	*/
	@Override
	public int getNumeroBolla() {
		return _righeFattureVedita.getNumeroBolla();
	}

	/**
	* Sets the numero bolla of this righe fatture vedita.
	*
	* @param numeroBolla the numero bolla of this righe fatture vedita
	*/
	@Override
	public void setNumeroBolla(int numeroBolla) {
		_righeFattureVedita.setNumeroBolla(numeroBolla);
	}

	/**
	* Returns the data bolla evasa of this righe fatture vedita.
	*
	* @return the data bolla evasa of this righe fatture vedita
	*/
	@Override
	public java.util.Date getDataBollaEvasa() {
		return _righeFattureVedita.getDataBollaEvasa();
	}

	/**
	* Sets the data bolla evasa of this righe fatture vedita.
	*
	* @param dataBollaEvasa the data bolla evasa of this righe fatture vedita
	*/
	@Override
	public void setDataBollaEvasa(java.util.Date dataBollaEvasa) {
		_righeFattureVedita.setDataBollaEvasa(dataBollaEvasa);
	}

	/**
	* Returns the rigo d d t of this righe fatture vedita.
	*
	* @return the rigo d d t of this righe fatture vedita
	*/
	@Override
	public int getRigoDDT() {
		return _righeFattureVedita.getRigoDDT();
	}

	/**
	* Sets the rigo d d t of this righe fatture vedita.
	*
	* @param rigoDDT the rigo d d t of this righe fatture vedita
	*/
	@Override
	public void setRigoDDT(int rigoDDT) {
		_righeFattureVedita.setRigoDDT(rigoDDT);
	}

	/**
	* Returns the tipo rigo d d t of this righe fatture vedita.
	*
	* @return the tipo rigo d d t of this righe fatture vedita
	*/
	@Override
	public int getTipoRigoDDT() {
		return _righeFattureVedita.getTipoRigoDDT();
	}

	/**
	* Sets the tipo rigo d d t of this righe fatture vedita.
	*
	* @param tipoRigoDDT the tipo rigo d d t of this righe fatture vedita
	*/
	@Override
	public void setTipoRigoDDT(int tipoRigoDDT) {
		_righeFattureVedita.setTipoRigoDDT(tipoRigoDDT);
	}

	/**
	* Returns the codice causale magazzino of this righe fatture vedita.
	*
	* @return the codice causale magazzino of this righe fatture vedita
	*/
	@Override
	public java.lang.String getCodiceCausaleMagazzino() {
		return _righeFattureVedita.getCodiceCausaleMagazzino();
	}

	/**
	* Sets the codice causale magazzino of this righe fatture vedita.
	*
	* @param codiceCausaleMagazzino the codice causale magazzino of this righe fatture vedita
	*/
	@Override
	public void setCodiceCausaleMagazzino(
		java.lang.String codiceCausaleMagazzino) {
		_righeFattureVedita.setCodiceCausaleMagazzino(codiceCausaleMagazzino);
	}

	/**
	* Returns the codice deposito magazzino of this righe fatture vedita.
	*
	* @return the codice deposito magazzino of this righe fatture vedita
	*/
	@Override
	public java.lang.String getCodiceDepositoMagazzino() {
		return _righeFattureVedita.getCodiceDepositoMagazzino();
	}

	/**
	* Sets the codice deposito magazzino of this righe fatture vedita.
	*
	* @param codiceDepositoMagazzino the codice deposito magazzino of this righe fatture vedita
	*/
	@Override
	public void setCodiceDepositoMagazzino(
		java.lang.String codiceDepositoMagazzino) {
		_righeFattureVedita.setCodiceDepositoMagazzino(codiceDepositoMagazzino);
	}

	/**
	* Returns the codice articolo of this righe fatture vedita.
	*
	* @return the codice articolo of this righe fatture vedita
	*/
	@Override
	public java.lang.String getCodiceArticolo() {
		return _righeFattureVedita.getCodiceArticolo();
	}

	/**
	* Sets the codice articolo of this righe fatture vedita.
	*
	* @param codiceArticolo the codice articolo of this righe fatture vedita
	*/
	@Override
	public void setCodiceArticolo(java.lang.String codiceArticolo) {
		_righeFattureVedita.setCodiceArticolo(codiceArticolo);
	}

	/**
	* Returns the codice variante of this righe fatture vedita.
	*
	* @return the codice variante of this righe fatture vedita
	*/
	@Override
	public java.lang.String getCodiceVariante() {
		return _righeFattureVedita.getCodiceVariante();
	}

	/**
	* Sets the codice variante of this righe fatture vedita.
	*
	* @param codiceVariante the codice variante of this righe fatture vedita
	*/
	@Override
	public void setCodiceVariante(java.lang.String codiceVariante) {
		_righeFattureVedita.setCodiceVariante(codiceVariante);
	}

	/**
	* Returns the codice testo descrizioni of this righe fatture vedita.
	*
	* @return the codice testo descrizioni of this righe fatture vedita
	*/
	@Override
	public java.lang.String getCodiceTestoDescrizioni() {
		return _righeFattureVedita.getCodiceTestoDescrizioni();
	}

	/**
	* Sets the codice testo descrizioni of this righe fatture vedita.
	*
	* @param codiceTestoDescrizioni the codice testo descrizioni of this righe fatture vedita
	*/
	@Override
	public void setCodiceTestoDescrizioni(
		java.lang.String codiceTestoDescrizioni) {
		_righeFattureVedita.setCodiceTestoDescrizioni(codiceTestoDescrizioni);
	}

	/**
	* Returns the descrizione of this righe fatture vedita.
	*
	* @return the descrizione of this righe fatture vedita
	*/
	@Override
	public java.lang.String getDescrizione() {
		return _righeFattureVedita.getDescrizione();
	}

	/**
	* Sets the descrizione of this righe fatture vedita.
	*
	* @param descrizione the descrizione of this righe fatture vedita
	*/
	@Override
	public void setDescrizione(java.lang.String descrizione) {
		_righeFattureVedita.setDescrizione(descrizione);
	}

	/**
	* Returns the unita misura of this righe fatture vedita.
	*
	* @return the unita misura of this righe fatture vedita
	*/
	@Override
	public java.lang.String getUnitaMisura() {
		return _righeFattureVedita.getUnitaMisura();
	}

	/**
	* Sets the unita misura of this righe fatture vedita.
	*
	* @param unitaMisura the unita misura of this righe fatture vedita
	*/
	@Override
	public void setUnitaMisura(java.lang.String unitaMisura) {
		_righeFattureVedita.setUnitaMisura(unitaMisura);
	}

	/**
	* Returns the numero decimalli quant of this righe fatture vedita.
	*
	* @return the numero decimalli quant of this righe fatture vedita
	*/
	@Override
	public int getNumeroDecimalliQuant() {
		return _righeFattureVedita.getNumeroDecimalliQuant();
	}

	/**
	* Sets the numero decimalli quant of this righe fatture vedita.
	*
	* @param numeroDecimalliQuant the numero decimalli quant of this righe fatture vedita
	*/
	@Override
	public void setNumeroDecimalliQuant(int numeroDecimalliQuant) {
		_righeFattureVedita.setNumeroDecimalliQuant(numeroDecimalliQuant);
	}

	/**
	* Returns the quantita1 of this righe fatture vedita.
	*
	* @return the quantita1 of this righe fatture vedita
	*/
	@Override
	public double getQuantita1() {
		return _righeFattureVedita.getQuantita1();
	}

	/**
	* Sets the quantita1 of this righe fatture vedita.
	*
	* @param quantita1 the quantita1 of this righe fatture vedita
	*/
	@Override
	public void setQuantita1(double quantita1) {
		_righeFattureVedita.setQuantita1(quantita1);
	}

	/**
	* Returns the quantita2 of this righe fatture vedita.
	*
	* @return the quantita2 of this righe fatture vedita
	*/
	@Override
	public double getQuantita2() {
		return _righeFattureVedita.getQuantita2();
	}

	/**
	* Sets the quantita2 of this righe fatture vedita.
	*
	* @param quantita2 the quantita2 of this righe fatture vedita
	*/
	@Override
	public void setQuantita2(double quantita2) {
		_righeFattureVedita.setQuantita2(quantita2);
	}

	/**
	* Returns the quantita3 of this righe fatture vedita.
	*
	* @return the quantita3 of this righe fatture vedita
	*/
	@Override
	public double getQuantita3() {
		return _righeFattureVedita.getQuantita3();
	}

	/**
	* Sets the quantita3 of this righe fatture vedita.
	*
	* @param quantita3 the quantita3 of this righe fatture vedita
	*/
	@Override
	public void setQuantita3(double quantita3) {
		_righeFattureVedita.setQuantita3(quantita3);
	}

	/**
	* Returns the quantita of this righe fatture vedita.
	*
	* @return the quantita of this righe fatture vedita
	*/
	@Override
	public double getQuantita() {
		return _righeFattureVedita.getQuantita();
	}

	/**
	* Sets the quantita of this righe fatture vedita.
	*
	* @param quantita the quantita of this righe fatture vedita
	*/
	@Override
	public void setQuantita(double quantita) {
		_righeFattureVedita.setQuantita(quantita);
	}

	/**
	* Returns the unita misura2 of this righe fatture vedita.
	*
	* @return the unita misura2 of this righe fatture vedita
	*/
	@Override
	public java.lang.String getUnitaMisura2() {
		return _righeFattureVedita.getUnitaMisura2();
	}

	/**
	* Sets the unita misura2 of this righe fatture vedita.
	*
	* @param unitaMisura2 the unita misura2 of this righe fatture vedita
	*/
	@Override
	public void setUnitaMisura2(java.lang.String unitaMisura2) {
		_righeFattureVedita.setUnitaMisura2(unitaMisura2);
	}

	/**
	* Returns the quantita unit mis sec of this righe fatture vedita.
	*
	* @return the quantita unit mis sec of this righe fatture vedita
	*/
	@Override
	public double getQuantitaUnitMisSec() {
		return _righeFattureVedita.getQuantitaUnitMisSec();
	}

	/**
	* Sets the quantita unit mis sec of this righe fatture vedita.
	*
	* @param quantitaUnitMisSec the quantita unit mis sec of this righe fatture vedita
	*/
	@Override
	public void setQuantitaUnitMisSec(double quantitaUnitMisSec) {
		_righeFattureVedita.setQuantitaUnitMisSec(quantitaUnitMisSec);
	}

	/**
	* Returns the numero decimalli prezzo of this righe fatture vedita.
	*
	* @return the numero decimalli prezzo of this righe fatture vedita
	*/
	@Override
	public int getNumeroDecimalliPrezzo() {
		return _righeFattureVedita.getNumeroDecimalliPrezzo();
	}

	/**
	* Sets the numero decimalli prezzo of this righe fatture vedita.
	*
	* @param numeroDecimalliPrezzo the numero decimalli prezzo of this righe fatture vedita
	*/
	@Override
	public void setNumeroDecimalliPrezzo(int numeroDecimalliPrezzo) {
		_righeFattureVedita.setNumeroDecimalliPrezzo(numeroDecimalliPrezzo);
	}

	/**
	* Returns the prezzo of this righe fatture vedita.
	*
	* @return the prezzo of this righe fatture vedita
	*/
	@Override
	public double getPrezzo() {
		return _righeFattureVedita.getPrezzo();
	}

	/**
	* Sets the prezzo of this righe fatture vedita.
	*
	* @param prezzo the prezzo of this righe fatture vedita
	*/
	@Override
	public void setPrezzo(double prezzo) {
		_righeFattureVedita.setPrezzo(prezzo);
	}

	/**
	* Returns the prezzo i v a of this righe fatture vedita.
	*
	* @return the prezzo i v a of this righe fatture vedita
	*/
	@Override
	public double getPrezzoIVA() {
		return _righeFattureVedita.getPrezzoIVA();
	}

	/**
	* Sets the prezzo i v a of this righe fatture vedita.
	*
	* @param prezzoIVA the prezzo i v a of this righe fatture vedita
	*/
	@Override
	public void setPrezzoIVA(double prezzoIVA) {
		_righeFattureVedita.setPrezzoIVA(prezzoIVA);
	}

	/**
	* Returns the importo lordo of this righe fatture vedita.
	*
	* @return the importo lordo of this righe fatture vedita
	*/
	@Override
	public double getImportoLordo() {
		return _righeFattureVedita.getImportoLordo();
	}

	/**
	* Sets the importo lordo of this righe fatture vedita.
	*
	* @param importoLordo the importo lordo of this righe fatture vedita
	*/
	@Override
	public void setImportoLordo(double importoLordo) {
		_righeFattureVedita.setImportoLordo(importoLordo);
	}

	/**
	* Returns the importo lordo i v a of this righe fatture vedita.
	*
	* @return the importo lordo i v a of this righe fatture vedita
	*/
	@Override
	public double getImportoLordoIVA() {
		return _righeFattureVedita.getImportoLordoIVA();
	}

	/**
	* Sets the importo lordo i v a of this righe fatture vedita.
	*
	* @param importoLordoIVA the importo lordo i v a of this righe fatture vedita
	*/
	@Override
	public void setImportoLordoIVA(double importoLordoIVA) {
		_righeFattureVedita.setImportoLordoIVA(importoLordoIVA);
	}

	/**
	* Returns the sconto1 of this righe fatture vedita.
	*
	* @return the sconto1 of this righe fatture vedita
	*/
	@Override
	public double getSconto1() {
		return _righeFattureVedita.getSconto1();
	}

	/**
	* Sets the sconto1 of this righe fatture vedita.
	*
	* @param sconto1 the sconto1 of this righe fatture vedita
	*/
	@Override
	public void setSconto1(double sconto1) {
		_righeFattureVedita.setSconto1(sconto1);
	}

	/**
	* Returns the sconto2 of this righe fatture vedita.
	*
	* @return the sconto2 of this righe fatture vedita
	*/
	@Override
	public double getSconto2() {
		return _righeFattureVedita.getSconto2();
	}

	/**
	* Sets the sconto2 of this righe fatture vedita.
	*
	* @param sconto2 the sconto2 of this righe fatture vedita
	*/
	@Override
	public void setSconto2(double sconto2) {
		_righeFattureVedita.setSconto2(sconto2);
	}

	/**
	* Returns the sconto3 of this righe fatture vedita.
	*
	* @return the sconto3 of this righe fatture vedita
	*/
	@Override
	public double getSconto3() {
		return _righeFattureVedita.getSconto3();
	}

	/**
	* Sets the sconto3 of this righe fatture vedita.
	*
	* @param sconto3 the sconto3 of this righe fatture vedita
	*/
	@Override
	public void setSconto3(double sconto3) {
		_righeFattureVedita.setSconto3(sconto3);
	}

	/**
	* Returns the importo netto rigo of this righe fatture vedita.
	*
	* @return the importo netto rigo of this righe fatture vedita
	*/
	@Override
	public double getImportoNettoRigo() {
		return _righeFattureVedita.getImportoNettoRigo();
	}

	/**
	* Sets the importo netto rigo of this righe fatture vedita.
	*
	* @param importoNettoRigo the importo netto rigo of this righe fatture vedita
	*/
	@Override
	public void setImportoNettoRigo(double importoNettoRigo) {
		_righeFattureVedita.setImportoNettoRigo(importoNettoRigo);
	}

	/**
	* Returns the importo netto rigo i v a of this righe fatture vedita.
	*
	* @return the importo netto rigo i v a of this righe fatture vedita
	*/
	@Override
	public double getImportoNettoRigoIVA() {
		return _righeFattureVedita.getImportoNettoRigoIVA();
	}

	/**
	* Sets the importo netto rigo i v a of this righe fatture vedita.
	*
	* @param importoNettoRigoIVA the importo netto rigo i v a of this righe fatture vedita
	*/
	@Override
	public void setImportoNettoRigoIVA(double importoNettoRigoIVA) {
		_righeFattureVedita.setImportoNettoRigoIVA(importoNettoRigoIVA);
	}

	/**
	* Returns the importo netto of this righe fatture vedita.
	*
	* @return the importo netto of this righe fatture vedita
	*/
	@Override
	public double getImportoNetto() {
		return _righeFattureVedita.getImportoNetto();
	}

	/**
	* Sets the importo netto of this righe fatture vedita.
	*
	* @param importoNetto the importo netto of this righe fatture vedita
	*/
	@Override
	public void setImportoNetto(double importoNetto) {
		_righeFattureVedita.setImportoNetto(importoNetto);
	}

	/**
	* Returns the importo netto i v a of this righe fatture vedita.
	*
	* @return the importo netto i v a of this righe fatture vedita
	*/
	@Override
	public double getImportoNettoIVA() {
		return _righeFattureVedita.getImportoNettoIVA();
	}

	/**
	* Sets the importo netto i v a of this righe fatture vedita.
	*
	* @param importoNettoIVA the importo netto i v a of this righe fatture vedita
	*/
	@Override
	public void setImportoNettoIVA(double importoNettoIVA) {
		_righeFattureVedita.setImportoNettoIVA(importoNettoIVA);
	}

	/**
	* Returns the importo c o n a i addebitato int of this righe fatture vedita.
	*
	* @return the importo c o n a i addebitato int of this righe fatture vedita
	*/
	@Override
	public double getImportoCONAIAddebitatoInt() {
		return _righeFattureVedita.getImportoCONAIAddebitatoInt();
	}

	/**
	* Sets the importo c o n a i addebitato int of this righe fatture vedita.
	*
	* @param importoCONAIAddebitatoInt the importo c o n a i addebitato int of this righe fatture vedita
	*/
	@Override
	public void setImportoCONAIAddebitatoInt(double importoCONAIAddebitatoInt) {
		_righeFattureVedita.setImportoCONAIAddebitatoInt(importoCONAIAddebitatoInt);
	}

	/**
	* Returns the importo c o n a i addebitato orig of this righe fatture vedita.
	*
	* @return the importo c o n a i addebitato orig of this righe fatture vedita
	*/
	@Override
	public double getImportoCONAIAddebitatoOrig() {
		return _righeFattureVedita.getImportoCONAIAddebitatoOrig();
	}

	/**
	* Sets the importo c o n a i addebitato orig of this righe fatture vedita.
	*
	* @param importoCONAIAddebitatoOrig the importo c o n a i addebitato orig of this righe fatture vedita
	*/
	@Override
	public void setImportoCONAIAddebitatoOrig(double importoCONAIAddebitatoOrig) {
		_righeFattureVedita.setImportoCONAIAddebitatoOrig(importoCONAIAddebitatoOrig);
	}

	/**
	* Returns the codice i v a fatturazione of this righe fatture vedita.
	*
	* @return the codice i v a fatturazione of this righe fatture vedita
	*/
	@Override
	public java.lang.String getCodiceIVAFatturazione() {
		return _righeFattureVedita.getCodiceIVAFatturazione();
	}

	/**
	* Sets the codice i v a fatturazione of this righe fatture vedita.
	*
	* @param codiceIVAFatturazione the codice i v a fatturazione of this righe fatture vedita
	*/
	@Override
	public void setCodiceIVAFatturazione(java.lang.String codiceIVAFatturazione) {
		_righeFattureVedita.setCodiceIVAFatturazione(codiceIVAFatturazione);
	}

	/**
	* Returns the nomenclatura combinata of this righe fatture vedita.
	*
	* @return the nomenclatura combinata of this righe fatture vedita
	*/
	@Override
	public int getNomenclaturaCombinata() {
		return _righeFattureVedita.getNomenclaturaCombinata();
	}

	/**
	* Sets the nomenclatura combinata of this righe fatture vedita.
	*
	* @param nomenclaturaCombinata the nomenclatura combinata of this righe fatture vedita
	*/
	@Override
	public void setNomenclaturaCombinata(int nomenclaturaCombinata) {
		_righeFattureVedita.setNomenclaturaCombinata(nomenclaturaCombinata);
	}

	/**
	* Returns the test stampa dis base of this righe fatture vedita.
	*
	* @return the test stampa dis base of this righe fatture vedita
	*/
	@Override
	public boolean getTestStampaDisBase() {
		return _righeFattureVedita.getTestStampaDisBase();
	}

	/**
	* Returns <code>true</code> if this righe fatture vedita is test stampa dis base.
	*
	* @return <code>true</code> if this righe fatture vedita is test stampa dis base; <code>false</code> otherwise
	*/
	@Override
	public boolean isTestStampaDisBase() {
		return _righeFattureVedita.isTestStampaDisBase();
	}

	/**
	* Sets whether this righe fatture vedita is test stampa dis base.
	*
	* @param testStampaDisBase the test stampa dis base of this righe fatture vedita
	*/
	@Override
	public void setTestStampaDisBase(boolean testStampaDisBase) {
		_righeFattureVedita.setTestStampaDisBase(testStampaDisBase);
	}

	/**
	* Returns the tipo ordine evaso of this righe fatture vedita.
	*
	* @return the tipo ordine evaso of this righe fatture vedita
	*/
	@Override
	public int getTipoOrdineEvaso() {
		return _righeFattureVedita.getTipoOrdineEvaso();
	}

	/**
	* Sets the tipo ordine evaso of this righe fatture vedita.
	*
	* @param tipoOrdineEvaso the tipo ordine evaso of this righe fatture vedita
	*/
	@Override
	public void setTipoOrdineEvaso(int tipoOrdineEvaso) {
		_righeFattureVedita.setTipoOrdineEvaso(tipoOrdineEvaso);
	}

	/**
	* Returns the anno ordine evaso of this righe fatture vedita.
	*
	* @return the anno ordine evaso of this righe fatture vedita
	*/
	@Override
	public int getAnnoOrdineEvaso() {
		return _righeFattureVedita.getAnnoOrdineEvaso();
	}

	/**
	* Sets the anno ordine evaso of this righe fatture vedita.
	*
	* @param annoOrdineEvaso the anno ordine evaso of this righe fatture vedita
	*/
	@Override
	public void setAnnoOrdineEvaso(int annoOrdineEvaso) {
		_righeFattureVedita.setAnnoOrdineEvaso(annoOrdineEvaso);
	}

	/**
	* Returns the numero ordine evaso of this righe fatture vedita.
	*
	* @return the numero ordine evaso of this righe fatture vedita
	*/
	@Override
	public int getNumeroOrdineEvaso() {
		return _righeFattureVedita.getNumeroOrdineEvaso();
	}

	/**
	* Sets the numero ordine evaso of this righe fatture vedita.
	*
	* @param numeroOrdineEvaso the numero ordine evaso of this righe fatture vedita
	*/
	@Override
	public void setNumeroOrdineEvaso(int numeroOrdineEvaso) {
		_righeFattureVedita.setNumeroOrdineEvaso(numeroOrdineEvaso);
	}

	/**
	* Returns the numero rigo ordine evaso of this righe fatture vedita.
	*
	* @return the numero rigo ordine evaso of this righe fatture vedita
	*/
	@Override
	public int getNumeroRigoOrdineEvaso() {
		return _righeFattureVedita.getNumeroRigoOrdineEvaso();
	}

	/**
	* Sets the numero rigo ordine evaso of this righe fatture vedita.
	*
	* @param numeroRigoOrdineEvaso the numero rigo ordine evaso of this righe fatture vedita
	*/
	@Override
	public void setNumeroRigoOrdineEvaso(int numeroRigoOrdineEvaso) {
		_righeFattureVedita.setNumeroRigoOrdineEvaso(numeroRigoOrdineEvaso);
	}

	/**
	* Returns the tipo evasione of this righe fatture vedita.
	*
	* @return the tipo evasione of this righe fatture vedita
	*/
	@Override
	public java.lang.String getTipoEvasione() {
		return _righeFattureVedita.getTipoEvasione();
	}

	/**
	* Sets the tipo evasione of this righe fatture vedita.
	*
	* @param tipoEvasione the tipo evasione of this righe fatture vedita
	*/
	@Override
	public void setTipoEvasione(java.lang.String tipoEvasione) {
		_righeFattureVedita.setTipoEvasione(tipoEvasione);
	}

	/**
	* Returns the descrizione riferimento of this righe fatture vedita.
	*
	* @return the descrizione riferimento of this righe fatture vedita
	*/
	@Override
	public java.lang.String getDescrizioneRiferimento() {
		return _righeFattureVedita.getDescrizioneRiferimento();
	}

	/**
	* Sets the descrizione riferimento of this righe fatture vedita.
	*
	* @param descrizioneRiferimento the descrizione riferimento of this righe fatture vedita
	*/
	@Override
	public void setDescrizioneRiferimento(
		java.lang.String descrizioneRiferimento) {
		_righeFattureVedita.setDescrizioneRiferimento(descrizioneRiferimento);
	}

	/**
	* Returns the numero prima nota magazzino of this righe fatture vedita.
	*
	* @return the numero prima nota magazzino of this righe fatture vedita
	*/
	@Override
	public int getNumeroPrimaNotaMagazzino() {
		return _righeFattureVedita.getNumeroPrimaNotaMagazzino();
	}

	/**
	* Sets the numero prima nota magazzino of this righe fatture vedita.
	*
	* @param numeroPrimaNotaMagazzino the numero prima nota magazzino of this righe fatture vedita
	*/
	@Override
	public void setNumeroPrimaNotaMagazzino(int numeroPrimaNotaMagazzino) {
		_righeFattureVedita.setNumeroPrimaNotaMagazzino(numeroPrimaNotaMagazzino);
	}

	/**
	* Returns the numero movimento magazzino of this righe fatture vedita.
	*
	* @return the numero movimento magazzino of this righe fatture vedita
	*/
	@Override
	public int getNumeroMovimentoMagazzino() {
		return _righeFattureVedita.getNumeroMovimentoMagazzino();
	}

	/**
	* Sets the numero movimento magazzino of this righe fatture vedita.
	*
	* @param numeroMovimentoMagazzino the numero movimento magazzino of this righe fatture vedita
	*/
	@Override
	public void setNumeroMovimentoMagazzino(int numeroMovimentoMagazzino) {
		_righeFattureVedita.setNumeroMovimentoMagazzino(numeroMovimentoMagazzino);
	}

	/**
	* Returns the tipo esplosione of this righe fatture vedita.
	*
	* @return the tipo esplosione of this righe fatture vedita
	*/
	@Override
	public int getTipoEsplosione() {
		return _righeFattureVedita.getTipoEsplosione();
	}

	/**
	* Sets the tipo esplosione of this righe fatture vedita.
	*
	* @param tipoEsplosione the tipo esplosione of this righe fatture vedita
	*/
	@Override
	public void setTipoEsplosione(int tipoEsplosione) {
		_righeFattureVedita.setTipoEsplosione(tipoEsplosione);
	}

	/**
	* Returns the livello massimo esplosione of this righe fatture vedita.
	*
	* @return the livello massimo esplosione of this righe fatture vedita
	*/
	@Override
	public int getLivelloMassimoEsplosione() {
		return _righeFattureVedita.getLivelloMassimoEsplosione();
	}

	/**
	* Sets the livello massimo esplosione of this righe fatture vedita.
	*
	* @param livelloMassimoEsplosione the livello massimo esplosione of this righe fatture vedita
	*/
	@Override
	public void setLivelloMassimoEsplosione(int livelloMassimoEsplosione) {
		_righeFattureVedita.setLivelloMassimoEsplosione(livelloMassimoEsplosione);
	}

	/**
	* Returns the lib str1 of this righe fatture vedita.
	*
	* @return the lib str1 of this righe fatture vedita
	*/
	@Override
	public java.lang.String getLibStr1() {
		return _righeFattureVedita.getLibStr1();
	}

	/**
	* Sets the lib str1 of this righe fatture vedita.
	*
	* @param libStr1 the lib str1 of this righe fatture vedita
	*/
	@Override
	public void setLibStr1(java.lang.String libStr1) {
		_righeFattureVedita.setLibStr1(libStr1);
	}

	/**
	* Returns the lib str2 of this righe fatture vedita.
	*
	* @return the lib str2 of this righe fatture vedita
	*/
	@Override
	public java.lang.String getLibStr2() {
		return _righeFattureVedita.getLibStr2();
	}

	/**
	* Sets the lib str2 of this righe fatture vedita.
	*
	* @param libStr2 the lib str2 of this righe fatture vedita
	*/
	@Override
	public void setLibStr2(java.lang.String libStr2) {
		_righeFattureVedita.setLibStr2(libStr2);
	}

	/**
	* Returns the lib str3 of this righe fatture vedita.
	*
	* @return the lib str3 of this righe fatture vedita
	*/
	@Override
	public java.lang.String getLibStr3() {
		return _righeFattureVedita.getLibStr3();
	}

	/**
	* Sets the lib str3 of this righe fatture vedita.
	*
	* @param libStr3 the lib str3 of this righe fatture vedita
	*/
	@Override
	public void setLibStr3(java.lang.String libStr3) {
		_righeFattureVedita.setLibStr3(libStr3);
	}

	/**
	* Returns the lib dbl1 of this righe fatture vedita.
	*
	* @return the lib dbl1 of this righe fatture vedita
	*/
	@Override
	public double getLibDbl1() {
		return _righeFattureVedita.getLibDbl1();
	}

	/**
	* Sets the lib dbl1 of this righe fatture vedita.
	*
	* @param libDbl1 the lib dbl1 of this righe fatture vedita
	*/
	@Override
	public void setLibDbl1(double libDbl1) {
		_righeFattureVedita.setLibDbl1(libDbl1);
	}

	/**
	* Returns the lib dbl2 of this righe fatture vedita.
	*
	* @return the lib dbl2 of this righe fatture vedita
	*/
	@Override
	public double getLibDbl2() {
		return _righeFattureVedita.getLibDbl2();
	}

	/**
	* Sets the lib dbl2 of this righe fatture vedita.
	*
	* @param libDbl2 the lib dbl2 of this righe fatture vedita
	*/
	@Override
	public void setLibDbl2(double libDbl2) {
		_righeFattureVedita.setLibDbl2(libDbl2);
	}

	/**
	* Returns the lib dbl3 of this righe fatture vedita.
	*
	* @return the lib dbl3 of this righe fatture vedita
	*/
	@Override
	public double getLibDbl3() {
		return _righeFattureVedita.getLibDbl3();
	}

	/**
	* Sets the lib dbl3 of this righe fatture vedita.
	*
	* @param libDbl3 the lib dbl3 of this righe fatture vedita
	*/
	@Override
	public void setLibDbl3(double libDbl3) {
		_righeFattureVedita.setLibDbl3(libDbl3);
	}

	/**
	* Returns the lib lng1 of this righe fatture vedita.
	*
	* @return the lib lng1 of this righe fatture vedita
	*/
	@Override
	public long getLibLng1() {
		return _righeFattureVedita.getLibLng1();
	}

	/**
	* Sets the lib lng1 of this righe fatture vedita.
	*
	* @param libLng1 the lib lng1 of this righe fatture vedita
	*/
	@Override
	public void setLibLng1(long libLng1) {
		_righeFattureVedita.setLibLng1(libLng1);
	}

	/**
	* Returns the lib lng2 of this righe fatture vedita.
	*
	* @return the lib lng2 of this righe fatture vedita
	*/
	@Override
	public long getLibLng2() {
		return _righeFattureVedita.getLibLng2();
	}

	/**
	* Sets the lib lng2 of this righe fatture vedita.
	*
	* @param libLng2 the lib lng2 of this righe fatture vedita
	*/
	@Override
	public void setLibLng2(long libLng2) {
		_righeFattureVedita.setLibLng2(libLng2);
	}

	/**
	* Returns the lib lng3 of this righe fatture vedita.
	*
	* @return the lib lng3 of this righe fatture vedita
	*/
	@Override
	public long getLibLng3() {
		return _righeFattureVedita.getLibLng3();
	}

	/**
	* Sets the lib lng3 of this righe fatture vedita.
	*
	* @param libLng3 the lib lng3 of this righe fatture vedita
	*/
	@Override
	public void setLibLng3(long libLng3) {
		_righeFattureVedita.setLibLng3(libLng3);
	}

	/**
	* Returns the lib dat1 of this righe fatture vedita.
	*
	* @return the lib dat1 of this righe fatture vedita
	*/
	@Override
	public java.util.Date getLibDat1() {
		return _righeFattureVedita.getLibDat1();
	}

	/**
	* Sets the lib dat1 of this righe fatture vedita.
	*
	* @param libDat1 the lib dat1 of this righe fatture vedita
	*/
	@Override
	public void setLibDat1(java.util.Date libDat1) {
		_righeFattureVedita.setLibDat1(libDat1);
	}

	/**
	* Returns the lib dat2 of this righe fatture vedita.
	*
	* @return the lib dat2 of this righe fatture vedita
	*/
	@Override
	public java.util.Date getLibDat2() {
		return _righeFattureVedita.getLibDat2();
	}

	/**
	* Sets the lib dat2 of this righe fatture vedita.
	*
	* @param libDat2 the lib dat2 of this righe fatture vedita
	*/
	@Override
	public void setLibDat2(java.util.Date libDat2) {
		_righeFattureVedita.setLibDat2(libDat2);
	}

	/**
	* Returns the lib dat3 of this righe fatture vedita.
	*
	* @return the lib dat3 of this righe fatture vedita
	*/
	@Override
	public java.util.Date getLibDat3() {
		return _righeFattureVedita.getLibDat3();
	}

	/**
	* Sets the lib dat3 of this righe fatture vedita.
	*
	* @param libDat3 the lib dat3 of this righe fatture vedita
	*/
	@Override
	public void setLibDat3(java.util.Date libDat3) {
		_righeFattureVedita.setLibDat3(libDat3);
	}

	/**
	* Returns the test fatturazione auto of this righe fatture vedita.
	*
	* @return the test fatturazione auto of this righe fatture vedita
	*/
	@Override
	public boolean getTestFatturazioneAuto() {
		return _righeFattureVedita.getTestFatturazioneAuto();
	}

	/**
	* Returns <code>true</code> if this righe fatture vedita is test fatturazione auto.
	*
	* @return <code>true</code> if this righe fatture vedita is test fatturazione auto; <code>false</code> otherwise
	*/
	@Override
	public boolean isTestFatturazioneAuto() {
		return _righeFattureVedita.isTestFatturazioneAuto();
	}

	/**
	* Sets whether this righe fatture vedita is test fatturazione auto.
	*
	* @param testFatturazioneAuto the test fatturazione auto of this righe fatture vedita
	*/
	@Override
	public void setTestFatturazioneAuto(boolean testFatturazioneAuto) {
		_righeFattureVedita.setTestFatturazioneAuto(testFatturazioneAuto);
	}

	@Override
	public boolean isNew() {
		return _righeFattureVedita.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_righeFattureVedita.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _righeFattureVedita.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_righeFattureVedita.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _righeFattureVedita.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _righeFattureVedita.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_righeFattureVedita.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _righeFattureVedita.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_righeFattureVedita.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_righeFattureVedita.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_righeFattureVedita.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new RigheFattureVeditaWrapper((RigheFattureVedita)_righeFattureVedita.clone());
	}

	@Override
	public int compareTo(
		it.bysoftware.ct.model.RigheFattureVedita righeFattureVedita) {
		return _righeFattureVedita.compareTo(righeFattureVedita);
	}

	@Override
	public int hashCode() {
		return _righeFattureVedita.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.RigheFattureVedita> toCacheModel() {
		return _righeFattureVedita.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.RigheFattureVedita toEscapedModel() {
		return new RigheFattureVeditaWrapper(_righeFattureVedita.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.RigheFattureVedita toUnescapedModel() {
		return new RigheFattureVeditaWrapper(_righeFattureVedita.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _righeFattureVedita.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _righeFattureVedita.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_righeFattureVedita.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof RigheFattureVeditaWrapper)) {
			return false;
		}

		RigheFattureVeditaWrapper righeFattureVeditaWrapper = (RigheFattureVeditaWrapper)obj;

		if (Validator.equals(_righeFattureVedita,
					righeFattureVeditaWrapper._righeFattureVedita)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public RigheFattureVedita getWrappedRigheFattureVedita() {
		return _righeFattureVedita;
	}

	@Override
	public RigheFattureVedita getWrappedModel() {
		return _righeFattureVedita;
	}

	@Override
	public void resetOriginalValues() {
		_righeFattureVedita.resetOriginalValues();
	}

	private RigheFattureVedita _righeFattureVedita;
}