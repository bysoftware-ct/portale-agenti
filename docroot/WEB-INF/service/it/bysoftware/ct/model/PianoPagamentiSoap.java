/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.PianoPagamentiServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.PianoPagamentiServiceSoap
 * @generated
 */
public class PianoPagamentiSoap implements Serializable {
	public static PianoPagamentiSoap toSoapModel(PianoPagamenti model) {
		PianoPagamentiSoap soapModel = new PianoPagamentiSoap();

		soapModel.setCodicePianoPagamento(model.getCodicePianoPagamento());
		soapModel.setDescrizione(model.getDescrizione());
		soapModel.setPrimoMeseEscluso(model.getPrimoMeseEscluso());
		soapModel.setGiornoPrimoMeseSucc(model.getGiornoPrimoMeseSucc());
		soapModel.setGiornoSecondoMeseSucc(model.getGiornoSecondoMeseSucc());
		soapModel.setSecondoMeseEscluso(model.getSecondoMeseEscluso());
		soapModel.setInizioFattDaBolla(model.getInizioFattDaBolla());
		soapModel.setPercScChiusura(model.getPercScChiusura());
		soapModel.setPercScCassa(model.getPercScCassa());

		return soapModel;
	}

	public static PianoPagamentiSoap[] toSoapModels(PianoPagamenti[] models) {
		PianoPagamentiSoap[] soapModels = new PianoPagamentiSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PianoPagamentiSoap[][] toSoapModels(PianoPagamenti[][] models) {
		PianoPagamentiSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PianoPagamentiSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PianoPagamentiSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PianoPagamentiSoap[] toSoapModels(List<PianoPagamenti> models) {
		List<PianoPagamentiSoap> soapModels = new ArrayList<PianoPagamentiSoap>(models.size());

		for (PianoPagamenti model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PianoPagamentiSoap[soapModels.size()]);
	}

	public PianoPagamentiSoap() {
	}

	public String getPrimaryKey() {
		return _codicePianoPagamento;
	}

	public void setPrimaryKey(String pk) {
		setCodicePianoPagamento(pk);
	}

	public String getCodicePianoPagamento() {
		return _codicePianoPagamento;
	}

	public void setCodicePianoPagamento(String codicePianoPagamento) {
		_codicePianoPagamento = codicePianoPagamento;
	}

	public String getDescrizione() {
		return _descrizione;
	}

	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;
	}

	public int getPrimoMeseEscluso() {
		return _primoMeseEscluso;
	}

	public void setPrimoMeseEscluso(int primoMeseEscluso) {
		_primoMeseEscluso = primoMeseEscluso;
	}

	public int getGiornoPrimoMeseSucc() {
		return _giornoPrimoMeseSucc;
	}

	public void setGiornoPrimoMeseSucc(int giornoPrimoMeseSucc) {
		_giornoPrimoMeseSucc = giornoPrimoMeseSucc;
	}

	public int getGiornoSecondoMeseSucc() {
		return _giornoSecondoMeseSucc;
	}

	public void setGiornoSecondoMeseSucc(int giornoSecondoMeseSucc) {
		_giornoSecondoMeseSucc = giornoSecondoMeseSucc;
	}

	public int getSecondoMeseEscluso() {
		return _secondoMeseEscluso;
	}

	public void setSecondoMeseEscluso(int secondoMeseEscluso) {
		_secondoMeseEscluso = secondoMeseEscluso;
	}

	public boolean getInizioFattDaBolla() {
		return _inizioFattDaBolla;
	}

	public boolean isInizioFattDaBolla() {
		return _inizioFattDaBolla;
	}

	public void setInizioFattDaBolla(boolean inizioFattDaBolla) {
		_inizioFattDaBolla = inizioFattDaBolla;
	}

	public double getPercScChiusura() {
		return _percScChiusura;
	}

	public void setPercScChiusura(double percScChiusura) {
		_percScChiusura = percScChiusura;
	}

	public double getPercScCassa() {
		return _percScCassa;
	}

	public void setPercScCassa(double percScCassa) {
		_percScCassa = percScCassa;
	}

	private String _codicePianoPagamento;
	private String _descrizione;
	private int _primoMeseEscluso;
	private int _giornoPrimoMeseSucc;
	private int _giornoSecondoMeseSucc;
	private int _secondoMeseEscluso;
	private boolean _inizioFattDaBolla;
	private double _percScChiusura;
	private double _percScCassa;
}