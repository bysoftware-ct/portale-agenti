/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the AnagraficaAgenti service. Represents a row in the &quot;AGENTI&quot; database table, with each column mapped to a property of this class.
 *
 * @author Mario Torrisi
 * @see AnagraficaAgentiModel
 * @see it.bysoftware.ct.model.impl.AnagraficaAgentiImpl
 * @see it.bysoftware.ct.model.impl.AnagraficaAgentiModelImpl
 * @generated
 */
public interface AnagraficaAgenti extends AnagraficaAgentiModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link it.bysoftware.ct.model.impl.AnagraficaAgentiImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
}