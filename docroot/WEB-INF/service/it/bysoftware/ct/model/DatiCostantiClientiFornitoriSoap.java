/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.DatiCostantiClientiFornitoriServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.DatiCostantiClientiFornitoriServiceSoap
 * @generated
 */
public class DatiCostantiClientiFornitoriSoap implements Serializable {
	public static DatiCostantiClientiFornitoriSoap toSoapModel(
		DatiCostantiClientiFornitori model) {
		DatiCostantiClientiFornitoriSoap soapModel = new DatiCostantiClientiFornitoriSoap();

		soapModel.setTipoSoggetto(model.getTipoSoggetto());
		soapModel.setCodiceSoggetto(model.getCodiceSoggetto());
		soapModel.setCodiceSottoconto(model.getCodiceSottoconto());
		soapModel.setMassimoScoperto(model.getMassimoScoperto());
		soapModel.setGestioneEstrattoConto(model.getGestioneEstrattoConto());
		soapModel.setCodicePagamento(model.getCodicePagamento());
		soapModel.setCodiceBancaPagFor(model.getCodiceBancaPagFor());
		soapModel.setCodiceAgenziaPagFor(model.getCodiceAgenziaPagFor());
		soapModel.setCodiceContropartita(model.getCodiceContropartita());
		soapModel.setCalcoloIntMora(model.getCalcoloIntMora());

		return soapModel;
	}

	public static DatiCostantiClientiFornitoriSoap[] toSoapModels(
		DatiCostantiClientiFornitori[] models) {
		DatiCostantiClientiFornitoriSoap[] soapModels = new DatiCostantiClientiFornitoriSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DatiCostantiClientiFornitoriSoap[][] toSoapModels(
		DatiCostantiClientiFornitori[][] models) {
		DatiCostantiClientiFornitoriSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DatiCostantiClientiFornitoriSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DatiCostantiClientiFornitoriSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DatiCostantiClientiFornitoriSoap[] toSoapModels(
		List<DatiCostantiClientiFornitori> models) {
		List<DatiCostantiClientiFornitoriSoap> soapModels = new ArrayList<DatiCostantiClientiFornitoriSoap>(models.size());

		for (DatiCostantiClientiFornitori model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DatiCostantiClientiFornitoriSoap[soapModels.size()]);
	}

	public DatiCostantiClientiFornitoriSoap() {
	}

	public DatiCostantiClientiFornitoriPK getPrimaryKey() {
		return new DatiCostantiClientiFornitoriPK(_tipoSoggetto, _codiceSoggetto);
	}

	public void setPrimaryKey(DatiCostantiClientiFornitoriPK pk) {
		setTipoSoggetto(pk.tipoSoggetto);
		setCodiceSoggetto(pk.codiceSoggetto);
	}

	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;
	}

	public String getCodiceSoggetto() {
		return _codiceSoggetto;
	}

	public void setCodiceSoggetto(String codiceSoggetto) {
		_codiceSoggetto = codiceSoggetto;
	}

	public String getCodiceSottoconto() {
		return _codiceSottoconto;
	}

	public void setCodiceSottoconto(String codiceSottoconto) {
		_codiceSottoconto = codiceSottoconto;
	}

	public double getMassimoScoperto() {
		return _massimoScoperto;
	}

	public void setMassimoScoperto(double massimoScoperto) {
		_massimoScoperto = massimoScoperto;
	}

	public boolean getGestioneEstrattoConto() {
		return _gestioneEstrattoConto;
	}

	public boolean isGestioneEstrattoConto() {
		return _gestioneEstrattoConto;
	}

	public void setGestioneEstrattoConto(boolean gestioneEstrattoConto) {
		_gestioneEstrattoConto = gestioneEstrattoConto;
	}

	public String getCodicePagamento() {
		return _codicePagamento;
	}

	public void setCodicePagamento(String codicePagamento) {
		_codicePagamento = codicePagamento;
	}

	public String getCodiceBancaPagFor() {
		return _codiceBancaPagFor;
	}

	public void setCodiceBancaPagFor(String codiceBancaPagFor) {
		_codiceBancaPagFor = codiceBancaPagFor;
	}

	public String getCodiceAgenziaPagFor() {
		return _codiceAgenziaPagFor;
	}

	public void setCodiceAgenziaPagFor(String codiceAgenziaPagFor) {
		_codiceAgenziaPagFor = codiceAgenziaPagFor;
	}

	public String getCodiceContropartita() {
		return _codiceContropartita;
	}

	public void setCodiceContropartita(String codiceContropartita) {
		_codiceContropartita = codiceContropartita;
	}

	public boolean getCalcoloIntMora() {
		return _calcoloIntMora;
	}

	public boolean isCalcoloIntMora() {
		return _calcoloIntMora;
	}

	public void setCalcoloIntMora(boolean calcoloIntMora) {
		_calcoloIntMora = calcoloIntMora;
	}

	private boolean _tipoSoggetto;
	private String _codiceSoggetto;
	private String _codiceSottoconto;
	private double _massimoScoperto;
	private boolean _gestioneEstrattoConto;
	private String _codicePagamento;
	private String _codiceBancaPagFor;
	private String _codiceAgenziaPagFor;
	private String _codiceContropartita;
	private boolean _calcoloIntMora;
}