/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link OrdiniClienti}.
 * </p>
 *
 * @author Mario Torrisi
 * @see OrdiniClienti
 * @generated
 */
public class OrdiniClientiWrapper implements OrdiniClienti,
	ModelWrapper<OrdiniClienti> {
	public OrdiniClientiWrapper(OrdiniClienti ordiniClienti) {
		_ordiniClienti = ordiniClienti;
	}

	@Override
	public Class<?> getModelClass() {
		return OrdiniClienti.class;
	}

	@Override
	public String getModelClassName() {
		return OrdiniClienti.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("anno", getAnno());
		attributes.put("codiceAttivita", getCodiceAttivita());
		attributes.put("codiceCentro", getCodiceCentro());
		attributes.put("codiceDeposito", getCodiceDeposito());
		attributes.put("tipoOrdine", getTipoOrdine());
		attributes.put("numeroOrdine", getNumeroOrdine());
		attributes.put("tipoDocumento", getTipoDocumento());
		attributes.put("codiceContAnalitica", getCodiceContAnalitica());
		attributes.put("idTipoDocumento", getIdTipoDocumento());
		attributes.put("statoOrdine", getStatoOrdine());
		attributes.put("descrEstremiDoc", getDescrEstremiDoc());
		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceCliente", getCodiceCliente());
		attributes.put("naturaTransazione", getNaturaTransazione());
		attributes.put("codiceEsenzione", getCodiceEsenzione());
		attributes.put("codiceDivisa", getCodiceDivisa());
		attributes.put("valoreCambio", getValoreCambio());
		attributes.put("dataValoreCambio", getDataValoreCambio());
		attributes.put("codicePianoPag", getCodicePianoPag());
		attributes.put("inizioCalcoloPag", getInizioCalcoloPag());
		attributes.put("percentualeScontoMaggiorazione",
			getPercentualeScontoMaggiorazione());
		attributes.put("percentualeScontoProntaCassa",
			getPercentualeScontoProntaCassa());
		attributes.put("percentualeProvvChiusura", getPercentualeProvvChiusura());
		attributes.put("dataDocumento", getDataDocumento());
		attributes.put("dataRegistrazione", getDataRegistrazione());
		attributes.put("causaleEstrattoConto", getCausaleEstrattoConto());
		attributes.put("dataPrimaRata", getDataPrimaRata());
		attributes.put("dataUltimaRata", getDataUltimaRata());
		attributes.put("codiceBanca", getCodiceBanca());
		attributes.put("codiceAgenzia", getCodiceAgenzia());
		attributes.put("codiceAgente", getCodiceAgente());
		attributes.put("codiceGruppoAgenti", getCodiceGruppoAgenti());
		attributes.put("codiceZona", getCodiceZona());
		attributes.put("codiceSpedizione", getCodiceSpedizione());
		attributes.put("codicePorto", getCodicePorto());
		attributes.put("codiceDestinatario", getCodiceDestinatario());
		attributes.put("codiceListino", getCodiceListino());
		attributes.put("codiceLingua", getCodiceLingua());
		attributes.put("numeroDecPrezzo", getNumeroDecPrezzo());
		attributes.put("note", getNote());
		attributes.put("percentualeSpeseTrasp", getPercentualeSpeseTrasp());
		attributes.put("speseTrasporto", getSpeseTrasporto());
		attributes.put("speseImballaggio", getSpeseImballaggio());
		attributes.put("speseVarie", getSpeseVarie());
		attributes.put("speseBanca", getSpeseBanca());
		attributes.put("curaTrasporto", getCuraTrasporto());
		attributes.put("causaleTrasporto", getCausaleTrasporto());
		attributes.put("aspettoEstriore", getAspettoEstriore());
		attributes.put("vettore1", getVettore1());
		attributes.put("vettore2", getVettore2());
		attributes.put("vettore3", getVettore3());
		attributes.put("numeroColli", getNumeroColli());
		attributes.put("pesoLordo", getPesoLordo());
		attributes.put("pesoNetto", getPesoNetto());
		attributes.put("volume", getVolume());
		attributes.put("numeroCopie", getNumeroCopie());
		attributes.put("numeroCopieStampate", getNumeroCopieStampate());
		attributes.put("inviatoEmail", getInviatoEmail());
		attributes.put("nomePDF", getNomePDF());
		attributes.put("riferimentoOrdine", getRiferimentoOrdine());
		attributes.put("dataConferma", getDataConferma());
		attributes.put("confermaStampata", getConfermaStampata());
		attributes.put("totaleOrdine", getTotaleOrdine());
		attributes.put("codiceIVATrasp", getCodiceIVATrasp());
		attributes.put("codiceIVAImballo", getCodiceIVAImballo());
		attributes.put("codiceIVAVarie", getCodiceIVAVarie());
		attributes.put("codiceIVABanca", getCodiceIVABanca());
		attributes.put("libStr1", getLibStr1());
		attributes.put("libStr2", getLibStr2());
		attributes.put("libStr3", getLibStr3());
		attributes.put("libDbl1", getLibDbl1());
		attributes.put("libDbl2", getLibDbl2());
		attributes.put("libDbl3", getLibDbl3());
		attributes.put("libDat1", getLibDat1());
		attributes.put("libDat2", getLibDat2());
		attributes.put("libDat3", getLibDat3());
		attributes.put("libLng1", getLibLng1());
		attributes.put("libLng2", getLibLng2());
		attributes.put("libLng3", getLibLng3());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer anno = (Integer)attributes.get("anno");

		if (anno != null) {
			setAnno(anno);
		}

		String codiceAttivita = (String)attributes.get("codiceAttivita");

		if (codiceAttivita != null) {
			setCodiceAttivita(codiceAttivita);
		}

		String codiceCentro = (String)attributes.get("codiceCentro");

		if (codiceCentro != null) {
			setCodiceCentro(codiceCentro);
		}

		String codiceDeposito = (String)attributes.get("codiceDeposito");

		if (codiceDeposito != null) {
			setCodiceDeposito(codiceDeposito);
		}

		Integer tipoOrdine = (Integer)attributes.get("tipoOrdine");

		if (tipoOrdine != null) {
			setTipoOrdine(tipoOrdine);
		}

		Integer numeroOrdine = (Integer)attributes.get("numeroOrdine");

		if (numeroOrdine != null) {
			setNumeroOrdine(numeroOrdine);
		}

		String tipoDocumento = (String)attributes.get("tipoDocumento");

		if (tipoDocumento != null) {
			setTipoDocumento(tipoDocumento);
		}

		String codiceContAnalitica = (String)attributes.get(
				"codiceContAnalitica");

		if (codiceContAnalitica != null) {
			setCodiceContAnalitica(codiceContAnalitica);
		}

		Integer idTipoDocumento = (Integer)attributes.get("idTipoDocumento");

		if (idTipoDocumento != null) {
			setIdTipoDocumento(idTipoDocumento);
		}

		Boolean statoOrdine = (Boolean)attributes.get("statoOrdine");

		if (statoOrdine != null) {
			setStatoOrdine(statoOrdine);
		}

		String descrEstremiDoc = (String)attributes.get("descrEstremiDoc");

		if (descrEstremiDoc != null) {
			setDescrEstremiDoc(descrEstremiDoc);
		}

		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceCliente = (String)attributes.get("codiceCliente");

		if (codiceCliente != null) {
			setCodiceCliente(codiceCliente);
		}

		String naturaTransazione = (String)attributes.get("naturaTransazione");

		if (naturaTransazione != null) {
			setNaturaTransazione(naturaTransazione);
		}

		String codiceEsenzione = (String)attributes.get("codiceEsenzione");

		if (codiceEsenzione != null) {
			setCodiceEsenzione(codiceEsenzione);
		}

		String codiceDivisa = (String)attributes.get("codiceDivisa");

		if (codiceDivisa != null) {
			setCodiceDivisa(codiceDivisa);
		}

		Double valoreCambio = (Double)attributes.get("valoreCambio");

		if (valoreCambio != null) {
			setValoreCambio(valoreCambio);
		}

		Date dataValoreCambio = (Date)attributes.get("dataValoreCambio");

		if (dataValoreCambio != null) {
			setDataValoreCambio(dataValoreCambio);
		}

		String codicePianoPag = (String)attributes.get("codicePianoPag");

		if (codicePianoPag != null) {
			setCodicePianoPag(codicePianoPag);
		}

		Date inizioCalcoloPag = (Date)attributes.get("inizioCalcoloPag");

		if (inizioCalcoloPag != null) {
			setInizioCalcoloPag(inizioCalcoloPag);
		}

		Double percentualeScontoMaggiorazione = (Double)attributes.get(
				"percentualeScontoMaggiorazione");

		if (percentualeScontoMaggiorazione != null) {
			setPercentualeScontoMaggiorazione(percentualeScontoMaggiorazione);
		}

		Double percentualeScontoProntaCassa = (Double)attributes.get(
				"percentualeScontoProntaCassa");

		if (percentualeScontoProntaCassa != null) {
			setPercentualeScontoProntaCassa(percentualeScontoProntaCassa);
		}

		Double percentualeProvvChiusura = (Double)attributes.get(
				"percentualeProvvChiusura");

		if (percentualeProvvChiusura != null) {
			setPercentualeProvvChiusura(percentualeProvvChiusura);
		}

		Date dataDocumento = (Date)attributes.get("dataDocumento");

		if (dataDocumento != null) {
			setDataDocumento(dataDocumento);
		}

		Date dataRegistrazione = (Date)attributes.get("dataRegistrazione");

		if (dataRegistrazione != null) {
			setDataRegistrazione(dataRegistrazione);
		}

		String causaleEstrattoConto = (String)attributes.get(
				"causaleEstrattoConto");

		if (causaleEstrattoConto != null) {
			setCausaleEstrattoConto(causaleEstrattoConto);
		}

		Date dataPrimaRata = (Date)attributes.get("dataPrimaRata");

		if (dataPrimaRata != null) {
			setDataPrimaRata(dataPrimaRata);
		}

		Date dataUltimaRata = (Date)attributes.get("dataUltimaRata");

		if (dataUltimaRata != null) {
			setDataUltimaRata(dataUltimaRata);
		}

		String codiceBanca = (String)attributes.get("codiceBanca");

		if (codiceBanca != null) {
			setCodiceBanca(codiceBanca);
		}

		String codiceAgenzia = (String)attributes.get("codiceAgenzia");

		if (codiceAgenzia != null) {
			setCodiceAgenzia(codiceAgenzia);
		}

		String codiceAgente = (String)attributes.get("codiceAgente");

		if (codiceAgente != null) {
			setCodiceAgente(codiceAgente);
		}

		String codiceGruppoAgenti = (String)attributes.get("codiceGruppoAgenti");

		if (codiceGruppoAgenti != null) {
			setCodiceGruppoAgenti(codiceGruppoAgenti);
		}

		String codiceZona = (String)attributes.get("codiceZona");

		if (codiceZona != null) {
			setCodiceZona(codiceZona);
		}

		String codiceSpedizione = (String)attributes.get("codiceSpedizione");

		if (codiceSpedizione != null) {
			setCodiceSpedizione(codiceSpedizione);
		}

		String codicePorto = (String)attributes.get("codicePorto");

		if (codicePorto != null) {
			setCodicePorto(codicePorto);
		}

		String codiceDestinatario = (String)attributes.get("codiceDestinatario");

		if (codiceDestinatario != null) {
			setCodiceDestinatario(codiceDestinatario);
		}

		String codiceListino = (String)attributes.get("codiceListino");

		if (codiceListino != null) {
			setCodiceListino(codiceListino);
		}

		String codiceLingua = (String)attributes.get("codiceLingua");

		if (codiceLingua != null) {
			setCodiceLingua(codiceLingua);
		}

		Integer numeroDecPrezzo = (Integer)attributes.get("numeroDecPrezzo");

		if (numeroDecPrezzo != null) {
			setNumeroDecPrezzo(numeroDecPrezzo);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}

		Double percentualeSpeseTrasp = (Double)attributes.get(
				"percentualeSpeseTrasp");

		if (percentualeSpeseTrasp != null) {
			setPercentualeSpeseTrasp(percentualeSpeseTrasp);
		}

		Double speseTrasporto = (Double)attributes.get("speseTrasporto");

		if (speseTrasporto != null) {
			setSpeseTrasporto(speseTrasporto);
		}

		Double speseImballaggio = (Double)attributes.get("speseImballaggio");

		if (speseImballaggio != null) {
			setSpeseImballaggio(speseImballaggio);
		}

		Double speseVarie = (Double)attributes.get("speseVarie");

		if (speseVarie != null) {
			setSpeseVarie(speseVarie);
		}

		Double speseBanca = (Double)attributes.get("speseBanca");

		if (speseBanca != null) {
			setSpeseBanca(speseBanca);
		}

		String curaTrasporto = (String)attributes.get("curaTrasporto");

		if (curaTrasporto != null) {
			setCuraTrasporto(curaTrasporto);
		}

		String causaleTrasporto = (String)attributes.get("causaleTrasporto");

		if (causaleTrasporto != null) {
			setCausaleTrasporto(causaleTrasporto);
		}

		String aspettoEstriore = (String)attributes.get("aspettoEstriore");

		if (aspettoEstriore != null) {
			setAspettoEstriore(aspettoEstriore);
		}

		String vettore1 = (String)attributes.get("vettore1");

		if (vettore1 != null) {
			setVettore1(vettore1);
		}

		String vettore2 = (String)attributes.get("vettore2");

		if (vettore2 != null) {
			setVettore2(vettore2);
		}

		String vettore3 = (String)attributes.get("vettore3");

		if (vettore3 != null) {
			setVettore3(vettore3);
		}

		Integer numeroColli = (Integer)attributes.get("numeroColli");

		if (numeroColli != null) {
			setNumeroColli(numeroColli);
		}

		Double pesoLordo = (Double)attributes.get("pesoLordo");

		if (pesoLordo != null) {
			setPesoLordo(pesoLordo);
		}

		Double pesoNetto = (Double)attributes.get("pesoNetto");

		if (pesoNetto != null) {
			setPesoNetto(pesoNetto);
		}

		Double volume = (Double)attributes.get("volume");

		if (volume != null) {
			setVolume(volume);
		}

		Integer numeroCopie = (Integer)attributes.get("numeroCopie");

		if (numeroCopie != null) {
			setNumeroCopie(numeroCopie);
		}

		Integer numeroCopieStampate = (Integer)attributes.get(
				"numeroCopieStampate");

		if (numeroCopieStampate != null) {
			setNumeroCopieStampate(numeroCopieStampate);
		}

		Boolean inviatoEmail = (Boolean)attributes.get("inviatoEmail");

		if (inviatoEmail != null) {
			setInviatoEmail(inviatoEmail);
		}

		String nomePDF = (String)attributes.get("nomePDF");

		if (nomePDF != null) {
			setNomePDF(nomePDF);
		}

		String riferimentoOrdine = (String)attributes.get("riferimentoOrdine");

		if (riferimentoOrdine != null) {
			setRiferimentoOrdine(riferimentoOrdine);
		}

		Date dataConferma = (Date)attributes.get("dataConferma");

		if (dataConferma != null) {
			setDataConferma(dataConferma);
		}

		Boolean confermaStampata = (Boolean)attributes.get("confermaStampata");

		if (confermaStampata != null) {
			setConfermaStampata(confermaStampata);
		}

		Double totaleOrdine = (Double)attributes.get("totaleOrdine");

		if (totaleOrdine != null) {
			setTotaleOrdine(totaleOrdine);
		}

		String codiceIVATrasp = (String)attributes.get("codiceIVATrasp");

		if (codiceIVATrasp != null) {
			setCodiceIVATrasp(codiceIVATrasp);
		}

		String codiceIVAImballo = (String)attributes.get("codiceIVAImballo");

		if (codiceIVAImballo != null) {
			setCodiceIVAImballo(codiceIVAImballo);
		}

		String codiceIVAVarie = (String)attributes.get("codiceIVAVarie");

		if (codiceIVAVarie != null) {
			setCodiceIVAVarie(codiceIVAVarie);
		}

		String codiceIVABanca = (String)attributes.get("codiceIVABanca");

		if (codiceIVABanca != null) {
			setCodiceIVABanca(codiceIVABanca);
		}

		String libStr1 = (String)attributes.get("libStr1");

		if (libStr1 != null) {
			setLibStr1(libStr1);
		}

		String libStr2 = (String)attributes.get("libStr2");

		if (libStr2 != null) {
			setLibStr2(libStr2);
		}

		String libStr3 = (String)attributes.get("libStr3");

		if (libStr3 != null) {
			setLibStr3(libStr3);
		}

		Double libDbl1 = (Double)attributes.get("libDbl1");

		if (libDbl1 != null) {
			setLibDbl1(libDbl1);
		}

		Double libDbl2 = (Double)attributes.get("libDbl2");

		if (libDbl2 != null) {
			setLibDbl2(libDbl2);
		}

		Double libDbl3 = (Double)attributes.get("libDbl3");

		if (libDbl3 != null) {
			setLibDbl3(libDbl3);
		}

		Date libDat1 = (Date)attributes.get("libDat1");

		if (libDat1 != null) {
			setLibDat1(libDat1);
		}

		Date libDat2 = (Date)attributes.get("libDat2");

		if (libDat2 != null) {
			setLibDat2(libDat2);
		}

		Date libDat3 = (Date)attributes.get("libDat3");

		if (libDat3 != null) {
			setLibDat3(libDat3);
		}

		Long libLng1 = (Long)attributes.get("libLng1");

		if (libLng1 != null) {
			setLibLng1(libLng1);
		}

		Long libLng2 = (Long)attributes.get("libLng2");

		if (libLng2 != null) {
			setLibLng2(libLng2);
		}

		Long libLng3 = (Long)attributes.get("libLng3");

		if (libLng3 != null) {
			setLibLng3(libLng3);
		}
	}

	/**
	* Returns the primary key of this ordini clienti.
	*
	* @return the primary key of this ordini clienti
	*/
	@Override
	public it.bysoftware.ct.service.persistence.OrdiniClientiPK getPrimaryKey() {
		return _ordiniClienti.getPrimaryKey();
	}

	/**
	* Sets the primary key of this ordini clienti.
	*
	* @param primaryKey the primary key of this ordini clienti
	*/
	@Override
	public void setPrimaryKey(
		it.bysoftware.ct.service.persistence.OrdiniClientiPK primaryKey) {
		_ordiniClienti.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the anno of this ordini clienti.
	*
	* @return the anno of this ordini clienti
	*/
	@Override
	public int getAnno() {
		return _ordiniClienti.getAnno();
	}

	/**
	* Sets the anno of this ordini clienti.
	*
	* @param anno the anno of this ordini clienti
	*/
	@Override
	public void setAnno(int anno) {
		_ordiniClienti.setAnno(anno);
	}

	/**
	* Returns the codice attivita of this ordini clienti.
	*
	* @return the codice attivita of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceAttivita() {
		return _ordiniClienti.getCodiceAttivita();
	}

	/**
	* Sets the codice attivita of this ordini clienti.
	*
	* @param codiceAttivita the codice attivita of this ordini clienti
	*/
	@Override
	public void setCodiceAttivita(java.lang.String codiceAttivita) {
		_ordiniClienti.setCodiceAttivita(codiceAttivita);
	}

	/**
	* Returns the codice centro of this ordini clienti.
	*
	* @return the codice centro of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceCentro() {
		return _ordiniClienti.getCodiceCentro();
	}

	/**
	* Sets the codice centro of this ordini clienti.
	*
	* @param codiceCentro the codice centro of this ordini clienti
	*/
	@Override
	public void setCodiceCentro(java.lang.String codiceCentro) {
		_ordiniClienti.setCodiceCentro(codiceCentro);
	}

	/**
	* Returns the codice deposito of this ordini clienti.
	*
	* @return the codice deposito of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceDeposito() {
		return _ordiniClienti.getCodiceDeposito();
	}

	/**
	* Sets the codice deposito of this ordini clienti.
	*
	* @param codiceDeposito the codice deposito of this ordini clienti
	*/
	@Override
	public void setCodiceDeposito(java.lang.String codiceDeposito) {
		_ordiniClienti.setCodiceDeposito(codiceDeposito);
	}

	/**
	* Returns the tipo ordine of this ordini clienti.
	*
	* @return the tipo ordine of this ordini clienti
	*/
	@Override
	public int getTipoOrdine() {
		return _ordiniClienti.getTipoOrdine();
	}

	/**
	* Sets the tipo ordine of this ordini clienti.
	*
	* @param tipoOrdine the tipo ordine of this ordini clienti
	*/
	@Override
	public void setTipoOrdine(int tipoOrdine) {
		_ordiniClienti.setTipoOrdine(tipoOrdine);
	}

	/**
	* Returns the numero ordine of this ordini clienti.
	*
	* @return the numero ordine of this ordini clienti
	*/
	@Override
	public int getNumeroOrdine() {
		return _ordiniClienti.getNumeroOrdine();
	}

	/**
	* Sets the numero ordine of this ordini clienti.
	*
	* @param numeroOrdine the numero ordine of this ordini clienti
	*/
	@Override
	public void setNumeroOrdine(int numeroOrdine) {
		_ordiniClienti.setNumeroOrdine(numeroOrdine);
	}

	/**
	* Returns the tipo documento of this ordini clienti.
	*
	* @return the tipo documento of this ordini clienti
	*/
	@Override
	public java.lang.String getTipoDocumento() {
		return _ordiniClienti.getTipoDocumento();
	}

	/**
	* Sets the tipo documento of this ordini clienti.
	*
	* @param tipoDocumento the tipo documento of this ordini clienti
	*/
	@Override
	public void setTipoDocumento(java.lang.String tipoDocumento) {
		_ordiniClienti.setTipoDocumento(tipoDocumento);
	}

	/**
	* Returns the codice cont analitica of this ordini clienti.
	*
	* @return the codice cont analitica of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceContAnalitica() {
		return _ordiniClienti.getCodiceContAnalitica();
	}

	/**
	* Sets the codice cont analitica of this ordini clienti.
	*
	* @param codiceContAnalitica the codice cont analitica of this ordini clienti
	*/
	@Override
	public void setCodiceContAnalitica(java.lang.String codiceContAnalitica) {
		_ordiniClienti.setCodiceContAnalitica(codiceContAnalitica);
	}

	/**
	* Returns the id tipo documento of this ordini clienti.
	*
	* @return the id tipo documento of this ordini clienti
	*/
	@Override
	public int getIdTipoDocumento() {
		return _ordiniClienti.getIdTipoDocumento();
	}

	/**
	* Sets the id tipo documento of this ordini clienti.
	*
	* @param idTipoDocumento the id tipo documento of this ordini clienti
	*/
	@Override
	public void setIdTipoDocumento(int idTipoDocumento) {
		_ordiniClienti.setIdTipoDocumento(idTipoDocumento);
	}

	/**
	* Returns the stato ordine of this ordini clienti.
	*
	* @return the stato ordine of this ordini clienti
	*/
	@Override
	public boolean getStatoOrdine() {
		return _ordiniClienti.getStatoOrdine();
	}

	/**
	* Returns <code>true</code> if this ordini clienti is stato ordine.
	*
	* @return <code>true</code> if this ordini clienti is stato ordine; <code>false</code> otherwise
	*/
	@Override
	public boolean isStatoOrdine() {
		return _ordiniClienti.isStatoOrdine();
	}

	/**
	* Sets whether this ordini clienti is stato ordine.
	*
	* @param statoOrdine the stato ordine of this ordini clienti
	*/
	@Override
	public void setStatoOrdine(boolean statoOrdine) {
		_ordiniClienti.setStatoOrdine(statoOrdine);
	}

	/**
	* Returns the descr estremi doc of this ordini clienti.
	*
	* @return the descr estremi doc of this ordini clienti
	*/
	@Override
	public java.lang.String getDescrEstremiDoc() {
		return _ordiniClienti.getDescrEstremiDoc();
	}

	/**
	* Sets the descr estremi doc of this ordini clienti.
	*
	* @param descrEstremiDoc the descr estremi doc of this ordini clienti
	*/
	@Override
	public void setDescrEstremiDoc(java.lang.String descrEstremiDoc) {
		_ordiniClienti.setDescrEstremiDoc(descrEstremiDoc);
	}

	/**
	* Returns the tipo soggetto of this ordini clienti.
	*
	* @return the tipo soggetto of this ordini clienti
	*/
	@Override
	public boolean getTipoSoggetto() {
		return _ordiniClienti.getTipoSoggetto();
	}

	/**
	* Returns <code>true</code> if this ordini clienti is tipo soggetto.
	*
	* @return <code>true</code> if this ordini clienti is tipo soggetto; <code>false</code> otherwise
	*/
	@Override
	public boolean isTipoSoggetto() {
		return _ordiniClienti.isTipoSoggetto();
	}

	/**
	* Sets whether this ordini clienti is tipo soggetto.
	*
	* @param tipoSoggetto the tipo soggetto of this ordini clienti
	*/
	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_ordiniClienti.setTipoSoggetto(tipoSoggetto);
	}

	/**
	* Returns the codice cliente of this ordini clienti.
	*
	* @return the codice cliente of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceCliente() {
		return _ordiniClienti.getCodiceCliente();
	}

	/**
	* Sets the codice cliente of this ordini clienti.
	*
	* @param codiceCliente the codice cliente of this ordini clienti
	*/
	@Override
	public void setCodiceCliente(java.lang.String codiceCliente) {
		_ordiniClienti.setCodiceCliente(codiceCliente);
	}

	/**
	* Returns the natura transazione of this ordini clienti.
	*
	* @return the natura transazione of this ordini clienti
	*/
	@Override
	public java.lang.String getNaturaTransazione() {
		return _ordiniClienti.getNaturaTransazione();
	}

	/**
	* Sets the natura transazione of this ordini clienti.
	*
	* @param naturaTransazione the natura transazione of this ordini clienti
	*/
	@Override
	public void setNaturaTransazione(java.lang.String naturaTransazione) {
		_ordiniClienti.setNaturaTransazione(naturaTransazione);
	}

	/**
	* Returns the codice esenzione of this ordini clienti.
	*
	* @return the codice esenzione of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceEsenzione() {
		return _ordiniClienti.getCodiceEsenzione();
	}

	/**
	* Sets the codice esenzione of this ordini clienti.
	*
	* @param codiceEsenzione the codice esenzione of this ordini clienti
	*/
	@Override
	public void setCodiceEsenzione(java.lang.String codiceEsenzione) {
		_ordiniClienti.setCodiceEsenzione(codiceEsenzione);
	}

	/**
	* Returns the codice divisa of this ordini clienti.
	*
	* @return the codice divisa of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceDivisa() {
		return _ordiniClienti.getCodiceDivisa();
	}

	/**
	* Sets the codice divisa of this ordini clienti.
	*
	* @param codiceDivisa the codice divisa of this ordini clienti
	*/
	@Override
	public void setCodiceDivisa(java.lang.String codiceDivisa) {
		_ordiniClienti.setCodiceDivisa(codiceDivisa);
	}

	/**
	* Returns the valore cambio of this ordini clienti.
	*
	* @return the valore cambio of this ordini clienti
	*/
	@Override
	public double getValoreCambio() {
		return _ordiniClienti.getValoreCambio();
	}

	/**
	* Sets the valore cambio of this ordini clienti.
	*
	* @param valoreCambio the valore cambio of this ordini clienti
	*/
	@Override
	public void setValoreCambio(double valoreCambio) {
		_ordiniClienti.setValoreCambio(valoreCambio);
	}

	/**
	* Returns the data valore cambio of this ordini clienti.
	*
	* @return the data valore cambio of this ordini clienti
	*/
	@Override
	public java.util.Date getDataValoreCambio() {
		return _ordiniClienti.getDataValoreCambio();
	}

	/**
	* Sets the data valore cambio of this ordini clienti.
	*
	* @param dataValoreCambio the data valore cambio of this ordini clienti
	*/
	@Override
	public void setDataValoreCambio(java.util.Date dataValoreCambio) {
		_ordiniClienti.setDataValoreCambio(dataValoreCambio);
	}

	/**
	* Returns the codice piano pag of this ordini clienti.
	*
	* @return the codice piano pag of this ordini clienti
	*/
	@Override
	public java.lang.String getCodicePianoPag() {
		return _ordiniClienti.getCodicePianoPag();
	}

	/**
	* Sets the codice piano pag of this ordini clienti.
	*
	* @param codicePianoPag the codice piano pag of this ordini clienti
	*/
	@Override
	public void setCodicePianoPag(java.lang.String codicePianoPag) {
		_ordiniClienti.setCodicePianoPag(codicePianoPag);
	}

	/**
	* Returns the inizio calcolo pag of this ordini clienti.
	*
	* @return the inizio calcolo pag of this ordini clienti
	*/
	@Override
	public java.util.Date getInizioCalcoloPag() {
		return _ordiniClienti.getInizioCalcoloPag();
	}

	/**
	* Sets the inizio calcolo pag of this ordini clienti.
	*
	* @param inizioCalcoloPag the inizio calcolo pag of this ordini clienti
	*/
	@Override
	public void setInizioCalcoloPag(java.util.Date inizioCalcoloPag) {
		_ordiniClienti.setInizioCalcoloPag(inizioCalcoloPag);
	}

	/**
	* Returns the percentuale sconto maggiorazione of this ordini clienti.
	*
	* @return the percentuale sconto maggiorazione of this ordini clienti
	*/
	@Override
	public double getPercentualeScontoMaggiorazione() {
		return _ordiniClienti.getPercentualeScontoMaggiorazione();
	}

	/**
	* Sets the percentuale sconto maggiorazione of this ordini clienti.
	*
	* @param percentualeScontoMaggiorazione the percentuale sconto maggiorazione of this ordini clienti
	*/
	@Override
	public void setPercentualeScontoMaggiorazione(
		double percentualeScontoMaggiorazione) {
		_ordiniClienti.setPercentualeScontoMaggiorazione(percentualeScontoMaggiorazione);
	}

	/**
	* Returns the percentuale sconto pronta cassa of this ordini clienti.
	*
	* @return the percentuale sconto pronta cassa of this ordini clienti
	*/
	@Override
	public double getPercentualeScontoProntaCassa() {
		return _ordiniClienti.getPercentualeScontoProntaCassa();
	}

	/**
	* Sets the percentuale sconto pronta cassa of this ordini clienti.
	*
	* @param percentualeScontoProntaCassa the percentuale sconto pronta cassa of this ordini clienti
	*/
	@Override
	public void setPercentualeScontoProntaCassa(
		double percentualeScontoProntaCassa) {
		_ordiniClienti.setPercentualeScontoProntaCassa(percentualeScontoProntaCassa);
	}

	/**
	* Returns the percentuale provv chiusura of this ordini clienti.
	*
	* @return the percentuale provv chiusura of this ordini clienti
	*/
	@Override
	public double getPercentualeProvvChiusura() {
		return _ordiniClienti.getPercentualeProvvChiusura();
	}

	/**
	* Sets the percentuale provv chiusura of this ordini clienti.
	*
	* @param percentualeProvvChiusura the percentuale provv chiusura of this ordini clienti
	*/
	@Override
	public void setPercentualeProvvChiusura(double percentualeProvvChiusura) {
		_ordiniClienti.setPercentualeProvvChiusura(percentualeProvvChiusura);
	}

	/**
	* Returns the data documento of this ordini clienti.
	*
	* @return the data documento of this ordini clienti
	*/
	@Override
	public java.util.Date getDataDocumento() {
		return _ordiniClienti.getDataDocumento();
	}

	/**
	* Sets the data documento of this ordini clienti.
	*
	* @param dataDocumento the data documento of this ordini clienti
	*/
	@Override
	public void setDataDocumento(java.util.Date dataDocumento) {
		_ordiniClienti.setDataDocumento(dataDocumento);
	}

	/**
	* Returns the data registrazione of this ordini clienti.
	*
	* @return the data registrazione of this ordini clienti
	*/
	@Override
	public java.util.Date getDataRegistrazione() {
		return _ordiniClienti.getDataRegistrazione();
	}

	/**
	* Sets the data registrazione of this ordini clienti.
	*
	* @param dataRegistrazione the data registrazione of this ordini clienti
	*/
	@Override
	public void setDataRegistrazione(java.util.Date dataRegistrazione) {
		_ordiniClienti.setDataRegistrazione(dataRegistrazione);
	}

	/**
	* Returns the causale estratto conto of this ordini clienti.
	*
	* @return the causale estratto conto of this ordini clienti
	*/
	@Override
	public java.lang.String getCausaleEstrattoConto() {
		return _ordiniClienti.getCausaleEstrattoConto();
	}

	/**
	* Sets the causale estratto conto of this ordini clienti.
	*
	* @param causaleEstrattoConto the causale estratto conto of this ordini clienti
	*/
	@Override
	public void setCausaleEstrattoConto(java.lang.String causaleEstrattoConto) {
		_ordiniClienti.setCausaleEstrattoConto(causaleEstrattoConto);
	}

	/**
	* Returns the data prima rata of this ordini clienti.
	*
	* @return the data prima rata of this ordini clienti
	*/
	@Override
	public java.util.Date getDataPrimaRata() {
		return _ordiniClienti.getDataPrimaRata();
	}

	/**
	* Sets the data prima rata of this ordini clienti.
	*
	* @param dataPrimaRata the data prima rata of this ordini clienti
	*/
	@Override
	public void setDataPrimaRata(java.util.Date dataPrimaRata) {
		_ordiniClienti.setDataPrimaRata(dataPrimaRata);
	}

	/**
	* Returns the data ultima rata of this ordini clienti.
	*
	* @return the data ultima rata of this ordini clienti
	*/
	@Override
	public java.util.Date getDataUltimaRata() {
		return _ordiniClienti.getDataUltimaRata();
	}

	/**
	* Sets the data ultima rata of this ordini clienti.
	*
	* @param dataUltimaRata the data ultima rata of this ordini clienti
	*/
	@Override
	public void setDataUltimaRata(java.util.Date dataUltimaRata) {
		_ordiniClienti.setDataUltimaRata(dataUltimaRata);
	}

	/**
	* Returns the codice banca of this ordini clienti.
	*
	* @return the codice banca of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceBanca() {
		return _ordiniClienti.getCodiceBanca();
	}

	/**
	* Sets the codice banca of this ordini clienti.
	*
	* @param codiceBanca the codice banca of this ordini clienti
	*/
	@Override
	public void setCodiceBanca(java.lang.String codiceBanca) {
		_ordiniClienti.setCodiceBanca(codiceBanca);
	}

	/**
	* Returns the codice agenzia of this ordini clienti.
	*
	* @return the codice agenzia of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceAgenzia() {
		return _ordiniClienti.getCodiceAgenzia();
	}

	/**
	* Sets the codice agenzia of this ordini clienti.
	*
	* @param codiceAgenzia the codice agenzia of this ordini clienti
	*/
	@Override
	public void setCodiceAgenzia(java.lang.String codiceAgenzia) {
		_ordiniClienti.setCodiceAgenzia(codiceAgenzia);
	}

	/**
	* Returns the codice agente of this ordini clienti.
	*
	* @return the codice agente of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceAgente() {
		return _ordiniClienti.getCodiceAgente();
	}

	/**
	* Sets the codice agente of this ordini clienti.
	*
	* @param codiceAgente the codice agente of this ordini clienti
	*/
	@Override
	public void setCodiceAgente(java.lang.String codiceAgente) {
		_ordiniClienti.setCodiceAgente(codiceAgente);
	}

	/**
	* Returns the codice gruppo agenti of this ordini clienti.
	*
	* @return the codice gruppo agenti of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceGruppoAgenti() {
		return _ordiniClienti.getCodiceGruppoAgenti();
	}

	/**
	* Sets the codice gruppo agenti of this ordini clienti.
	*
	* @param codiceGruppoAgenti the codice gruppo agenti of this ordini clienti
	*/
	@Override
	public void setCodiceGruppoAgenti(java.lang.String codiceGruppoAgenti) {
		_ordiniClienti.setCodiceGruppoAgenti(codiceGruppoAgenti);
	}

	/**
	* Returns the codice zona of this ordini clienti.
	*
	* @return the codice zona of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceZona() {
		return _ordiniClienti.getCodiceZona();
	}

	/**
	* Sets the codice zona of this ordini clienti.
	*
	* @param codiceZona the codice zona of this ordini clienti
	*/
	@Override
	public void setCodiceZona(java.lang.String codiceZona) {
		_ordiniClienti.setCodiceZona(codiceZona);
	}

	/**
	* Returns the codice spedizione of this ordini clienti.
	*
	* @return the codice spedizione of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceSpedizione() {
		return _ordiniClienti.getCodiceSpedizione();
	}

	/**
	* Sets the codice spedizione of this ordini clienti.
	*
	* @param codiceSpedizione the codice spedizione of this ordini clienti
	*/
	@Override
	public void setCodiceSpedizione(java.lang.String codiceSpedizione) {
		_ordiniClienti.setCodiceSpedizione(codiceSpedizione);
	}

	/**
	* Returns the codice porto of this ordini clienti.
	*
	* @return the codice porto of this ordini clienti
	*/
	@Override
	public java.lang.String getCodicePorto() {
		return _ordiniClienti.getCodicePorto();
	}

	/**
	* Sets the codice porto of this ordini clienti.
	*
	* @param codicePorto the codice porto of this ordini clienti
	*/
	@Override
	public void setCodicePorto(java.lang.String codicePorto) {
		_ordiniClienti.setCodicePorto(codicePorto);
	}

	/**
	* Returns the codice destinatario of this ordini clienti.
	*
	* @return the codice destinatario of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceDestinatario() {
		return _ordiniClienti.getCodiceDestinatario();
	}

	/**
	* Sets the codice destinatario of this ordini clienti.
	*
	* @param codiceDestinatario the codice destinatario of this ordini clienti
	*/
	@Override
	public void setCodiceDestinatario(java.lang.String codiceDestinatario) {
		_ordiniClienti.setCodiceDestinatario(codiceDestinatario);
	}

	/**
	* Returns the codice listino of this ordini clienti.
	*
	* @return the codice listino of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceListino() {
		return _ordiniClienti.getCodiceListino();
	}

	/**
	* Sets the codice listino of this ordini clienti.
	*
	* @param codiceListino the codice listino of this ordini clienti
	*/
	@Override
	public void setCodiceListino(java.lang.String codiceListino) {
		_ordiniClienti.setCodiceListino(codiceListino);
	}

	/**
	* Returns the codice lingua of this ordini clienti.
	*
	* @return the codice lingua of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceLingua() {
		return _ordiniClienti.getCodiceLingua();
	}

	/**
	* Sets the codice lingua of this ordini clienti.
	*
	* @param codiceLingua the codice lingua of this ordini clienti
	*/
	@Override
	public void setCodiceLingua(java.lang.String codiceLingua) {
		_ordiniClienti.setCodiceLingua(codiceLingua);
	}

	/**
	* Returns the numero dec prezzo of this ordini clienti.
	*
	* @return the numero dec prezzo of this ordini clienti
	*/
	@Override
	public int getNumeroDecPrezzo() {
		return _ordiniClienti.getNumeroDecPrezzo();
	}

	/**
	* Sets the numero dec prezzo of this ordini clienti.
	*
	* @param numeroDecPrezzo the numero dec prezzo of this ordini clienti
	*/
	@Override
	public void setNumeroDecPrezzo(int numeroDecPrezzo) {
		_ordiniClienti.setNumeroDecPrezzo(numeroDecPrezzo);
	}

	/**
	* Returns the note of this ordini clienti.
	*
	* @return the note of this ordini clienti
	*/
	@Override
	public java.lang.String getNote() {
		return _ordiniClienti.getNote();
	}

	/**
	* Sets the note of this ordini clienti.
	*
	* @param note the note of this ordini clienti
	*/
	@Override
	public void setNote(java.lang.String note) {
		_ordiniClienti.setNote(note);
	}

	/**
	* Returns the percentuale spese trasp of this ordini clienti.
	*
	* @return the percentuale spese trasp of this ordini clienti
	*/
	@Override
	public double getPercentualeSpeseTrasp() {
		return _ordiniClienti.getPercentualeSpeseTrasp();
	}

	/**
	* Sets the percentuale spese trasp of this ordini clienti.
	*
	* @param percentualeSpeseTrasp the percentuale spese trasp of this ordini clienti
	*/
	@Override
	public void setPercentualeSpeseTrasp(double percentualeSpeseTrasp) {
		_ordiniClienti.setPercentualeSpeseTrasp(percentualeSpeseTrasp);
	}

	/**
	* Returns the spese trasporto of this ordini clienti.
	*
	* @return the spese trasporto of this ordini clienti
	*/
	@Override
	public double getSpeseTrasporto() {
		return _ordiniClienti.getSpeseTrasporto();
	}

	/**
	* Sets the spese trasporto of this ordini clienti.
	*
	* @param speseTrasporto the spese trasporto of this ordini clienti
	*/
	@Override
	public void setSpeseTrasporto(double speseTrasporto) {
		_ordiniClienti.setSpeseTrasporto(speseTrasporto);
	}

	/**
	* Returns the spese imballaggio of this ordini clienti.
	*
	* @return the spese imballaggio of this ordini clienti
	*/
	@Override
	public double getSpeseImballaggio() {
		return _ordiniClienti.getSpeseImballaggio();
	}

	/**
	* Sets the spese imballaggio of this ordini clienti.
	*
	* @param speseImballaggio the spese imballaggio of this ordini clienti
	*/
	@Override
	public void setSpeseImballaggio(double speseImballaggio) {
		_ordiniClienti.setSpeseImballaggio(speseImballaggio);
	}

	/**
	* Returns the spese varie of this ordini clienti.
	*
	* @return the spese varie of this ordini clienti
	*/
	@Override
	public double getSpeseVarie() {
		return _ordiniClienti.getSpeseVarie();
	}

	/**
	* Sets the spese varie of this ordini clienti.
	*
	* @param speseVarie the spese varie of this ordini clienti
	*/
	@Override
	public void setSpeseVarie(double speseVarie) {
		_ordiniClienti.setSpeseVarie(speseVarie);
	}

	/**
	* Returns the spese banca of this ordini clienti.
	*
	* @return the spese banca of this ordini clienti
	*/
	@Override
	public double getSpeseBanca() {
		return _ordiniClienti.getSpeseBanca();
	}

	/**
	* Sets the spese banca of this ordini clienti.
	*
	* @param speseBanca the spese banca of this ordini clienti
	*/
	@Override
	public void setSpeseBanca(double speseBanca) {
		_ordiniClienti.setSpeseBanca(speseBanca);
	}

	/**
	* Returns the cura trasporto of this ordini clienti.
	*
	* @return the cura trasporto of this ordini clienti
	*/
	@Override
	public java.lang.String getCuraTrasporto() {
		return _ordiniClienti.getCuraTrasporto();
	}

	/**
	* Sets the cura trasporto of this ordini clienti.
	*
	* @param curaTrasporto the cura trasporto of this ordini clienti
	*/
	@Override
	public void setCuraTrasporto(java.lang.String curaTrasporto) {
		_ordiniClienti.setCuraTrasporto(curaTrasporto);
	}

	/**
	* Returns the causale trasporto of this ordini clienti.
	*
	* @return the causale trasporto of this ordini clienti
	*/
	@Override
	public java.lang.String getCausaleTrasporto() {
		return _ordiniClienti.getCausaleTrasporto();
	}

	/**
	* Sets the causale trasporto of this ordini clienti.
	*
	* @param causaleTrasporto the causale trasporto of this ordini clienti
	*/
	@Override
	public void setCausaleTrasporto(java.lang.String causaleTrasporto) {
		_ordiniClienti.setCausaleTrasporto(causaleTrasporto);
	}

	/**
	* Returns the aspetto estriore of this ordini clienti.
	*
	* @return the aspetto estriore of this ordini clienti
	*/
	@Override
	public java.lang.String getAspettoEstriore() {
		return _ordiniClienti.getAspettoEstriore();
	}

	/**
	* Sets the aspetto estriore of this ordini clienti.
	*
	* @param aspettoEstriore the aspetto estriore of this ordini clienti
	*/
	@Override
	public void setAspettoEstriore(java.lang.String aspettoEstriore) {
		_ordiniClienti.setAspettoEstriore(aspettoEstriore);
	}

	/**
	* Returns the vettore1 of this ordini clienti.
	*
	* @return the vettore1 of this ordini clienti
	*/
	@Override
	public java.lang.String getVettore1() {
		return _ordiniClienti.getVettore1();
	}

	/**
	* Sets the vettore1 of this ordini clienti.
	*
	* @param vettore1 the vettore1 of this ordini clienti
	*/
	@Override
	public void setVettore1(java.lang.String vettore1) {
		_ordiniClienti.setVettore1(vettore1);
	}

	/**
	* Returns the vettore2 of this ordini clienti.
	*
	* @return the vettore2 of this ordini clienti
	*/
	@Override
	public java.lang.String getVettore2() {
		return _ordiniClienti.getVettore2();
	}

	/**
	* Sets the vettore2 of this ordini clienti.
	*
	* @param vettore2 the vettore2 of this ordini clienti
	*/
	@Override
	public void setVettore2(java.lang.String vettore2) {
		_ordiniClienti.setVettore2(vettore2);
	}

	/**
	* Returns the vettore3 of this ordini clienti.
	*
	* @return the vettore3 of this ordini clienti
	*/
	@Override
	public java.lang.String getVettore3() {
		return _ordiniClienti.getVettore3();
	}

	/**
	* Sets the vettore3 of this ordini clienti.
	*
	* @param vettore3 the vettore3 of this ordini clienti
	*/
	@Override
	public void setVettore3(java.lang.String vettore3) {
		_ordiniClienti.setVettore3(vettore3);
	}

	/**
	* Returns the numero colli of this ordini clienti.
	*
	* @return the numero colli of this ordini clienti
	*/
	@Override
	public int getNumeroColli() {
		return _ordiniClienti.getNumeroColli();
	}

	/**
	* Sets the numero colli of this ordini clienti.
	*
	* @param numeroColli the numero colli of this ordini clienti
	*/
	@Override
	public void setNumeroColli(int numeroColli) {
		_ordiniClienti.setNumeroColli(numeroColli);
	}

	/**
	* Returns the peso lordo of this ordini clienti.
	*
	* @return the peso lordo of this ordini clienti
	*/
	@Override
	public double getPesoLordo() {
		return _ordiniClienti.getPesoLordo();
	}

	/**
	* Sets the peso lordo of this ordini clienti.
	*
	* @param pesoLordo the peso lordo of this ordini clienti
	*/
	@Override
	public void setPesoLordo(double pesoLordo) {
		_ordiniClienti.setPesoLordo(pesoLordo);
	}

	/**
	* Returns the peso netto of this ordini clienti.
	*
	* @return the peso netto of this ordini clienti
	*/
	@Override
	public double getPesoNetto() {
		return _ordiniClienti.getPesoNetto();
	}

	/**
	* Sets the peso netto of this ordini clienti.
	*
	* @param pesoNetto the peso netto of this ordini clienti
	*/
	@Override
	public void setPesoNetto(double pesoNetto) {
		_ordiniClienti.setPesoNetto(pesoNetto);
	}

	/**
	* Returns the volume of this ordini clienti.
	*
	* @return the volume of this ordini clienti
	*/
	@Override
	public double getVolume() {
		return _ordiniClienti.getVolume();
	}

	/**
	* Sets the volume of this ordini clienti.
	*
	* @param volume the volume of this ordini clienti
	*/
	@Override
	public void setVolume(double volume) {
		_ordiniClienti.setVolume(volume);
	}

	/**
	* Returns the numero copie of this ordini clienti.
	*
	* @return the numero copie of this ordini clienti
	*/
	@Override
	public int getNumeroCopie() {
		return _ordiniClienti.getNumeroCopie();
	}

	/**
	* Sets the numero copie of this ordini clienti.
	*
	* @param numeroCopie the numero copie of this ordini clienti
	*/
	@Override
	public void setNumeroCopie(int numeroCopie) {
		_ordiniClienti.setNumeroCopie(numeroCopie);
	}

	/**
	* Returns the numero copie stampate of this ordini clienti.
	*
	* @return the numero copie stampate of this ordini clienti
	*/
	@Override
	public int getNumeroCopieStampate() {
		return _ordiniClienti.getNumeroCopieStampate();
	}

	/**
	* Sets the numero copie stampate of this ordini clienti.
	*
	* @param numeroCopieStampate the numero copie stampate of this ordini clienti
	*/
	@Override
	public void setNumeroCopieStampate(int numeroCopieStampate) {
		_ordiniClienti.setNumeroCopieStampate(numeroCopieStampate);
	}

	/**
	* Returns the inviato email of this ordini clienti.
	*
	* @return the inviato email of this ordini clienti
	*/
	@Override
	public boolean getInviatoEmail() {
		return _ordiniClienti.getInviatoEmail();
	}

	/**
	* Returns <code>true</code> if this ordini clienti is inviato email.
	*
	* @return <code>true</code> if this ordini clienti is inviato email; <code>false</code> otherwise
	*/
	@Override
	public boolean isInviatoEmail() {
		return _ordiniClienti.isInviatoEmail();
	}

	/**
	* Sets whether this ordini clienti is inviato email.
	*
	* @param inviatoEmail the inviato email of this ordini clienti
	*/
	@Override
	public void setInviatoEmail(boolean inviatoEmail) {
		_ordiniClienti.setInviatoEmail(inviatoEmail);
	}

	/**
	* Returns the nome p d f of this ordini clienti.
	*
	* @return the nome p d f of this ordini clienti
	*/
	@Override
	public java.lang.String getNomePDF() {
		return _ordiniClienti.getNomePDF();
	}

	/**
	* Sets the nome p d f of this ordini clienti.
	*
	* @param nomePDF the nome p d f of this ordini clienti
	*/
	@Override
	public void setNomePDF(java.lang.String nomePDF) {
		_ordiniClienti.setNomePDF(nomePDF);
	}

	/**
	* Returns the riferimento ordine of this ordini clienti.
	*
	* @return the riferimento ordine of this ordini clienti
	*/
	@Override
	public java.lang.String getRiferimentoOrdine() {
		return _ordiniClienti.getRiferimentoOrdine();
	}

	/**
	* Sets the riferimento ordine of this ordini clienti.
	*
	* @param riferimentoOrdine the riferimento ordine of this ordini clienti
	*/
	@Override
	public void setRiferimentoOrdine(java.lang.String riferimentoOrdine) {
		_ordiniClienti.setRiferimentoOrdine(riferimentoOrdine);
	}

	/**
	* Returns the data conferma of this ordini clienti.
	*
	* @return the data conferma of this ordini clienti
	*/
	@Override
	public java.util.Date getDataConferma() {
		return _ordiniClienti.getDataConferma();
	}

	/**
	* Sets the data conferma of this ordini clienti.
	*
	* @param dataConferma the data conferma of this ordini clienti
	*/
	@Override
	public void setDataConferma(java.util.Date dataConferma) {
		_ordiniClienti.setDataConferma(dataConferma);
	}

	/**
	* Returns the conferma stampata of this ordini clienti.
	*
	* @return the conferma stampata of this ordini clienti
	*/
	@Override
	public boolean getConfermaStampata() {
		return _ordiniClienti.getConfermaStampata();
	}

	/**
	* Returns <code>true</code> if this ordini clienti is conferma stampata.
	*
	* @return <code>true</code> if this ordini clienti is conferma stampata; <code>false</code> otherwise
	*/
	@Override
	public boolean isConfermaStampata() {
		return _ordiniClienti.isConfermaStampata();
	}

	/**
	* Sets whether this ordini clienti is conferma stampata.
	*
	* @param confermaStampata the conferma stampata of this ordini clienti
	*/
	@Override
	public void setConfermaStampata(boolean confermaStampata) {
		_ordiniClienti.setConfermaStampata(confermaStampata);
	}

	/**
	* Returns the totale ordine of this ordini clienti.
	*
	* @return the totale ordine of this ordini clienti
	*/
	@Override
	public double getTotaleOrdine() {
		return _ordiniClienti.getTotaleOrdine();
	}

	/**
	* Sets the totale ordine of this ordini clienti.
	*
	* @param totaleOrdine the totale ordine of this ordini clienti
	*/
	@Override
	public void setTotaleOrdine(double totaleOrdine) {
		_ordiniClienti.setTotaleOrdine(totaleOrdine);
	}

	/**
	* Returns the codice i v a trasp of this ordini clienti.
	*
	* @return the codice i v a trasp of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceIVATrasp() {
		return _ordiniClienti.getCodiceIVATrasp();
	}

	/**
	* Sets the codice i v a trasp of this ordini clienti.
	*
	* @param codiceIVATrasp the codice i v a trasp of this ordini clienti
	*/
	@Override
	public void setCodiceIVATrasp(java.lang.String codiceIVATrasp) {
		_ordiniClienti.setCodiceIVATrasp(codiceIVATrasp);
	}

	/**
	* Returns the codice i v a imballo of this ordini clienti.
	*
	* @return the codice i v a imballo of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceIVAImballo() {
		return _ordiniClienti.getCodiceIVAImballo();
	}

	/**
	* Sets the codice i v a imballo of this ordini clienti.
	*
	* @param codiceIVAImballo the codice i v a imballo of this ordini clienti
	*/
	@Override
	public void setCodiceIVAImballo(java.lang.String codiceIVAImballo) {
		_ordiniClienti.setCodiceIVAImballo(codiceIVAImballo);
	}

	/**
	* Returns the codice i v a varie of this ordini clienti.
	*
	* @return the codice i v a varie of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceIVAVarie() {
		return _ordiniClienti.getCodiceIVAVarie();
	}

	/**
	* Sets the codice i v a varie of this ordini clienti.
	*
	* @param codiceIVAVarie the codice i v a varie of this ordini clienti
	*/
	@Override
	public void setCodiceIVAVarie(java.lang.String codiceIVAVarie) {
		_ordiniClienti.setCodiceIVAVarie(codiceIVAVarie);
	}

	/**
	* Returns the codice i v a banca of this ordini clienti.
	*
	* @return the codice i v a banca of this ordini clienti
	*/
	@Override
	public java.lang.String getCodiceIVABanca() {
		return _ordiniClienti.getCodiceIVABanca();
	}

	/**
	* Sets the codice i v a banca of this ordini clienti.
	*
	* @param codiceIVABanca the codice i v a banca of this ordini clienti
	*/
	@Override
	public void setCodiceIVABanca(java.lang.String codiceIVABanca) {
		_ordiniClienti.setCodiceIVABanca(codiceIVABanca);
	}

	/**
	* Returns the lib str1 of this ordini clienti.
	*
	* @return the lib str1 of this ordini clienti
	*/
	@Override
	public java.lang.String getLibStr1() {
		return _ordiniClienti.getLibStr1();
	}

	/**
	* Sets the lib str1 of this ordini clienti.
	*
	* @param libStr1 the lib str1 of this ordini clienti
	*/
	@Override
	public void setLibStr1(java.lang.String libStr1) {
		_ordiniClienti.setLibStr1(libStr1);
	}

	/**
	* Returns the lib str2 of this ordini clienti.
	*
	* @return the lib str2 of this ordini clienti
	*/
	@Override
	public java.lang.String getLibStr2() {
		return _ordiniClienti.getLibStr2();
	}

	/**
	* Sets the lib str2 of this ordini clienti.
	*
	* @param libStr2 the lib str2 of this ordini clienti
	*/
	@Override
	public void setLibStr2(java.lang.String libStr2) {
		_ordiniClienti.setLibStr2(libStr2);
	}

	/**
	* Returns the lib str3 of this ordini clienti.
	*
	* @return the lib str3 of this ordini clienti
	*/
	@Override
	public java.lang.String getLibStr3() {
		return _ordiniClienti.getLibStr3();
	}

	/**
	* Sets the lib str3 of this ordini clienti.
	*
	* @param libStr3 the lib str3 of this ordini clienti
	*/
	@Override
	public void setLibStr3(java.lang.String libStr3) {
		_ordiniClienti.setLibStr3(libStr3);
	}

	/**
	* Returns the lib dbl1 of this ordini clienti.
	*
	* @return the lib dbl1 of this ordini clienti
	*/
	@Override
	public double getLibDbl1() {
		return _ordiniClienti.getLibDbl1();
	}

	/**
	* Sets the lib dbl1 of this ordini clienti.
	*
	* @param libDbl1 the lib dbl1 of this ordini clienti
	*/
	@Override
	public void setLibDbl1(double libDbl1) {
		_ordiniClienti.setLibDbl1(libDbl1);
	}

	/**
	* Returns the lib dbl2 of this ordini clienti.
	*
	* @return the lib dbl2 of this ordini clienti
	*/
	@Override
	public double getLibDbl2() {
		return _ordiniClienti.getLibDbl2();
	}

	/**
	* Sets the lib dbl2 of this ordini clienti.
	*
	* @param libDbl2 the lib dbl2 of this ordini clienti
	*/
	@Override
	public void setLibDbl2(double libDbl2) {
		_ordiniClienti.setLibDbl2(libDbl2);
	}

	/**
	* Returns the lib dbl3 of this ordini clienti.
	*
	* @return the lib dbl3 of this ordini clienti
	*/
	@Override
	public double getLibDbl3() {
		return _ordiniClienti.getLibDbl3();
	}

	/**
	* Sets the lib dbl3 of this ordini clienti.
	*
	* @param libDbl3 the lib dbl3 of this ordini clienti
	*/
	@Override
	public void setLibDbl3(double libDbl3) {
		_ordiniClienti.setLibDbl3(libDbl3);
	}

	/**
	* Returns the lib dat1 of this ordini clienti.
	*
	* @return the lib dat1 of this ordini clienti
	*/
	@Override
	public java.util.Date getLibDat1() {
		return _ordiniClienti.getLibDat1();
	}

	/**
	* Sets the lib dat1 of this ordini clienti.
	*
	* @param libDat1 the lib dat1 of this ordini clienti
	*/
	@Override
	public void setLibDat1(java.util.Date libDat1) {
		_ordiniClienti.setLibDat1(libDat1);
	}

	/**
	* Returns the lib dat2 of this ordini clienti.
	*
	* @return the lib dat2 of this ordini clienti
	*/
	@Override
	public java.util.Date getLibDat2() {
		return _ordiniClienti.getLibDat2();
	}

	/**
	* Sets the lib dat2 of this ordini clienti.
	*
	* @param libDat2 the lib dat2 of this ordini clienti
	*/
	@Override
	public void setLibDat2(java.util.Date libDat2) {
		_ordiniClienti.setLibDat2(libDat2);
	}

	/**
	* Returns the lib dat3 of this ordini clienti.
	*
	* @return the lib dat3 of this ordini clienti
	*/
	@Override
	public java.util.Date getLibDat3() {
		return _ordiniClienti.getLibDat3();
	}

	/**
	* Sets the lib dat3 of this ordini clienti.
	*
	* @param libDat3 the lib dat3 of this ordini clienti
	*/
	@Override
	public void setLibDat3(java.util.Date libDat3) {
		_ordiniClienti.setLibDat3(libDat3);
	}

	/**
	* Returns the lib lng1 of this ordini clienti.
	*
	* @return the lib lng1 of this ordini clienti
	*/
	@Override
	public long getLibLng1() {
		return _ordiniClienti.getLibLng1();
	}

	/**
	* Sets the lib lng1 of this ordini clienti.
	*
	* @param libLng1 the lib lng1 of this ordini clienti
	*/
	@Override
	public void setLibLng1(long libLng1) {
		_ordiniClienti.setLibLng1(libLng1);
	}

	/**
	* Returns the lib lng2 of this ordini clienti.
	*
	* @return the lib lng2 of this ordini clienti
	*/
	@Override
	public long getLibLng2() {
		return _ordiniClienti.getLibLng2();
	}

	/**
	* Sets the lib lng2 of this ordini clienti.
	*
	* @param libLng2 the lib lng2 of this ordini clienti
	*/
	@Override
	public void setLibLng2(long libLng2) {
		_ordiniClienti.setLibLng2(libLng2);
	}

	/**
	* Returns the lib lng3 of this ordini clienti.
	*
	* @return the lib lng3 of this ordini clienti
	*/
	@Override
	public long getLibLng3() {
		return _ordiniClienti.getLibLng3();
	}

	/**
	* Sets the lib lng3 of this ordini clienti.
	*
	* @param libLng3 the lib lng3 of this ordini clienti
	*/
	@Override
	public void setLibLng3(long libLng3) {
		_ordiniClienti.setLibLng3(libLng3);
	}

	@Override
	public boolean isNew() {
		return _ordiniClienti.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_ordiniClienti.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _ordiniClienti.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_ordiniClienti.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _ordiniClienti.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _ordiniClienti.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_ordiniClienti.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _ordiniClienti.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_ordiniClienti.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_ordiniClienti.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_ordiniClienti.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new OrdiniClientiWrapper((OrdiniClienti)_ordiniClienti.clone());
	}

	@Override
	public int compareTo(it.bysoftware.ct.model.OrdiniClienti ordiniClienti) {
		return _ordiniClienti.compareTo(ordiniClienti);
	}

	@Override
	public int hashCode() {
		return _ordiniClienti.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.OrdiniClienti> toCacheModel() {
		return _ordiniClienti.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.OrdiniClienti toEscapedModel() {
		return new OrdiniClientiWrapper(_ordiniClienti.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.OrdiniClienti toUnescapedModel() {
		return new OrdiniClientiWrapper(_ordiniClienti.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _ordiniClienti.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _ordiniClienti.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_ordiniClienti.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OrdiniClientiWrapper)) {
			return false;
		}

		OrdiniClientiWrapper ordiniClientiWrapper = (OrdiniClientiWrapper)obj;

		if (Validator.equals(_ordiniClienti, ordiniClientiWrapper._ordiniClienti)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public OrdiniClienti getWrappedOrdiniClienti() {
		return _ordiniClienti;
	}

	@Override
	public OrdiniClienti getWrappedModel() {
		return _ordiniClienti;
	}

	@Override
	public void resetOriginalValues() {
		_ordiniClienti.resetOriginalValues();
	}

	private OrdiniClienti _ordiniClienti;
}