/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ListaMovimentiMagazzino}.
 * </p>
 *
 * @author Mario Torrisi
 * @see ListaMovimentiMagazzino
 * @generated
 */
public class ListaMovimentiMagazzinoWrapper implements ListaMovimentiMagazzino,
	ModelWrapper<ListaMovimentiMagazzino> {
	public ListaMovimentiMagazzinoWrapper(
		ListaMovimentiMagazzino listaMovimentiMagazzino) {
		_listaMovimentiMagazzino = listaMovimentiMagazzino;
	}

	@Override
	public Class<?> getModelClass() {
		return ListaMovimentiMagazzino.class;
	}

	@Override
	public String getModelClassName() {
		return ListaMovimentiMagazzino.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ID", getID());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("descrizione", getDescrizione());
		attributes.put("quantita", getQuantita());
		attributes.put("testCaricoScarico", getTestCaricoScarico());
		attributes.put("soloValore", getSoloValore());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String ID = (String)attributes.get("ID");

		if (ID != null) {
			setID(ID);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		String quantita = (String)attributes.get("quantita");

		if (quantita != null) {
			setQuantita(quantita);
		}

		Integer testCaricoScarico = (Integer)attributes.get("testCaricoScarico");

		if (testCaricoScarico != null) {
			setTestCaricoScarico(testCaricoScarico);
		}

		Integer soloValore = (Integer)attributes.get("soloValore");

		if (soloValore != null) {
			setSoloValore(soloValore);
		}
	}

	/**
	* Returns the primary key of this lista movimenti magazzino.
	*
	* @return the primary key of this lista movimenti magazzino
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _listaMovimentiMagazzino.getPrimaryKey();
	}

	/**
	* Sets the primary key of this lista movimenti magazzino.
	*
	* @param primaryKey the primary key of this lista movimenti magazzino
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_listaMovimentiMagazzino.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the i d of this lista movimenti magazzino.
	*
	* @return the i d of this lista movimenti magazzino
	*/
	@Override
	public java.lang.String getID() {
		return _listaMovimentiMagazzino.getID();
	}

	/**
	* Sets the i d of this lista movimenti magazzino.
	*
	* @param ID the i d of this lista movimenti magazzino
	*/
	@Override
	public void setID(java.lang.String ID) {
		_listaMovimentiMagazzino.setID(ID);
	}

	/**
	* Returns the codice articolo of this lista movimenti magazzino.
	*
	* @return the codice articolo of this lista movimenti magazzino
	*/
	@Override
	public java.lang.String getCodiceArticolo() {
		return _listaMovimentiMagazzino.getCodiceArticolo();
	}

	/**
	* Sets the codice articolo of this lista movimenti magazzino.
	*
	* @param codiceArticolo the codice articolo of this lista movimenti magazzino
	*/
	@Override
	public void setCodiceArticolo(java.lang.String codiceArticolo) {
		_listaMovimentiMagazzino.setCodiceArticolo(codiceArticolo);
	}

	/**
	* Returns the codice variante of this lista movimenti magazzino.
	*
	* @return the codice variante of this lista movimenti magazzino
	*/
	@Override
	public java.lang.String getCodiceVariante() {
		return _listaMovimentiMagazzino.getCodiceVariante();
	}

	/**
	* Sets the codice variante of this lista movimenti magazzino.
	*
	* @param codiceVariante the codice variante of this lista movimenti magazzino
	*/
	@Override
	public void setCodiceVariante(java.lang.String codiceVariante) {
		_listaMovimentiMagazzino.setCodiceVariante(codiceVariante);
	}

	/**
	* Returns the descrizione of this lista movimenti magazzino.
	*
	* @return the descrizione of this lista movimenti magazzino
	*/
	@Override
	public java.lang.String getDescrizione() {
		return _listaMovimentiMagazzino.getDescrizione();
	}

	/**
	* Sets the descrizione of this lista movimenti magazzino.
	*
	* @param descrizione the descrizione of this lista movimenti magazzino
	*/
	@Override
	public void setDescrizione(java.lang.String descrizione) {
		_listaMovimentiMagazzino.setDescrizione(descrizione);
	}

	/**
	* Returns the quantita of this lista movimenti magazzino.
	*
	* @return the quantita of this lista movimenti magazzino
	*/
	@Override
	public java.lang.String getQuantita() {
		return _listaMovimentiMagazzino.getQuantita();
	}

	/**
	* Sets the quantita of this lista movimenti magazzino.
	*
	* @param quantita the quantita of this lista movimenti magazzino
	*/
	@Override
	public void setQuantita(java.lang.String quantita) {
		_listaMovimentiMagazzino.setQuantita(quantita);
	}

	/**
	* Returns the test carico scarico of this lista movimenti magazzino.
	*
	* @return the test carico scarico of this lista movimenti magazzino
	*/
	@Override
	public int getTestCaricoScarico() {
		return _listaMovimentiMagazzino.getTestCaricoScarico();
	}

	/**
	* Sets the test carico scarico of this lista movimenti magazzino.
	*
	* @param testCaricoScarico the test carico scarico of this lista movimenti magazzino
	*/
	@Override
	public void setTestCaricoScarico(int testCaricoScarico) {
		_listaMovimentiMagazzino.setTestCaricoScarico(testCaricoScarico);
	}

	/**
	* Returns the solo valore of this lista movimenti magazzino.
	*
	* @return the solo valore of this lista movimenti magazzino
	*/
	@Override
	public int getSoloValore() {
		return _listaMovimentiMagazzino.getSoloValore();
	}

	/**
	* Sets the solo valore of this lista movimenti magazzino.
	*
	* @param soloValore the solo valore of this lista movimenti magazzino
	*/
	@Override
	public void setSoloValore(int soloValore) {
		_listaMovimentiMagazzino.setSoloValore(soloValore);
	}

	@Override
	public boolean isNew() {
		return _listaMovimentiMagazzino.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_listaMovimentiMagazzino.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _listaMovimentiMagazzino.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_listaMovimentiMagazzino.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _listaMovimentiMagazzino.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _listaMovimentiMagazzino.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_listaMovimentiMagazzino.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _listaMovimentiMagazzino.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_listaMovimentiMagazzino.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_listaMovimentiMagazzino.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_listaMovimentiMagazzino.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ListaMovimentiMagazzinoWrapper((ListaMovimentiMagazzino)_listaMovimentiMagazzino.clone());
	}

	@Override
	public int compareTo(
		it.bysoftware.ct.model.ListaMovimentiMagazzino listaMovimentiMagazzino) {
		return _listaMovimentiMagazzino.compareTo(listaMovimentiMagazzino);
	}

	@Override
	public int hashCode() {
		return _listaMovimentiMagazzino.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.ListaMovimentiMagazzino> toCacheModel() {
		return _listaMovimentiMagazzino.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.ListaMovimentiMagazzino toEscapedModel() {
		return new ListaMovimentiMagazzinoWrapper(_listaMovimentiMagazzino.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.ListaMovimentiMagazzino toUnescapedModel() {
		return new ListaMovimentiMagazzinoWrapper(_listaMovimentiMagazzino.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _listaMovimentiMagazzino.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _listaMovimentiMagazzino.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_listaMovimentiMagazzino.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ListaMovimentiMagazzinoWrapper)) {
			return false;
		}

		ListaMovimentiMagazzinoWrapper listaMovimentiMagazzinoWrapper = (ListaMovimentiMagazzinoWrapper)obj;

		if (Validator.equals(_listaMovimentiMagazzino,
					listaMovimentiMagazzinoWrapper._listaMovimentiMagazzino)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ListaMovimentiMagazzino getWrappedListaMovimentiMagazzino() {
		return _listaMovimentiMagazzino;
	}

	@Override
	public ListaMovimentiMagazzino getWrappedModel() {
		return _listaMovimentiMagazzino;
	}

	@Override
	public void resetOriginalValues() {
		_listaMovimentiMagazzino.resetOriginalValues();
	}

	private ListaMovimentiMagazzino _listaMovimentiMagazzino;
}