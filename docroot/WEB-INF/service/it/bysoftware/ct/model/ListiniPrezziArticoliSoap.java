/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.ListiniPrezziArticoliPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.ListiniPrezziArticoliServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.ListiniPrezziArticoliServiceSoap
 * @generated
 */
public class ListiniPrezziArticoliSoap implements Serializable {
	public static ListiniPrezziArticoliSoap toSoapModel(
		ListiniPrezziArticoli model) {
		ListiniPrezziArticoliSoap soapModel = new ListiniPrezziArticoliSoap();

		soapModel.setCodiceListino(model.getCodiceListino());
		soapModel.setCodiceDivisa(model.getCodiceDivisa());
		soapModel.setTipoSoggetto(model.getTipoSoggetto());
		soapModel.setCodiceSoggetto(model.getCodiceSoggetto());
		soapModel.setDataInizioValidita(model.getDataInizioValidita());
		soapModel.setCodiceArticolo(model.getCodiceArticolo());
		soapModel.setCodiceVariante(model.getCodiceVariante());
		soapModel.setCodiceLavorazione(model.getCodiceLavorazione());
		soapModel.setQuantInizioValiditaPrezzo(model.getQuantInizioValiditaPrezzo());
		soapModel.setDataFineValidita(model.getDataFineValidita());
		soapModel.setQuantFineValiditaPrezzo(model.getQuantFineValiditaPrezzo());
		soapModel.setPrezzoNettoIVA(model.getPrezzoNettoIVA());
		soapModel.setCodiceIVA(model.getCodiceIVA());
		soapModel.setUtilizzoSconti(model.getUtilizzoSconti());
		soapModel.setScontoListino1(model.getScontoListino1());
		soapModel.setScontoListino2(model.getScontoListino2());
		soapModel.setScontoListino3(model.getScontoListino3());

		return soapModel;
	}

	public static ListiniPrezziArticoliSoap[] toSoapModels(
		ListiniPrezziArticoli[] models) {
		ListiniPrezziArticoliSoap[] soapModels = new ListiniPrezziArticoliSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ListiniPrezziArticoliSoap[][] toSoapModels(
		ListiniPrezziArticoli[][] models) {
		ListiniPrezziArticoliSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ListiniPrezziArticoliSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ListiniPrezziArticoliSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ListiniPrezziArticoliSoap[] toSoapModels(
		List<ListiniPrezziArticoli> models) {
		List<ListiniPrezziArticoliSoap> soapModels = new ArrayList<ListiniPrezziArticoliSoap>(models.size());

		for (ListiniPrezziArticoli model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ListiniPrezziArticoliSoap[soapModels.size()]);
	}

	public ListiniPrezziArticoliSoap() {
	}

	public ListiniPrezziArticoliPK getPrimaryKey() {
		return new ListiniPrezziArticoliPK(_codiceListino, _codiceDivisa,
			_tipoSoggetto, _codiceSoggetto, _dataInizioValidita,
			_codiceArticolo, _codiceVariante, _codiceLavorazione,
			_quantInizioValiditaPrezzo);
	}

	public void setPrimaryKey(ListiniPrezziArticoliPK pk) {
		setCodiceListino(pk.codiceListino);
		setCodiceDivisa(pk.codiceDivisa);
		setTipoSoggetto(pk.tipoSoggetto);
		setCodiceSoggetto(pk.codiceSoggetto);
		setDataInizioValidita(pk.dataInizioValidita);
		setCodiceArticolo(pk.codiceArticolo);
		setCodiceVariante(pk.codiceVariante);
		setCodiceLavorazione(pk.codiceLavorazione);
		setQuantInizioValiditaPrezzo(pk.quantInizioValiditaPrezzo);
	}

	public String getCodiceListino() {
		return _codiceListino;
	}

	public void setCodiceListino(String codiceListino) {
		_codiceListino = codiceListino;
	}

	public String getCodiceDivisa() {
		return _codiceDivisa;
	}

	public void setCodiceDivisa(String codiceDivisa) {
		_codiceDivisa = codiceDivisa;
	}

	public int getTipoSoggetto() {
		return _tipoSoggetto;
	}

	public void setTipoSoggetto(int tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;
	}

	public String getCodiceSoggetto() {
		return _codiceSoggetto;
	}

	public void setCodiceSoggetto(String codiceSoggetto) {
		_codiceSoggetto = codiceSoggetto;
	}

	public Date getDataInizioValidita() {
		return _dataInizioValidita;
	}

	public void setDataInizioValidita(Date dataInizioValidita) {
		_dataInizioValidita = dataInizioValidita;
	}

	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;
	}

	public String getCodiceVariante() {
		return _codiceVariante;
	}

	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;
	}

	public String getCodiceLavorazione() {
		return _codiceLavorazione;
	}

	public void setCodiceLavorazione(String codiceLavorazione) {
		_codiceLavorazione = codiceLavorazione;
	}

	public double getQuantInizioValiditaPrezzo() {
		return _quantInizioValiditaPrezzo;
	}

	public void setQuantInizioValiditaPrezzo(double quantInizioValiditaPrezzo) {
		_quantInizioValiditaPrezzo = quantInizioValiditaPrezzo;
	}

	public Date getDataFineValidita() {
		return _dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		_dataFineValidita = dataFineValidita;
	}

	public double getQuantFineValiditaPrezzo() {
		return _quantFineValiditaPrezzo;
	}

	public void setQuantFineValiditaPrezzo(double quantFineValiditaPrezzo) {
		_quantFineValiditaPrezzo = quantFineValiditaPrezzo;
	}

	public double getPrezzoNettoIVA() {
		return _prezzoNettoIVA;
	}

	public void setPrezzoNettoIVA(double prezzoNettoIVA) {
		_prezzoNettoIVA = prezzoNettoIVA;
	}

	public String getCodiceIVA() {
		return _codiceIVA;
	}

	public void setCodiceIVA(String codiceIVA) {
		_codiceIVA = codiceIVA;
	}

	public boolean getUtilizzoSconti() {
		return _utilizzoSconti;
	}

	public boolean isUtilizzoSconti() {
		return _utilizzoSconti;
	}

	public void setUtilizzoSconti(boolean utilizzoSconti) {
		_utilizzoSconti = utilizzoSconti;
	}

	public double getScontoListino1() {
		return _scontoListino1;
	}

	public void setScontoListino1(double scontoListino1) {
		_scontoListino1 = scontoListino1;
	}

	public double getScontoListino2() {
		return _scontoListino2;
	}

	public void setScontoListino2(double scontoListino2) {
		_scontoListino2 = scontoListino2;
	}

	public double getScontoListino3() {
		return _scontoListino3;
	}

	public void setScontoListino3(double scontoListino3) {
		_scontoListino3 = scontoListino3;
	}

	private String _codiceListino;
	private String _codiceDivisa;
	private int _tipoSoggetto;
	private String _codiceSoggetto;
	private Date _dataInizioValidita;
	private String _codiceArticolo;
	private String _codiceVariante;
	private String _codiceLavorazione;
	private double _quantInizioValiditaPrezzo;
	private Date _dataFineValidita;
	private double _quantFineValiditaPrezzo;
	private double _prezzoNettoIVA;
	private String _codiceIVA;
	private boolean _utilizzoSconti;
	private double _scontoListino1;
	private double _scontoListino2;
	private double _scontoListino3;
}