/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.UltimiPrezziArticoliLocalServiceUtil;
import it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class UltimiPrezziArticoliClp extends BaseModelImpl<UltimiPrezziArticoli>
	implements UltimiPrezziArticoli {
	public UltimiPrezziArticoliClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return UltimiPrezziArticoli.class;
	}

	@Override
	public String getModelClassName() {
		return UltimiPrezziArticoli.class.getName();
	}

	@Override
	public UltimiPrezziArticoliPK getPrimaryKey() {
		return new UltimiPrezziArticoliPK(_tipoSoggetto, _codiceSoggetto,
			_codiceArticolo, _codiceVariante);
	}

	@Override
	public void setPrimaryKey(UltimiPrezziArticoliPK primaryKey) {
		setTipoSoggetto(primaryKey.tipoSoggetto);
		setCodiceSoggetto(primaryKey.codiceSoggetto);
		setCodiceArticolo(primaryKey.codiceArticolo);
		setCodiceVariante(primaryKey.codiceVariante);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new UltimiPrezziArticoliPK(_tipoSoggetto, _codiceSoggetto,
			_codiceArticolo, _codiceVariante);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((UltimiPrezziArticoliPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceSoggetto", getCodiceSoggetto());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("dataDocumento", getDataDocumento());
		attributes.put("codiceDivisa", getCodiceDivisa());
		attributes.put("prezzo", getPrezzo());
		attributes.put("sconto1", getSconto1());
		attributes.put("sconto2", getSconto2());
		attributes.put("sconto3", getSconto3());
		attributes.put("scontoChiusura", getScontoChiusura());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceSoggetto = (String)attributes.get("codiceSoggetto");

		if (codiceSoggetto != null) {
			setCodiceSoggetto(codiceSoggetto);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		Date dataDocumento = (Date)attributes.get("dataDocumento");

		if (dataDocumento != null) {
			setDataDocumento(dataDocumento);
		}

		String codiceDivisa = (String)attributes.get("codiceDivisa");

		if (codiceDivisa != null) {
			setCodiceDivisa(codiceDivisa);
		}

		Double prezzo = (Double)attributes.get("prezzo");

		if (prezzo != null) {
			setPrezzo(prezzo);
		}

		Double sconto1 = (Double)attributes.get("sconto1");

		if (sconto1 != null) {
			setSconto1(sconto1);
		}

		Double sconto2 = (Double)attributes.get("sconto2");

		if (sconto2 != null) {
			setSconto2(sconto2);
		}

		Double sconto3 = (Double)attributes.get("sconto3");

		if (sconto3 != null) {
			setSconto3(sconto3);
		}

		Double scontoChiusura = (Double)attributes.get("scontoChiusura");

		if (scontoChiusura != null) {
			setScontoChiusura(scontoChiusura);
		}
	}

	@Override
	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;

		if (_ultimiPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _ultimiPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoSoggetto", boolean.class);

				method.invoke(_ultimiPrezziArticoliRemoteModel, tipoSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceSoggetto() {
		return _codiceSoggetto;
	}

	@Override
	public void setCodiceSoggetto(String codiceSoggetto) {
		_codiceSoggetto = codiceSoggetto;

		if (_ultimiPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _ultimiPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceSoggetto",
						String.class);

				method.invoke(_ultimiPrezziArticoliRemoteModel, codiceSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	@Override
	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;

		if (_ultimiPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _ultimiPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceArticolo",
						String.class);

				method.invoke(_ultimiPrezziArticoliRemoteModel, codiceArticolo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceVariante() {
		return _codiceVariante;
	}

	@Override
	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;

		if (_ultimiPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _ultimiPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceVariante",
						String.class);

				method.invoke(_ultimiPrezziArticoliRemoteModel, codiceVariante);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataDocumento() {
		return _dataDocumento;
	}

	@Override
	public void setDataDocumento(Date dataDocumento) {
		_dataDocumento = dataDocumento;

		if (_ultimiPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _ultimiPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setDataDocumento", Date.class);

				method.invoke(_ultimiPrezziArticoliRemoteModel, dataDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDivisa() {
		return _codiceDivisa;
	}

	@Override
	public void setCodiceDivisa(String codiceDivisa) {
		_codiceDivisa = codiceDivisa;

		if (_ultimiPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _ultimiPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDivisa", String.class);

				method.invoke(_ultimiPrezziArticoliRemoteModel, codiceDivisa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPrezzo() {
		return _prezzo;
	}

	@Override
	public void setPrezzo(double prezzo) {
		_prezzo = prezzo;

		if (_ultimiPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _ultimiPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setPrezzo", double.class);

				method.invoke(_ultimiPrezziArticoliRemoteModel, prezzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSconto1() {
		return _sconto1;
	}

	@Override
	public void setSconto1(double sconto1) {
		_sconto1 = sconto1;

		if (_ultimiPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _ultimiPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setSconto1", double.class);

				method.invoke(_ultimiPrezziArticoliRemoteModel, sconto1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSconto2() {
		return _sconto2;
	}

	@Override
	public void setSconto2(double sconto2) {
		_sconto2 = sconto2;

		if (_ultimiPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _ultimiPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setSconto2", double.class);

				method.invoke(_ultimiPrezziArticoliRemoteModel, sconto2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSconto3() {
		return _sconto3;
	}

	@Override
	public void setSconto3(double sconto3) {
		_sconto3 = sconto3;

		if (_ultimiPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _ultimiPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setSconto3", double.class);

				method.invoke(_ultimiPrezziArticoliRemoteModel, sconto3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getScontoChiusura() {
		return _scontoChiusura;
	}

	@Override
	public void setScontoChiusura(double scontoChiusura) {
		_scontoChiusura = scontoChiusura;

		if (_ultimiPrezziArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _ultimiPrezziArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setScontoChiusura",
						double.class);

				method.invoke(_ultimiPrezziArticoliRemoteModel, scontoChiusura);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getUltimiPrezziArticoliRemoteModel() {
		return _ultimiPrezziArticoliRemoteModel;
	}

	public void setUltimiPrezziArticoliRemoteModel(
		BaseModel<?> ultimiPrezziArticoliRemoteModel) {
		_ultimiPrezziArticoliRemoteModel = ultimiPrezziArticoliRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _ultimiPrezziArticoliRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_ultimiPrezziArticoliRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			UltimiPrezziArticoliLocalServiceUtil.addUltimiPrezziArticoli(this);
		}
		else {
			UltimiPrezziArticoliLocalServiceUtil.updateUltimiPrezziArticoli(this);
		}
	}

	@Override
	public UltimiPrezziArticoli toEscapedModel() {
		return (UltimiPrezziArticoli)ProxyUtil.newProxyInstance(UltimiPrezziArticoli.class.getClassLoader(),
			new Class[] { UltimiPrezziArticoli.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		UltimiPrezziArticoliClp clone = new UltimiPrezziArticoliClp();

		clone.setTipoSoggetto(getTipoSoggetto());
		clone.setCodiceSoggetto(getCodiceSoggetto());
		clone.setCodiceArticolo(getCodiceArticolo());
		clone.setCodiceVariante(getCodiceVariante());
		clone.setDataDocumento(getDataDocumento());
		clone.setCodiceDivisa(getCodiceDivisa());
		clone.setPrezzo(getPrezzo());
		clone.setSconto1(getSconto1());
		clone.setSconto2(getSconto2());
		clone.setSconto3(getSconto3());
		clone.setScontoChiusura(getScontoChiusura());

		return clone;
	}

	@Override
	public int compareTo(UltimiPrezziArticoli ultimiPrezziArticoli) {
		int value = 0;

		value = DateUtil.compareTo(getDataDocumento(),
				ultimiPrezziArticoli.getDataDocumento());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UltimiPrezziArticoliClp)) {
			return false;
		}

		UltimiPrezziArticoliClp ultimiPrezziArticoli = (UltimiPrezziArticoliClp)obj;

		UltimiPrezziArticoliPK primaryKey = ultimiPrezziArticoli.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{tipoSoggetto=");
		sb.append(getTipoSoggetto());
		sb.append(", codiceSoggetto=");
		sb.append(getCodiceSoggetto());
		sb.append(", codiceArticolo=");
		sb.append(getCodiceArticolo());
		sb.append(", codiceVariante=");
		sb.append(getCodiceVariante());
		sb.append(", dataDocumento=");
		sb.append(getDataDocumento());
		sb.append(", codiceDivisa=");
		sb.append(getCodiceDivisa());
		sb.append(", prezzo=");
		sb.append(getPrezzo());
		sb.append(", sconto1=");
		sb.append(getSconto1());
		sb.append(", sconto2=");
		sb.append(getSconto2());
		sb.append(", sconto3=");
		sb.append(getSconto3());
		sb.append(", scontoChiusura=");
		sb.append(getScontoChiusura());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(37);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.UltimiPrezziArticoli");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>tipoSoggetto</column-name><column-value><![CDATA[");
		sb.append(getTipoSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceSoggetto</column-name><column-value><![CDATA[");
		sb.append(getCodiceSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceArticolo</column-name><column-value><![CDATA[");
		sb.append(getCodiceArticolo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceVariante</column-name><column-value><![CDATA[");
		sb.append(getCodiceVariante());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataDocumento</column-name><column-value><![CDATA[");
		sb.append(getDataDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDivisa</column-name><column-value><![CDATA[");
		sb.append(getCodiceDivisa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>prezzo</column-name><column-value><![CDATA[");
		sb.append(getPrezzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sconto1</column-name><column-value><![CDATA[");
		sb.append(getSconto1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sconto2</column-name><column-value><![CDATA[");
		sb.append(getSconto2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sconto3</column-name><column-value><![CDATA[");
		sb.append(getSconto3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>scontoChiusura</column-name><column-value><![CDATA[");
		sb.append(getScontoChiusura());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private boolean _tipoSoggetto;
	private String _codiceSoggetto;
	private String _codiceArticolo;
	private String _codiceVariante;
	private Date _dataDocumento;
	private String _codiceDivisa;
	private double _prezzo;
	private double _sconto1;
	private double _sconto2;
	private double _sconto3;
	private double _scontoChiusura;
	private BaseModel<?> _ultimiPrezziArticoliRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}