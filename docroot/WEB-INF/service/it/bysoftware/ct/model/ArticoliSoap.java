/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.ArticoliServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.ArticoliServiceSoap
 * @generated
 */
public class ArticoliSoap implements Serializable {
	public static ArticoliSoap toSoapModel(Articoli model) {
		ArticoliSoap soapModel = new ArticoliSoap();

		soapModel.setCodiceArticolo(model.getCodiceArticolo());
		soapModel.setDescrizione(model.getDescrizione());
		soapModel.setDescrizioneAggiuntiva(model.getDescrizioneAggiuntiva());
		soapModel.setDescrizioneBreveFiscale(model.getDescrizioneBreveFiscale());
		soapModel.setPathImmagine(model.getPathImmagine());
		soapModel.setUnitaMisura(model.getUnitaMisura());
		soapModel.setCategoriaMerceologica(model.getCategoriaMerceologica());
		soapModel.setCategoriaFiscale(model.getCategoriaFiscale());
		soapModel.setCodiceIVA(model.getCodiceIVA());
		soapModel.setCodiceProvvigioni(model.getCodiceProvvigioni());
		soapModel.setCodiceSconto1(model.getCodiceSconto1());
		soapModel.setCodiceSconto2(model.getCodiceSconto2());
		soapModel.setCategoriaInventario(model.getCategoriaInventario());
		soapModel.setPrezzo1(model.getPrezzo1());
		soapModel.setPrezzo2(model.getPrezzo2());
		soapModel.setPrezzo3(model.getPrezzo3());
		soapModel.setCodiceIVAPrezzo1(model.getCodiceIVAPrezzo1());
		soapModel.setCodiceIVAPrezzo2(model.getCodiceIVAPrezzo2());
		soapModel.setCodiceIVAPrezzo3(model.getCodiceIVAPrezzo3());
		soapModel.setNumeroDecimaliPrezzo(model.getNumeroDecimaliPrezzo());
		soapModel.setNumeroDecimaliQuantita(model.getNumeroDecimaliQuantita());
		soapModel.setNumeroMesiPerinvenduto(model.getNumeroMesiPerinvenduto());
		soapModel.setUnitaMisura2(model.getUnitaMisura2());
		soapModel.setFattoreMoltiplicativo(model.getFattoreMoltiplicativo());
		soapModel.setFattoreDivisione(model.getFattoreDivisione());
		soapModel.setRagruppamentoLibero(model.getRagruppamentoLibero());
		soapModel.setTipoLivelloDistintaBase(model.getTipoLivelloDistintaBase());
		soapModel.setTipoCostoStatistico(model.getTipoCostoStatistico());
		soapModel.setTipoGestioneArticolo(model.getTipoGestioneArticolo());
		soapModel.setTipoApprovvigionamentoArticolo(model.getTipoApprovvigionamentoArticolo());
		soapModel.setNomenclaturaCombinata(model.getNomenclaturaCombinata());
		soapModel.setDescrizioneEstesa(model.getDescrizioneEstesa());
		soapModel.setGenerazioneMovimenti(model.getGenerazioneMovimenti());
		soapModel.setTipoEsplosioneDistintaBase(model.getTipoEsplosioneDistintaBase());
		soapModel.setLivelloMaxEsplosioneDistintaBase(model.getLivelloMaxEsplosioneDistintaBase());
		soapModel.setValorizzazioneMagazzino(model.getValorizzazioneMagazzino());
		soapModel.setAbilitatoEC(model.getAbilitatoEC());
		soapModel.setCategoriaEC(model.getCategoriaEC());
		soapModel.setQuantitaMinimaEC(model.getQuantitaMinimaEC());
		soapModel.setQuantitaDefaultEC(model.getQuantitaDefaultEC());
		soapModel.setCodiceSchedaTecnicaEC(model.getCodiceSchedaTecnicaEC());
		soapModel.setGiorniPrevistaConsegna(model.getGiorniPrevistaConsegna());
		soapModel.setGestioneLotti(model.getGestioneLotti());
		soapModel.setCreazioneLotti(model.getCreazioneLotti());
		soapModel.setPrefissoCodiceLottoAutomatico(model.getPrefissoCodiceLottoAutomatico());
		soapModel.setNumeroCifreProgressivo(model.getNumeroCifreProgressivo());
		soapModel.setScaricoAutomaticoLotti(model.getScaricoAutomaticoLotti());
		soapModel.setGiorniInizioValiditaLotto(model.getGiorniInizioValiditaLotto());
		soapModel.setGiorniValiditaLotto(model.getGiorniValiditaLotto());
		soapModel.setGiorniPreavvisoScadenzaLotto(model.getGiorniPreavvisoScadenzaLotto());
		soapModel.setFattoreMoltQuantRiservata(model.getFattoreMoltQuantRiservata());
		soapModel.setFattoreDivQuantRiservata(model.getFattoreDivQuantRiservata());
		soapModel.setObsoleto(model.getObsoleto());
		soapModel.setComeImballo(model.getComeImballo());
		soapModel.setFattoreMoltCalcoloImballo(model.getFattoreMoltCalcoloImballo());
		soapModel.setFattoreDivCalcoloImballo(model.getFattoreDivCalcoloImballo());
		soapModel.setPesoLordo(model.getPesoLordo());
		soapModel.setTara(model.getTara());
		soapModel.setCalcoloVolume(model.getCalcoloVolume());
		soapModel.setLunghezza(model.getLunghezza());
		soapModel.setLarghezza(model.getLarghezza());
		soapModel.setProfondita(model.getProfondita());
		soapModel.setCodiceImballo(model.getCodiceImballo());
		soapModel.setFattoreMoltUnitMisSupp(model.getFattoreMoltUnitMisSupp());
		soapModel.setFattoreDivUnitMisSupp(model.getFattoreDivUnitMisSupp());
		soapModel.setStampaetichette(model.getStampaetichette());
		soapModel.setGestioneVarianti(model.getGestioneVarianti());
		soapModel.setGestioneVariantiDistintaBase(model.getGestioneVariantiDistintaBase());
		soapModel.setLiberoString1(model.getLiberoString1());
		soapModel.setLiberoString2(model.getLiberoString2());
		soapModel.setLiberoString3(model.getLiberoString3());
		soapModel.setLiberoString4(model.getLiberoString4());
		soapModel.setLiberoString5(model.getLiberoString5());
		soapModel.setLiberoDate1(model.getLiberoDate1());
		soapModel.setLiberoDate2(model.getLiberoDate2());
		soapModel.setLiberoDate3(model.getLiberoDate3());
		soapModel.setLiberoDate4(model.getLiberoDate4());
		soapModel.setLiberoDate5(model.getLiberoDate5());
		soapModel.setLiberoLong1(model.getLiberoLong1());
		soapModel.setLiberoLong2(model.getLiberoLong2());
		soapModel.setLiberoLong3(model.getLiberoLong3());
		soapModel.setLiberoLong4(model.getLiberoLong4());
		soapModel.setLiberoLong5(model.getLiberoLong5());
		soapModel.setLiberoDouble1(model.getLiberoDouble1());
		soapModel.setLiberoDouble2(model.getLiberoDouble2());
		soapModel.setLiberoDouble3(model.getLiberoDouble3());
		soapModel.setLiberoDouble4(model.getLiberoDouble4());
		soapModel.setLiberoDouble5(model.getLiberoDouble5());

		return soapModel;
	}

	public static ArticoliSoap[] toSoapModels(Articoli[] models) {
		ArticoliSoap[] soapModels = new ArticoliSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ArticoliSoap[][] toSoapModels(Articoli[][] models) {
		ArticoliSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ArticoliSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ArticoliSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ArticoliSoap[] toSoapModels(List<Articoli> models) {
		List<ArticoliSoap> soapModels = new ArrayList<ArticoliSoap>(models.size());

		for (Articoli model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ArticoliSoap[soapModels.size()]);
	}

	public ArticoliSoap() {
	}

	public String getPrimaryKey() {
		return _codiceArticolo;
	}

	public void setPrimaryKey(String pk) {
		setCodiceArticolo(pk);
	}

	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;
	}

	public String getDescrizione() {
		return _descrizione;
	}

	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;
	}

	public String getDescrizioneAggiuntiva() {
		return _descrizioneAggiuntiva;
	}

	public void setDescrizioneAggiuntiva(String descrizioneAggiuntiva) {
		_descrizioneAggiuntiva = descrizioneAggiuntiva;
	}

	public String getDescrizioneBreveFiscale() {
		return _descrizioneBreveFiscale;
	}

	public void setDescrizioneBreveFiscale(String descrizioneBreveFiscale) {
		_descrizioneBreveFiscale = descrizioneBreveFiscale;
	}

	public String getPathImmagine() {
		return _pathImmagine;
	}

	public void setPathImmagine(String pathImmagine) {
		_pathImmagine = pathImmagine;
	}

	public String getUnitaMisura() {
		return _unitaMisura;
	}

	public void setUnitaMisura(String unitaMisura) {
		_unitaMisura = unitaMisura;
	}

	public String getCategoriaMerceologica() {
		return _categoriaMerceologica;
	}

	public void setCategoriaMerceologica(String categoriaMerceologica) {
		_categoriaMerceologica = categoriaMerceologica;
	}

	public String getCategoriaFiscale() {
		return _categoriaFiscale;
	}

	public void setCategoriaFiscale(String categoriaFiscale) {
		_categoriaFiscale = categoriaFiscale;
	}

	public String getCodiceIVA() {
		return _codiceIVA;
	}

	public void setCodiceIVA(String codiceIVA) {
		_codiceIVA = codiceIVA;
	}

	public String getCodiceProvvigioni() {
		return _codiceProvvigioni;
	}

	public void setCodiceProvvigioni(String codiceProvvigioni) {
		_codiceProvvigioni = codiceProvvigioni;
	}

	public String getCodiceSconto1() {
		return _codiceSconto1;
	}

	public void setCodiceSconto1(String codiceSconto1) {
		_codiceSconto1 = codiceSconto1;
	}

	public String getCodiceSconto2() {
		return _codiceSconto2;
	}

	public void setCodiceSconto2(String codiceSconto2) {
		_codiceSconto2 = codiceSconto2;
	}

	public String getCategoriaInventario() {
		return _categoriaInventario;
	}

	public void setCategoriaInventario(String categoriaInventario) {
		_categoriaInventario = categoriaInventario;
	}

	public double getPrezzo1() {
		return _prezzo1;
	}

	public void setPrezzo1(double prezzo1) {
		_prezzo1 = prezzo1;
	}

	public double getPrezzo2() {
		return _prezzo2;
	}

	public void setPrezzo2(double prezzo2) {
		_prezzo2 = prezzo2;
	}

	public double getPrezzo3() {
		return _prezzo3;
	}

	public void setPrezzo3(double prezzo3) {
		_prezzo3 = prezzo3;
	}

	public String getCodiceIVAPrezzo1() {
		return _codiceIVAPrezzo1;
	}

	public void setCodiceIVAPrezzo1(String codiceIVAPrezzo1) {
		_codiceIVAPrezzo1 = codiceIVAPrezzo1;
	}

	public String getCodiceIVAPrezzo2() {
		return _codiceIVAPrezzo2;
	}

	public void setCodiceIVAPrezzo2(String codiceIVAPrezzo2) {
		_codiceIVAPrezzo2 = codiceIVAPrezzo2;
	}

	public String getCodiceIVAPrezzo3() {
		return _codiceIVAPrezzo3;
	}

	public void setCodiceIVAPrezzo3(String codiceIVAPrezzo3) {
		_codiceIVAPrezzo3 = codiceIVAPrezzo3;
	}

	public int getNumeroDecimaliPrezzo() {
		return _numeroDecimaliPrezzo;
	}

	public void setNumeroDecimaliPrezzo(int numeroDecimaliPrezzo) {
		_numeroDecimaliPrezzo = numeroDecimaliPrezzo;
	}

	public int getNumeroDecimaliQuantita() {
		return _numeroDecimaliQuantita;
	}

	public void setNumeroDecimaliQuantita(int numeroDecimaliQuantita) {
		_numeroDecimaliQuantita = numeroDecimaliQuantita;
	}

	public int getNumeroMesiPerinvenduto() {
		return _numeroMesiPerinvenduto;
	}

	public void setNumeroMesiPerinvenduto(int numeroMesiPerinvenduto) {
		_numeroMesiPerinvenduto = numeroMesiPerinvenduto;
	}

	public String getUnitaMisura2() {
		return _unitaMisura2;
	}

	public void setUnitaMisura2(String unitaMisura2) {
		_unitaMisura2 = unitaMisura2;
	}

	public double getFattoreMoltiplicativo() {
		return _fattoreMoltiplicativo;
	}

	public void setFattoreMoltiplicativo(double fattoreMoltiplicativo) {
		_fattoreMoltiplicativo = fattoreMoltiplicativo;
	}

	public double getFattoreDivisione() {
		return _fattoreDivisione;
	}

	public void setFattoreDivisione(double fattoreDivisione) {
		_fattoreDivisione = fattoreDivisione;
	}

	public String getRagruppamentoLibero() {
		return _ragruppamentoLibero;
	}

	public void setRagruppamentoLibero(String ragruppamentoLibero) {
		_ragruppamentoLibero = ragruppamentoLibero;
	}

	public int getTipoLivelloDistintaBase() {
		return _tipoLivelloDistintaBase;
	}

	public void setTipoLivelloDistintaBase(int tipoLivelloDistintaBase) {
		_tipoLivelloDistintaBase = tipoLivelloDistintaBase;
	}

	public int getTipoCostoStatistico() {
		return _tipoCostoStatistico;
	}

	public void setTipoCostoStatistico(int tipoCostoStatistico) {
		_tipoCostoStatistico = tipoCostoStatistico;
	}

	public int getTipoGestioneArticolo() {
		return _tipoGestioneArticolo;
	}

	public void setTipoGestioneArticolo(int tipoGestioneArticolo) {
		_tipoGestioneArticolo = tipoGestioneArticolo;
	}

	public int getTipoApprovvigionamentoArticolo() {
		return _tipoApprovvigionamentoArticolo;
	}

	public void setTipoApprovvigionamentoArticolo(
		int tipoApprovvigionamentoArticolo) {
		_tipoApprovvigionamentoArticolo = tipoApprovvigionamentoArticolo;
	}

	public double getNomenclaturaCombinata() {
		return _nomenclaturaCombinata;
	}

	public void setNomenclaturaCombinata(double nomenclaturaCombinata) {
		_nomenclaturaCombinata = nomenclaturaCombinata;
	}

	public String getDescrizioneEstesa() {
		return _descrizioneEstesa;
	}

	public void setDescrizioneEstesa(String descrizioneEstesa) {
		_descrizioneEstesa = descrizioneEstesa;
	}

	public boolean getGenerazioneMovimenti() {
		return _generazioneMovimenti;
	}

	public boolean isGenerazioneMovimenti() {
		return _generazioneMovimenti;
	}

	public void setGenerazioneMovimenti(boolean generazioneMovimenti) {
		_generazioneMovimenti = generazioneMovimenti;
	}

	public int getTipoEsplosioneDistintaBase() {
		return _tipoEsplosioneDistintaBase;
	}

	public void setTipoEsplosioneDistintaBase(int tipoEsplosioneDistintaBase) {
		_tipoEsplosioneDistintaBase = tipoEsplosioneDistintaBase;
	}

	public int getLivelloMaxEsplosioneDistintaBase() {
		return _livelloMaxEsplosioneDistintaBase;
	}

	public void setLivelloMaxEsplosioneDistintaBase(
		int livelloMaxEsplosioneDistintaBase) {
		_livelloMaxEsplosioneDistintaBase = livelloMaxEsplosioneDistintaBase;
	}

	public boolean getValorizzazioneMagazzino() {
		return _valorizzazioneMagazzino;
	}

	public boolean isValorizzazioneMagazzino() {
		return _valorizzazioneMagazzino;
	}

	public void setValorizzazioneMagazzino(boolean valorizzazioneMagazzino) {
		_valorizzazioneMagazzino = valorizzazioneMagazzino;
	}

	public boolean getAbilitatoEC() {
		return _abilitatoEC;
	}

	public boolean isAbilitatoEC() {
		return _abilitatoEC;
	}

	public void setAbilitatoEC(boolean abilitatoEC) {
		_abilitatoEC = abilitatoEC;
	}

	public String getCategoriaEC() {
		return _categoriaEC;
	}

	public void setCategoriaEC(String categoriaEC) {
		_categoriaEC = categoriaEC;
	}

	public double getQuantitaMinimaEC() {
		return _quantitaMinimaEC;
	}

	public void setQuantitaMinimaEC(double quantitaMinimaEC) {
		_quantitaMinimaEC = quantitaMinimaEC;
	}

	public double getQuantitaDefaultEC() {
		return _quantitaDefaultEC;
	}

	public void setQuantitaDefaultEC(double quantitaDefaultEC) {
		_quantitaDefaultEC = quantitaDefaultEC;
	}

	public String getCodiceSchedaTecnicaEC() {
		return _codiceSchedaTecnicaEC;
	}

	public void setCodiceSchedaTecnicaEC(String codiceSchedaTecnicaEC) {
		_codiceSchedaTecnicaEC = codiceSchedaTecnicaEC;
	}

	public int getGiorniPrevistaConsegna() {
		return _giorniPrevistaConsegna;
	}

	public void setGiorniPrevistaConsegna(int giorniPrevistaConsegna) {
		_giorniPrevistaConsegna = giorniPrevistaConsegna;
	}

	public boolean getGestioneLotti() {
		return _gestioneLotti;
	}

	public boolean isGestioneLotti() {
		return _gestioneLotti;
	}

	public void setGestioneLotti(boolean gestioneLotti) {
		_gestioneLotti = gestioneLotti;
	}

	public boolean getCreazioneLotti() {
		return _creazioneLotti;
	}

	public boolean isCreazioneLotti() {
		return _creazioneLotti;
	}

	public void setCreazioneLotti(boolean creazioneLotti) {
		_creazioneLotti = creazioneLotti;
	}

	public String getPrefissoCodiceLottoAutomatico() {
		return _prefissoCodiceLottoAutomatico;
	}

	public void setPrefissoCodiceLottoAutomatico(
		String prefissoCodiceLottoAutomatico) {
		_prefissoCodiceLottoAutomatico = prefissoCodiceLottoAutomatico;
	}

	public int getNumeroCifreProgressivo() {
		return _numeroCifreProgressivo;
	}

	public void setNumeroCifreProgressivo(int numeroCifreProgressivo) {
		_numeroCifreProgressivo = numeroCifreProgressivo;
	}

	public boolean getScaricoAutomaticoLotti() {
		return _scaricoAutomaticoLotti;
	}

	public boolean isScaricoAutomaticoLotti() {
		return _scaricoAutomaticoLotti;
	}

	public void setScaricoAutomaticoLotti(boolean scaricoAutomaticoLotti) {
		_scaricoAutomaticoLotti = scaricoAutomaticoLotti;
	}

	public int getGiorniInizioValiditaLotto() {
		return _giorniInizioValiditaLotto;
	}

	public void setGiorniInizioValiditaLotto(int giorniInizioValiditaLotto) {
		_giorniInizioValiditaLotto = giorniInizioValiditaLotto;
	}

	public int getGiorniValiditaLotto() {
		return _giorniValiditaLotto;
	}

	public void setGiorniValiditaLotto(int giorniValiditaLotto) {
		_giorniValiditaLotto = giorniValiditaLotto;
	}

	public int getGiorniPreavvisoScadenzaLotto() {
		return _giorniPreavvisoScadenzaLotto;
	}

	public void setGiorniPreavvisoScadenzaLotto(
		int giorniPreavvisoScadenzaLotto) {
		_giorniPreavvisoScadenzaLotto = giorniPreavvisoScadenzaLotto;
	}

	public double getFattoreMoltQuantRiservata() {
		return _fattoreMoltQuantRiservata;
	}

	public void setFattoreMoltQuantRiservata(double fattoreMoltQuantRiservata) {
		_fattoreMoltQuantRiservata = fattoreMoltQuantRiservata;
	}

	public double getFattoreDivQuantRiservata() {
		return _fattoreDivQuantRiservata;
	}

	public void setFattoreDivQuantRiservata(double fattoreDivQuantRiservata) {
		_fattoreDivQuantRiservata = fattoreDivQuantRiservata;
	}

	public boolean getObsoleto() {
		return _obsoleto;
	}

	public boolean isObsoleto() {
		return _obsoleto;
	}

	public void setObsoleto(boolean obsoleto) {
		_obsoleto = obsoleto;
	}

	public boolean getComeImballo() {
		return _comeImballo;
	}

	public boolean isComeImballo() {
		return _comeImballo;
	}

	public void setComeImballo(boolean comeImballo) {
		_comeImballo = comeImballo;
	}

	public double getFattoreMoltCalcoloImballo() {
		return _fattoreMoltCalcoloImballo;
	}

	public void setFattoreMoltCalcoloImballo(double fattoreMoltCalcoloImballo) {
		_fattoreMoltCalcoloImballo = fattoreMoltCalcoloImballo;
	}

	public double getFattoreDivCalcoloImballo() {
		return _fattoreDivCalcoloImballo;
	}

	public void setFattoreDivCalcoloImballo(double fattoreDivCalcoloImballo) {
		_fattoreDivCalcoloImballo = fattoreDivCalcoloImballo;
	}

	public double getPesoLordo() {
		return _pesoLordo;
	}

	public void setPesoLordo(double pesoLordo) {
		_pesoLordo = pesoLordo;
	}

	public double getTara() {
		return _tara;
	}

	public void setTara(double tara) {
		_tara = tara;
	}

	public int getCalcoloVolume() {
		return _calcoloVolume;
	}

	public void setCalcoloVolume(int calcoloVolume) {
		_calcoloVolume = calcoloVolume;
	}

	public double getLunghezza() {
		return _lunghezza;
	}

	public void setLunghezza(double lunghezza) {
		_lunghezza = lunghezza;
	}

	public double getLarghezza() {
		return _larghezza;
	}

	public void setLarghezza(double larghezza) {
		_larghezza = larghezza;
	}

	public double getProfondita() {
		return _profondita;
	}

	public void setProfondita(double profondita) {
		_profondita = profondita;
	}

	public String getCodiceImballo() {
		return _codiceImballo;
	}

	public void setCodiceImballo(String codiceImballo) {
		_codiceImballo = codiceImballo;
	}

	public double getFattoreMoltUnitMisSupp() {
		return _fattoreMoltUnitMisSupp;
	}

	public void setFattoreMoltUnitMisSupp(double fattoreMoltUnitMisSupp) {
		_fattoreMoltUnitMisSupp = fattoreMoltUnitMisSupp;
	}

	public double getFattoreDivUnitMisSupp() {
		return _fattoreDivUnitMisSupp;
	}

	public void setFattoreDivUnitMisSupp(double fattoreDivUnitMisSupp) {
		_fattoreDivUnitMisSupp = fattoreDivUnitMisSupp;
	}

	public int getStampaetichette() {
		return _stampaetichette;
	}

	public void setStampaetichette(int stampaetichette) {
		_stampaetichette = stampaetichette;
	}

	public boolean getGestioneVarianti() {
		return _gestioneVarianti;
	}

	public boolean isGestioneVarianti() {
		return _gestioneVarianti;
	}

	public void setGestioneVarianti(boolean gestioneVarianti) {
		_gestioneVarianti = gestioneVarianti;
	}

	public boolean getGestioneVariantiDistintaBase() {
		return _gestioneVariantiDistintaBase;
	}

	public boolean isGestioneVariantiDistintaBase() {
		return _gestioneVariantiDistintaBase;
	}

	public void setGestioneVariantiDistintaBase(
		boolean gestioneVariantiDistintaBase) {
		_gestioneVariantiDistintaBase = gestioneVariantiDistintaBase;
	}

	public String getLiberoString1() {
		return _liberoString1;
	}

	public void setLiberoString1(String liberoString1) {
		_liberoString1 = liberoString1;
	}

	public String getLiberoString2() {
		return _liberoString2;
	}

	public void setLiberoString2(String liberoString2) {
		_liberoString2 = liberoString2;
	}

	public String getLiberoString3() {
		return _liberoString3;
	}

	public void setLiberoString3(String liberoString3) {
		_liberoString3 = liberoString3;
	}

	public String getLiberoString4() {
		return _liberoString4;
	}

	public void setLiberoString4(String liberoString4) {
		_liberoString4 = liberoString4;
	}

	public String getLiberoString5() {
		return _liberoString5;
	}

	public void setLiberoString5(String liberoString5) {
		_liberoString5 = liberoString5;
	}

	public Date getLiberoDate1() {
		return _liberoDate1;
	}

	public void setLiberoDate1(Date liberoDate1) {
		_liberoDate1 = liberoDate1;
	}

	public Date getLiberoDate2() {
		return _liberoDate2;
	}

	public void setLiberoDate2(Date liberoDate2) {
		_liberoDate2 = liberoDate2;
	}

	public Date getLiberoDate3() {
		return _liberoDate3;
	}

	public void setLiberoDate3(Date liberoDate3) {
		_liberoDate3 = liberoDate3;
	}

	public Date getLiberoDate4() {
		return _liberoDate4;
	}

	public void setLiberoDate4(Date liberoDate4) {
		_liberoDate4 = liberoDate4;
	}

	public Date getLiberoDate5() {
		return _liberoDate5;
	}

	public void setLiberoDate5(Date liberoDate5) {
		_liberoDate5 = liberoDate5;
	}

	public long getLiberoLong1() {
		return _liberoLong1;
	}

	public void setLiberoLong1(long liberoLong1) {
		_liberoLong1 = liberoLong1;
	}

	public long getLiberoLong2() {
		return _liberoLong2;
	}

	public void setLiberoLong2(long liberoLong2) {
		_liberoLong2 = liberoLong2;
	}

	public long getLiberoLong3() {
		return _liberoLong3;
	}

	public void setLiberoLong3(long liberoLong3) {
		_liberoLong3 = liberoLong3;
	}

	public long getLiberoLong4() {
		return _liberoLong4;
	}

	public void setLiberoLong4(long liberoLong4) {
		_liberoLong4 = liberoLong4;
	}

	public long getLiberoLong5() {
		return _liberoLong5;
	}

	public void setLiberoLong5(long liberoLong5) {
		_liberoLong5 = liberoLong5;
	}

	public double getLiberoDouble1() {
		return _liberoDouble1;
	}

	public void setLiberoDouble1(double liberoDouble1) {
		_liberoDouble1 = liberoDouble1;
	}

	public double getLiberoDouble2() {
		return _liberoDouble2;
	}

	public void setLiberoDouble2(double liberoDouble2) {
		_liberoDouble2 = liberoDouble2;
	}

	public double getLiberoDouble3() {
		return _liberoDouble3;
	}

	public void setLiberoDouble3(double liberoDouble3) {
		_liberoDouble3 = liberoDouble3;
	}

	public double getLiberoDouble4() {
		return _liberoDouble4;
	}

	public void setLiberoDouble4(double liberoDouble4) {
		_liberoDouble4 = liberoDouble4;
	}

	public double getLiberoDouble5() {
		return _liberoDouble5;
	}

	public void setLiberoDouble5(double liberoDouble5) {
		_liberoDouble5 = liberoDouble5;
	}

	private String _codiceArticolo;
	private String _descrizione;
	private String _descrizioneAggiuntiva;
	private String _descrizioneBreveFiscale;
	private String _pathImmagine;
	private String _unitaMisura;
	private String _categoriaMerceologica;
	private String _categoriaFiscale;
	private String _codiceIVA;
	private String _codiceProvvigioni;
	private String _codiceSconto1;
	private String _codiceSconto2;
	private String _categoriaInventario;
	private double _prezzo1;
	private double _prezzo2;
	private double _prezzo3;
	private String _codiceIVAPrezzo1;
	private String _codiceIVAPrezzo2;
	private String _codiceIVAPrezzo3;
	private int _numeroDecimaliPrezzo;
	private int _numeroDecimaliQuantita;
	private int _numeroMesiPerinvenduto;
	private String _unitaMisura2;
	private double _fattoreMoltiplicativo;
	private double _fattoreDivisione;
	private String _ragruppamentoLibero;
	private int _tipoLivelloDistintaBase;
	private int _tipoCostoStatistico;
	private int _tipoGestioneArticolo;
	private int _tipoApprovvigionamentoArticolo;
	private double _nomenclaturaCombinata;
	private String _descrizioneEstesa;
	private boolean _generazioneMovimenti;
	private int _tipoEsplosioneDistintaBase;
	private int _livelloMaxEsplosioneDistintaBase;
	private boolean _valorizzazioneMagazzino;
	private boolean _abilitatoEC;
	private String _categoriaEC;
	private double _quantitaMinimaEC;
	private double _quantitaDefaultEC;
	private String _codiceSchedaTecnicaEC;
	private int _giorniPrevistaConsegna;
	private boolean _gestioneLotti;
	private boolean _creazioneLotti;
	private String _prefissoCodiceLottoAutomatico;
	private int _numeroCifreProgressivo;
	private boolean _scaricoAutomaticoLotti;
	private int _giorniInizioValiditaLotto;
	private int _giorniValiditaLotto;
	private int _giorniPreavvisoScadenzaLotto;
	private double _fattoreMoltQuantRiservata;
	private double _fattoreDivQuantRiservata;
	private boolean _obsoleto;
	private boolean _comeImballo;
	private double _fattoreMoltCalcoloImballo;
	private double _fattoreDivCalcoloImballo;
	private double _pesoLordo;
	private double _tara;
	private int _calcoloVolume;
	private double _lunghezza;
	private double _larghezza;
	private double _profondita;
	private String _codiceImballo;
	private double _fattoreMoltUnitMisSupp;
	private double _fattoreDivUnitMisSupp;
	private int _stampaetichette;
	private boolean _gestioneVarianti;
	private boolean _gestioneVariantiDistintaBase;
	private String _liberoString1;
	private String _liberoString2;
	private String _liberoString3;
	private String _liberoString4;
	private String _liberoString5;
	private Date _liberoDate1;
	private Date _liberoDate2;
	private Date _liberoDate3;
	private Date _liberoDate4;
	private Date _liberoDate5;
	private long _liberoLong1;
	private long _liberoLong2;
	private long _liberoLong3;
	private long _liberoLong4;
	private long _liberoLong5;
	private double _liberoDouble1;
	private double _liberoDouble2;
	private double _liberoDouble3;
	private double _liberoDouble4;
	private double _liberoDouble5;
}