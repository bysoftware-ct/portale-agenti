/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link UltimiPrezziArticoli}.
 * </p>
 *
 * @author Mario Torrisi
 * @see UltimiPrezziArticoli
 * @generated
 */
public class UltimiPrezziArticoliWrapper implements UltimiPrezziArticoli,
	ModelWrapper<UltimiPrezziArticoli> {
	public UltimiPrezziArticoliWrapper(
		UltimiPrezziArticoli ultimiPrezziArticoli) {
		_ultimiPrezziArticoli = ultimiPrezziArticoli;
	}

	@Override
	public Class<?> getModelClass() {
		return UltimiPrezziArticoli.class;
	}

	@Override
	public String getModelClassName() {
		return UltimiPrezziArticoli.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceSoggetto", getCodiceSoggetto());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("dataDocumento", getDataDocumento());
		attributes.put("codiceDivisa", getCodiceDivisa());
		attributes.put("prezzo", getPrezzo());
		attributes.put("sconto1", getSconto1());
		attributes.put("sconto2", getSconto2());
		attributes.put("sconto3", getSconto3());
		attributes.put("scontoChiusura", getScontoChiusura());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceSoggetto = (String)attributes.get("codiceSoggetto");

		if (codiceSoggetto != null) {
			setCodiceSoggetto(codiceSoggetto);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		Date dataDocumento = (Date)attributes.get("dataDocumento");

		if (dataDocumento != null) {
			setDataDocumento(dataDocumento);
		}

		String codiceDivisa = (String)attributes.get("codiceDivisa");

		if (codiceDivisa != null) {
			setCodiceDivisa(codiceDivisa);
		}

		Double prezzo = (Double)attributes.get("prezzo");

		if (prezzo != null) {
			setPrezzo(prezzo);
		}

		Double sconto1 = (Double)attributes.get("sconto1");

		if (sconto1 != null) {
			setSconto1(sconto1);
		}

		Double sconto2 = (Double)attributes.get("sconto2");

		if (sconto2 != null) {
			setSconto2(sconto2);
		}

		Double sconto3 = (Double)attributes.get("sconto3");

		if (sconto3 != null) {
			setSconto3(sconto3);
		}

		Double scontoChiusura = (Double)attributes.get("scontoChiusura");

		if (scontoChiusura != null) {
			setScontoChiusura(scontoChiusura);
		}
	}

	/**
	* Returns the primary key of this ultimi prezzi articoli.
	*
	* @return the primary key of this ultimi prezzi articoli
	*/
	@Override
	public it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK getPrimaryKey() {
		return _ultimiPrezziArticoli.getPrimaryKey();
	}

	/**
	* Sets the primary key of this ultimi prezzi articoli.
	*
	* @param primaryKey the primary key of this ultimi prezzi articoli
	*/
	@Override
	public void setPrimaryKey(
		it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK primaryKey) {
		_ultimiPrezziArticoli.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the tipo soggetto of this ultimi prezzi articoli.
	*
	* @return the tipo soggetto of this ultimi prezzi articoli
	*/
	@Override
	public boolean getTipoSoggetto() {
		return _ultimiPrezziArticoli.getTipoSoggetto();
	}

	/**
	* Returns <code>true</code> if this ultimi prezzi articoli is tipo soggetto.
	*
	* @return <code>true</code> if this ultimi prezzi articoli is tipo soggetto; <code>false</code> otherwise
	*/
	@Override
	public boolean isTipoSoggetto() {
		return _ultimiPrezziArticoli.isTipoSoggetto();
	}

	/**
	* Sets whether this ultimi prezzi articoli is tipo soggetto.
	*
	* @param tipoSoggetto the tipo soggetto of this ultimi prezzi articoli
	*/
	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_ultimiPrezziArticoli.setTipoSoggetto(tipoSoggetto);
	}

	/**
	* Returns the codice soggetto of this ultimi prezzi articoli.
	*
	* @return the codice soggetto of this ultimi prezzi articoli
	*/
	@Override
	public java.lang.String getCodiceSoggetto() {
		return _ultimiPrezziArticoli.getCodiceSoggetto();
	}

	/**
	* Sets the codice soggetto of this ultimi prezzi articoli.
	*
	* @param codiceSoggetto the codice soggetto of this ultimi prezzi articoli
	*/
	@Override
	public void setCodiceSoggetto(java.lang.String codiceSoggetto) {
		_ultimiPrezziArticoli.setCodiceSoggetto(codiceSoggetto);
	}

	/**
	* Returns the codice articolo of this ultimi prezzi articoli.
	*
	* @return the codice articolo of this ultimi prezzi articoli
	*/
	@Override
	public java.lang.String getCodiceArticolo() {
		return _ultimiPrezziArticoli.getCodiceArticolo();
	}

	/**
	* Sets the codice articolo of this ultimi prezzi articoli.
	*
	* @param codiceArticolo the codice articolo of this ultimi prezzi articoli
	*/
	@Override
	public void setCodiceArticolo(java.lang.String codiceArticolo) {
		_ultimiPrezziArticoli.setCodiceArticolo(codiceArticolo);
	}

	/**
	* Returns the codice variante of this ultimi prezzi articoli.
	*
	* @return the codice variante of this ultimi prezzi articoli
	*/
	@Override
	public java.lang.String getCodiceVariante() {
		return _ultimiPrezziArticoli.getCodiceVariante();
	}

	/**
	* Sets the codice variante of this ultimi prezzi articoli.
	*
	* @param codiceVariante the codice variante of this ultimi prezzi articoli
	*/
	@Override
	public void setCodiceVariante(java.lang.String codiceVariante) {
		_ultimiPrezziArticoli.setCodiceVariante(codiceVariante);
	}

	/**
	* Returns the data documento of this ultimi prezzi articoli.
	*
	* @return the data documento of this ultimi prezzi articoli
	*/
	@Override
	public java.util.Date getDataDocumento() {
		return _ultimiPrezziArticoli.getDataDocumento();
	}

	/**
	* Sets the data documento of this ultimi prezzi articoli.
	*
	* @param dataDocumento the data documento of this ultimi prezzi articoli
	*/
	@Override
	public void setDataDocumento(java.util.Date dataDocumento) {
		_ultimiPrezziArticoli.setDataDocumento(dataDocumento);
	}

	/**
	* Returns the codice divisa of this ultimi prezzi articoli.
	*
	* @return the codice divisa of this ultimi prezzi articoli
	*/
	@Override
	public java.lang.String getCodiceDivisa() {
		return _ultimiPrezziArticoli.getCodiceDivisa();
	}

	/**
	* Sets the codice divisa of this ultimi prezzi articoli.
	*
	* @param codiceDivisa the codice divisa of this ultimi prezzi articoli
	*/
	@Override
	public void setCodiceDivisa(java.lang.String codiceDivisa) {
		_ultimiPrezziArticoli.setCodiceDivisa(codiceDivisa);
	}

	/**
	* Returns the prezzo of this ultimi prezzi articoli.
	*
	* @return the prezzo of this ultimi prezzi articoli
	*/
	@Override
	public double getPrezzo() {
		return _ultimiPrezziArticoli.getPrezzo();
	}

	/**
	* Sets the prezzo of this ultimi prezzi articoli.
	*
	* @param prezzo the prezzo of this ultimi prezzi articoli
	*/
	@Override
	public void setPrezzo(double prezzo) {
		_ultimiPrezziArticoli.setPrezzo(prezzo);
	}

	/**
	* Returns the sconto1 of this ultimi prezzi articoli.
	*
	* @return the sconto1 of this ultimi prezzi articoli
	*/
	@Override
	public double getSconto1() {
		return _ultimiPrezziArticoli.getSconto1();
	}

	/**
	* Sets the sconto1 of this ultimi prezzi articoli.
	*
	* @param sconto1 the sconto1 of this ultimi prezzi articoli
	*/
	@Override
	public void setSconto1(double sconto1) {
		_ultimiPrezziArticoli.setSconto1(sconto1);
	}

	/**
	* Returns the sconto2 of this ultimi prezzi articoli.
	*
	* @return the sconto2 of this ultimi prezzi articoli
	*/
	@Override
	public double getSconto2() {
		return _ultimiPrezziArticoli.getSconto2();
	}

	/**
	* Sets the sconto2 of this ultimi prezzi articoli.
	*
	* @param sconto2 the sconto2 of this ultimi prezzi articoli
	*/
	@Override
	public void setSconto2(double sconto2) {
		_ultimiPrezziArticoli.setSconto2(sconto2);
	}

	/**
	* Returns the sconto3 of this ultimi prezzi articoli.
	*
	* @return the sconto3 of this ultimi prezzi articoli
	*/
	@Override
	public double getSconto3() {
		return _ultimiPrezziArticoli.getSconto3();
	}

	/**
	* Sets the sconto3 of this ultimi prezzi articoli.
	*
	* @param sconto3 the sconto3 of this ultimi prezzi articoli
	*/
	@Override
	public void setSconto3(double sconto3) {
		_ultimiPrezziArticoli.setSconto3(sconto3);
	}

	/**
	* Returns the sconto chiusura of this ultimi prezzi articoli.
	*
	* @return the sconto chiusura of this ultimi prezzi articoli
	*/
	@Override
	public double getScontoChiusura() {
		return _ultimiPrezziArticoli.getScontoChiusura();
	}

	/**
	* Sets the sconto chiusura of this ultimi prezzi articoli.
	*
	* @param scontoChiusura the sconto chiusura of this ultimi prezzi articoli
	*/
	@Override
	public void setScontoChiusura(double scontoChiusura) {
		_ultimiPrezziArticoli.setScontoChiusura(scontoChiusura);
	}

	@Override
	public boolean isNew() {
		return _ultimiPrezziArticoli.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_ultimiPrezziArticoli.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _ultimiPrezziArticoli.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_ultimiPrezziArticoli.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _ultimiPrezziArticoli.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _ultimiPrezziArticoli.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_ultimiPrezziArticoli.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _ultimiPrezziArticoli.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_ultimiPrezziArticoli.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_ultimiPrezziArticoli.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_ultimiPrezziArticoli.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new UltimiPrezziArticoliWrapper((UltimiPrezziArticoli)_ultimiPrezziArticoli.clone());
	}

	@Override
	public int compareTo(
		it.bysoftware.ct.model.UltimiPrezziArticoli ultimiPrezziArticoli) {
		return _ultimiPrezziArticoli.compareTo(ultimiPrezziArticoli);
	}

	@Override
	public int hashCode() {
		return _ultimiPrezziArticoli.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.UltimiPrezziArticoli> toCacheModel() {
		return _ultimiPrezziArticoli.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.UltimiPrezziArticoli toEscapedModel() {
		return new UltimiPrezziArticoliWrapper(_ultimiPrezziArticoli.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.UltimiPrezziArticoli toUnescapedModel() {
		return new UltimiPrezziArticoliWrapper(_ultimiPrezziArticoli.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _ultimiPrezziArticoli.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _ultimiPrezziArticoli.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_ultimiPrezziArticoli.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UltimiPrezziArticoliWrapper)) {
			return false;
		}

		UltimiPrezziArticoliWrapper ultimiPrezziArticoliWrapper = (UltimiPrezziArticoliWrapper)obj;

		if (Validator.equals(_ultimiPrezziArticoli,
					ultimiPrezziArticoliWrapper._ultimiPrezziArticoli)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public UltimiPrezziArticoli getWrappedUltimiPrezziArticoli() {
		return _ultimiPrezziArticoli;
	}

	@Override
	public UltimiPrezziArticoli getWrappedModel() {
		return _ultimiPrezziArticoli;
	}

	@Override
	public void resetOriginalValues() {
		_ultimiPrezziArticoli.resetOriginalValues();
	}

	private UltimiPrezziArticoli _ultimiPrezziArticoli;
}