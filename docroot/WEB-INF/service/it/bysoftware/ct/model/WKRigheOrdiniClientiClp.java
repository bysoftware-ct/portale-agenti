/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.WKRigheOrdiniClientiLocalServiceUtil;
import it.bysoftware.ct.service.persistence.WKRigheOrdiniClientiPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class WKRigheOrdiniClientiClp extends BaseModelImpl<WKRigheOrdiniClienti>
	implements WKRigheOrdiniClienti {
	public WKRigheOrdiniClientiClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return WKRigheOrdiniClienti.class;
	}

	@Override
	public String getModelClassName() {
		return WKRigheOrdiniClienti.class.getName();
	}

	@Override
	public WKRigheOrdiniClientiPK getPrimaryKey() {
		return new WKRigheOrdiniClientiPK(_anno, _tipoOrdine, _numeroOrdine,
			_numeroRigo);
	}

	@Override
	public void setPrimaryKey(WKRigheOrdiniClientiPK primaryKey) {
		setAnno(primaryKey.anno);
		setTipoOrdine(primaryKey.tipoOrdine);
		setNumeroOrdine(primaryKey.numeroOrdine);
		setNumeroRigo(primaryKey.numeroRigo);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new WKRigheOrdiniClientiPK(_anno, _tipoOrdine, _numeroOrdine,
			_numeroRigo);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((WKRigheOrdiniClientiPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("anno", getAnno());
		attributes.put("tipoOrdine", getTipoOrdine());
		attributes.put("numeroOrdine", getNumeroOrdine());
		attributes.put("numeroRigo", getNumeroRigo());
		attributes.put("statoRigo", getStatoRigo());
		attributes.put("tipoRigo", getTipoRigo());
		attributes.put("codiceDepositoMov", getCodiceDepositoMov());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("descrizione", getDescrizione());
		attributes.put("codiceUnitMis", getCodiceUnitMis());
		attributes.put("decimaliQuant", getDecimaliQuant());
		attributes.put("quantita1", getQuantita1());
		attributes.put("quantita2", getQuantita2());
		attributes.put("quantita3", getQuantita3());
		attributes.put("quantita", getQuantita());
		attributes.put("codiceUnitMis2", getCodiceUnitMis2());
		attributes.put("quantitaUnitMis2", getQuantitaUnitMis2());
		attributes.put("decimaliPrezzo", getDecimaliPrezzo());
		attributes.put("prezzo", getPrezzo());
		attributes.put("importoLordo", getImportoLordo());
		attributes.put("sconto1", getSconto1());
		attributes.put("sconto2", getSconto2());
		attributes.put("sconto3", getSconto3());
		attributes.put("importoNetto", getImportoNetto());
		attributes.put("importo", getImporto());
		attributes.put("codiceIVAFatturazione", getCodiceIVAFatturazione());
		attributes.put("codiceCliente", getCodiceCliente());
		attributes.put("riferimentoOrdineCliente", getRiferimentoOrdineCliente());
		attributes.put("dataOridine", getDataOridine());
		attributes.put("statoEvasione", getStatoEvasione());
		attributes.put("dataPrevistaConsegna", getDataPrevistaConsegna());
		attributes.put("dataRegistrazioneOrdine", getDataRegistrazioneOrdine());
		attributes.put("libStr1", getLibStr1());
		attributes.put("libStr2", getLibStr2());
		attributes.put("libStr3", getLibStr3());
		attributes.put("libDbl1", getLibDbl1());
		attributes.put("libDbl2", getLibDbl2());
		attributes.put("libDbl3", getLibDbl3());
		attributes.put("libDat1", getLibDat1());
		attributes.put("libDat2", getLibDat2());
		attributes.put("libDat3", getLibDat3());
		attributes.put("libLng1", getLibLng1());
		attributes.put("libLng2", getLibLng2());
		attributes.put("libLng3", getLibLng3());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer anno = (Integer)attributes.get("anno");

		if (anno != null) {
			setAnno(anno);
		}

		Integer tipoOrdine = (Integer)attributes.get("tipoOrdine");

		if (tipoOrdine != null) {
			setTipoOrdine(tipoOrdine);
		}

		Integer numeroOrdine = (Integer)attributes.get("numeroOrdine");

		if (numeroOrdine != null) {
			setNumeroOrdine(numeroOrdine);
		}

		Integer numeroRigo = (Integer)attributes.get("numeroRigo");

		if (numeroRigo != null) {
			setNumeroRigo(numeroRigo);
		}

		Boolean statoRigo = (Boolean)attributes.get("statoRigo");

		if (statoRigo != null) {
			setStatoRigo(statoRigo);
		}

		Integer tipoRigo = (Integer)attributes.get("tipoRigo");

		if (tipoRigo != null) {
			setTipoRigo(tipoRigo);
		}

		String codiceDepositoMov = (String)attributes.get("codiceDepositoMov");

		if (codiceDepositoMov != null) {
			setCodiceDepositoMov(codiceDepositoMov);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		String codiceUnitMis = (String)attributes.get("codiceUnitMis");

		if (codiceUnitMis != null) {
			setCodiceUnitMis(codiceUnitMis);
		}

		Integer decimaliQuant = (Integer)attributes.get("decimaliQuant");

		if (decimaliQuant != null) {
			setDecimaliQuant(decimaliQuant);
		}

		Double quantita1 = (Double)attributes.get("quantita1");

		if (quantita1 != null) {
			setQuantita1(quantita1);
		}

		Double quantita2 = (Double)attributes.get("quantita2");

		if (quantita2 != null) {
			setQuantita2(quantita2);
		}

		Double quantita3 = (Double)attributes.get("quantita3");

		if (quantita3 != null) {
			setQuantita3(quantita3);
		}

		Double quantita = (Double)attributes.get("quantita");

		if (quantita != null) {
			setQuantita(quantita);
		}

		String codiceUnitMis2 = (String)attributes.get("codiceUnitMis2");

		if (codiceUnitMis2 != null) {
			setCodiceUnitMis2(codiceUnitMis2);
		}

		Double quantitaUnitMis2 = (Double)attributes.get("quantitaUnitMis2");

		if (quantitaUnitMis2 != null) {
			setQuantitaUnitMis2(quantitaUnitMis2);
		}

		Integer decimaliPrezzo = (Integer)attributes.get("decimaliPrezzo");

		if (decimaliPrezzo != null) {
			setDecimaliPrezzo(decimaliPrezzo);
		}

		Double prezzo = (Double)attributes.get("prezzo");

		if (prezzo != null) {
			setPrezzo(prezzo);
		}

		Double importoLordo = (Double)attributes.get("importoLordo");

		if (importoLordo != null) {
			setImportoLordo(importoLordo);
		}

		Double sconto1 = (Double)attributes.get("sconto1");

		if (sconto1 != null) {
			setSconto1(sconto1);
		}

		Double sconto2 = (Double)attributes.get("sconto2");

		if (sconto2 != null) {
			setSconto2(sconto2);
		}

		Double sconto3 = (Double)attributes.get("sconto3");

		if (sconto3 != null) {
			setSconto3(sconto3);
		}

		Double importoNetto = (Double)attributes.get("importoNetto");

		if (importoNetto != null) {
			setImportoNetto(importoNetto);
		}

		Double importo = (Double)attributes.get("importo");

		if (importo != null) {
			setImporto(importo);
		}

		String codiceIVAFatturazione = (String)attributes.get(
				"codiceIVAFatturazione");

		if (codiceIVAFatturazione != null) {
			setCodiceIVAFatturazione(codiceIVAFatturazione);
		}

		String codiceCliente = (String)attributes.get("codiceCliente");

		if (codiceCliente != null) {
			setCodiceCliente(codiceCliente);
		}

		String riferimentoOrdineCliente = (String)attributes.get(
				"riferimentoOrdineCliente");

		if (riferimentoOrdineCliente != null) {
			setRiferimentoOrdineCliente(riferimentoOrdineCliente);
		}

		Date dataOridine = (Date)attributes.get("dataOridine");

		if (dataOridine != null) {
			setDataOridine(dataOridine);
		}

		Boolean statoEvasione = (Boolean)attributes.get("statoEvasione");

		if (statoEvasione != null) {
			setStatoEvasione(statoEvasione);
		}

		Date dataPrevistaConsegna = (Date)attributes.get("dataPrevistaConsegna");

		if (dataPrevistaConsegna != null) {
			setDataPrevistaConsegna(dataPrevistaConsegna);
		}

		Date dataRegistrazioneOrdine = (Date)attributes.get(
				"dataRegistrazioneOrdine");

		if (dataRegistrazioneOrdine != null) {
			setDataRegistrazioneOrdine(dataRegistrazioneOrdine);
		}

		String libStr1 = (String)attributes.get("libStr1");

		if (libStr1 != null) {
			setLibStr1(libStr1);
		}

		String libStr2 = (String)attributes.get("libStr2");

		if (libStr2 != null) {
			setLibStr2(libStr2);
		}

		String libStr3 = (String)attributes.get("libStr3");

		if (libStr3 != null) {
			setLibStr3(libStr3);
		}

		Double libDbl1 = (Double)attributes.get("libDbl1");

		if (libDbl1 != null) {
			setLibDbl1(libDbl1);
		}

		Double libDbl2 = (Double)attributes.get("libDbl2");

		if (libDbl2 != null) {
			setLibDbl2(libDbl2);
		}

		Double libDbl3 = (Double)attributes.get("libDbl3");

		if (libDbl3 != null) {
			setLibDbl3(libDbl3);
		}

		Date libDat1 = (Date)attributes.get("libDat1");

		if (libDat1 != null) {
			setLibDat1(libDat1);
		}

		Date libDat2 = (Date)attributes.get("libDat2");

		if (libDat2 != null) {
			setLibDat2(libDat2);
		}

		Date libDat3 = (Date)attributes.get("libDat3");

		if (libDat3 != null) {
			setLibDat3(libDat3);
		}

		Long libLng1 = (Long)attributes.get("libLng1");

		if (libLng1 != null) {
			setLibLng1(libLng1);
		}

		Long libLng2 = (Long)attributes.get("libLng2");

		if (libLng2 != null) {
			setLibLng2(libLng2);
		}

		Long libLng3 = (Long)attributes.get("libLng3");

		if (libLng3 != null) {
			setLibLng3(libLng3);
		}
	}

	@Override
	public int getAnno() {
		return _anno;
	}

	@Override
	public void setAnno(int anno) {
		_anno = anno;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setAnno", int.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, anno);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoOrdine() {
		return _tipoOrdine;
	}

	@Override
	public void setTipoOrdine(int tipoOrdine) {
		_tipoOrdine = tipoOrdine;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoOrdine", int.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, tipoOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroOrdine() {
		return _numeroOrdine;
	}

	@Override
	public void setNumeroOrdine(int numeroOrdine) {
		_numeroOrdine = numeroOrdine;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroOrdine", int.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, numeroOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroRigo() {
		return _numeroRigo;
	}

	@Override
	public void setNumeroRigo(int numeroRigo) {
		_numeroRigo = numeroRigo;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroRigo", int.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, numeroRigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getStatoRigo() {
		return _statoRigo;
	}

	@Override
	public boolean isStatoRigo() {
		return _statoRigo;
	}

	@Override
	public void setStatoRigo(boolean statoRigo) {
		_statoRigo = statoRigo;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setStatoRigo", boolean.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, statoRigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoRigo() {
		return _tipoRigo;
	}

	@Override
	public void setTipoRigo(int tipoRigo) {
		_tipoRigo = tipoRigo;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoRigo", int.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, tipoRigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDepositoMov() {
		return _codiceDepositoMov;
	}

	@Override
	public void setCodiceDepositoMov(String codiceDepositoMov) {
		_codiceDepositoMov = codiceDepositoMov;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDepositoMov",
						String.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel,
					codiceDepositoMov);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	@Override
	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceArticolo",
						String.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, codiceArticolo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceVariante() {
		return _codiceVariante;
	}

	@Override
	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceVariante",
						String.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, codiceVariante);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizione() {
		return _descrizione;
	}

	@Override
	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizione", String.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, descrizione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceUnitMis() {
		return _codiceUnitMis;
	}

	@Override
	public void setCodiceUnitMis(String codiceUnitMis) {
		_codiceUnitMis = codiceUnitMis;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceUnitMis", String.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, codiceUnitMis);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getDecimaliQuant() {
		return _decimaliQuant;
	}

	@Override
	public void setDecimaliQuant(int decimaliQuant) {
		_decimaliQuant = decimaliQuant;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDecimaliQuant", int.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, decimaliQuant);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantita1() {
		return _quantita1;
	}

	@Override
	public void setQuantita1(double quantita1) {
		_quantita1 = quantita1;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantita1", double.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, quantita1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantita2() {
		return _quantita2;
	}

	@Override
	public void setQuantita2(double quantita2) {
		_quantita2 = quantita2;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantita2", double.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, quantita2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantita3() {
		return _quantita3;
	}

	@Override
	public void setQuantita3(double quantita3) {
		_quantita3 = quantita3;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantita3", double.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, quantita3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantita() {
		return _quantita;
	}

	@Override
	public void setQuantita(double quantita) {
		_quantita = quantita;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantita", double.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, quantita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceUnitMis2() {
		return _codiceUnitMis2;
	}

	@Override
	public void setCodiceUnitMis2(String codiceUnitMis2) {
		_codiceUnitMis2 = codiceUnitMis2;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceUnitMis2",
						String.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, codiceUnitMis2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaUnitMis2() {
		return _quantitaUnitMis2;
	}

	@Override
	public void setQuantitaUnitMis2(double quantitaUnitMis2) {
		_quantitaUnitMis2 = quantitaUnitMis2;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaUnitMis2",
						double.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, quantitaUnitMis2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getDecimaliPrezzo() {
		return _decimaliPrezzo;
	}

	@Override
	public void setDecimaliPrezzo(int decimaliPrezzo) {
		_decimaliPrezzo = decimaliPrezzo;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDecimaliPrezzo", int.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, decimaliPrezzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPrezzo() {
		return _prezzo;
	}

	@Override
	public void setPrezzo(double prezzo) {
		_prezzo = prezzo;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setPrezzo", double.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, prezzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoLordo() {
		return _importoLordo;
	}

	@Override
	public void setImportoLordo(double importoLordo) {
		_importoLordo = importoLordo;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoLordo", double.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, importoLordo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSconto1() {
		return _sconto1;
	}

	@Override
	public void setSconto1(double sconto1) {
		_sconto1 = sconto1;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setSconto1", double.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, sconto1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSconto2() {
		return _sconto2;
	}

	@Override
	public void setSconto2(double sconto2) {
		_sconto2 = sconto2;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setSconto2", double.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, sconto2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSconto3() {
		return _sconto3;
	}

	@Override
	public void setSconto3(double sconto3) {
		_sconto3 = sconto3;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setSconto3", double.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, sconto3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoNetto() {
		return _importoNetto;
	}

	@Override
	public void setImportoNetto(double importoNetto) {
		_importoNetto = importoNetto;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoNetto", double.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, importoNetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImporto() {
		return _importo;
	}

	@Override
	public void setImporto(double importo) {
		_importo = importo;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setImporto", double.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, importo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVAFatturazione() {
		return _codiceIVAFatturazione;
	}

	@Override
	public void setCodiceIVAFatturazione(String codiceIVAFatturazione) {
		_codiceIVAFatturazione = codiceIVAFatturazione;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVAFatturazione",
						String.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel,
					codiceIVAFatturazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCliente() {
		return _codiceCliente;
	}

	@Override
	public void setCodiceCliente(String codiceCliente) {
		_codiceCliente = codiceCliente;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCliente", String.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, codiceCliente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRiferimentoOrdineCliente() {
		return _riferimentoOrdineCliente;
	}

	@Override
	public void setRiferimentoOrdineCliente(String riferimentoOrdineCliente) {
		_riferimentoOrdineCliente = riferimentoOrdineCliente;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setRiferimentoOrdineCliente",
						String.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel,
					riferimentoOrdineCliente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataOridine() {
		return _dataOridine;
	}

	@Override
	public void setDataOridine(Date dataOridine) {
		_dataOridine = dataOridine;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataOridine", Date.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, dataOridine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getStatoEvasione() {
		return _statoEvasione;
	}

	@Override
	public boolean isStatoEvasione() {
		return _statoEvasione;
	}

	@Override
	public void setStatoEvasione(boolean statoEvasione) {
		_statoEvasione = statoEvasione;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setStatoEvasione",
						boolean.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, statoEvasione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataPrevistaConsegna() {
		return _dataPrevistaConsegna;
	}

	@Override
	public void setDataPrevistaConsegna(Date dataPrevistaConsegna) {
		_dataPrevistaConsegna = dataPrevistaConsegna;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataPrevistaConsegna",
						Date.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel,
					dataPrevistaConsegna);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataRegistrazioneOrdine() {
		return _dataRegistrazioneOrdine;
	}

	@Override
	public void setDataRegistrazioneOrdine(Date dataRegistrazioneOrdine) {
		_dataRegistrazioneOrdine = dataRegistrazioneOrdine;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataRegistrazioneOrdine",
						Date.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel,
					dataRegistrazioneOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr1() {
		return _libStr1;
	}

	@Override
	public void setLibStr1(String libStr1) {
		_libStr1 = libStr1;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr1", String.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, libStr1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr2() {
		return _libStr2;
	}

	@Override
	public void setLibStr2(String libStr2) {
		_libStr2 = libStr2;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr2", String.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, libStr2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr3() {
		return _libStr3;
	}

	@Override
	public void setLibStr3(String libStr3) {
		_libStr3 = libStr3;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr3", String.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, libStr3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl1() {
		return _libDbl1;
	}

	@Override
	public void setLibDbl1(double libDbl1) {
		_libDbl1 = libDbl1;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl1", double.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, libDbl1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl2() {
		return _libDbl2;
	}

	@Override
	public void setLibDbl2(double libDbl2) {
		_libDbl2 = libDbl2;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl2", double.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, libDbl2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl3() {
		return _libDbl3;
	}

	@Override
	public void setLibDbl3(double libDbl3) {
		_libDbl3 = libDbl3;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl3", double.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, libDbl3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat1() {
		return _libDat1;
	}

	@Override
	public void setLibDat1(Date libDat1) {
		_libDat1 = libDat1;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat1", Date.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, libDat1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat2() {
		return _libDat2;
	}

	@Override
	public void setLibDat2(Date libDat2) {
		_libDat2 = libDat2;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat2", Date.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, libDat2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat3() {
		return _libDat3;
	}

	@Override
	public void setLibDat3(Date libDat3) {
		_libDat3 = libDat3;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat3", Date.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, libDat3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng1() {
		return _libLng1;
	}

	@Override
	public void setLibLng1(long libLng1) {
		_libLng1 = libLng1;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng1", long.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, libLng1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng2() {
		return _libLng2;
	}

	@Override
	public void setLibLng2(long libLng2) {
		_libLng2 = libLng2;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng2", long.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, libLng2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng3() {
		return _libLng3;
	}

	@Override
	public void setLibLng3(long libLng3) {
		_libLng3 = libLng3;

		if (_wkRigheOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkRigheOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng3", long.class);

				method.invoke(_wkRigheOrdiniClientiRemoteModel, libLng3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getWKRigheOrdiniClientiRemoteModel() {
		return _wkRigheOrdiniClientiRemoteModel;
	}

	public void setWKRigheOrdiniClientiRemoteModel(
		BaseModel<?> wkRigheOrdiniClientiRemoteModel) {
		_wkRigheOrdiniClientiRemoteModel = wkRigheOrdiniClientiRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _wkRigheOrdiniClientiRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_wkRigheOrdiniClientiRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			WKRigheOrdiniClientiLocalServiceUtil.addWKRigheOrdiniClienti(this);
		}
		else {
			WKRigheOrdiniClientiLocalServiceUtil.updateWKRigheOrdiniClienti(this);
		}
	}

	@Override
	public WKRigheOrdiniClienti toEscapedModel() {
		return (WKRigheOrdiniClienti)ProxyUtil.newProxyInstance(WKRigheOrdiniClienti.class.getClassLoader(),
			new Class[] { WKRigheOrdiniClienti.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		WKRigheOrdiniClientiClp clone = new WKRigheOrdiniClientiClp();

		clone.setAnno(getAnno());
		clone.setTipoOrdine(getTipoOrdine());
		clone.setNumeroOrdine(getNumeroOrdine());
		clone.setNumeroRigo(getNumeroRigo());
		clone.setStatoRigo(getStatoRigo());
		clone.setTipoRigo(getTipoRigo());
		clone.setCodiceDepositoMov(getCodiceDepositoMov());
		clone.setCodiceArticolo(getCodiceArticolo());
		clone.setCodiceVariante(getCodiceVariante());
		clone.setDescrizione(getDescrizione());
		clone.setCodiceUnitMis(getCodiceUnitMis());
		clone.setDecimaliQuant(getDecimaliQuant());
		clone.setQuantita1(getQuantita1());
		clone.setQuantita2(getQuantita2());
		clone.setQuantita3(getQuantita3());
		clone.setQuantita(getQuantita());
		clone.setCodiceUnitMis2(getCodiceUnitMis2());
		clone.setQuantitaUnitMis2(getQuantitaUnitMis2());
		clone.setDecimaliPrezzo(getDecimaliPrezzo());
		clone.setPrezzo(getPrezzo());
		clone.setImportoLordo(getImportoLordo());
		clone.setSconto1(getSconto1());
		clone.setSconto2(getSconto2());
		clone.setSconto3(getSconto3());
		clone.setImportoNetto(getImportoNetto());
		clone.setImporto(getImporto());
		clone.setCodiceIVAFatturazione(getCodiceIVAFatturazione());
		clone.setCodiceCliente(getCodiceCliente());
		clone.setRiferimentoOrdineCliente(getRiferimentoOrdineCliente());
		clone.setDataOridine(getDataOridine());
		clone.setStatoEvasione(getStatoEvasione());
		clone.setDataPrevistaConsegna(getDataPrevistaConsegna());
		clone.setDataRegistrazioneOrdine(getDataRegistrazioneOrdine());
		clone.setLibStr1(getLibStr1());
		clone.setLibStr2(getLibStr2());
		clone.setLibStr3(getLibStr3());
		clone.setLibDbl1(getLibDbl1());
		clone.setLibDbl2(getLibDbl2());
		clone.setLibDbl3(getLibDbl3());
		clone.setLibDat1(getLibDat1());
		clone.setLibDat2(getLibDat2());
		clone.setLibDat3(getLibDat3());
		clone.setLibLng1(getLibLng1());
		clone.setLibLng2(getLibLng2());
		clone.setLibLng3(getLibLng3());

		return clone;
	}

	@Override
	public int compareTo(WKRigheOrdiniClienti wkRigheOrdiniClienti) {
		int value = 0;

		if (getNumeroOrdine() < wkRigheOrdiniClienti.getNumeroOrdine()) {
			value = -1;
		}
		else if (getNumeroOrdine() > wkRigheOrdiniClienti.getNumeroOrdine()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof WKRigheOrdiniClientiClp)) {
			return false;
		}

		WKRigheOrdiniClientiClp wkRigheOrdiniClienti = (WKRigheOrdiniClientiClp)obj;

		WKRigheOrdiniClientiPK primaryKey = wkRigheOrdiniClienti.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(91);

		sb.append("{anno=");
		sb.append(getAnno());
		sb.append(", tipoOrdine=");
		sb.append(getTipoOrdine());
		sb.append(", numeroOrdine=");
		sb.append(getNumeroOrdine());
		sb.append(", numeroRigo=");
		sb.append(getNumeroRigo());
		sb.append(", statoRigo=");
		sb.append(getStatoRigo());
		sb.append(", tipoRigo=");
		sb.append(getTipoRigo());
		sb.append(", codiceDepositoMov=");
		sb.append(getCodiceDepositoMov());
		sb.append(", codiceArticolo=");
		sb.append(getCodiceArticolo());
		sb.append(", codiceVariante=");
		sb.append(getCodiceVariante());
		sb.append(", descrizione=");
		sb.append(getDescrizione());
		sb.append(", codiceUnitMis=");
		sb.append(getCodiceUnitMis());
		sb.append(", decimaliQuant=");
		sb.append(getDecimaliQuant());
		sb.append(", quantita1=");
		sb.append(getQuantita1());
		sb.append(", quantita2=");
		sb.append(getQuantita2());
		sb.append(", quantita3=");
		sb.append(getQuantita3());
		sb.append(", quantita=");
		sb.append(getQuantita());
		sb.append(", codiceUnitMis2=");
		sb.append(getCodiceUnitMis2());
		sb.append(", quantitaUnitMis2=");
		sb.append(getQuantitaUnitMis2());
		sb.append(", decimaliPrezzo=");
		sb.append(getDecimaliPrezzo());
		sb.append(", prezzo=");
		sb.append(getPrezzo());
		sb.append(", importoLordo=");
		sb.append(getImportoLordo());
		sb.append(", sconto1=");
		sb.append(getSconto1());
		sb.append(", sconto2=");
		sb.append(getSconto2());
		sb.append(", sconto3=");
		sb.append(getSconto3());
		sb.append(", importoNetto=");
		sb.append(getImportoNetto());
		sb.append(", importo=");
		sb.append(getImporto());
		sb.append(", codiceIVAFatturazione=");
		sb.append(getCodiceIVAFatturazione());
		sb.append(", codiceCliente=");
		sb.append(getCodiceCliente());
		sb.append(", riferimentoOrdineCliente=");
		sb.append(getRiferimentoOrdineCliente());
		sb.append(", dataOridine=");
		sb.append(getDataOridine());
		sb.append(", statoEvasione=");
		sb.append(getStatoEvasione());
		sb.append(", dataPrevistaConsegna=");
		sb.append(getDataPrevistaConsegna());
		sb.append(", dataRegistrazioneOrdine=");
		sb.append(getDataRegistrazioneOrdine());
		sb.append(", libStr1=");
		sb.append(getLibStr1());
		sb.append(", libStr2=");
		sb.append(getLibStr2());
		sb.append(", libStr3=");
		sb.append(getLibStr3());
		sb.append(", libDbl1=");
		sb.append(getLibDbl1());
		sb.append(", libDbl2=");
		sb.append(getLibDbl2());
		sb.append(", libDbl3=");
		sb.append(getLibDbl3());
		sb.append(", libDat1=");
		sb.append(getLibDat1());
		sb.append(", libDat2=");
		sb.append(getLibDat2());
		sb.append(", libDat3=");
		sb.append(getLibDat3());
		sb.append(", libLng1=");
		sb.append(getLibLng1());
		sb.append(", libLng2=");
		sb.append(getLibLng2());
		sb.append(", libLng3=");
		sb.append(getLibLng3());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(139);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.WKRigheOrdiniClienti");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>anno</column-name><column-value><![CDATA[");
		sb.append(getAnno());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoOrdine</column-name><column-value><![CDATA[");
		sb.append(getTipoOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroOrdine</column-name><column-value><![CDATA[");
		sb.append(getNumeroOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroRigo</column-name><column-value><![CDATA[");
		sb.append(getNumeroRigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statoRigo</column-name><column-value><![CDATA[");
		sb.append(getStatoRigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoRigo</column-name><column-value><![CDATA[");
		sb.append(getTipoRigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDepositoMov</column-name><column-value><![CDATA[");
		sb.append(getCodiceDepositoMov());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceArticolo</column-name><column-value><![CDATA[");
		sb.append(getCodiceArticolo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceVariante</column-name><column-value><![CDATA[");
		sb.append(getCodiceVariante());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizione</column-name><column-value><![CDATA[");
		sb.append(getDescrizione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceUnitMis</column-name><column-value><![CDATA[");
		sb.append(getCodiceUnitMis());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>decimaliQuant</column-name><column-value><![CDATA[");
		sb.append(getDecimaliQuant());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantita1</column-name><column-value><![CDATA[");
		sb.append(getQuantita1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantita2</column-name><column-value><![CDATA[");
		sb.append(getQuantita2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantita3</column-name><column-value><![CDATA[");
		sb.append(getQuantita3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantita</column-name><column-value><![CDATA[");
		sb.append(getQuantita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceUnitMis2</column-name><column-value><![CDATA[");
		sb.append(getCodiceUnitMis2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaUnitMis2</column-name><column-value><![CDATA[");
		sb.append(getQuantitaUnitMis2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>decimaliPrezzo</column-name><column-value><![CDATA[");
		sb.append(getDecimaliPrezzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>prezzo</column-name><column-value><![CDATA[");
		sb.append(getPrezzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoLordo</column-name><column-value><![CDATA[");
		sb.append(getImportoLordo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sconto1</column-name><column-value><![CDATA[");
		sb.append(getSconto1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sconto2</column-name><column-value><![CDATA[");
		sb.append(getSconto2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sconto3</column-name><column-value><![CDATA[");
		sb.append(getSconto3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoNetto</column-name><column-value><![CDATA[");
		sb.append(getImportoNetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importo</column-name><column-value><![CDATA[");
		sb.append(getImporto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVAFatturazione</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVAFatturazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCliente</column-name><column-value><![CDATA[");
		sb.append(getCodiceCliente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>riferimentoOrdineCliente</column-name><column-value><![CDATA[");
		sb.append(getRiferimentoOrdineCliente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataOridine</column-name><column-value><![CDATA[");
		sb.append(getDataOridine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statoEvasione</column-name><column-value><![CDATA[");
		sb.append(getStatoEvasione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataPrevistaConsegna</column-name><column-value><![CDATA[");
		sb.append(getDataPrevistaConsegna());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataRegistrazioneOrdine</column-name><column-value><![CDATA[");
		sb.append(getDataRegistrazioneOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr1</column-name><column-value><![CDATA[");
		sb.append(getLibStr1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr2</column-name><column-value><![CDATA[");
		sb.append(getLibStr2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr3</column-name><column-value><![CDATA[");
		sb.append(getLibStr3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl1</column-name><column-value><![CDATA[");
		sb.append(getLibDbl1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl2</column-name><column-value><![CDATA[");
		sb.append(getLibDbl2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl3</column-name><column-value><![CDATA[");
		sb.append(getLibDbl3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat1</column-name><column-value><![CDATA[");
		sb.append(getLibDat1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat2</column-name><column-value><![CDATA[");
		sb.append(getLibDat2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat3</column-name><column-value><![CDATA[");
		sb.append(getLibDat3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng1</column-name><column-value><![CDATA[");
		sb.append(getLibLng1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng2</column-name><column-value><![CDATA[");
		sb.append(getLibLng2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng3</column-name><column-value><![CDATA[");
		sb.append(getLibLng3());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _anno;
	private int _tipoOrdine;
	private int _numeroOrdine;
	private int _numeroRigo;
	private boolean _statoRigo;
	private int _tipoRigo;
	private String _codiceDepositoMov;
	private String _codiceArticolo;
	private String _codiceVariante;
	private String _descrizione;
	private String _codiceUnitMis;
	private int _decimaliQuant;
	private double _quantita1;
	private double _quantita2;
	private double _quantita3;
	private double _quantita;
	private String _codiceUnitMis2;
	private double _quantitaUnitMis2;
	private int _decimaliPrezzo;
	private double _prezzo;
	private double _importoLordo;
	private double _sconto1;
	private double _sconto2;
	private double _sconto3;
	private double _importoNetto;
	private double _importo;
	private String _codiceIVAFatturazione;
	private String _codiceCliente;
	private String _riferimentoOrdineCliente;
	private Date _dataOridine;
	private boolean _statoEvasione;
	private Date _dataPrevistaConsegna;
	private Date _dataRegistrazioneOrdine;
	private String _libStr1;
	private String _libStr2;
	private String _libStr3;
	private double _libDbl1;
	private double _libDbl2;
	private double _libDbl3;
	private Date _libDat1;
	private Date _libDat2;
	private Date _libDat3;
	private long _libLng1;
	private long _libLng2;
	private long _libLng3;
	private BaseModel<?> _wkRigheOrdiniClientiRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}