/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.AnagraficaClientiFornitoriServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.AnagraficaClientiFornitoriServiceSoap
 * @generated
 */
public class AnagraficaClientiFornitoriSoap implements Serializable {
	public static AnagraficaClientiFornitoriSoap toSoapModel(
		AnagraficaClientiFornitori model) {
		AnagraficaClientiFornitoriSoap soapModel = new AnagraficaClientiFornitoriSoap();

		soapModel.setCodiceSoggetto(model.getCodiceSoggetto());
		soapModel.setRagioneSociale(model.getRagioneSociale());
		soapModel.setRagioneSocialeAggiuntiva(model.getRagioneSocialeAggiuntiva());
		soapModel.setIndirizzo(model.getIndirizzo());
		soapModel.setComune(model.getComune());
		soapModel.setCAP(model.getCAP());
		soapModel.setSiglaProvincia(model.getSiglaProvincia());
		soapModel.setSiglaStato(model.getSiglaStato());
		soapModel.setPartitaIVA(model.getPartitaIVA());
		soapModel.setCodiceFiscale(model.getCodiceFiscale());
		soapModel.setNote(model.getNote());
		soapModel.setTipoSoggetto(model.getTipoSoggetto());
		soapModel.setCodiceMnemonico(model.getCodiceMnemonico());
		soapModel.setTipoSollecito(model.getTipoSollecito());
		soapModel.setAttivoEC(model.getAttivoEC());

		return soapModel;
	}

	public static AnagraficaClientiFornitoriSoap[] toSoapModels(
		AnagraficaClientiFornitori[] models) {
		AnagraficaClientiFornitoriSoap[] soapModels = new AnagraficaClientiFornitoriSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AnagraficaClientiFornitoriSoap[][] toSoapModels(
		AnagraficaClientiFornitori[][] models) {
		AnagraficaClientiFornitoriSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AnagraficaClientiFornitoriSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AnagraficaClientiFornitoriSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AnagraficaClientiFornitoriSoap[] toSoapModels(
		List<AnagraficaClientiFornitori> models) {
		List<AnagraficaClientiFornitoriSoap> soapModels = new ArrayList<AnagraficaClientiFornitoriSoap>(models.size());

		for (AnagraficaClientiFornitori model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AnagraficaClientiFornitoriSoap[soapModels.size()]);
	}

	public AnagraficaClientiFornitoriSoap() {
	}

	public String getPrimaryKey() {
		return _codiceSoggetto;
	}

	public void setPrimaryKey(String pk) {
		setCodiceSoggetto(pk);
	}

	public String getCodiceSoggetto() {
		return _codiceSoggetto;
	}

	public void setCodiceSoggetto(String codiceSoggetto) {
		_codiceSoggetto = codiceSoggetto;
	}

	public String getRagioneSociale() {
		return _ragioneSociale;
	}

	public void setRagioneSociale(String ragioneSociale) {
		_ragioneSociale = ragioneSociale;
	}

	public String getRagioneSocialeAggiuntiva() {
		return _ragioneSocialeAggiuntiva;
	}

	public void setRagioneSocialeAggiuntiva(String ragioneSocialeAggiuntiva) {
		_ragioneSocialeAggiuntiva = ragioneSocialeAggiuntiva;
	}

	public String getIndirizzo() {
		return _indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		_indirizzo = indirizzo;
	}

	public String getComune() {
		return _comune;
	}

	public void setComune(String comune) {
		_comune = comune;
	}

	public String getCAP() {
		return _CAP;
	}

	public void setCAP(String CAP) {
		_CAP = CAP;
	}

	public String getSiglaProvincia() {
		return _siglaProvincia;
	}

	public void setSiglaProvincia(String siglaProvincia) {
		_siglaProvincia = siglaProvincia;
	}

	public String getSiglaStato() {
		return _siglaStato;
	}

	public void setSiglaStato(String siglaStato) {
		_siglaStato = siglaStato;
	}

	public String getPartitaIVA() {
		return _partitaIVA;
	}

	public void setPartitaIVA(String partitaIVA) {
		_partitaIVA = partitaIVA;
	}

	public String getCodiceFiscale() {
		return _codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		_codiceFiscale = codiceFiscale;
	}

	public String getNote() {
		return _note;
	}

	public void setNote(String note) {
		_note = note;
	}

	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;
	}

	public String getCodiceMnemonico() {
		return _codiceMnemonico;
	}

	public void setCodiceMnemonico(String codiceMnemonico) {
		_codiceMnemonico = codiceMnemonico;
	}

	public int getTipoSollecito() {
		return _tipoSollecito;
	}

	public void setTipoSollecito(int tipoSollecito) {
		_tipoSollecito = tipoSollecito;
	}

	public boolean getAttivoEC() {
		return _attivoEC;
	}

	public boolean isAttivoEC() {
		return _attivoEC;
	}

	public void setAttivoEC(boolean attivoEC) {
		_attivoEC = attivoEC;
	}

	private String _codiceSoggetto;
	private String _ragioneSociale;
	private String _ragioneSocialeAggiuntiva;
	private String _indirizzo;
	private String _comune;
	private String _CAP;
	private String _siglaProvincia;
	private String _siglaStato;
	private String _partitaIVA;
	private String _codiceFiscale;
	private String _note;
	private boolean _tipoSoggetto;
	private String _codiceMnemonico;
	private int _tipoSollecito;
	private boolean _attivoEC;
}