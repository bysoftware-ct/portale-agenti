/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.DatiClientiFornitoriServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.DatiClientiFornitoriServiceSoap
 * @generated
 */
public class DatiClientiFornitoriSoap implements Serializable {
	public static DatiClientiFornitoriSoap toSoapModel(
		DatiClientiFornitori model) {
		DatiClientiFornitoriSoap soapModel = new DatiClientiFornitoriSoap();

		soapModel.setTipoSoggetto(model.getTipoSoggetto());
		soapModel.setCodiceSoggetto(model.getCodiceSoggetto());
		soapModel.setCodiceBanca(model.getCodiceBanca());
		soapModel.setCodiceAgenzia(model.getCodiceAgenzia());
		soapModel.setCodiceIdBanca(model.getCodiceIdBanca());
		soapModel.setCodicePaese(model.getCodicePaese());
		soapModel.setCodiceCINInt(model.getCodiceCINInt());
		soapModel.setCodiceCINNaz(model.getCodiceCINNaz());
		soapModel.setNumeroContoCorrente(model.getNumeroContoCorrente());
		soapModel.setIBAN(model.getIBAN());
		soapModel.setCategoriaEconomica(model.getCategoriaEconomica());
		soapModel.setCodiceEsenzioneIVA(model.getCodiceEsenzioneIVA());
		soapModel.setCodiceDivisa(model.getCodiceDivisa());
		soapModel.setCodicePagamento(model.getCodicePagamento());
		soapModel.setCodiceZona(model.getCodiceZona());
		soapModel.setCodiceAgente(model.getCodiceAgente());
		soapModel.setCodiceGruppoAgente(model.getCodiceGruppoAgente());
		soapModel.setCodiceCatProvv(model.getCodiceCatProvv());
		soapModel.setCodiceSpedizione(model.getCodiceSpedizione());
		soapModel.setCodicePorto(model.getCodicePorto());
		soapModel.setCodiceVettore(model.getCodiceVettore());
		soapModel.setCodiceDestDiversa(model.getCodiceDestDiversa());
		soapModel.setCodiceListinoPrezzi(model.getCodiceListinoPrezzi());
		soapModel.setCodiceScontoTotale(model.getCodiceScontoTotale());
		soapModel.setCodiceScontoPreIVA(model.getCodiceScontoPreIVA());
		soapModel.setScontoCat1(model.getScontoCat1());
		soapModel.setScontoCat2(model.getScontoCat2());
		soapModel.setCodiceLingua(model.getCodiceLingua());
		soapModel.setCodiceLinguaEC(model.getCodiceLinguaEC());
		soapModel.setAddebitoBolli(model.getAddebitoBolli());
		soapModel.setAddebitoSpeseBanca(model.getAddebitoSpeseBanca());
		soapModel.setRagruppaBolle(model.getRagruppaBolle());
		soapModel.setSospensioneIVA(model.getSospensioneIVA());
		soapModel.setRagruppaOrdini(model.getRagruppaOrdini());
		soapModel.setRagruppaXDestinazione(model.getRagruppaXDestinazione());
		soapModel.setRagruppaXPorto(model.getRagruppaXPorto());
		soapModel.setPercSpeseTrasporto(model.getPercSpeseTrasporto());
		soapModel.setPrezzoDaProporre(model.getPrezzoDaProporre());
		soapModel.setSogettoFatturazione(model.getSogettoFatturazione());
		soapModel.setCodiceRaggEffetti(model.getCodiceRaggEffetti());
		soapModel.setRiportoRiferimenti(model.getRiportoRiferimenti());
		soapModel.setStampaPrezzo(model.getStampaPrezzo());
		soapModel.setBloccato(model.getBloccato());
		soapModel.setMotivazione(model.getMotivazione());
		soapModel.setLivelloBlocco(model.getLivelloBlocco());
		soapModel.setAddebitoCONAI(model.getAddebitoCONAI());
		soapModel.setLibStr1(model.getLibStr1());
		soapModel.setLibStr2(model.getLibStr2());
		soapModel.setLibStr3(model.getLibStr3());
		soapModel.setLibStr4(model.getLibStr4());
		soapModel.setLibStr5(model.getLibStr5());
		soapModel.setLibDat1(model.getLibDat1());
		soapModel.setLibDat2(model.getLibDat2());
		soapModel.setLibDat3(model.getLibDat3());
		soapModel.setLibDat4(model.getLibDat4());
		soapModel.setLibDat5(model.getLibDat5());
		soapModel.setLibLng1(model.getLibLng1());
		soapModel.setLibLng2(model.getLibLng2());
		soapModel.setLibLng3(model.getLibLng3());
		soapModel.setLibLng4(model.getLibLng4());
		soapModel.setLibLng5(model.getLibLng5());
		soapModel.setLibDbl1(model.getLibDbl1());
		soapModel.setLibDbl2(model.getLibDbl2());
		soapModel.setLibDbl3(model.getLibDbl3());
		soapModel.setLibDbl4(model.getLibDbl4());
		soapModel.setLibDbl5(model.getLibDbl5());

		return soapModel;
	}

	public static DatiClientiFornitoriSoap[] toSoapModels(
		DatiClientiFornitori[] models) {
		DatiClientiFornitoriSoap[] soapModels = new DatiClientiFornitoriSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DatiClientiFornitoriSoap[][] toSoapModels(
		DatiClientiFornitori[][] models) {
		DatiClientiFornitoriSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DatiClientiFornitoriSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DatiClientiFornitoriSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DatiClientiFornitoriSoap[] toSoapModels(
		List<DatiClientiFornitori> models) {
		List<DatiClientiFornitoriSoap> soapModels = new ArrayList<DatiClientiFornitoriSoap>(models.size());

		for (DatiClientiFornitori model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DatiClientiFornitoriSoap[soapModels.size()]);
	}

	public DatiClientiFornitoriSoap() {
	}

	public DatiClientiFornitoriPK getPrimaryKey() {
		return new DatiClientiFornitoriPK(_tipoSoggetto, _codiceSoggetto);
	}

	public void setPrimaryKey(DatiClientiFornitoriPK pk) {
		setTipoSoggetto(pk.tipoSoggetto);
		setCodiceSoggetto(pk.codiceSoggetto);
	}

	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;
	}

	public String getCodiceSoggetto() {
		return _codiceSoggetto;
	}

	public void setCodiceSoggetto(String codiceSoggetto) {
		_codiceSoggetto = codiceSoggetto;
	}

	public String getCodiceBanca() {
		return _codiceBanca;
	}

	public void setCodiceBanca(String codiceBanca) {
		_codiceBanca = codiceBanca;
	}

	public String getCodiceAgenzia() {
		return _codiceAgenzia;
	}

	public void setCodiceAgenzia(String codiceAgenzia) {
		_codiceAgenzia = codiceAgenzia;
	}

	public String getCodiceIdBanca() {
		return _codiceIdBanca;
	}

	public void setCodiceIdBanca(String codiceIdBanca) {
		_codiceIdBanca = codiceIdBanca;
	}

	public String getCodicePaese() {
		return _codicePaese;
	}

	public void setCodicePaese(String codicePaese) {
		_codicePaese = codicePaese;
	}

	public String getCodiceCINInt() {
		return _codiceCINInt;
	}

	public void setCodiceCINInt(String codiceCINInt) {
		_codiceCINInt = codiceCINInt;
	}

	public String getCodiceCINNaz() {
		return _codiceCINNaz;
	}

	public void setCodiceCINNaz(String codiceCINNaz) {
		_codiceCINNaz = codiceCINNaz;
	}

	public String getNumeroContoCorrente() {
		return _numeroContoCorrente;
	}

	public void setNumeroContoCorrente(String numeroContoCorrente) {
		_numeroContoCorrente = numeroContoCorrente;
	}

	public String getIBAN() {
		return _IBAN;
	}

	public void setIBAN(String IBAN) {
		_IBAN = IBAN;
	}

	public String getCategoriaEconomica() {
		return _categoriaEconomica;
	}

	public void setCategoriaEconomica(String categoriaEconomica) {
		_categoriaEconomica = categoriaEconomica;
	}

	public String getCodiceEsenzioneIVA() {
		return _codiceEsenzioneIVA;
	}

	public void setCodiceEsenzioneIVA(String codiceEsenzioneIVA) {
		_codiceEsenzioneIVA = codiceEsenzioneIVA;
	}

	public String getCodiceDivisa() {
		return _codiceDivisa;
	}

	public void setCodiceDivisa(String codiceDivisa) {
		_codiceDivisa = codiceDivisa;
	}

	public String getCodicePagamento() {
		return _codicePagamento;
	}

	public void setCodicePagamento(String codicePagamento) {
		_codicePagamento = codicePagamento;
	}

	public String getCodiceZona() {
		return _codiceZona;
	}

	public void setCodiceZona(String codiceZona) {
		_codiceZona = codiceZona;
	}

	public String getCodiceAgente() {
		return _codiceAgente;
	}

	public void setCodiceAgente(String codiceAgente) {
		_codiceAgente = codiceAgente;
	}

	public String getCodiceGruppoAgente() {
		return _codiceGruppoAgente;
	}

	public void setCodiceGruppoAgente(String codiceGruppoAgente) {
		_codiceGruppoAgente = codiceGruppoAgente;
	}

	public String getCodiceCatProvv() {
		return _codiceCatProvv;
	}

	public void setCodiceCatProvv(String codiceCatProvv) {
		_codiceCatProvv = codiceCatProvv;
	}

	public String getCodiceSpedizione() {
		return _codiceSpedizione;
	}

	public void setCodiceSpedizione(String codiceSpedizione) {
		_codiceSpedizione = codiceSpedizione;
	}

	public String getCodicePorto() {
		return _codicePorto;
	}

	public void setCodicePorto(String codicePorto) {
		_codicePorto = codicePorto;
	}

	public String getCodiceVettore() {
		return _codiceVettore;
	}

	public void setCodiceVettore(String codiceVettore) {
		_codiceVettore = codiceVettore;
	}

	public String getCodiceDestDiversa() {
		return _codiceDestDiversa;
	}

	public void setCodiceDestDiversa(String codiceDestDiversa) {
		_codiceDestDiversa = codiceDestDiversa;
	}

	public String getCodiceListinoPrezzi() {
		return _codiceListinoPrezzi;
	}

	public void setCodiceListinoPrezzi(String codiceListinoPrezzi) {
		_codiceListinoPrezzi = codiceListinoPrezzi;
	}

	public String getCodiceScontoTotale() {
		return _codiceScontoTotale;
	}

	public void setCodiceScontoTotale(String codiceScontoTotale) {
		_codiceScontoTotale = codiceScontoTotale;
	}

	public String getCodiceScontoPreIVA() {
		return _codiceScontoPreIVA;
	}

	public void setCodiceScontoPreIVA(String codiceScontoPreIVA) {
		_codiceScontoPreIVA = codiceScontoPreIVA;
	}

	public String getScontoCat1() {
		return _scontoCat1;
	}

	public void setScontoCat1(String scontoCat1) {
		_scontoCat1 = scontoCat1;
	}

	public String getScontoCat2() {
		return _scontoCat2;
	}

	public void setScontoCat2(String scontoCat2) {
		_scontoCat2 = scontoCat2;
	}

	public String getCodiceLingua() {
		return _codiceLingua;
	}

	public void setCodiceLingua(String codiceLingua) {
		_codiceLingua = codiceLingua;
	}

	public String getCodiceLinguaEC() {
		return _codiceLinguaEC;
	}

	public void setCodiceLinguaEC(String codiceLinguaEC) {
		_codiceLinguaEC = codiceLinguaEC;
	}

	public boolean getAddebitoBolli() {
		return _addebitoBolli;
	}

	public boolean isAddebitoBolli() {
		return _addebitoBolli;
	}

	public void setAddebitoBolli(boolean addebitoBolli) {
		_addebitoBolli = addebitoBolli;
	}

	public boolean getAddebitoSpeseBanca() {
		return _addebitoSpeseBanca;
	}

	public boolean isAddebitoSpeseBanca() {
		return _addebitoSpeseBanca;
	}

	public void setAddebitoSpeseBanca(boolean addebitoSpeseBanca) {
		_addebitoSpeseBanca = addebitoSpeseBanca;
	}

	public boolean getRagruppaBolle() {
		return _ragruppaBolle;
	}

	public boolean isRagruppaBolle() {
		return _ragruppaBolle;
	}

	public void setRagruppaBolle(boolean ragruppaBolle) {
		_ragruppaBolle = ragruppaBolle;
	}

	public boolean getSospensioneIVA() {
		return _sospensioneIVA;
	}

	public boolean isSospensioneIVA() {
		return _sospensioneIVA;
	}

	public void setSospensioneIVA(boolean sospensioneIVA) {
		_sospensioneIVA = sospensioneIVA;
	}

	public int getRagruppaOrdini() {
		return _ragruppaOrdini;
	}

	public void setRagruppaOrdini(int ragruppaOrdini) {
		_ragruppaOrdini = ragruppaOrdini;
	}

	public boolean getRagruppaXDestinazione() {
		return _ragruppaXDestinazione;
	}

	public boolean isRagruppaXDestinazione() {
		return _ragruppaXDestinazione;
	}

	public void setRagruppaXDestinazione(boolean ragruppaXDestinazione) {
		_ragruppaXDestinazione = ragruppaXDestinazione;
	}

	public boolean getRagruppaXPorto() {
		return _ragruppaXPorto;
	}

	public boolean isRagruppaXPorto() {
		return _ragruppaXPorto;
	}

	public void setRagruppaXPorto(boolean ragruppaXPorto) {
		_ragruppaXPorto = ragruppaXPorto;
	}

	public double getPercSpeseTrasporto() {
		return _percSpeseTrasporto;
	}

	public void setPercSpeseTrasporto(double percSpeseTrasporto) {
		_percSpeseTrasporto = percSpeseTrasporto;
	}

	public int getPrezzoDaProporre() {
		return _prezzoDaProporre;
	}

	public void setPrezzoDaProporre(int prezzoDaProporre) {
		_prezzoDaProporre = prezzoDaProporre;
	}

	public String getSogettoFatturazione() {
		return _sogettoFatturazione;
	}

	public void setSogettoFatturazione(String sogettoFatturazione) {
		_sogettoFatturazione = sogettoFatturazione;
	}

	public String getCodiceRaggEffetti() {
		return _codiceRaggEffetti;
	}

	public void setCodiceRaggEffetti(String codiceRaggEffetti) {
		_codiceRaggEffetti = codiceRaggEffetti;
	}

	public int getRiportoRiferimenti() {
		return _riportoRiferimenti;
	}

	public void setRiportoRiferimenti(int riportoRiferimenti) {
		_riportoRiferimenti = riportoRiferimenti;
	}

	public boolean getStampaPrezzo() {
		return _stampaPrezzo;
	}

	public boolean isStampaPrezzo() {
		return _stampaPrezzo;
	}

	public void setStampaPrezzo(boolean stampaPrezzo) {
		_stampaPrezzo = stampaPrezzo;
	}

	public int getBloccato() {
		return _bloccato;
	}

	public void setBloccato(int bloccato) {
		_bloccato = bloccato;
	}

	public String getMotivazione() {
		return _motivazione;
	}

	public void setMotivazione(String motivazione) {
		_motivazione = motivazione;
	}

	public int getLivelloBlocco() {
		return _livelloBlocco;
	}

	public void setLivelloBlocco(int livelloBlocco) {
		_livelloBlocco = livelloBlocco;
	}

	public boolean getAddebitoCONAI() {
		return _addebitoCONAI;
	}

	public boolean isAddebitoCONAI() {
		return _addebitoCONAI;
	}

	public void setAddebitoCONAI(boolean addebitoCONAI) {
		_addebitoCONAI = addebitoCONAI;
	}

	public String getLibStr1() {
		return _libStr1;
	}

	public void setLibStr1(String libStr1) {
		_libStr1 = libStr1;
	}

	public String getLibStr2() {
		return _libStr2;
	}

	public void setLibStr2(String libStr2) {
		_libStr2 = libStr2;
	}

	public String getLibStr3() {
		return _libStr3;
	}

	public void setLibStr3(String libStr3) {
		_libStr3 = libStr3;
	}

	public String getLibStr4() {
		return _libStr4;
	}

	public void setLibStr4(String libStr4) {
		_libStr4 = libStr4;
	}

	public String getLibStr5() {
		return _libStr5;
	}

	public void setLibStr5(String libStr5) {
		_libStr5 = libStr5;
	}

	public Date getLibDat1() {
		return _libDat1;
	}

	public void setLibDat1(Date libDat1) {
		_libDat1 = libDat1;
	}

	public Date getLibDat2() {
		return _libDat2;
	}

	public void setLibDat2(Date libDat2) {
		_libDat2 = libDat2;
	}

	public Date getLibDat3() {
		return _libDat3;
	}

	public void setLibDat3(Date libDat3) {
		_libDat3 = libDat3;
	}

	public Date getLibDat4() {
		return _libDat4;
	}

	public void setLibDat4(Date libDat4) {
		_libDat4 = libDat4;
	}

	public Date getLibDat5() {
		return _libDat5;
	}

	public void setLibDat5(Date libDat5) {
		_libDat5 = libDat5;
	}

	public long getLibLng1() {
		return _libLng1;
	}

	public void setLibLng1(long libLng1) {
		_libLng1 = libLng1;
	}

	public long getLibLng2() {
		return _libLng2;
	}

	public void setLibLng2(long libLng2) {
		_libLng2 = libLng2;
	}

	public long getLibLng3() {
		return _libLng3;
	}

	public void setLibLng3(long libLng3) {
		_libLng3 = libLng3;
	}

	public long getLibLng4() {
		return _libLng4;
	}

	public void setLibLng4(long libLng4) {
		_libLng4 = libLng4;
	}

	public long getLibLng5() {
		return _libLng5;
	}

	public void setLibLng5(long libLng5) {
		_libLng5 = libLng5;
	}

	public double getLibDbl1() {
		return _libDbl1;
	}

	public void setLibDbl1(double libDbl1) {
		_libDbl1 = libDbl1;
	}

	public double getLibDbl2() {
		return _libDbl2;
	}

	public void setLibDbl2(double libDbl2) {
		_libDbl2 = libDbl2;
	}

	public double getLibDbl3() {
		return _libDbl3;
	}

	public void setLibDbl3(double libDbl3) {
		_libDbl3 = libDbl3;
	}

	public double getLibDbl4() {
		return _libDbl4;
	}

	public void setLibDbl4(double libDbl4) {
		_libDbl4 = libDbl4;
	}

	public double getLibDbl5() {
		return _libDbl5;
	}

	public void setLibDbl5(double libDbl5) {
		_libDbl5 = libDbl5;
	}

	private boolean _tipoSoggetto;
	private String _codiceSoggetto;
	private String _codiceBanca;
	private String _codiceAgenzia;
	private String _codiceIdBanca;
	private String _codicePaese;
	private String _codiceCINInt;
	private String _codiceCINNaz;
	private String _numeroContoCorrente;
	private String _IBAN;
	private String _categoriaEconomica;
	private String _codiceEsenzioneIVA;
	private String _codiceDivisa;
	private String _codicePagamento;
	private String _codiceZona;
	private String _codiceAgente;
	private String _codiceGruppoAgente;
	private String _codiceCatProvv;
	private String _codiceSpedizione;
	private String _codicePorto;
	private String _codiceVettore;
	private String _codiceDestDiversa;
	private String _codiceListinoPrezzi;
	private String _codiceScontoTotale;
	private String _codiceScontoPreIVA;
	private String _scontoCat1;
	private String _scontoCat2;
	private String _codiceLingua;
	private String _codiceLinguaEC;
	private boolean _addebitoBolli;
	private boolean _addebitoSpeseBanca;
	private boolean _ragruppaBolle;
	private boolean _sospensioneIVA;
	private int _ragruppaOrdini;
	private boolean _ragruppaXDestinazione;
	private boolean _ragruppaXPorto;
	private double _percSpeseTrasporto;
	private int _prezzoDaProporre;
	private String _sogettoFatturazione;
	private String _codiceRaggEffetti;
	private int _riportoRiferimenti;
	private boolean _stampaPrezzo;
	private int _bloccato;
	private String _motivazione;
	private int _livelloBlocco;
	private boolean _addebitoCONAI;
	private String _libStr1;
	private String _libStr2;
	private String _libStr3;
	private String _libStr4;
	private String _libStr5;
	private Date _libDat1;
	private Date _libDat2;
	private Date _libDat3;
	private Date _libDat4;
	private Date _libDat5;
	private long _libLng1;
	private long _libLng2;
	private long _libLng3;
	private long _libLng4;
	private long _libLng5;
	private double _libDbl1;
	private double _libDbl2;
	private double _libDbl3;
	private double _libDbl4;
	private double _libDbl5;
}