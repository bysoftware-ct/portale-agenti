/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link DatiCostantiClientiFornitori}.
 * </p>
 *
 * @author Mario Torrisi
 * @see DatiCostantiClientiFornitori
 * @generated
 */
public class DatiCostantiClientiFornitoriWrapper
	implements DatiCostantiClientiFornitori,
		ModelWrapper<DatiCostantiClientiFornitori> {
	public DatiCostantiClientiFornitoriWrapper(
		DatiCostantiClientiFornitori datiCostantiClientiFornitori) {
		_datiCostantiClientiFornitori = datiCostantiClientiFornitori;
	}

	@Override
	public Class<?> getModelClass() {
		return DatiCostantiClientiFornitori.class;
	}

	@Override
	public String getModelClassName() {
		return DatiCostantiClientiFornitori.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceSoggetto", getCodiceSoggetto());
		attributes.put("codiceSottoconto", getCodiceSottoconto());
		attributes.put("massimoScoperto", getMassimoScoperto());
		attributes.put("gestioneEstrattoConto", getGestioneEstrattoConto());
		attributes.put("codicePagamento", getCodicePagamento());
		attributes.put("codiceBancaPagFor", getCodiceBancaPagFor());
		attributes.put("codiceAgenziaPagFor", getCodiceAgenziaPagFor());
		attributes.put("codiceContropartita", getCodiceContropartita());
		attributes.put("calcoloIntMora", getCalcoloIntMora());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceSoggetto = (String)attributes.get("codiceSoggetto");

		if (codiceSoggetto != null) {
			setCodiceSoggetto(codiceSoggetto);
		}

		String codiceSottoconto = (String)attributes.get("codiceSottoconto");

		if (codiceSottoconto != null) {
			setCodiceSottoconto(codiceSottoconto);
		}

		Double massimoScoperto = (Double)attributes.get("massimoScoperto");

		if (massimoScoperto != null) {
			setMassimoScoperto(massimoScoperto);
		}

		Boolean gestioneEstrattoConto = (Boolean)attributes.get(
				"gestioneEstrattoConto");

		if (gestioneEstrattoConto != null) {
			setGestioneEstrattoConto(gestioneEstrattoConto);
		}

		String codicePagamento = (String)attributes.get("codicePagamento");

		if (codicePagamento != null) {
			setCodicePagamento(codicePagamento);
		}

		String codiceBancaPagFor = (String)attributes.get("codiceBancaPagFor");

		if (codiceBancaPagFor != null) {
			setCodiceBancaPagFor(codiceBancaPagFor);
		}

		String codiceAgenziaPagFor = (String)attributes.get(
				"codiceAgenziaPagFor");

		if (codiceAgenziaPagFor != null) {
			setCodiceAgenziaPagFor(codiceAgenziaPagFor);
		}

		String codiceContropartita = (String)attributes.get(
				"codiceContropartita");

		if (codiceContropartita != null) {
			setCodiceContropartita(codiceContropartita);
		}

		Boolean calcoloIntMora = (Boolean)attributes.get("calcoloIntMora");

		if (calcoloIntMora != null) {
			setCalcoloIntMora(calcoloIntMora);
		}
	}

	/**
	* Returns the primary key of this dati costanti clienti fornitori.
	*
	* @return the primary key of this dati costanti clienti fornitori
	*/
	@Override
	public it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK getPrimaryKey() {
		return _datiCostantiClientiFornitori.getPrimaryKey();
	}

	/**
	* Sets the primary key of this dati costanti clienti fornitori.
	*
	* @param primaryKey the primary key of this dati costanti clienti fornitori
	*/
	@Override
	public void setPrimaryKey(
		it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK primaryKey) {
		_datiCostantiClientiFornitori.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the tipo soggetto of this dati costanti clienti fornitori.
	*
	* @return the tipo soggetto of this dati costanti clienti fornitori
	*/
	@Override
	public boolean getTipoSoggetto() {
		return _datiCostantiClientiFornitori.getTipoSoggetto();
	}

	/**
	* Returns <code>true</code> if this dati costanti clienti fornitori is tipo soggetto.
	*
	* @return <code>true</code> if this dati costanti clienti fornitori is tipo soggetto; <code>false</code> otherwise
	*/
	@Override
	public boolean isTipoSoggetto() {
		return _datiCostantiClientiFornitori.isTipoSoggetto();
	}

	/**
	* Sets whether this dati costanti clienti fornitori is tipo soggetto.
	*
	* @param tipoSoggetto the tipo soggetto of this dati costanti clienti fornitori
	*/
	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_datiCostantiClientiFornitori.setTipoSoggetto(tipoSoggetto);
	}

	/**
	* Returns the codice soggetto of this dati costanti clienti fornitori.
	*
	* @return the codice soggetto of this dati costanti clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceSoggetto() {
		return _datiCostantiClientiFornitori.getCodiceSoggetto();
	}

	/**
	* Sets the codice soggetto of this dati costanti clienti fornitori.
	*
	* @param codiceSoggetto the codice soggetto of this dati costanti clienti fornitori
	*/
	@Override
	public void setCodiceSoggetto(java.lang.String codiceSoggetto) {
		_datiCostantiClientiFornitori.setCodiceSoggetto(codiceSoggetto);
	}

	/**
	* Returns the codice sottoconto of this dati costanti clienti fornitori.
	*
	* @return the codice sottoconto of this dati costanti clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceSottoconto() {
		return _datiCostantiClientiFornitori.getCodiceSottoconto();
	}

	/**
	* Sets the codice sottoconto of this dati costanti clienti fornitori.
	*
	* @param codiceSottoconto the codice sottoconto of this dati costanti clienti fornitori
	*/
	@Override
	public void setCodiceSottoconto(java.lang.String codiceSottoconto) {
		_datiCostantiClientiFornitori.setCodiceSottoconto(codiceSottoconto);
	}

	/**
	* Returns the massimo scoperto of this dati costanti clienti fornitori.
	*
	* @return the massimo scoperto of this dati costanti clienti fornitori
	*/
	@Override
	public double getMassimoScoperto() {
		return _datiCostantiClientiFornitori.getMassimoScoperto();
	}

	/**
	* Sets the massimo scoperto of this dati costanti clienti fornitori.
	*
	* @param massimoScoperto the massimo scoperto of this dati costanti clienti fornitori
	*/
	@Override
	public void setMassimoScoperto(double massimoScoperto) {
		_datiCostantiClientiFornitori.setMassimoScoperto(massimoScoperto);
	}

	/**
	* Returns the gestione estratto conto of this dati costanti clienti fornitori.
	*
	* @return the gestione estratto conto of this dati costanti clienti fornitori
	*/
	@Override
	public boolean getGestioneEstrattoConto() {
		return _datiCostantiClientiFornitori.getGestioneEstrattoConto();
	}

	/**
	* Returns <code>true</code> if this dati costanti clienti fornitori is gestione estratto conto.
	*
	* @return <code>true</code> if this dati costanti clienti fornitori is gestione estratto conto; <code>false</code> otherwise
	*/
	@Override
	public boolean isGestioneEstrattoConto() {
		return _datiCostantiClientiFornitori.isGestioneEstrattoConto();
	}

	/**
	* Sets whether this dati costanti clienti fornitori is gestione estratto conto.
	*
	* @param gestioneEstrattoConto the gestione estratto conto of this dati costanti clienti fornitori
	*/
	@Override
	public void setGestioneEstrattoConto(boolean gestioneEstrattoConto) {
		_datiCostantiClientiFornitori.setGestioneEstrattoConto(gestioneEstrattoConto);
	}

	/**
	* Returns the codice pagamento of this dati costanti clienti fornitori.
	*
	* @return the codice pagamento of this dati costanti clienti fornitori
	*/
	@Override
	public java.lang.String getCodicePagamento() {
		return _datiCostantiClientiFornitori.getCodicePagamento();
	}

	/**
	* Sets the codice pagamento of this dati costanti clienti fornitori.
	*
	* @param codicePagamento the codice pagamento of this dati costanti clienti fornitori
	*/
	@Override
	public void setCodicePagamento(java.lang.String codicePagamento) {
		_datiCostantiClientiFornitori.setCodicePagamento(codicePagamento);
	}

	/**
	* Returns the codice banca pag for of this dati costanti clienti fornitori.
	*
	* @return the codice banca pag for of this dati costanti clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceBancaPagFor() {
		return _datiCostantiClientiFornitori.getCodiceBancaPagFor();
	}

	/**
	* Sets the codice banca pag for of this dati costanti clienti fornitori.
	*
	* @param codiceBancaPagFor the codice banca pag for of this dati costanti clienti fornitori
	*/
	@Override
	public void setCodiceBancaPagFor(java.lang.String codiceBancaPagFor) {
		_datiCostantiClientiFornitori.setCodiceBancaPagFor(codiceBancaPagFor);
	}

	/**
	* Returns the codice agenzia pag for of this dati costanti clienti fornitori.
	*
	* @return the codice agenzia pag for of this dati costanti clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceAgenziaPagFor() {
		return _datiCostantiClientiFornitori.getCodiceAgenziaPagFor();
	}

	/**
	* Sets the codice agenzia pag for of this dati costanti clienti fornitori.
	*
	* @param codiceAgenziaPagFor the codice agenzia pag for of this dati costanti clienti fornitori
	*/
	@Override
	public void setCodiceAgenziaPagFor(java.lang.String codiceAgenziaPagFor) {
		_datiCostantiClientiFornitori.setCodiceAgenziaPagFor(codiceAgenziaPagFor);
	}

	/**
	* Returns the codice contropartita of this dati costanti clienti fornitori.
	*
	* @return the codice contropartita of this dati costanti clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceContropartita() {
		return _datiCostantiClientiFornitori.getCodiceContropartita();
	}

	/**
	* Sets the codice contropartita of this dati costanti clienti fornitori.
	*
	* @param codiceContropartita the codice contropartita of this dati costanti clienti fornitori
	*/
	@Override
	public void setCodiceContropartita(java.lang.String codiceContropartita) {
		_datiCostantiClientiFornitori.setCodiceContropartita(codiceContropartita);
	}

	/**
	* Returns the calcolo int mora of this dati costanti clienti fornitori.
	*
	* @return the calcolo int mora of this dati costanti clienti fornitori
	*/
	@Override
	public boolean getCalcoloIntMora() {
		return _datiCostantiClientiFornitori.getCalcoloIntMora();
	}

	/**
	* Returns <code>true</code> if this dati costanti clienti fornitori is calcolo int mora.
	*
	* @return <code>true</code> if this dati costanti clienti fornitori is calcolo int mora; <code>false</code> otherwise
	*/
	@Override
	public boolean isCalcoloIntMora() {
		return _datiCostantiClientiFornitori.isCalcoloIntMora();
	}

	/**
	* Sets whether this dati costanti clienti fornitori is calcolo int mora.
	*
	* @param calcoloIntMora the calcolo int mora of this dati costanti clienti fornitori
	*/
	@Override
	public void setCalcoloIntMora(boolean calcoloIntMora) {
		_datiCostantiClientiFornitori.setCalcoloIntMora(calcoloIntMora);
	}

	@Override
	public boolean isNew() {
		return _datiCostantiClientiFornitori.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_datiCostantiClientiFornitori.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _datiCostantiClientiFornitori.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_datiCostantiClientiFornitori.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _datiCostantiClientiFornitori.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _datiCostantiClientiFornitori.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_datiCostantiClientiFornitori.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _datiCostantiClientiFornitori.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_datiCostantiClientiFornitori.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_datiCostantiClientiFornitori.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_datiCostantiClientiFornitori.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new DatiCostantiClientiFornitoriWrapper((DatiCostantiClientiFornitori)_datiCostantiClientiFornitori.clone());
	}

	@Override
	public int compareTo(
		it.bysoftware.ct.model.DatiCostantiClientiFornitori datiCostantiClientiFornitori) {
		return _datiCostantiClientiFornitori.compareTo(datiCostantiClientiFornitori);
	}

	@Override
	public int hashCode() {
		return _datiCostantiClientiFornitori.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.DatiCostantiClientiFornitori> toCacheModel() {
		return _datiCostantiClientiFornitori.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.DatiCostantiClientiFornitori toEscapedModel() {
		return new DatiCostantiClientiFornitoriWrapper(_datiCostantiClientiFornitori.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.DatiCostantiClientiFornitori toUnescapedModel() {
		return new DatiCostantiClientiFornitoriWrapper(_datiCostantiClientiFornitori.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _datiCostantiClientiFornitori.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _datiCostantiClientiFornitori.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_datiCostantiClientiFornitori.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DatiCostantiClientiFornitoriWrapper)) {
			return false;
		}

		DatiCostantiClientiFornitoriWrapper datiCostantiClientiFornitoriWrapper = (DatiCostantiClientiFornitoriWrapper)obj;

		if (Validator.equals(_datiCostantiClientiFornitori,
					datiCostantiClientiFornitoriWrapper._datiCostantiClientiFornitori)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public DatiCostantiClientiFornitori getWrappedDatiCostantiClientiFornitori() {
		return _datiCostantiClientiFornitori;
	}

	@Override
	public DatiCostantiClientiFornitori getWrappedModel() {
		return _datiCostantiClientiFornitori;
	}

	@Override
	public void resetOriginalValues() {
		_datiCostantiClientiFornitori.resetOriginalValues();
	}

	private DatiCostantiClientiFornitori _datiCostantiClientiFornitori;
}