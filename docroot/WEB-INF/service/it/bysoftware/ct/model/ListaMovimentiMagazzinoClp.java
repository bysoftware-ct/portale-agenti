/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.ListaMovimentiMagazzinoLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class ListaMovimentiMagazzinoClp extends BaseModelImpl<ListaMovimentiMagazzino>
	implements ListaMovimentiMagazzino {
	public ListaMovimentiMagazzinoClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ListaMovimentiMagazzino.class;
	}

	@Override
	public String getModelClassName() {
		return ListaMovimentiMagazzino.class.getName();
	}

	@Override
	public String getPrimaryKey() {
		return _ID;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setID(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _ID;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ID", getID());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("descrizione", getDescrizione());
		attributes.put("quantita", getQuantita());
		attributes.put("testCaricoScarico", getTestCaricoScarico());
		attributes.put("soloValore", getSoloValore());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String ID = (String)attributes.get("ID");

		if (ID != null) {
			setID(ID);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		String quantita = (String)attributes.get("quantita");

		if (quantita != null) {
			setQuantita(quantita);
		}

		Integer testCaricoScarico = (Integer)attributes.get("testCaricoScarico");

		if (testCaricoScarico != null) {
			setTestCaricoScarico(testCaricoScarico);
		}

		Integer soloValore = (Integer)attributes.get("soloValore");

		if (soloValore != null) {
			setSoloValore(soloValore);
		}
	}

	@Override
	public String getID() {
		return _ID;
	}

	@Override
	public void setID(String ID) {
		_ID = ID;

		if (_listaMovimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _listaMovimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setID", String.class);

				method.invoke(_listaMovimentiMagazzinoRemoteModel, ID);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	@Override
	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;

		if (_listaMovimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _listaMovimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceArticolo",
						String.class);

				method.invoke(_listaMovimentiMagazzinoRemoteModel,
					codiceArticolo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceVariante() {
		return _codiceVariante;
	}

	@Override
	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;

		if (_listaMovimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _listaMovimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceVariante",
						String.class);

				method.invoke(_listaMovimentiMagazzinoRemoteModel,
					codiceVariante);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizione() {
		return _descrizione;
	}

	@Override
	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;

		if (_listaMovimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _listaMovimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizione", String.class);

				method.invoke(_listaMovimentiMagazzinoRemoteModel, descrizione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getQuantita() {
		return _quantita;
	}

	@Override
	public void setQuantita(String quantita) {
		_quantita = quantita;

		if (_listaMovimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _listaMovimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantita", String.class);

				method.invoke(_listaMovimentiMagazzinoRemoteModel, quantita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTestCaricoScarico() {
		return _testCaricoScarico;
	}

	@Override
	public void setTestCaricoScarico(int testCaricoScarico) {
		_testCaricoScarico = testCaricoScarico;

		if (_listaMovimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _listaMovimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setTestCaricoScarico",
						int.class);

				method.invoke(_listaMovimentiMagazzinoRemoteModel,
					testCaricoScarico);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getSoloValore() {
		return _soloValore;
	}

	@Override
	public void setSoloValore(int soloValore) {
		_soloValore = soloValore;

		if (_listaMovimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _listaMovimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setSoloValore", int.class);

				method.invoke(_listaMovimentiMagazzinoRemoteModel, soloValore);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getListaMovimentiMagazzinoRemoteModel() {
		return _listaMovimentiMagazzinoRemoteModel;
	}

	public void setListaMovimentiMagazzinoRemoteModel(
		BaseModel<?> listaMovimentiMagazzinoRemoteModel) {
		_listaMovimentiMagazzinoRemoteModel = listaMovimentiMagazzinoRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _listaMovimentiMagazzinoRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_listaMovimentiMagazzinoRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ListaMovimentiMagazzinoLocalServiceUtil.addListaMovimentiMagazzino(this);
		}
		else {
			ListaMovimentiMagazzinoLocalServiceUtil.updateListaMovimentiMagazzino(this);
		}
	}

	@Override
	public ListaMovimentiMagazzino toEscapedModel() {
		return (ListaMovimentiMagazzino)ProxyUtil.newProxyInstance(ListaMovimentiMagazzino.class.getClassLoader(),
			new Class[] { ListaMovimentiMagazzino.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ListaMovimentiMagazzinoClp clone = new ListaMovimentiMagazzinoClp();

		clone.setID(getID());
		clone.setCodiceArticolo(getCodiceArticolo());
		clone.setCodiceVariante(getCodiceVariante());
		clone.setDescrizione(getDescrizione());
		clone.setQuantita(getQuantita());
		clone.setTestCaricoScarico(getTestCaricoScarico());
		clone.setSoloValore(getSoloValore());

		return clone;
	}

	@Override
	public int compareTo(ListaMovimentiMagazzino listaMovimentiMagazzino) {
		String primaryKey = listaMovimentiMagazzino.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ListaMovimentiMagazzinoClp)) {
			return false;
		}

		ListaMovimentiMagazzinoClp listaMovimentiMagazzino = (ListaMovimentiMagazzinoClp)obj;

		String primaryKey = listaMovimentiMagazzino.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{ID=");
		sb.append(getID());
		sb.append(", codiceArticolo=");
		sb.append(getCodiceArticolo());
		sb.append(", codiceVariante=");
		sb.append(getCodiceVariante());
		sb.append(", descrizione=");
		sb.append(getDescrizione());
		sb.append(", quantita=");
		sb.append(getQuantita());
		sb.append(", testCaricoScarico=");
		sb.append(getTestCaricoScarico());
		sb.append(", soloValore=");
		sb.append(getSoloValore());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(25);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.ListaMovimentiMagazzino");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>ID</column-name><column-value><![CDATA[");
		sb.append(getID());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceArticolo</column-name><column-value><![CDATA[");
		sb.append(getCodiceArticolo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceVariante</column-name><column-value><![CDATA[");
		sb.append(getCodiceVariante());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizione</column-name><column-value><![CDATA[");
		sb.append(getDescrizione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantita</column-name><column-value><![CDATA[");
		sb.append(getQuantita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>testCaricoScarico</column-name><column-value><![CDATA[");
		sb.append(getTestCaricoScarico());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>soloValore</column-name><column-value><![CDATA[");
		sb.append(getSoloValore());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _ID;
	private String _codiceArticolo;
	private String _codiceVariante;
	private String _descrizione;
	private String _quantita;
	private int _testCaricoScarico;
	private int _soloValore;
	private BaseModel<?> _listaMovimentiMagazzinoRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}