/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link DatiClientiFornitori}.
 * </p>
 *
 * @author Mario Torrisi
 * @see DatiClientiFornitori
 * @generated
 */
public class DatiClientiFornitoriWrapper implements DatiClientiFornitori,
	ModelWrapper<DatiClientiFornitori> {
	public DatiClientiFornitoriWrapper(
		DatiClientiFornitori datiClientiFornitori) {
		_datiClientiFornitori = datiClientiFornitori;
	}

	@Override
	public Class<?> getModelClass() {
		return DatiClientiFornitori.class;
	}

	@Override
	public String getModelClassName() {
		return DatiClientiFornitori.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceSoggetto", getCodiceSoggetto());
		attributes.put("codiceBanca", getCodiceBanca());
		attributes.put("codiceAgenzia", getCodiceAgenzia());
		attributes.put("codiceIdBanca", getCodiceIdBanca());
		attributes.put("codicePaese", getCodicePaese());
		attributes.put("codiceCINInt", getCodiceCINInt());
		attributes.put("codiceCINNaz", getCodiceCINNaz());
		attributes.put("numeroContoCorrente", getNumeroContoCorrente());
		attributes.put("IBAN", getIBAN());
		attributes.put("categoriaEconomica", getCategoriaEconomica());
		attributes.put("codiceEsenzioneIVA", getCodiceEsenzioneIVA());
		attributes.put("codiceDivisa", getCodiceDivisa());
		attributes.put("codicePagamento", getCodicePagamento());
		attributes.put("codiceZona", getCodiceZona());
		attributes.put("codiceAgente", getCodiceAgente());
		attributes.put("codiceGruppoAgente", getCodiceGruppoAgente());
		attributes.put("codiceCatProvv", getCodiceCatProvv());
		attributes.put("codiceSpedizione", getCodiceSpedizione());
		attributes.put("codicePorto", getCodicePorto());
		attributes.put("codiceVettore", getCodiceVettore());
		attributes.put("codiceDestDiversa", getCodiceDestDiversa());
		attributes.put("codiceListinoPrezzi", getCodiceListinoPrezzi());
		attributes.put("codiceScontoTotale", getCodiceScontoTotale());
		attributes.put("codiceScontoPreIVA", getCodiceScontoPreIVA());
		attributes.put("scontoCat1", getScontoCat1());
		attributes.put("scontoCat2", getScontoCat2());
		attributes.put("codiceLingua", getCodiceLingua());
		attributes.put("codiceLinguaEC", getCodiceLinguaEC());
		attributes.put("addebitoBolli", getAddebitoBolli());
		attributes.put("addebitoSpeseBanca", getAddebitoSpeseBanca());
		attributes.put("ragruppaBolle", getRagruppaBolle());
		attributes.put("sospensioneIVA", getSospensioneIVA());
		attributes.put("ragruppaOrdini", getRagruppaOrdini());
		attributes.put("ragruppaXDestinazione", getRagruppaXDestinazione());
		attributes.put("ragruppaXPorto", getRagruppaXPorto());
		attributes.put("percSpeseTrasporto", getPercSpeseTrasporto());
		attributes.put("prezzoDaProporre", getPrezzoDaProporre());
		attributes.put("sogettoFatturazione", getSogettoFatturazione());
		attributes.put("codiceRaggEffetti", getCodiceRaggEffetti());
		attributes.put("riportoRiferimenti", getRiportoRiferimenti());
		attributes.put("stampaPrezzo", getStampaPrezzo());
		attributes.put("bloccato", getBloccato());
		attributes.put("motivazione", getMotivazione());
		attributes.put("livelloBlocco", getLivelloBlocco());
		attributes.put("addebitoCONAI", getAddebitoCONAI());
		attributes.put("libStr1", getLibStr1());
		attributes.put("libStr2", getLibStr2());
		attributes.put("libStr3", getLibStr3());
		attributes.put("libStr4", getLibStr4());
		attributes.put("libStr5", getLibStr5());
		attributes.put("libDat1", getLibDat1());
		attributes.put("libDat2", getLibDat2());
		attributes.put("libDat3", getLibDat3());
		attributes.put("libDat4", getLibDat4());
		attributes.put("libDat5", getLibDat5());
		attributes.put("libLng1", getLibLng1());
		attributes.put("libLng2", getLibLng2());
		attributes.put("libLng3", getLibLng3());
		attributes.put("libLng4", getLibLng4());
		attributes.put("libLng5", getLibLng5());
		attributes.put("libDbl1", getLibDbl1());
		attributes.put("libDbl2", getLibDbl2());
		attributes.put("libDbl3", getLibDbl3());
		attributes.put("libDbl4", getLibDbl4());
		attributes.put("libDbl5", getLibDbl5());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceSoggetto = (String)attributes.get("codiceSoggetto");

		if (codiceSoggetto != null) {
			setCodiceSoggetto(codiceSoggetto);
		}

		String codiceBanca = (String)attributes.get("codiceBanca");

		if (codiceBanca != null) {
			setCodiceBanca(codiceBanca);
		}

		String codiceAgenzia = (String)attributes.get("codiceAgenzia");

		if (codiceAgenzia != null) {
			setCodiceAgenzia(codiceAgenzia);
		}

		String codiceIdBanca = (String)attributes.get("codiceIdBanca");

		if (codiceIdBanca != null) {
			setCodiceIdBanca(codiceIdBanca);
		}

		String codicePaese = (String)attributes.get("codicePaese");

		if (codicePaese != null) {
			setCodicePaese(codicePaese);
		}

		String codiceCINInt = (String)attributes.get("codiceCINInt");

		if (codiceCINInt != null) {
			setCodiceCINInt(codiceCINInt);
		}

		String codiceCINNaz = (String)attributes.get("codiceCINNaz");

		if (codiceCINNaz != null) {
			setCodiceCINNaz(codiceCINNaz);
		}

		String numeroContoCorrente = (String)attributes.get(
				"numeroContoCorrente");

		if (numeroContoCorrente != null) {
			setNumeroContoCorrente(numeroContoCorrente);
		}

		String IBAN = (String)attributes.get("IBAN");

		if (IBAN != null) {
			setIBAN(IBAN);
		}

		String categoriaEconomica = (String)attributes.get("categoriaEconomica");

		if (categoriaEconomica != null) {
			setCategoriaEconomica(categoriaEconomica);
		}

		String codiceEsenzioneIVA = (String)attributes.get("codiceEsenzioneIVA");

		if (codiceEsenzioneIVA != null) {
			setCodiceEsenzioneIVA(codiceEsenzioneIVA);
		}

		String codiceDivisa = (String)attributes.get("codiceDivisa");

		if (codiceDivisa != null) {
			setCodiceDivisa(codiceDivisa);
		}

		String codicePagamento = (String)attributes.get("codicePagamento");

		if (codicePagamento != null) {
			setCodicePagamento(codicePagamento);
		}

		String codiceZona = (String)attributes.get("codiceZona");

		if (codiceZona != null) {
			setCodiceZona(codiceZona);
		}

		String codiceAgente = (String)attributes.get("codiceAgente");

		if (codiceAgente != null) {
			setCodiceAgente(codiceAgente);
		}

		String codiceGruppoAgente = (String)attributes.get("codiceGruppoAgente");

		if (codiceGruppoAgente != null) {
			setCodiceGruppoAgente(codiceGruppoAgente);
		}

		String codiceCatProvv = (String)attributes.get("codiceCatProvv");

		if (codiceCatProvv != null) {
			setCodiceCatProvv(codiceCatProvv);
		}

		String codiceSpedizione = (String)attributes.get("codiceSpedizione");

		if (codiceSpedizione != null) {
			setCodiceSpedizione(codiceSpedizione);
		}

		String codicePorto = (String)attributes.get("codicePorto");

		if (codicePorto != null) {
			setCodicePorto(codicePorto);
		}

		String codiceVettore = (String)attributes.get("codiceVettore");

		if (codiceVettore != null) {
			setCodiceVettore(codiceVettore);
		}

		String codiceDestDiversa = (String)attributes.get("codiceDestDiversa");

		if (codiceDestDiversa != null) {
			setCodiceDestDiversa(codiceDestDiversa);
		}

		String codiceListinoPrezzi = (String)attributes.get(
				"codiceListinoPrezzi");

		if (codiceListinoPrezzi != null) {
			setCodiceListinoPrezzi(codiceListinoPrezzi);
		}

		String codiceScontoTotale = (String)attributes.get("codiceScontoTotale");

		if (codiceScontoTotale != null) {
			setCodiceScontoTotale(codiceScontoTotale);
		}

		String codiceScontoPreIVA = (String)attributes.get("codiceScontoPreIVA");

		if (codiceScontoPreIVA != null) {
			setCodiceScontoPreIVA(codiceScontoPreIVA);
		}

		String scontoCat1 = (String)attributes.get("scontoCat1");

		if (scontoCat1 != null) {
			setScontoCat1(scontoCat1);
		}

		String scontoCat2 = (String)attributes.get("scontoCat2");

		if (scontoCat2 != null) {
			setScontoCat2(scontoCat2);
		}

		String codiceLingua = (String)attributes.get("codiceLingua");

		if (codiceLingua != null) {
			setCodiceLingua(codiceLingua);
		}

		String codiceLinguaEC = (String)attributes.get("codiceLinguaEC");

		if (codiceLinguaEC != null) {
			setCodiceLinguaEC(codiceLinguaEC);
		}

		Boolean addebitoBolli = (Boolean)attributes.get("addebitoBolli");

		if (addebitoBolli != null) {
			setAddebitoBolli(addebitoBolli);
		}

		Boolean addebitoSpeseBanca = (Boolean)attributes.get(
				"addebitoSpeseBanca");

		if (addebitoSpeseBanca != null) {
			setAddebitoSpeseBanca(addebitoSpeseBanca);
		}

		Boolean ragruppaBolle = (Boolean)attributes.get("ragruppaBolle");

		if (ragruppaBolle != null) {
			setRagruppaBolle(ragruppaBolle);
		}

		Boolean sospensioneIVA = (Boolean)attributes.get("sospensioneIVA");

		if (sospensioneIVA != null) {
			setSospensioneIVA(sospensioneIVA);
		}

		Integer ragruppaOrdini = (Integer)attributes.get("ragruppaOrdini");

		if (ragruppaOrdini != null) {
			setRagruppaOrdini(ragruppaOrdini);
		}

		Boolean ragruppaXDestinazione = (Boolean)attributes.get(
				"ragruppaXDestinazione");

		if (ragruppaXDestinazione != null) {
			setRagruppaXDestinazione(ragruppaXDestinazione);
		}

		Boolean ragruppaXPorto = (Boolean)attributes.get("ragruppaXPorto");

		if (ragruppaXPorto != null) {
			setRagruppaXPorto(ragruppaXPorto);
		}

		Double percSpeseTrasporto = (Double)attributes.get("percSpeseTrasporto");

		if (percSpeseTrasporto != null) {
			setPercSpeseTrasporto(percSpeseTrasporto);
		}

		Integer prezzoDaProporre = (Integer)attributes.get("prezzoDaProporre");

		if (prezzoDaProporre != null) {
			setPrezzoDaProporre(prezzoDaProporre);
		}

		String sogettoFatturazione = (String)attributes.get(
				"sogettoFatturazione");

		if (sogettoFatturazione != null) {
			setSogettoFatturazione(sogettoFatturazione);
		}

		String codiceRaggEffetti = (String)attributes.get("codiceRaggEffetti");

		if (codiceRaggEffetti != null) {
			setCodiceRaggEffetti(codiceRaggEffetti);
		}

		Integer riportoRiferimenti = (Integer)attributes.get(
				"riportoRiferimenti");

		if (riportoRiferimenti != null) {
			setRiportoRiferimenti(riportoRiferimenti);
		}

		Boolean stampaPrezzo = (Boolean)attributes.get("stampaPrezzo");

		if (stampaPrezzo != null) {
			setStampaPrezzo(stampaPrezzo);
		}

		Integer bloccato = (Integer)attributes.get("bloccato");

		if (bloccato != null) {
			setBloccato(bloccato);
		}

		String motivazione = (String)attributes.get("motivazione");

		if (motivazione != null) {
			setMotivazione(motivazione);
		}

		Integer livelloBlocco = (Integer)attributes.get("livelloBlocco");

		if (livelloBlocco != null) {
			setLivelloBlocco(livelloBlocco);
		}

		Boolean addebitoCONAI = (Boolean)attributes.get("addebitoCONAI");

		if (addebitoCONAI != null) {
			setAddebitoCONAI(addebitoCONAI);
		}

		String libStr1 = (String)attributes.get("libStr1");

		if (libStr1 != null) {
			setLibStr1(libStr1);
		}

		String libStr2 = (String)attributes.get("libStr2");

		if (libStr2 != null) {
			setLibStr2(libStr2);
		}

		String libStr3 = (String)attributes.get("libStr3");

		if (libStr3 != null) {
			setLibStr3(libStr3);
		}

		String libStr4 = (String)attributes.get("libStr4");

		if (libStr4 != null) {
			setLibStr4(libStr4);
		}

		String libStr5 = (String)attributes.get("libStr5");

		if (libStr5 != null) {
			setLibStr5(libStr5);
		}

		Date libDat1 = (Date)attributes.get("libDat1");

		if (libDat1 != null) {
			setLibDat1(libDat1);
		}

		Date libDat2 = (Date)attributes.get("libDat2");

		if (libDat2 != null) {
			setLibDat2(libDat2);
		}

		Date libDat3 = (Date)attributes.get("libDat3");

		if (libDat3 != null) {
			setLibDat3(libDat3);
		}

		Date libDat4 = (Date)attributes.get("libDat4");

		if (libDat4 != null) {
			setLibDat4(libDat4);
		}

		Date libDat5 = (Date)attributes.get("libDat5");

		if (libDat5 != null) {
			setLibDat5(libDat5);
		}

		Long libLng1 = (Long)attributes.get("libLng1");

		if (libLng1 != null) {
			setLibLng1(libLng1);
		}

		Long libLng2 = (Long)attributes.get("libLng2");

		if (libLng2 != null) {
			setLibLng2(libLng2);
		}

		Long libLng3 = (Long)attributes.get("libLng3");

		if (libLng3 != null) {
			setLibLng3(libLng3);
		}

		Long libLng4 = (Long)attributes.get("libLng4");

		if (libLng4 != null) {
			setLibLng4(libLng4);
		}

		Long libLng5 = (Long)attributes.get("libLng5");

		if (libLng5 != null) {
			setLibLng5(libLng5);
		}

		Double libDbl1 = (Double)attributes.get("libDbl1");

		if (libDbl1 != null) {
			setLibDbl1(libDbl1);
		}

		Double libDbl2 = (Double)attributes.get("libDbl2");

		if (libDbl2 != null) {
			setLibDbl2(libDbl2);
		}

		Double libDbl3 = (Double)attributes.get("libDbl3");

		if (libDbl3 != null) {
			setLibDbl3(libDbl3);
		}

		Double libDbl4 = (Double)attributes.get("libDbl4");

		if (libDbl4 != null) {
			setLibDbl4(libDbl4);
		}

		Double libDbl5 = (Double)attributes.get("libDbl5");

		if (libDbl5 != null) {
			setLibDbl5(libDbl5);
		}
	}

	/**
	* Returns the primary key of this dati clienti fornitori.
	*
	* @return the primary key of this dati clienti fornitori
	*/
	@Override
	public it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK getPrimaryKey() {
		return _datiClientiFornitori.getPrimaryKey();
	}

	/**
	* Sets the primary key of this dati clienti fornitori.
	*
	* @param primaryKey the primary key of this dati clienti fornitori
	*/
	@Override
	public void setPrimaryKey(
		it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK primaryKey) {
		_datiClientiFornitori.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the tipo soggetto of this dati clienti fornitori.
	*
	* @return the tipo soggetto of this dati clienti fornitori
	*/
	@Override
	public boolean getTipoSoggetto() {
		return _datiClientiFornitori.getTipoSoggetto();
	}

	/**
	* Returns <code>true</code> if this dati clienti fornitori is tipo soggetto.
	*
	* @return <code>true</code> if this dati clienti fornitori is tipo soggetto; <code>false</code> otherwise
	*/
	@Override
	public boolean isTipoSoggetto() {
		return _datiClientiFornitori.isTipoSoggetto();
	}

	/**
	* Sets whether this dati clienti fornitori is tipo soggetto.
	*
	* @param tipoSoggetto the tipo soggetto of this dati clienti fornitori
	*/
	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_datiClientiFornitori.setTipoSoggetto(tipoSoggetto);
	}

	/**
	* Returns the codice soggetto of this dati clienti fornitori.
	*
	* @return the codice soggetto of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceSoggetto() {
		return _datiClientiFornitori.getCodiceSoggetto();
	}

	/**
	* Sets the codice soggetto of this dati clienti fornitori.
	*
	* @param codiceSoggetto the codice soggetto of this dati clienti fornitori
	*/
	@Override
	public void setCodiceSoggetto(java.lang.String codiceSoggetto) {
		_datiClientiFornitori.setCodiceSoggetto(codiceSoggetto);
	}

	/**
	* Returns the codice banca of this dati clienti fornitori.
	*
	* @return the codice banca of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceBanca() {
		return _datiClientiFornitori.getCodiceBanca();
	}

	/**
	* Sets the codice banca of this dati clienti fornitori.
	*
	* @param codiceBanca the codice banca of this dati clienti fornitori
	*/
	@Override
	public void setCodiceBanca(java.lang.String codiceBanca) {
		_datiClientiFornitori.setCodiceBanca(codiceBanca);
	}

	/**
	* Returns the codice agenzia of this dati clienti fornitori.
	*
	* @return the codice agenzia of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceAgenzia() {
		return _datiClientiFornitori.getCodiceAgenzia();
	}

	/**
	* Sets the codice agenzia of this dati clienti fornitori.
	*
	* @param codiceAgenzia the codice agenzia of this dati clienti fornitori
	*/
	@Override
	public void setCodiceAgenzia(java.lang.String codiceAgenzia) {
		_datiClientiFornitori.setCodiceAgenzia(codiceAgenzia);
	}

	/**
	* Returns the codice ID banca of this dati clienti fornitori.
	*
	* @return the codice ID banca of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceIdBanca() {
		return _datiClientiFornitori.getCodiceIdBanca();
	}

	/**
	* Sets the codice ID banca of this dati clienti fornitori.
	*
	* @param codiceIdBanca the codice ID banca of this dati clienti fornitori
	*/
	@Override
	public void setCodiceIdBanca(java.lang.String codiceIdBanca) {
		_datiClientiFornitori.setCodiceIdBanca(codiceIdBanca);
	}

	/**
	* Returns the codice paese of this dati clienti fornitori.
	*
	* @return the codice paese of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodicePaese() {
		return _datiClientiFornitori.getCodicePaese();
	}

	/**
	* Sets the codice paese of this dati clienti fornitori.
	*
	* @param codicePaese the codice paese of this dati clienti fornitori
	*/
	@Override
	public void setCodicePaese(java.lang.String codicePaese) {
		_datiClientiFornitori.setCodicePaese(codicePaese);
	}

	/**
	* Returns the codice c i n int of this dati clienti fornitori.
	*
	* @return the codice c i n int of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceCINInt() {
		return _datiClientiFornitori.getCodiceCINInt();
	}

	/**
	* Sets the codice c i n int of this dati clienti fornitori.
	*
	* @param codiceCINInt the codice c i n int of this dati clienti fornitori
	*/
	@Override
	public void setCodiceCINInt(java.lang.String codiceCINInt) {
		_datiClientiFornitori.setCodiceCINInt(codiceCINInt);
	}

	/**
	* Returns the codice c i n naz of this dati clienti fornitori.
	*
	* @return the codice c i n naz of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceCINNaz() {
		return _datiClientiFornitori.getCodiceCINNaz();
	}

	/**
	* Sets the codice c i n naz of this dati clienti fornitori.
	*
	* @param codiceCINNaz the codice c i n naz of this dati clienti fornitori
	*/
	@Override
	public void setCodiceCINNaz(java.lang.String codiceCINNaz) {
		_datiClientiFornitori.setCodiceCINNaz(codiceCINNaz);
	}

	/**
	* Returns the numero conto corrente of this dati clienti fornitori.
	*
	* @return the numero conto corrente of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getNumeroContoCorrente() {
		return _datiClientiFornitori.getNumeroContoCorrente();
	}

	/**
	* Sets the numero conto corrente of this dati clienti fornitori.
	*
	* @param numeroContoCorrente the numero conto corrente of this dati clienti fornitori
	*/
	@Override
	public void setNumeroContoCorrente(java.lang.String numeroContoCorrente) {
		_datiClientiFornitori.setNumeroContoCorrente(numeroContoCorrente);
	}

	/**
	* Returns the i b a n of this dati clienti fornitori.
	*
	* @return the i b a n of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getIBAN() {
		return _datiClientiFornitori.getIBAN();
	}

	/**
	* Sets the i b a n of this dati clienti fornitori.
	*
	* @param IBAN the i b a n of this dati clienti fornitori
	*/
	@Override
	public void setIBAN(java.lang.String IBAN) {
		_datiClientiFornitori.setIBAN(IBAN);
	}

	/**
	* Returns the categoria economica of this dati clienti fornitori.
	*
	* @return the categoria economica of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCategoriaEconomica() {
		return _datiClientiFornitori.getCategoriaEconomica();
	}

	/**
	* Sets the categoria economica of this dati clienti fornitori.
	*
	* @param categoriaEconomica the categoria economica of this dati clienti fornitori
	*/
	@Override
	public void setCategoriaEconomica(java.lang.String categoriaEconomica) {
		_datiClientiFornitori.setCategoriaEconomica(categoriaEconomica);
	}

	/**
	* Returns the codice esenzione i v a of this dati clienti fornitori.
	*
	* @return the codice esenzione i v a of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceEsenzioneIVA() {
		return _datiClientiFornitori.getCodiceEsenzioneIVA();
	}

	/**
	* Sets the codice esenzione i v a of this dati clienti fornitori.
	*
	* @param codiceEsenzioneIVA the codice esenzione i v a of this dati clienti fornitori
	*/
	@Override
	public void setCodiceEsenzioneIVA(java.lang.String codiceEsenzioneIVA) {
		_datiClientiFornitori.setCodiceEsenzioneIVA(codiceEsenzioneIVA);
	}

	/**
	* Returns the codice divisa of this dati clienti fornitori.
	*
	* @return the codice divisa of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceDivisa() {
		return _datiClientiFornitori.getCodiceDivisa();
	}

	/**
	* Sets the codice divisa of this dati clienti fornitori.
	*
	* @param codiceDivisa the codice divisa of this dati clienti fornitori
	*/
	@Override
	public void setCodiceDivisa(java.lang.String codiceDivisa) {
		_datiClientiFornitori.setCodiceDivisa(codiceDivisa);
	}

	/**
	* Returns the codice pagamento of this dati clienti fornitori.
	*
	* @return the codice pagamento of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodicePagamento() {
		return _datiClientiFornitori.getCodicePagamento();
	}

	/**
	* Sets the codice pagamento of this dati clienti fornitori.
	*
	* @param codicePagamento the codice pagamento of this dati clienti fornitori
	*/
	@Override
	public void setCodicePagamento(java.lang.String codicePagamento) {
		_datiClientiFornitori.setCodicePagamento(codicePagamento);
	}

	/**
	* Returns the codice zona of this dati clienti fornitori.
	*
	* @return the codice zona of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceZona() {
		return _datiClientiFornitori.getCodiceZona();
	}

	/**
	* Sets the codice zona of this dati clienti fornitori.
	*
	* @param codiceZona the codice zona of this dati clienti fornitori
	*/
	@Override
	public void setCodiceZona(java.lang.String codiceZona) {
		_datiClientiFornitori.setCodiceZona(codiceZona);
	}

	/**
	* Returns the codice agente of this dati clienti fornitori.
	*
	* @return the codice agente of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceAgente() {
		return _datiClientiFornitori.getCodiceAgente();
	}

	/**
	* Sets the codice agente of this dati clienti fornitori.
	*
	* @param codiceAgente the codice agente of this dati clienti fornitori
	*/
	@Override
	public void setCodiceAgente(java.lang.String codiceAgente) {
		_datiClientiFornitori.setCodiceAgente(codiceAgente);
	}

	/**
	* Returns the codice gruppo agente of this dati clienti fornitori.
	*
	* @return the codice gruppo agente of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceGruppoAgente() {
		return _datiClientiFornitori.getCodiceGruppoAgente();
	}

	/**
	* Sets the codice gruppo agente of this dati clienti fornitori.
	*
	* @param codiceGruppoAgente the codice gruppo agente of this dati clienti fornitori
	*/
	@Override
	public void setCodiceGruppoAgente(java.lang.String codiceGruppoAgente) {
		_datiClientiFornitori.setCodiceGruppoAgente(codiceGruppoAgente);
	}

	/**
	* Returns the codice cat provv of this dati clienti fornitori.
	*
	* @return the codice cat provv of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceCatProvv() {
		return _datiClientiFornitori.getCodiceCatProvv();
	}

	/**
	* Sets the codice cat provv of this dati clienti fornitori.
	*
	* @param codiceCatProvv the codice cat provv of this dati clienti fornitori
	*/
	@Override
	public void setCodiceCatProvv(java.lang.String codiceCatProvv) {
		_datiClientiFornitori.setCodiceCatProvv(codiceCatProvv);
	}

	/**
	* Returns the codice spedizione of this dati clienti fornitori.
	*
	* @return the codice spedizione of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceSpedizione() {
		return _datiClientiFornitori.getCodiceSpedizione();
	}

	/**
	* Sets the codice spedizione of this dati clienti fornitori.
	*
	* @param codiceSpedizione the codice spedizione of this dati clienti fornitori
	*/
	@Override
	public void setCodiceSpedizione(java.lang.String codiceSpedizione) {
		_datiClientiFornitori.setCodiceSpedizione(codiceSpedizione);
	}

	/**
	* Returns the codice porto of this dati clienti fornitori.
	*
	* @return the codice porto of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodicePorto() {
		return _datiClientiFornitori.getCodicePorto();
	}

	/**
	* Sets the codice porto of this dati clienti fornitori.
	*
	* @param codicePorto the codice porto of this dati clienti fornitori
	*/
	@Override
	public void setCodicePorto(java.lang.String codicePorto) {
		_datiClientiFornitori.setCodicePorto(codicePorto);
	}

	/**
	* Returns the codice vettore of this dati clienti fornitori.
	*
	* @return the codice vettore of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceVettore() {
		return _datiClientiFornitori.getCodiceVettore();
	}

	/**
	* Sets the codice vettore of this dati clienti fornitori.
	*
	* @param codiceVettore the codice vettore of this dati clienti fornitori
	*/
	@Override
	public void setCodiceVettore(java.lang.String codiceVettore) {
		_datiClientiFornitori.setCodiceVettore(codiceVettore);
	}

	/**
	* Returns the codice dest diversa of this dati clienti fornitori.
	*
	* @return the codice dest diversa of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceDestDiversa() {
		return _datiClientiFornitori.getCodiceDestDiversa();
	}

	/**
	* Sets the codice dest diversa of this dati clienti fornitori.
	*
	* @param codiceDestDiversa the codice dest diversa of this dati clienti fornitori
	*/
	@Override
	public void setCodiceDestDiversa(java.lang.String codiceDestDiversa) {
		_datiClientiFornitori.setCodiceDestDiversa(codiceDestDiversa);
	}

	/**
	* Returns the codice listino prezzi of this dati clienti fornitori.
	*
	* @return the codice listino prezzi of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceListinoPrezzi() {
		return _datiClientiFornitori.getCodiceListinoPrezzi();
	}

	/**
	* Sets the codice listino prezzi of this dati clienti fornitori.
	*
	* @param codiceListinoPrezzi the codice listino prezzi of this dati clienti fornitori
	*/
	@Override
	public void setCodiceListinoPrezzi(java.lang.String codiceListinoPrezzi) {
		_datiClientiFornitori.setCodiceListinoPrezzi(codiceListinoPrezzi);
	}

	/**
	* Returns the codice sconto totale of this dati clienti fornitori.
	*
	* @return the codice sconto totale of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceScontoTotale() {
		return _datiClientiFornitori.getCodiceScontoTotale();
	}

	/**
	* Sets the codice sconto totale of this dati clienti fornitori.
	*
	* @param codiceScontoTotale the codice sconto totale of this dati clienti fornitori
	*/
	@Override
	public void setCodiceScontoTotale(java.lang.String codiceScontoTotale) {
		_datiClientiFornitori.setCodiceScontoTotale(codiceScontoTotale);
	}

	/**
	* Returns the codice sconto pre i v a of this dati clienti fornitori.
	*
	* @return the codice sconto pre i v a of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceScontoPreIVA() {
		return _datiClientiFornitori.getCodiceScontoPreIVA();
	}

	/**
	* Sets the codice sconto pre i v a of this dati clienti fornitori.
	*
	* @param codiceScontoPreIVA the codice sconto pre i v a of this dati clienti fornitori
	*/
	@Override
	public void setCodiceScontoPreIVA(java.lang.String codiceScontoPreIVA) {
		_datiClientiFornitori.setCodiceScontoPreIVA(codiceScontoPreIVA);
	}

	/**
	* Returns the sconto cat1 of this dati clienti fornitori.
	*
	* @return the sconto cat1 of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getScontoCat1() {
		return _datiClientiFornitori.getScontoCat1();
	}

	/**
	* Sets the sconto cat1 of this dati clienti fornitori.
	*
	* @param scontoCat1 the sconto cat1 of this dati clienti fornitori
	*/
	@Override
	public void setScontoCat1(java.lang.String scontoCat1) {
		_datiClientiFornitori.setScontoCat1(scontoCat1);
	}

	/**
	* Returns the sconto cat2 of this dati clienti fornitori.
	*
	* @return the sconto cat2 of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getScontoCat2() {
		return _datiClientiFornitori.getScontoCat2();
	}

	/**
	* Sets the sconto cat2 of this dati clienti fornitori.
	*
	* @param scontoCat2 the sconto cat2 of this dati clienti fornitori
	*/
	@Override
	public void setScontoCat2(java.lang.String scontoCat2) {
		_datiClientiFornitori.setScontoCat2(scontoCat2);
	}

	/**
	* Returns the codice lingua of this dati clienti fornitori.
	*
	* @return the codice lingua of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceLingua() {
		return _datiClientiFornitori.getCodiceLingua();
	}

	/**
	* Sets the codice lingua of this dati clienti fornitori.
	*
	* @param codiceLingua the codice lingua of this dati clienti fornitori
	*/
	@Override
	public void setCodiceLingua(java.lang.String codiceLingua) {
		_datiClientiFornitori.setCodiceLingua(codiceLingua);
	}

	/**
	* Returns the codice lingua e c of this dati clienti fornitori.
	*
	* @return the codice lingua e c of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceLinguaEC() {
		return _datiClientiFornitori.getCodiceLinguaEC();
	}

	/**
	* Sets the codice lingua e c of this dati clienti fornitori.
	*
	* @param codiceLinguaEC the codice lingua e c of this dati clienti fornitori
	*/
	@Override
	public void setCodiceLinguaEC(java.lang.String codiceLinguaEC) {
		_datiClientiFornitori.setCodiceLinguaEC(codiceLinguaEC);
	}

	/**
	* Returns the addebito bolli of this dati clienti fornitori.
	*
	* @return the addebito bolli of this dati clienti fornitori
	*/
	@Override
	public boolean getAddebitoBolli() {
		return _datiClientiFornitori.getAddebitoBolli();
	}

	/**
	* Returns <code>true</code> if this dati clienti fornitori is addebito bolli.
	*
	* @return <code>true</code> if this dati clienti fornitori is addebito bolli; <code>false</code> otherwise
	*/
	@Override
	public boolean isAddebitoBolli() {
		return _datiClientiFornitori.isAddebitoBolli();
	}

	/**
	* Sets whether this dati clienti fornitori is addebito bolli.
	*
	* @param addebitoBolli the addebito bolli of this dati clienti fornitori
	*/
	@Override
	public void setAddebitoBolli(boolean addebitoBolli) {
		_datiClientiFornitori.setAddebitoBolli(addebitoBolli);
	}

	/**
	* Returns the addebito spese banca of this dati clienti fornitori.
	*
	* @return the addebito spese banca of this dati clienti fornitori
	*/
	@Override
	public boolean getAddebitoSpeseBanca() {
		return _datiClientiFornitori.getAddebitoSpeseBanca();
	}

	/**
	* Returns <code>true</code> if this dati clienti fornitori is addebito spese banca.
	*
	* @return <code>true</code> if this dati clienti fornitori is addebito spese banca; <code>false</code> otherwise
	*/
	@Override
	public boolean isAddebitoSpeseBanca() {
		return _datiClientiFornitori.isAddebitoSpeseBanca();
	}

	/**
	* Sets whether this dati clienti fornitori is addebito spese banca.
	*
	* @param addebitoSpeseBanca the addebito spese banca of this dati clienti fornitori
	*/
	@Override
	public void setAddebitoSpeseBanca(boolean addebitoSpeseBanca) {
		_datiClientiFornitori.setAddebitoSpeseBanca(addebitoSpeseBanca);
	}

	/**
	* Returns the ragruppa bolle of this dati clienti fornitori.
	*
	* @return the ragruppa bolle of this dati clienti fornitori
	*/
	@Override
	public boolean getRagruppaBolle() {
		return _datiClientiFornitori.getRagruppaBolle();
	}

	/**
	* Returns <code>true</code> if this dati clienti fornitori is ragruppa bolle.
	*
	* @return <code>true</code> if this dati clienti fornitori is ragruppa bolle; <code>false</code> otherwise
	*/
	@Override
	public boolean isRagruppaBolle() {
		return _datiClientiFornitori.isRagruppaBolle();
	}

	/**
	* Sets whether this dati clienti fornitori is ragruppa bolle.
	*
	* @param ragruppaBolle the ragruppa bolle of this dati clienti fornitori
	*/
	@Override
	public void setRagruppaBolle(boolean ragruppaBolle) {
		_datiClientiFornitori.setRagruppaBolle(ragruppaBolle);
	}

	/**
	* Returns the sospensione i v a of this dati clienti fornitori.
	*
	* @return the sospensione i v a of this dati clienti fornitori
	*/
	@Override
	public boolean getSospensioneIVA() {
		return _datiClientiFornitori.getSospensioneIVA();
	}

	/**
	* Returns <code>true</code> if this dati clienti fornitori is sospensione i v a.
	*
	* @return <code>true</code> if this dati clienti fornitori is sospensione i v a; <code>false</code> otherwise
	*/
	@Override
	public boolean isSospensioneIVA() {
		return _datiClientiFornitori.isSospensioneIVA();
	}

	/**
	* Sets whether this dati clienti fornitori is sospensione i v a.
	*
	* @param sospensioneIVA the sospensione i v a of this dati clienti fornitori
	*/
	@Override
	public void setSospensioneIVA(boolean sospensioneIVA) {
		_datiClientiFornitori.setSospensioneIVA(sospensioneIVA);
	}

	/**
	* Returns the ragruppa ordini of this dati clienti fornitori.
	*
	* @return the ragruppa ordini of this dati clienti fornitori
	*/
	@Override
	public int getRagruppaOrdini() {
		return _datiClientiFornitori.getRagruppaOrdini();
	}

	/**
	* Sets the ragruppa ordini of this dati clienti fornitori.
	*
	* @param ragruppaOrdini the ragruppa ordini of this dati clienti fornitori
	*/
	@Override
	public void setRagruppaOrdini(int ragruppaOrdini) {
		_datiClientiFornitori.setRagruppaOrdini(ragruppaOrdini);
	}

	/**
	* Returns the ragruppa x destinazione of this dati clienti fornitori.
	*
	* @return the ragruppa x destinazione of this dati clienti fornitori
	*/
	@Override
	public boolean getRagruppaXDestinazione() {
		return _datiClientiFornitori.getRagruppaXDestinazione();
	}

	/**
	* Returns <code>true</code> if this dati clienti fornitori is ragruppa x destinazione.
	*
	* @return <code>true</code> if this dati clienti fornitori is ragruppa x destinazione; <code>false</code> otherwise
	*/
	@Override
	public boolean isRagruppaXDestinazione() {
		return _datiClientiFornitori.isRagruppaXDestinazione();
	}

	/**
	* Sets whether this dati clienti fornitori is ragruppa x destinazione.
	*
	* @param ragruppaXDestinazione the ragruppa x destinazione of this dati clienti fornitori
	*/
	@Override
	public void setRagruppaXDestinazione(boolean ragruppaXDestinazione) {
		_datiClientiFornitori.setRagruppaXDestinazione(ragruppaXDestinazione);
	}

	/**
	* Returns the ragruppa x porto of this dati clienti fornitori.
	*
	* @return the ragruppa x porto of this dati clienti fornitori
	*/
	@Override
	public boolean getRagruppaXPorto() {
		return _datiClientiFornitori.getRagruppaXPorto();
	}

	/**
	* Returns <code>true</code> if this dati clienti fornitori is ragruppa x porto.
	*
	* @return <code>true</code> if this dati clienti fornitori is ragruppa x porto; <code>false</code> otherwise
	*/
	@Override
	public boolean isRagruppaXPorto() {
		return _datiClientiFornitori.isRagruppaXPorto();
	}

	/**
	* Sets whether this dati clienti fornitori is ragruppa x porto.
	*
	* @param ragruppaXPorto the ragruppa x porto of this dati clienti fornitori
	*/
	@Override
	public void setRagruppaXPorto(boolean ragruppaXPorto) {
		_datiClientiFornitori.setRagruppaXPorto(ragruppaXPorto);
	}

	/**
	* Returns the perc spese trasporto of this dati clienti fornitori.
	*
	* @return the perc spese trasporto of this dati clienti fornitori
	*/
	@Override
	public double getPercSpeseTrasporto() {
		return _datiClientiFornitori.getPercSpeseTrasporto();
	}

	/**
	* Sets the perc spese trasporto of this dati clienti fornitori.
	*
	* @param percSpeseTrasporto the perc spese trasporto of this dati clienti fornitori
	*/
	@Override
	public void setPercSpeseTrasporto(double percSpeseTrasporto) {
		_datiClientiFornitori.setPercSpeseTrasporto(percSpeseTrasporto);
	}

	/**
	* Returns the prezzo da proporre of this dati clienti fornitori.
	*
	* @return the prezzo da proporre of this dati clienti fornitori
	*/
	@Override
	public int getPrezzoDaProporre() {
		return _datiClientiFornitori.getPrezzoDaProporre();
	}

	/**
	* Sets the prezzo da proporre of this dati clienti fornitori.
	*
	* @param prezzoDaProporre the prezzo da proporre of this dati clienti fornitori
	*/
	@Override
	public void setPrezzoDaProporre(int prezzoDaProporre) {
		_datiClientiFornitori.setPrezzoDaProporre(prezzoDaProporre);
	}

	/**
	* Returns the sogetto fatturazione of this dati clienti fornitori.
	*
	* @return the sogetto fatturazione of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getSogettoFatturazione() {
		return _datiClientiFornitori.getSogettoFatturazione();
	}

	/**
	* Sets the sogetto fatturazione of this dati clienti fornitori.
	*
	* @param sogettoFatturazione the sogetto fatturazione of this dati clienti fornitori
	*/
	@Override
	public void setSogettoFatturazione(java.lang.String sogettoFatturazione) {
		_datiClientiFornitori.setSogettoFatturazione(sogettoFatturazione);
	}

	/**
	* Returns the codice ragg effetti of this dati clienti fornitori.
	*
	* @return the codice ragg effetti of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceRaggEffetti() {
		return _datiClientiFornitori.getCodiceRaggEffetti();
	}

	/**
	* Sets the codice ragg effetti of this dati clienti fornitori.
	*
	* @param codiceRaggEffetti the codice ragg effetti of this dati clienti fornitori
	*/
	@Override
	public void setCodiceRaggEffetti(java.lang.String codiceRaggEffetti) {
		_datiClientiFornitori.setCodiceRaggEffetti(codiceRaggEffetti);
	}

	/**
	* Returns the riporto riferimenti of this dati clienti fornitori.
	*
	* @return the riporto riferimenti of this dati clienti fornitori
	*/
	@Override
	public int getRiportoRiferimenti() {
		return _datiClientiFornitori.getRiportoRiferimenti();
	}

	/**
	* Sets the riporto riferimenti of this dati clienti fornitori.
	*
	* @param riportoRiferimenti the riporto riferimenti of this dati clienti fornitori
	*/
	@Override
	public void setRiportoRiferimenti(int riportoRiferimenti) {
		_datiClientiFornitori.setRiportoRiferimenti(riportoRiferimenti);
	}

	/**
	* Returns the stampa prezzo of this dati clienti fornitori.
	*
	* @return the stampa prezzo of this dati clienti fornitori
	*/
	@Override
	public boolean getStampaPrezzo() {
		return _datiClientiFornitori.getStampaPrezzo();
	}

	/**
	* Returns <code>true</code> if this dati clienti fornitori is stampa prezzo.
	*
	* @return <code>true</code> if this dati clienti fornitori is stampa prezzo; <code>false</code> otherwise
	*/
	@Override
	public boolean isStampaPrezzo() {
		return _datiClientiFornitori.isStampaPrezzo();
	}

	/**
	* Sets whether this dati clienti fornitori is stampa prezzo.
	*
	* @param stampaPrezzo the stampa prezzo of this dati clienti fornitori
	*/
	@Override
	public void setStampaPrezzo(boolean stampaPrezzo) {
		_datiClientiFornitori.setStampaPrezzo(stampaPrezzo);
	}

	/**
	* Returns the bloccato of this dati clienti fornitori.
	*
	* @return the bloccato of this dati clienti fornitori
	*/
	@Override
	public int getBloccato() {
		return _datiClientiFornitori.getBloccato();
	}

	/**
	* Sets the bloccato of this dati clienti fornitori.
	*
	* @param bloccato the bloccato of this dati clienti fornitori
	*/
	@Override
	public void setBloccato(int bloccato) {
		_datiClientiFornitori.setBloccato(bloccato);
	}

	/**
	* Returns the motivazione of this dati clienti fornitori.
	*
	* @return the motivazione of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getMotivazione() {
		return _datiClientiFornitori.getMotivazione();
	}

	/**
	* Sets the motivazione of this dati clienti fornitori.
	*
	* @param motivazione the motivazione of this dati clienti fornitori
	*/
	@Override
	public void setMotivazione(java.lang.String motivazione) {
		_datiClientiFornitori.setMotivazione(motivazione);
	}

	/**
	* Returns the livello blocco of this dati clienti fornitori.
	*
	* @return the livello blocco of this dati clienti fornitori
	*/
	@Override
	public int getLivelloBlocco() {
		return _datiClientiFornitori.getLivelloBlocco();
	}

	/**
	* Sets the livello blocco of this dati clienti fornitori.
	*
	* @param livelloBlocco the livello blocco of this dati clienti fornitori
	*/
	@Override
	public void setLivelloBlocco(int livelloBlocco) {
		_datiClientiFornitori.setLivelloBlocco(livelloBlocco);
	}

	/**
	* Returns the addebito c o n a i of this dati clienti fornitori.
	*
	* @return the addebito c o n a i of this dati clienti fornitori
	*/
	@Override
	public boolean getAddebitoCONAI() {
		return _datiClientiFornitori.getAddebitoCONAI();
	}

	/**
	* Returns <code>true</code> if this dati clienti fornitori is addebito c o n a i.
	*
	* @return <code>true</code> if this dati clienti fornitori is addebito c o n a i; <code>false</code> otherwise
	*/
	@Override
	public boolean isAddebitoCONAI() {
		return _datiClientiFornitori.isAddebitoCONAI();
	}

	/**
	* Sets whether this dati clienti fornitori is addebito c o n a i.
	*
	* @param addebitoCONAI the addebito c o n a i of this dati clienti fornitori
	*/
	@Override
	public void setAddebitoCONAI(boolean addebitoCONAI) {
		_datiClientiFornitori.setAddebitoCONAI(addebitoCONAI);
	}

	/**
	* Returns the lib str1 of this dati clienti fornitori.
	*
	* @return the lib str1 of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getLibStr1() {
		return _datiClientiFornitori.getLibStr1();
	}

	/**
	* Sets the lib str1 of this dati clienti fornitori.
	*
	* @param libStr1 the lib str1 of this dati clienti fornitori
	*/
	@Override
	public void setLibStr1(java.lang.String libStr1) {
		_datiClientiFornitori.setLibStr1(libStr1);
	}

	/**
	* Returns the lib str2 of this dati clienti fornitori.
	*
	* @return the lib str2 of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getLibStr2() {
		return _datiClientiFornitori.getLibStr2();
	}

	/**
	* Sets the lib str2 of this dati clienti fornitori.
	*
	* @param libStr2 the lib str2 of this dati clienti fornitori
	*/
	@Override
	public void setLibStr2(java.lang.String libStr2) {
		_datiClientiFornitori.setLibStr2(libStr2);
	}

	/**
	* Returns the lib str3 of this dati clienti fornitori.
	*
	* @return the lib str3 of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getLibStr3() {
		return _datiClientiFornitori.getLibStr3();
	}

	/**
	* Sets the lib str3 of this dati clienti fornitori.
	*
	* @param libStr3 the lib str3 of this dati clienti fornitori
	*/
	@Override
	public void setLibStr3(java.lang.String libStr3) {
		_datiClientiFornitori.setLibStr3(libStr3);
	}

	/**
	* Returns the lib str4 of this dati clienti fornitori.
	*
	* @return the lib str4 of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getLibStr4() {
		return _datiClientiFornitori.getLibStr4();
	}

	/**
	* Sets the lib str4 of this dati clienti fornitori.
	*
	* @param libStr4 the lib str4 of this dati clienti fornitori
	*/
	@Override
	public void setLibStr4(java.lang.String libStr4) {
		_datiClientiFornitori.setLibStr4(libStr4);
	}

	/**
	* Returns the lib str5 of this dati clienti fornitori.
	*
	* @return the lib str5 of this dati clienti fornitori
	*/
	@Override
	public java.lang.String getLibStr5() {
		return _datiClientiFornitori.getLibStr5();
	}

	/**
	* Sets the lib str5 of this dati clienti fornitori.
	*
	* @param libStr5 the lib str5 of this dati clienti fornitori
	*/
	@Override
	public void setLibStr5(java.lang.String libStr5) {
		_datiClientiFornitori.setLibStr5(libStr5);
	}

	/**
	* Returns the lib dat1 of this dati clienti fornitori.
	*
	* @return the lib dat1 of this dati clienti fornitori
	*/
	@Override
	public java.util.Date getLibDat1() {
		return _datiClientiFornitori.getLibDat1();
	}

	/**
	* Sets the lib dat1 of this dati clienti fornitori.
	*
	* @param libDat1 the lib dat1 of this dati clienti fornitori
	*/
	@Override
	public void setLibDat1(java.util.Date libDat1) {
		_datiClientiFornitori.setLibDat1(libDat1);
	}

	/**
	* Returns the lib dat2 of this dati clienti fornitori.
	*
	* @return the lib dat2 of this dati clienti fornitori
	*/
	@Override
	public java.util.Date getLibDat2() {
		return _datiClientiFornitori.getLibDat2();
	}

	/**
	* Sets the lib dat2 of this dati clienti fornitori.
	*
	* @param libDat2 the lib dat2 of this dati clienti fornitori
	*/
	@Override
	public void setLibDat2(java.util.Date libDat2) {
		_datiClientiFornitori.setLibDat2(libDat2);
	}

	/**
	* Returns the lib dat3 of this dati clienti fornitori.
	*
	* @return the lib dat3 of this dati clienti fornitori
	*/
	@Override
	public java.util.Date getLibDat3() {
		return _datiClientiFornitori.getLibDat3();
	}

	/**
	* Sets the lib dat3 of this dati clienti fornitori.
	*
	* @param libDat3 the lib dat3 of this dati clienti fornitori
	*/
	@Override
	public void setLibDat3(java.util.Date libDat3) {
		_datiClientiFornitori.setLibDat3(libDat3);
	}

	/**
	* Returns the lib dat4 of this dati clienti fornitori.
	*
	* @return the lib dat4 of this dati clienti fornitori
	*/
	@Override
	public java.util.Date getLibDat4() {
		return _datiClientiFornitori.getLibDat4();
	}

	/**
	* Sets the lib dat4 of this dati clienti fornitori.
	*
	* @param libDat4 the lib dat4 of this dati clienti fornitori
	*/
	@Override
	public void setLibDat4(java.util.Date libDat4) {
		_datiClientiFornitori.setLibDat4(libDat4);
	}

	/**
	* Returns the lib dat5 of this dati clienti fornitori.
	*
	* @return the lib dat5 of this dati clienti fornitori
	*/
	@Override
	public java.util.Date getLibDat5() {
		return _datiClientiFornitori.getLibDat5();
	}

	/**
	* Sets the lib dat5 of this dati clienti fornitori.
	*
	* @param libDat5 the lib dat5 of this dati clienti fornitori
	*/
	@Override
	public void setLibDat5(java.util.Date libDat5) {
		_datiClientiFornitori.setLibDat5(libDat5);
	}

	/**
	* Returns the lib lng1 of this dati clienti fornitori.
	*
	* @return the lib lng1 of this dati clienti fornitori
	*/
	@Override
	public long getLibLng1() {
		return _datiClientiFornitori.getLibLng1();
	}

	/**
	* Sets the lib lng1 of this dati clienti fornitori.
	*
	* @param libLng1 the lib lng1 of this dati clienti fornitori
	*/
	@Override
	public void setLibLng1(long libLng1) {
		_datiClientiFornitori.setLibLng1(libLng1);
	}

	/**
	* Returns the lib lng2 of this dati clienti fornitori.
	*
	* @return the lib lng2 of this dati clienti fornitori
	*/
	@Override
	public long getLibLng2() {
		return _datiClientiFornitori.getLibLng2();
	}

	/**
	* Sets the lib lng2 of this dati clienti fornitori.
	*
	* @param libLng2 the lib lng2 of this dati clienti fornitori
	*/
	@Override
	public void setLibLng2(long libLng2) {
		_datiClientiFornitori.setLibLng2(libLng2);
	}

	/**
	* Returns the lib lng3 of this dati clienti fornitori.
	*
	* @return the lib lng3 of this dati clienti fornitori
	*/
	@Override
	public long getLibLng3() {
		return _datiClientiFornitori.getLibLng3();
	}

	/**
	* Sets the lib lng3 of this dati clienti fornitori.
	*
	* @param libLng3 the lib lng3 of this dati clienti fornitori
	*/
	@Override
	public void setLibLng3(long libLng3) {
		_datiClientiFornitori.setLibLng3(libLng3);
	}

	/**
	* Returns the lib lng4 of this dati clienti fornitori.
	*
	* @return the lib lng4 of this dati clienti fornitori
	*/
	@Override
	public long getLibLng4() {
		return _datiClientiFornitori.getLibLng4();
	}

	/**
	* Sets the lib lng4 of this dati clienti fornitori.
	*
	* @param libLng4 the lib lng4 of this dati clienti fornitori
	*/
	@Override
	public void setLibLng4(long libLng4) {
		_datiClientiFornitori.setLibLng4(libLng4);
	}

	/**
	* Returns the lib lng5 of this dati clienti fornitori.
	*
	* @return the lib lng5 of this dati clienti fornitori
	*/
	@Override
	public long getLibLng5() {
		return _datiClientiFornitori.getLibLng5();
	}

	/**
	* Sets the lib lng5 of this dati clienti fornitori.
	*
	* @param libLng5 the lib lng5 of this dati clienti fornitori
	*/
	@Override
	public void setLibLng5(long libLng5) {
		_datiClientiFornitori.setLibLng5(libLng5);
	}

	/**
	* Returns the lib dbl1 of this dati clienti fornitori.
	*
	* @return the lib dbl1 of this dati clienti fornitori
	*/
	@Override
	public double getLibDbl1() {
		return _datiClientiFornitori.getLibDbl1();
	}

	/**
	* Sets the lib dbl1 of this dati clienti fornitori.
	*
	* @param libDbl1 the lib dbl1 of this dati clienti fornitori
	*/
	@Override
	public void setLibDbl1(double libDbl1) {
		_datiClientiFornitori.setLibDbl1(libDbl1);
	}

	/**
	* Returns the lib dbl2 of this dati clienti fornitori.
	*
	* @return the lib dbl2 of this dati clienti fornitori
	*/
	@Override
	public double getLibDbl2() {
		return _datiClientiFornitori.getLibDbl2();
	}

	/**
	* Sets the lib dbl2 of this dati clienti fornitori.
	*
	* @param libDbl2 the lib dbl2 of this dati clienti fornitori
	*/
	@Override
	public void setLibDbl2(double libDbl2) {
		_datiClientiFornitori.setLibDbl2(libDbl2);
	}

	/**
	* Returns the lib dbl3 of this dati clienti fornitori.
	*
	* @return the lib dbl3 of this dati clienti fornitori
	*/
	@Override
	public double getLibDbl3() {
		return _datiClientiFornitori.getLibDbl3();
	}

	/**
	* Sets the lib dbl3 of this dati clienti fornitori.
	*
	* @param libDbl3 the lib dbl3 of this dati clienti fornitori
	*/
	@Override
	public void setLibDbl3(double libDbl3) {
		_datiClientiFornitori.setLibDbl3(libDbl3);
	}

	/**
	* Returns the lib dbl4 of this dati clienti fornitori.
	*
	* @return the lib dbl4 of this dati clienti fornitori
	*/
	@Override
	public double getLibDbl4() {
		return _datiClientiFornitori.getLibDbl4();
	}

	/**
	* Sets the lib dbl4 of this dati clienti fornitori.
	*
	* @param libDbl4 the lib dbl4 of this dati clienti fornitori
	*/
	@Override
	public void setLibDbl4(double libDbl4) {
		_datiClientiFornitori.setLibDbl4(libDbl4);
	}

	/**
	* Returns the lib dbl5 of this dati clienti fornitori.
	*
	* @return the lib dbl5 of this dati clienti fornitori
	*/
	@Override
	public double getLibDbl5() {
		return _datiClientiFornitori.getLibDbl5();
	}

	/**
	* Sets the lib dbl5 of this dati clienti fornitori.
	*
	* @param libDbl5 the lib dbl5 of this dati clienti fornitori
	*/
	@Override
	public void setLibDbl5(double libDbl5) {
		_datiClientiFornitori.setLibDbl5(libDbl5);
	}

	@Override
	public boolean isNew() {
		return _datiClientiFornitori.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_datiClientiFornitori.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _datiClientiFornitori.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_datiClientiFornitori.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _datiClientiFornitori.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _datiClientiFornitori.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_datiClientiFornitori.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _datiClientiFornitori.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_datiClientiFornitori.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_datiClientiFornitori.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_datiClientiFornitori.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new DatiClientiFornitoriWrapper((DatiClientiFornitori)_datiClientiFornitori.clone());
	}

	@Override
	public int compareTo(
		it.bysoftware.ct.model.DatiClientiFornitori datiClientiFornitori) {
		return _datiClientiFornitori.compareTo(datiClientiFornitori);
	}

	@Override
	public int hashCode() {
		return _datiClientiFornitori.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.DatiClientiFornitori> toCacheModel() {
		return _datiClientiFornitori.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.DatiClientiFornitori toEscapedModel() {
		return new DatiClientiFornitoriWrapper(_datiClientiFornitori.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.DatiClientiFornitori toUnescapedModel() {
		return new DatiClientiFornitoriWrapper(_datiClientiFornitori.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _datiClientiFornitori.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _datiClientiFornitori.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_datiClientiFornitori.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DatiClientiFornitoriWrapper)) {
			return false;
		}

		DatiClientiFornitoriWrapper datiClientiFornitoriWrapper = (DatiClientiFornitoriWrapper)obj;

		if (Validator.equals(_datiClientiFornitori,
					datiClientiFornitoriWrapper._datiClientiFornitori)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public DatiClientiFornitori getWrappedDatiClientiFornitori() {
		return _datiClientiFornitori;
	}

	@Override
	public DatiClientiFornitori getWrappedModel() {
		return _datiClientiFornitori;
	}

	@Override
	public void resetOriginalValues() {
		_datiClientiFornitori.resetOriginalValues();
	}

	private DatiClientiFornitori _datiClientiFornitori;
}