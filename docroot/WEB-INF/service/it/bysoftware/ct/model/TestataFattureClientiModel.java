/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import it.bysoftware.ct.service.persistence.TestataFattureClientiPK;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the TestataFattureClienti service. Represents a row in the &quot;TESFAT&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link it.bysoftware.ct.model.impl.TestataFattureClientiModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link it.bysoftware.ct.model.impl.TestataFattureClientiImpl}.
 * </p>
 *
 * @author Mario Torrisi
 * @see TestataFattureClienti
 * @see it.bysoftware.ct.model.impl.TestataFattureClientiImpl
 * @see it.bysoftware.ct.model.impl.TestataFattureClientiModelImpl
 * @generated
 */
public interface TestataFattureClientiModel extends BaseModel<TestataFattureClienti> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a testata fatture clienti model instance should use the {@link TestataFattureClienti} interface instead.
	 */

	/**
	 * Returns the primary key of this testata fatture clienti.
	 *
	 * @return the primary key of this testata fatture clienti
	 */
	public TestataFattureClientiPK getPrimaryKey();

	/**
	 * Sets the primary key of this testata fatture clienti.
	 *
	 * @param primaryKey the primary key of this testata fatture clienti
	 */
	public void setPrimaryKey(TestataFattureClientiPK primaryKey);

	/**
	 * Returns the anno of this testata fatture clienti.
	 *
	 * @return the anno of this testata fatture clienti
	 */
	public int getAnno();

	/**
	 * Sets the anno of this testata fatture clienti.
	 *
	 * @param anno the anno of this testata fatture clienti
	 */
	public void setAnno(int anno);

	/**
	 * Returns the codice attivita of this testata fatture clienti.
	 *
	 * @return the codice attivita of this testata fatture clienti
	 */
	@AutoEscape
	public String getCodiceAttivita();

	/**
	 * Sets the codice attivita of this testata fatture clienti.
	 *
	 * @param codiceAttivita the codice attivita of this testata fatture clienti
	 */
	public void setCodiceAttivita(String codiceAttivita);

	/**
	 * Returns the codice centro of this testata fatture clienti.
	 *
	 * @return the codice centro of this testata fatture clienti
	 */
	@AutoEscape
	public String getCodiceCentro();

	/**
	 * Sets the codice centro of this testata fatture clienti.
	 *
	 * @param codiceCentro the codice centro of this testata fatture clienti
	 */
	public void setCodiceCentro(String codiceCentro);

	/**
	 * Returns the numero protocollo of this testata fatture clienti.
	 *
	 * @return the numero protocollo of this testata fatture clienti
	 */
	public int getNumeroProtocollo();

	/**
	 * Sets the numero protocollo of this testata fatture clienti.
	 *
	 * @param numeroProtocollo the numero protocollo of this testata fatture clienti
	 */
	public void setNumeroProtocollo(int numeroProtocollo);

	/**
	 * Returns the tipo documento of this testata fatture clienti.
	 *
	 * @return the tipo documento of this testata fatture clienti
	 */
	@AutoEscape
	public String getTipoDocumento();

	/**
	 * Sets the tipo documento of this testata fatture clienti.
	 *
	 * @param tipoDocumento the tipo documento of this testata fatture clienti
	 */
	public void setTipoDocumento(String tipoDocumento);

	/**
	 * Returns the codice centro cont analitica of this testata fatture clienti.
	 *
	 * @return the codice centro cont analitica of this testata fatture clienti
	 */
	@AutoEscape
	public String getCodiceCentroContAnalitica();

	/**
	 * Sets the codice centro cont analitica of this testata fatture clienti.
	 *
	 * @param codiceCentroContAnalitica the codice centro cont analitica of this testata fatture clienti
	 */
	public void setCodiceCentroContAnalitica(String codiceCentroContAnalitica);

	/**
	 * Returns the id tipo documento origine of this testata fatture clienti.
	 *
	 * @return the id tipo documento origine of this testata fatture clienti
	 */
	public int getIdTipoDocumentoOrigine();

	/**
	 * Sets the id tipo documento origine of this testata fatture clienti.
	 *
	 * @param idTipoDocumentoOrigine the id tipo documento origine of this testata fatture clienti
	 */
	public void setIdTipoDocumentoOrigine(int idTipoDocumentoOrigine);

	/**
	 * Returns the stato fattura of this testata fatture clienti.
	 *
	 * @return the stato fattura of this testata fatture clienti
	 */
	public boolean getStatoFattura();

	/**
	 * Returns <code>true</code> if this testata fatture clienti is stato fattura.
	 *
	 * @return <code>true</code> if this testata fatture clienti is stato fattura; <code>false</code> otherwise
	 */
	public boolean isStatoFattura();

	/**
	 * Sets whether this testata fatture clienti is stato fattura.
	 *
	 * @param statoFattura the stato fattura of this testata fatture clienti
	 */
	public void setStatoFattura(boolean statoFattura);

	/**
	 * Returns the data registrazione of this testata fatture clienti.
	 *
	 * @return the data registrazione of this testata fatture clienti
	 */
	public Date getDataRegistrazione();

	/**
	 * Sets the data registrazione of this testata fatture clienti.
	 *
	 * @param dataRegistrazione the data registrazione of this testata fatture clienti
	 */
	public void setDataRegistrazione(Date dataRegistrazione);

	/**
	 * Returns the data operazione of this testata fatture clienti.
	 *
	 * @return the data operazione of this testata fatture clienti
	 */
	public Date getDataOperazione();

	/**
	 * Sets the data operazione of this testata fatture clienti.
	 *
	 * @param dataOperazione the data operazione of this testata fatture clienti
	 */
	public void setDataOperazione(Date dataOperazione);

	/**
	 * Returns the data annotazione of this testata fatture clienti.
	 *
	 * @return the data annotazione of this testata fatture clienti
	 */
	public Date getDataAnnotazione();

	/**
	 * Sets the data annotazione of this testata fatture clienti.
	 *
	 * @param dataAnnotazione the data annotazione of this testata fatture clienti
	 */
	public void setDataAnnotazione(Date dataAnnotazione);

	/**
	 * Returns the data documento of this testata fatture clienti.
	 *
	 * @return the data documento of this testata fatture clienti
	 */
	public Date getDataDocumento();

	/**
	 * Sets the data documento of this testata fatture clienti.
	 *
	 * @param dataDocumento the data documento of this testata fatture clienti
	 */
	public void setDataDocumento(Date dataDocumento);

	/**
	 * Returns the numero documento of this testata fatture clienti.
	 *
	 * @return the numero documento of this testata fatture clienti
	 */
	public int getNumeroDocumento();

	/**
	 * Sets the numero documento of this testata fatture clienti.
	 *
	 * @param numeroDocumento the numero documento of this testata fatture clienti
	 */
	public void setNumeroDocumento(int numeroDocumento);

	/**
	 * Returns the riferimento a scontrino of this testata fatture clienti.
	 *
	 * @return the riferimento a scontrino of this testata fatture clienti
	 */
	public int getRiferimentoAScontrino();

	/**
	 * Sets the riferimento a scontrino of this testata fatture clienti.
	 *
	 * @param riferimentoAScontrino the riferimento a scontrino of this testata fatture clienti
	 */
	public void setRiferimentoAScontrino(int riferimentoAScontrino);

	/**
	 * Returns the descrizione estremi documento of this testata fatture clienti.
	 *
	 * @return the descrizione estremi documento of this testata fatture clienti
	 */
	@AutoEscape
	public String getDescrizioneEstremiDocumento();

	/**
	 * Sets the descrizione estremi documento of this testata fatture clienti.
	 *
	 * @param descrizioneEstremiDocumento the descrizione estremi documento of this testata fatture clienti
	 */
	public void setDescrizioneEstremiDocumento(
		String descrizioneEstremiDocumento);

	/**
	 * Returns the tipo soggetto of this testata fatture clienti.
	 *
	 * @return the tipo soggetto of this testata fatture clienti
	 */
	public boolean getTipoSoggetto();

	/**
	 * Returns <code>true</code> if this testata fatture clienti is tipo soggetto.
	 *
	 * @return <code>true</code> if this testata fatture clienti is tipo soggetto; <code>false</code> otherwise
	 */
	public boolean isTipoSoggetto();

	/**
	 * Sets whether this testata fatture clienti is tipo soggetto.
	 *
	 * @param tipoSoggetto the tipo soggetto of this testata fatture clienti
	 */
	public void setTipoSoggetto(boolean tipoSoggetto);

	/**
	 * Returns the codice cliente of this testata fatture clienti.
	 *
	 * @return the codice cliente of this testata fatture clienti
	 */
	@AutoEscape
	public String getCodiceCliente();

	/**
	 * Sets the codice cliente of this testata fatture clienti.
	 *
	 * @param codiceCliente the codice cliente of this testata fatture clienti
	 */
	public void setCodiceCliente(String codiceCliente);

	/**
	 * Returns the descrizione aggiuntiva fattura of this testata fatture clienti.
	 *
	 * @return the descrizione aggiuntiva fattura of this testata fatture clienti
	 */
	@AutoEscape
	public String getDescrizioneAggiuntivaFattura();

	/**
	 * Sets the descrizione aggiuntiva fattura of this testata fatture clienti.
	 *
	 * @param descrizioneAggiuntivaFattura the descrizione aggiuntiva fattura of this testata fatture clienti
	 */
	public void setDescrizioneAggiuntivaFattura(
		String descrizioneAggiuntivaFattura);

	/**
	 * Returns the estremi ordine of this testata fatture clienti.
	 *
	 * @return the estremi ordine of this testata fatture clienti
	 */
	@AutoEscape
	public String getEstremiOrdine();

	/**
	 * Sets the estremi ordine of this testata fatture clienti.
	 *
	 * @param estremiOrdine the estremi ordine of this testata fatture clienti
	 */
	public void setEstremiOrdine(String estremiOrdine);

	/**
	 * Returns the estremi bolla of this testata fatture clienti.
	 *
	 * @return the estremi bolla of this testata fatture clienti
	 */
	@AutoEscape
	public String getEstremiBolla();

	/**
	 * Sets the estremi bolla of this testata fatture clienti.
	 *
	 * @param estremiBolla the estremi bolla of this testata fatture clienti
	 */
	public void setEstremiBolla(String estremiBolla);

	/**
	 * Returns the codice pagamento of this testata fatture clienti.
	 *
	 * @return the codice pagamento of this testata fatture clienti
	 */
	@AutoEscape
	public String getCodicePagamento();

	/**
	 * Sets the codice pagamento of this testata fatture clienti.
	 *
	 * @param codicePagamento the codice pagamento of this testata fatture clienti
	 */
	public void setCodicePagamento(String codicePagamento);

	/**
	 * Returns the codice agente of this testata fatture clienti.
	 *
	 * @return the codice agente of this testata fatture clienti
	 */
	@AutoEscape
	public String getCodiceAgente();

	/**
	 * Sets the codice agente of this testata fatture clienti.
	 *
	 * @param codiceAgente the codice agente of this testata fatture clienti
	 */
	public void setCodiceAgente(String codiceAgente);

	/**
	 * Returns the codice gruppo agenti of this testata fatture clienti.
	 *
	 * @return the codice gruppo agenti of this testata fatture clienti
	 */
	@AutoEscape
	public String getCodiceGruppoAgenti();

	/**
	 * Sets the codice gruppo agenti of this testata fatture clienti.
	 *
	 * @param codiceGruppoAgenti the codice gruppo agenti of this testata fatture clienti
	 */
	public void setCodiceGruppoAgenti(String codiceGruppoAgenti);

	/**
	 * Returns the annotazioni of this testata fatture clienti.
	 *
	 * @return the annotazioni of this testata fatture clienti
	 */
	@AutoEscape
	public String getAnnotazioni();

	/**
	 * Sets the annotazioni of this testata fatture clienti.
	 *
	 * @param annotazioni the annotazioni of this testata fatture clienti
	 */
	public void setAnnotazioni(String annotazioni);

	/**
	 * Returns the sconto chiusura of this testata fatture clienti.
	 *
	 * @return the sconto chiusura of this testata fatture clienti
	 */
	public double getScontoChiusura();

	/**
	 * Sets the sconto chiusura of this testata fatture clienti.
	 *
	 * @param scontoChiusura the sconto chiusura of this testata fatture clienti
	 */
	public void setScontoChiusura(double scontoChiusura);

	/**
	 * Returns the sconto pronta cassa of this testata fatture clienti.
	 *
	 * @return the sconto pronta cassa of this testata fatture clienti
	 */
	public double getScontoProntaCassa();

	/**
	 * Sets the sconto pronta cassa of this testata fatture clienti.
	 *
	 * @param scontoProntaCassa the sconto pronta cassa of this testata fatture clienti
	 */
	public void setScontoProntaCassa(double scontoProntaCassa);

	/**
	 * Returns the pertuale spese trasp of this testata fatture clienti.
	 *
	 * @return the pertuale spese trasp of this testata fatture clienti
	 */
	public double getPertualeSpeseTrasp();

	/**
	 * Sets the pertuale spese trasp of this testata fatture clienti.
	 *
	 * @param pertualeSpeseTrasp the pertuale spese trasp of this testata fatture clienti
	 */
	public void setPertualeSpeseTrasp(double pertualeSpeseTrasp);

	/**
	 * Returns the importo spese trasp of this testata fatture clienti.
	 *
	 * @return the importo spese trasp of this testata fatture clienti
	 */
	public double getImportoSpeseTrasp();

	/**
	 * Sets the importo spese trasp of this testata fatture clienti.
	 *
	 * @param importoSpeseTrasp the importo spese trasp of this testata fatture clienti
	 */
	public void setImportoSpeseTrasp(double importoSpeseTrasp);

	/**
	 * Returns the importo spese imb of this testata fatture clienti.
	 *
	 * @return the importo spese imb of this testata fatture clienti
	 */
	public double getImportoSpeseImb();

	/**
	 * Sets the importo spese imb of this testata fatture clienti.
	 *
	 * @param importoSpeseImb the importo spese imb of this testata fatture clienti
	 */
	public void setImportoSpeseImb(double importoSpeseImb);

	/**
	 * Returns the importo spese varie of this testata fatture clienti.
	 *
	 * @return the importo spese varie of this testata fatture clienti
	 */
	public double getImportoSpeseVarie();

	/**
	 * Sets the importo spese varie of this testata fatture clienti.
	 *
	 * @param importoSpeseVarie the importo spese varie of this testata fatture clienti
	 */
	public void setImportoSpeseVarie(double importoSpeseVarie);

	/**
	 * Returns the importo spese bancarie of this testata fatture clienti.
	 *
	 * @return the importo spese bancarie of this testata fatture clienti
	 */
	public double getImportoSpeseBancarie();

	/**
	 * Sets the importo spese bancarie of this testata fatture clienti.
	 *
	 * @param importoSpeseBancarie the importo spese bancarie of this testata fatture clienti
	 */
	public void setImportoSpeseBancarie(double importoSpeseBancarie);

	/**
	 * Returns the codice i v a trasp of this testata fatture clienti.
	 *
	 * @return the codice i v a trasp of this testata fatture clienti
	 */
	@AutoEscape
	public String getCodiceIVATrasp();

	/**
	 * Sets the codice i v a trasp of this testata fatture clienti.
	 *
	 * @param codiceIVATrasp the codice i v a trasp of this testata fatture clienti
	 */
	public void setCodiceIVATrasp(String codiceIVATrasp);

	/**
	 * Returns the codice i v a imb of this testata fatture clienti.
	 *
	 * @return the codice i v a imb of this testata fatture clienti
	 */
	@AutoEscape
	public String getCodiceIVAImb();

	/**
	 * Sets the codice i v a imb of this testata fatture clienti.
	 *
	 * @param codiceIVAImb the codice i v a imb of this testata fatture clienti
	 */
	public void setCodiceIVAImb(String codiceIVAImb);

	/**
	 * Returns the codice i v a varie of this testata fatture clienti.
	 *
	 * @return the codice i v a varie of this testata fatture clienti
	 */
	@AutoEscape
	public String getCodiceIVAVarie();

	/**
	 * Sets the codice i v a varie of this testata fatture clienti.
	 *
	 * @param codiceIVAVarie the codice i v a varie of this testata fatture clienti
	 */
	public void setCodiceIVAVarie(String codiceIVAVarie);

	/**
	 * Returns the codice i v a bancarie of this testata fatture clienti.
	 *
	 * @return the codice i v a bancarie of this testata fatture clienti
	 */
	@AutoEscape
	public String getCodiceIVABancarie();

	/**
	 * Sets the codice i v a bancarie of this testata fatture clienti.
	 *
	 * @param codiceIVABancarie the codice i v a bancarie of this testata fatture clienti
	 */
	public void setCodiceIVABancarie(String codiceIVABancarie);

	/**
	 * Returns the totale sconti corpo of this testata fatture clienti.
	 *
	 * @return the totale sconti corpo of this testata fatture clienti
	 */
	public double getTotaleScontiCorpo();

	/**
	 * Sets the totale sconti corpo of this testata fatture clienti.
	 *
	 * @param totaleScontiCorpo the totale sconti corpo of this testata fatture clienti
	 */
	public void setTotaleScontiCorpo(double totaleScontiCorpo);

	/**
	 * Returns the imponibile of this testata fatture clienti.
	 *
	 * @return the imponibile of this testata fatture clienti
	 */
	public double getImponibile();

	/**
	 * Sets the imponibile of this testata fatture clienti.
	 *
	 * @param imponibile the imponibile of this testata fatture clienti
	 */
	public void setImponibile(double imponibile);

	/**
	 * Returns the i v a of this testata fatture clienti.
	 *
	 * @return the i v a of this testata fatture clienti
	 */
	public double getIVA();

	/**
	 * Sets the i v a of this testata fatture clienti.
	 *
	 * @param IVA the i v a of this testata fatture clienti
	 */
	public void setIVA(double IVA);

	/**
	 * Returns the importo fattura of this testata fatture clienti.
	 *
	 * @return the importo fattura of this testata fatture clienti
	 */
	public double getImportoFattura();

	/**
	 * Sets the importo fattura of this testata fatture clienti.
	 *
	 * @param importoFattura the importo fattura of this testata fatture clienti
	 */
	public void setImportoFattura(double importoFattura);

	/**
	 * Returns the lib str1 of this testata fatture clienti.
	 *
	 * @return the lib str1 of this testata fatture clienti
	 */
	@AutoEscape
	public String getLibStr1();

	/**
	 * Sets the lib str1 of this testata fatture clienti.
	 *
	 * @param libStr1 the lib str1 of this testata fatture clienti
	 */
	public void setLibStr1(String libStr1);

	/**
	 * Returns the lib str2 of this testata fatture clienti.
	 *
	 * @return the lib str2 of this testata fatture clienti
	 */
	@AutoEscape
	public String getLibStr2();

	/**
	 * Sets the lib str2 of this testata fatture clienti.
	 *
	 * @param libStr2 the lib str2 of this testata fatture clienti
	 */
	public void setLibStr2(String libStr2);

	/**
	 * Returns the lib str3 of this testata fatture clienti.
	 *
	 * @return the lib str3 of this testata fatture clienti
	 */
	@AutoEscape
	public String getLibStr3();

	/**
	 * Sets the lib str3 of this testata fatture clienti.
	 *
	 * @param libStr3 the lib str3 of this testata fatture clienti
	 */
	public void setLibStr3(String libStr3);

	/**
	 * Returns the lib dbl1 of this testata fatture clienti.
	 *
	 * @return the lib dbl1 of this testata fatture clienti
	 */
	public double getLibDbl1();

	/**
	 * Sets the lib dbl1 of this testata fatture clienti.
	 *
	 * @param libDbl1 the lib dbl1 of this testata fatture clienti
	 */
	public void setLibDbl1(double libDbl1);

	/**
	 * Returns the lib dbl2 of this testata fatture clienti.
	 *
	 * @return the lib dbl2 of this testata fatture clienti
	 */
	public double getLibDbl2();

	/**
	 * Sets the lib dbl2 of this testata fatture clienti.
	 *
	 * @param libDbl2 the lib dbl2 of this testata fatture clienti
	 */
	public void setLibDbl2(double libDbl2);

	/**
	 * Returns the lib dbl3 of this testata fatture clienti.
	 *
	 * @return the lib dbl3 of this testata fatture clienti
	 */
	public double getLibDbl3();

	/**
	 * Sets the lib dbl3 of this testata fatture clienti.
	 *
	 * @param libDbl3 the lib dbl3 of this testata fatture clienti
	 */
	public void setLibDbl3(double libDbl3);

	/**
	 * Returns the lib lng1 of this testata fatture clienti.
	 *
	 * @return the lib lng1 of this testata fatture clienti
	 */
	public long getLibLng1();

	/**
	 * Sets the lib lng1 of this testata fatture clienti.
	 *
	 * @param libLng1 the lib lng1 of this testata fatture clienti
	 */
	public void setLibLng1(long libLng1);

	/**
	 * Returns the lib lng2 of this testata fatture clienti.
	 *
	 * @return the lib lng2 of this testata fatture clienti
	 */
	public long getLibLng2();

	/**
	 * Sets the lib lng2 of this testata fatture clienti.
	 *
	 * @param libLng2 the lib lng2 of this testata fatture clienti
	 */
	public void setLibLng2(long libLng2);

	/**
	 * Returns the lib lng3 of this testata fatture clienti.
	 *
	 * @return the lib lng3 of this testata fatture clienti
	 */
	public long getLibLng3();

	/**
	 * Sets the lib lng3 of this testata fatture clienti.
	 *
	 * @param libLng3 the lib lng3 of this testata fatture clienti
	 */
	public void setLibLng3(long libLng3);

	/**
	 * Returns the lib dat1 of this testata fatture clienti.
	 *
	 * @return the lib dat1 of this testata fatture clienti
	 */
	public Date getLibDat1();

	/**
	 * Sets the lib dat1 of this testata fatture clienti.
	 *
	 * @param libDat1 the lib dat1 of this testata fatture clienti
	 */
	public void setLibDat1(Date libDat1);

	/**
	 * Returns the lib dat2 of this testata fatture clienti.
	 *
	 * @return the lib dat2 of this testata fatture clienti
	 */
	public Date getLibDat2();

	/**
	 * Sets the lib dat2 of this testata fatture clienti.
	 *
	 * @param libDat2 the lib dat2 of this testata fatture clienti
	 */
	public void setLibDat2(Date libDat2);

	/**
	 * Returns the lib dat3 of this testata fatture clienti.
	 *
	 * @return the lib dat3 of this testata fatture clienti
	 */
	public Date getLibDat3();

	/**
	 * Sets the lib dat3 of this testata fatture clienti.
	 *
	 * @param libDat3 the lib dat3 of this testata fatture clienti
	 */
	public void setLibDat3(Date libDat3);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(
		it.bysoftware.ct.model.TestataFattureClienti testataFattureClienti);

	@Override
	public int hashCode();

	@Override
	public CacheModel<it.bysoftware.ct.model.TestataFattureClienti> toCacheModel();

	@Override
	public it.bysoftware.ct.model.TestataFattureClienti toEscapedModel();

	@Override
	public it.bysoftware.ct.model.TestataFattureClienti toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}