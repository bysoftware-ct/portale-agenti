/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.VociIvaLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class VociIvaClp extends BaseModelImpl<VociIva> implements VociIva {
	public VociIvaClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return VociIva.class;
	}

	@Override
	public String getModelClassName() {
		return VociIva.class.getName();
	}

	@Override
	public String getPrimaryKey() {
		return _codiceIva;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setCodiceIva(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _codiceIva;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codiceIva", getCodiceIva());
		attributes.put("descrizione", getDescrizione());
		attributes.put("aliquota", getAliquota());
		attributes.put("aliquotaVandita", getAliquotaVandita());
		attributes.put("aliquotaScorporo", getAliquotaScorporo());
		attributes.put("aliquotaUlterioreDetr", getAliquotaUlterioreDetr());
		attributes.put("tipoVoceIva", getTipoVoceIva());
		attributes.put("gestioneOmaggio", getGestioneOmaggio());
		attributes.put("codIvaSpeseOmaggio", getCodIvaSpeseOmaggio());
		attributes.put("descrizioneDocuemto", getDescrizioneDocuemto());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codiceIva = (String)attributes.get("codiceIva");

		if (codiceIva != null) {
			setCodiceIva(codiceIva);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		Double aliquota = (Double)attributes.get("aliquota");

		if (aliquota != null) {
			setAliquota(aliquota);
		}

		Double aliquotaVandita = (Double)attributes.get("aliquotaVandita");

		if (aliquotaVandita != null) {
			setAliquotaVandita(aliquotaVandita);
		}

		Double aliquotaScorporo = (Double)attributes.get("aliquotaScorporo");

		if (aliquotaScorporo != null) {
			setAliquotaScorporo(aliquotaScorporo);
		}

		Double aliquotaUlterioreDetr = (Double)attributes.get(
				"aliquotaUlterioreDetr");

		if (aliquotaUlterioreDetr != null) {
			setAliquotaUlterioreDetr(aliquotaUlterioreDetr);
		}

		Integer tipoVoceIva = (Integer)attributes.get("tipoVoceIva");

		if (tipoVoceIva != null) {
			setTipoVoceIva(tipoVoceIva);
		}

		Integer gestioneOmaggio = (Integer)attributes.get("gestioneOmaggio");

		if (gestioneOmaggio != null) {
			setGestioneOmaggio(gestioneOmaggio);
		}

		String codIvaSpeseOmaggio = (String)attributes.get("codIvaSpeseOmaggio");

		if (codIvaSpeseOmaggio != null) {
			setCodIvaSpeseOmaggio(codIvaSpeseOmaggio);
		}

		String descrizioneDocuemto = (String)attributes.get(
				"descrizioneDocuemto");

		if (descrizioneDocuemto != null) {
			setDescrizioneDocuemto(descrizioneDocuemto);
		}
	}

	@Override
	public String getCodiceIva() {
		return _codiceIva;
	}

	@Override
	public void setCodiceIva(String codiceIva) {
		_codiceIva = codiceIva;

		if (_vociIvaRemoteModel != null) {
			try {
				Class<?> clazz = _vociIvaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIva", String.class);

				method.invoke(_vociIvaRemoteModel, codiceIva);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizione() {
		return _descrizione;
	}

	@Override
	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;

		if (_vociIvaRemoteModel != null) {
			try {
				Class<?> clazz = _vociIvaRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizione", String.class);

				method.invoke(_vociIvaRemoteModel, descrizione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getAliquota() {
		return _aliquota;
	}

	@Override
	public void setAliquota(double aliquota) {
		_aliquota = aliquota;

		if (_vociIvaRemoteModel != null) {
			try {
				Class<?> clazz = _vociIvaRemoteModel.getClass();

				Method method = clazz.getMethod("setAliquota", double.class);

				method.invoke(_vociIvaRemoteModel, aliquota);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getAliquotaVandita() {
		return _aliquotaVandita;
	}

	@Override
	public void setAliquotaVandita(double aliquotaVandita) {
		_aliquotaVandita = aliquotaVandita;

		if (_vociIvaRemoteModel != null) {
			try {
				Class<?> clazz = _vociIvaRemoteModel.getClass();

				Method method = clazz.getMethod("setAliquotaVandita",
						double.class);

				method.invoke(_vociIvaRemoteModel, aliquotaVandita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getAliquotaScorporo() {
		return _aliquotaScorporo;
	}

	@Override
	public void setAliquotaScorporo(double aliquotaScorporo) {
		_aliquotaScorporo = aliquotaScorporo;

		if (_vociIvaRemoteModel != null) {
			try {
				Class<?> clazz = _vociIvaRemoteModel.getClass();

				Method method = clazz.getMethod("setAliquotaScorporo",
						double.class);

				method.invoke(_vociIvaRemoteModel, aliquotaScorporo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getAliquotaUlterioreDetr() {
		return _aliquotaUlterioreDetr;
	}

	@Override
	public void setAliquotaUlterioreDetr(double aliquotaUlterioreDetr) {
		_aliquotaUlterioreDetr = aliquotaUlterioreDetr;

		if (_vociIvaRemoteModel != null) {
			try {
				Class<?> clazz = _vociIvaRemoteModel.getClass();

				Method method = clazz.getMethod("setAliquotaUlterioreDetr",
						double.class);

				method.invoke(_vociIvaRemoteModel, aliquotaUlterioreDetr);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoVoceIva() {
		return _tipoVoceIva;
	}

	@Override
	public void setTipoVoceIva(int tipoVoceIva) {
		_tipoVoceIva = tipoVoceIva;

		if (_vociIvaRemoteModel != null) {
			try {
				Class<?> clazz = _vociIvaRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoVoceIva", int.class);

				method.invoke(_vociIvaRemoteModel, tipoVoceIva);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getGestioneOmaggio() {
		return _gestioneOmaggio;
	}

	@Override
	public void setGestioneOmaggio(int gestioneOmaggio) {
		_gestioneOmaggio = gestioneOmaggio;

		if (_vociIvaRemoteModel != null) {
			try {
				Class<?> clazz = _vociIvaRemoteModel.getClass();

				Method method = clazz.getMethod("setGestioneOmaggio", int.class);

				method.invoke(_vociIvaRemoteModel, gestioneOmaggio);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodIvaSpeseOmaggio() {
		return _codIvaSpeseOmaggio;
	}

	@Override
	public void setCodIvaSpeseOmaggio(String codIvaSpeseOmaggio) {
		_codIvaSpeseOmaggio = codIvaSpeseOmaggio;

		if (_vociIvaRemoteModel != null) {
			try {
				Class<?> clazz = _vociIvaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodIvaSpeseOmaggio",
						String.class);

				method.invoke(_vociIvaRemoteModel, codIvaSpeseOmaggio);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizioneDocuemto() {
		return _descrizioneDocuemto;
	}

	@Override
	public void setDescrizioneDocuemto(String descrizioneDocuemto) {
		_descrizioneDocuemto = descrizioneDocuemto;

		if (_vociIvaRemoteModel != null) {
			try {
				Class<?> clazz = _vociIvaRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizioneDocuemto",
						String.class);

				method.invoke(_vociIvaRemoteModel, descrizioneDocuemto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getVociIvaRemoteModel() {
		return _vociIvaRemoteModel;
	}

	public void setVociIvaRemoteModel(BaseModel<?> vociIvaRemoteModel) {
		_vociIvaRemoteModel = vociIvaRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _vociIvaRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_vociIvaRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			VociIvaLocalServiceUtil.addVociIva(this);
		}
		else {
			VociIvaLocalServiceUtil.updateVociIva(this);
		}
	}

	@Override
	public VociIva toEscapedModel() {
		return (VociIva)ProxyUtil.newProxyInstance(VociIva.class.getClassLoader(),
			new Class[] { VociIva.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		VociIvaClp clone = new VociIvaClp();

		clone.setCodiceIva(getCodiceIva());
		clone.setDescrizione(getDescrizione());
		clone.setAliquota(getAliquota());
		clone.setAliquotaVandita(getAliquotaVandita());
		clone.setAliquotaScorporo(getAliquotaScorporo());
		clone.setAliquotaUlterioreDetr(getAliquotaUlterioreDetr());
		clone.setTipoVoceIva(getTipoVoceIva());
		clone.setGestioneOmaggio(getGestioneOmaggio());
		clone.setCodIvaSpeseOmaggio(getCodIvaSpeseOmaggio());
		clone.setDescrizioneDocuemto(getDescrizioneDocuemto());

		return clone;
	}

	@Override
	public int compareTo(VociIva vociIva) {
		String primaryKey = vociIva.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VociIvaClp)) {
			return false;
		}

		VociIvaClp vociIva = (VociIvaClp)obj;

		String primaryKey = vociIva.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{codiceIva=");
		sb.append(getCodiceIva());
		sb.append(", descrizione=");
		sb.append(getDescrizione());
		sb.append(", aliquota=");
		sb.append(getAliquota());
		sb.append(", aliquotaVandita=");
		sb.append(getAliquotaVandita());
		sb.append(", aliquotaScorporo=");
		sb.append(getAliquotaScorporo());
		sb.append(", aliquotaUlterioreDetr=");
		sb.append(getAliquotaUlterioreDetr());
		sb.append(", tipoVoceIva=");
		sb.append(getTipoVoceIva());
		sb.append(", gestioneOmaggio=");
		sb.append(getGestioneOmaggio());
		sb.append(", codIvaSpeseOmaggio=");
		sb.append(getCodIvaSpeseOmaggio());
		sb.append(", descrizioneDocuemto=");
		sb.append(getDescrizioneDocuemto());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(34);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.VociIva");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>codiceIva</column-name><column-value><![CDATA[");
		sb.append(getCodiceIva());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizione</column-name><column-value><![CDATA[");
		sb.append(getDescrizione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>aliquota</column-name><column-value><![CDATA[");
		sb.append(getAliquota());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>aliquotaVandita</column-name><column-value><![CDATA[");
		sb.append(getAliquotaVandita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>aliquotaScorporo</column-name><column-value><![CDATA[");
		sb.append(getAliquotaScorporo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>aliquotaUlterioreDetr</column-name><column-value><![CDATA[");
		sb.append(getAliquotaUlterioreDetr());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoVoceIva</column-name><column-value><![CDATA[");
		sb.append(getTipoVoceIva());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>gestioneOmaggio</column-name><column-value><![CDATA[");
		sb.append(getGestioneOmaggio());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codIvaSpeseOmaggio</column-name><column-value><![CDATA[");
		sb.append(getCodIvaSpeseOmaggio());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizioneDocuemto</column-name><column-value><![CDATA[");
		sb.append(getDescrizioneDocuemto());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _codiceIva;
	private String _descrizione;
	private double _aliquota;
	private double _aliquotaVandita;
	private double _aliquotaScorporo;
	private double _aliquotaUlterioreDetr;
	private int _tipoVoceIva;
	private int _gestioneOmaggio;
	private String _codIvaSpeseOmaggio;
	private String _descrizioneDocuemto;
	private BaseModel<?> _vociIvaRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}