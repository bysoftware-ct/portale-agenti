/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.RigheFattureVeditaLocalServiceUtil;
import it.bysoftware.ct.service.persistence.RigheFattureVeditaPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class RigheFattureVeditaClp extends BaseModelImpl<RigheFattureVedita>
	implements RigheFattureVedita {
	public RigheFattureVeditaClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return RigheFattureVedita.class;
	}

	@Override
	public String getModelClassName() {
		return RigheFattureVedita.class.getName();
	}

	@Override
	public RigheFattureVeditaPK getPrimaryKey() {
		return new RigheFattureVeditaPK(_anno, _codiceAttivita, _codiceCentro,
			_numeroProtocollo, _numeroRigo);
	}

	@Override
	public void setPrimaryKey(RigheFattureVeditaPK primaryKey) {
		setAnno(primaryKey.anno);
		setCodiceAttivita(primaryKey.codiceAttivita);
		setCodiceCentro(primaryKey.codiceCentro);
		setNumeroProtocollo(primaryKey.numeroProtocollo);
		setNumeroRigo(primaryKey.numeroRigo);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new RigheFattureVeditaPK(_anno, _codiceAttivita, _codiceCentro,
			_numeroProtocollo, _numeroRigo);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((RigheFattureVeditaPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("anno", getAnno());
		attributes.put("codiceAttivita", getCodiceAttivita());
		attributes.put("codiceCentro", getCodiceCentro());
		attributes.put("numeroProtocollo", getNumeroProtocollo());
		attributes.put("numeroRigo", getNumeroRigo());
		attributes.put("statoRigo", getStatoRigo());
		attributes.put("annoBolla", getAnnoBolla());
		attributes.put("tipoIdentificativoBolla", getTipoIdentificativoBolla());
		attributes.put("tipoBolla", getTipoBolla());
		attributes.put("codiceDeposito", getCodiceDeposito());
		attributes.put("numeroBollettario", getNumeroBollettario());
		attributes.put("numeroBolla", getNumeroBolla());
		attributes.put("dataBollaEvasa", getDataBollaEvasa());
		attributes.put("rigoDDT", getRigoDDT());
		attributes.put("tipoRigoDDT", getTipoRigoDDT());
		attributes.put("codiceCausaleMagazzino", getCodiceCausaleMagazzino());
		attributes.put("codiceDepositoMagazzino", getCodiceDepositoMagazzino());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("codiceTestoDescrizioni", getCodiceTestoDescrizioni());
		attributes.put("descrizione", getDescrizione());
		attributes.put("unitaMisura", getUnitaMisura());
		attributes.put("numeroDecimalliQuant", getNumeroDecimalliQuant());
		attributes.put("quantita1", getQuantita1());
		attributes.put("quantita2", getQuantita2());
		attributes.put("quantita3", getQuantita3());
		attributes.put("quantita", getQuantita());
		attributes.put("unitaMisura2", getUnitaMisura2());
		attributes.put("quantitaUnitMisSec", getQuantitaUnitMisSec());
		attributes.put("numeroDecimalliPrezzo", getNumeroDecimalliPrezzo());
		attributes.put("prezzo", getPrezzo());
		attributes.put("prezzoIVA", getPrezzoIVA());
		attributes.put("importoLordo", getImportoLordo());
		attributes.put("importoLordoIVA", getImportoLordoIVA());
		attributes.put("sconto1", getSconto1());
		attributes.put("sconto2", getSconto2());
		attributes.put("sconto3", getSconto3());
		attributes.put("importoNettoRigo", getImportoNettoRigo());
		attributes.put("importoNettoRigoIVA", getImportoNettoRigoIVA());
		attributes.put("importoNetto", getImportoNetto());
		attributes.put("importoNettoIVA", getImportoNettoIVA());
		attributes.put("importoCONAIAddebitatoInt",
			getImportoCONAIAddebitatoInt());
		attributes.put("importoCONAIAddebitatoOrig",
			getImportoCONAIAddebitatoOrig());
		attributes.put("codiceIVAFatturazione", getCodiceIVAFatturazione());
		attributes.put("nomenclaturaCombinata", getNomenclaturaCombinata());
		attributes.put("testStampaDisBase", getTestStampaDisBase());
		attributes.put("tipoOrdineEvaso", getTipoOrdineEvaso());
		attributes.put("annoOrdineEvaso", getAnnoOrdineEvaso());
		attributes.put("numeroOrdineEvaso", getNumeroOrdineEvaso());
		attributes.put("numeroRigoOrdineEvaso", getNumeroRigoOrdineEvaso());
		attributes.put("tipoEvasione", getTipoEvasione());
		attributes.put("descrizioneRiferimento", getDescrizioneRiferimento());
		attributes.put("numeroPrimaNotaMagazzino", getNumeroPrimaNotaMagazzino());
		attributes.put("numeroMovimentoMagazzino", getNumeroMovimentoMagazzino());
		attributes.put("tipoEsplosione", getTipoEsplosione());
		attributes.put("livelloMassimoEsplosione", getLivelloMassimoEsplosione());
		attributes.put("libStr1", getLibStr1());
		attributes.put("libStr2", getLibStr2());
		attributes.put("libStr3", getLibStr3());
		attributes.put("libDbl1", getLibDbl1());
		attributes.put("libDbl2", getLibDbl2());
		attributes.put("libDbl3", getLibDbl3());
		attributes.put("libLng1", getLibLng1());
		attributes.put("libLng2", getLibLng2());
		attributes.put("libLng3", getLibLng3());
		attributes.put("libDat1", getLibDat1());
		attributes.put("libDat2", getLibDat2());
		attributes.put("libDat3", getLibDat3());
		attributes.put("testFatturazioneAuto", getTestFatturazioneAuto());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer anno = (Integer)attributes.get("anno");

		if (anno != null) {
			setAnno(anno);
		}

		String codiceAttivita = (String)attributes.get("codiceAttivita");

		if (codiceAttivita != null) {
			setCodiceAttivita(codiceAttivita);
		}

		String codiceCentro = (String)attributes.get("codiceCentro");

		if (codiceCentro != null) {
			setCodiceCentro(codiceCentro);
		}

		Integer numeroProtocollo = (Integer)attributes.get("numeroProtocollo");

		if (numeroProtocollo != null) {
			setNumeroProtocollo(numeroProtocollo);
		}

		Integer numeroRigo = (Integer)attributes.get("numeroRigo");

		if (numeroRigo != null) {
			setNumeroRigo(numeroRigo);
		}

		Boolean statoRigo = (Boolean)attributes.get("statoRigo");

		if (statoRigo != null) {
			setStatoRigo(statoRigo);
		}

		Integer annoBolla = (Integer)attributes.get("annoBolla");

		if (annoBolla != null) {
			setAnnoBolla(annoBolla);
		}

		Integer tipoIdentificativoBolla = (Integer)attributes.get(
				"tipoIdentificativoBolla");

		if (tipoIdentificativoBolla != null) {
			setTipoIdentificativoBolla(tipoIdentificativoBolla);
		}

		String tipoBolla = (String)attributes.get("tipoBolla");

		if (tipoBolla != null) {
			setTipoBolla(tipoBolla);
		}

		String codiceDeposito = (String)attributes.get("codiceDeposito");

		if (codiceDeposito != null) {
			setCodiceDeposito(codiceDeposito);
		}

		Integer numeroBollettario = (Integer)attributes.get("numeroBollettario");

		if (numeroBollettario != null) {
			setNumeroBollettario(numeroBollettario);
		}

		Integer numeroBolla = (Integer)attributes.get("numeroBolla");

		if (numeroBolla != null) {
			setNumeroBolla(numeroBolla);
		}

		Date dataBollaEvasa = (Date)attributes.get("dataBollaEvasa");

		if (dataBollaEvasa != null) {
			setDataBollaEvasa(dataBollaEvasa);
		}

		Integer rigoDDT = (Integer)attributes.get("rigoDDT");

		if (rigoDDT != null) {
			setRigoDDT(rigoDDT);
		}

		Integer tipoRigoDDT = (Integer)attributes.get("tipoRigoDDT");

		if (tipoRigoDDT != null) {
			setTipoRigoDDT(tipoRigoDDT);
		}

		String codiceCausaleMagazzino = (String)attributes.get(
				"codiceCausaleMagazzino");

		if (codiceCausaleMagazzino != null) {
			setCodiceCausaleMagazzino(codiceCausaleMagazzino);
		}

		String codiceDepositoMagazzino = (String)attributes.get(
				"codiceDepositoMagazzino");

		if (codiceDepositoMagazzino != null) {
			setCodiceDepositoMagazzino(codiceDepositoMagazzino);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		String codiceTestoDescrizioni = (String)attributes.get(
				"codiceTestoDescrizioni");

		if (codiceTestoDescrizioni != null) {
			setCodiceTestoDescrizioni(codiceTestoDescrizioni);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		String unitaMisura = (String)attributes.get("unitaMisura");

		if (unitaMisura != null) {
			setUnitaMisura(unitaMisura);
		}

		Integer numeroDecimalliQuant = (Integer)attributes.get(
				"numeroDecimalliQuant");

		if (numeroDecimalliQuant != null) {
			setNumeroDecimalliQuant(numeroDecimalliQuant);
		}

		Double quantita1 = (Double)attributes.get("quantita1");

		if (quantita1 != null) {
			setQuantita1(quantita1);
		}

		Double quantita2 = (Double)attributes.get("quantita2");

		if (quantita2 != null) {
			setQuantita2(quantita2);
		}

		Double quantita3 = (Double)attributes.get("quantita3");

		if (quantita3 != null) {
			setQuantita3(quantita3);
		}

		Double quantita = (Double)attributes.get("quantita");

		if (quantita != null) {
			setQuantita(quantita);
		}

		String unitaMisura2 = (String)attributes.get("unitaMisura2");

		if (unitaMisura2 != null) {
			setUnitaMisura2(unitaMisura2);
		}

		Double quantitaUnitMisSec = (Double)attributes.get("quantitaUnitMisSec");

		if (quantitaUnitMisSec != null) {
			setQuantitaUnitMisSec(quantitaUnitMisSec);
		}

		Integer numeroDecimalliPrezzo = (Integer)attributes.get(
				"numeroDecimalliPrezzo");

		if (numeroDecimalliPrezzo != null) {
			setNumeroDecimalliPrezzo(numeroDecimalliPrezzo);
		}

		Double prezzo = (Double)attributes.get("prezzo");

		if (prezzo != null) {
			setPrezzo(prezzo);
		}

		Double prezzoIVA = (Double)attributes.get("prezzoIVA");

		if (prezzoIVA != null) {
			setPrezzoIVA(prezzoIVA);
		}

		Double importoLordo = (Double)attributes.get("importoLordo");

		if (importoLordo != null) {
			setImportoLordo(importoLordo);
		}

		Double importoLordoIVA = (Double)attributes.get("importoLordoIVA");

		if (importoLordoIVA != null) {
			setImportoLordoIVA(importoLordoIVA);
		}

		Double sconto1 = (Double)attributes.get("sconto1");

		if (sconto1 != null) {
			setSconto1(sconto1);
		}

		Double sconto2 = (Double)attributes.get("sconto2");

		if (sconto2 != null) {
			setSconto2(sconto2);
		}

		Double sconto3 = (Double)attributes.get("sconto3");

		if (sconto3 != null) {
			setSconto3(sconto3);
		}

		Double importoNettoRigo = (Double)attributes.get("importoNettoRigo");

		if (importoNettoRigo != null) {
			setImportoNettoRigo(importoNettoRigo);
		}

		Double importoNettoRigoIVA = (Double)attributes.get(
				"importoNettoRigoIVA");

		if (importoNettoRigoIVA != null) {
			setImportoNettoRigoIVA(importoNettoRigoIVA);
		}

		Double importoNetto = (Double)attributes.get("importoNetto");

		if (importoNetto != null) {
			setImportoNetto(importoNetto);
		}

		Double importoNettoIVA = (Double)attributes.get("importoNettoIVA");

		if (importoNettoIVA != null) {
			setImportoNettoIVA(importoNettoIVA);
		}

		Double importoCONAIAddebitatoInt = (Double)attributes.get(
				"importoCONAIAddebitatoInt");

		if (importoCONAIAddebitatoInt != null) {
			setImportoCONAIAddebitatoInt(importoCONAIAddebitatoInt);
		}

		Double importoCONAIAddebitatoOrig = (Double)attributes.get(
				"importoCONAIAddebitatoOrig");

		if (importoCONAIAddebitatoOrig != null) {
			setImportoCONAIAddebitatoOrig(importoCONAIAddebitatoOrig);
		}

		String codiceIVAFatturazione = (String)attributes.get(
				"codiceIVAFatturazione");

		if (codiceIVAFatturazione != null) {
			setCodiceIVAFatturazione(codiceIVAFatturazione);
		}

		Integer nomenclaturaCombinata = (Integer)attributes.get(
				"nomenclaturaCombinata");

		if (nomenclaturaCombinata != null) {
			setNomenclaturaCombinata(nomenclaturaCombinata);
		}

		Boolean testStampaDisBase = (Boolean)attributes.get("testStampaDisBase");

		if (testStampaDisBase != null) {
			setTestStampaDisBase(testStampaDisBase);
		}

		Integer tipoOrdineEvaso = (Integer)attributes.get("tipoOrdineEvaso");

		if (tipoOrdineEvaso != null) {
			setTipoOrdineEvaso(tipoOrdineEvaso);
		}

		Integer annoOrdineEvaso = (Integer)attributes.get("annoOrdineEvaso");

		if (annoOrdineEvaso != null) {
			setAnnoOrdineEvaso(annoOrdineEvaso);
		}

		Integer numeroOrdineEvaso = (Integer)attributes.get("numeroOrdineEvaso");

		if (numeroOrdineEvaso != null) {
			setNumeroOrdineEvaso(numeroOrdineEvaso);
		}

		Integer numeroRigoOrdineEvaso = (Integer)attributes.get(
				"numeroRigoOrdineEvaso");

		if (numeroRigoOrdineEvaso != null) {
			setNumeroRigoOrdineEvaso(numeroRigoOrdineEvaso);
		}

		String tipoEvasione = (String)attributes.get("tipoEvasione");

		if (tipoEvasione != null) {
			setTipoEvasione(tipoEvasione);
		}

		String descrizioneRiferimento = (String)attributes.get(
				"descrizioneRiferimento");

		if (descrizioneRiferimento != null) {
			setDescrizioneRiferimento(descrizioneRiferimento);
		}

		Integer numeroPrimaNotaMagazzino = (Integer)attributes.get(
				"numeroPrimaNotaMagazzino");

		if (numeroPrimaNotaMagazzino != null) {
			setNumeroPrimaNotaMagazzino(numeroPrimaNotaMagazzino);
		}

		Integer numeroMovimentoMagazzino = (Integer)attributes.get(
				"numeroMovimentoMagazzino");

		if (numeroMovimentoMagazzino != null) {
			setNumeroMovimentoMagazzino(numeroMovimentoMagazzino);
		}

		Integer tipoEsplosione = (Integer)attributes.get("tipoEsplosione");

		if (tipoEsplosione != null) {
			setTipoEsplosione(tipoEsplosione);
		}

		Integer livelloMassimoEsplosione = (Integer)attributes.get(
				"livelloMassimoEsplosione");

		if (livelloMassimoEsplosione != null) {
			setLivelloMassimoEsplosione(livelloMassimoEsplosione);
		}

		String libStr1 = (String)attributes.get("libStr1");

		if (libStr1 != null) {
			setLibStr1(libStr1);
		}

		String libStr2 = (String)attributes.get("libStr2");

		if (libStr2 != null) {
			setLibStr2(libStr2);
		}

		String libStr3 = (String)attributes.get("libStr3");

		if (libStr3 != null) {
			setLibStr3(libStr3);
		}

		Double libDbl1 = (Double)attributes.get("libDbl1");

		if (libDbl1 != null) {
			setLibDbl1(libDbl1);
		}

		Double libDbl2 = (Double)attributes.get("libDbl2");

		if (libDbl2 != null) {
			setLibDbl2(libDbl2);
		}

		Double libDbl3 = (Double)attributes.get("libDbl3");

		if (libDbl3 != null) {
			setLibDbl3(libDbl3);
		}

		Long libLng1 = (Long)attributes.get("libLng1");

		if (libLng1 != null) {
			setLibLng1(libLng1);
		}

		Long libLng2 = (Long)attributes.get("libLng2");

		if (libLng2 != null) {
			setLibLng2(libLng2);
		}

		Long libLng3 = (Long)attributes.get("libLng3");

		if (libLng3 != null) {
			setLibLng3(libLng3);
		}

		Date libDat1 = (Date)attributes.get("libDat1");

		if (libDat1 != null) {
			setLibDat1(libDat1);
		}

		Date libDat2 = (Date)attributes.get("libDat2");

		if (libDat2 != null) {
			setLibDat2(libDat2);
		}

		Date libDat3 = (Date)attributes.get("libDat3");

		if (libDat3 != null) {
			setLibDat3(libDat3);
		}

		Boolean testFatturazioneAuto = (Boolean)attributes.get(
				"testFatturazioneAuto");

		if (testFatturazioneAuto != null) {
			setTestFatturazioneAuto(testFatturazioneAuto);
		}
	}

	@Override
	public int getAnno() {
		return _anno;
	}

	@Override
	public void setAnno(int anno) {
		_anno = anno;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setAnno", int.class);

				method.invoke(_righeFattureVeditaRemoteModel, anno);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceAttivita() {
		return _codiceAttivita;
	}

	@Override
	public void setCodiceAttivita(String codiceAttivita) {
		_codiceAttivita = codiceAttivita;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceAttivita",
						String.class);

				method.invoke(_righeFattureVeditaRemoteModel, codiceAttivita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCentro() {
		return _codiceCentro;
	}

	@Override
	public void setCodiceCentro(String codiceCentro) {
		_codiceCentro = codiceCentro;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCentro", String.class);

				method.invoke(_righeFattureVeditaRemoteModel, codiceCentro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroProtocollo() {
		return _numeroProtocollo;
	}

	@Override
	public void setNumeroProtocollo(int numeroProtocollo) {
		_numeroProtocollo = numeroProtocollo;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroProtocollo", int.class);

				method.invoke(_righeFattureVeditaRemoteModel, numeroProtocollo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroRigo() {
		return _numeroRigo;
	}

	@Override
	public void setNumeroRigo(int numeroRigo) {
		_numeroRigo = numeroRigo;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroRigo", int.class);

				method.invoke(_righeFattureVeditaRemoteModel, numeroRigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getStatoRigo() {
		return _statoRigo;
	}

	@Override
	public boolean isStatoRigo() {
		return _statoRigo;
	}

	@Override
	public void setStatoRigo(boolean statoRigo) {
		_statoRigo = statoRigo;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setStatoRigo", boolean.class);

				method.invoke(_righeFattureVeditaRemoteModel, statoRigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getAnnoBolla() {
		return _annoBolla;
	}

	@Override
	public void setAnnoBolla(int annoBolla) {
		_annoBolla = annoBolla;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setAnnoBolla", int.class);

				method.invoke(_righeFattureVeditaRemoteModel, annoBolla);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoIdentificativoBolla() {
		return _tipoIdentificativoBolla;
	}

	@Override
	public void setTipoIdentificativoBolla(int tipoIdentificativoBolla) {
		_tipoIdentificativoBolla = tipoIdentificativoBolla;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoIdentificativoBolla",
						int.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					tipoIdentificativoBolla);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoBolla() {
		return _tipoBolla;
	}

	@Override
	public void setTipoBolla(String tipoBolla) {
		_tipoBolla = tipoBolla;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoBolla", String.class);

				method.invoke(_righeFattureVeditaRemoteModel, tipoBolla);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDeposito() {
		return _codiceDeposito;
	}

	@Override
	public void setCodiceDeposito(String codiceDeposito) {
		_codiceDeposito = codiceDeposito;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDeposito",
						String.class);

				method.invoke(_righeFattureVeditaRemoteModel, codiceDeposito);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroBollettario() {
		return _numeroBollettario;
	}

	@Override
	public void setNumeroBollettario(int numeroBollettario) {
		_numeroBollettario = numeroBollettario;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroBollettario",
						int.class);

				method.invoke(_righeFattureVeditaRemoteModel, numeroBollettario);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroBolla() {
		return _numeroBolla;
	}

	@Override
	public void setNumeroBolla(int numeroBolla) {
		_numeroBolla = numeroBolla;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroBolla", int.class);

				method.invoke(_righeFattureVeditaRemoteModel, numeroBolla);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataBollaEvasa() {
		return _dataBollaEvasa;
	}

	@Override
	public void setDataBollaEvasa(Date dataBollaEvasa) {
		_dataBollaEvasa = dataBollaEvasa;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setDataBollaEvasa", Date.class);

				method.invoke(_righeFattureVeditaRemoteModel, dataBollaEvasa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getRigoDDT() {
		return _rigoDDT;
	}

	@Override
	public void setRigoDDT(int rigoDDT) {
		_rigoDDT = rigoDDT;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setRigoDDT", int.class);

				method.invoke(_righeFattureVeditaRemoteModel, rigoDDT);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoRigoDDT() {
		return _tipoRigoDDT;
	}

	@Override
	public void setTipoRigoDDT(int tipoRigoDDT) {
		_tipoRigoDDT = tipoRigoDDT;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoRigoDDT", int.class);

				method.invoke(_righeFattureVeditaRemoteModel, tipoRigoDDT);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCausaleMagazzino() {
		return _codiceCausaleMagazzino;
	}

	@Override
	public void setCodiceCausaleMagazzino(String codiceCausaleMagazzino) {
		_codiceCausaleMagazzino = codiceCausaleMagazzino;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCausaleMagazzino",
						String.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					codiceCausaleMagazzino);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDepositoMagazzino() {
		return _codiceDepositoMagazzino;
	}

	@Override
	public void setCodiceDepositoMagazzino(String codiceDepositoMagazzino) {
		_codiceDepositoMagazzino = codiceDepositoMagazzino;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDepositoMagazzino",
						String.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					codiceDepositoMagazzino);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	@Override
	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceArticolo",
						String.class);

				method.invoke(_righeFattureVeditaRemoteModel, codiceArticolo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceVariante() {
		return _codiceVariante;
	}

	@Override
	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceVariante",
						String.class);

				method.invoke(_righeFattureVeditaRemoteModel, codiceVariante);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceTestoDescrizioni() {
		return _codiceTestoDescrizioni;
	}

	@Override
	public void setCodiceTestoDescrizioni(String codiceTestoDescrizioni) {
		_codiceTestoDescrizioni = codiceTestoDescrizioni;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceTestoDescrizioni",
						String.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					codiceTestoDescrizioni);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizione() {
		return _descrizione;
	}

	@Override
	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizione", String.class);

				method.invoke(_righeFattureVeditaRemoteModel, descrizione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUnitaMisura() {
		return _unitaMisura;
	}

	@Override
	public void setUnitaMisura(String unitaMisura) {
		_unitaMisura = unitaMisura;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setUnitaMisura", String.class);

				method.invoke(_righeFattureVeditaRemoteModel, unitaMisura);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroDecimalliQuant() {
		return _numeroDecimalliQuant;
	}

	@Override
	public void setNumeroDecimalliQuant(int numeroDecimalliQuant) {
		_numeroDecimalliQuant = numeroDecimalliQuant;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroDecimalliQuant",
						int.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					numeroDecimalliQuant);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantita1() {
		return _quantita1;
	}

	@Override
	public void setQuantita1(double quantita1) {
		_quantita1 = quantita1;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantita1", double.class);

				method.invoke(_righeFattureVeditaRemoteModel, quantita1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantita2() {
		return _quantita2;
	}

	@Override
	public void setQuantita2(double quantita2) {
		_quantita2 = quantita2;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantita2", double.class);

				method.invoke(_righeFattureVeditaRemoteModel, quantita2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantita3() {
		return _quantita3;
	}

	@Override
	public void setQuantita3(double quantita3) {
		_quantita3 = quantita3;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantita3", double.class);

				method.invoke(_righeFattureVeditaRemoteModel, quantita3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantita() {
		return _quantita;
	}

	@Override
	public void setQuantita(double quantita) {
		_quantita = quantita;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantita", double.class);

				method.invoke(_righeFattureVeditaRemoteModel, quantita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUnitaMisura2() {
		return _unitaMisura2;
	}

	@Override
	public void setUnitaMisura2(String unitaMisura2) {
		_unitaMisura2 = unitaMisura2;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setUnitaMisura2", String.class);

				method.invoke(_righeFattureVeditaRemoteModel, unitaMisura2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaUnitMisSec() {
		return _quantitaUnitMisSec;
	}

	@Override
	public void setQuantitaUnitMisSec(double quantitaUnitMisSec) {
		_quantitaUnitMisSec = quantitaUnitMisSec;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaUnitMisSec",
						double.class);

				method.invoke(_righeFattureVeditaRemoteModel, quantitaUnitMisSec);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroDecimalliPrezzo() {
		return _numeroDecimalliPrezzo;
	}

	@Override
	public void setNumeroDecimalliPrezzo(int numeroDecimalliPrezzo) {
		_numeroDecimalliPrezzo = numeroDecimalliPrezzo;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroDecimalliPrezzo",
						int.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					numeroDecimalliPrezzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPrezzo() {
		return _prezzo;
	}

	@Override
	public void setPrezzo(double prezzo) {
		_prezzo = prezzo;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setPrezzo", double.class);

				method.invoke(_righeFattureVeditaRemoteModel, prezzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPrezzoIVA() {
		return _prezzoIVA;
	}

	@Override
	public void setPrezzoIVA(double prezzoIVA) {
		_prezzoIVA = prezzoIVA;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setPrezzoIVA", double.class);

				method.invoke(_righeFattureVeditaRemoteModel, prezzoIVA);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoLordo() {
		return _importoLordo;
	}

	@Override
	public void setImportoLordo(double importoLordo) {
		_importoLordo = importoLordo;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoLordo", double.class);

				method.invoke(_righeFattureVeditaRemoteModel, importoLordo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoLordoIVA() {
		return _importoLordoIVA;
	}

	@Override
	public void setImportoLordoIVA(double importoLordoIVA) {
		_importoLordoIVA = importoLordoIVA;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoLordoIVA",
						double.class);

				method.invoke(_righeFattureVeditaRemoteModel, importoLordoIVA);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSconto1() {
		return _sconto1;
	}

	@Override
	public void setSconto1(double sconto1) {
		_sconto1 = sconto1;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setSconto1", double.class);

				method.invoke(_righeFattureVeditaRemoteModel, sconto1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSconto2() {
		return _sconto2;
	}

	@Override
	public void setSconto2(double sconto2) {
		_sconto2 = sconto2;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setSconto2", double.class);

				method.invoke(_righeFattureVeditaRemoteModel, sconto2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSconto3() {
		return _sconto3;
	}

	@Override
	public void setSconto3(double sconto3) {
		_sconto3 = sconto3;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setSconto3", double.class);

				method.invoke(_righeFattureVeditaRemoteModel, sconto3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoNettoRigo() {
		return _importoNettoRigo;
	}

	@Override
	public void setImportoNettoRigo(double importoNettoRigo) {
		_importoNettoRigo = importoNettoRigo;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoNettoRigo",
						double.class);

				method.invoke(_righeFattureVeditaRemoteModel, importoNettoRigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoNettoRigoIVA() {
		return _importoNettoRigoIVA;
	}

	@Override
	public void setImportoNettoRigoIVA(double importoNettoRigoIVA) {
		_importoNettoRigoIVA = importoNettoRigoIVA;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoNettoRigoIVA",
						double.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					importoNettoRigoIVA);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoNetto() {
		return _importoNetto;
	}

	@Override
	public void setImportoNetto(double importoNetto) {
		_importoNetto = importoNetto;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoNetto", double.class);

				method.invoke(_righeFattureVeditaRemoteModel, importoNetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoNettoIVA() {
		return _importoNettoIVA;
	}

	@Override
	public void setImportoNettoIVA(double importoNettoIVA) {
		_importoNettoIVA = importoNettoIVA;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoNettoIVA",
						double.class);

				method.invoke(_righeFattureVeditaRemoteModel, importoNettoIVA);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoCONAIAddebitatoInt() {
		return _importoCONAIAddebitatoInt;
	}

	@Override
	public void setImportoCONAIAddebitatoInt(double importoCONAIAddebitatoInt) {
		_importoCONAIAddebitatoInt = importoCONAIAddebitatoInt;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoCONAIAddebitatoInt",
						double.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					importoCONAIAddebitatoInt);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoCONAIAddebitatoOrig() {
		return _importoCONAIAddebitatoOrig;
	}

	@Override
	public void setImportoCONAIAddebitatoOrig(double importoCONAIAddebitatoOrig) {
		_importoCONAIAddebitatoOrig = importoCONAIAddebitatoOrig;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoCONAIAddebitatoOrig",
						double.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					importoCONAIAddebitatoOrig);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVAFatturazione() {
		return _codiceIVAFatturazione;
	}

	@Override
	public void setCodiceIVAFatturazione(String codiceIVAFatturazione) {
		_codiceIVAFatturazione = codiceIVAFatturazione;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVAFatturazione",
						String.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					codiceIVAFatturazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNomenclaturaCombinata() {
		return _nomenclaturaCombinata;
	}

	@Override
	public void setNomenclaturaCombinata(int nomenclaturaCombinata) {
		_nomenclaturaCombinata = nomenclaturaCombinata;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setNomenclaturaCombinata",
						int.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					nomenclaturaCombinata);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getTestStampaDisBase() {
		return _testStampaDisBase;
	}

	@Override
	public boolean isTestStampaDisBase() {
		return _testStampaDisBase;
	}

	@Override
	public void setTestStampaDisBase(boolean testStampaDisBase) {
		_testStampaDisBase = testStampaDisBase;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setTestStampaDisBase",
						boolean.class);

				method.invoke(_righeFattureVeditaRemoteModel, testStampaDisBase);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoOrdineEvaso() {
		return _tipoOrdineEvaso;
	}

	@Override
	public void setTipoOrdineEvaso(int tipoOrdineEvaso) {
		_tipoOrdineEvaso = tipoOrdineEvaso;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoOrdineEvaso", int.class);

				method.invoke(_righeFattureVeditaRemoteModel, tipoOrdineEvaso);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getAnnoOrdineEvaso() {
		return _annoOrdineEvaso;
	}

	@Override
	public void setAnnoOrdineEvaso(int annoOrdineEvaso) {
		_annoOrdineEvaso = annoOrdineEvaso;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setAnnoOrdineEvaso", int.class);

				method.invoke(_righeFattureVeditaRemoteModel, annoOrdineEvaso);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroOrdineEvaso() {
		return _numeroOrdineEvaso;
	}

	@Override
	public void setNumeroOrdineEvaso(int numeroOrdineEvaso) {
		_numeroOrdineEvaso = numeroOrdineEvaso;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroOrdineEvaso",
						int.class);

				method.invoke(_righeFattureVeditaRemoteModel, numeroOrdineEvaso);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroRigoOrdineEvaso() {
		return _numeroRigoOrdineEvaso;
	}

	@Override
	public void setNumeroRigoOrdineEvaso(int numeroRigoOrdineEvaso) {
		_numeroRigoOrdineEvaso = numeroRigoOrdineEvaso;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroRigoOrdineEvaso",
						int.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					numeroRigoOrdineEvaso);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoEvasione() {
		return _tipoEvasione;
	}

	@Override
	public void setTipoEvasione(String tipoEvasione) {
		_tipoEvasione = tipoEvasione;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoEvasione", String.class);

				method.invoke(_righeFattureVeditaRemoteModel, tipoEvasione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizioneRiferimento() {
		return _descrizioneRiferimento;
	}

	@Override
	public void setDescrizioneRiferimento(String descrizioneRiferimento) {
		_descrizioneRiferimento = descrizioneRiferimento;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizioneRiferimento",
						String.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					descrizioneRiferimento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroPrimaNotaMagazzino() {
		return _numeroPrimaNotaMagazzino;
	}

	@Override
	public void setNumeroPrimaNotaMagazzino(int numeroPrimaNotaMagazzino) {
		_numeroPrimaNotaMagazzino = numeroPrimaNotaMagazzino;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroPrimaNotaMagazzino",
						int.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					numeroPrimaNotaMagazzino);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroMovimentoMagazzino() {
		return _numeroMovimentoMagazzino;
	}

	@Override
	public void setNumeroMovimentoMagazzino(int numeroMovimentoMagazzino) {
		_numeroMovimentoMagazzino = numeroMovimentoMagazzino;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroMovimentoMagazzino",
						int.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					numeroMovimentoMagazzino);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoEsplosione() {
		return _tipoEsplosione;
	}

	@Override
	public void setTipoEsplosione(int tipoEsplosione) {
		_tipoEsplosione = tipoEsplosione;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoEsplosione", int.class);

				method.invoke(_righeFattureVeditaRemoteModel, tipoEsplosione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getLivelloMassimoEsplosione() {
		return _livelloMassimoEsplosione;
	}

	@Override
	public void setLivelloMassimoEsplosione(int livelloMassimoEsplosione) {
		_livelloMassimoEsplosione = livelloMassimoEsplosione;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setLivelloMassimoEsplosione",
						int.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					livelloMassimoEsplosione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr1() {
		return _libStr1;
	}

	@Override
	public void setLibStr1(String libStr1) {
		_libStr1 = libStr1;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr1", String.class);

				method.invoke(_righeFattureVeditaRemoteModel, libStr1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr2() {
		return _libStr2;
	}

	@Override
	public void setLibStr2(String libStr2) {
		_libStr2 = libStr2;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr2", String.class);

				method.invoke(_righeFattureVeditaRemoteModel, libStr2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr3() {
		return _libStr3;
	}

	@Override
	public void setLibStr3(String libStr3) {
		_libStr3 = libStr3;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr3", String.class);

				method.invoke(_righeFattureVeditaRemoteModel, libStr3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl1() {
		return _libDbl1;
	}

	@Override
	public void setLibDbl1(double libDbl1) {
		_libDbl1 = libDbl1;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl1", double.class);

				method.invoke(_righeFattureVeditaRemoteModel, libDbl1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl2() {
		return _libDbl2;
	}

	@Override
	public void setLibDbl2(double libDbl2) {
		_libDbl2 = libDbl2;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl2", double.class);

				method.invoke(_righeFattureVeditaRemoteModel, libDbl2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl3() {
		return _libDbl3;
	}

	@Override
	public void setLibDbl3(double libDbl3) {
		_libDbl3 = libDbl3;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl3", double.class);

				method.invoke(_righeFattureVeditaRemoteModel, libDbl3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng1() {
		return _libLng1;
	}

	@Override
	public void setLibLng1(long libLng1) {
		_libLng1 = libLng1;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng1", long.class);

				method.invoke(_righeFattureVeditaRemoteModel, libLng1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng2() {
		return _libLng2;
	}

	@Override
	public void setLibLng2(long libLng2) {
		_libLng2 = libLng2;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng2", long.class);

				method.invoke(_righeFattureVeditaRemoteModel, libLng2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng3() {
		return _libLng3;
	}

	@Override
	public void setLibLng3(long libLng3) {
		_libLng3 = libLng3;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng3", long.class);

				method.invoke(_righeFattureVeditaRemoteModel, libLng3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat1() {
		return _libDat1;
	}

	@Override
	public void setLibDat1(Date libDat1) {
		_libDat1 = libDat1;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat1", Date.class);

				method.invoke(_righeFattureVeditaRemoteModel, libDat1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat2() {
		return _libDat2;
	}

	@Override
	public void setLibDat2(Date libDat2) {
		_libDat2 = libDat2;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat2", Date.class);

				method.invoke(_righeFattureVeditaRemoteModel, libDat2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat3() {
		return _libDat3;
	}

	@Override
	public void setLibDat3(Date libDat3) {
		_libDat3 = libDat3;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat3", Date.class);

				method.invoke(_righeFattureVeditaRemoteModel, libDat3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getTestFatturazioneAuto() {
		return _testFatturazioneAuto;
	}

	@Override
	public boolean isTestFatturazioneAuto() {
		return _testFatturazioneAuto;
	}

	@Override
	public void setTestFatturazioneAuto(boolean testFatturazioneAuto) {
		_testFatturazioneAuto = testFatturazioneAuto;

		if (_righeFattureVeditaRemoteModel != null) {
			try {
				Class<?> clazz = _righeFattureVeditaRemoteModel.getClass();

				Method method = clazz.getMethod("setTestFatturazioneAuto",
						boolean.class);

				method.invoke(_righeFattureVeditaRemoteModel,
					testFatturazioneAuto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getRigheFattureVeditaRemoteModel() {
		return _righeFattureVeditaRemoteModel;
	}

	public void setRigheFattureVeditaRemoteModel(
		BaseModel<?> righeFattureVeditaRemoteModel) {
		_righeFattureVeditaRemoteModel = righeFattureVeditaRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _righeFattureVeditaRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_righeFattureVeditaRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			RigheFattureVeditaLocalServiceUtil.addRigheFattureVedita(this);
		}
		else {
			RigheFattureVeditaLocalServiceUtil.updateRigheFattureVedita(this);
		}
	}

	@Override
	public RigheFattureVedita toEscapedModel() {
		return (RigheFattureVedita)ProxyUtil.newProxyInstance(RigheFattureVedita.class.getClassLoader(),
			new Class[] { RigheFattureVedita.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		RigheFattureVeditaClp clone = new RigheFattureVeditaClp();

		clone.setAnno(getAnno());
		clone.setCodiceAttivita(getCodiceAttivita());
		clone.setCodiceCentro(getCodiceCentro());
		clone.setNumeroProtocollo(getNumeroProtocollo());
		clone.setNumeroRigo(getNumeroRigo());
		clone.setStatoRigo(getStatoRigo());
		clone.setAnnoBolla(getAnnoBolla());
		clone.setTipoIdentificativoBolla(getTipoIdentificativoBolla());
		clone.setTipoBolla(getTipoBolla());
		clone.setCodiceDeposito(getCodiceDeposito());
		clone.setNumeroBollettario(getNumeroBollettario());
		clone.setNumeroBolla(getNumeroBolla());
		clone.setDataBollaEvasa(getDataBollaEvasa());
		clone.setRigoDDT(getRigoDDT());
		clone.setTipoRigoDDT(getTipoRigoDDT());
		clone.setCodiceCausaleMagazzino(getCodiceCausaleMagazzino());
		clone.setCodiceDepositoMagazzino(getCodiceDepositoMagazzino());
		clone.setCodiceArticolo(getCodiceArticolo());
		clone.setCodiceVariante(getCodiceVariante());
		clone.setCodiceTestoDescrizioni(getCodiceTestoDescrizioni());
		clone.setDescrizione(getDescrizione());
		clone.setUnitaMisura(getUnitaMisura());
		clone.setNumeroDecimalliQuant(getNumeroDecimalliQuant());
		clone.setQuantita1(getQuantita1());
		clone.setQuantita2(getQuantita2());
		clone.setQuantita3(getQuantita3());
		clone.setQuantita(getQuantita());
		clone.setUnitaMisura2(getUnitaMisura2());
		clone.setQuantitaUnitMisSec(getQuantitaUnitMisSec());
		clone.setNumeroDecimalliPrezzo(getNumeroDecimalliPrezzo());
		clone.setPrezzo(getPrezzo());
		clone.setPrezzoIVA(getPrezzoIVA());
		clone.setImportoLordo(getImportoLordo());
		clone.setImportoLordoIVA(getImportoLordoIVA());
		clone.setSconto1(getSconto1());
		clone.setSconto2(getSconto2());
		clone.setSconto3(getSconto3());
		clone.setImportoNettoRigo(getImportoNettoRigo());
		clone.setImportoNettoRigoIVA(getImportoNettoRigoIVA());
		clone.setImportoNetto(getImportoNetto());
		clone.setImportoNettoIVA(getImportoNettoIVA());
		clone.setImportoCONAIAddebitatoInt(getImportoCONAIAddebitatoInt());
		clone.setImportoCONAIAddebitatoOrig(getImportoCONAIAddebitatoOrig());
		clone.setCodiceIVAFatturazione(getCodiceIVAFatturazione());
		clone.setNomenclaturaCombinata(getNomenclaturaCombinata());
		clone.setTestStampaDisBase(getTestStampaDisBase());
		clone.setTipoOrdineEvaso(getTipoOrdineEvaso());
		clone.setAnnoOrdineEvaso(getAnnoOrdineEvaso());
		clone.setNumeroOrdineEvaso(getNumeroOrdineEvaso());
		clone.setNumeroRigoOrdineEvaso(getNumeroRigoOrdineEvaso());
		clone.setTipoEvasione(getTipoEvasione());
		clone.setDescrizioneRiferimento(getDescrizioneRiferimento());
		clone.setNumeroPrimaNotaMagazzino(getNumeroPrimaNotaMagazzino());
		clone.setNumeroMovimentoMagazzino(getNumeroMovimentoMagazzino());
		clone.setTipoEsplosione(getTipoEsplosione());
		clone.setLivelloMassimoEsplosione(getLivelloMassimoEsplosione());
		clone.setLibStr1(getLibStr1());
		clone.setLibStr2(getLibStr2());
		clone.setLibStr3(getLibStr3());
		clone.setLibDbl1(getLibDbl1());
		clone.setLibDbl2(getLibDbl2());
		clone.setLibDbl3(getLibDbl3());
		clone.setLibLng1(getLibLng1());
		clone.setLibLng2(getLibLng2());
		clone.setLibLng3(getLibLng3());
		clone.setLibDat1(getLibDat1());
		clone.setLibDat2(getLibDat2());
		clone.setLibDat3(getLibDat3());
		clone.setTestFatturazioneAuto(getTestFatturazioneAuto());

		return clone;
	}

	@Override
	public int compareTo(RigheFattureVedita righeFattureVedita) {
		RigheFattureVeditaPK primaryKey = righeFattureVedita.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof RigheFattureVeditaClp)) {
			return false;
		}

		RigheFattureVeditaClp righeFattureVedita = (RigheFattureVeditaClp)obj;

		RigheFattureVeditaPK primaryKey = righeFattureVedita.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(139);

		sb.append("{anno=");
		sb.append(getAnno());
		sb.append(", codiceAttivita=");
		sb.append(getCodiceAttivita());
		sb.append(", codiceCentro=");
		sb.append(getCodiceCentro());
		sb.append(", numeroProtocollo=");
		sb.append(getNumeroProtocollo());
		sb.append(", numeroRigo=");
		sb.append(getNumeroRigo());
		sb.append(", statoRigo=");
		sb.append(getStatoRigo());
		sb.append(", annoBolla=");
		sb.append(getAnnoBolla());
		sb.append(", tipoIdentificativoBolla=");
		sb.append(getTipoIdentificativoBolla());
		sb.append(", tipoBolla=");
		sb.append(getTipoBolla());
		sb.append(", codiceDeposito=");
		sb.append(getCodiceDeposito());
		sb.append(", numeroBollettario=");
		sb.append(getNumeroBollettario());
		sb.append(", numeroBolla=");
		sb.append(getNumeroBolla());
		sb.append(", dataBollaEvasa=");
		sb.append(getDataBollaEvasa());
		sb.append(", rigoDDT=");
		sb.append(getRigoDDT());
		sb.append(", tipoRigoDDT=");
		sb.append(getTipoRigoDDT());
		sb.append(", codiceCausaleMagazzino=");
		sb.append(getCodiceCausaleMagazzino());
		sb.append(", codiceDepositoMagazzino=");
		sb.append(getCodiceDepositoMagazzino());
		sb.append(", codiceArticolo=");
		sb.append(getCodiceArticolo());
		sb.append(", codiceVariante=");
		sb.append(getCodiceVariante());
		sb.append(", codiceTestoDescrizioni=");
		sb.append(getCodiceTestoDescrizioni());
		sb.append(", descrizione=");
		sb.append(getDescrizione());
		sb.append(", unitaMisura=");
		sb.append(getUnitaMisura());
		sb.append(", numeroDecimalliQuant=");
		sb.append(getNumeroDecimalliQuant());
		sb.append(", quantita1=");
		sb.append(getQuantita1());
		sb.append(", quantita2=");
		sb.append(getQuantita2());
		sb.append(", quantita3=");
		sb.append(getQuantita3());
		sb.append(", quantita=");
		sb.append(getQuantita());
		sb.append(", unitaMisura2=");
		sb.append(getUnitaMisura2());
		sb.append(", quantitaUnitMisSec=");
		sb.append(getQuantitaUnitMisSec());
		sb.append(", numeroDecimalliPrezzo=");
		sb.append(getNumeroDecimalliPrezzo());
		sb.append(", prezzo=");
		sb.append(getPrezzo());
		sb.append(", prezzoIVA=");
		sb.append(getPrezzoIVA());
		sb.append(", importoLordo=");
		sb.append(getImportoLordo());
		sb.append(", importoLordoIVA=");
		sb.append(getImportoLordoIVA());
		sb.append(", sconto1=");
		sb.append(getSconto1());
		sb.append(", sconto2=");
		sb.append(getSconto2());
		sb.append(", sconto3=");
		sb.append(getSconto3());
		sb.append(", importoNettoRigo=");
		sb.append(getImportoNettoRigo());
		sb.append(", importoNettoRigoIVA=");
		sb.append(getImportoNettoRigoIVA());
		sb.append(", importoNetto=");
		sb.append(getImportoNetto());
		sb.append(", importoNettoIVA=");
		sb.append(getImportoNettoIVA());
		sb.append(", importoCONAIAddebitatoInt=");
		sb.append(getImportoCONAIAddebitatoInt());
		sb.append(", importoCONAIAddebitatoOrig=");
		sb.append(getImportoCONAIAddebitatoOrig());
		sb.append(", codiceIVAFatturazione=");
		sb.append(getCodiceIVAFatturazione());
		sb.append(", nomenclaturaCombinata=");
		sb.append(getNomenclaturaCombinata());
		sb.append(", testStampaDisBase=");
		sb.append(getTestStampaDisBase());
		sb.append(", tipoOrdineEvaso=");
		sb.append(getTipoOrdineEvaso());
		sb.append(", annoOrdineEvaso=");
		sb.append(getAnnoOrdineEvaso());
		sb.append(", numeroOrdineEvaso=");
		sb.append(getNumeroOrdineEvaso());
		sb.append(", numeroRigoOrdineEvaso=");
		sb.append(getNumeroRigoOrdineEvaso());
		sb.append(", tipoEvasione=");
		sb.append(getTipoEvasione());
		sb.append(", descrizioneRiferimento=");
		sb.append(getDescrizioneRiferimento());
		sb.append(", numeroPrimaNotaMagazzino=");
		sb.append(getNumeroPrimaNotaMagazzino());
		sb.append(", numeroMovimentoMagazzino=");
		sb.append(getNumeroMovimentoMagazzino());
		sb.append(", tipoEsplosione=");
		sb.append(getTipoEsplosione());
		sb.append(", livelloMassimoEsplosione=");
		sb.append(getLivelloMassimoEsplosione());
		sb.append(", libStr1=");
		sb.append(getLibStr1());
		sb.append(", libStr2=");
		sb.append(getLibStr2());
		sb.append(", libStr3=");
		sb.append(getLibStr3());
		sb.append(", libDbl1=");
		sb.append(getLibDbl1());
		sb.append(", libDbl2=");
		sb.append(getLibDbl2());
		sb.append(", libDbl3=");
		sb.append(getLibDbl3());
		sb.append(", libLng1=");
		sb.append(getLibLng1());
		sb.append(", libLng2=");
		sb.append(getLibLng2());
		sb.append(", libLng3=");
		sb.append(getLibLng3());
		sb.append(", libDat1=");
		sb.append(getLibDat1());
		sb.append(", libDat2=");
		sb.append(getLibDat2());
		sb.append(", libDat3=");
		sb.append(getLibDat3());
		sb.append(", testFatturazioneAuto=");
		sb.append(getTestFatturazioneAuto());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(211);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.RigheFattureVedita");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>anno</column-name><column-value><![CDATA[");
		sb.append(getAnno());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceAttivita</column-name><column-value><![CDATA[");
		sb.append(getCodiceAttivita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCentro</column-name><column-value><![CDATA[");
		sb.append(getCodiceCentro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroProtocollo</column-name><column-value><![CDATA[");
		sb.append(getNumeroProtocollo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroRigo</column-name><column-value><![CDATA[");
		sb.append(getNumeroRigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statoRigo</column-name><column-value><![CDATA[");
		sb.append(getStatoRigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>annoBolla</column-name><column-value><![CDATA[");
		sb.append(getAnnoBolla());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoIdentificativoBolla</column-name><column-value><![CDATA[");
		sb.append(getTipoIdentificativoBolla());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoBolla</column-name><column-value><![CDATA[");
		sb.append(getTipoBolla());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDeposito</column-name><column-value><![CDATA[");
		sb.append(getCodiceDeposito());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroBollettario</column-name><column-value><![CDATA[");
		sb.append(getNumeroBollettario());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroBolla</column-name><column-value><![CDATA[");
		sb.append(getNumeroBolla());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataBollaEvasa</column-name><column-value><![CDATA[");
		sb.append(getDataBollaEvasa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>rigoDDT</column-name><column-value><![CDATA[");
		sb.append(getRigoDDT());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoRigoDDT</column-name><column-value><![CDATA[");
		sb.append(getTipoRigoDDT());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCausaleMagazzino</column-name><column-value><![CDATA[");
		sb.append(getCodiceCausaleMagazzino());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDepositoMagazzino</column-name><column-value><![CDATA[");
		sb.append(getCodiceDepositoMagazzino());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceArticolo</column-name><column-value><![CDATA[");
		sb.append(getCodiceArticolo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceVariante</column-name><column-value><![CDATA[");
		sb.append(getCodiceVariante());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceTestoDescrizioni</column-name><column-value><![CDATA[");
		sb.append(getCodiceTestoDescrizioni());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizione</column-name><column-value><![CDATA[");
		sb.append(getDescrizione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>unitaMisura</column-name><column-value><![CDATA[");
		sb.append(getUnitaMisura());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroDecimalliQuant</column-name><column-value><![CDATA[");
		sb.append(getNumeroDecimalliQuant());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantita1</column-name><column-value><![CDATA[");
		sb.append(getQuantita1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantita2</column-name><column-value><![CDATA[");
		sb.append(getQuantita2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantita3</column-name><column-value><![CDATA[");
		sb.append(getQuantita3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantita</column-name><column-value><![CDATA[");
		sb.append(getQuantita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>unitaMisura2</column-name><column-value><![CDATA[");
		sb.append(getUnitaMisura2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaUnitMisSec</column-name><column-value><![CDATA[");
		sb.append(getQuantitaUnitMisSec());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroDecimalliPrezzo</column-name><column-value><![CDATA[");
		sb.append(getNumeroDecimalliPrezzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>prezzo</column-name><column-value><![CDATA[");
		sb.append(getPrezzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>prezzoIVA</column-name><column-value><![CDATA[");
		sb.append(getPrezzoIVA());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoLordo</column-name><column-value><![CDATA[");
		sb.append(getImportoLordo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoLordoIVA</column-name><column-value><![CDATA[");
		sb.append(getImportoLordoIVA());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sconto1</column-name><column-value><![CDATA[");
		sb.append(getSconto1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sconto2</column-name><column-value><![CDATA[");
		sb.append(getSconto2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sconto3</column-name><column-value><![CDATA[");
		sb.append(getSconto3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoNettoRigo</column-name><column-value><![CDATA[");
		sb.append(getImportoNettoRigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoNettoRigoIVA</column-name><column-value><![CDATA[");
		sb.append(getImportoNettoRigoIVA());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoNetto</column-name><column-value><![CDATA[");
		sb.append(getImportoNetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoNettoIVA</column-name><column-value><![CDATA[");
		sb.append(getImportoNettoIVA());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoCONAIAddebitatoInt</column-name><column-value><![CDATA[");
		sb.append(getImportoCONAIAddebitatoInt());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoCONAIAddebitatoOrig</column-name><column-value><![CDATA[");
		sb.append(getImportoCONAIAddebitatoOrig());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVAFatturazione</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVAFatturazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nomenclaturaCombinata</column-name><column-value><![CDATA[");
		sb.append(getNomenclaturaCombinata());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>testStampaDisBase</column-name><column-value><![CDATA[");
		sb.append(getTestStampaDisBase());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoOrdineEvaso</column-name><column-value><![CDATA[");
		sb.append(getTipoOrdineEvaso());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>annoOrdineEvaso</column-name><column-value><![CDATA[");
		sb.append(getAnnoOrdineEvaso());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroOrdineEvaso</column-name><column-value><![CDATA[");
		sb.append(getNumeroOrdineEvaso());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroRigoOrdineEvaso</column-name><column-value><![CDATA[");
		sb.append(getNumeroRigoOrdineEvaso());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoEvasione</column-name><column-value><![CDATA[");
		sb.append(getTipoEvasione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizioneRiferimento</column-name><column-value><![CDATA[");
		sb.append(getDescrizioneRiferimento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroPrimaNotaMagazzino</column-name><column-value><![CDATA[");
		sb.append(getNumeroPrimaNotaMagazzino());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroMovimentoMagazzino</column-name><column-value><![CDATA[");
		sb.append(getNumeroMovimentoMagazzino());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoEsplosione</column-name><column-value><![CDATA[");
		sb.append(getTipoEsplosione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>livelloMassimoEsplosione</column-name><column-value><![CDATA[");
		sb.append(getLivelloMassimoEsplosione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr1</column-name><column-value><![CDATA[");
		sb.append(getLibStr1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr2</column-name><column-value><![CDATA[");
		sb.append(getLibStr2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr3</column-name><column-value><![CDATA[");
		sb.append(getLibStr3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl1</column-name><column-value><![CDATA[");
		sb.append(getLibDbl1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl2</column-name><column-value><![CDATA[");
		sb.append(getLibDbl2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl3</column-name><column-value><![CDATA[");
		sb.append(getLibDbl3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng1</column-name><column-value><![CDATA[");
		sb.append(getLibLng1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng2</column-name><column-value><![CDATA[");
		sb.append(getLibLng2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng3</column-name><column-value><![CDATA[");
		sb.append(getLibLng3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat1</column-name><column-value><![CDATA[");
		sb.append(getLibDat1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat2</column-name><column-value><![CDATA[");
		sb.append(getLibDat2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat3</column-name><column-value><![CDATA[");
		sb.append(getLibDat3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>testFatturazioneAuto</column-name><column-value><![CDATA[");
		sb.append(getTestFatturazioneAuto());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _anno;
	private String _codiceAttivita;
	private String _codiceCentro;
	private int _numeroProtocollo;
	private int _numeroRigo;
	private boolean _statoRigo;
	private int _annoBolla;
	private int _tipoIdentificativoBolla;
	private String _tipoBolla;
	private String _codiceDeposito;
	private int _numeroBollettario;
	private int _numeroBolla;
	private Date _dataBollaEvasa;
	private int _rigoDDT;
	private int _tipoRigoDDT;
	private String _codiceCausaleMagazzino;
	private String _codiceDepositoMagazzino;
	private String _codiceArticolo;
	private String _codiceVariante;
	private String _codiceTestoDescrizioni;
	private String _descrizione;
	private String _unitaMisura;
	private int _numeroDecimalliQuant;
	private double _quantita1;
	private double _quantita2;
	private double _quantita3;
	private double _quantita;
	private String _unitaMisura2;
	private double _quantitaUnitMisSec;
	private int _numeroDecimalliPrezzo;
	private double _prezzo;
	private double _prezzoIVA;
	private double _importoLordo;
	private double _importoLordoIVA;
	private double _sconto1;
	private double _sconto2;
	private double _sconto3;
	private double _importoNettoRigo;
	private double _importoNettoRigoIVA;
	private double _importoNetto;
	private double _importoNettoIVA;
	private double _importoCONAIAddebitatoInt;
	private double _importoCONAIAddebitatoOrig;
	private String _codiceIVAFatturazione;
	private int _nomenclaturaCombinata;
	private boolean _testStampaDisBase;
	private int _tipoOrdineEvaso;
	private int _annoOrdineEvaso;
	private int _numeroOrdineEvaso;
	private int _numeroRigoOrdineEvaso;
	private String _tipoEvasione;
	private String _descrizioneRiferimento;
	private int _numeroPrimaNotaMagazzino;
	private int _numeroMovimentoMagazzino;
	private int _tipoEsplosione;
	private int _livelloMassimoEsplosione;
	private String _libStr1;
	private String _libStr2;
	private String _libStr3;
	private double _libDbl1;
	private double _libDbl2;
	private double _libDbl3;
	private long _libLng1;
	private long _libLng2;
	private long _libLng3;
	private Date _libDat1;
	private Date _libDat2;
	private Date _libDat3;
	private boolean _testFatturazioneAuto;
	private BaseModel<?> _righeFattureVeditaRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}