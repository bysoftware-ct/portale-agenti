/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ArticoliLocalServiceUtil;
import it.bysoftware.ct.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class ArticoliClp extends BaseModelImpl<Articoli> implements Articoli {
	public ArticoliClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Articoli.class;
	}

	@Override
	public String getModelClassName() {
		return Articoli.class.getName();
	}

	@Override
	public String getPrimaryKey() {
		return _codiceArticolo;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setCodiceArticolo(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _codiceArticolo;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("descrizione", getDescrizione());
		attributes.put("descrizioneAggiuntiva", getDescrizioneAggiuntiva());
		attributes.put("descrizioneBreveFiscale", getDescrizioneBreveFiscale());
		attributes.put("pathImmagine", getPathImmagine());
		attributes.put("unitaMisura", getUnitaMisura());
		attributes.put("categoriaMerceologica", getCategoriaMerceologica());
		attributes.put("categoriaFiscale", getCategoriaFiscale());
		attributes.put("codiceIVA", getCodiceIVA());
		attributes.put("codiceProvvigioni", getCodiceProvvigioni());
		attributes.put("codiceSconto1", getCodiceSconto1());
		attributes.put("codiceSconto2", getCodiceSconto2());
		attributes.put("categoriaInventario", getCategoriaInventario());
		attributes.put("prezzo1", getPrezzo1());
		attributes.put("prezzo2", getPrezzo2());
		attributes.put("prezzo3", getPrezzo3());
		attributes.put("codiceIVAPrezzo1", getCodiceIVAPrezzo1());
		attributes.put("codiceIVAPrezzo2", getCodiceIVAPrezzo2());
		attributes.put("codiceIVAPrezzo3", getCodiceIVAPrezzo3());
		attributes.put("numeroDecimaliPrezzo", getNumeroDecimaliPrezzo());
		attributes.put("numeroDecimaliQuantita", getNumeroDecimaliQuantita());
		attributes.put("numeroMesiPerinvenduto", getNumeroMesiPerinvenduto());
		attributes.put("unitaMisura2", getUnitaMisura2());
		attributes.put("fattoreMoltiplicativo", getFattoreMoltiplicativo());
		attributes.put("fattoreDivisione", getFattoreDivisione());
		attributes.put("ragruppamentoLibero", getRagruppamentoLibero());
		attributes.put("tipoLivelloDistintaBase", getTipoLivelloDistintaBase());
		attributes.put("tipoCostoStatistico", getTipoCostoStatistico());
		attributes.put("tipoGestioneArticolo", getTipoGestioneArticolo());
		attributes.put("tipoApprovvigionamentoArticolo",
			getTipoApprovvigionamentoArticolo());
		attributes.put("nomenclaturaCombinata", getNomenclaturaCombinata());
		attributes.put("descrizioneEstesa", getDescrizioneEstesa());
		attributes.put("generazioneMovimenti", getGenerazioneMovimenti());
		attributes.put("tipoEsplosioneDistintaBase",
			getTipoEsplosioneDistintaBase());
		attributes.put("livelloMaxEsplosioneDistintaBase",
			getLivelloMaxEsplosioneDistintaBase());
		attributes.put("valorizzazioneMagazzino", getValorizzazioneMagazzino());
		attributes.put("abilitatoEC", getAbilitatoEC());
		attributes.put("categoriaEC", getCategoriaEC());
		attributes.put("quantitaMinimaEC", getQuantitaMinimaEC());
		attributes.put("quantitaDefaultEC", getQuantitaDefaultEC());
		attributes.put("codiceSchedaTecnicaEC", getCodiceSchedaTecnicaEC());
		attributes.put("giorniPrevistaConsegna", getGiorniPrevistaConsegna());
		attributes.put("gestioneLotti", getGestioneLotti());
		attributes.put("creazioneLotti", getCreazioneLotti());
		attributes.put("prefissoCodiceLottoAutomatico",
			getPrefissoCodiceLottoAutomatico());
		attributes.put("numeroCifreProgressivo", getNumeroCifreProgressivo());
		attributes.put("scaricoAutomaticoLotti", getScaricoAutomaticoLotti());
		attributes.put("giorniInizioValiditaLotto",
			getGiorniInizioValiditaLotto());
		attributes.put("giorniValiditaLotto", getGiorniValiditaLotto());
		attributes.put("giorniPreavvisoScadenzaLotto",
			getGiorniPreavvisoScadenzaLotto());
		attributes.put("fattoreMoltQuantRiservata",
			getFattoreMoltQuantRiservata());
		attributes.put("fattoreDivQuantRiservata", getFattoreDivQuantRiservata());
		attributes.put("obsoleto", getObsoleto());
		attributes.put("comeImballo", getComeImballo());
		attributes.put("fattoreMoltCalcoloImballo",
			getFattoreMoltCalcoloImballo());
		attributes.put("fattoreDivCalcoloImballo", getFattoreDivCalcoloImballo());
		attributes.put("pesoLordo", getPesoLordo());
		attributes.put("tara", getTara());
		attributes.put("calcoloVolume", getCalcoloVolume());
		attributes.put("lunghezza", getLunghezza());
		attributes.put("larghezza", getLarghezza());
		attributes.put("profondita", getProfondita());
		attributes.put("codiceImballo", getCodiceImballo());
		attributes.put("fattoreMoltUnitMisSupp", getFattoreMoltUnitMisSupp());
		attributes.put("fattoreDivUnitMisSupp", getFattoreDivUnitMisSupp());
		attributes.put("stampaetichette", getStampaetichette());
		attributes.put("gestioneVarianti", getGestioneVarianti());
		attributes.put("gestioneVariantiDistintaBase",
			getGestioneVariantiDistintaBase());
		attributes.put("liberoString1", getLiberoString1());
		attributes.put("liberoString2", getLiberoString2());
		attributes.put("liberoString3", getLiberoString3());
		attributes.put("liberoString4", getLiberoString4());
		attributes.put("liberoString5", getLiberoString5());
		attributes.put("liberoDate1", getLiberoDate1());
		attributes.put("liberoDate2", getLiberoDate2());
		attributes.put("liberoDate3", getLiberoDate3());
		attributes.put("liberoDate4", getLiberoDate4());
		attributes.put("liberoDate5", getLiberoDate5());
		attributes.put("liberoLong1", getLiberoLong1());
		attributes.put("liberoLong2", getLiberoLong2());
		attributes.put("liberoLong3", getLiberoLong3());
		attributes.put("liberoLong4", getLiberoLong4());
		attributes.put("liberoLong5", getLiberoLong5());
		attributes.put("liberoDouble1", getLiberoDouble1());
		attributes.put("liberoDouble2", getLiberoDouble2());
		attributes.put("liberoDouble3", getLiberoDouble3());
		attributes.put("liberoDouble4", getLiberoDouble4());
		attributes.put("liberoDouble5", getLiberoDouble5());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		String descrizioneAggiuntiva = (String)attributes.get(
				"descrizioneAggiuntiva");

		if (descrizioneAggiuntiva != null) {
			setDescrizioneAggiuntiva(descrizioneAggiuntiva);
		}

		String descrizioneBreveFiscale = (String)attributes.get(
				"descrizioneBreveFiscale");

		if (descrizioneBreveFiscale != null) {
			setDescrizioneBreveFiscale(descrizioneBreveFiscale);
		}

		String pathImmagine = (String)attributes.get("pathImmagine");

		if (pathImmagine != null) {
			setPathImmagine(pathImmagine);
		}

		String unitaMisura = (String)attributes.get("unitaMisura");

		if (unitaMisura != null) {
			setUnitaMisura(unitaMisura);
		}

		String categoriaMerceologica = (String)attributes.get(
				"categoriaMerceologica");

		if (categoriaMerceologica != null) {
			setCategoriaMerceologica(categoriaMerceologica);
		}

		String categoriaFiscale = (String)attributes.get("categoriaFiscale");

		if (categoriaFiscale != null) {
			setCategoriaFiscale(categoriaFiscale);
		}

		String codiceIVA = (String)attributes.get("codiceIVA");

		if (codiceIVA != null) {
			setCodiceIVA(codiceIVA);
		}

		String codiceProvvigioni = (String)attributes.get("codiceProvvigioni");

		if (codiceProvvigioni != null) {
			setCodiceProvvigioni(codiceProvvigioni);
		}

		String codiceSconto1 = (String)attributes.get("codiceSconto1");

		if (codiceSconto1 != null) {
			setCodiceSconto1(codiceSconto1);
		}

		String codiceSconto2 = (String)attributes.get("codiceSconto2");

		if (codiceSconto2 != null) {
			setCodiceSconto2(codiceSconto2);
		}

		String categoriaInventario = (String)attributes.get(
				"categoriaInventario");

		if (categoriaInventario != null) {
			setCategoriaInventario(categoriaInventario);
		}

		Double prezzo1 = (Double)attributes.get("prezzo1");

		if (prezzo1 != null) {
			setPrezzo1(prezzo1);
		}

		Double prezzo2 = (Double)attributes.get("prezzo2");

		if (prezzo2 != null) {
			setPrezzo2(prezzo2);
		}

		Double prezzo3 = (Double)attributes.get("prezzo3");

		if (prezzo3 != null) {
			setPrezzo3(prezzo3);
		}

		String codiceIVAPrezzo1 = (String)attributes.get("codiceIVAPrezzo1");

		if (codiceIVAPrezzo1 != null) {
			setCodiceIVAPrezzo1(codiceIVAPrezzo1);
		}

		String codiceIVAPrezzo2 = (String)attributes.get("codiceIVAPrezzo2");

		if (codiceIVAPrezzo2 != null) {
			setCodiceIVAPrezzo2(codiceIVAPrezzo2);
		}

		String codiceIVAPrezzo3 = (String)attributes.get("codiceIVAPrezzo3");

		if (codiceIVAPrezzo3 != null) {
			setCodiceIVAPrezzo3(codiceIVAPrezzo3);
		}

		Integer numeroDecimaliPrezzo = (Integer)attributes.get(
				"numeroDecimaliPrezzo");

		if (numeroDecimaliPrezzo != null) {
			setNumeroDecimaliPrezzo(numeroDecimaliPrezzo);
		}

		Integer numeroDecimaliQuantita = (Integer)attributes.get(
				"numeroDecimaliQuantita");

		if (numeroDecimaliQuantita != null) {
			setNumeroDecimaliQuantita(numeroDecimaliQuantita);
		}

		Integer numeroMesiPerinvenduto = (Integer)attributes.get(
				"numeroMesiPerinvenduto");

		if (numeroMesiPerinvenduto != null) {
			setNumeroMesiPerinvenduto(numeroMesiPerinvenduto);
		}

		String unitaMisura2 = (String)attributes.get("unitaMisura2");

		if (unitaMisura2 != null) {
			setUnitaMisura2(unitaMisura2);
		}

		Double fattoreMoltiplicativo = (Double)attributes.get(
				"fattoreMoltiplicativo");

		if (fattoreMoltiplicativo != null) {
			setFattoreMoltiplicativo(fattoreMoltiplicativo);
		}

		Double fattoreDivisione = (Double)attributes.get("fattoreDivisione");

		if (fattoreDivisione != null) {
			setFattoreDivisione(fattoreDivisione);
		}

		String ragruppamentoLibero = (String)attributes.get(
				"ragruppamentoLibero");

		if (ragruppamentoLibero != null) {
			setRagruppamentoLibero(ragruppamentoLibero);
		}

		Integer tipoLivelloDistintaBase = (Integer)attributes.get(
				"tipoLivelloDistintaBase");

		if (tipoLivelloDistintaBase != null) {
			setTipoLivelloDistintaBase(tipoLivelloDistintaBase);
		}

		Integer tipoCostoStatistico = (Integer)attributes.get(
				"tipoCostoStatistico");

		if (tipoCostoStatistico != null) {
			setTipoCostoStatistico(tipoCostoStatistico);
		}

		Integer tipoGestioneArticolo = (Integer)attributes.get(
				"tipoGestioneArticolo");

		if (tipoGestioneArticolo != null) {
			setTipoGestioneArticolo(tipoGestioneArticolo);
		}

		Integer tipoApprovvigionamentoArticolo = (Integer)attributes.get(
				"tipoApprovvigionamentoArticolo");

		if (tipoApprovvigionamentoArticolo != null) {
			setTipoApprovvigionamentoArticolo(tipoApprovvigionamentoArticolo);
		}

		Double nomenclaturaCombinata = (Double)attributes.get(
				"nomenclaturaCombinata");

		if (nomenclaturaCombinata != null) {
			setNomenclaturaCombinata(nomenclaturaCombinata);
		}

		String descrizioneEstesa = (String)attributes.get("descrizioneEstesa");

		if (descrizioneEstesa != null) {
			setDescrizioneEstesa(descrizioneEstesa);
		}

		Boolean generazioneMovimenti = (Boolean)attributes.get(
				"generazioneMovimenti");

		if (generazioneMovimenti != null) {
			setGenerazioneMovimenti(generazioneMovimenti);
		}

		Integer tipoEsplosioneDistintaBase = (Integer)attributes.get(
				"tipoEsplosioneDistintaBase");

		if (tipoEsplosioneDistintaBase != null) {
			setTipoEsplosioneDistintaBase(tipoEsplosioneDistintaBase);
		}

		Integer livelloMaxEsplosioneDistintaBase = (Integer)attributes.get(
				"livelloMaxEsplosioneDistintaBase");

		if (livelloMaxEsplosioneDistintaBase != null) {
			setLivelloMaxEsplosioneDistintaBase(livelloMaxEsplosioneDistintaBase);
		}

		Boolean valorizzazioneMagazzino = (Boolean)attributes.get(
				"valorizzazioneMagazzino");

		if (valorizzazioneMagazzino != null) {
			setValorizzazioneMagazzino(valorizzazioneMagazzino);
		}

		Boolean abilitatoEC = (Boolean)attributes.get("abilitatoEC");

		if (abilitatoEC != null) {
			setAbilitatoEC(abilitatoEC);
		}

		String categoriaEC = (String)attributes.get("categoriaEC");

		if (categoriaEC != null) {
			setCategoriaEC(categoriaEC);
		}

		Double quantitaMinimaEC = (Double)attributes.get("quantitaMinimaEC");

		if (quantitaMinimaEC != null) {
			setQuantitaMinimaEC(quantitaMinimaEC);
		}

		Double quantitaDefaultEC = (Double)attributes.get("quantitaDefaultEC");

		if (quantitaDefaultEC != null) {
			setQuantitaDefaultEC(quantitaDefaultEC);
		}

		String codiceSchedaTecnicaEC = (String)attributes.get(
				"codiceSchedaTecnicaEC");

		if (codiceSchedaTecnicaEC != null) {
			setCodiceSchedaTecnicaEC(codiceSchedaTecnicaEC);
		}

		Integer giorniPrevistaConsegna = (Integer)attributes.get(
				"giorniPrevistaConsegna");

		if (giorniPrevistaConsegna != null) {
			setGiorniPrevistaConsegna(giorniPrevistaConsegna);
		}

		Boolean gestioneLotti = (Boolean)attributes.get("gestioneLotti");

		if (gestioneLotti != null) {
			setGestioneLotti(gestioneLotti);
		}

		Boolean creazioneLotti = (Boolean)attributes.get("creazioneLotti");

		if (creazioneLotti != null) {
			setCreazioneLotti(creazioneLotti);
		}

		String prefissoCodiceLottoAutomatico = (String)attributes.get(
				"prefissoCodiceLottoAutomatico");

		if (prefissoCodiceLottoAutomatico != null) {
			setPrefissoCodiceLottoAutomatico(prefissoCodiceLottoAutomatico);
		}

		Integer numeroCifreProgressivo = (Integer)attributes.get(
				"numeroCifreProgressivo");

		if (numeroCifreProgressivo != null) {
			setNumeroCifreProgressivo(numeroCifreProgressivo);
		}

		Boolean scaricoAutomaticoLotti = (Boolean)attributes.get(
				"scaricoAutomaticoLotti");

		if (scaricoAutomaticoLotti != null) {
			setScaricoAutomaticoLotti(scaricoAutomaticoLotti);
		}

		Integer giorniInizioValiditaLotto = (Integer)attributes.get(
				"giorniInizioValiditaLotto");

		if (giorniInizioValiditaLotto != null) {
			setGiorniInizioValiditaLotto(giorniInizioValiditaLotto);
		}

		Integer giorniValiditaLotto = (Integer)attributes.get(
				"giorniValiditaLotto");

		if (giorniValiditaLotto != null) {
			setGiorniValiditaLotto(giorniValiditaLotto);
		}

		Integer giorniPreavvisoScadenzaLotto = (Integer)attributes.get(
				"giorniPreavvisoScadenzaLotto");

		if (giorniPreavvisoScadenzaLotto != null) {
			setGiorniPreavvisoScadenzaLotto(giorniPreavvisoScadenzaLotto);
		}

		Double fattoreMoltQuantRiservata = (Double)attributes.get(
				"fattoreMoltQuantRiservata");

		if (fattoreMoltQuantRiservata != null) {
			setFattoreMoltQuantRiservata(fattoreMoltQuantRiservata);
		}

		Double fattoreDivQuantRiservata = (Double)attributes.get(
				"fattoreDivQuantRiservata");

		if (fattoreDivQuantRiservata != null) {
			setFattoreDivQuantRiservata(fattoreDivQuantRiservata);
		}

		Boolean obsoleto = (Boolean)attributes.get("obsoleto");

		if (obsoleto != null) {
			setObsoleto(obsoleto);
		}

		Boolean comeImballo = (Boolean)attributes.get("comeImballo");

		if (comeImballo != null) {
			setComeImballo(comeImballo);
		}

		Double fattoreMoltCalcoloImballo = (Double)attributes.get(
				"fattoreMoltCalcoloImballo");

		if (fattoreMoltCalcoloImballo != null) {
			setFattoreMoltCalcoloImballo(fattoreMoltCalcoloImballo);
		}

		Double fattoreDivCalcoloImballo = (Double)attributes.get(
				"fattoreDivCalcoloImballo");

		if (fattoreDivCalcoloImballo != null) {
			setFattoreDivCalcoloImballo(fattoreDivCalcoloImballo);
		}

		Double pesoLordo = (Double)attributes.get("pesoLordo");

		if (pesoLordo != null) {
			setPesoLordo(pesoLordo);
		}

		Double tara = (Double)attributes.get("tara");

		if (tara != null) {
			setTara(tara);
		}

		Integer calcoloVolume = (Integer)attributes.get("calcoloVolume");

		if (calcoloVolume != null) {
			setCalcoloVolume(calcoloVolume);
		}

		Double lunghezza = (Double)attributes.get("lunghezza");

		if (lunghezza != null) {
			setLunghezza(lunghezza);
		}

		Double larghezza = (Double)attributes.get("larghezza");

		if (larghezza != null) {
			setLarghezza(larghezza);
		}

		Double profondita = (Double)attributes.get("profondita");

		if (profondita != null) {
			setProfondita(profondita);
		}

		String codiceImballo = (String)attributes.get("codiceImballo");

		if (codiceImballo != null) {
			setCodiceImballo(codiceImballo);
		}

		Double fattoreMoltUnitMisSupp = (Double)attributes.get(
				"fattoreMoltUnitMisSupp");

		if (fattoreMoltUnitMisSupp != null) {
			setFattoreMoltUnitMisSupp(fattoreMoltUnitMisSupp);
		}

		Double fattoreDivUnitMisSupp = (Double)attributes.get(
				"fattoreDivUnitMisSupp");

		if (fattoreDivUnitMisSupp != null) {
			setFattoreDivUnitMisSupp(fattoreDivUnitMisSupp);
		}

		Integer stampaetichette = (Integer)attributes.get("stampaetichette");

		if (stampaetichette != null) {
			setStampaetichette(stampaetichette);
		}

		Boolean gestioneVarianti = (Boolean)attributes.get("gestioneVarianti");

		if (gestioneVarianti != null) {
			setGestioneVarianti(gestioneVarianti);
		}

		Boolean gestioneVariantiDistintaBase = (Boolean)attributes.get(
				"gestioneVariantiDistintaBase");

		if (gestioneVariantiDistintaBase != null) {
			setGestioneVariantiDistintaBase(gestioneVariantiDistintaBase);
		}

		String liberoString1 = (String)attributes.get("liberoString1");

		if (liberoString1 != null) {
			setLiberoString1(liberoString1);
		}

		String liberoString2 = (String)attributes.get("liberoString2");

		if (liberoString2 != null) {
			setLiberoString2(liberoString2);
		}

		String liberoString3 = (String)attributes.get("liberoString3");

		if (liberoString3 != null) {
			setLiberoString3(liberoString3);
		}

		String liberoString4 = (String)attributes.get("liberoString4");

		if (liberoString4 != null) {
			setLiberoString4(liberoString4);
		}

		String liberoString5 = (String)attributes.get("liberoString5");

		if (liberoString5 != null) {
			setLiberoString5(liberoString5);
		}

		Date liberoDate1 = (Date)attributes.get("liberoDate1");

		if (liberoDate1 != null) {
			setLiberoDate1(liberoDate1);
		}

		Date liberoDate2 = (Date)attributes.get("liberoDate2");

		if (liberoDate2 != null) {
			setLiberoDate2(liberoDate2);
		}

		Date liberoDate3 = (Date)attributes.get("liberoDate3");

		if (liberoDate3 != null) {
			setLiberoDate3(liberoDate3);
		}

		Date liberoDate4 = (Date)attributes.get("liberoDate4");

		if (liberoDate4 != null) {
			setLiberoDate4(liberoDate4);
		}

		Date liberoDate5 = (Date)attributes.get("liberoDate5");

		if (liberoDate5 != null) {
			setLiberoDate5(liberoDate5);
		}

		Long liberoLong1 = (Long)attributes.get("liberoLong1");

		if (liberoLong1 != null) {
			setLiberoLong1(liberoLong1);
		}

		Long liberoLong2 = (Long)attributes.get("liberoLong2");

		if (liberoLong2 != null) {
			setLiberoLong2(liberoLong2);
		}

		Long liberoLong3 = (Long)attributes.get("liberoLong3");

		if (liberoLong3 != null) {
			setLiberoLong3(liberoLong3);
		}

		Long liberoLong4 = (Long)attributes.get("liberoLong4");

		if (liberoLong4 != null) {
			setLiberoLong4(liberoLong4);
		}

		Long liberoLong5 = (Long)attributes.get("liberoLong5");

		if (liberoLong5 != null) {
			setLiberoLong5(liberoLong5);
		}

		Double liberoDouble1 = (Double)attributes.get("liberoDouble1");

		if (liberoDouble1 != null) {
			setLiberoDouble1(liberoDouble1);
		}

		Double liberoDouble2 = (Double)attributes.get("liberoDouble2");

		if (liberoDouble2 != null) {
			setLiberoDouble2(liberoDouble2);
		}

		Double liberoDouble3 = (Double)attributes.get("liberoDouble3");

		if (liberoDouble3 != null) {
			setLiberoDouble3(liberoDouble3);
		}

		Double liberoDouble4 = (Double)attributes.get("liberoDouble4");

		if (liberoDouble4 != null) {
			setLiberoDouble4(liberoDouble4);
		}

		Double liberoDouble5 = (Double)attributes.get("liberoDouble5");

		if (liberoDouble5 != null) {
			setLiberoDouble5(liberoDouble5);
		}
	}

	@Override
	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	@Override
	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceArticolo",
						String.class);

				method.invoke(_articoliRemoteModel, codiceArticolo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizione() {
		return _descrizione;
	}

	@Override
	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizione", String.class);

				method.invoke(_articoliRemoteModel, descrizione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizioneAggiuntiva() {
		return _descrizioneAggiuntiva;
	}

	@Override
	public void setDescrizioneAggiuntiva(String descrizioneAggiuntiva) {
		_descrizioneAggiuntiva = descrizioneAggiuntiva;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizioneAggiuntiva",
						String.class);

				method.invoke(_articoliRemoteModel, descrizioneAggiuntiva);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizioneBreveFiscale() {
		return _descrizioneBreveFiscale;
	}

	@Override
	public void setDescrizioneBreveFiscale(String descrizioneBreveFiscale) {
		_descrizioneBreveFiscale = descrizioneBreveFiscale;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizioneBreveFiscale",
						String.class);

				method.invoke(_articoliRemoteModel, descrizioneBreveFiscale);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPathImmagine() {
		return _pathImmagine;
	}

	@Override
	public void setPathImmagine(String pathImmagine) {
		_pathImmagine = pathImmagine;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setPathImmagine", String.class);

				method.invoke(_articoliRemoteModel, pathImmagine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUnitaMisura() {
		return _unitaMisura;
	}

	@Override
	public void setUnitaMisura(String unitaMisura) {
		_unitaMisura = unitaMisura;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setUnitaMisura", String.class);

				method.invoke(_articoliRemoteModel, unitaMisura);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCategoriaMerceologica() {
		return _categoriaMerceologica;
	}

	@Override
	public void setCategoriaMerceologica(String categoriaMerceologica) {
		_categoriaMerceologica = categoriaMerceologica;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCategoriaMerceologica",
						String.class);

				method.invoke(_articoliRemoteModel, categoriaMerceologica);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCategoriaFiscale() {
		return _categoriaFiscale;
	}

	@Override
	public void setCategoriaFiscale(String categoriaFiscale) {
		_categoriaFiscale = categoriaFiscale;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCategoriaFiscale",
						String.class);

				method.invoke(_articoliRemoteModel, categoriaFiscale);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVA() {
		return _codiceIVA;
	}

	@Override
	public void setCodiceIVA(String codiceIVA) {
		_codiceIVA = codiceIVA;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVA", String.class);

				method.invoke(_articoliRemoteModel, codiceIVA);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceProvvigioni() {
		return _codiceProvvigioni;
	}

	@Override
	public void setCodiceProvvigioni(String codiceProvvigioni) {
		_codiceProvvigioni = codiceProvvigioni;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceProvvigioni",
						String.class);

				method.invoke(_articoliRemoteModel, codiceProvvigioni);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceSconto1() {
		return _codiceSconto1;
	}

	@Override
	public void setCodiceSconto1(String codiceSconto1) {
		_codiceSconto1 = codiceSconto1;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceSconto1", String.class);

				method.invoke(_articoliRemoteModel, codiceSconto1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceSconto2() {
		return _codiceSconto2;
	}

	@Override
	public void setCodiceSconto2(String codiceSconto2) {
		_codiceSconto2 = codiceSconto2;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceSconto2", String.class);

				method.invoke(_articoliRemoteModel, codiceSconto2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCategoriaInventario() {
		return _categoriaInventario;
	}

	@Override
	public void setCategoriaInventario(String categoriaInventario) {
		_categoriaInventario = categoriaInventario;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCategoriaInventario",
						String.class);

				method.invoke(_articoliRemoteModel, categoriaInventario);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPrezzo1() {
		return _prezzo1;
	}

	@Override
	public void setPrezzo1(double prezzo1) {
		_prezzo1 = prezzo1;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setPrezzo1", double.class);

				method.invoke(_articoliRemoteModel, prezzo1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPrezzo2() {
		return _prezzo2;
	}

	@Override
	public void setPrezzo2(double prezzo2) {
		_prezzo2 = prezzo2;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setPrezzo2", double.class);

				method.invoke(_articoliRemoteModel, prezzo2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPrezzo3() {
		return _prezzo3;
	}

	@Override
	public void setPrezzo3(double prezzo3) {
		_prezzo3 = prezzo3;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setPrezzo3", double.class);

				method.invoke(_articoliRemoteModel, prezzo3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVAPrezzo1() {
		return _codiceIVAPrezzo1;
	}

	@Override
	public void setCodiceIVAPrezzo1(String codiceIVAPrezzo1) {
		_codiceIVAPrezzo1 = codiceIVAPrezzo1;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVAPrezzo1",
						String.class);

				method.invoke(_articoliRemoteModel, codiceIVAPrezzo1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVAPrezzo2() {
		return _codiceIVAPrezzo2;
	}

	@Override
	public void setCodiceIVAPrezzo2(String codiceIVAPrezzo2) {
		_codiceIVAPrezzo2 = codiceIVAPrezzo2;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVAPrezzo2",
						String.class);

				method.invoke(_articoliRemoteModel, codiceIVAPrezzo2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVAPrezzo3() {
		return _codiceIVAPrezzo3;
	}

	@Override
	public void setCodiceIVAPrezzo3(String codiceIVAPrezzo3) {
		_codiceIVAPrezzo3 = codiceIVAPrezzo3;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVAPrezzo3",
						String.class);

				method.invoke(_articoliRemoteModel, codiceIVAPrezzo3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroDecimaliPrezzo() {
		return _numeroDecimaliPrezzo;
	}

	@Override
	public void setNumeroDecimaliPrezzo(int numeroDecimaliPrezzo) {
		_numeroDecimaliPrezzo = numeroDecimaliPrezzo;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroDecimaliPrezzo",
						int.class);

				method.invoke(_articoliRemoteModel, numeroDecimaliPrezzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroDecimaliQuantita() {
		return _numeroDecimaliQuantita;
	}

	@Override
	public void setNumeroDecimaliQuantita(int numeroDecimaliQuantita) {
		_numeroDecimaliQuantita = numeroDecimaliQuantita;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroDecimaliQuantita",
						int.class);

				method.invoke(_articoliRemoteModel, numeroDecimaliQuantita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroMesiPerinvenduto() {
		return _numeroMesiPerinvenduto;
	}

	@Override
	public void setNumeroMesiPerinvenduto(int numeroMesiPerinvenduto) {
		_numeroMesiPerinvenduto = numeroMesiPerinvenduto;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroMesiPerinvenduto",
						int.class);

				method.invoke(_articoliRemoteModel, numeroMesiPerinvenduto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUnitaMisura2() {
		return _unitaMisura2;
	}

	@Override
	public void setUnitaMisura2(String unitaMisura2) {
		_unitaMisura2 = unitaMisura2;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setUnitaMisura2", String.class);

				method.invoke(_articoliRemoteModel, unitaMisura2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getFattoreMoltiplicativo() {
		return _fattoreMoltiplicativo;
	}

	@Override
	public void setFattoreMoltiplicativo(double fattoreMoltiplicativo) {
		_fattoreMoltiplicativo = fattoreMoltiplicativo;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setFattoreMoltiplicativo",
						double.class);

				method.invoke(_articoliRemoteModel, fattoreMoltiplicativo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getFattoreDivisione() {
		return _fattoreDivisione;
	}

	@Override
	public void setFattoreDivisione(double fattoreDivisione) {
		_fattoreDivisione = fattoreDivisione;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setFattoreDivisione",
						double.class);

				method.invoke(_articoliRemoteModel, fattoreDivisione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRagruppamentoLibero() {
		return _ragruppamentoLibero;
	}

	@Override
	public void setRagruppamentoLibero(String ragruppamentoLibero) {
		_ragruppamentoLibero = ragruppamentoLibero;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setRagruppamentoLibero",
						String.class);

				method.invoke(_articoliRemoteModel, ragruppamentoLibero);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoLivelloDistintaBase() {
		return _tipoLivelloDistintaBase;
	}

	@Override
	public void setTipoLivelloDistintaBase(int tipoLivelloDistintaBase) {
		_tipoLivelloDistintaBase = tipoLivelloDistintaBase;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoLivelloDistintaBase",
						int.class);

				method.invoke(_articoliRemoteModel, tipoLivelloDistintaBase);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoCostoStatistico() {
		return _tipoCostoStatistico;
	}

	@Override
	public void setTipoCostoStatistico(int tipoCostoStatistico) {
		_tipoCostoStatistico = tipoCostoStatistico;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoCostoStatistico",
						int.class);

				method.invoke(_articoliRemoteModel, tipoCostoStatistico);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoGestioneArticolo() {
		return _tipoGestioneArticolo;
	}

	@Override
	public void setTipoGestioneArticolo(int tipoGestioneArticolo) {
		_tipoGestioneArticolo = tipoGestioneArticolo;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoGestioneArticolo",
						int.class);

				method.invoke(_articoliRemoteModel, tipoGestioneArticolo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoApprovvigionamentoArticolo() {
		return _tipoApprovvigionamentoArticolo;
	}

	@Override
	public void setTipoApprovvigionamentoArticolo(
		int tipoApprovvigionamentoArticolo) {
		_tipoApprovvigionamentoArticolo = tipoApprovvigionamentoArticolo;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoApprovvigionamentoArticolo",
						int.class);

				method.invoke(_articoliRemoteModel,
					tipoApprovvigionamentoArticolo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getNomenclaturaCombinata() {
		return _nomenclaturaCombinata;
	}

	@Override
	public void setNomenclaturaCombinata(double nomenclaturaCombinata) {
		_nomenclaturaCombinata = nomenclaturaCombinata;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setNomenclaturaCombinata",
						double.class);

				method.invoke(_articoliRemoteModel, nomenclaturaCombinata);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizioneEstesa() {
		return _descrizioneEstesa;
	}

	@Override
	public void setDescrizioneEstesa(String descrizioneEstesa) {
		_descrizioneEstesa = descrizioneEstesa;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizioneEstesa",
						String.class);

				method.invoke(_articoliRemoteModel, descrizioneEstesa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getGenerazioneMovimenti() {
		return _generazioneMovimenti;
	}

	@Override
	public boolean isGenerazioneMovimenti() {
		return _generazioneMovimenti;
	}

	@Override
	public void setGenerazioneMovimenti(boolean generazioneMovimenti) {
		_generazioneMovimenti = generazioneMovimenti;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setGenerazioneMovimenti",
						boolean.class);

				method.invoke(_articoliRemoteModel, generazioneMovimenti);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoEsplosioneDistintaBase() {
		return _tipoEsplosioneDistintaBase;
	}

	@Override
	public void setTipoEsplosioneDistintaBase(int tipoEsplosioneDistintaBase) {
		_tipoEsplosioneDistintaBase = tipoEsplosioneDistintaBase;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoEsplosioneDistintaBase",
						int.class);

				method.invoke(_articoliRemoteModel, tipoEsplosioneDistintaBase);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getLivelloMaxEsplosioneDistintaBase() {
		return _livelloMaxEsplosioneDistintaBase;
	}

	@Override
	public void setLivelloMaxEsplosioneDistintaBase(
		int livelloMaxEsplosioneDistintaBase) {
		_livelloMaxEsplosioneDistintaBase = livelloMaxEsplosioneDistintaBase;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLivelloMaxEsplosioneDistintaBase",
						int.class);

				method.invoke(_articoliRemoteModel,
					livelloMaxEsplosioneDistintaBase);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getValorizzazioneMagazzino() {
		return _valorizzazioneMagazzino;
	}

	@Override
	public boolean isValorizzazioneMagazzino() {
		return _valorizzazioneMagazzino;
	}

	@Override
	public void setValorizzazioneMagazzino(boolean valorizzazioneMagazzino) {
		_valorizzazioneMagazzino = valorizzazioneMagazzino;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setValorizzazioneMagazzino",
						boolean.class);

				method.invoke(_articoliRemoteModel, valorizzazioneMagazzino);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getAbilitatoEC() {
		return _abilitatoEC;
	}

	@Override
	public boolean isAbilitatoEC() {
		return _abilitatoEC;
	}

	@Override
	public void setAbilitatoEC(boolean abilitatoEC) {
		_abilitatoEC = abilitatoEC;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setAbilitatoEC", boolean.class);

				method.invoke(_articoliRemoteModel, abilitatoEC);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCategoriaEC() {
		return _categoriaEC;
	}

	@Override
	public void setCategoriaEC(String categoriaEC) {
		_categoriaEC = categoriaEC;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCategoriaEC", String.class);

				method.invoke(_articoliRemoteModel, categoriaEC);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaMinimaEC() {
		return _quantitaMinimaEC;
	}

	@Override
	public void setQuantitaMinimaEC(double quantitaMinimaEC) {
		_quantitaMinimaEC = quantitaMinimaEC;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaMinimaEC",
						double.class);

				method.invoke(_articoliRemoteModel, quantitaMinimaEC);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaDefaultEC() {
		return _quantitaDefaultEC;
	}

	@Override
	public void setQuantitaDefaultEC(double quantitaDefaultEC) {
		_quantitaDefaultEC = quantitaDefaultEC;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaDefaultEC",
						double.class);

				method.invoke(_articoliRemoteModel, quantitaDefaultEC);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceSchedaTecnicaEC() {
		return _codiceSchedaTecnicaEC;
	}

	@Override
	public void setCodiceSchedaTecnicaEC(String codiceSchedaTecnicaEC) {
		_codiceSchedaTecnicaEC = codiceSchedaTecnicaEC;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceSchedaTecnicaEC",
						String.class);

				method.invoke(_articoliRemoteModel, codiceSchedaTecnicaEC);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getGiorniPrevistaConsegna() {
		return _giorniPrevistaConsegna;
	}

	@Override
	public void setGiorniPrevistaConsegna(int giorniPrevistaConsegna) {
		_giorniPrevistaConsegna = giorniPrevistaConsegna;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setGiorniPrevistaConsegna",
						int.class);

				method.invoke(_articoliRemoteModel, giorniPrevistaConsegna);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getGestioneLotti() {
		return _gestioneLotti;
	}

	@Override
	public boolean isGestioneLotti() {
		return _gestioneLotti;
	}

	@Override
	public void setGestioneLotti(boolean gestioneLotti) {
		_gestioneLotti = gestioneLotti;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setGestioneLotti",
						boolean.class);

				method.invoke(_articoliRemoteModel, gestioneLotti);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getCreazioneLotti() {
		return _creazioneLotti;
	}

	@Override
	public boolean isCreazioneLotti() {
		return _creazioneLotti;
	}

	@Override
	public void setCreazioneLotti(boolean creazioneLotti) {
		_creazioneLotti = creazioneLotti;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCreazioneLotti",
						boolean.class);

				method.invoke(_articoliRemoteModel, creazioneLotti);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPrefissoCodiceLottoAutomatico() {
		return _prefissoCodiceLottoAutomatico;
	}

	@Override
	public void setPrefissoCodiceLottoAutomatico(
		String prefissoCodiceLottoAutomatico) {
		_prefissoCodiceLottoAutomatico = prefissoCodiceLottoAutomatico;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setPrefissoCodiceLottoAutomatico",
						String.class);

				method.invoke(_articoliRemoteModel,
					prefissoCodiceLottoAutomatico);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroCifreProgressivo() {
		return _numeroCifreProgressivo;
	}

	@Override
	public void setNumeroCifreProgressivo(int numeroCifreProgressivo) {
		_numeroCifreProgressivo = numeroCifreProgressivo;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroCifreProgressivo",
						int.class);

				method.invoke(_articoliRemoteModel, numeroCifreProgressivo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getScaricoAutomaticoLotti() {
		return _scaricoAutomaticoLotti;
	}

	@Override
	public boolean isScaricoAutomaticoLotti() {
		return _scaricoAutomaticoLotti;
	}

	@Override
	public void setScaricoAutomaticoLotti(boolean scaricoAutomaticoLotti) {
		_scaricoAutomaticoLotti = scaricoAutomaticoLotti;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setScaricoAutomaticoLotti",
						boolean.class);

				method.invoke(_articoliRemoteModel, scaricoAutomaticoLotti);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getGiorniInizioValiditaLotto() {
		return _giorniInizioValiditaLotto;
	}

	@Override
	public void setGiorniInizioValiditaLotto(int giorniInizioValiditaLotto) {
		_giorniInizioValiditaLotto = giorniInizioValiditaLotto;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setGiorniInizioValiditaLotto",
						int.class);

				method.invoke(_articoliRemoteModel, giorniInizioValiditaLotto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getGiorniValiditaLotto() {
		return _giorniValiditaLotto;
	}

	@Override
	public void setGiorniValiditaLotto(int giorniValiditaLotto) {
		_giorniValiditaLotto = giorniValiditaLotto;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setGiorniValiditaLotto",
						int.class);

				method.invoke(_articoliRemoteModel, giorniValiditaLotto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getGiorniPreavvisoScadenzaLotto() {
		return _giorniPreavvisoScadenzaLotto;
	}

	@Override
	public void setGiorniPreavvisoScadenzaLotto(
		int giorniPreavvisoScadenzaLotto) {
		_giorniPreavvisoScadenzaLotto = giorniPreavvisoScadenzaLotto;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setGiorniPreavvisoScadenzaLotto",
						int.class);

				method.invoke(_articoliRemoteModel, giorniPreavvisoScadenzaLotto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getFattoreMoltQuantRiservata() {
		return _fattoreMoltQuantRiservata;
	}

	@Override
	public void setFattoreMoltQuantRiservata(double fattoreMoltQuantRiservata) {
		_fattoreMoltQuantRiservata = fattoreMoltQuantRiservata;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setFattoreMoltQuantRiservata",
						double.class);

				method.invoke(_articoliRemoteModel, fattoreMoltQuantRiservata);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getFattoreDivQuantRiservata() {
		return _fattoreDivQuantRiservata;
	}

	@Override
	public void setFattoreDivQuantRiservata(double fattoreDivQuantRiservata) {
		_fattoreDivQuantRiservata = fattoreDivQuantRiservata;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setFattoreDivQuantRiservata",
						double.class);

				method.invoke(_articoliRemoteModel, fattoreDivQuantRiservata);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getObsoleto() {
		return _obsoleto;
	}

	@Override
	public boolean isObsoleto() {
		return _obsoleto;
	}

	@Override
	public void setObsoleto(boolean obsoleto) {
		_obsoleto = obsoleto;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setObsoleto", boolean.class);

				method.invoke(_articoliRemoteModel, obsoleto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getComeImballo() {
		return _comeImballo;
	}

	@Override
	public boolean isComeImballo() {
		return _comeImballo;
	}

	@Override
	public void setComeImballo(boolean comeImballo) {
		_comeImballo = comeImballo;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setComeImballo", boolean.class);

				method.invoke(_articoliRemoteModel, comeImballo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getFattoreMoltCalcoloImballo() {
		return _fattoreMoltCalcoloImballo;
	}

	@Override
	public void setFattoreMoltCalcoloImballo(double fattoreMoltCalcoloImballo) {
		_fattoreMoltCalcoloImballo = fattoreMoltCalcoloImballo;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setFattoreMoltCalcoloImballo",
						double.class);

				method.invoke(_articoliRemoteModel, fattoreMoltCalcoloImballo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getFattoreDivCalcoloImballo() {
		return _fattoreDivCalcoloImballo;
	}

	@Override
	public void setFattoreDivCalcoloImballo(double fattoreDivCalcoloImballo) {
		_fattoreDivCalcoloImballo = fattoreDivCalcoloImballo;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setFattoreDivCalcoloImballo",
						double.class);

				method.invoke(_articoliRemoteModel, fattoreDivCalcoloImballo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPesoLordo() {
		return _pesoLordo;
	}

	@Override
	public void setPesoLordo(double pesoLordo) {
		_pesoLordo = pesoLordo;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setPesoLordo", double.class);

				method.invoke(_articoliRemoteModel, pesoLordo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getTara() {
		return _tara;
	}

	@Override
	public void setTara(double tara) {
		_tara = tara;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setTara", double.class);

				method.invoke(_articoliRemoteModel, tara);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getCalcoloVolume() {
		return _calcoloVolume;
	}

	@Override
	public void setCalcoloVolume(int calcoloVolume) {
		_calcoloVolume = calcoloVolume;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCalcoloVolume", int.class);

				method.invoke(_articoliRemoteModel, calcoloVolume);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLunghezza() {
		return _lunghezza;
	}

	@Override
	public void setLunghezza(double lunghezza) {
		_lunghezza = lunghezza;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLunghezza", double.class);

				method.invoke(_articoliRemoteModel, lunghezza);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLarghezza() {
		return _larghezza;
	}

	@Override
	public void setLarghezza(double larghezza) {
		_larghezza = larghezza;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLarghezza", double.class);

				method.invoke(_articoliRemoteModel, larghezza);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getProfondita() {
		return _profondita;
	}

	@Override
	public void setProfondita(double profondita) {
		_profondita = profondita;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setProfondita", double.class);

				method.invoke(_articoliRemoteModel, profondita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceImballo() {
		return _codiceImballo;
	}

	@Override
	public void setCodiceImballo(String codiceImballo) {
		_codiceImballo = codiceImballo;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceImballo", String.class);

				method.invoke(_articoliRemoteModel, codiceImballo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getFattoreMoltUnitMisSupp() {
		return _fattoreMoltUnitMisSupp;
	}

	@Override
	public void setFattoreMoltUnitMisSupp(double fattoreMoltUnitMisSupp) {
		_fattoreMoltUnitMisSupp = fattoreMoltUnitMisSupp;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setFattoreMoltUnitMisSupp",
						double.class);

				method.invoke(_articoliRemoteModel, fattoreMoltUnitMisSupp);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getFattoreDivUnitMisSupp() {
		return _fattoreDivUnitMisSupp;
	}

	@Override
	public void setFattoreDivUnitMisSupp(double fattoreDivUnitMisSupp) {
		_fattoreDivUnitMisSupp = fattoreDivUnitMisSupp;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setFattoreDivUnitMisSupp",
						double.class);

				method.invoke(_articoliRemoteModel, fattoreDivUnitMisSupp);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getStampaetichette() {
		return _stampaetichette;
	}

	@Override
	public void setStampaetichette(int stampaetichette) {
		_stampaetichette = stampaetichette;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setStampaetichette", int.class);

				method.invoke(_articoliRemoteModel, stampaetichette);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getGestioneVarianti() {
		return _gestioneVarianti;
	}

	@Override
	public boolean isGestioneVarianti() {
		return _gestioneVarianti;
	}

	@Override
	public void setGestioneVarianti(boolean gestioneVarianti) {
		_gestioneVarianti = gestioneVarianti;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setGestioneVarianti",
						boolean.class);

				method.invoke(_articoliRemoteModel, gestioneVarianti);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getGestioneVariantiDistintaBase() {
		return _gestioneVariantiDistintaBase;
	}

	@Override
	public boolean isGestioneVariantiDistintaBase() {
		return _gestioneVariantiDistintaBase;
	}

	@Override
	public void setGestioneVariantiDistintaBase(
		boolean gestioneVariantiDistintaBase) {
		_gestioneVariantiDistintaBase = gestioneVariantiDistintaBase;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setGestioneVariantiDistintaBase",
						boolean.class);

				method.invoke(_articoliRemoteModel, gestioneVariantiDistintaBase);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLiberoString1() {
		return _liberoString1;
	}

	@Override
	public void setLiberoString1(String liberoString1) {
		_liberoString1 = liberoString1;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoString1", String.class);

				method.invoke(_articoliRemoteModel, liberoString1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLiberoString2() {
		return _liberoString2;
	}

	@Override
	public void setLiberoString2(String liberoString2) {
		_liberoString2 = liberoString2;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoString2", String.class);

				method.invoke(_articoliRemoteModel, liberoString2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLiberoString3() {
		return _liberoString3;
	}

	@Override
	public void setLiberoString3(String liberoString3) {
		_liberoString3 = liberoString3;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoString3", String.class);

				method.invoke(_articoliRemoteModel, liberoString3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLiberoString4() {
		return _liberoString4;
	}

	@Override
	public void setLiberoString4(String liberoString4) {
		_liberoString4 = liberoString4;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoString4", String.class);

				method.invoke(_articoliRemoteModel, liberoString4);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLiberoString5() {
		return _liberoString5;
	}

	@Override
	public void setLiberoString5(String liberoString5) {
		_liberoString5 = liberoString5;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoString5", String.class);

				method.invoke(_articoliRemoteModel, liberoString5);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLiberoDate1() {
		return _liberoDate1;
	}

	@Override
	public void setLiberoDate1(Date liberoDate1) {
		_liberoDate1 = liberoDate1;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoDate1", Date.class);

				method.invoke(_articoliRemoteModel, liberoDate1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLiberoDate2() {
		return _liberoDate2;
	}

	@Override
	public void setLiberoDate2(Date liberoDate2) {
		_liberoDate2 = liberoDate2;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoDate2", Date.class);

				method.invoke(_articoliRemoteModel, liberoDate2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLiberoDate3() {
		return _liberoDate3;
	}

	@Override
	public void setLiberoDate3(Date liberoDate3) {
		_liberoDate3 = liberoDate3;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoDate3", Date.class);

				method.invoke(_articoliRemoteModel, liberoDate3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLiberoDate4() {
		return _liberoDate4;
	}

	@Override
	public void setLiberoDate4(Date liberoDate4) {
		_liberoDate4 = liberoDate4;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoDate4", Date.class);

				method.invoke(_articoliRemoteModel, liberoDate4);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLiberoDate5() {
		return _liberoDate5;
	}

	@Override
	public void setLiberoDate5(Date liberoDate5) {
		_liberoDate5 = liberoDate5;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoDate5", Date.class);

				method.invoke(_articoliRemoteModel, liberoDate5);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLiberoLong1() {
		return _liberoLong1;
	}

	@Override
	public void setLiberoLong1(long liberoLong1) {
		_liberoLong1 = liberoLong1;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoLong1", long.class);

				method.invoke(_articoliRemoteModel, liberoLong1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLiberoLong2() {
		return _liberoLong2;
	}

	@Override
	public void setLiberoLong2(long liberoLong2) {
		_liberoLong2 = liberoLong2;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoLong2", long.class);

				method.invoke(_articoliRemoteModel, liberoLong2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLiberoLong3() {
		return _liberoLong3;
	}

	@Override
	public void setLiberoLong3(long liberoLong3) {
		_liberoLong3 = liberoLong3;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoLong3", long.class);

				method.invoke(_articoliRemoteModel, liberoLong3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLiberoLong4() {
		return _liberoLong4;
	}

	@Override
	public void setLiberoLong4(long liberoLong4) {
		_liberoLong4 = liberoLong4;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoLong4", long.class);

				method.invoke(_articoliRemoteModel, liberoLong4);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLiberoLong5() {
		return _liberoLong5;
	}

	@Override
	public void setLiberoLong5(long liberoLong5) {
		_liberoLong5 = liberoLong5;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoLong5", long.class);

				method.invoke(_articoliRemoteModel, liberoLong5);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLiberoDouble1() {
		return _liberoDouble1;
	}

	@Override
	public void setLiberoDouble1(double liberoDouble1) {
		_liberoDouble1 = liberoDouble1;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoDouble1", double.class);

				method.invoke(_articoliRemoteModel, liberoDouble1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLiberoDouble2() {
		return _liberoDouble2;
	}

	@Override
	public void setLiberoDouble2(double liberoDouble2) {
		_liberoDouble2 = liberoDouble2;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoDouble2", double.class);

				method.invoke(_articoliRemoteModel, liberoDouble2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLiberoDouble3() {
		return _liberoDouble3;
	}

	@Override
	public void setLiberoDouble3(double liberoDouble3) {
		_liberoDouble3 = liberoDouble3;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoDouble3", double.class);

				method.invoke(_articoliRemoteModel, liberoDouble3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLiberoDouble4() {
		return _liberoDouble4;
	}

	@Override
	public void setLiberoDouble4(double liberoDouble4) {
		_liberoDouble4 = liberoDouble4;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoDouble4", double.class);

				method.invoke(_articoliRemoteModel, liberoDouble4);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLiberoDouble5() {
		return _liberoDouble5;
	}

	@Override
	public void setLiberoDouble5(double liberoDouble5) {
		_liberoDouble5 = liberoDouble5;

		if (_articoliRemoteModel != null) {
			try {
				Class<?> clazz = _articoliRemoteModel.getClass();

				Method method = clazz.getMethod("setLiberoDouble5", double.class);

				method.invoke(_articoliRemoteModel, liberoDouble5);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getArticoliRemoteModel() {
		return _articoliRemoteModel;
	}

	public void setArticoliRemoteModel(BaseModel<?> articoliRemoteModel) {
		_articoliRemoteModel = articoliRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _articoliRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_articoliRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ArticoliLocalServiceUtil.addArticoli(this);
		}
		else {
			ArticoliLocalServiceUtil.updateArticoli(this);
		}
	}

	@Override
	public Articoli toEscapedModel() {
		return (Articoli)ProxyUtil.newProxyInstance(Articoli.class.getClassLoader(),
			new Class[] { Articoli.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ArticoliClp clone = new ArticoliClp();

		clone.setCodiceArticolo(getCodiceArticolo());
		clone.setDescrizione(getDescrizione());
		clone.setDescrizioneAggiuntiva(getDescrizioneAggiuntiva());
		clone.setDescrizioneBreveFiscale(getDescrizioneBreveFiscale());
		clone.setPathImmagine(getPathImmagine());
		clone.setUnitaMisura(getUnitaMisura());
		clone.setCategoriaMerceologica(getCategoriaMerceologica());
		clone.setCategoriaFiscale(getCategoriaFiscale());
		clone.setCodiceIVA(getCodiceIVA());
		clone.setCodiceProvvigioni(getCodiceProvvigioni());
		clone.setCodiceSconto1(getCodiceSconto1());
		clone.setCodiceSconto2(getCodiceSconto2());
		clone.setCategoriaInventario(getCategoriaInventario());
		clone.setPrezzo1(getPrezzo1());
		clone.setPrezzo2(getPrezzo2());
		clone.setPrezzo3(getPrezzo3());
		clone.setCodiceIVAPrezzo1(getCodiceIVAPrezzo1());
		clone.setCodiceIVAPrezzo2(getCodiceIVAPrezzo2());
		clone.setCodiceIVAPrezzo3(getCodiceIVAPrezzo3());
		clone.setNumeroDecimaliPrezzo(getNumeroDecimaliPrezzo());
		clone.setNumeroDecimaliQuantita(getNumeroDecimaliQuantita());
		clone.setNumeroMesiPerinvenduto(getNumeroMesiPerinvenduto());
		clone.setUnitaMisura2(getUnitaMisura2());
		clone.setFattoreMoltiplicativo(getFattoreMoltiplicativo());
		clone.setFattoreDivisione(getFattoreDivisione());
		clone.setRagruppamentoLibero(getRagruppamentoLibero());
		clone.setTipoLivelloDistintaBase(getTipoLivelloDistintaBase());
		clone.setTipoCostoStatistico(getTipoCostoStatistico());
		clone.setTipoGestioneArticolo(getTipoGestioneArticolo());
		clone.setTipoApprovvigionamentoArticolo(getTipoApprovvigionamentoArticolo());
		clone.setNomenclaturaCombinata(getNomenclaturaCombinata());
		clone.setDescrizioneEstesa(getDescrizioneEstesa());
		clone.setGenerazioneMovimenti(getGenerazioneMovimenti());
		clone.setTipoEsplosioneDistintaBase(getTipoEsplosioneDistintaBase());
		clone.setLivelloMaxEsplosioneDistintaBase(getLivelloMaxEsplosioneDistintaBase());
		clone.setValorizzazioneMagazzino(getValorizzazioneMagazzino());
		clone.setAbilitatoEC(getAbilitatoEC());
		clone.setCategoriaEC(getCategoriaEC());
		clone.setQuantitaMinimaEC(getQuantitaMinimaEC());
		clone.setQuantitaDefaultEC(getQuantitaDefaultEC());
		clone.setCodiceSchedaTecnicaEC(getCodiceSchedaTecnicaEC());
		clone.setGiorniPrevistaConsegna(getGiorniPrevistaConsegna());
		clone.setGestioneLotti(getGestioneLotti());
		clone.setCreazioneLotti(getCreazioneLotti());
		clone.setPrefissoCodiceLottoAutomatico(getPrefissoCodiceLottoAutomatico());
		clone.setNumeroCifreProgressivo(getNumeroCifreProgressivo());
		clone.setScaricoAutomaticoLotti(getScaricoAutomaticoLotti());
		clone.setGiorniInizioValiditaLotto(getGiorniInizioValiditaLotto());
		clone.setGiorniValiditaLotto(getGiorniValiditaLotto());
		clone.setGiorniPreavvisoScadenzaLotto(getGiorniPreavvisoScadenzaLotto());
		clone.setFattoreMoltQuantRiservata(getFattoreMoltQuantRiservata());
		clone.setFattoreDivQuantRiservata(getFattoreDivQuantRiservata());
		clone.setObsoleto(getObsoleto());
		clone.setComeImballo(getComeImballo());
		clone.setFattoreMoltCalcoloImballo(getFattoreMoltCalcoloImballo());
		clone.setFattoreDivCalcoloImballo(getFattoreDivCalcoloImballo());
		clone.setPesoLordo(getPesoLordo());
		clone.setTara(getTara());
		clone.setCalcoloVolume(getCalcoloVolume());
		clone.setLunghezza(getLunghezza());
		clone.setLarghezza(getLarghezza());
		clone.setProfondita(getProfondita());
		clone.setCodiceImballo(getCodiceImballo());
		clone.setFattoreMoltUnitMisSupp(getFattoreMoltUnitMisSupp());
		clone.setFattoreDivUnitMisSupp(getFattoreDivUnitMisSupp());
		clone.setStampaetichette(getStampaetichette());
		clone.setGestioneVarianti(getGestioneVarianti());
		clone.setGestioneVariantiDistintaBase(getGestioneVariantiDistintaBase());
		clone.setLiberoString1(getLiberoString1());
		clone.setLiberoString2(getLiberoString2());
		clone.setLiberoString3(getLiberoString3());
		clone.setLiberoString4(getLiberoString4());
		clone.setLiberoString5(getLiberoString5());
		clone.setLiberoDate1(getLiberoDate1());
		clone.setLiberoDate2(getLiberoDate2());
		clone.setLiberoDate3(getLiberoDate3());
		clone.setLiberoDate4(getLiberoDate4());
		clone.setLiberoDate5(getLiberoDate5());
		clone.setLiberoLong1(getLiberoLong1());
		clone.setLiberoLong2(getLiberoLong2());
		clone.setLiberoLong3(getLiberoLong3());
		clone.setLiberoLong4(getLiberoLong4());
		clone.setLiberoLong5(getLiberoLong5());
		clone.setLiberoDouble1(getLiberoDouble1());
		clone.setLiberoDouble2(getLiberoDouble2());
		clone.setLiberoDouble3(getLiberoDouble3());
		clone.setLiberoDouble4(getLiberoDouble4());
		clone.setLiberoDouble5(getLiberoDouble5());

		return clone;
	}

	@Override
	public int compareTo(Articoli articoli) {
		String primaryKey = articoli.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ArticoliClp)) {
			return false;
		}

		ArticoliClp articoli = (ArticoliClp)obj;

		String primaryKey = articoli.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(177);

		sb.append("{codiceArticolo=");
		sb.append(getCodiceArticolo());
		sb.append(", descrizione=");
		sb.append(getDescrizione());
		sb.append(", descrizioneAggiuntiva=");
		sb.append(getDescrizioneAggiuntiva());
		sb.append(", descrizioneBreveFiscale=");
		sb.append(getDescrizioneBreveFiscale());
		sb.append(", pathImmagine=");
		sb.append(getPathImmagine());
		sb.append(", unitaMisura=");
		sb.append(getUnitaMisura());
		sb.append(", categoriaMerceologica=");
		sb.append(getCategoriaMerceologica());
		sb.append(", categoriaFiscale=");
		sb.append(getCategoriaFiscale());
		sb.append(", codiceIVA=");
		sb.append(getCodiceIVA());
		sb.append(", codiceProvvigioni=");
		sb.append(getCodiceProvvigioni());
		sb.append(", codiceSconto1=");
		sb.append(getCodiceSconto1());
		sb.append(", codiceSconto2=");
		sb.append(getCodiceSconto2());
		sb.append(", categoriaInventario=");
		sb.append(getCategoriaInventario());
		sb.append(", prezzo1=");
		sb.append(getPrezzo1());
		sb.append(", prezzo2=");
		sb.append(getPrezzo2());
		sb.append(", prezzo3=");
		sb.append(getPrezzo3());
		sb.append(", codiceIVAPrezzo1=");
		sb.append(getCodiceIVAPrezzo1());
		sb.append(", codiceIVAPrezzo2=");
		sb.append(getCodiceIVAPrezzo2());
		sb.append(", codiceIVAPrezzo3=");
		sb.append(getCodiceIVAPrezzo3());
		sb.append(", numeroDecimaliPrezzo=");
		sb.append(getNumeroDecimaliPrezzo());
		sb.append(", numeroDecimaliQuantita=");
		sb.append(getNumeroDecimaliQuantita());
		sb.append(", numeroMesiPerinvenduto=");
		sb.append(getNumeroMesiPerinvenduto());
		sb.append(", unitaMisura2=");
		sb.append(getUnitaMisura2());
		sb.append(", fattoreMoltiplicativo=");
		sb.append(getFattoreMoltiplicativo());
		sb.append(", fattoreDivisione=");
		sb.append(getFattoreDivisione());
		sb.append(", ragruppamentoLibero=");
		sb.append(getRagruppamentoLibero());
		sb.append(", tipoLivelloDistintaBase=");
		sb.append(getTipoLivelloDistintaBase());
		sb.append(", tipoCostoStatistico=");
		sb.append(getTipoCostoStatistico());
		sb.append(", tipoGestioneArticolo=");
		sb.append(getTipoGestioneArticolo());
		sb.append(", tipoApprovvigionamentoArticolo=");
		sb.append(getTipoApprovvigionamentoArticolo());
		sb.append(", nomenclaturaCombinata=");
		sb.append(getNomenclaturaCombinata());
		sb.append(", descrizioneEstesa=");
		sb.append(getDescrizioneEstesa());
		sb.append(", generazioneMovimenti=");
		sb.append(getGenerazioneMovimenti());
		sb.append(", tipoEsplosioneDistintaBase=");
		sb.append(getTipoEsplosioneDistintaBase());
		sb.append(", livelloMaxEsplosioneDistintaBase=");
		sb.append(getLivelloMaxEsplosioneDistintaBase());
		sb.append(", valorizzazioneMagazzino=");
		sb.append(getValorizzazioneMagazzino());
		sb.append(", abilitatoEC=");
		sb.append(getAbilitatoEC());
		sb.append(", categoriaEC=");
		sb.append(getCategoriaEC());
		sb.append(", quantitaMinimaEC=");
		sb.append(getQuantitaMinimaEC());
		sb.append(", quantitaDefaultEC=");
		sb.append(getQuantitaDefaultEC());
		sb.append(", codiceSchedaTecnicaEC=");
		sb.append(getCodiceSchedaTecnicaEC());
		sb.append(", giorniPrevistaConsegna=");
		sb.append(getGiorniPrevistaConsegna());
		sb.append(", gestioneLotti=");
		sb.append(getGestioneLotti());
		sb.append(", creazioneLotti=");
		sb.append(getCreazioneLotti());
		sb.append(", prefissoCodiceLottoAutomatico=");
		sb.append(getPrefissoCodiceLottoAutomatico());
		sb.append(", numeroCifreProgressivo=");
		sb.append(getNumeroCifreProgressivo());
		sb.append(", scaricoAutomaticoLotti=");
		sb.append(getScaricoAutomaticoLotti());
		sb.append(", giorniInizioValiditaLotto=");
		sb.append(getGiorniInizioValiditaLotto());
		sb.append(", giorniValiditaLotto=");
		sb.append(getGiorniValiditaLotto());
		sb.append(", giorniPreavvisoScadenzaLotto=");
		sb.append(getGiorniPreavvisoScadenzaLotto());
		sb.append(", fattoreMoltQuantRiservata=");
		sb.append(getFattoreMoltQuantRiservata());
		sb.append(", fattoreDivQuantRiservata=");
		sb.append(getFattoreDivQuantRiservata());
		sb.append(", obsoleto=");
		sb.append(getObsoleto());
		sb.append(", comeImballo=");
		sb.append(getComeImballo());
		sb.append(", fattoreMoltCalcoloImballo=");
		sb.append(getFattoreMoltCalcoloImballo());
		sb.append(", fattoreDivCalcoloImballo=");
		sb.append(getFattoreDivCalcoloImballo());
		sb.append(", pesoLordo=");
		sb.append(getPesoLordo());
		sb.append(", tara=");
		sb.append(getTara());
		sb.append(", calcoloVolume=");
		sb.append(getCalcoloVolume());
		sb.append(", lunghezza=");
		sb.append(getLunghezza());
		sb.append(", larghezza=");
		sb.append(getLarghezza());
		sb.append(", profondita=");
		sb.append(getProfondita());
		sb.append(", codiceImballo=");
		sb.append(getCodiceImballo());
		sb.append(", fattoreMoltUnitMisSupp=");
		sb.append(getFattoreMoltUnitMisSupp());
		sb.append(", fattoreDivUnitMisSupp=");
		sb.append(getFattoreDivUnitMisSupp());
		sb.append(", stampaetichette=");
		sb.append(getStampaetichette());
		sb.append(", gestioneVarianti=");
		sb.append(getGestioneVarianti());
		sb.append(", gestioneVariantiDistintaBase=");
		sb.append(getGestioneVariantiDistintaBase());
		sb.append(", liberoString1=");
		sb.append(getLiberoString1());
		sb.append(", liberoString2=");
		sb.append(getLiberoString2());
		sb.append(", liberoString3=");
		sb.append(getLiberoString3());
		sb.append(", liberoString4=");
		sb.append(getLiberoString4());
		sb.append(", liberoString5=");
		sb.append(getLiberoString5());
		sb.append(", liberoDate1=");
		sb.append(getLiberoDate1());
		sb.append(", liberoDate2=");
		sb.append(getLiberoDate2());
		sb.append(", liberoDate3=");
		sb.append(getLiberoDate3());
		sb.append(", liberoDate4=");
		sb.append(getLiberoDate4());
		sb.append(", liberoDate5=");
		sb.append(getLiberoDate5());
		sb.append(", liberoLong1=");
		sb.append(getLiberoLong1());
		sb.append(", liberoLong2=");
		sb.append(getLiberoLong2());
		sb.append(", liberoLong3=");
		sb.append(getLiberoLong3());
		sb.append(", liberoLong4=");
		sb.append(getLiberoLong4());
		sb.append(", liberoLong5=");
		sb.append(getLiberoLong5());
		sb.append(", liberoDouble1=");
		sb.append(getLiberoDouble1());
		sb.append(", liberoDouble2=");
		sb.append(getLiberoDouble2());
		sb.append(", liberoDouble3=");
		sb.append(getLiberoDouble3());
		sb.append(", liberoDouble4=");
		sb.append(getLiberoDouble4());
		sb.append(", liberoDouble5=");
		sb.append(getLiberoDouble5());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(268);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.Articoli");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>codiceArticolo</column-name><column-value><![CDATA[");
		sb.append(getCodiceArticolo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizione</column-name><column-value><![CDATA[");
		sb.append(getDescrizione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizioneAggiuntiva</column-name><column-value><![CDATA[");
		sb.append(getDescrizioneAggiuntiva());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizioneBreveFiscale</column-name><column-value><![CDATA[");
		sb.append(getDescrizioneBreveFiscale());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>pathImmagine</column-name><column-value><![CDATA[");
		sb.append(getPathImmagine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>unitaMisura</column-name><column-value><![CDATA[");
		sb.append(getUnitaMisura());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>categoriaMerceologica</column-name><column-value><![CDATA[");
		sb.append(getCategoriaMerceologica());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>categoriaFiscale</column-name><column-value><![CDATA[");
		sb.append(getCategoriaFiscale());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVA</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVA());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceProvvigioni</column-name><column-value><![CDATA[");
		sb.append(getCodiceProvvigioni());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceSconto1</column-name><column-value><![CDATA[");
		sb.append(getCodiceSconto1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceSconto2</column-name><column-value><![CDATA[");
		sb.append(getCodiceSconto2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>categoriaInventario</column-name><column-value><![CDATA[");
		sb.append(getCategoriaInventario());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>prezzo1</column-name><column-value><![CDATA[");
		sb.append(getPrezzo1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>prezzo2</column-name><column-value><![CDATA[");
		sb.append(getPrezzo2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>prezzo3</column-name><column-value><![CDATA[");
		sb.append(getPrezzo3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVAPrezzo1</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVAPrezzo1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVAPrezzo2</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVAPrezzo2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVAPrezzo3</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVAPrezzo3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroDecimaliPrezzo</column-name><column-value><![CDATA[");
		sb.append(getNumeroDecimaliPrezzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroDecimaliQuantita</column-name><column-value><![CDATA[");
		sb.append(getNumeroDecimaliQuantita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroMesiPerinvenduto</column-name><column-value><![CDATA[");
		sb.append(getNumeroMesiPerinvenduto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>unitaMisura2</column-name><column-value><![CDATA[");
		sb.append(getUnitaMisura2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fattoreMoltiplicativo</column-name><column-value><![CDATA[");
		sb.append(getFattoreMoltiplicativo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fattoreDivisione</column-name><column-value><![CDATA[");
		sb.append(getFattoreDivisione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ragruppamentoLibero</column-name><column-value><![CDATA[");
		sb.append(getRagruppamentoLibero());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoLivelloDistintaBase</column-name><column-value><![CDATA[");
		sb.append(getTipoLivelloDistintaBase());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoCostoStatistico</column-name><column-value><![CDATA[");
		sb.append(getTipoCostoStatistico());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoGestioneArticolo</column-name><column-value><![CDATA[");
		sb.append(getTipoGestioneArticolo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoApprovvigionamentoArticolo</column-name><column-value><![CDATA[");
		sb.append(getTipoApprovvigionamentoArticolo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nomenclaturaCombinata</column-name><column-value><![CDATA[");
		sb.append(getNomenclaturaCombinata());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizioneEstesa</column-name><column-value><![CDATA[");
		sb.append(getDescrizioneEstesa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>generazioneMovimenti</column-name><column-value><![CDATA[");
		sb.append(getGenerazioneMovimenti());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoEsplosioneDistintaBase</column-name><column-value><![CDATA[");
		sb.append(getTipoEsplosioneDistintaBase());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>livelloMaxEsplosioneDistintaBase</column-name><column-value><![CDATA[");
		sb.append(getLivelloMaxEsplosioneDistintaBase());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>valorizzazioneMagazzino</column-name><column-value><![CDATA[");
		sb.append(getValorizzazioneMagazzino());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>abilitatoEC</column-name><column-value><![CDATA[");
		sb.append(getAbilitatoEC());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>categoriaEC</column-name><column-value><![CDATA[");
		sb.append(getCategoriaEC());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaMinimaEC</column-name><column-value><![CDATA[");
		sb.append(getQuantitaMinimaEC());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaDefaultEC</column-name><column-value><![CDATA[");
		sb.append(getQuantitaDefaultEC());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceSchedaTecnicaEC</column-name><column-value><![CDATA[");
		sb.append(getCodiceSchedaTecnicaEC());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>giorniPrevistaConsegna</column-name><column-value><![CDATA[");
		sb.append(getGiorniPrevistaConsegna());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>gestioneLotti</column-name><column-value><![CDATA[");
		sb.append(getGestioneLotti());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>creazioneLotti</column-name><column-value><![CDATA[");
		sb.append(getCreazioneLotti());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>prefissoCodiceLottoAutomatico</column-name><column-value><![CDATA[");
		sb.append(getPrefissoCodiceLottoAutomatico());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroCifreProgressivo</column-name><column-value><![CDATA[");
		sb.append(getNumeroCifreProgressivo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>scaricoAutomaticoLotti</column-name><column-value><![CDATA[");
		sb.append(getScaricoAutomaticoLotti());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>giorniInizioValiditaLotto</column-name><column-value><![CDATA[");
		sb.append(getGiorniInizioValiditaLotto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>giorniValiditaLotto</column-name><column-value><![CDATA[");
		sb.append(getGiorniValiditaLotto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>giorniPreavvisoScadenzaLotto</column-name><column-value><![CDATA[");
		sb.append(getGiorniPreavvisoScadenzaLotto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fattoreMoltQuantRiservata</column-name><column-value><![CDATA[");
		sb.append(getFattoreMoltQuantRiservata());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fattoreDivQuantRiservata</column-name><column-value><![CDATA[");
		sb.append(getFattoreDivQuantRiservata());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>obsoleto</column-name><column-value><![CDATA[");
		sb.append(getObsoleto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>comeImballo</column-name><column-value><![CDATA[");
		sb.append(getComeImballo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fattoreMoltCalcoloImballo</column-name><column-value><![CDATA[");
		sb.append(getFattoreMoltCalcoloImballo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fattoreDivCalcoloImballo</column-name><column-value><![CDATA[");
		sb.append(getFattoreDivCalcoloImballo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>pesoLordo</column-name><column-value><![CDATA[");
		sb.append(getPesoLordo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tara</column-name><column-value><![CDATA[");
		sb.append(getTara());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>calcoloVolume</column-name><column-value><![CDATA[");
		sb.append(getCalcoloVolume());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lunghezza</column-name><column-value><![CDATA[");
		sb.append(getLunghezza());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>larghezza</column-name><column-value><![CDATA[");
		sb.append(getLarghezza());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>profondita</column-name><column-value><![CDATA[");
		sb.append(getProfondita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceImballo</column-name><column-value><![CDATA[");
		sb.append(getCodiceImballo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fattoreMoltUnitMisSupp</column-name><column-value><![CDATA[");
		sb.append(getFattoreMoltUnitMisSupp());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fattoreDivUnitMisSupp</column-name><column-value><![CDATA[");
		sb.append(getFattoreDivUnitMisSupp());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>stampaetichette</column-name><column-value><![CDATA[");
		sb.append(getStampaetichette());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>gestioneVarianti</column-name><column-value><![CDATA[");
		sb.append(getGestioneVarianti());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>gestioneVariantiDistintaBase</column-name><column-value><![CDATA[");
		sb.append(getGestioneVariantiDistintaBase());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoString1</column-name><column-value><![CDATA[");
		sb.append(getLiberoString1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoString2</column-name><column-value><![CDATA[");
		sb.append(getLiberoString2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoString3</column-name><column-value><![CDATA[");
		sb.append(getLiberoString3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoString4</column-name><column-value><![CDATA[");
		sb.append(getLiberoString4());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoString5</column-name><column-value><![CDATA[");
		sb.append(getLiberoString5());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoDate1</column-name><column-value><![CDATA[");
		sb.append(getLiberoDate1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoDate2</column-name><column-value><![CDATA[");
		sb.append(getLiberoDate2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoDate3</column-name><column-value><![CDATA[");
		sb.append(getLiberoDate3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoDate4</column-name><column-value><![CDATA[");
		sb.append(getLiberoDate4());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoDate5</column-name><column-value><![CDATA[");
		sb.append(getLiberoDate5());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoLong1</column-name><column-value><![CDATA[");
		sb.append(getLiberoLong1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoLong2</column-name><column-value><![CDATA[");
		sb.append(getLiberoLong2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoLong3</column-name><column-value><![CDATA[");
		sb.append(getLiberoLong3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoLong4</column-name><column-value><![CDATA[");
		sb.append(getLiberoLong4());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoLong5</column-name><column-value><![CDATA[");
		sb.append(getLiberoLong5());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoDouble1</column-name><column-value><![CDATA[");
		sb.append(getLiberoDouble1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoDouble2</column-name><column-value><![CDATA[");
		sb.append(getLiberoDouble2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoDouble3</column-name><column-value><![CDATA[");
		sb.append(getLiberoDouble3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoDouble4</column-name><column-value><![CDATA[");
		sb.append(getLiberoDouble4());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>liberoDouble5</column-name><column-value><![CDATA[");
		sb.append(getLiberoDouble5());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _codiceArticolo;
	private String _descrizione;
	private String _descrizioneAggiuntiva;
	private String _descrizioneBreveFiscale;
	private String _pathImmagine;
	private String _unitaMisura;
	private String _categoriaMerceologica;
	private String _categoriaFiscale;
	private String _codiceIVA;
	private String _codiceProvvigioni;
	private String _codiceSconto1;
	private String _codiceSconto2;
	private String _categoriaInventario;
	private double _prezzo1;
	private double _prezzo2;
	private double _prezzo3;
	private String _codiceIVAPrezzo1;
	private String _codiceIVAPrezzo2;
	private String _codiceIVAPrezzo3;
	private int _numeroDecimaliPrezzo;
	private int _numeroDecimaliQuantita;
	private int _numeroMesiPerinvenduto;
	private String _unitaMisura2;
	private double _fattoreMoltiplicativo;
	private double _fattoreDivisione;
	private String _ragruppamentoLibero;
	private int _tipoLivelloDistintaBase;
	private int _tipoCostoStatistico;
	private int _tipoGestioneArticolo;
	private int _tipoApprovvigionamentoArticolo;
	private double _nomenclaturaCombinata;
	private String _descrizioneEstesa;
	private boolean _generazioneMovimenti;
	private int _tipoEsplosioneDistintaBase;
	private int _livelloMaxEsplosioneDistintaBase;
	private boolean _valorizzazioneMagazzino;
	private boolean _abilitatoEC;
	private String _categoriaEC;
	private double _quantitaMinimaEC;
	private double _quantitaDefaultEC;
	private String _codiceSchedaTecnicaEC;
	private int _giorniPrevistaConsegna;
	private boolean _gestioneLotti;
	private boolean _creazioneLotti;
	private String _prefissoCodiceLottoAutomatico;
	private int _numeroCifreProgressivo;
	private boolean _scaricoAutomaticoLotti;
	private int _giorniInizioValiditaLotto;
	private int _giorniValiditaLotto;
	private int _giorniPreavvisoScadenzaLotto;
	private double _fattoreMoltQuantRiservata;
	private double _fattoreDivQuantRiservata;
	private boolean _obsoleto;
	private boolean _comeImballo;
	private double _fattoreMoltCalcoloImballo;
	private double _fattoreDivCalcoloImballo;
	private double _pesoLordo;
	private double _tara;
	private int _calcoloVolume;
	private double _lunghezza;
	private double _larghezza;
	private double _profondita;
	private String _codiceImballo;
	private double _fattoreMoltUnitMisSupp;
	private double _fattoreDivUnitMisSupp;
	private int _stampaetichette;
	private boolean _gestioneVarianti;
	private boolean _gestioneVariantiDistintaBase;
	private String _liberoString1;
	private String _liberoString2;
	private String _liberoString3;
	private String _liberoString4;
	private String _liberoString5;
	private Date _liberoDate1;
	private Date _liberoDate2;
	private Date _liberoDate3;
	private Date _liberoDate4;
	private Date _liberoDate5;
	private long _liberoLong1;
	private long _liberoLong2;
	private long _liberoLong3;
	private long _liberoLong4;
	private long _liberoLong5;
	private double _liberoDouble1;
	private double _liberoDouble2;
	private double _liberoDouble3;
	private double _liberoDouble4;
	private double _liberoDouble5;
	private BaseModel<?> _articoliRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}