/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.PianoPagamentiLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class PianoPagamentiClp extends BaseModelImpl<PianoPagamenti>
	implements PianoPagamenti {
	public PianoPagamentiClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return PianoPagamenti.class;
	}

	@Override
	public String getModelClassName() {
		return PianoPagamenti.class.getName();
	}

	@Override
	public String getPrimaryKey() {
		return _codicePianoPagamento;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setCodicePianoPagamento(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _codicePianoPagamento;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codicePianoPagamento", getCodicePianoPagamento());
		attributes.put("descrizione", getDescrizione());
		attributes.put("primoMeseEscluso", getPrimoMeseEscluso());
		attributes.put("giornoPrimoMeseSucc", getGiornoPrimoMeseSucc());
		attributes.put("giornoSecondoMeseSucc", getGiornoSecondoMeseSucc());
		attributes.put("secondoMeseEscluso", getSecondoMeseEscluso());
		attributes.put("inizioFattDaBolla", getInizioFattDaBolla());
		attributes.put("percScChiusura", getPercScChiusura());
		attributes.put("percScCassa", getPercScCassa());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codicePianoPagamento = (String)attributes.get(
				"codicePianoPagamento");

		if (codicePianoPagamento != null) {
			setCodicePianoPagamento(codicePianoPagamento);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		Integer primoMeseEscluso = (Integer)attributes.get("primoMeseEscluso");

		if (primoMeseEscluso != null) {
			setPrimoMeseEscluso(primoMeseEscluso);
		}

		Integer giornoPrimoMeseSucc = (Integer)attributes.get(
				"giornoPrimoMeseSucc");

		if (giornoPrimoMeseSucc != null) {
			setGiornoPrimoMeseSucc(giornoPrimoMeseSucc);
		}

		Integer giornoSecondoMeseSucc = (Integer)attributes.get(
				"giornoSecondoMeseSucc");

		if (giornoSecondoMeseSucc != null) {
			setGiornoSecondoMeseSucc(giornoSecondoMeseSucc);
		}

		Integer secondoMeseEscluso = (Integer)attributes.get(
				"secondoMeseEscluso");

		if (secondoMeseEscluso != null) {
			setSecondoMeseEscluso(secondoMeseEscluso);
		}

		Boolean inizioFattDaBolla = (Boolean)attributes.get("inizioFattDaBolla");

		if (inizioFattDaBolla != null) {
			setInizioFattDaBolla(inizioFattDaBolla);
		}

		Double percScChiusura = (Double)attributes.get("percScChiusura");

		if (percScChiusura != null) {
			setPercScChiusura(percScChiusura);
		}

		Double percScCassa = (Double)attributes.get("percScCassa");

		if (percScCassa != null) {
			setPercScCassa(percScCassa);
		}
	}

	@Override
	public String getCodicePianoPagamento() {
		return _codicePianoPagamento;
	}

	@Override
	public void setCodicePianoPagamento(String codicePianoPagamento) {
		_codicePianoPagamento = codicePianoPagamento;

		if (_pianoPagamentiRemoteModel != null) {
			try {
				Class<?> clazz = _pianoPagamentiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodicePianoPagamento",
						String.class);

				method.invoke(_pianoPagamentiRemoteModel, codicePianoPagamento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizione() {
		return _descrizione;
	}

	@Override
	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;

		if (_pianoPagamentiRemoteModel != null) {
			try {
				Class<?> clazz = _pianoPagamentiRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizione", String.class);

				method.invoke(_pianoPagamentiRemoteModel, descrizione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getPrimoMeseEscluso() {
		return _primoMeseEscluso;
	}

	@Override
	public void setPrimoMeseEscluso(int primoMeseEscluso) {
		_primoMeseEscluso = primoMeseEscluso;

		if (_pianoPagamentiRemoteModel != null) {
			try {
				Class<?> clazz = _pianoPagamentiRemoteModel.getClass();

				Method method = clazz.getMethod("setPrimoMeseEscluso", int.class);

				method.invoke(_pianoPagamentiRemoteModel, primoMeseEscluso);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getGiornoPrimoMeseSucc() {
		return _giornoPrimoMeseSucc;
	}

	@Override
	public void setGiornoPrimoMeseSucc(int giornoPrimoMeseSucc) {
		_giornoPrimoMeseSucc = giornoPrimoMeseSucc;

		if (_pianoPagamentiRemoteModel != null) {
			try {
				Class<?> clazz = _pianoPagamentiRemoteModel.getClass();

				Method method = clazz.getMethod("setGiornoPrimoMeseSucc",
						int.class);

				method.invoke(_pianoPagamentiRemoteModel, giornoPrimoMeseSucc);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getGiornoSecondoMeseSucc() {
		return _giornoSecondoMeseSucc;
	}

	@Override
	public void setGiornoSecondoMeseSucc(int giornoSecondoMeseSucc) {
		_giornoSecondoMeseSucc = giornoSecondoMeseSucc;

		if (_pianoPagamentiRemoteModel != null) {
			try {
				Class<?> clazz = _pianoPagamentiRemoteModel.getClass();

				Method method = clazz.getMethod("setGiornoSecondoMeseSucc",
						int.class);

				method.invoke(_pianoPagamentiRemoteModel, giornoSecondoMeseSucc);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getSecondoMeseEscluso() {
		return _secondoMeseEscluso;
	}

	@Override
	public void setSecondoMeseEscluso(int secondoMeseEscluso) {
		_secondoMeseEscluso = secondoMeseEscluso;

		if (_pianoPagamentiRemoteModel != null) {
			try {
				Class<?> clazz = _pianoPagamentiRemoteModel.getClass();

				Method method = clazz.getMethod("setSecondoMeseEscluso",
						int.class);

				method.invoke(_pianoPagamentiRemoteModel, secondoMeseEscluso);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getInizioFattDaBolla() {
		return _inizioFattDaBolla;
	}

	@Override
	public boolean isInizioFattDaBolla() {
		return _inizioFattDaBolla;
	}

	@Override
	public void setInizioFattDaBolla(boolean inizioFattDaBolla) {
		_inizioFattDaBolla = inizioFattDaBolla;

		if (_pianoPagamentiRemoteModel != null) {
			try {
				Class<?> clazz = _pianoPagamentiRemoteModel.getClass();

				Method method = clazz.getMethod("setInizioFattDaBolla",
						boolean.class);

				method.invoke(_pianoPagamentiRemoteModel, inizioFattDaBolla);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPercScChiusura() {
		return _percScChiusura;
	}

	@Override
	public void setPercScChiusura(double percScChiusura) {
		_percScChiusura = percScChiusura;

		if (_pianoPagamentiRemoteModel != null) {
			try {
				Class<?> clazz = _pianoPagamentiRemoteModel.getClass();

				Method method = clazz.getMethod("setPercScChiusura",
						double.class);

				method.invoke(_pianoPagamentiRemoteModel, percScChiusura);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPercScCassa() {
		return _percScCassa;
	}

	@Override
	public void setPercScCassa(double percScCassa) {
		_percScCassa = percScCassa;

		if (_pianoPagamentiRemoteModel != null) {
			try {
				Class<?> clazz = _pianoPagamentiRemoteModel.getClass();

				Method method = clazz.getMethod("setPercScCassa", double.class);

				method.invoke(_pianoPagamentiRemoteModel, percScCassa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getPianoPagamentiRemoteModel() {
		return _pianoPagamentiRemoteModel;
	}

	public void setPianoPagamentiRemoteModel(
		BaseModel<?> pianoPagamentiRemoteModel) {
		_pianoPagamentiRemoteModel = pianoPagamentiRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _pianoPagamentiRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_pianoPagamentiRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			PianoPagamentiLocalServiceUtil.addPianoPagamenti(this);
		}
		else {
			PianoPagamentiLocalServiceUtil.updatePianoPagamenti(this);
		}
	}

	@Override
	public PianoPagamenti toEscapedModel() {
		return (PianoPagamenti)ProxyUtil.newProxyInstance(PianoPagamenti.class.getClassLoader(),
			new Class[] { PianoPagamenti.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		PianoPagamentiClp clone = new PianoPagamentiClp();

		clone.setCodicePianoPagamento(getCodicePianoPagamento());
		clone.setDescrizione(getDescrizione());
		clone.setPrimoMeseEscluso(getPrimoMeseEscluso());
		clone.setGiornoPrimoMeseSucc(getGiornoPrimoMeseSucc());
		clone.setGiornoSecondoMeseSucc(getGiornoSecondoMeseSucc());
		clone.setSecondoMeseEscluso(getSecondoMeseEscluso());
		clone.setInizioFattDaBolla(getInizioFattDaBolla());
		clone.setPercScChiusura(getPercScChiusura());
		clone.setPercScCassa(getPercScCassa());

		return clone;
	}

	@Override
	public int compareTo(PianoPagamenti pianoPagamenti) {
		String primaryKey = pianoPagamenti.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PianoPagamentiClp)) {
			return false;
		}

		PianoPagamentiClp pianoPagamenti = (PianoPagamentiClp)obj;

		String primaryKey = pianoPagamenti.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{codicePianoPagamento=");
		sb.append(getCodicePianoPagamento());
		sb.append(", descrizione=");
		sb.append(getDescrizione());
		sb.append(", primoMeseEscluso=");
		sb.append(getPrimoMeseEscluso());
		sb.append(", giornoPrimoMeseSucc=");
		sb.append(getGiornoPrimoMeseSucc());
		sb.append(", giornoSecondoMeseSucc=");
		sb.append(getGiornoSecondoMeseSucc());
		sb.append(", secondoMeseEscluso=");
		sb.append(getSecondoMeseEscluso());
		sb.append(", inizioFattDaBolla=");
		sb.append(getInizioFattDaBolla());
		sb.append(", percScChiusura=");
		sb.append(getPercScChiusura());
		sb.append(", percScCassa=");
		sb.append(getPercScCassa());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(31);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.PianoPagamenti");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>codicePianoPagamento</column-name><column-value><![CDATA[");
		sb.append(getCodicePianoPagamento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizione</column-name><column-value><![CDATA[");
		sb.append(getDescrizione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>primoMeseEscluso</column-name><column-value><![CDATA[");
		sb.append(getPrimoMeseEscluso());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>giornoPrimoMeseSucc</column-name><column-value><![CDATA[");
		sb.append(getGiornoPrimoMeseSucc());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>giornoSecondoMeseSucc</column-name><column-value><![CDATA[");
		sb.append(getGiornoSecondoMeseSucc());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>secondoMeseEscluso</column-name><column-value><![CDATA[");
		sb.append(getSecondoMeseEscluso());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>inizioFattDaBolla</column-name><column-value><![CDATA[");
		sb.append(getInizioFattDaBolla());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>percScChiusura</column-name><column-value><![CDATA[");
		sb.append(getPercScChiusura());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>percScCassa</column-name><column-value><![CDATA[");
		sb.append(getPercScCassa());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _codicePianoPagamento;
	private String _descrizione;
	private int _primoMeseEscluso;
	private int _giornoPrimoMeseSucc;
	private int _giornoSecondoMeseSucc;
	private int _secondoMeseEscluso;
	private boolean _inizioFattDaBolla;
	private double _percScChiusura;
	private double _percScCassa;
	private BaseModel<?> _pianoPagamentiRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}