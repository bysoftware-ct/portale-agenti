/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ScontiArticoli}.
 * </p>
 *
 * @author Mario Torrisi
 * @see ScontiArticoli
 * @generated
 */
public class ScontiArticoliWrapper implements ScontiArticoli,
	ModelWrapper<ScontiArticoli> {
	public ScontiArticoliWrapper(ScontiArticoli scontiArticoli) {
		_scontiArticoli = scontiArticoli;
	}

	@Override
	public Class<?> getModelClass() {
		return ScontiArticoli.class;
	}

	@Override
	public String getModelClassName() {
		return ScontiArticoli.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codScontoCliente", getCodScontoCliente());
		attributes.put("codScontoArticolo", getCodScontoArticolo());
		attributes.put("sconto", getSconto());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codScontoCliente = (String)attributes.get("codScontoCliente");

		if (codScontoCliente != null) {
			setCodScontoCliente(codScontoCliente);
		}

		String codScontoArticolo = (String)attributes.get("codScontoArticolo");

		if (codScontoArticolo != null) {
			setCodScontoArticolo(codScontoArticolo);
		}

		Double sconto = (Double)attributes.get("sconto");

		if (sconto != null) {
			setSconto(sconto);
		}
	}

	/**
	* Returns the primary key of this sconti articoli.
	*
	* @return the primary key of this sconti articoli
	*/
	@Override
	public it.bysoftware.ct.service.persistence.ScontiArticoliPK getPrimaryKey() {
		return _scontiArticoli.getPrimaryKey();
	}

	/**
	* Sets the primary key of this sconti articoli.
	*
	* @param primaryKey the primary key of this sconti articoli
	*/
	@Override
	public void setPrimaryKey(
		it.bysoftware.ct.service.persistence.ScontiArticoliPK primaryKey) {
		_scontiArticoli.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the cod sconto cliente of this sconti articoli.
	*
	* @return the cod sconto cliente of this sconti articoli
	*/
	@Override
	public java.lang.String getCodScontoCliente() {
		return _scontiArticoli.getCodScontoCliente();
	}

	/**
	* Sets the cod sconto cliente of this sconti articoli.
	*
	* @param codScontoCliente the cod sconto cliente of this sconti articoli
	*/
	@Override
	public void setCodScontoCliente(java.lang.String codScontoCliente) {
		_scontiArticoli.setCodScontoCliente(codScontoCliente);
	}

	/**
	* Returns the cod sconto articolo of this sconti articoli.
	*
	* @return the cod sconto articolo of this sconti articoli
	*/
	@Override
	public java.lang.String getCodScontoArticolo() {
		return _scontiArticoli.getCodScontoArticolo();
	}

	/**
	* Sets the cod sconto articolo of this sconti articoli.
	*
	* @param codScontoArticolo the cod sconto articolo of this sconti articoli
	*/
	@Override
	public void setCodScontoArticolo(java.lang.String codScontoArticolo) {
		_scontiArticoli.setCodScontoArticolo(codScontoArticolo);
	}

	/**
	* Returns the sconto of this sconti articoli.
	*
	* @return the sconto of this sconti articoli
	*/
	@Override
	public double getSconto() {
		return _scontiArticoli.getSconto();
	}

	/**
	* Sets the sconto of this sconti articoli.
	*
	* @param sconto the sconto of this sconti articoli
	*/
	@Override
	public void setSconto(double sconto) {
		_scontiArticoli.setSconto(sconto);
	}

	@Override
	public boolean isNew() {
		return _scontiArticoli.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_scontiArticoli.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _scontiArticoli.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_scontiArticoli.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _scontiArticoli.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _scontiArticoli.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_scontiArticoli.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _scontiArticoli.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_scontiArticoli.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_scontiArticoli.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_scontiArticoli.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ScontiArticoliWrapper((ScontiArticoli)_scontiArticoli.clone());
	}

	@Override
	public int compareTo(it.bysoftware.ct.model.ScontiArticoli scontiArticoli) {
		return _scontiArticoli.compareTo(scontiArticoli);
	}

	@Override
	public int hashCode() {
		return _scontiArticoli.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.ScontiArticoli> toCacheModel() {
		return _scontiArticoli.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.ScontiArticoli toEscapedModel() {
		return new ScontiArticoliWrapper(_scontiArticoli.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.ScontiArticoli toUnescapedModel() {
		return new ScontiArticoliWrapper(_scontiArticoli.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _scontiArticoli.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _scontiArticoli.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_scontiArticoli.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ScontiArticoliWrapper)) {
			return false;
		}

		ScontiArticoliWrapper scontiArticoliWrapper = (ScontiArticoliWrapper)obj;

		if (Validator.equals(_scontiArticoli,
					scontiArticoliWrapper._scontiArticoli)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ScontiArticoli getWrappedScontiArticoli() {
		return _scontiArticoli;
	}

	@Override
	public ScontiArticoli getWrappedModel() {
		return _scontiArticoli;
	}

	@Override
	public void resetOriginalValues() {
		_scontiArticoli.resetOriginalValues();
	}

	private ScontiArticoli _scontiArticoli;
}