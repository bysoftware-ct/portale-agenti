/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.MovimentiMagazzinoLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class MovimentiMagazzinoClp extends BaseModelImpl<MovimentiMagazzino>
	implements MovimentiMagazzino {
	public MovimentiMagazzinoClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return MovimentiMagazzino.class;
	}

	@Override
	public String getModelClassName() {
		return MovimentiMagazzino.class.getName();
	}

	@Override
	public String getPrimaryKey() {
		return _ID;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setID(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _ID;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ID", getID());
		attributes.put("anno", getAnno());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("descrizione", getDescrizione());
		attributes.put("unitaMisura", getUnitaMisura());
		attributes.put("quantita", getQuantita());
		attributes.put("testCaricoScarico", getTestCaricoScarico());
		attributes.put("dataRegistrazione", getDataRegistrazione());
		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceSoggetto", getCodiceSoggetto());
		attributes.put("ragioneSociale", getRagioneSociale());
		attributes.put("estremiDocumenti", getEstremiDocumenti());
		attributes.put("dataDocumento", getDataDocumento());
		attributes.put("numeroDocumento", getNumeroDocumento());
		attributes.put("tipoDocumento", getTipoDocumento());
		attributes.put("soloValore", getSoloValore());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String ID = (String)attributes.get("ID");

		if (ID != null) {
			setID(ID);
		}

		Integer anno = (Integer)attributes.get("anno");

		if (anno != null) {
			setAnno(anno);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		String unitaMisura = (String)attributes.get("unitaMisura");

		if (unitaMisura != null) {
			setUnitaMisura(unitaMisura);
		}

		Double quantita = (Double)attributes.get("quantita");

		if (quantita != null) {
			setQuantita(quantita);
		}

		Integer testCaricoScarico = (Integer)attributes.get("testCaricoScarico");

		if (testCaricoScarico != null) {
			setTestCaricoScarico(testCaricoScarico);
		}

		Date dataRegistrazione = (Date)attributes.get("dataRegistrazione");

		if (dataRegistrazione != null) {
			setDataRegistrazione(dataRegistrazione);
		}

		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceSoggetto = (String)attributes.get("codiceSoggetto");

		if (codiceSoggetto != null) {
			setCodiceSoggetto(codiceSoggetto);
		}

		String ragioneSociale = (String)attributes.get("ragioneSociale");

		if (ragioneSociale != null) {
			setRagioneSociale(ragioneSociale);
		}

		String estremiDocumenti = (String)attributes.get("estremiDocumenti");

		if (estremiDocumenti != null) {
			setEstremiDocumenti(estremiDocumenti);
		}

		Date dataDocumento = (Date)attributes.get("dataDocumento");

		if (dataDocumento != null) {
			setDataDocumento(dataDocumento);
		}

		Integer numeroDocumento = (Integer)attributes.get("numeroDocumento");

		if (numeroDocumento != null) {
			setNumeroDocumento(numeroDocumento);
		}

		String tipoDocumento = (String)attributes.get("tipoDocumento");

		if (tipoDocumento != null) {
			setTipoDocumento(tipoDocumento);
		}

		Integer soloValore = (Integer)attributes.get("soloValore");

		if (soloValore != null) {
			setSoloValore(soloValore);
		}
	}

	@Override
	public String getID() {
		return _ID;
	}

	@Override
	public void setID(String ID) {
		_ID = ID;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setID", String.class);

				method.invoke(_movimentiMagazzinoRemoteModel, ID);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getAnno() {
		return _anno;
	}

	@Override
	public void setAnno(int anno) {
		_anno = anno;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setAnno", int.class);

				method.invoke(_movimentiMagazzinoRemoteModel, anno);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	@Override
	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceArticolo",
						String.class);

				method.invoke(_movimentiMagazzinoRemoteModel, codiceArticolo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceVariante() {
		return _codiceVariante;
	}

	@Override
	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceVariante",
						String.class);

				method.invoke(_movimentiMagazzinoRemoteModel, codiceVariante);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizione() {
		return _descrizione;
	}

	@Override
	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizione", String.class);

				method.invoke(_movimentiMagazzinoRemoteModel, descrizione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUnitaMisura() {
		return _unitaMisura;
	}

	@Override
	public void setUnitaMisura(String unitaMisura) {
		_unitaMisura = unitaMisura;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setUnitaMisura", String.class);

				method.invoke(_movimentiMagazzinoRemoteModel, unitaMisura);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Double getQuantita() {
		return _quantita;
	}

	@Override
	public void setQuantita(Double quantita) {
		_quantita = quantita;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantita", Double.class);

				method.invoke(_movimentiMagazzinoRemoteModel, quantita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTestCaricoScarico() {
		return _testCaricoScarico;
	}

	@Override
	public void setTestCaricoScarico(int testCaricoScarico) {
		_testCaricoScarico = testCaricoScarico;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setTestCaricoScarico",
						int.class);

				method.invoke(_movimentiMagazzinoRemoteModel, testCaricoScarico);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataRegistrazione() {
		return _dataRegistrazione;
	}

	@Override
	public void setDataRegistrazione(Date dataRegistrazione) {
		_dataRegistrazione = dataRegistrazione;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setDataRegistrazione",
						Date.class);

				method.invoke(_movimentiMagazzinoRemoteModel, dataRegistrazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoSoggetto", boolean.class);

				method.invoke(_movimentiMagazzinoRemoteModel, tipoSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceSoggetto() {
		return _codiceSoggetto;
	}

	@Override
	public void setCodiceSoggetto(String codiceSoggetto) {
		_codiceSoggetto = codiceSoggetto;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceSoggetto",
						String.class);

				method.invoke(_movimentiMagazzinoRemoteModel, codiceSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRagioneSociale() {
		return _ragioneSociale;
	}

	@Override
	public void setRagioneSociale(String ragioneSociale) {
		_ragioneSociale = ragioneSociale;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setRagioneSociale",
						String.class);

				method.invoke(_movimentiMagazzinoRemoteModel, ragioneSociale);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEstremiDocumenti() {
		return _estremiDocumenti;
	}

	@Override
	public void setEstremiDocumenti(String estremiDocumenti) {
		_estremiDocumenti = estremiDocumenti;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setEstremiDocumenti",
						String.class);

				method.invoke(_movimentiMagazzinoRemoteModel, estremiDocumenti);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataDocumento() {
		return _dataDocumento;
	}

	@Override
	public void setDataDocumento(Date dataDocumento) {
		_dataDocumento = dataDocumento;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setDataDocumento", Date.class);

				method.invoke(_movimentiMagazzinoRemoteModel, dataDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroDocumento() {
		return _numeroDocumento;
	}

	@Override
	public void setNumeroDocumento(int numeroDocumento) {
		_numeroDocumento = numeroDocumento;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroDocumento", int.class);

				method.invoke(_movimentiMagazzinoRemoteModel, numeroDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoDocumento() {
		return _tipoDocumento;
	}

	@Override
	public void setTipoDocumento(String tipoDocumento) {
		_tipoDocumento = tipoDocumento;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoDocumento", String.class);

				method.invoke(_movimentiMagazzinoRemoteModel, tipoDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getSoloValore() {
		return _soloValore;
	}

	@Override
	public void setSoloValore(int soloValore) {
		_soloValore = soloValore;

		if (_movimentiMagazzinoRemoteModel != null) {
			try {
				Class<?> clazz = _movimentiMagazzinoRemoteModel.getClass();

				Method method = clazz.getMethod("setSoloValore", int.class);

				method.invoke(_movimentiMagazzinoRemoteModel, soloValore);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getMovimentiMagazzinoRemoteModel() {
		return _movimentiMagazzinoRemoteModel;
	}

	public void setMovimentiMagazzinoRemoteModel(
		BaseModel<?> movimentiMagazzinoRemoteModel) {
		_movimentiMagazzinoRemoteModel = movimentiMagazzinoRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _movimentiMagazzinoRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_movimentiMagazzinoRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			MovimentiMagazzinoLocalServiceUtil.addMovimentiMagazzino(this);
		}
		else {
			MovimentiMagazzinoLocalServiceUtil.updateMovimentiMagazzino(this);
		}
	}

	@Override
	public MovimentiMagazzino toEscapedModel() {
		return (MovimentiMagazzino)ProxyUtil.newProxyInstance(MovimentiMagazzino.class.getClassLoader(),
			new Class[] { MovimentiMagazzino.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		MovimentiMagazzinoClp clone = new MovimentiMagazzinoClp();

		clone.setID(getID());
		clone.setAnno(getAnno());
		clone.setCodiceArticolo(getCodiceArticolo());
		clone.setCodiceVariante(getCodiceVariante());
		clone.setDescrizione(getDescrizione());
		clone.setUnitaMisura(getUnitaMisura());
		clone.setQuantita(getQuantita());
		clone.setTestCaricoScarico(getTestCaricoScarico());
		clone.setDataRegistrazione(getDataRegistrazione());
		clone.setTipoSoggetto(getTipoSoggetto());
		clone.setCodiceSoggetto(getCodiceSoggetto());
		clone.setRagioneSociale(getRagioneSociale());
		clone.setEstremiDocumenti(getEstremiDocumenti());
		clone.setDataDocumento(getDataDocumento());
		clone.setNumeroDocumento(getNumeroDocumento());
		clone.setTipoDocumento(getTipoDocumento());
		clone.setSoloValore(getSoloValore());

		return clone;
	}

	@Override
	public int compareTo(MovimentiMagazzino movimentiMagazzino) {
		int value = 0;

		value = DateUtil.compareTo(getDataRegistrazione(),
				movimentiMagazzino.getDataRegistrazione());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MovimentiMagazzinoClp)) {
			return false;
		}

		MovimentiMagazzinoClp movimentiMagazzino = (MovimentiMagazzinoClp)obj;

		String primaryKey = movimentiMagazzino.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(35);

		sb.append("{ID=");
		sb.append(getID());
		sb.append(", anno=");
		sb.append(getAnno());
		sb.append(", codiceArticolo=");
		sb.append(getCodiceArticolo());
		sb.append(", codiceVariante=");
		sb.append(getCodiceVariante());
		sb.append(", descrizione=");
		sb.append(getDescrizione());
		sb.append(", unitaMisura=");
		sb.append(getUnitaMisura());
		sb.append(", quantita=");
		sb.append(getQuantita());
		sb.append(", testCaricoScarico=");
		sb.append(getTestCaricoScarico());
		sb.append(", dataRegistrazione=");
		sb.append(getDataRegistrazione());
		sb.append(", tipoSoggetto=");
		sb.append(getTipoSoggetto());
		sb.append(", codiceSoggetto=");
		sb.append(getCodiceSoggetto());
		sb.append(", ragioneSociale=");
		sb.append(getRagioneSociale());
		sb.append(", estremiDocumenti=");
		sb.append(getEstremiDocumenti());
		sb.append(", dataDocumento=");
		sb.append(getDataDocumento());
		sb.append(", numeroDocumento=");
		sb.append(getNumeroDocumento());
		sb.append(", tipoDocumento=");
		sb.append(getTipoDocumento());
		sb.append(", soloValore=");
		sb.append(getSoloValore());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(55);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.MovimentiMagazzino");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>ID</column-name><column-value><![CDATA[");
		sb.append(getID());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>anno</column-name><column-value><![CDATA[");
		sb.append(getAnno());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceArticolo</column-name><column-value><![CDATA[");
		sb.append(getCodiceArticolo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceVariante</column-name><column-value><![CDATA[");
		sb.append(getCodiceVariante());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizione</column-name><column-value><![CDATA[");
		sb.append(getDescrizione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>unitaMisura</column-name><column-value><![CDATA[");
		sb.append(getUnitaMisura());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantita</column-name><column-value><![CDATA[");
		sb.append(getQuantita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>testCaricoScarico</column-name><column-value><![CDATA[");
		sb.append(getTestCaricoScarico());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataRegistrazione</column-name><column-value><![CDATA[");
		sb.append(getDataRegistrazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoSoggetto</column-name><column-value><![CDATA[");
		sb.append(getTipoSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceSoggetto</column-name><column-value><![CDATA[");
		sb.append(getCodiceSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ragioneSociale</column-name><column-value><![CDATA[");
		sb.append(getRagioneSociale());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>estremiDocumenti</column-name><column-value><![CDATA[");
		sb.append(getEstremiDocumenti());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataDocumento</column-name><column-value><![CDATA[");
		sb.append(getDataDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroDocumento</column-name><column-value><![CDATA[");
		sb.append(getNumeroDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoDocumento</column-name><column-value><![CDATA[");
		sb.append(getTipoDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>soloValore</column-name><column-value><![CDATA[");
		sb.append(getSoloValore());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _ID;
	private int _anno;
	private String _codiceArticolo;
	private String _codiceVariante;
	private String _descrizione;
	private String _unitaMisura;
	private Double _quantita;
	private int _testCaricoScarico;
	private Date _dataRegistrazione;
	private boolean _tipoSoggetto;
	private String _codiceSoggetto;
	private String _ragioneSociale;
	private String _estremiDocumenti;
	private Date _dataDocumento;
	private int _numeroDocumento;
	private String _tipoDocumento;
	private int _soloValore;
	private BaseModel<?> _movimentiMagazzinoRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}