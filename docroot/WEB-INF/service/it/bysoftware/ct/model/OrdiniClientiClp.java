/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.OrdiniClientiLocalServiceUtil;
import it.bysoftware.ct.service.persistence.OrdiniClientiPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class OrdiniClientiClp extends BaseModelImpl<OrdiniClienti>
	implements OrdiniClienti {
	public OrdiniClientiClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return OrdiniClienti.class;
	}

	@Override
	public String getModelClassName() {
		return OrdiniClienti.class.getName();
	}

	@Override
	public OrdiniClientiPK getPrimaryKey() {
		return new OrdiniClientiPK(_anno, _codiceAttivita, _codiceCentro,
			_codiceDeposito, _tipoOrdine, _numeroOrdine);
	}

	@Override
	public void setPrimaryKey(OrdiniClientiPK primaryKey) {
		setAnno(primaryKey.anno);
		setCodiceAttivita(primaryKey.codiceAttivita);
		setCodiceCentro(primaryKey.codiceCentro);
		setCodiceDeposito(primaryKey.codiceDeposito);
		setTipoOrdine(primaryKey.tipoOrdine);
		setNumeroOrdine(primaryKey.numeroOrdine);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new OrdiniClientiPK(_anno, _codiceAttivita, _codiceCentro,
			_codiceDeposito, _tipoOrdine, _numeroOrdine);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((OrdiniClientiPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("anno", getAnno());
		attributes.put("codiceAttivita", getCodiceAttivita());
		attributes.put("codiceCentro", getCodiceCentro());
		attributes.put("codiceDeposito", getCodiceDeposito());
		attributes.put("tipoOrdine", getTipoOrdine());
		attributes.put("numeroOrdine", getNumeroOrdine());
		attributes.put("tipoDocumento", getTipoDocumento());
		attributes.put("codiceContAnalitica", getCodiceContAnalitica());
		attributes.put("idTipoDocumento", getIdTipoDocumento());
		attributes.put("statoOrdine", getStatoOrdine());
		attributes.put("descrEstremiDoc", getDescrEstremiDoc());
		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceCliente", getCodiceCliente());
		attributes.put("naturaTransazione", getNaturaTransazione());
		attributes.put("codiceEsenzione", getCodiceEsenzione());
		attributes.put("codiceDivisa", getCodiceDivisa());
		attributes.put("valoreCambio", getValoreCambio());
		attributes.put("dataValoreCambio", getDataValoreCambio());
		attributes.put("codicePianoPag", getCodicePianoPag());
		attributes.put("inizioCalcoloPag", getInizioCalcoloPag());
		attributes.put("percentualeScontoMaggiorazione",
			getPercentualeScontoMaggiorazione());
		attributes.put("percentualeScontoProntaCassa",
			getPercentualeScontoProntaCassa());
		attributes.put("percentualeProvvChiusura", getPercentualeProvvChiusura());
		attributes.put("dataDocumento", getDataDocumento());
		attributes.put("dataRegistrazione", getDataRegistrazione());
		attributes.put("causaleEstrattoConto", getCausaleEstrattoConto());
		attributes.put("dataPrimaRata", getDataPrimaRata());
		attributes.put("dataUltimaRata", getDataUltimaRata());
		attributes.put("codiceBanca", getCodiceBanca());
		attributes.put("codiceAgenzia", getCodiceAgenzia());
		attributes.put("codiceAgente", getCodiceAgente());
		attributes.put("codiceGruppoAgenti", getCodiceGruppoAgenti());
		attributes.put("codiceZona", getCodiceZona());
		attributes.put("codiceSpedizione", getCodiceSpedizione());
		attributes.put("codicePorto", getCodicePorto());
		attributes.put("codiceDestinatario", getCodiceDestinatario());
		attributes.put("codiceListino", getCodiceListino());
		attributes.put("codiceLingua", getCodiceLingua());
		attributes.put("numeroDecPrezzo", getNumeroDecPrezzo());
		attributes.put("note", getNote());
		attributes.put("percentualeSpeseTrasp", getPercentualeSpeseTrasp());
		attributes.put("speseTrasporto", getSpeseTrasporto());
		attributes.put("speseImballaggio", getSpeseImballaggio());
		attributes.put("speseVarie", getSpeseVarie());
		attributes.put("speseBanca", getSpeseBanca());
		attributes.put("curaTrasporto", getCuraTrasporto());
		attributes.put("causaleTrasporto", getCausaleTrasporto());
		attributes.put("aspettoEstriore", getAspettoEstriore());
		attributes.put("vettore1", getVettore1());
		attributes.put("vettore2", getVettore2());
		attributes.put("vettore3", getVettore3());
		attributes.put("numeroColli", getNumeroColli());
		attributes.put("pesoLordo", getPesoLordo());
		attributes.put("pesoNetto", getPesoNetto());
		attributes.put("volume", getVolume());
		attributes.put("numeroCopie", getNumeroCopie());
		attributes.put("numeroCopieStampate", getNumeroCopieStampate());
		attributes.put("inviatoEmail", getInviatoEmail());
		attributes.put("nomePDF", getNomePDF());
		attributes.put("riferimentoOrdine", getRiferimentoOrdine());
		attributes.put("dataConferma", getDataConferma());
		attributes.put("confermaStampata", getConfermaStampata());
		attributes.put("totaleOrdine", getTotaleOrdine());
		attributes.put("codiceIVATrasp", getCodiceIVATrasp());
		attributes.put("codiceIVAImballo", getCodiceIVAImballo());
		attributes.put("codiceIVAVarie", getCodiceIVAVarie());
		attributes.put("codiceIVABanca", getCodiceIVABanca());
		attributes.put("libStr1", getLibStr1());
		attributes.put("libStr2", getLibStr2());
		attributes.put("libStr3", getLibStr3());
		attributes.put("libDbl1", getLibDbl1());
		attributes.put("libDbl2", getLibDbl2());
		attributes.put("libDbl3", getLibDbl3());
		attributes.put("libDat1", getLibDat1());
		attributes.put("libDat2", getLibDat2());
		attributes.put("libDat3", getLibDat3());
		attributes.put("libLng1", getLibLng1());
		attributes.put("libLng2", getLibLng2());
		attributes.put("libLng3", getLibLng3());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer anno = (Integer)attributes.get("anno");

		if (anno != null) {
			setAnno(anno);
		}

		String codiceAttivita = (String)attributes.get("codiceAttivita");

		if (codiceAttivita != null) {
			setCodiceAttivita(codiceAttivita);
		}

		String codiceCentro = (String)attributes.get("codiceCentro");

		if (codiceCentro != null) {
			setCodiceCentro(codiceCentro);
		}

		String codiceDeposito = (String)attributes.get("codiceDeposito");

		if (codiceDeposito != null) {
			setCodiceDeposito(codiceDeposito);
		}

		Integer tipoOrdine = (Integer)attributes.get("tipoOrdine");

		if (tipoOrdine != null) {
			setTipoOrdine(tipoOrdine);
		}

		Integer numeroOrdine = (Integer)attributes.get("numeroOrdine");

		if (numeroOrdine != null) {
			setNumeroOrdine(numeroOrdine);
		}

		String tipoDocumento = (String)attributes.get("tipoDocumento");

		if (tipoDocumento != null) {
			setTipoDocumento(tipoDocumento);
		}

		String codiceContAnalitica = (String)attributes.get(
				"codiceContAnalitica");

		if (codiceContAnalitica != null) {
			setCodiceContAnalitica(codiceContAnalitica);
		}

		Integer idTipoDocumento = (Integer)attributes.get("idTipoDocumento");

		if (idTipoDocumento != null) {
			setIdTipoDocumento(idTipoDocumento);
		}

		Boolean statoOrdine = (Boolean)attributes.get("statoOrdine");

		if (statoOrdine != null) {
			setStatoOrdine(statoOrdine);
		}

		String descrEstremiDoc = (String)attributes.get("descrEstremiDoc");

		if (descrEstremiDoc != null) {
			setDescrEstremiDoc(descrEstremiDoc);
		}

		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceCliente = (String)attributes.get("codiceCliente");

		if (codiceCliente != null) {
			setCodiceCliente(codiceCliente);
		}

		String naturaTransazione = (String)attributes.get("naturaTransazione");

		if (naturaTransazione != null) {
			setNaturaTransazione(naturaTransazione);
		}

		String codiceEsenzione = (String)attributes.get("codiceEsenzione");

		if (codiceEsenzione != null) {
			setCodiceEsenzione(codiceEsenzione);
		}

		String codiceDivisa = (String)attributes.get("codiceDivisa");

		if (codiceDivisa != null) {
			setCodiceDivisa(codiceDivisa);
		}

		Double valoreCambio = (Double)attributes.get("valoreCambio");

		if (valoreCambio != null) {
			setValoreCambio(valoreCambio);
		}

		Date dataValoreCambio = (Date)attributes.get("dataValoreCambio");

		if (dataValoreCambio != null) {
			setDataValoreCambio(dataValoreCambio);
		}

		String codicePianoPag = (String)attributes.get("codicePianoPag");

		if (codicePianoPag != null) {
			setCodicePianoPag(codicePianoPag);
		}

		Date inizioCalcoloPag = (Date)attributes.get("inizioCalcoloPag");

		if (inizioCalcoloPag != null) {
			setInizioCalcoloPag(inizioCalcoloPag);
		}

		Double percentualeScontoMaggiorazione = (Double)attributes.get(
				"percentualeScontoMaggiorazione");

		if (percentualeScontoMaggiorazione != null) {
			setPercentualeScontoMaggiorazione(percentualeScontoMaggiorazione);
		}

		Double percentualeScontoProntaCassa = (Double)attributes.get(
				"percentualeScontoProntaCassa");

		if (percentualeScontoProntaCassa != null) {
			setPercentualeScontoProntaCassa(percentualeScontoProntaCassa);
		}

		Double percentualeProvvChiusura = (Double)attributes.get(
				"percentualeProvvChiusura");

		if (percentualeProvvChiusura != null) {
			setPercentualeProvvChiusura(percentualeProvvChiusura);
		}

		Date dataDocumento = (Date)attributes.get("dataDocumento");

		if (dataDocumento != null) {
			setDataDocumento(dataDocumento);
		}

		Date dataRegistrazione = (Date)attributes.get("dataRegistrazione");

		if (dataRegistrazione != null) {
			setDataRegistrazione(dataRegistrazione);
		}

		String causaleEstrattoConto = (String)attributes.get(
				"causaleEstrattoConto");

		if (causaleEstrattoConto != null) {
			setCausaleEstrattoConto(causaleEstrattoConto);
		}

		Date dataPrimaRata = (Date)attributes.get("dataPrimaRata");

		if (dataPrimaRata != null) {
			setDataPrimaRata(dataPrimaRata);
		}

		Date dataUltimaRata = (Date)attributes.get("dataUltimaRata");

		if (dataUltimaRata != null) {
			setDataUltimaRata(dataUltimaRata);
		}

		String codiceBanca = (String)attributes.get("codiceBanca");

		if (codiceBanca != null) {
			setCodiceBanca(codiceBanca);
		}

		String codiceAgenzia = (String)attributes.get("codiceAgenzia");

		if (codiceAgenzia != null) {
			setCodiceAgenzia(codiceAgenzia);
		}

		String codiceAgente = (String)attributes.get("codiceAgente");

		if (codiceAgente != null) {
			setCodiceAgente(codiceAgente);
		}

		String codiceGruppoAgenti = (String)attributes.get("codiceGruppoAgenti");

		if (codiceGruppoAgenti != null) {
			setCodiceGruppoAgenti(codiceGruppoAgenti);
		}

		String codiceZona = (String)attributes.get("codiceZona");

		if (codiceZona != null) {
			setCodiceZona(codiceZona);
		}

		String codiceSpedizione = (String)attributes.get("codiceSpedizione");

		if (codiceSpedizione != null) {
			setCodiceSpedizione(codiceSpedizione);
		}

		String codicePorto = (String)attributes.get("codicePorto");

		if (codicePorto != null) {
			setCodicePorto(codicePorto);
		}

		String codiceDestinatario = (String)attributes.get("codiceDestinatario");

		if (codiceDestinatario != null) {
			setCodiceDestinatario(codiceDestinatario);
		}

		String codiceListino = (String)attributes.get("codiceListino");

		if (codiceListino != null) {
			setCodiceListino(codiceListino);
		}

		String codiceLingua = (String)attributes.get("codiceLingua");

		if (codiceLingua != null) {
			setCodiceLingua(codiceLingua);
		}

		Integer numeroDecPrezzo = (Integer)attributes.get("numeroDecPrezzo");

		if (numeroDecPrezzo != null) {
			setNumeroDecPrezzo(numeroDecPrezzo);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}

		Double percentualeSpeseTrasp = (Double)attributes.get(
				"percentualeSpeseTrasp");

		if (percentualeSpeseTrasp != null) {
			setPercentualeSpeseTrasp(percentualeSpeseTrasp);
		}

		Double speseTrasporto = (Double)attributes.get("speseTrasporto");

		if (speseTrasporto != null) {
			setSpeseTrasporto(speseTrasporto);
		}

		Double speseImballaggio = (Double)attributes.get("speseImballaggio");

		if (speseImballaggio != null) {
			setSpeseImballaggio(speseImballaggio);
		}

		Double speseVarie = (Double)attributes.get("speseVarie");

		if (speseVarie != null) {
			setSpeseVarie(speseVarie);
		}

		Double speseBanca = (Double)attributes.get("speseBanca");

		if (speseBanca != null) {
			setSpeseBanca(speseBanca);
		}

		String curaTrasporto = (String)attributes.get("curaTrasporto");

		if (curaTrasporto != null) {
			setCuraTrasporto(curaTrasporto);
		}

		String causaleTrasporto = (String)attributes.get("causaleTrasporto");

		if (causaleTrasporto != null) {
			setCausaleTrasporto(causaleTrasporto);
		}

		String aspettoEstriore = (String)attributes.get("aspettoEstriore");

		if (aspettoEstriore != null) {
			setAspettoEstriore(aspettoEstriore);
		}

		String vettore1 = (String)attributes.get("vettore1");

		if (vettore1 != null) {
			setVettore1(vettore1);
		}

		String vettore2 = (String)attributes.get("vettore2");

		if (vettore2 != null) {
			setVettore2(vettore2);
		}

		String vettore3 = (String)attributes.get("vettore3");

		if (vettore3 != null) {
			setVettore3(vettore3);
		}

		Integer numeroColli = (Integer)attributes.get("numeroColli");

		if (numeroColli != null) {
			setNumeroColli(numeroColli);
		}

		Double pesoLordo = (Double)attributes.get("pesoLordo");

		if (pesoLordo != null) {
			setPesoLordo(pesoLordo);
		}

		Double pesoNetto = (Double)attributes.get("pesoNetto");

		if (pesoNetto != null) {
			setPesoNetto(pesoNetto);
		}

		Double volume = (Double)attributes.get("volume");

		if (volume != null) {
			setVolume(volume);
		}

		Integer numeroCopie = (Integer)attributes.get("numeroCopie");

		if (numeroCopie != null) {
			setNumeroCopie(numeroCopie);
		}

		Integer numeroCopieStampate = (Integer)attributes.get(
				"numeroCopieStampate");

		if (numeroCopieStampate != null) {
			setNumeroCopieStampate(numeroCopieStampate);
		}

		Boolean inviatoEmail = (Boolean)attributes.get("inviatoEmail");

		if (inviatoEmail != null) {
			setInviatoEmail(inviatoEmail);
		}

		String nomePDF = (String)attributes.get("nomePDF");

		if (nomePDF != null) {
			setNomePDF(nomePDF);
		}

		String riferimentoOrdine = (String)attributes.get("riferimentoOrdine");

		if (riferimentoOrdine != null) {
			setRiferimentoOrdine(riferimentoOrdine);
		}

		Date dataConferma = (Date)attributes.get("dataConferma");

		if (dataConferma != null) {
			setDataConferma(dataConferma);
		}

		Boolean confermaStampata = (Boolean)attributes.get("confermaStampata");

		if (confermaStampata != null) {
			setConfermaStampata(confermaStampata);
		}

		Double totaleOrdine = (Double)attributes.get("totaleOrdine");

		if (totaleOrdine != null) {
			setTotaleOrdine(totaleOrdine);
		}

		String codiceIVATrasp = (String)attributes.get("codiceIVATrasp");

		if (codiceIVATrasp != null) {
			setCodiceIVATrasp(codiceIVATrasp);
		}

		String codiceIVAImballo = (String)attributes.get("codiceIVAImballo");

		if (codiceIVAImballo != null) {
			setCodiceIVAImballo(codiceIVAImballo);
		}

		String codiceIVAVarie = (String)attributes.get("codiceIVAVarie");

		if (codiceIVAVarie != null) {
			setCodiceIVAVarie(codiceIVAVarie);
		}

		String codiceIVABanca = (String)attributes.get("codiceIVABanca");

		if (codiceIVABanca != null) {
			setCodiceIVABanca(codiceIVABanca);
		}

		String libStr1 = (String)attributes.get("libStr1");

		if (libStr1 != null) {
			setLibStr1(libStr1);
		}

		String libStr2 = (String)attributes.get("libStr2");

		if (libStr2 != null) {
			setLibStr2(libStr2);
		}

		String libStr3 = (String)attributes.get("libStr3");

		if (libStr3 != null) {
			setLibStr3(libStr3);
		}

		Double libDbl1 = (Double)attributes.get("libDbl1");

		if (libDbl1 != null) {
			setLibDbl1(libDbl1);
		}

		Double libDbl2 = (Double)attributes.get("libDbl2");

		if (libDbl2 != null) {
			setLibDbl2(libDbl2);
		}

		Double libDbl3 = (Double)attributes.get("libDbl3");

		if (libDbl3 != null) {
			setLibDbl3(libDbl3);
		}

		Date libDat1 = (Date)attributes.get("libDat1");

		if (libDat1 != null) {
			setLibDat1(libDat1);
		}

		Date libDat2 = (Date)attributes.get("libDat2");

		if (libDat2 != null) {
			setLibDat2(libDat2);
		}

		Date libDat3 = (Date)attributes.get("libDat3");

		if (libDat3 != null) {
			setLibDat3(libDat3);
		}

		Long libLng1 = (Long)attributes.get("libLng1");

		if (libLng1 != null) {
			setLibLng1(libLng1);
		}

		Long libLng2 = (Long)attributes.get("libLng2");

		if (libLng2 != null) {
			setLibLng2(libLng2);
		}

		Long libLng3 = (Long)attributes.get("libLng3");

		if (libLng3 != null) {
			setLibLng3(libLng3);
		}
	}

	@Override
	public int getAnno() {
		return _anno;
	}

	@Override
	public void setAnno(int anno) {
		_anno = anno;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setAnno", int.class);

				method.invoke(_ordiniClientiRemoteModel, anno);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceAttivita() {
		return _codiceAttivita;
	}

	@Override
	public void setCodiceAttivita(String codiceAttivita) {
		_codiceAttivita = codiceAttivita;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceAttivita",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceAttivita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCentro() {
		return _codiceCentro;
	}

	@Override
	public void setCodiceCentro(String codiceCentro) {
		_codiceCentro = codiceCentro;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCentro", String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceCentro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDeposito() {
		return _codiceDeposito;
	}

	@Override
	public void setCodiceDeposito(String codiceDeposito) {
		_codiceDeposito = codiceDeposito;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDeposito",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceDeposito);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoOrdine() {
		return _tipoOrdine;
	}

	@Override
	public void setTipoOrdine(int tipoOrdine) {
		_tipoOrdine = tipoOrdine;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoOrdine", int.class);

				method.invoke(_ordiniClientiRemoteModel, tipoOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroOrdine() {
		return _numeroOrdine;
	}

	@Override
	public void setNumeroOrdine(int numeroOrdine) {
		_numeroOrdine = numeroOrdine;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroOrdine", int.class);

				method.invoke(_ordiniClientiRemoteModel, numeroOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoDocumento() {
		return _tipoDocumento;
	}

	@Override
	public void setTipoDocumento(String tipoDocumento) {
		_tipoDocumento = tipoDocumento;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoDocumento", String.class);

				method.invoke(_ordiniClientiRemoteModel, tipoDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceContAnalitica() {
		return _codiceContAnalitica;
	}

	@Override
	public void setCodiceContAnalitica(String codiceContAnalitica) {
		_codiceContAnalitica = codiceContAnalitica;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceContAnalitica",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceContAnalitica);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getIdTipoDocumento() {
		return _idTipoDocumento;
	}

	@Override
	public void setIdTipoDocumento(int idTipoDocumento) {
		_idTipoDocumento = idTipoDocumento;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setIdTipoDocumento", int.class);

				method.invoke(_ordiniClientiRemoteModel, idTipoDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getStatoOrdine() {
		return _statoOrdine;
	}

	@Override
	public boolean isStatoOrdine() {
		return _statoOrdine;
	}

	@Override
	public void setStatoOrdine(boolean statoOrdine) {
		_statoOrdine = statoOrdine;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setStatoOrdine", boolean.class);

				method.invoke(_ordiniClientiRemoteModel, statoOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrEstremiDoc() {
		return _descrEstremiDoc;
	}

	@Override
	public void setDescrEstremiDoc(String descrEstremiDoc) {
		_descrEstremiDoc = descrEstremiDoc;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrEstremiDoc",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, descrEstremiDoc);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoSoggetto", boolean.class);

				method.invoke(_ordiniClientiRemoteModel, tipoSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCliente() {
		return _codiceCliente;
	}

	@Override
	public void setCodiceCliente(String codiceCliente) {
		_codiceCliente = codiceCliente;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCliente", String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceCliente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNaturaTransazione() {
		return _naturaTransazione;
	}

	@Override
	public void setNaturaTransazione(String naturaTransazione) {
		_naturaTransazione = naturaTransazione;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNaturaTransazione",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, naturaTransazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceEsenzione() {
		return _codiceEsenzione;
	}

	@Override
	public void setCodiceEsenzione(String codiceEsenzione) {
		_codiceEsenzione = codiceEsenzione;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceEsenzione",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceEsenzione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDivisa() {
		return _codiceDivisa;
	}

	@Override
	public void setCodiceDivisa(String codiceDivisa) {
		_codiceDivisa = codiceDivisa;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDivisa", String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceDivisa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getValoreCambio() {
		return _valoreCambio;
	}

	@Override
	public void setValoreCambio(double valoreCambio) {
		_valoreCambio = valoreCambio;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setValoreCambio", double.class);

				method.invoke(_ordiniClientiRemoteModel, valoreCambio);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataValoreCambio() {
		return _dataValoreCambio;
	}

	@Override
	public void setDataValoreCambio(Date dataValoreCambio) {
		_dataValoreCambio = dataValoreCambio;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataValoreCambio",
						Date.class);

				method.invoke(_ordiniClientiRemoteModel, dataValoreCambio);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodicePianoPag() {
		return _codicePianoPag;
	}

	@Override
	public void setCodicePianoPag(String codicePianoPag) {
		_codicePianoPag = codicePianoPag;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodicePianoPag",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, codicePianoPag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getInizioCalcoloPag() {
		return _inizioCalcoloPag;
	}

	@Override
	public void setInizioCalcoloPag(Date inizioCalcoloPag) {
		_inizioCalcoloPag = inizioCalcoloPag;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setInizioCalcoloPag",
						Date.class);

				method.invoke(_ordiniClientiRemoteModel, inizioCalcoloPag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPercentualeScontoMaggiorazione() {
		return _percentualeScontoMaggiorazione;
	}

	@Override
	public void setPercentualeScontoMaggiorazione(
		double percentualeScontoMaggiorazione) {
		_percentualeScontoMaggiorazione = percentualeScontoMaggiorazione;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setPercentualeScontoMaggiorazione",
						double.class);

				method.invoke(_ordiniClientiRemoteModel,
					percentualeScontoMaggiorazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPercentualeScontoProntaCassa() {
		return _percentualeScontoProntaCassa;
	}

	@Override
	public void setPercentualeScontoProntaCassa(
		double percentualeScontoProntaCassa) {
		_percentualeScontoProntaCassa = percentualeScontoProntaCassa;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setPercentualeScontoProntaCassa",
						double.class);

				method.invoke(_ordiniClientiRemoteModel,
					percentualeScontoProntaCassa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPercentualeProvvChiusura() {
		return _percentualeProvvChiusura;
	}

	@Override
	public void setPercentualeProvvChiusura(double percentualeProvvChiusura) {
		_percentualeProvvChiusura = percentualeProvvChiusura;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setPercentualeProvvChiusura",
						double.class);

				method.invoke(_ordiniClientiRemoteModel,
					percentualeProvvChiusura);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataDocumento() {
		return _dataDocumento;
	}

	@Override
	public void setDataDocumento(Date dataDocumento) {
		_dataDocumento = dataDocumento;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataDocumento", Date.class);

				method.invoke(_ordiniClientiRemoteModel, dataDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataRegistrazione() {
		return _dataRegistrazione;
	}

	@Override
	public void setDataRegistrazione(Date dataRegistrazione) {
		_dataRegistrazione = dataRegistrazione;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataRegistrazione",
						Date.class);

				method.invoke(_ordiniClientiRemoteModel, dataRegistrazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCausaleEstrattoConto() {
		return _causaleEstrattoConto;
	}

	@Override
	public void setCausaleEstrattoConto(String causaleEstrattoConto) {
		_causaleEstrattoConto = causaleEstrattoConto;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCausaleEstrattoConto",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, causaleEstrattoConto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataPrimaRata() {
		return _dataPrimaRata;
	}

	@Override
	public void setDataPrimaRata(Date dataPrimaRata) {
		_dataPrimaRata = dataPrimaRata;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataPrimaRata", Date.class);

				method.invoke(_ordiniClientiRemoteModel, dataPrimaRata);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataUltimaRata() {
		return _dataUltimaRata;
	}

	@Override
	public void setDataUltimaRata(Date dataUltimaRata) {
		_dataUltimaRata = dataUltimaRata;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataUltimaRata", Date.class);

				method.invoke(_ordiniClientiRemoteModel, dataUltimaRata);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceBanca() {
		return _codiceBanca;
	}

	@Override
	public void setCodiceBanca(String codiceBanca) {
		_codiceBanca = codiceBanca;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceBanca", String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceBanca);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceAgenzia() {
		return _codiceAgenzia;
	}

	@Override
	public void setCodiceAgenzia(String codiceAgenzia) {
		_codiceAgenzia = codiceAgenzia;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceAgenzia", String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceAgenzia);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceAgente() {
		return _codiceAgente;
	}

	@Override
	public void setCodiceAgente(String codiceAgente) {
		_codiceAgente = codiceAgente;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceAgente", String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceAgente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceGruppoAgenti() {
		return _codiceGruppoAgenti;
	}

	@Override
	public void setCodiceGruppoAgenti(String codiceGruppoAgenti) {
		_codiceGruppoAgenti = codiceGruppoAgenti;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceGruppoAgenti",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceGruppoAgenti);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceZona() {
		return _codiceZona;
	}

	@Override
	public void setCodiceZona(String codiceZona) {
		_codiceZona = codiceZona;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceZona", String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceZona);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceSpedizione() {
		return _codiceSpedizione;
	}

	@Override
	public void setCodiceSpedizione(String codiceSpedizione) {
		_codiceSpedizione = codiceSpedizione;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceSpedizione",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceSpedizione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodicePorto() {
		return _codicePorto;
	}

	@Override
	public void setCodicePorto(String codicePorto) {
		_codicePorto = codicePorto;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodicePorto", String.class);

				method.invoke(_ordiniClientiRemoteModel, codicePorto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDestinatario() {
		return _codiceDestinatario;
	}

	@Override
	public void setCodiceDestinatario(String codiceDestinatario) {
		_codiceDestinatario = codiceDestinatario;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDestinatario",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceDestinatario);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceListino() {
		return _codiceListino;
	}

	@Override
	public void setCodiceListino(String codiceListino) {
		_codiceListino = codiceListino;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceListino", String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceListino);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceLingua() {
		return _codiceLingua;
	}

	@Override
	public void setCodiceLingua(String codiceLingua) {
		_codiceLingua = codiceLingua;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceLingua", String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceLingua);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroDecPrezzo() {
		return _numeroDecPrezzo;
	}

	@Override
	public void setNumeroDecPrezzo(int numeroDecPrezzo) {
		_numeroDecPrezzo = numeroDecPrezzo;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroDecPrezzo", int.class);

				method.invoke(_ordiniClientiRemoteModel, numeroDecPrezzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNote() {
		return _note;
	}

	@Override
	public void setNote(String note) {
		_note = note;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNote", String.class);

				method.invoke(_ordiniClientiRemoteModel, note);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPercentualeSpeseTrasp() {
		return _percentualeSpeseTrasp;
	}

	@Override
	public void setPercentualeSpeseTrasp(double percentualeSpeseTrasp) {
		_percentualeSpeseTrasp = percentualeSpeseTrasp;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setPercentualeSpeseTrasp",
						double.class);

				method.invoke(_ordiniClientiRemoteModel, percentualeSpeseTrasp);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSpeseTrasporto() {
		return _speseTrasporto;
	}

	@Override
	public void setSpeseTrasporto(double speseTrasporto) {
		_speseTrasporto = speseTrasporto;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setSpeseTrasporto",
						double.class);

				method.invoke(_ordiniClientiRemoteModel, speseTrasporto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSpeseImballaggio() {
		return _speseImballaggio;
	}

	@Override
	public void setSpeseImballaggio(double speseImballaggio) {
		_speseImballaggio = speseImballaggio;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setSpeseImballaggio",
						double.class);

				method.invoke(_ordiniClientiRemoteModel, speseImballaggio);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSpeseVarie() {
		return _speseVarie;
	}

	@Override
	public void setSpeseVarie(double speseVarie) {
		_speseVarie = speseVarie;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setSpeseVarie", double.class);

				method.invoke(_ordiniClientiRemoteModel, speseVarie);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSpeseBanca() {
		return _speseBanca;
	}

	@Override
	public void setSpeseBanca(double speseBanca) {
		_speseBanca = speseBanca;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setSpeseBanca", double.class);

				method.invoke(_ordiniClientiRemoteModel, speseBanca);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCuraTrasporto() {
		return _curaTrasporto;
	}

	@Override
	public void setCuraTrasporto(String curaTrasporto) {
		_curaTrasporto = curaTrasporto;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCuraTrasporto", String.class);

				method.invoke(_ordiniClientiRemoteModel, curaTrasporto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCausaleTrasporto() {
		return _causaleTrasporto;
	}

	@Override
	public void setCausaleTrasporto(String causaleTrasporto) {
		_causaleTrasporto = causaleTrasporto;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCausaleTrasporto",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, causaleTrasporto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getAspettoEstriore() {
		return _aspettoEstriore;
	}

	@Override
	public void setAspettoEstriore(String aspettoEstriore) {
		_aspettoEstriore = aspettoEstriore;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setAspettoEstriore",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, aspettoEstriore);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getVettore1() {
		return _vettore1;
	}

	@Override
	public void setVettore1(String vettore1) {
		_vettore1 = vettore1;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setVettore1", String.class);

				method.invoke(_ordiniClientiRemoteModel, vettore1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getVettore2() {
		return _vettore2;
	}

	@Override
	public void setVettore2(String vettore2) {
		_vettore2 = vettore2;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setVettore2", String.class);

				method.invoke(_ordiniClientiRemoteModel, vettore2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getVettore3() {
		return _vettore3;
	}

	@Override
	public void setVettore3(String vettore3) {
		_vettore3 = vettore3;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setVettore3", String.class);

				method.invoke(_ordiniClientiRemoteModel, vettore3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroColli() {
		return _numeroColli;
	}

	@Override
	public void setNumeroColli(int numeroColli) {
		_numeroColli = numeroColli;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroColli", int.class);

				method.invoke(_ordiniClientiRemoteModel, numeroColli);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPesoLordo() {
		return _pesoLordo;
	}

	@Override
	public void setPesoLordo(double pesoLordo) {
		_pesoLordo = pesoLordo;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setPesoLordo", double.class);

				method.invoke(_ordiniClientiRemoteModel, pesoLordo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPesoNetto() {
		return _pesoNetto;
	}

	@Override
	public void setPesoNetto(double pesoNetto) {
		_pesoNetto = pesoNetto;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setPesoNetto", double.class);

				method.invoke(_ordiniClientiRemoteModel, pesoNetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getVolume() {
		return _volume;
	}

	@Override
	public void setVolume(double volume) {
		_volume = volume;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setVolume", double.class);

				method.invoke(_ordiniClientiRemoteModel, volume);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroCopie() {
		return _numeroCopie;
	}

	@Override
	public void setNumeroCopie(int numeroCopie) {
		_numeroCopie = numeroCopie;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroCopie", int.class);

				method.invoke(_ordiniClientiRemoteModel, numeroCopie);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroCopieStampate() {
		return _numeroCopieStampate;
	}

	@Override
	public void setNumeroCopieStampate(int numeroCopieStampate) {
		_numeroCopieStampate = numeroCopieStampate;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroCopieStampate",
						int.class);

				method.invoke(_ordiniClientiRemoteModel, numeroCopieStampate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getInviatoEmail() {
		return _inviatoEmail;
	}

	@Override
	public boolean isInviatoEmail() {
		return _inviatoEmail;
	}

	@Override
	public void setInviatoEmail(boolean inviatoEmail) {
		_inviatoEmail = inviatoEmail;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setInviatoEmail", boolean.class);

				method.invoke(_ordiniClientiRemoteModel, inviatoEmail);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNomePDF() {
		return _nomePDF;
	}

	@Override
	public void setNomePDF(String nomePDF) {
		_nomePDF = nomePDF;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNomePDF", String.class);

				method.invoke(_ordiniClientiRemoteModel, nomePDF);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRiferimentoOrdine() {
		return _riferimentoOrdine;
	}

	@Override
	public void setRiferimentoOrdine(String riferimentoOrdine) {
		_riferimentoOrdine = riferimentoOrdine;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setRiferimentoOrdine",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, riferimentoOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataConferma() {
		return _dataConferma;
	}

	@Override
	public void setDataConferma(Date dataConferma) {
		_dataConferma = dataConferma;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataConferma", Date.class);

				method.invoke(_ordiniClientiRemoteModel, dataConferma);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getConfermaStampata() {
		return _confermaStampata;
	}

	@Override
	public boolean isConfermaStampata() {
		return _confermaStampata;
	}

	@Override
	public void setConfermaStampata(boolean confermaStampata) {
		_confermaStampata = confermaStampata;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setConfermaStampata",
						boolean.class);

				method.invoke(_ordiniClientiRemoteModel, confermaStampata);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getTotaleOrdine() {
		return _totaleOrdine;
	}

	@Override
	public void setTotaleOrdine(double totaleOrdine) {
		_totaleOrdine = totaleOrdine;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTotaleOrdine", double.class);

				method.invoke(_ordiniClientiRemoteModel, totaleOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVATrasp() {
		return _codiceIVATrasp;
	}

	@Override
	public void setCodiceIVATrasp(String codiceIVATrasp) {
		_codiceIVATrasp = codiceIVATrasp;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVATrasp",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceIVATrasp);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVAImballo() {
		return _codiceIVAImballo;
	}

	@Override
	public void setCodiceIVAImballo(String codiceIVAImballo) {
		_codiceIVAImballo = codiceIVAImballo;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVAImballo",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceIVAImballo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVAVarie() {
		return _codiceIVAVarie;
	}

	@Override
	public void setCodiceIVAVarie(String codiceIVAVarie) {
		_codiceIVAVarie = codiceIVAVarie;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVAVarie",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceIVAVarie);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVABanca() {
		return _codiceIVABanca;
	}

	@Override
	public void setCodiceIVABanca(String codiceIVABanca) {
		_codiceIVABanca = codiceIVABanca;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVABanca",
						String.class);

				method.invoke(_ordiniClientiRemoteModel, codiceIVABanca);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr1() {
		return _libStr1;
	}

	@Override
	public void setLibStr1(String libStr1) {
		_libStr1 = libStr1;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr1", String.class);

				method.invoke(_ordiniClientiRemoteModel, libStr1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr2() {
		return _libStr2;
	}

	@Override
	public void setLibStr2(String libStr2) {
		_libStr2 = libStr2;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr2", String.class);

				method.invoke(_ordiniClientiRemoteModel, libStr2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr3() {
		return _libStr3;
	}

	@Override
	public void setLibStr3(String libStr3) {
		_libStr3 = libStr3;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr3", String.class);

				method.invoke(_ordiniClientiRemoteModel, libStr3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl1() {
		return _libDbl1;
	}

	@Override
	public void setLibDbl1(double libDbl1) {
		_libDbl1 = libDbl1;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl1", double.class);

				method.invoke(_ordiniClientiRemoteModel, libDbl1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl2() {
		return _libDbl2;
	}

	@Override
	public void setLibDbl2(double libDbl2) {
		_libDbl2 = libDbl2;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl2", double.class);

				method.invoke(_ordiniClientiRemoteModel, libDbl2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl3() {
		return _libDbl3;
	}

	@Override
	public void setLibDbl3(double libDbl3) {
		_libDbl3 = libDbl3;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl3", double.class);

				method.invoke(_ordiniClientiRemoteModel, libDbl3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat1() {
		return _libDat1;
	}

	@Override
	public void setLibDat1(Date libDat1) {
		_libDat1 = libDat1;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat1", Date.class);

				method.invoke(_ordiniClientiRemoteModel, libDat1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat2() {
		return _libDat2;
	}

	@Override
	public void setLibDat2(Date libDat2) {
		_libDat2 = libDat2;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat2", Date.class);

				method.invoke(_ordiniClientiRemoteModel, libDat2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat3() {
		return _libDat3;
	}

	@Override
	public void setLibDat3(Date libDat3) {
		_libDat3 = libDat3;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat3", Date.class);

				method.invoke(_ordiniClientiRemoteModel, libDat3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng1() {
		return _libLng1;
	}

	@Override
	public void setLibLng1(long libLng1) {
		_libLng1 = libLng1;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng1", long.class);

				method.invoke(_ordiniClientiRemoteModel, libLng1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng2() {
		return _libLng2;
	}

	@Override
	public void setLibLng2(long libLng2) {
		_libLng2 = libLng2;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng2", long.class);

				method.invoke(_ordiniClientiRemoteModel, libLng2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng3() {
		return _libLng3;
	}

	@Override
	public void setLibLng3(long libLng3) {
		_libLng3 = libLng3;

		if (_ordiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _ordiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng3", long.class);

				method.invoke(_ordiniClientiRemoteModel, libLng3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getOrdiniClientiRemoteModel() {
		return _ordiniClientiRemoteModel;
	}

	public void setOrdiniClientiRemoteModel(
		BaseModel<?> ordiniClientiRemoteModel) {
		_ordiniClientiRemoteModel = ordiniClientiRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _ordiniClientiRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_ordiniClientiRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			OrdiniClientiLocalServiceUtil.addOrdiniClienti(this);
		}
		else {
			OrdiniClientiLocalServiceUtil.updateOrdiniClienti(this);
		}
	}

	@Override
	public OrdiniClienti toEscapedModel() {
		return (OrdiniClienti)ProxyUtil.newProxyInstance(OrdiniClienti.class.getClassLoader(),
			new Class[] { OrdiniClienti.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		OrdiniClientiClp clone = new OrdiniClientiClp();

		clone.setAnno(getAnno());
		clone.setCodiceAttivita(getCodiceAttivita());
		clone.setCodiceCentro(getCodiceCentro());
		clone.setCodiceDeposito(getCodiceDeposito());
		clone.setTipoOrdine(getTipoOrdine());
		clone.setNumeroOrdine(getNumeroOrdine());
		clone.setTipoDocumento(getTipoDocumento());
		clone.setCodiceContAnalitica(getCodiceContAnalitica());
		clone.setIdTipoDocumento(getIdTipoDocumento());
		clone.setStatoOrdine(getStatoOrdine());
		clone.setDescrEstremiDoc(getDescrEstremiDoc());
		clone.setTipoSoggetto(getTipoSoggetto());
		clone.setCodiceCliente(getCodiceCliente());
		clone.setNaturaTransazione(getNaturaTransazione());
		clone.setCodiceEsenzione(getCodiceEsenzione());
		clone.setCodiceDivisa(getCodiceDivisa());
		clone.setValoreCambio(getValoreCambio());
		clone.setDataValoreCambio(getDataValoreCambio());
		clone.setCodicePianoPag(getCodicePianoPag());
		clone.setInizioCalcoloPag(getInizioCalcoloPag());
		clone.setPercentualeScontoMaggiorazione(getPercentualeScontoMaggiorazione());
		clone.setPercentualeScontoProntaCassa(getPercentualeScontoProntaCassa());
		clone.setPercentualeProvvChiusura(getPercentualeProvvChiusura());
		clone.setDataDocumento(getDataDocumento());
		clone.setDataRegistrazione(getDataRegistrazione());
		clone.setCausaleEstrattoConto(getCausaleEstrattoConto());
		clone.setDataPrimaRata(getDataPrimaRata());
		clone.setDataUltimaRata(getDataUltimaRata());
		clone.setCodiceBanca(getCodiceBanca());
		clone.setCodiceAgenzia(getCodiceAgenzia());
		clone.setCodiceAgente(getCodiceAgente());
		clone.setCodiceGruppoAgenti(getCodiceGruppoAgenti());
		clone.setCodiceZona(getCodiceZona());
		clone.setCodiceSpedizione(getCodiceSpedizione());
		clone.setCodicePorto(getCodicePorto());
		clone.setCodiceDestinatario(getCodiceDestinatario());
		clone.setCodiceListino(getCodiceListino());
		clone.setCodiceLingua(getCodiceLingua());
		clone.setNumeroDecPrezzo(getNumeroDecPrezzo());
		clone.setNote(getNote());
		clone.setPercentualeSpeseTrasp(getPercentualeSpeseTrasp());
		clone.setSpeseTrasporto(getSpeseTrasporto());
		clone.setSpeseImballaggio(getSpeseImballaggio());
		clone.setSpeseVarie(getSpeseVarie());
		clone.setSpeseBanca(getSpeseBanca());
		clone.setCuraTrasporto(getCuraTrasporto());
		clone.setCausaleTrasporto(getCausaleTrasporto());
		clone.setAspettoEstriore(getAspettoEstriore());
		clone.setVettore1(getVettore1());
		clone.setVettore2(getVettore2());
		clone.setVettore3(getVettore3());
		clone.setNumeroColli(getNumeroColli());
		clone.setPesoLordo(getPesoLordo());
		clone.setPesoNetto(getPesoNetto());
		clone.setVolume(getVolume());
		clone.setNumeroCopie(getNumeroCopie());
		clone.setNumeroCopieStampate(getNumeroCopieStampate());
		clone.setInviatoEmail(getInviatoEmail());
		clone.setNomePDF(getNomePDF());
		clone.setRiferimentoOrdine(getRiferimentoOrdine());
		clone.setDataConferma(getDataConferma());
		clone.setConfermaStampata(getConfermaStampata());
		clone.setTotaleOrdine(getTotaleOrdine());
		clone.setCodiceIVATrasp(getCodiceIVATrasp());
		clone.setCodiceIVAImballo(getCodiceIVAImballo());
		clone.setCodiceIVAVarie(getCodiceIVAVarie());
		clone.setCodiceIVABanca(getCodiceIVABanca());
		clone.setLibStr1(getLibStr1());
		clone.setLibStr2(getLibStr2());
		clone.setLibStr3(getLibStr3());
		clone.setLibDbl1(getLibDbl1());
		clone.setLibDbl2(getLibDbl2());
		clone.setLibDbl3(getLibDbl3());
		clone.setLibDat1(getLibDat1());
		clone.setLibDat2(getLibDat2());
		clone.setLibDat3(getLibDat3());
		clone.setLibLng1(getLibLng1());
		clone.setLibLng2(getLibLng2());
		clone.setLibLng3(getLibLng3());

		return clone;
	}

	@Override
	public int compareTo(OrdiniClienti ordiniClienti) {
		int value = 0;

		if (getAnno() < ordiniClienti.getAnno()) {
			value = -1;
		}
		else if (getAnno() > ordiniClienti.getAnno()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		if (getNumeroOrdine() < ordiniClienti.getNumeroOrdine()) {
			value = -1;
		}
		else if (getNumeroOrdine() > ordiniClienti.getNumeroOrdine()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OrdiniClientiClp)) {
			return false;
		}

		OrdiniClientiClp ordiniClienti = (OrdiniClientiClp)obj;

		OrdiniClientiPK primaryKey = ordiniClienti.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(159);

		sb.append("{anno=");
		sb.append(getAnno());
		sb.append(", codiceAttivita=");
		sb.append(getCodiceAttivita());
		sb.append(", codiceCentro=");
		sb.append(getCodiceCentro());
		sb.append(", codiceDeposito=");
		sb.append(getCodiceDeposito());
		sb.append(", tipoOrdine=");
		sb.append(getTipoOrdine());
		sb.append(", numeroOrdine=");
		sb.append(getNumeroOrdine());
		sb.append(", tipoDocumento=");
		sb.append(getTipoDocumento());
		sb.append(", codiceContAnalitica=");
		sb.append(getCodiceContAnalitica());
		sb.append(", idTipoDocumento=");
		sb.append(getIdTipoDocumento());
		sb.append(", statoOrdine=");
		sb.append(getStatoOrdine());
		sb.append(", descrEstremiDoc=");
		sb.append(getDescrEstremiDoc());
		sb.append(", tipoSoggetto=");
		sb.append(getTipoSoggetto());
		sb.append(", codiceCliente=");
		sb.append(getCodiceCliente());
		sb.append(", naturaTransazione=");
		sb.append(getNaturaTransazione());
		sb.append(", codiceEsenzione=");
		sb.append(getCodiceEsenzione());
		sb.append(", codiceDivisa=");
		sb.append(getCodiceDivisa());
		sb.append(", valoreCambio=");
		sb.append(getValoreCambio());
		sb.append(", dataValoreCambio=");
		sb.append(getDataValoreCambio());
		sb.append(", codicePianoPag=");
		sb.append(getCodicePianoPag());
		sb.append(", inizioCalcoloPag=");
		sb.append(getInizioCalcoloPag());
		sb.append(", percentualeScontoMaggiorazione=");
		sb.append(getPercentualeScontoMaggiorazione());
		sb.append(", percentualeScontoProntaCassa=");
		sb.append(getPercentualeScontoProntaCassa());
		sb.append(", percentualeProvvChiusura=");
		sb.append(getPercentualeProvvChiusura());
		sb.append(", dataDocumento=");
		sb.append(getDataDocumento());
		sb.append(", dataRegistrazione=");
		sb.append(getDataRegistrazione());
		sb.append(", causaleEstrattoConto=");
		sb.append(getCausaleEstrattoConto());
		sb.append(", dataPrimaRata=");
		sb.append(getDataPrimaRata());
		sb.append(", dataUltimaRata=");
		sb.append(getDataUltimaRata());
		sb.append(", codiceBanca=");
		sb.append(getCodiceBanca());
		sb.append(", codiceAgenzia=");
		sb.append(getCodiceAgenzia());
		sb.append(", codiceAgente=");
		sb.append(getCodiceAgente());
		sb.append(", codiceGruppoAgenti=");
		sb.append(getCodiceGruppoAgenti());
		sb.append(", codiceZona=");
		sb.append(getCodiceZona());
		sb.append(", codiceSpedizione=");
		sb.append(getCodiceSpedizione());
		sb.append(", codicePorto=");
		sb.append(getCodicePorto());
		sb.append(", codiceDestinatario=");
		sb.append(getCodiceDestinatario());
		sb.append(", codiceListino=");
		sb.append(getCodiceListino());
		sb.append(", codiceLingua=");
		sb.append(getCodiceLingua());
		sb.append(", numeroDecPrezzo=");
		sb.append(getNumeroDecPrezzo());
		sb.append(", note=");
		sb.append(getNote());
		sb.append(", percentualeSpeseTrasp=");
		sb.append(getPercentualeSpeseTrasp());
		sb.append(", speseTrasporto=");
		sb.append(getSpeseTrasporto());
		sb.append(", speseImballaggio=");
		sb.append(getSpeseImballaggio());
		sb.append(", speseVarie=");
		sb.append(getSpeseVarie());
		sb.append(", speseBanca=");
		sb.append(getSpeseBanca());
		sb.append(", curaTrasporto=");
		sb.append(getCuraTrasporto());
		sb.append(", causaleTrasporto=");
		sb.append(getCausaleTrasporto());
		sb.append(", aspettoEstriore=");
		sb.append(getAspettoEstriore());
		sb.append(", vettore1=");
		sb.append(getVettore1());
		sb.append(", vettore2=");
		sb.append(getVettore2());
		sb.append(", vettore3=");
		sb.append(getVettore3());
		sb.append(", numeroColli=");
		sb.append(getNumeroColli());
		sb.append(", pesoLordo=");
		sb.append(getPesoLordo());
		sb.append(", pesoNetto=");
		sb.append(getPesoNetto());
		sb.append(", volume=");
		sb.append(getVolume());
		sb.append(", numeroCopie=");
		sb.append(getNumeroCopie());
		sb.append(", numeroCopieStampate=");
		sb.append(getNumeroCopieStampate());
		sb.append(", inviatoEmail=");
		sb.append(getInviatoEmail());
		sb.append(", nomePDF=");
		sb.append(getNomePDF());
		sb.append(", riferimentoOrdine=");
		sb.append(getRiferimentoOrdine());
		sb.append(", dataConferma=");
		sb.append(getDataConferma());
		sb.append(", confermaStampata=");
		sb.append(getConfermaStampata());
		sb.append(", totaleOrdine=");
		sb.append(getTotaleOrdine());
		sb.append(", codiceIVATrasp=");
		sb.append(getCodiceIVATrasp());
		sb.append(", codiceIVAImballo=");
		sb.append(getCodiceIVAImballo());
		sb.append(", codiceIVAVarie=");
		sb.append(getCodiceIVAVarie());
		sb.append(", codiceIVABanca=");
		sb.append(getCodiceIVABanca());
		sb.append(", libStr1=");
		sb.append(getLibStr1());
		sb.append(", libStr2=");
		sb.append(getLibStr2());
		sb.append(", libStr3=");
		sb.append(getLibStr3());
		sb.append(", libDbl1=");
		sb.append(getLibDbl1());
		sb.append(", libDbl2=");
		sb.append(getLibDbl2());
		sb.append(", libDbl3=");
		sb.append(getLibDbl3());
		sb.append(", libDat1=");
		sb.append(getLibDat1());
		sb.append(", libDat2=");
		sb.append(getLibDat2());
		sb.append(", libDat3=");
		sb.append(getLibDat3());
		sb.append(", libLng1=");
		sb.append(getLibLng1());
		sb.append(", libLng2=");
		sb.append(getLibLng2());
		sb.append(", libLng3=");
		sb.append(getLibLng3());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(241);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.OrdiniClienti");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>anno</column-name><column-value><![CDATA[");
		sb.append(getAnno());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceAttivita</column-name><column-value><![CDATA[");
		sb.append(getCodiceAttivita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCentro</column-name><column-value><![CDATA[");
		sb.append(getCodiceCentro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDeposito</column-name><column-value><![CDATA[");
		sb.append(getCodiceDeposito());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoOrdine</column-name><column-value><![CDATA[");
		sb.append(getTipoOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroOrdine</column-name><column-value><![CDATA[");
		sb.append(getNumeroOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoDocumento</column-name><column-value><![CDATA[");
		sb.append(getTipoDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceContAnalitica</column-name><column-value><![CDATA[");
		sb.append(getCodiceContAnalitica());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idTipoDocumento</column-name><column-value><![CDATA[");
		sb.append(getIdTipoDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statoOrdine</column-name><column-value><![CDATA[");
		sb.append(getStatoOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrEstremiDoc</column-name><column-value><![CDATA[");
		sb.append(getDescrEstremiDoc());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoSoggetto</column-name><column-value><![CDATA[");
		sb.append(getTipoSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCliente</column-name><column-value><![CDATA[");
		sb.append(getCodiceCliente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>naturaTransazione</column-name><column-value><![CDATA[");
		sb.append(getNaturaTransazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceEsenzione</column-name><column-value><![CDATA[");
		sb.append(getCodiceEsenzione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDivisa</column-name><column-value><![CDATA[");
		sb.append(getCodiceDivisa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>valoreCambio</column-name><column-value><![CDATA[");
		sb.append(getValoreCambio());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataValoreCambio</column-name><column-value><![CDATA[");
		sb.append(getDataValoreCambio());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codicePianoPag</column-name><column-value><![CDATA[");
		sb.append(getCodicePianoPag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>inizioCalcoloPag</column-name><column-value><![CDATA[");
		sb.append(getInizioCalcoloPag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>percentualeScontoMaggiorazione</column-name><column-value><![CDATA[");
		sb.append(getPercentualeScontoMaggiorazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>percentualeScontoProntaCassa</column-name><column-value><![CDATA[");
		sb.append(getPercentualeScontoProntaCassa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>percentualeProvvChiusura</column-name><column-value><![CDATA[");
		sb.append(getPercentualeProvvChiusura());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataDocumento</column-name><column-value><![CDATA[");
		sb.append(getDataDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataRegistrazione</column-name><column-value><![CDATA[");
		sb.append(getDataRegistrazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>causaleEstrattoConto</column-name><column-value><![CDATA[");
		sb.append(getCausaleEstrattoConto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataPrimaRata</column-name><column-value><![CDATA[");
		sb.append(getDataPrimaRata());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataUltimaRata</column-name><column-value><![CDATA[");
		sb.append(getDataUltimaRata());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceBanca</column-name><column-value><![CDATA[");
		sb.append(getCodiceBanca());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceAgenzia</column-name><column-value><![CDATA[");
		sb.append(getCodiceAgenzia());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceAgente</column-name><column-value><![CDATA[");
		sb.append(getCodiceAgente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceGruppoAgenti</column-name><column-value><![CDATA[");
		sb.append(getCodiceGruppoAgenti());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceZona</column-name><column-value><![CDATA[");
		sb.append(getCodiceZona());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceSpedizione</column-name><column-value><![CDATA[");
		sb.append(getCodiceSpedizione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codicePorto</column-name><column-value><![CDATA[");
		sb.append(getCodicePorto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDestinatario</column-name><column-value><![CDATA[");
		sb.append(getCodiceDestinatario());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceListino</column-name><column-value><![CDATA[");
		sb.append(getCodiceListino());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceLingua</column-name><column-value><![CDATA[");
		sb.append(getCodiceLingua());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroDecPrezzo</column-name><column-value><![CDATA[");
		sb.append(getNumeroDecPrezzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>note</column-name><column-value><![CDATA[");
		sb.append(getNote());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>percentualeSpeseTrasp</column-name><column-value><![CDATA[");
		sb.append(getPercentualeSpeseTrasp());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>speseTrasporto</column-name><column-value><![CDATA[");
		sb.append(getSpeseTrasporto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>speseImballaggio</column-name><column-value><![CDATA[");
		sb.append(getSpeseImballaggio());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>speseVarie</column-name><column-value><![CDATA[");
		sb.append(getSpeseVarie());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>speseBanca</column-name><column-value><![CDATA[");
		sb.append(getSpeseBanca());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>curaTrasporto</column-name><column-value><![CDATA[");
		sb.append(getCuraTrasporto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>causaleTrasporto</column-name><column-value><![CDATA[");
		sb.append(getCausaleTrasporto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>aspettoEstriore</column-name><column-value><![CDATA[");
		sb.append(getAspettoEstriore());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>vettore1</column-name><column-value><![CDATA[");
		sb.append(getVettore1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>vettore2</column-name><column-value><![CDATA[");
		sb.append(getVettore2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>vettore3</column-name><column-value><![CDATA[");
		sb.append(getVettore3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroColli</column-name><column-value><![CDATA[");
		sb.append(getNumeroColli());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>pesoLordo</column-name><column-value><![CDATA[");
		sb.append(getPesoLordo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>pesoNetto</column-name><column-value><![CDATA[");
		sb.append(getPesoNetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>volume</column-name><column-value><![CDATA[");
		sb.append(getVolume());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroCopie</column-name><column-value><![CDATA[");
		sb.append(getNumeroCopie());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroCopieStampate</column-name><column-value><![CDATA[");
		sb.append(getNumeroCopieStampate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>inviatoEmail</column-name><column-value><![CDATA[");
		sb.append(getInviatoEmail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nomePDF</column-name><column-value><![CDATA[");
		sb.append(getNomePDF());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>riferimentoOrdine</column-name><column-value><![CDATA[");
		sb.append(getRiferimentoOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataConferma</column-name><column-value><![CDATA[");
		sb.append(getDataConferma());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>confermaStampata</column-name><column-value><![CDATA[");
		sb.append(getConfermaStampata());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>totaleOrdine</column-name><column-value><![CDATA[");
		sb.append(getTotaleOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVATrasp</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVATrasp());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVAImballo</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVAImballo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVAVarie</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVAVarie());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVABanca</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVABanca());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr1</column-name><column-value><![CDATA[");
		sb.append(getLibStr1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr2</column-name><column-value><![CDATA[");
		sb.append(getLibStr2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr3</column-name><column-value><![CDATA[");
		sb.append(getLibStr3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl1</column-name><column-value><![CDATA[");
		sb.append(getLibDbl1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl2</column-name><column-value><![CDATA[");
		sb.append(getLibDbl2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl3</column-name><column-value><![CDATA[");
		sb.append(getLibDbl3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat1</column-name><column-value><![CDATA[");
		sb.append(getLibDat1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat2</column-name><column-value><![CDATA[");
		sb.append(getLibDat2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat3</column-name><column-value><![CDATA[");
		sb.append(getLibDat3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng1</column-name><column-value><![CDATA[");
		sb.append(getLibLng1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng2</column-name><column-value><![CDATA[");
		sb.append(getLibLng2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng3</column-name><column-value><![CDATA[");
		sb.append(getLibLng3());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _anno;
	private String _codiceAttivita;
	private String _codiceCentro;
	private String _codiceDeposito;
	private int _tipoOrdine;
	private int _numeroOrdine;
	private String _tipoDocumento;
	private String _codiceContAnalitica;
	private int _idTipoDocumento;
	private boolean _statoOrdine;
	private String _descrEstremiDoc;
	private boolean _tipoSoggetto;
	private String _codiceCliente;
	private String _naturaTransazione;
	private String _codiceEsenzione;
	private String _codiceDivisa;
	private double _valoreCambio;
	private Date _dataValoreCambio;
	private String _codicePianoPag;
	private Date _inizioCalcoloPag;
	private double _percentualeScontoMaggiorazione;
	private double _percentualeScontoProntaCassa;
	private double _percentualeProvvChiusura;
	private Date _dataDocumento;
	private Date _dataRegistrazione;
	private String _causaleEstrattoConto;
	private Date _dataPrimaRata;
	private Date _dataUltimaRata;
	private String _codiceBanca;
	private String _codiceAgenzia;
	private String _codiceAgente;
	private String _codiceGruppoAgenti;
	private String _codiceZona;
	private String _codiceSpedizione;
	private String _codicePorto;
	private String _codiceDestinatario;
	private String _codiceListino;
	private String _codiceLingua;
	private int _numeroDecPrezzo;
	private String _note;
	private double _percentualeSpeseTrasp;
	private double _speseTrasporto;
	private double _speseImballaggio;
	private double _speseVarie;
	private double _speseBanca;
	private String _curaTrasporto;
	private String _causaleTrasporto;
	private String _aspettoEstriore;
	private String _vettore1;
	private String _vettore2;
	private String _vettore3;
	private int _numeroColli;
	private double _pesoLordo;
	private double _pesoNetto;
	private double _volume;
	private int _numeroCopie;
	private int _numeroCopieStampate;
	private boolean _inviatoEmail;
	private String _nomePDF;
	private String _riferimentoOrdine;
	private Date _dataConferma;
	private boolean _confermaStampata;
	private double _totaleOrdine;
	private String _codiceIVATrasp;
	private String _codiceIVAImballo;
	private String _codiceIVAVarie;
	private String _codiceIVABanca;
	private String _libStr1;
	private String _libStr2;
	private String _libStr3;
	private double _libDbl1;
	private double _libDbl2;
	private double _libDbl3;
	private Date _libDat1;
	private Date _libDat2;
	private Date _libDat3;
	private long _libLng1;
	private long _libLng2;
	private long _libLng3;
	private BaseModel<?> _ordiniClientiRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}