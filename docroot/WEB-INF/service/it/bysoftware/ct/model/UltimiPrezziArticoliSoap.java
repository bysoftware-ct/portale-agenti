/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.UltimiPrezziArticoliPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.UltimiPrezziArticoliServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.UltimiPrezziArticoliServiceSoap
 * @generated
 */
public class UltimiPrezziArticoliSoap implements Serializable {
	public static UltimiPrezziArticoliSoap toSoapModel(
		UltimiPrezziArticoli model) {
		UltimiPrezziArticoliSoap soapModel = new UltimiPrezziArticoliSoap();

		soapModel.setTipoSoggetto(model.getTipoSoggetto());
		soapModel.setCodiceSoggetto(model.getCodiceSoggetto());
		soapModel.setCodiceArticolo(model.getCodiceArticolo());
		soapModel.setCodiceVariante(model.getCodiceVariante());
		soapModel.setDataDocumento(model.getDataDocumento());
		soapModel.setCodiceDivisa(model.getCodiceDivisa());
		soapModel.setPrezzo(model.getPrezzo());
		soapModel.setSconto1(model.getSconto1());
		soapModel.setSconto2(model.getSconto2());
		soapModel.setSconto3(model.getSconto3());
		soapModel.setScontoChiusura(model.getScontoChiusura());

		return soapModel;
	}

	public static UltimiPrezziArticoliSoap[] toSoapModels(
		UltimiPrezziArticoli[] models) {
		UltimiPrezziArticoliSoap[] soapModels = new UltimiPrezziArticoliSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UltimiPrezziArticoliSoap[][] toSoapModels(
		UltimiPrezziArticoli[][] models) {
		UltimiPrezziArticoliSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UltimiPrezziArticoliSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UltimiPrezziArticoliSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UltimiPrezziArticoliSoap[] toSoapModels(
		List<UltimiPrezziArticoli> models) {
		List<UltimiPrezziArticoliSoap> soapModels = new ArrayList<UltimiPrezziArticoliSoap>(models.size());

		for (UltimiPrezziArticoli model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UltimiPrezziArticoliSoap[soapModels.size()]);
	}

	public UltimiPrezziArticoliSoap() {
	}

	public UltimiPrezziArticoliPK getPrimaryKey() {
		return new UltimiPrezziArticoliPK(_tipoSoggetto, _codiceSoggetto,
			_codiceArticolo, _codiceVariante);
	}

	public void setPrimaryKey(UltimiPrezziArticoliPK pk) {
		setTipoSoggetto(pk.tipoSoggetto);
		setCodiceSoggetto(pk.codiceSoggetto);
		setCodiceArticolo(pk.codiceArticolo);
		setCodiceVariante(pk.codiceVariante);
	}

	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;
	}

	public String getCodiceSoggetto() {
		return _codiceSoggetto;
	}

	public void setCodiceSoggetto(String codiceSoggetto) {
		_codiceSoggetto = codiceSoggetto;
	}

	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;
	}

	public String getCodiceVariante() {
		return _codiceVariante;
	}

	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;
	}

	public Date getDataDocumento() {
		return _dataDocumento;
	}

	public void setDataDocumento(Date dataDocumento) {
		_dataDocumento = dataDocumento;
	}

	public String getCodiceDivisa() {
		return _codiceDivisa;
	}

	public void setCodiceDivisa(String codiceDivisa) {
		_codiceDivisa = codiceDivisa;
	}

	public double getPrezzo() {
		return _prezzo;
	}

	public void setPrezzo(double prezzo) {
		_prezzo = prezzo;
	}

	public double getSconto1() {
		return _sconto1;
	}

	public void setSconto1(double sconto1) {
		_sconto1 = sconto1;
	}

	public double getSconto2() {
		return _sconto2;
	}

	public void setSconto2(double sconto2) {
		_sconto2 = sconto2;
	}

	public double getSconto3() {
		return _sconto3;
	}

	public void setSconto3(double sconto3) {
		_sconto3 = sconto3;
	}

	public double getScontoChiusura() {
		return _scontoChiusura;
	}

	public void setScontoChiusura(double scontoChiusura) {
		_scontoChiusura = scontoChiusura;
	}

	private boolean _tipoSoggetto;
	private String _codiceSoggetto;
	private String _codiceArticolo;
	private String _codiceVariante;
	private Date _dataDocumento;
	private String _codiceDivisa;
	private double _prezzo;
	private double _sconto1;
	private double _sconto2;
	private double _sconto3;
	private double _scontoChiusura;
}