/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.AnagraficaClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class AnagraficaClientiFornitoriClp extends BaseModelImpl<AnagraficaClientiFornitori>
	implements AnagraficaClientiFornitori {
	public AnagraficaClientiFornitoriClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return AnagraficaClientiFornitori.class;
	}

	@Override
	public String getModelClassName() {
		return AnagraficaClientiFornitori.class.getName();
	}

	@Override
	public String getPrimaryKey() {
		return _codiceSoggetto;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setCodiceSoggetto(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _codiceSoggetto;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codiceSoggetto", getCodiceSoggetto());
		attributes.put("ragioneSociale", getRagioneSociale());
		attributes.put("ragioneSocialeAggiuntiva", getRagioneSocialeAggiuntiva());
		attributes.put("indirizzo", getIndirizzo());
		attributes.put("comune", getComune());
		attributes.put("CAP", getCAP());
		attributes.put("siglaProvincia", getSiglaProvincia());
		attributes.put("siglaStato", getSiglaStato());
		attributes.put("partitaIVA", getPartitaIVA());
		attributes.put("codiceFiscale", getCodiceFiscale());
		attributes.put("note", getNote());
		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceMnemonico", getCodiceMnemonico());
		attributes.put("tipoSollecito", getTipoSollecito());
		attributes.put("attivoEC", getAttivoEC());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codiceSoggetto = (String)attributes.get("codiceSoggetto");

		if (codiceSoggetto != null) {
			setCodiceSoggetto(codiceSoggetto);
		}

		String ragioneSociale = (String)attributes.get("ragioneSociale");

		if (ragioneSociale != null) {
			setRagioneSociale(ragioneSociale);
		}

		String ragioneSocialeAggiuntiva = (String)attributes.get(
				"ragioneSocialeAggiuntiva");

		if (ragioneSocialeAggiuntiva != null) {
			setRagioneSocialeAggiuntiva(ragioneSocialeAggiuntiva);
		}

		String indirizzo = (String)attributes.get("indirizzo");

		if (indirizzo != null) {
			setIndirizzo(indirizzo);
		}

		String comune = (String)attributes.get("comune");

		if (comune != null) {
			setComune(comune);
		}

		String CAP = (String)attributes.get("CAP");

		if (CAP != null) {
			setCAP(CAP);
		}

		String siglaProvincia = (String)attributes.get("siglaProvincia");

		if (siglaProvincia != null) {
			setSiglaProvincia(siglaProvincia);
		}

		String siglaStato = (String)attributes.get("siglaStato");

		if (siglaStato != null) {
			setSiglaStato(siglaStato);
		}

		String partitaIVA = (String)attributes.get("partitaIVA");

		if (partitaIVA != null) {
			setPartitaIVA(partitaIVA);
		}

		String codiceFiscale = (String)attributes.get("codiceFiscale");

		if (codiceFiscale != null) {
			setCodiceFiscale(codiceFiscale);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}

		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceMnemonico = (String)attributes.get("codiceMnemonico");

		if (codiceMnemonico != null) {
			setCodiceMnemonico(codiceMnemonico);
		}

		Integer tipoSollecito = (Integer)attributes.get("tipoSollecito");

		if (tipoSollecito != null) {
			setTipoSollecito(tipoSollecito);
		}

		Boolean attivoEC = (Boolean)attributes.get("attivoEC");

		if (attivoEC != null) {
			setAttivoEC(attivoEC);
		}
	}

	@Override
	public String getCodiceSoggetto() {
		return _codiceSoggetto;
	}

	@Override
	public void setCodiceSoggetto(String codiceSoggetto) {
		_codiceSoggetto = codiceSoggetto;

		if (_anagraficaClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceSoggetto",
						String.class);

				method.invoke(_anagraficaClientiFornitoriRemoteModel,
					codiceSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRagioneSociale() {
		return _ragioneSociale;
	}

	@Override
	public void setRagioneSociale(String ragioneSociale) {
		_ragioneSociale = ragioneSociale;

		if (_anagraficaClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setRagioneSociale",
						String.class);

				method.invoke(_anagraficaClientiFornitoriRemoteModel,
					ragioneSociale);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRagioneSocialeAggiuntiva() {
		return _ragioneSocialeAggiuntiva;
	}

	@Override
	public void setRagioneSocialeAggiuntiva(String ragioneSocialeAggiuntiva) {
		_ragioneSocialeAggiuntiva = ragioneSocialeAggiuntiva;

		if (_anagraficaClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setRagioneSocialeAggiuntiva",
						String.class);

				method.invoke(_anagraficaClientiFornitoriRemoteModel,
					ragioneSocialeAggiuntiva);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIndirizzo() {
		return _indirizzo;
	}

	@Override
	public void setIndirizzo(String indirizzo) {
		_indirizzo = indirizzo;

		if (_anagraficaClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setIndirizzo", String.class);

				method.invoke(_anagraficaClientiFornitoriRemoteModel, indirizzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getComune() {
		return _comune;
	}

	@Override
	public void setComune(String comune) {
		_comune = comune;

		if (_anagraficaClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setComune", String.class);

				method.invoke(_anagraficaClientiFornitoriRemoteModel, comune);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCAP() {
		return _CAP;
	}

	@Override
	public void setCAP(String CAP) {
		_CAP = CAP;

		if (_anagraficaClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCAP", String.class);

				method.invoke(_anagraficaClientiFornitoriRemoteModel, CAP);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getSiglaProvincia() {
		return _siglaProvincia;
	}

	@Override
	public void setSiglaProvincia(String siglaProvincia) {
		_siglaProvincia = siglaProvincia;

		if (_anagraficaClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setSiglaProvincia",
						String.class);

				method.invoke(_anagraficaClientiFornitoriRemoteModel,
					siglaProvincia);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getSiglaStato() {
		return _siglaStato;
	}

	@Override
	public void setSiglaStato(String siglaStato) {
		_siglaStato = siglaStato;

		if (_anagraficaClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setSiglaStato", String.class);

				method.invoke(_anagraficaClientiFornitoriRemoteModel, siglaStato);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPartitaIVA() {
		return _partitaIVA;
	}

	@Override
	public void setPartitaIVA(String partitaIVA) {
		_partitaIVA = partitaIVA;

		if (_anagraficaClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setPartitaIVA", String.class);

				method.invoke(_anagraficaClientiFornitoriRemoteModel, partitaIVA);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceFiscale() {
		return _codiceFiscale;
	}

	@Override
	public void setCodiceFiscale(String codiceFiscale) {
		_codiceFiscale = codiceFiscale;

		if (_anagraficaClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceFiscale", String.class);

				method.invoke(_anagraficaClientiFornitoriRemoteModel,
					codiceFiscale);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNote() {
		return _note;
	}

	@Override
	public void setNote(String note) {
		_note = note;

		if (_anagraficaClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setNote", String.class);

				method.invoke(_anagraficaClientiFornitoriRemoteModel, note);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;

		if (_anagraficaClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoSoggetto", boolean.class);

				method.invoke(_anagraficaClientiFornitoriRemoteModel,
					tipoSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceMnemonico() {
		return _codiceMnemonico;
	}

	@Override
	public void setCodiceMnemonico(String codiceMnemonico) {
		_codiceMnemonico = codiceMnemonico;

		if (_anagraficaClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceMnemonico",
						String.class);

				method.invoke(_anagraficaClientiFornitoriRemoteModel,
					codiceMnemonico);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoSollecito() {
		return _tipoSollecito;
	}

	@Override
	public void setTipoSollecito(int tipoSollecito) {
		_tipoSollecito = tipoSollecito;

		if (_anagraficaClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoSollecito", int.class);

				method.invoke(_anagraficaClientiFornitoriRemoteModel,
					tipoSollecito);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getAttivoEC() {
		return _attivoEC;
	}

	@Override
	public boolean isAttivoEC() {
		return _attivoEC;
	}

	@Override
	public void setAttivoEC(boolean attivoEC) {
		_attivoEC = attivoEC;

		if (_anagraficaClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setAttivoEC", boolean.class);

				method.invoke(_anagraficaClientiFornitoriRemoteModel, attivoEC);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getAnagraficaClientiFornitoriRemoteModel() {
		return _anagraficaClientiFornitoriRemoteModel;
	}

	public void setAnagraficaClientiFornitoriRemoteModel(
		BaseModel<?> anagraficaClientiFornitoriRemoteModel) {
		_anagraficaClientiFornitoriRemoteModel = anagraficaClientiFornitoriRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _anagraficaClientiFornitoriRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_anagraficaClientiFornitoriRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			AnagraficaClientiFornitoriLocalServiceUtil.addAnagraficaClientiFornitori(this);
		}
		else {
			AnagraficaClientiFornitoriLocalServiceUtil.updateAnagraficaClientiFornitori(this);
		}
	}

	@Override
	public AnagraficaClientiFornitori toEscapedModel() {
		return (AnagraficaClientiFornitori)ProxyUtil.newProxyInstance(AnagraficaClientiFornitori.class.getClassLoader(),
			new Class[] { AnagraficaClientiFornitori.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		AnagraficaClientiFornitoriClp clone = new AnagraficaClientiFornitoriClp();

		clone.setCodiceSoggetto(getCodiceSoggetto());
		clone.setRagioneSociale(getRagioneSociale());
		clone.setRagioneSocialeAggiuntiva(getRagioneSocialeAggiuntiva());
		clone.setIndirizzo(getIndirizzo());
		clone.setComune(getComune());
		clone.setCAP(getCAP());
		clone.setSiglaProvincia(getSiglaProvincia());
		clone.setSiglaStato(getSiglaStato());
		clone.setPartitaIVA(getPartitaIVA());
		clone.setCodiceFiscale(getCodiceFiscale());
		clone.setNote(getNote());
		clone.setTipoSoggetto(getTipoSoggetto());
		clone.setCodiceMnemonico(getCodiceMnemonico());
		clone.setTipoSollecito(getTipoSollecito());
		clone.setAttivoEC(getAttivoEC());

		return clone;
	}

	@Override
	public int compareTo(AnagraficaClientiFornitori anagraficaClientiFornitori) {
		String primaryKey = anagraficaClientiFornitori.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AnagraficaClientiFornitoriClp)) {
			return false;
		}

		AnagraficaClientiFornitoriClp anagraficaClientiFornitori = (AnagraficaClientiFornitoriClp)obj;

		String primaryKey = anagraficaClientiFornitori.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{codiceSoggetto=");
		sb.append(getCodiceSoggetto());
		sb.append(", ragioneSociale=");
		sb.append(getRagioneSociale());
		sb.append(", ragioneSocialeAggiuntiva=");
		sb.append(getRagioneSocialeAggiuntiva());
		sb.append(", indirizzo=");
		sb.append(getIndirizzo());
		sb.append(", comune=");
		sb.append(getComune());
		sb.append(", CAP=");
		sb.append(getCAP());
		sb.append(", siglaProvincia=");
		sb.append(getSiglaProvincia());
		sb.append(", siglaStato=");
		sb.append(getSiglaStato());
		sb.append(", partitaIVA=");
		sb.append(getPartitaIVA());
		sb.append(", codiceFiscale=");
		sb.append(getCodiceFiscale());
		sb.append(", note=");
		sb.append(getNote());
		sb.append(", tipoSoggetto=");
		sb.append(getTipoSoggetto());
		sb.append(", codiceMnemonico=");
		sb.append(getCodiceMnemonico());
		sb.append(", tipoSollecito=");
		sb.append(getTipoSollecito());
		sb.append(", attivoEC=");
		sb.append(getAttivoEC());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(49);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.AnagraficaClientiFornitori");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>codiceSoggetto</column-name><column-value><![CDATA[");
		sb.append(getCodiceSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ragioneSociale</column-name><column-value><![CDATA[");
		sb.append(getRagioneSociale());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ragioneSocialeAggiuntiva</column-name><column-value><![CDATA[");
		sb.append(getRagioneSocialeAggiuntiva());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>indirizzo</column-name><column-value><![CDATA[");
		sb.append(getIndirizzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>comune</column-name><column-value><![CDATA[");
		sb.append(getComune());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>CAP</column-name><column-value><![CDATA[");
		sb.append(getCAP());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>siglaProvincia</column-name><column-value><![CDATA[");
		sb.append(getSiglaProvincia());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>siglaStato</column-name><column-value><![CDATA[");
		sb.append(getSiglaStato());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>partitaIVA</column-name><column-value><![CDATA[");
		sb.append(getPartitaIVA());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceFiscale</column-name><column-value><![CDATA[");
		sb.append(getCodiceFiscale());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>note</column-name><column-value><![CDATA[");
		sb.append(getNote());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoSoggetto</column-name><column-value><![CDATA[");
		sb.append(getTipoSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceMnemonico</column-name><column-value><![CDATA[");
		sb.append(getCodiceMnemonico());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoSollecito</column-name><column-value><![CDATA[");
		sb.append(getTipoSollecito());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>attivoEC</column-name><column-value><![CDATA[");
		sb.append(getAttivoEC());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _codiceSoggetto;
	private String _ragioneSociale;
	private String _ragioneSocialeAggiuntiva;
	private String _indirizzo;
	private String _comune;
	private String _CAP;
	private String _siglaProvincia;
	private String _siglaStato;
	private String _partitaIVA;
	private String _codiceFiscale;
	private String _note;
	private boolean _tipoSoggetto;
	private String _codiceMnemonico;
	private int _tipoSollecito;
	private boolean _attivoEC;
	private BaseModel<?> _anagraficaClientiFornitoriRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}