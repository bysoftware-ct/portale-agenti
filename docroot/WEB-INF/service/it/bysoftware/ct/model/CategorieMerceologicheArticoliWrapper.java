/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CategorieMerceologicheArticoli}.
 * </p>
 *
 * @author Mario Torrisi
 * @see CategorieMerceologicheArticoli
 * @generated
 */
public class CategorieMerceologicheArticoliWrapper
	implements CategorieMerceologicheArticoli,
		ModelWrapper<CategorieMerceologicheArticoli> {
	public CategorieMerceologicheArticoliWrapper(
		CategorieMerceologicheArticoli categorieMerceologicheArticoli) {
		_categorieMerceologicheArticoli = categorieMerceologicheArticoli;
	}

	@Override
	public Class<?> getModelClass() {
		return CategorieMerceologicheArticoli.class;
	}

	@Override
	public String getModelClassName() {
		return CategorieMerceologicheArticoli.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codiceCategoria", getCodiceCategoria());
		attributes.put("descrizione", getDescrizione());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codiceCategoria = (String)attributes.get("codiceCategoria");

		if (codiceCategoria != null) {
			setCodiceCategoria(codiceCategoria);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}
	}

	/**
	* Returns the primary key of this categorie merceologiche articoli.
	*
	* @return the primary key of this categorie merceologiche articoli
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _categorieMerceologicheArticoli.getPrimaryKey();
	}

	/**
	* Sets the primary key of this categorie merceologiche articoli.
	*
	* @param primaryKey the primary key of this categorie merceologiche articoli
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_categorieMerceologicheArticoli.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the codice categoria of this categorie merceologiche articoli.
	*
	* @return the codice categoria of this categorie merceologiche articoli
	*/
	@Override
	public java.lang.String getCodiceCategoria() {
		return _categorieMerceologicheArticoli.getCodiceCategoria();
	}

	/**
	* Sets the codice categoria of this categorie merceologiche articoli.
	*
	* @param codiceCategoria the codice categoria of this categorie merceologiche articoli
	*/
	@Override
	public void setCodiceCategoria(java.lang.String codiceCategoria) {
		_categorieMerceologicheArticoli.setCodiceCategoria(codiceCategoria);
	}

	/**
	* Returns the descrizione of this categorie merceologiche articoli.
	*
	* @return the descrizione of this categorie merceologiche articoli
	*/
	@Override
	public java.lang.String getDescrizione() {
		return _categorieMerceologicheArticoli.getDescrizione();
	}

	/**
	* Sets the descrizione of this categorie merceologiche articoli.
	*
	* @param descrizione the descrizione of this categorie merceologiche articoli
	*/
	@Override
	public void setDescrizione(java.lang.String descrizione) {
		_categorieMerceologicheArticoli.setDescrizione(descrizione);
	}

	@Override
	public boolean isNew() {
		return _categorieMerceologicheArticoli.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_categorieMerceologicheArticoli.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _categorieMerceologicheArticoli.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_categorieMerceologicheArticoli.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _categorieMerceologicheArticoli.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _categorieMerceologicheArticoli.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_categorieMerceologicheArticoli.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _categorieMerceologicheArticoli.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_categorieMerceologicheArticoli.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_categorieMerceologicheArticoli.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_categorieMerceologicheArticoli.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CategorieMerceologicheArticoliWrapper((CategorieMerceologicheArticoli)_categorieMerceologicheArticoli.clone());
	}

	@Override
	public int compareTo(
		it.bysoftware.ct.model.CategorieMerceologicheArticoli categorieMerceologicheArticoli) {
		return _categorieMerceologicheArticoli.compareTo(categorieMerceologicheArticoli);
	}

	@Override
	public int hashCode() {
		return _categorieMerceologicheArticoli.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.CategorieMerceologicheArticoli> toCacheModel() {
		return _categorieMerceologicheArticoli.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.CategorieMerceologicheArticoli toEscapedModel() {
		return new CategorieMerceologicheArticoliWrapper(_categorieMerceologicheArticoli.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.CategorieMerceologicheArticoli toUnescapedModel() {
		return new CategorieMerceologicheArticoliWrapper(_categorieMerceologicheArticoli.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _categorieMerceologicheArticoli.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _categorieMerceologicheArticoli.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_categorieMerceologicheArticoli.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CategorieMerceologicheArticoliWrapper)) {
			return false;
		}

		CategorieMerceologicheArticoliWrapper categorieMerceologicheArticoliWrapper =
			(CategorieMerceologicheArticoliWrapper)obj;

		if (Validator.equals(_categorieMerceologicheArticoli,
					categorieMerceologicheArticoliWrapper._categorieMerceologicheArticoli)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CategorieMerceologicheArticoli getWrappedCategorieMerceologicheArticoli() {
		return _categorieMerceologicheArticoli;
	}

	@Override
	public CategorieMerceologicheArticoli getWrappedModel() {
		return _categorieMerceologicheArticoli;
	}

	@Override
	public void resetOriginalValues() {
		_categorieMerceologicheArticoli.resetOriginalValues();
	}

	private CategorieMerceologicheArticoli _categorieMerceologicheArticoli;
}