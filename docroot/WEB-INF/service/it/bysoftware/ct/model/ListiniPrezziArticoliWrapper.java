/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ListiniPrezziArticoli}.
 * </p>
 *
 * @author Mario Torrisi
 * @see ListiniPrezziArticoli
 * @generated
 */
public class ListiniPrezziArticoliWrapper implements ListiniPrezziArticoli,
	ModelWrapper<ListiniPrezziArticoli> {
	public ListiniPrezziArticoliWrapper(
		ListiniPrezziArticoli listiniPrezziArticoli) {
		_listiniPrezziArticoli = listiniPrezziArticoli;
	}

	@Override
	public Class<?> getModelClass() {
		return ListiniPrezziArticoli.class;
	}

	@Override
	public String getModelClassName() {
		return ListiniPrezziArticoli.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codiceListino", getCodiceListino());
		attributes.put("codiceDivisa", getCodiceDivisa());
		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceSoggetto", getCodiceSoggetto());
		attributes.put("dataInizioValidita", getDataInizioValidita());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("codiceLavorazione", getCodiceLavorazione());
		attributes.put("quantInizioValiditaPrezzo",
			getQuantInizioValiditaPrezzo());
		attributes.put("dataFineValidita", getDataFineValidita());
		attributes.put("quantFineValiditaPrezzo", getQuantFineValiditaPrezzo());
		attributes.put("prezzoNettoIVA", getPrezzoNettoIVA());
		attributes.put("codiceIVA", getCodiceIVA());
		attributes.put("utilizzoSconti", getUtilizzoSconti());
		attributes.put("scontoListino1", getScontoListino1());
		attributes.put("scontoListino2", getScontoListino2());
		attributes.put("scontoListino3", getScontoListino3());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codiceListino = (String)attributes.get("codiceListino");

		if (codiceListino != null) {
			setCodiceListino(codiceListino);
		}

		String codiceDivisa = (String)attributes.get("codiceDivisa");

		if (codiceDivisa != null) {
			setCodiceDivisa(codiceDivisa);
		}

		Integer tipoSoggetto = (Integer)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceSoggetto = (String)attributes.get("codiceSoggetto");

		if (codiceSoggetto != null) {
			setCodiceSoggetto(codiceSoggetto);
		}

		Date dataInizioValidita = (Date)attributes.get("dataInizioValidita");

		if (dataInizioValidita != null) {
			setDataInizioValidita(dataInizioValidita);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		String codiceLavorazione = (String)attributes.get("codiceLavorazione");

		if (codiceLavorazione != null) {
			setCodiceLavorazione(codiceLavorazione);
		}

		Double quantInizioValiditaPrezzo = (Double)attributes.get(
				"quantInizioValiditaPrezzo");

		if (quantInizioValiditaPrezzo != null) {
			setQuantInizioValiditaPrezzo(quantInizioValiditaPrezzo);
		}

		Date dataFineValidita = (Date)attributes.get("dataFineValidita");

		if (dataFineValidita != null) {
			setDataFineValidita(dataFineValidita);
		}

		Double quantFineValiditaPrezzo = (Double)attributes.get(
				"quantFineValiditaPrezzo");

		if (quantFineValiditaPrezzo != null) {
			setQuantFineValiditaPrezzo(quantFineValiditaPrezzo);
		}

		Double prezzoNettoIVA = (Double)attributes.get("prezzoNettoIVA");

		if (prezzoNettoIVA != null) {
			setPrezzoNettoIVA(prezzoNettoIVA);
		}

		String codiceIVA = (String)attributes.get("codiceIVA");

		if (codiceIVA != null) {
			setCodiceIVA(codiceIVA);
		}

		Boolean utilizzoSconti = (Boolean)attributes.get("utilizzoSconti");

		if (utilizzoSconti != null) {
			setUtilizzoSconti(utilizzoSconti);
		}

		Double scontoListino1 = (Double)attributes.get("scontoListino1");

		if (scontoListino1 != null) {
			setScontoListino1(scontoListino1);
		}

		Double scontoListino2 = (Double)attributes.get("scontoListino2");

		if (scontoListino2 != null) {
			setScontoListino2(scontoListino2);
		}

		Double scontoListino3 = (Double)attributes.get("scontoListino3");

		if (scontoListino3 != null) {
			setScontoListino3(scontoListino3);
		}
	}

	/**
	* Returns the primary key of this listini prezzi articoli.
	*
	* @return the primary key of this listini prezzi articoli
	*/
	@Override
	public it.bysoftware.ct.service.persistence.ListiniPrezziArticoliPK getPrimaryKey() {
		return _listiniPrezziArticoli.getPrimaryKey();
	}

	/**
	* Sets the primary key of this listini prezzi articoli.
	*
	* @param primaryKey the primary key of this listini prezzi articoli
	*/
	@Override
	public void setPrimaryKey(
		it.bysoftware.ct.service.persistence.ListiniPrezziArticoliPK primaryKey) {
		_listiniPrezziArticoli.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the codice listino of this listini prezzi articoli.
	*
	* @return the codice listino of this listini prezzi articoli
	*/
	@Override
	public java.lang.String getCodiceListino() {
		return _listiniPrezziArticoli.getCodiceListino();
	}

	/**
	* Sets the codice listino of this listini prezzi articoli.
	*
	* @param codiceListino the codice listino of this listini prezzi articoli
	*/
	@Override
	public void setCodiceListino(java.lang.String codiceListino) {
		_listiniPrezziArticoli.setCodiceListino(codiceListino);
	}

	/**
	* Returns the codice divisa of this listini prezzi articoli.
	*
	* @return the codice divisa of this listini prezzi articoli
	*/
	@Override
	public java.lang.String getCodiceDivisa() {
		return _listiniPrezziArticoli.getCodiceDivisa();
	}

	/**
	* Sets the codice divisa of this listini prezzi articoli.
	*
	* @param codiceDivisa the codice divisa of this listini prezzi articoli
	*/
	@Override
	public void setCodiceDivisa(java.lang.String codiceDivisa) {
		_listiniPrezziArticoli.setCodiceDivisa(codiceDivisa);
	}

	/**
	* Returns the tipo soggetto of this listini prezzi articoli.
	*
	* @return the tipo soggetto of this listini prezzi articoli
	*/
	@Override
	public int getTipoSoggetto() {
		return _listiniPrezziArticoli.getTipoSoggetto();
	}

	/**
	* Sets the tipo soggetto of this listini prezzi articoli.
	*
	* @param tipoSoggetto the tipo soggetto of this listini prezzi articoli
	*/
	@Override
	public void setTipoSoggetto(int tipoSoggetto) {
		_listiniPrezziArticoli.setTipoSoggetto(tipoSoggetto);
	}

	/**
	* Returns the codice soggetto of this listini prezzi articoli.
	*
	* @return the codice soggetto of this listini prezzi articoli
	*/
	@Override
	public java.lang.String getCodiceSoggetto() {
		return _listiniPrezziArticoli.getCodiceSoggetto();
	}

	/**
	* Sets the codice soggetto of this listini prezzi articoli.
	*
	* @param codiceSoggetto the codice soggetto of this listini prezzi articoli
	*/
	@Override
	public void setCodiceSoggetto(java.lang.String codiceSoggetto) {
		_listiniPrezziArticoli.setCodiceSoggetto(codiceSoggetto);
	}

	/**
	* Returns the data inizio validita of this listini prezzi articoli.
	*
	* @return the data inizio validita of this listini prezzi articoli
	*/
	@Override
	public java.util.Date getDataInizioValidita() {
		return _listiniPrezziArticoli.getDataInizioValidita();
	}

	/**
	* Sets the data inizio validita of this listini prezzi articoli.
	*
	* @param dataInizioValidita the data inizio validita of this listini prezzi articoli
	*/
	@Override
	public void setDataInizioValidita(java.util.Date dataInizioValidita) {
		_listiniPrezziArticoli.setDataInizioValidita(dataInizioValidita);
	}

	/**
	* Returns the codice articolo of this listini prezzi articoli.
	*
	* @return the codice articolo of this listini prezzi articoli
	*/
	@Override
	public java.lang.String getCodiceArticolo() {
		return _listiniPrezziArticoli.getCodiceArticolo();
	}

	/**
	* Sets the codice articolo of this listini prezzi articoli.
	*
	* @param codiceArticolo the codice articolo of this listini prezzi articoli
	*/
	@Override
	public void setCodiceArticolo(java.lang.String codiceArticolo) {
		_listiniPrezziArticoli.setCodiceArticolo(codiceArticolo);
	}

	/**
	* Returns the codice variante of this listini prezzi articoli.
	*
	* @return the codice variante of this listini prezzi articoli
	*/
	@Override
	public java.lang.String getCodiceVariante() {
		return _listiniPrezziArticoli.getCodiceVariante();
	}

	/**
	* Sets the codice variante of this listini prezzi articoli.
	*
	* @param codiceVariante the codice variante of this listini prezzi articoli
	*/
	@Override
	public void setCodiceVariante(java.lang.String codiceVariante) {
		_listiniPrezziArticoli.setCodiceVariante(codiceVariante);
	}

	/**
	* Returns the codice lavorazione of this listini prezzi articoli.
	*
	* @return the codice lavorazione of this listini prezzi articoli
	*/
	@Override
	public java.lang.String getCodiceLavorazione() {
		return _listiniPrezziArticoli.getCodiceLavorazione();
	}

	/**
	* Sets the codice lavorazione of this listini prezzi articoli.
	*
	* @param codiceLavorazione the codice lavorazione of this listini prezzi articoli
	*/
	@Override
	public void setCodiceLavorazione(java.lang.String codiceLavorazione) {
		_listiniPrezziArticoli.setCodiceLavorazione(codiceLavorazione);
	}

	/**
	* Returns the quant inizio validita prezzo of this listini prezzi articoli.
	*
	* @return the quant inizio validita prezzo of this listini prezzi articoli
	*/
	@Override
	public double getQuantInizioValiditaPrezzo() {
		return _listiniPrezziArticoli.getQuantInizioValiditaPrezzo();
	}

	/**
	* Sets the quant inizio validita prezzo of this listini prezzi articoli.
	*
	* @param quantInizioValiditaPrezzo the quant inizio validita prezzo of this listini prezzi articoli
	*/
	@Override
	public void setQuantInizioValiditaPrezzo(double quantInizioValiditaPrezzo) {
		_listiniPrezziArticoli.setQuantInizioValiditaPrezzo(quantInizioValiditaPrezzo);
	}

	/**
	* Returns the data fine validita of this listini prezzi articoli.
	*
	* @return the data fine validita of this listini prezzi articoli
	*/
	@Override
	public java.util.Date getDataFineValidita() {
		return _listiniPrezziArticoli.getDataFineValidita();
	}

	/**
	* Sets the data fine validita of this listini prezzi articoli.
	*
	* @param dataFineValidita the data fine validita of this listini prezzi articoli
	*/
	@Override
	public void setDataFineValidita(java.util.Date dataFineValidita) {
		_listiniPrezziArticoli.setDataFineValidita(dataFineValidita);
	}

	/**
	* Returns the quant fine validita prezzo of this listini prezzi articoli.
	*
	* @return the quant fine validita prezzo of this listini prezzi articoli
	*/
	@Override
	public double getQuantFineValiditaPrezzo() {
		return _listiniPrezziArticoli.getQuantFineValiditaPrezzo();
	}

	/**
	* Sets the quant fine validita prezzo of this listini prezzi articoli.
	*
	* @param quantFineValiditaPrezzo the quant fine validita prezzo of this listini prezzi articoli
	*/
	@Override
	public void setQuantFineValiditaPrezzo(double quantFineValiditaPrezzo) {
		_listiniPrezziArticoli.setQuantFineValiditaPrezzo(quantFineValiditaPrezzo);
	}

	/**
	* Returns the prezzo netto i v a of this listini prezzi articoli.
	*
	* @return the prezzo netto i v a of this listini prezzi articoli
	*/
	@Override
	public double getPrezzoNettoIVA() {
		return _listiniPrezziArticoli.getPrezzoNettoIVA();
	}

	/**
	* Sets the prezzo netto i v a of this listini prezzi articoli.
	*
	* @param prezzoNettoIVA the prezzo netto i v a of this listini prezzi articoli
	*/
	@Override
	public void setPrezzoNettoIVA(double prezzoNettoIVA) {
		_listiniPrezziArticoli.setPrezzoNettoIVA(prezzoNettoIVA);
	}

	/**
	* Returns the codice i v a of this listini prezzi articoli.
	*
	* @return the codice i v a of this listini prezzi articoli
	*/
	@Override
	public java.lang.String getCodiceIVA() {
		return _listiniPrezziArticoli.getCodiceIVA();
	}

	/**
	* Sets the codice i v a of this listini prezzi articoli.
	*
	* @param codiceIVA the codice i v a of this listini prezzi articoli
	*/
	@Override
	public void setCodiceIVA(java.lang.String codiceIVA) {
		_listiniPrezziArticoli.setCodiceIVA(codiceIVA);
	}

	/**
	* Returns the utilizzo sconti of this listini prezzi articoli.
	*
	* @return the utilizzo sconti of this listini prezzi articoli
	*/
	@Override
	public boolean getUtilizzoSconti() {
		return _listiniPrezziArticoli.getUtilizzoSconti();
	}

	/**
	* Returns <code>true</code> if this listini prezzi articoli is utilizzo sconti.
	*
	* @return <code>true</code> if this listini prezzi articoli is utilizzo sconti; <code>false</code> otherwise
	*/
	@Override
	public boolean isUtilizzoSconti() {
		return _listiniPrezziArticoli.isUtilizzoSconti();
	}

	/**
	* Sets whether this listini prezzi articoli is utilizzo sconti.
	*
	* @param utilizzoSconti the utilizzo sconti of this listini prezzi articoli
	*/
	@Override
	public void setUtilizzoSconti(boolean utilizzoSconti) {
		_listiniPrezziArticoli.setUtilizzoSconti(utilizzoSconti);
	}

	/**
	* Returns the sconto listino1 of this listini prezzi articoli.
	*
	* @return the sconto listino1 of this listini prezzi articoli
	*/
	@Override
	public double getScontoListino1() {
		return _listiniPrezziArticoli.getScontoListino1();
	}

	/**
	* Sets the sconto listino1 of this listini prezzi articoli.
	*
	* @param scontoListino1 the sconto listino1 of this listini prezzi articoli
	*/
	@Override
	public void setScontoListino1(double scontoListino1) {
		_listiniPrezziArticoli.setScontoListino1(scontoListino1);
	}

	/**
	* Returns the sconto listino2 of this listini prezzi articoli.
	*
	* @return the sconto listino2 of this listini prezzi articoli
	*/
	@Override
	public double getScontoListino2() {
		return _listiniPrezziArticoli.getScontoListino2();
	}

	/**
	* Sets the sconto listino2 of this listini prezzi articoli.
	*
	* @param scontoListino2 the sconto listino2 of this listini prezzi articoli
	*/
	@Override
	public void setScontoListino2(double scontoListino2) {
		_listiniPrezziArticoli.setScontoListino2(scontoListino2);
	}

	/**
	* Returns the sconto listino3 of this listini prezzi articoli.
	*
	* @return the sconto listino3 of this listini prezzi articoli
	*/
	@Override
	public double getScontoListino3() {
		return _listiniPrezziArticoli.getScontoListino3();
	}

	/**
	* Sets the sconto listino3 of this listini prezzi articoli.
	*
	* @param scontoListino3 the sconto listino3 of this listini prezzi articoli
	*/
	@Override
	public void setScontoListino3(double scontoListino3) {
		_listiniPrezziArticoli.setScontoListino3(scontoListino3);
	}

	@Override
	public boolean isNew() {
		return _listiniPrezziArticoli.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_listiniPrezziArticoli.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _listiniPrezziArticoli.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_listiniPrezziArticoli.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _listiniPrezziArticoli.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _listiniPrezziArticoli.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_listiniPrezziArticoli.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _listiniPrezziArticoli.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_listiniPrezziArticoli.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_listiniPrezziArticoli.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_listiniPrezziArticoli.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ListiniPrezziArticoliWrapper((ListiniPrezziArticoli)_listiniPrezziArticoli.clone());
	}

	@Override
	public int compareTo(
		it.bysoftware.ct.model.ListiniPrezziArticoli listiniPrezziArticoli) {
		return _listiniPrezziArticoli.compareTo(listiniPrezziArticoli);
	}

	@Override
	public int hashCode() {
		return _listiniPrezziArticoli.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.ListiniPrezziArticoli> toCacheModel() {
		return _listiniPrezziArticoli.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.ListiniPrezziArticoli toEscapedModel() {
		return new ListiniPrezziArticoliWrapper(_listiniPrezziArticoli.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.ListiniPrezziArticoli toUnescapedModel() {
		return new ListiniPrezziArticoliWrapper(_listiniPrezziArticoli.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _listiniPrezziArticoli.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _listiniPrezziArticoli.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_listiniPrezziArticoli.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ListiniPrezziArticoliWrapper)) {
			return false;
		}

		ListiniPrezziArticoliWrapper listiniPrezziArticoliWrapper = (ListiniPrezziArticoliWrapper)obj;

		if (Validator.equals(_listiniPrezziArticoli,
					listiniPrezziArticoliWrapper._listiniPrezziArticoli)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ListiniPrezziArticoli getWrappedListiniPrezziArticoli() {
		return _listiniPrezziArticoli;
	}

	@Override
	public ListiniPrezziArticoli getWrappedModel() {
		return _listiniPrezziArticoli;
	}

	@Override
	public void resetOriginalValues() {
		_listiniPrezziArticoli.resetOriginalValues();
	}

	private ListiniPrezziArticoli _listiniPrezziArticoli;
}