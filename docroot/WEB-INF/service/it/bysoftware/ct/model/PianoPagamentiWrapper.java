/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PianoPagamenti}.
 * </p>
 *
 * @author Mario Torrisi
 * @see PianoPagamenti
 * @generated
 */
public class PianoPagamentiWrapper implements PianoPagamenti,
	ModelWrapper<PianoPagamenti> {
	public PianoPagamentiWrapper(PianoPagamenti pianoPagamenti) {
		_pianoPagamenti = pianoPagamenti;
	}

	@Override
	public Class<?> getModelClass() {
		return PianoPagamenti.class;
	}

	@Override
	public String getModelClassName() {
		return PianoPagamenti.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codicePianoPagamento", getCodicePianoPagamento());
		attributes.put("descrizione", getDescrizione());
		attributes.put("primoMeseEscluso", getPrimoMeseEscluso());
		attributes.put("giornoPrimoMeseSucc", getGiornoPrimoMeseSucc());
		attributes.put("giornoSecondoMeseSucc", getGiornoSecondoMeseSucc());
		attributes.put("secondoMeseEscluso", getSecondoMeseEscluso());
		attributes.put("inizioFattDaBolla", getInizioFattDaBolla());
		attributes.put("percScChiusura", getPercScChiusura());
		attributes.put("percScCassa", getPercScCassa());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codicePianoPagamento = (String)attributes.get(
				"codicePianoPagamento");

		if (codicePianoPagamento != null) {
			setCodicePianoPagamento(codicePianoPagamento);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		Integer primoMeseEscluso = (Integer)attributes.get("primoMeseEscluso");

		if (primoMeseEscluso != null) {
			setPrimoMeseEscluso(primoMeseEscluso);
		}

		Integer giornoPrimoMeseSucc = (Integer)attributes.get(
				"giornoPrimoMeseSucc");

		if (giornoPrimoMeseSucc != null) {
			setGiornoPrimoMeseSucc(giornoPrimoMeseSucc);
		}

		Integer giornoSecondoMeseSucc = (Integer)attributes.get(
				"giornoSecondoMeseSucc");

		if (giornoSecondoMeseSucc != null) {
			setGiornoSecondoMeseSucc(giornoSecondoMeseSucc);
		}

		Integer secondoMeseEscluso = (Integer)attributes.get(
				"secondoMeseEscluso");

		if (secondoMeseEscluso != null) {
			setSecondoMeseEscluso(secondoMeseEscluso);
		}

		Boolean inizioFattDaBolla = (Boolean)attributes.get("inizioFattDaBolla");

		if (inizioFattDaBolla != null) {
			setInizioFattDaBolla(inizioFattDaBolla);
		}

		Double percScChiusura = (Double)attributes.get("percScChiusura");

		if (percScChiusura != null) {
			setPercScChiusura(percScChiusura);
		}

		Double percScCassa = (Double)attributes.get("percScCassa");

		if (percScCassa != null) {
			setPercScCassa(percScCassa);
		}
	}

	/**
	* Returns the primary key of this piano pagamenti.
	*
	* @return the primary key of this piano pagamenti
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _pianoPagamenti.getPrimaryKey();
	}

	/**
	* Sets the primary key of this piano pagamenti.
	*
	* @param primaryKey the primary key of this piano pagamenti
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_pianoPagamenti.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the codice piano pagamento of this piano pagamenti.
	*
	* @return the codice piano pagamento of this piano pagamenti
	*/
	@Override
	public java.lang.String getCodicePianoPagamento() {
		return _pianoPagamenti.getCodicePianoPagamento();
	}

	/**
	* Sets the codice piano pagamento of this piano pagamenti.
	*
	* @param codicePianoPagamento the codice piano pagamento of this piano pagamenti
	*/
	@Override
	public void setCodicePianoPagamento(java.lang.String codicePianoPagamento) {
		_pianoPagamenti.setCodicePianoPagamento(codicePianoPagamento);
	}

	/**
	* Returns the descrizione of this piano pagamenti.
	*
	* @return the descrizione of this piano pagamenti
	*/
	@Override
	public java.lang.String getDescrizione() {
		return _pianoPagamenti.getDescrizione();
	}

	/**
	* Sets the descrizione of this piano pagamenti.
	*
	* @param descrizione the descrizione of this piano pagamenti
	*/
	@Override
	public void setDescrizione(java.lang.String descrizione) {
		_pianoPagamenti.setDescrizione(descrizione);
	}

	/**
	* Returns the primo mese escluso of this piano pagamenti.
	*
	* @return the primo mese escluso of this piano pagamenti
	*/
	@Override
	public int getPrimoMeseEscluso() {
		return _pianoPagamenti.getPrimoMeseEscluso();
	}

	/**
	* Sets the primo mese escluso of this piano pagamenti.
	*
	* @param primoMeseEscluso the primo mese escluso of this piano pagamenti
	*/
	@Override
	public void setPrimoMeseEscluso(int primoMeseEscluso) {
		_pianoPagamenti.setPrimoMeseEscluso(primoMeseEscluso);
	}

	/**
	* Returns the giorno primo mese succ of this piano pagamenti.
	*
	* @return the giorno primo mese succ of this piano pagamenti
	*/
	@Override
	public int getGiornoPrimoMeseSucc() {
		return _pianoPagamenti.getGiornoPrimoMeseSucc();
	}

	/**
	* Sets the giorno primo mese succ of this piano pagamenti.
	*
	* @param giornoPrimoMeseSucc the giorno primo mese succ of this piano pagamenti
	*/
	@Override
	public void setGiornoPrimoMeseSucc(int giornoPrimoMeseSucc) {
		_pianoPagamenti.setGiornoPrimoMeseSucc(giornoPrimoMeseSucc);
	}

	/**
	* Returns the giorno secondo mese succ of this piano pagamenti.
	*
	* @return the giorno secondo mese succ of this piano pagamenti
	*/
	@Override
	public int getGiornoSecondoMeseSucc() {
		return _pianoPagamenti.getGiornoSecondoMeseSucc();
	}

	/**
	* Sets the giorno secondo mese succ of this piano pagamenti.
	*
	* @param giornoSecondoMeseSucc the giorno secondo mese succ of this piano pagamenti
	*/
	@Override
	public void setGiornoSecondoMeseSucc(int giornoSecondoMeseSucc) {
		_pianoPagamenti.setGiornoSecondoMeseSucc(giornoSecondoMeseSucc);
	}

	/**
	* Returns the secondo mese escluso of this piano pagamenti.
	*
	* @return the secondo mese escluso of this piano pagamenti
	*/
	@Override
	public int getSecondoMeseEscluso() {
		return _pianoPagamenti.getSecondoMeseEscluso();
	}

	/**
	* Sets the secondo mese escluso of this piano pagamenti.
	*
	* @param secondoMeseEscluso the secondo mese escluso of this piano pagamenti
	*/
	@Override
	public void setSecondoMeseEscluso(int secondoMeseEscluso) {
		_pianoPagamenti.setSecondoMeseEscluso(secondoMeseEscluso);
	}

	/**
	* Returns the inizio fatt da bolla of this piano pagamenti.
	*
	* @return the inizio fatt da bolla of this piano pagamenti
	*/
	@Override
	public boolean getInizioFattDaBolla() {
		return _pianoPagamenti.getInizioFattDaBolla();
	}

	/**
	* Returns <code>true</code> if this piano pagamenti is inizio fatt da bolla.
	*
	* @return <code>true</code> if this piano pagamenti is inizio fatt da bolla; <code>false</code> otherwise
	*/
	@Override
	public boolean isInizioFattDaBolla() {
		return _pianoPagamenti.isInizioFattDaBolla();
	}

	/**
	* Sets whether this piano pagamenti is inizio fatt da bolla.
	*
	* @param inizioFattDaBolla the inizio fatt da bolla of this piano pagamenti
	*/
	@Override
	public void setInizioFattDaBolla(boolean inizioFattDaBolla) {
		_pianoPagamenti.setInizioFattDaBolla(inizioFattDaBolla);
	}

	/**
	* Returns the perc sc chiusura of this piano pagamenti.
	*
	* @return the perc sc chiusura of this piano pagamenti
	*/
	@Override
	public double getPercScChiusura() {
		return _pianoPagamenti.getPercScChiusura();
	}

	/**
	* Sets the perc sc chiusura of this piano pagamenti.
	*
	* @param percScChiusura the perc sc chiusura of this piano pagamenti
	*/
	@Override
	public void setPercScChiusura(double percScChiusura) {
		_pianoPagamenti.setPercScChiusura(percScChiusura);
	}

	/**
	* Returns the perc sc cassa of this piano pagamenti.
	*
	* @return the perc sc cassa of this piano pagamenti
	*/
	@Override
	public double getPercScCassa() {
		return _pianoPagamenti.getPercScCassa();
	}

	/**
	* Sets the perc sc cassa of this piano pagamenti.
	*
	* @param percScCassa the perc sc cassa of this piano pagamenti
	*/
	@Override
	public void setPercScCassa(double percScCassa) {
		_pianoPagamenti.setPercScCassa(percScCassa);
	}

	@Override
	public boolean isNew() {
		return _pianoPagamenti.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_pianoPagamenti.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _pianoPagamenti.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_pianoPagamenti.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _pianoPagamenti.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _pianoPagamenti.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_pianoPagamenti.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _pianoPagamenti.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_pianoPagamenti.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_pianoPagamenti.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_pianoPagamenti.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new PianoPagamentiWrapper((PianoPagamenti)_pianoPagamenti.clone());
	}

	@Override
	public int compareTo(it.bysoftware.ct.model.PianoPagamenti pianoPagamenti) {
		return _pianoPagamenti.compareTo(pianoPagamenti);
	}

	@Override
	public int hashCode() {
		return _pianoPagamenti.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.PianoPagamenti> toCacheModel() {
		return _pianoPagamenti.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.PianoPagamenti toEscapedModel() {
		return new PianoPagamentiWrapper(_pianoPagamenti.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.PianoPagamenti toUnescapedModel() {
		return new PianoPagamentiWrapper(_pianoPagamenti.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _pianoPagamenti.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _pianoPagamenti.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_pianoPagamenti.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PianoPagamentiWrapper)) {
			return false;
		}

		PianoPagamentiWrapper pianoPagamentiWrapper = (PianoPagamentiWrapper)obj;

		if (Validator.equals(_pianoPagamenti,
					pianoPagamentiWrapper._pianoPagamenti)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public PianoPagamenti getWrappedPianoPagamenti() {
		return _pianoPagamenti;
	}

	@Override
	public PianoPagamenti getWrappedModel() {
		return _pianoPagamenti;
	}

	@Override
	public void resetOriginalValues() {
		_pianoPagamenti.resetOriginalValues();
	}

	private PianoPagamenti _pianoPagamenti;
}