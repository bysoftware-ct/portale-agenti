/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.OrdinatoLocalServiceUtil;
import it.bysoftware.ct.service.persistence.OrdinatoPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class OrdinatoClp extends BaseModelImpl<Ordinato> implements Ordinato {
	public OrdinatoClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Ordinato.class;
	}

	@Override
	public String getModelClassName() {
		return Ordinato.class.getName();
	}

	@Override
	public OrdinatoPK getPrimaryKey() {
		return new OrdinatoPK(_codiceDesposito, _codiceArticolo, _codiceVariante);
	}

	@Override
	public void setPrimaryKey(OrdinatoPK primaryKey) {
		setCodiceDesposito(primaryKey.codiceDesposito);
		setCodiceArticolo(primaryKey.codiceArticolo);
		setCodiceVariante(primaryKey.codiceVariante);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new OrdinatoPK(_codiceDesposito, _codiceArticolo, _codiceVariante);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((OrdinatoPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codiceDesposito", getCodiceDesposito());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("quantitaOrdinataFornitore",
			getQuantitaOrdinataFornitore());
		attributes.put("quantitaOrdinataProduzione",
			getQuantitaOrdinataProduzione());
		attributes.put("quantitaOrdinataAltri", getQuantitaOrdinataAltri());
		attributes.put("quantitaImpegnataClienti", getQuantitaImpegnataClienti());
		attributes.put("quantitaImpegnataProduzione",
			getQuantitaImpegnataProduzione());
		attributes.put("quantitaImpegnataAltri", getQuantitaImpegnataAltri());
		attributes.put("quantitaOrdinataFornitoreUnMis2",
			getQuantitaOrdinataFornitoreUnMis2());
		attributes.put("quantitaOrdinataProduzioneUnMis2",
			getQuantitaOrdinataProduzioneUnMis2());
		attributes.put("quantitaOrdinataAltriUnMis2",
			getQuantitaOrdinataAltriUnMis2());
		attributes.put("quantitaImpegnataClientiUnMis2",
			getQuantitaImpegnataClientiUnMis2());
		attributes.put("quantitaImpegnataProduzioneUnMis2",
			getQuantitaImpegnataProduzioneUnMis2());
		attributes.put("quantitaImpegnataAltriUnMis2",
			getQuantitaImpegnataAltriUnMis2());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codiceDesposito = (String)attributes.get("codiceDesposito");

		if (codiceDesposito != null) {
			setCodiceDesposito(codiceDesposito);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		Double quantitaOrdinataFornitore = (Double)attributes.get(
				"quantitaOrdinataFornitore");

		if (quantitaOrdinataFornitore != null) {
			setQuantitaOrdinataFornitore(quantitaOrdinataFornitore);
		}

		Double quantitaOrdinataProduzione = (Double)attributes.get(
				"quantitaOrdinataProduzione");

		if (quantitaOrdinataProduzione != null) {
			setQuantitaOrdinataProduzione(quantitaOrdinataProduzione);
		}

		Double quantitaOrdinataAltri = (Double)attributes.get(
				"quantitaOrdinataAltri");

		if (quantitaOrdinataAltri != null) {
			setQuantitaOrdinataAltri(quantitaOrdinataAltri);
		}

		Double quantitaImpegnataClienti = (Double)attributes.get(
				"quantitaImpegnataClienti");

		if (quantitaImpegnataClienti != null) {
			setQuantitaImpegnataClienti(quantitaImpegnataClienti);
		}

		Double quantitaImpegnataProduzione = (Double)attributes.get(
				"quantitaImpegnataProduzione");

		if (quantitaImpegnataProduzione != null) {
			setQuantitaImpegnataProduzione(quantitaImpegnataProduzione);
		}

		Double quantitaImpegnataAltri = (Double)attributes.get(
				"quantitaImpegnataAltri");

		if (quantitaImpegnataAltri != null) {
			setQuantitaImpegnataAltri(quantitaImpegnataAltri);
		}

		Double quantitaOrdinataFornitoreUnMis2 = (Double)attributes.get(
				"quantitaOrdinataFornitoreUnMis2");

		if (quantitaOrdinataFornitoreUnMis2 != null) {
			setQuantitaOrdinataFornitoreUnMis2(quantitaOrdinataFornitoreUnMis2);
		}

		Double quantitaOrdinataProduzioneUnMis2 = (Double)attributes.get(
				"quantitaOrdinataProduzioneUnMis2");

		if (quantitaOrdinataProduzioneUnMis2 != null) {
			setQuantitaOrdinataProduzioneUnMis2(quantitaOrdinataProduzioneUnMis2);
		}

		Double quantitaOrdinataAltriUnMis2 = (Double)attributes.get(
				"quantitaOrdinataAltriUnMis2");

		if (quantitaOrdinataAltriUnMis2 != null) {
			setQuantitaOrdinataAltriUnMis2(quantitaOrdinataAltriUnMis2);
		}

		Double quantitaImpegnataClientiUnMis2 = (Double)attributes.get(
				"quantitaImpegnataClientiUnMis2");

		if (quantitaImpegnataClientiUnMis2 != null) {
			setQuantitaImpegnataClientiUnMis2(quantitaImpegnataClientiUnMis2);
		}

		Double quantitaImpegnataProduzioneUnMis2 = (Double)attributes.get(
				"quantitaImpegnataProduzioneUnMis2");

		if (quantitaImpegnataProduzioneUnMis2 != null) {
			setQuantitaImpegnataProduzioneUnMis2(quantitaImpegnataProduzioneUnMis2);
		}

		Double quantitaImpegnataAltriUnMis2 = (Double)attributes.get(
				"quantitaImpegnataAltriUnMis2");

		if (quantitaImpegnataAltriUnMis2 != null) {
			setQuantitaImpegnataAltriUnMis2(quantitaImpegnataAltriUnMis2);
		}
	}

	@Override
	public String getCodiceDesposito() {
		return _codiceDesposito;
	}

	@Override
	public void setCodiceDesposito(String codiceDesposito) {
		_codiceDesposito = codiceDesposito;

		if (_ordinatoRemoteModel != null) {
			try {
				Class<?> clazz = _ordinatoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDesposito",
						String.class);

				method.invoke(_ordinatoRemoteModel, codiceDesposito);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	@Override
	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;

		if (_ordinatoRemoteModel != null) {
			try {
				Class<?> clazz = _ordinatoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceArticolo",
						String.class);

				method.invoke(_ordinatoRemoteModel, codiceArticolo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceVariante() {
		return _codiceVariante;
	}

	@Override
	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;

		if (_ordinatoRemoteModel != null) {
			try {
				Class<?> clazz = _ordinatoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceVariante",
						String.class);

				method.invoke(_ordinatoRemoteModel, codiceVariante);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaOrdinataFornitore() {
		return _quantitaOrdinataFornitore;
	}

	@Override
	public void setQuantitaOrdinataFornitore(double quantitaOrdinataFornitore) {
		_quantitaOrdinataFornitore = quantitaOrdinataFornitore;

		if (_ordinatoRemoteModel != null) {
			try {
				Class<?> clazz = _ordinatoRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaOrdinataFornitore",
						double.class);

				method.invoke(_ordinatoRemoteModel, quantitaOrdinataFornitore);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaOrdinataProduzione() {
		return _quantitaOrdinataProduzione;
	}

	@Override
	public void setQuantitaOrdinataProduzione(double quantitaOrdinataProduzione) {
		_quantitaOrdinataProduzione = quantitaOrdinataProduzione;

		if (_ordinatoRemoteModel != null) {
			try {
				Class<?> clazz = _ordinatoRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaOrdinataProduzione",
						double.class);

				method.invoke(_ordinatoRemoteModel, quantitaOrdinataProduzione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaOrdinataAltri() {
		return _quantitaOrdinataAltri;
	}

	@Override
	public void setQuantitaOrdinataAltri(double quantitaOrdinataAltri) {
		_quantitaOrdinataAltri = quantitaOrdinataAltri;

		if (_ordinatoRemoteModel != null) {
			try {
				Class<?> clazz = _ordinatoRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaOrdinataAltri",
						double.class);

				method.invoke(_ordinatoRemoteModel, quantitaOrdinataAltri);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaImpegnataClienti() {
		return _quantitaImpegnataClienti;
	}

	@Override
	public void setQuantitaImpegnataClienti(double quantitaImpegnataClienti) {
		_quantitaImpegnataClienti = quantitaImpegnataClienti;

		if (_ordinatoRemoteModel != null) {
			try {
				Class<?> clazz = _ordinatoRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaImpegnataClienti",
						double.class);

				method.invoke(_ordinatoRemoteModel, quantitaImpegnataClienti);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaImpegnataProduzione() {
		return _quantitaImpegnataProduzione;
	}

	@Override
	public void setQuantitaImpegnataProduzione(
		double quantitaImpegnataProduzione) {
		_quantitaImpegnataProduzione = quantitaImpegnataProduzione;

		if (_ordinatoRemoteModel != null) {
			try {
				Class<?> clazz = _ordinatoRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaImpegnataProduzione",
						double.class);

				method.invoke(_ordinatoRemoteModel, quantitaImpegnataProduzione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaImpegnataAltri() {
		return _quantitaImpegnataAltri;
	}

	@Override
	public void setQuantitaImpegnataAltri(double quantitaImpegnataAltri) {
		_quantitaImpegnataAltri = quantitaImpegnataAltri;

		if (_ordinatoRemoteModel != null) {
			try {
				Class<?> clazz = _ordinatoRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaImpegnataAltri",
						double.class);

				method.invoke(_ordinatoRemoteModel, quantitaImpegnataAltri);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaOrdinataFornitoreUnMis2() {
		return _quantitaOrdinataFornitoreUnMis2;
	}

	@Override
	public void setQuantitaOrdinataFornitoreUnMis2(
		double quantitaOrdinataFornitoreUnMis2) {
		_quantitaOrdinataFornitoreUnMis2 = quantitaOrdinataFornitoreUnMis2;

		if (_ordinatoRemoteModel != null) {
			try {
				Class<?> clazz = _ordinatoRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaOrdinataFornitoreUnMis2",
						double.class);

				method.invoke(_ordinatoRemoteModel,
					quantitaOrdinataFornitoreUnMis2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaOrdinataProduzioneUnMis2() {
		return _quantitaOrdinataProduzioneUnMis2;
	}

	@Override
	public void setQuantitaOrdinataProduzioneUnMis2(
		double quantitaOrdinataProduzioneUnMis2) {
		_quantitaOrdinataProduzioneUnMis2 = quantitaOrdinataProduzioneUnMis2;

		if (_ordinatoRemoteModel != null) {
			try {
				Class<?> clazz = _ordinatoRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaOrdinataProduzioneUnMis2",
						double.class);

				method.invoke(_ordinatoRemoteModel,
					quantitaOrdinataProduzioneUnMis2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaOrdinataAltriUnMis2() {
		return _quantitaOrdinataAltriUnMis2;
	}

	@Override
	public void setQuantitaOrdinataAltriUnMis2(
		double quantitaOrdinataAltriUnMis2) {
		_quantitaOrdinataAltriUnMis2 = quantitaOrdinataAltriUnMis2;

		if (_ordinatoRemoteModel != null) {
			try {
				Class<?> clazz = _ordinatoRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaOrdinataAltriUnMis2",
						double.class);

				method.invoke(_ordinatoRemoteModel, quantitaOrdinataAltriUnMis2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaImpegnataClientiUnMis2() {
		return _quantitaImpegnataClientiUnMis2;
	}

	@Override
	public void setQuantitaImpegnataClientiUnMis2(
		double quantitaImpegnataClientiUnMis2) {
		_quantitaImpegnataClientiUnMis2 = quantitaImpegnataClientiUnMis2;

		if (_ordinatoRemoteModel != null) {
			try {
				Class<?> clazz = _ordinatoRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaImpegnataClientiUnMis2",
						double.class);

				method.invoke(_ordinatoRemoteModel,
					quantitaImpegnataClientiUnMis2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaImpegnataProduzioneUnMis2() {
		return _quantitaImpegnataProduzioneUnMis2;
	}

	@Override
	public void setQuantitaImpegnataProduzioneUnMis2(
		double quantitaImpegnataProduzioneUnMis2) {
		_quantitaImpegnataProduzioneUnMis2 = quantitaImpegnataProduzioneUnMis2;

		if (_ordinatoRemoteModel != null) {
			try {
				Class<?> clazz = _ordinatoRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaImpegnataProduzioneUnMis2",
						double.class);

				method.invoke(_ordinatoRemoteModel,
					quantitaImpegnataProduzioneUnMis2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaImpegnataAltriUnMis2() {
		return _quantitaImpegnataAltriUnMis2;
	}

	@Override
	public void setQuantitaImpegnataAltriUnMis2(
		double quantitaImpegnataAltriUnMis2) {
		_quantitaImpegnataAltriUnMis2 = quantitaImpegnataAltriUnMis2;

		if (_ordinatoRemoteModel != null) {
			try {
				Class<?> clazz = _ordinatoRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaImpegnataAltriUnMis2",
						double.class);

				method.invoke(_ordinatoRemoteModel, quantitaImpegnataAltriUnMis2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getOrdinatoRemoteModel() {
		return _ordinatoRemoteModel;
	}

	public void setOrdinatoRemoteModel(BaseModel<?> ordinatoRemoteModel) {
		_ordinatoRemoteModel = ordinatoRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _ordinatoRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_ordinatoRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			OrdinatoLocalServiceUtil.addOrdinato(this);
		}
		else {
			OrdinatoLocalServiceUtil.updateOrdinato(this);
		}
	}

	@Override
	public Ordinato toEscapedModel() {
		return (Ordinato)ProxyUtil.newProxyInstance(Ordinato.class.getClassLoader(),
			new Class[] { Ordinato.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		OrdinatoClp clone = new OrdinatoClp();

		clone.setCodiceDesposito(getCodiceDesposito());
		clone.setCodiceArticolo(getCodiceArticolo());
		clone.setCodiceVariante(getCodiceVariante());
		clone.setQuantitaOrdinataFornitore(getQuantitaOrdinataFornitore());
		clone.setQuantitaOrdinataProduzione(getQuantitaOrdinataProduzione());
		clone.setQuantitaOrdinataAltri(getQuantitaOrdinataAltri());
		clone.setQuantitaImpegnataClienti(getQuantitaImpegnataClienti());
		clone.setQuantitaImpegnataProduzione(getQuantitaImpegnataProduzione());
		clone.setQuantitaImpegnataAltri(getQuantitaImpegnataAltri());
		clone.setQuantitaOrdinataFornitoreUnMis2(getQuantitaOrdinataFornitoreUnMis2());
		clone.setQuantitaOrdinataProduzioneUnMis2(getQuantitaOrdinataProduzioneUnMis2());
		clone.setQuantitaOrdinataAltriUnMis2(getQuantitaOrdinataAltriUnMis2());
		clone.setQuantitaImpegnataClientiUnMis2(getQuantitaImpegnataClientiUnMis2());
		clone.setQuantitaImpegnataProduzioneUnMis2(getQuantitaImpegnataProduzioneUnMis2());
		clone.setQuantitaImpegnataAltriUnMis2(getQuantitaImpegnataAltriUnMis2());

		return clone;
	}

	@Override
	public int compareTo(Ordinato ordinato) {
		OrdinatoPK primaryKey = ordinato.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OrdinatoClp)) {
			return false;
		}

		OrdinatoClp ordinato = (OrdinatoClp)obj;

		OrdinatoPK primaryKey = ordinato.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{codiceDesposito=");
		sb.append(getCodiceDesposito());
		sb.append(", codiceArticolo=");
		sb.append(getCodiceArticolo());
		sb.append(", codiceVariante=");
		sb.append(getCodiceVariante());
		sb.append(", quantitaOrdinataFornitore=");
		sb.append(getQuantitaOrdinataFornitore());
		sb.append(", quantitaOrdinataProduzione=");
		sb.append(getQuantitaOrdinataProduzione());
		sb.append(", quantitaOrdinataAltri=");
		sb.append(getQuantitaOrdinataAltri());
		sb.append(", quantitaImpegnataClienti=");
		sb.append(getQuantitaImpegnataClienti());
		sb.append(", quantitaImpegnataProduzione=");
		sb.append(getQuantitaImpegnataProduzione());
		sb.append(", quantitaImpegnataAltri=");
		sb.append(getQuantitaImpegnataAltri());
		sb.append(", quantitaOrdinataFornitoreUnMis2=");
		sb.append(getQuantitaOrdinataFornitoreUnMis2());
		sb.append(", quantitaOrdinataProduzioneUnMis2=");
		sb.append(getQuantitaOrdinataProduzioneUnMis2());
		sb.append(", quantitaOrdinataAltriUnMis2=");
		sb.append(getQuantitaOrdinataAltriUnMis2());
		sb.append(", quantitaImpegnataClientiUnMis2=");
		sb.append(getQuantitaImpegnataClientiUnMis2());
		sb.append(", quantitaImpegnataProduzioneUnMis2=");
		sb.append(getQuantitaImpegnataProduzioneUnMis2());
		sb.append(", quantitaImpegnataAltriUnMis2=");
		sb.append(getQuantitaImpegnataAltriUnMis2());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(49);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.Ordinato");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>codiceDesposito</column-name><column-value><![CDATA[");
		sb.append(getCodiceDesposito());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceArticolo</column-name><column-value><![CDATA[");
		sb.append(getCodiceArticolo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceVariante</column-name><column-value><![CDATA[");
		sb.append(getCodiceVariante());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaOrdinataFornitore</column-name><column-value><![CDATA[");
		sb.append(getQuantitaOrdinataFornitore());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaOrdinataProduzione</column-name><column-value><![CDATA[");
		sb.append(getQuantitaOrdinataProduzione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaOrdinataAltri</column-name><column-value><![CDATA[");
		sb.append(getQuantitaOrdinataAltri());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaImpegnataClienti</column-name><column-value><![CDATA[");
		sb.append(getQuantitaImpegnataClienti());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaImpegnataProduzione</column-name><column-value><![CDATA[");
		sb.append(getQuantitaImpegnataProduzione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaImpegnataAltri</column-name><column-value><![CDATA[");
		sb.append(getQuantitaImpegnataAltri());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaOrdinataFornitoreUnMis2</column-name><column-value><![CDATA[");
		sb.append(getQuantitaOrdinataFornitoreUnMis2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaOrdinataProduzioneUnMis2</column-name><column-value><![CDATA[");
		sb.append(getQuantitaOrdinataProduzioneUnMis2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaOrdinataAltriUnMis2</column-name><column-value><![CDATA[");
		sb.append(getQuantitaOrdinataAltriUnMis2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaImpegnataClientiUnMis2</column-name><column-value><![CDATA[");
		sb.append(getQuantitaImpegnataClientiUnMis2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaImpegnataProduzioneUnMis2</column-name><column-value><![CDATA[");
		sb.append(getQuantitaImpegnataProduzioneUnMis2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaImpegnataAltriUnMis2</column-name><column-value><![CDATA[");
		sb.append(getQuantitaImpegnataAltriUnMis2());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _codiceDesposito;
	private String _codiceArticolo;
	private String _codiceVariante;
	private double _quantitaOrdinataFornitore;
	private double _quantitaOrdinataProduzione;
	private double _quantitaOrdinataAltri;
	private double _quantitaImpegnataClienti;
	private double _quantitaImpegnataProduzione;
	private double _quantitaImpegnataAltri;
	private double _quantitaOrdinataFornitoreUnMis2;
	private double _quantitaOrdinataProduzioneUnMis2;
	private double _quantitaOrdinataAltriUnMis2;
	private double _quantitaImpegnataClientiUnMis2;
	private double _quantitaImpegnataProduzioneUnMis2;
	private double _quantitaImpegnataAltriUnMis2;
	private BaseModel<?> _ordinatoRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}