/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.EvasioniOrdiniLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class EvasioniOrdiniClp extends BaseModelImpl<EvasioniOrdini>
	implements EvasioniOrdini {
	public EvasioniOrdiniClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return EvasioniOrdini.class;
	}

	@Override
	public String getModelClassName() {
		return EvasioniOrdini.class.getName();
	}

	@Override
	public String getPrimaryKey() {
		return _ID;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setID(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _ID;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ID", getID());
		attributes.put("anno", getAnno());
		attributes.put("codiceAttivita", getCodiceAttivita());
		attributes.put("codiceCentro", getCodiceCentro());
		attributes.put("codiceDeposito", getCodiceDeposito());
		attributes.put("tipoOrdine", getTipoOrdine());
		attributes.put("numeroOrdine", getNumeroOrdine());
		attributes.put("codiceCliente", getCodiceCliente());
		attributes.put("numeroRigo", getNumeroRigo());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("descrizione", getDescrizione());
		attributes.put("unitaMisura", getUnitaMisura());
		attributes.put("quantitaOrdinata", getQuantitaOrdinata());
		attributes.put("numeroProgrezzivoEvasione",
			getNumeroProgrezzivoEvasione());
		attributes.put("quantitaConsegnata", getQuantitaConsegnata());
		attributes.put("dataDocumentoConsegna", getDataDocumentoConsegna());
		attributes.put("testAccontoSaldo", getTestAccontoSaldo());
		attributes.put("tipoDocumentoEvasione", getTipoDocumentoEvasione());
		attributes.put("numeroDocumentoEvasione", getNumeroDocumentoEvasione());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String ID = (String)attributes.get("ID");

		if (ID != null) {
			setID(ID);
		}

		Integer anno = (Integer)attributes.get("anno");

		if (anno != null) {
			setAnno(anno);
		}

		String codiceAttivita = (String)attributes.get("codiceAttivita");

		if (codiceAttivita != null) {
			setCodiceAttivita(codiceAttivita);
		}

		String codiceCentro = (String)attributes.get("codiceCentro");

		if (codiceCentro != null) {
			setCodiceCentro(codiceCentro);
		}

		String codiceDeposito = (String)attributes.get("codiceDeposito");

		if (codiceDeposito != null) {
			setCodiceDeposito(codiceDeposito);
		}

		Integer tipoOrdine = (Integer)attributes.get("tipoOrdine");

		if (tipoOrdine != null) {
			setTipoOrdine(tipoOrdine);
		}

		Integer numeroOrdine = (Integer)attributes.get("numeroOrdine");

		if (numeroOrdine != null) {
			setNumeroOrdine(numeroOrdine);
		}

		String codiceCliente = (String)attributes.get("codiceCliente");

		if (codiceCliente != null) {
			setCodiceCliente(codiceCliente);
		}

		Integer numeroRigo = (Integer)attributes.get("numeroRigo");

		if (numeroRigo != null) {
			setNumeroRigo(numeroRigo);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		String unitaMisura = (String)attributes.get("unitaMisura");

		if (unitaMisura != null) {
			setUnitaMisura(unitaMisura);
		}

		Double quantitaOrdinata = (Double)attributes.get("quantitaOrdinata");

		if (quantitaOrdinata != null) {
			setQuantitaOrdinata(quantitaOrdinata);
		}

		Integer numeroProgrezzivoEvasione = (Integer)attributes.get(
				"numeroProgrezzivoEvasione");

		if (numeroProgrezzivoEvasione != null) {
			setNumeroProgrezzivoEvasione(numeroProgrezzivoEvasione);
		}

		Double quantitaConsegnata = (Double)attributes.get("quantitaConsegnata");

		if (quantitaConsegnata != null) {
			setQuantitaConsegnata(quantitaConsegnata);
		}

		Date dataDocumentoConsegna = (Date)attributes.get(
				"dataDocumentoConsegna");

		if (dataDocumentoConsegna != null) {
			setDataDocumentoConsegna(dataDocumentoConsegna);
		}

		String testAccontoSaldo = (String)attributes.get("testAccontoSaldo");

		if (testAccontoSaldo != null) {
			setTestAccontoSaldo(testAccontoSaldo);
		}

		String tipoDocumentoEvasione = (String)attributes.get(
				"tipoDocumentoEvasione");

		if (tipoDocumentoEvasione != null) {
			setTipoDocumentoEvasione(tipoDocumentoEvasione);
		}

		Integer numeroDocumentoEvasione = (Integer)attributes.get(
				"numeroDocumentoEvasione");

		if (numeroDocumentoEvasione != null) {
			setNumeroDocumentoEvasione(numeroDocumentoEvasione);
		}
	}

	@Override
	public String getID() {
		return _ID;
	}

	@Override
	public void setID(String ID) {
		_ID = ID;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setID", String.class);

				method.invoke(_evasioniOrdiniRemoteModel, ID);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getAnno() {
		return _anno;
	}

	@Override
	public void setAnno(int anno) {
		_anno = anno;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setAnno", int.class);

				method.invoke(_evasioniOrdiniRemoteModel, anno);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceAttivita() {
		return _codiceAttivita;
	}

	@Override
	public void setCodiceAttivita(String codiceAttivita) {
		_codiceAttivita = codiceAttivita;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceAttivita",
						String.class);

				method.invoke(_evasioniOrdiniRemoteModel, codiceAttivita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCentro() {
		return _codiceCentro;
	}

	@Override
	public void setCodiceCentro(String codiceCentro) {
		_codiceCentro = codiceCentro;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCentro", String.class);

				method.invoke(_evasioniOrdiniRemoteModel, codiceCentro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDeposito() {
		return _codiceDeposito;
	}

	@Override
	public void setCodiceDeposito(String codiceDeposito) {
		_codiceDeposito = codiceDeposito;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDeposito",
						String.class);

				method.invoke(_evasioniOrdiniRemoteModel, codiceDeposito);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoOrdine() {
		return _tipoOrdine;
	}

	@Override
	public void setTipoOrdine(int tipoOrdine) {
		_tipoOrdine = tipoOrdine;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoOrdine", int.class);

				method.invoke(_evasioniOrdiniRemoteModel, tipoOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroOrdine() {
		return _numeroOrdine;
	}

	@Override
	public void setNumeroOrdine(int numeroOrdine) {
		_numeroOrdine = numeroOrdine;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroOrdine", int.class);

				method.invoke(_evasioniOrdiniRemoteModel, numeroOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCliente() {
		return _codiceCliente;
	}

	@Override
	public void setCodiceCliente(String codiceCliente) {
		_codiceCliente = codiceCliente;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCliente", String.class);

				method.invoke(_evasioniOrdiniRemoteModel, codiceCliente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroRigo() {
		return _numeroRigo;
	}

	@Override
	public void setNumeroRigo(int numeroRigo) {
		_numeroRigo = numeroRigo;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroRigo", int.class);

				method.invoke(_evasioniOrdiniRemoteModel, numeroRigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	@Override
	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceArticolo",
						String.class);

				method.invoke(_evasioniOrdiniRemoteModel, codiceArticolo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceVariante() {
		return _codiceVariante;
	}

	@Override
	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceVariante",
						String.class);

				method.invoke(_evasioniOrdiniRemoteModel, codiceVariante);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizione() {
		return _descrizione;
	}

	@Override
	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizione", String.class);

				method.invoke(_evasioniOrdiniRemoteModel, descrizione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUnitaMisura() {
		return _unitaMisura;
	}

	@Override
	public void setUnitaMisura(String unitaMisura) {
		_unitaMisura = unitaMisura;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setUnitaMisura", String.class);

				method.invoke(_evasioniOrdiniRemoteModel, unitaMisura);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaOrdinata() {
		return _quantitaOrdinata;
	}

	@Override
	public void setQuantitaOrdinata(double quantitaOrdinata) {
		_quantitaOrdinata = quantitaOrdinata;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaOrdinata",
						double.class);

				method.invoke(_evasioniOrdiniRemoteModel, quantitaOrdinata);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroProgrezzivoEvasione() {
		return _numeroProgrezzivoEvasione;
	}

	@Override
	public void setNumeroProgrezzivoEvasione(int numeroProgrezzivoEvasione) {
		_numeroProgrezzivoEvasione = numeroProgrezzivoEvasione;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroProgrezzivoEvasione",
						int.class);

				method.invoke(_evasioniOrdiniRemoteModel,
					numeroProgrezzivoEvasione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaConsegnata() {
		return _quantitaConsegnata;
	}

	@Override
	public void setQuantitaConsegnata(double quantitaConsegnata) {
		_quantitaConsegnata = quantitaConsegnata;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaConsegnata",
						double.class);

				method.invoke(_evasioniOrdiniRemoteModel, quantitaConsegnata);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataDocumentoConsegna() {
		return _dataDocumentoConsegna;
	}

	@Override
	public void setDataDocumentoConsegna(Date dataDocumentoConsegna) {
		_dataDocumentoConsegna = dataDocumentoConsegna;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setDataDocumentoConsegna",
						Date.class);

				method.invoke(_evasioniOrdiniRemoteModel, dataDocumentoConsegna);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTestAccontoSaldo() {
		return _testAccontoSaldo;
	}

	@Override
	public void setTestAccontoSaldo(String testAccontoSaldo) {
		_testAccontoSaldo = testAccontoSaldo;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setTestAccontoSaldo",
						String.class);

				method.invoke(_evasioniOrdiniRemoteModel, testAccontoSaldo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoDocumentoEvasione() {
		return _tipoDocumentoEvasione;
	}

	@Override
	public void setTipoDocumentoEvasione(String tipoDocumentoEvasione) {
		_tipoDocumentoEvasione = tipoDocumentoEvasione;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoDocumentoEvasione",
						String.class);

				method.invoke(_evasioniOrdiniRemoteModel, tipoDocumentoEvasione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroDocumentoEvasione() {
		return _numeroDocumentoEvasione;
	}

	@Override
	public void setNumeroDocumentoEvasione(int numeroDocumentoEvasione) {
		_numeroDocumentoEvasione = numeroDocumentoEvasione;

		if (_evasioniOrdiniRemoteModel != null) {
			try {
				Class<?> clazz = _evasioniOrdiniRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroDocumentoEvasione",
						int.class);

				method.invoke(_evasioniOrdiniRemoteModel,
					numeroDocumentoEvasione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getEvasioniOrdiniRemoteModel() {
		return _evasioniOrdiniRemoteModel;
	}

	public void setEvasioniOrdiniRemoteModel(
		BaseModel<?> evasioniOrdiniRemoteModel) {
		_evasioniOrdiniRemoteModel = evasioniOrdiniRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _evasioniOrdiniRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_evasioniOrdiniRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			EvasioniOrdiniLocalServiceUtil.addEvasioniOrdini(this);
		}
		else {
			EvasioniOrdiniLocalServiceUtil.updateEvasioniOrdini(this);
		}
	}

	@Override
	public EvasioniOrdini toEscapedModel() {
		return (EvasioniOrdini)ProxyUtil.newProxyInstance(EvasioniOrdini.class.getClassLoader(),
			new Class[] { EvasioniOrdini.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		EvasioniOrdiniClp clone = new EvasioniOrdiniClp();

		clone.setID(getID());
		clone.setAnno(getAnno());
		clone.setCodiceAttivita(getCodiceAttivita());
		clone.setCodiceCentro(getCodiceCentro());
		clone.setCodiceDeposito(getCodiceDeposito());
		clone.setTipoOrdine(getTipoOrdine());
		clone.setNumeroOrdine(getNumeroOrdine());
		clone.setCodiceCliente(getCodiceCliente());
		clone.setNumeroRigo(getNumeroRigo());
		clone.setCodiceArticolo(getCodiceArticolo());
		clone.setCodiceVariante(getCodiceVariante());
		clone.setDescrizione(getDescrizione());
		clone.setUnitaMisura(getUnitaMisura());
		clone.setQuantitaOrdinata(getQuantitaOrdinata());
		clone.setNumeroProgrezzivoEvasione(getNumeroProgrezzivoEvasione());
		clone.setQuantitaConsegnata(getQuantitaConsegnata());
		clone.setDataDocumentoConsegna(getDataDocumentoConsegna());
		clone.setTestAccontoSaldo(getTestAccontoSaldo());
		clone.setTipoDocumentoEvasione(getTipoDocumentoEvasione());
		clone.setNumeroDocumentoEvasione(getNumeroDocumentoEvasione());

		return clone;
	}

	@Override
	public int compareTo(EvasioniOrdini evasioniOrdini) {
		int value = 0;

		if (getNumeroDocumentoEvasione() < evasioniOrdini.getNumeroDocumentoEvasione()) {
			value = -1;
		}
		else if (getNumeroDocumentoEvasione() > evasioniOrdini.getNumeroDocumentoEvasione()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		value = getCodiceArticolo().compareTo(evasioniOrdini.getCodiceArticolo());

		if (value != 0) {
			return value;
		}

		value = getCodiceVariante().compareTo(evasioniOrdini.getCodiceVariante());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EvasioniOrdiniClp)) {
			return false;
		}

		EvasioniOrdiniClp evasioniOrdini = (EvasioniOrdiniClp)obj;

		String primaryKey = evasioniOrdini.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(41);

		sb.append("{ID=");
		sb.append(getID());
		sb.append(", anno=");
		sb.append(getAnno());
		sb.append(", codiceAttivita=");
		sb.append(getCodiceAttivita());
		sb.append(", codiceCentro=");
		sb.append(getCodiceCentro());
		sb.append(", codiceDeposito=");
		sb.append(getCodiceDeposito());
		sb.append(", tipoOrdine=");
		sb.append(getTipoOrdine());
		sb.append(", numeroOrdine=");
		sb.append(getNumeroOrdine());
		sb.append(", codiceCliente=");
		sb.append(getCodiceCliente());
		sb.append(", numeroRigo=");
		sb.append(getNumeroRigo());
		sb.append(", codiceArticolo=");
		sb.append(getCodiceArticolo());
		sb.append(", codiceVariante=");
		sb.append(getCodiceVariante());
		sb.append(", descrizione=");
		sb.append(getDescrizione());
		sb.append(", unitaMisura=");
		sb.append(getUnitaMisura());
		sb.append(", quantitaOrdinata=");
		sb.append(getQuantitaOrdinata());
		sb.append(", numeroProgrezzivoEvasione=");
		sb.append(getNumeroProgrezzivoEvasione());
		sb.append(", quantitaConsegnata=");
		sb.append(getQuantitaConsegnata());
		sb.append(", dataDocumentoConsegna=");
		sb.append(getDataDocumentoConsegna());
		sb.append(", testAccontoSaldo=");
		sb.append(getTestAccontoSaldo());
		sb.append(", tipoDocumentoEvasione=");
		sb.append(getTipoDocumentoEvasione());
		sb.append(", numeroDocumentoEvasione=");
		sb.append(getNumeroDocumentoEvasione());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(64);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.EvasioniOrdini");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>ID</column-name><column-value><![CDATA[");
		sb.append(getID());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>anno</column-name><column-value><![CDATA[");
		sb.append(getAnno());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceAttivita</column-name><column-value><![CDATA[");
		sb.append(getCodiceAttivita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCentro</column-name><column-value><![CDATA[");
		sb.append(getCodiceCentro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDeposito</column-name><column-value><![CDATA[");
		sb.append(getCodiceDeposito());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoOrdine</column-name><column-value><![CDATA[");
		sb.append(getTipoOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroOrdine</column-name><column-value><![CDATA[");
		sb.append(getNumeroOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCliente</column-name><column-value><![CDATA[");
		sb.append(getCodiceCliente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroRigo</column-name><column-value><![CDATA[");
		sb.append(getNumeroRigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceArticolo</column-name><column-value><![CDATA[");
		sb.append(getCodiceArticolo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceVariante</column-name><column-value><![CDATA[");
		sb.append(getCodiceVariante());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizione</column-name><column-value><![CDATA[");
		sb.append(getDescrizione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>unitaMisura</column-name><column-value><![CDATA[");
		sb.append(getUnitaMisura());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaOrdinata</column-name><column-value><![CDATA[");
		sb.append(getQuantitaOrdinata());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroProgrezzivoEvasione</column-name><column-value><![CDATA[");
		sb.append(getNumeroProgrezzivoEvasione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaConsegnata</column-name><column-value><![CDATA[");
		sb.append(getQuantitaConsegnata());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataDocumentoConsegna</column-name><column-value><![CDATA[");
		sb.append(getDataDocumentoConsegna());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>testAccontoSaldo</column-name><column-value><![CDATA[");
		sb.append(getTestAccontoSaldo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoDocumentoEvasione</column-name><column-value><![CDATA[");
		sb.append(getTipoDocumentoEvasione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroDocumentoEvasione</column-name><column-value><![CDATA[");
		sb.append(getNumeroDocumentoEvasione());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _ID;
	private int _anno;
	private String _codiceAttivita;
	private String _codiceCentro;
	private String _codiceDeposito;
	private int _tipoOrdine;
	private int _numeroOrdine;
	private String _codiceCliente;
	private int _numeroRigo;
	private String _codiceArticolo;
	private String _codiceVariante;
	private String _descrizione;
	private String _unitaMisura;
	private double _quantitaOrdinata;
	private int _numeroProgrezzivoEvasione;
	private double _quantitaConsegnata;
	private Date _dataDocumentoConsegna;
	private String _testAccontoSaldo;
	private String _tipoDocumentoEvasione;
	private int _numeroDocumentoEvasione;
	private BaseModel<?> _evasioniOrdiniRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}