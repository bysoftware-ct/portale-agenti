/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.RigheFattureVeditaPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.RigheFattureVeditaServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.RigheFattureVeditaServiceSoap
 * @generated
 */
public class RigheFattureVeditaSoap implements Serializable {
	public static RigheFattureVeditaSoap toSoapModel(RigheFattureVedita model) {
		RigheFattureVeditaSoap soapModel = new RigheFattureVeditaSoap();

		soapModel.setAnno(model.getAnno());
		soapModel.setCodiceAttivita(model.getCodiceAttivita());
		soapModel.setCodiceCentro(model.getCodiceCentro());
		soapModel.setNumeroProtocollo(model.getNumeroProtocollo());
		soapModel.setNumeroRigo(model.getNumeroRigo());
		soapModel.setStatoRigo(model.getStatoRigo());
		soapModel.setAnnoBolla(model.getAnnoBolla());
		soapModel.setTipoIdentificativoBolla(model.getTipoIdentificativoBolla());
		soapModel.setTipoBolla(model.getTipoBolla());
		soapModel.setCodiceDeposito(model.getCodiceDeposito());
		soapModel.setNumeroBollettario(model.getNumeroBollettario());
		soapModel.setNumeroBolla(model.getNumeroBolla());
		soapModel.setDataBollaEvasa(model.getDataBollaEvasa());
		soapModel.setRigoDDT(model.getRigoDDT());
		soapModel.setTipoRigoDDT(model.getTipoRigoDDT());
		soapModel.setCodiceCausaleMagazzino(model.getCodiceCausaleMagazzino());
		soapModel.setCodiceDepositoMagazzino(model.getCodiceDepositoMagazzino());
		soapModel.setCodiceArticolo(model.getCodiceArticolo());
		soapModel.setCodiceVariante(model.getCodiceVariante());
		soapModel.setCodiceTestoDescrizioni(model.getCodiceTestoDescrizioni());
		soapModel.setDescrizione(model.getDescrizione());
		soapModel.setUnitaMisura(model.getUnitaMisura());
		soapModel.setNumeroDecimalliQuant(model.getNumeroDecimalliQuant());
		soapModel.setQuantita1(model.getQuantita1());
		soapModel.setQuantita2(model.getQuantita2());
		soapModel.setQuantita3(model.getQuantita3());
		soapModel.setQuantita(model.getQuantita());
		soapModel.setUnitaMisura2(model.getUnitaMisura2());
		soapModel.setQuantitaUnitMisSec(model.getQuantitaUnitMisSec());
		soapModel.setNumeroDecimalliPrezzo(model.getNumeroDecimalliPrezzo());
		soapModel.setPrezzo(model.getPrezzo());
		soapModel.setPrezzoIVA(model.getPrezzoIVA());
		soapModel.setImportoLordo(model.getImportoLordo());
		soapModel.setImportoLordoIVA(model.getImportoLordoIVA());
		soapModel.setSconto1(model.getSconto1());
		soapModel.setSconto2(model.getSconto2());
		soapModel.setSconto3(model.getSconto3());
		soapModel.setImportoNettoRigo(model.getImportoNettoRigo());
		soapModel.setImportoNettoRigoIVA(model.getImportoNettoRigoIVA());
		soapModel.setImportoNetto(model.getImportoNetto());
		soapModel.setImportoNettoIVA(model.getImportoNettoIVA());
		soapModel.setImportoCONAIAddebitatoInt(model.getImportoCONAIAddebitatoInt());
		soapModel.setImportoCONAIAddebitatoOrig(model.getImportoCONAIAddebitatoOrig());
		soapModel.setCodiceIVAFatturazione(model.getCodiceIVAFatturazione());
		soapModel.setNomenclaturaCombinata(model.getNomenclaturaCombinata());
		soapModel.setTestStampaDisBase(model.getTestStampaDisBase());
		soapModel.setTipoOrdineEvaso(model.getTipoOrdineEvaso());
		soapModel.setAnnoOrdineEvaso(model.getAnnoOrdineEvaso());
		soapModel.setNumeroOrdineEvaso(model.getNumeroOrdineEvaso());
		soapModel.setNumeroRigoOrdineEvaso(model.getNumeroRigoOrdineEvaso());
		soapModel.setTipoEvasione(model.getTipoEvasione());
		soapModel.setDescrizioneRiferimento(model.getDescrizioneRiferimento());
		soapModel.setNumeroPrimaNotaMagazzino(model.getNumeroPrimaNotaMagazzino());
		soapModel.setNumeroMovimentoMagazzino(model.getNumeroMovimentoMagazzino());
		soapModel.setTipoEsplosione(model.getTipoEsplosione());
		soapModel.setLivelloMassimoEsplosione(model.getLivelloMassimoEsplosione());
		soapModel.setLibStr1(model.getLibStr1());
		soapModel.setLibStr2(model.getLibStr2());
		soapModel.setLibStr3(model.getLibStr3());
		soapModel.setLibDbl1(model.getLibDbl1());
		soapModel.setLibDbl2(model.getLibDbl2());
		soapModel.setLibDbl3(model.getLibDbl3());
		soapModel.setLibLng1(model.getLibLng1());
		soapModel.setLibLng2(model.getLibLng2());
		soapModel.setLibLng3(model.getLibLng3());
		soapModel.setLibDat1(model.getLibDat1());
		soapModel.setLibDat2(model.getLibDat2());
		soapModel.setLibDat3(model.getLibDat3());
		soapModel.setTestFatturazioneAuto(model.getTestFatturazioneAuto());

		return soapModel;
	}

	public static RigheFattureVeditaSoap[] toSoapModels(
		RigheFattureVedita[] models) {
		RigheFattureVeditaSoap[] soapModels = new RigheFattureVeditaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static RigheFattureVeditaSoap[][] toSoapModels(
		RigheFattureVedita[][] models) {
		RigheFattureVeditaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new RigheFattureVeditaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new RigheFattureVeditaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static RigheFattureVeditaSoap[] toSoapModels(
		List<RigheFattureVedita> models) {
		List<RigheFattureVeditaSoap> soapModels = new ArrayList<RigheFattureVeditaSoap>(models.size());

		for (RigheFattureVedita model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new RigheFattureVeditaSoap[soapModels.size()]);
	}

	public RigheFattureVeditaSoap() {
	}

	public RigheFattureVeditaPK getPrimaryKey() {
		return new RigheFattureVeditaPK(_anno, _codiceAttivita, _codiceCentro,
			_numeroProtocollo, _numeroRigo);
	}

	public void setPrimaryKey(RigheFattureVeditaPK pk) {
		setAnno(pk.anno);
		setCodiceAttivita(pk.codiceAttivita);
		setCodiceCentro(pk.codiceCentro);
		setNumeroProtocollo(pk.numeroProtocollo);
		setNumeroRigo(pk.numeroRigo);
	}

	public int getAnno() {
		return _anno;
	}

	public void setAnno(int anno) {
		_anno = anno;
	}

	public String getCodiceAttivita() {
		return _codiceAttivita;
	}

	public void setCodiceAttivita(String codiceAttivita) {
		_codiceAttivita = codiceAttivita;
	}

	public String getCodiceCentro() {
		return _codiceCentro;
	}

	public void setCodiceCentro(String codiceCentro) {
		_codiceCentro = codiceCentro;
	}

	public int getNumeroProtocollo() {
		return _numeroProtocollo;
	}

	public void setNumeroProtocollo(int numeroProtocollo) {
		_numeroProtocollo = numeroProtocollo;
	}

	public int getNumeroRigo() {
		return _numeroRigo;
	}

	public void setNumeroRigo(int numeroRigo) {
		_numeroRigo = numeroRigo;
	}

	public boolean getStatoRigo() {
		return _statoRigo;
	}

	public boolean isStatoRigo() {
		return _statoRigo;
	}

	public void setStatoRigo(boolean statoRigo) {
		_statoRigo = statoRigo;
	}

	public int getAnnoBolla() {
		return _annoBolla;
	}

	public void setAnnoBolla(int annoBolla) {
		_annoBolla = annoBolla;
	}

	public int getTipoIdentificativoBolla() {
		return _tipoIdentificativoBolla;
	}

	public void setTipoIdentificativoBolla(int tipoIdentificativoBolla) {
		_tipoIdentificativoBolla = tipoIdentificativoBolla;
	}

	public String getTipoBolla() {
		return _tipoBolla;
	}

	public void setTipoBolla(String tipoBolla) {
		_tipoBolla = tipoBolla;
	}

	public String getCodiceDeposito() {
		return _codiceDeposito;
	}

	public void setCodiceDeposito(String codiceDeposito) {
		_codiceDeposito = codiceDeposito;
	}

	public int getNumeroBollettario() {
		return _numeroBollettario;
	}

	public void setNumeroBollettario(int numeroBollettario) {
		_numeroBollettario = numeroBollettario;
	}

	public int getNumeroBolla() {
		return _numeroBolla;
	}

	public void setNumeroBolla(int numeroBolla) {
		_numeroBolla = numeroBolla;
	}

	public Date getDataBollaEvasa() {
		return _dataBollaEvasa;
	}

	public void setDataBollaEvasa(Date dataBollaEvasa) {
		_dataBollaEvasa = dataBollaEvasa;
	}

	public int getRigoDDT() {
		return _rigoDDT;
	}

	public void setRigoDDT(int rigoDDT) {
		_rigoDDT = rigoDDT;
	}

	public int getTipoRigoDDT() {
		return _tipoRigoDDT;
	}

	public void setTipoRigoDDT(int tipoRigoDDT) {
		_tipoRigoDDT = tipoRigoDDT;
	}

	public String getCodiceCausaleMagazzino() {
		return _codiceCausaleMagazzino;
	}

	public void setCodiceCausaleMagazzino(String codiceCausaleMagazzino) {
		_codiceCausaleMagazzino = codiceCausaleMagazzino;
	}

	public String getCodiceDepositoMagazzino() {
		return _codiceDepositoMagazzino;
	}

	public void setCodiceDepositoMagazzino(String codiceDepositoMagazzino) {
		_codiceDepositoMagazzino = codiceDepositoMagazzino;
	}

	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;
	}

	public String getCodiceVariante() {
		return _codiceVariante;
	}

	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;
	}

	public String getCodiceTestoDescrizioni() {
		return _codiceTestoDescrizioni;
	}

	public void setCodiceTestoDescrizioni(String codiceTestoDescrizioni) {
		_codiceTestoDescrizioni = codiceTestoDescrizioni;
	}

	public String getDescrizione() {
		return _descrizione;
	}

	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;
	}

	public String getUnitaMisura() {
		return _unitaMisura;
	}

	public void setUnitaMisura(String unitaMisura) {
		_unitaMisura = unitaMisura;
	}

	public int getNumeroDecimalliQuant() {
		return _numeroDecimalliQuant;
	}

	public void setNumeroDecimalliQuant(int numeroDecimalliQuant) {
		_numeroDecimalliQuant = numeroDecimalliQuant;
	}

	public double getQuantita1() {
		return _quantita1;
	}

	public void setQuantita1(double quantita1) {
		_quantita1 = quantita1;
	}

	public double getQuantita2() {
		return _quantita2;
	}

	public void setQuantita2(double quantita2) {
		_quantita2 = quantita2;
	}

	public double getQuantita3() {
		return _quantita3;
	}

	public void setQuantita3(double quantita3) {
		_quantita3 = quantita3;
	}

	public double getQuantita() {
		return _quantita;
	}

	public void setQuantita(double quantita) {
		_quantita = quantita;
	}

	public String getUnitaMisura2() {
		return _unitaMisura2;
	}

	public void setUnitaMisura2(String unitaMisura2) {
		_unitaMisura2 = unitaMisura2;
	}

	public double getQuantitaUnitMisSec() {
		return _quantitaUnitMisSec;
	}

	public void setQuantitaUnitMisSec(double quantitaUnitMisSec) {
		_quantitaUnitMisSec = quantitaUnitMisSec;
	}

	public int getNumeroDecimalliPrezzo() {
		return _numeroDecimalliPrezzo;
	}

	public void setNumeroDecimalliPrezzo(int numeroDecimalliPrezzo) {
		_numeroDecimalliPrezzo = numeroDecimalliPrezzo;
	}

	public double getPrezzo() {
		return _prezzo;
	}

	public void setPrezzo(double prezzo) {
		_prezzo = prezzo;
	}

	public double getPrezzoIVA() {
		return _prezzoIVA;
	}

	public void setPrezzoIVA(double prezzoIVA) {
		_prezzoIVA = prezzoIVA;
	}

	public double getImportoLordo() {
		return _importoLordo;
	}

	public void setImportoLordo(double importoLordo) {
		_importoLordo = importoLordo;
	}

	public double getImportoLordoIVA() {
		return _importoLordoIVA;
	}

	public void setImportoLordoIVA(double importoLordoIVA) {
		_importoLordoIVA = importoLordoIVA;
	}

	public double getSconto1() {
		return _sconto1;
	}

	public void setSconto1(double sconto1) {
		_sconto1 = sconto1;
	}

	public double getSconto2() {
		return _sconto2;
	}

	public void setSconto2(double sconto2) {
		_sconto2 = sconto2;
	}

	public double getSconto3() {
		return _sconto3;
	}

	public void setSconto3(double sconto3) {
		_sconto3 = sconto3;
	}

	public double getImportoNettoRigo() {
		return _importoNettoRigo;
	}

	public void setImportoNettoRigo(double importoNettoRigo) {
		_importoNettoRigo = importoNettoRigo;
	}

	public double getImportoNettoRigoIVA() {
		return _importoNettoRigoIVA;
	}

	public void setImportoNettoRigoIVA(double importoNettoRigoIVA) {
		_importoNettoRigoIVA = importoNettoRigoIVA;
	}

	public double getImportoNetto() {
		return _importoNetto;
	}

	public void setImportoNetto(double importoNetto) {
		_importoNetto = importoNetto;
	}

	public double getImportoNettoIVA() {
		return _importoNettoIVA;
	}

	public void setImportoNettoIVA(double importoNettoIVA) {
		_importoNettoIVA = importoNettoIVA;
	}

	public double getImportoCONAIAddebitatoInt() {
		return _importoCONAIAddebitatoInt;
	}

	public void setImportoCONAIAddebitatoInt(double importoCONAIAddebitatoInt) {
		_importoCONAIAddebitatoInt = importoCONAIAddebitatoInt;
	}

	public double getImportoCONAIAddebitatoOrig() {
		return _importoCONAIAddebitatoOrig;
	}

	public void setImportoCONAIAddebitatoOrig(double importoCONAIAddebitatoOrig) {
		_importoCONAIAddebitatoOrig = importoCONAIAddebitatoOrig;
	}

	public String getCodiceIVAFatturazione() {
		return _codiceIVAFatturazione;
	}

	public void setCodiceIVAFatturazione(String codiceIVAFatturazione) {
		_codiceIVAFatturazione = codiceIVAFatturazione;
	}

	public int getNomenclaturaCombinata() {
		return _nomenclaturaCombinata;
	}

	public void setNomenclaturaCombinata(int nomenclaturaCombinata) {
		_nomenclaturaCombinata = nomenclaturaCombinata;
	}

	public boolean getTestStampaDisBase() {
		return _testStampaDisBase;
	}

	public boolean isTestStampaDisBase() {
		return _testStampaDisBase;
	}

	public void setTestStampaDisBase(boolean testStampaDisBase) {
		_testStampaDisBase = testStampaDisBase;
	}

	public int getTipoOrdineEvaso() {
		return _tipoOrdineEvaso;
	}

	public void setTipoOrdineEvaso(int tipoOrdineEvaso) {
		_tipoOrdineEvaso = tipoOrdineEvaso;
	}

	public int getAnnoOrdineEvaso() {
		return _annoOrdineEvaso;
	}

	public void setAnnoOrdineEvaso(int annoOrdineEvaso) {
		_annoOrdineEvaso = annoOrdineEvaso;
	}

	public int getNumeroOrdineEvaso() {
		return _numeroOrdineEvaso;
	}

	public void setNumeroOrdineEvaso(int numeroOrdineEvaso) {
		_numeroOrdineEvaso = numeroOrdineEvaso;
	}

	public int getNumeroRigoOrdineEvaso() {
		return _numeroRigoOrdineEvaso;
	}

	public void setNumeroRigoOrdineEvaso(int numeroRigoOrdineEvaso) {
		_numeroRigoOrdineEvaso = numeroRigoOrdineEvaso;
	}

	public String getTipoEvasione() {
		return _tipoEvasione;
	}

	public void setTipoEvasione(String tipoEvasione) {
		_tipoEvasione = tipoEvasione;
	}

	public String getDescrizioneRiferimento() {
		return _descrizioneRiferimento;
	}

	public void setDescrizioneRiferimento(String descrizioneRiferimento) {
		_descrizioneRiferimento = descrizioneRiferimento;
	}

	public int getNumeroPrimaNotaMagazzino() {
		return _numeroPrimaNotaMagazzino;
	}

	public void setNumeroPrimaNotaMagazzino(int numeroPrimaNotaMagazzino) {
		_numeroPrimaNotaMagazzino = numeroPrimaNotaMagazzino;
	}

	public int getNumeroMovimentoMagazzino() {
		return _numeroMovimentoMagazzino;
	}

	public void setNumeroMovimentoMagazzino(int numeroMovimentoMagazzino) {
		_numeroMovimentoMagazzino = numeroMovimentoMagazzino;
	}

	public int getTipoEsplosione() {
		return _tipoEsplosione;
	}

	public void setTipoEsplosione(int tipoEsplosione) {
		_tipoEsplosione = tipoEsplosione;
	}

	public int getLivelloMassimoEsplosione() {
		return _livelloMassimoEsplosione;
	}

	public void setLivelloMassimoEsplosione(int livelloMassimoEsplosione) {
		_livelloMassimoEsplosione = livelloMassimoEsplosione;
	}

	public String getLibStr1() {
		return _libStr1;
	}

	public void setLibStr1(String libStr1) {
		_libStr1 = libStr1;
	}

	public String getLibStr2() {
		return _libStr2;
	}

	public void setLibStr2(String libStr2) {
		_libStr2 = libStr2;
	}

	public String getLibStr3() {
		return _libStr3;
	}

	public void setLibStr3(String libStr3) {
		_libStr3 = libStr3;
	}

	public double getLibDbl1() {
		return _libDbl1;
	}

	public void setLibDbl1(double libDbl1) {
		_libDbl1 = libDbl1;
	}

	public double getLibDbl2() {
		return _libDbl2;
	}

	public void setLibDbl2(double libDbl2) {
		_libDbl2 = libDbl2;
	}

	public double getLibDbl3() {
		return _libDbl3;
	}

	public void setLibDbl3(double libDbl3) {
		_libDbl3 = libDbl3;
	}

	public long getLibLng1() {
		return _libLng1;
	}

	public void setLibLng1(long libLng1) {
		_libLng1 = libLng1;
	}

	public long getLibLng2() {
		return _libLng2;
	}

	public void setLibLng2(long libLng2) {
		_libLng2 = libLng2;
	}

	public long getLibLng3() {
		return _libLng3;
	}

	public void setLibLng3(long libLng3) {
		_libLng3 = libLng3;
	}

	public Date getLibDat1() {
		return _libDat1;
	}

	public void setLibDat1(Date libDat1) {
		_libDat1 = libDat1;
	}

	public Date getLibDat2() {
		return _libDat2;
	}

	public void setLibDat2(Date libDat2) {
		_libDat2 = libDat2;
	}

	public Date getLibDat3() {
		return _libDat3;
	}

	public void setLibDat3(Date libDat3) {
		_libDat3 = libDat3;
	}

	public boolean getTestFatturazioneAuto() {
		return _testFatturazioneAuto;
	}

	public boolean isTestFatturazioneAuto() {
		return _testFatturazioneAuto;
	}

	public void setTestFatturazioneAuto(boolean testFatturazioneAuto) {
		_testFatturazioneAuto = testFatturazioneAuto;
	}

	private int _anno;
	private String _codiceAttivita;
	private String _codiceCentro;
	private int _numeroProtocollo;
	private int _numeroRigo;
	private boolean _statoRigo;
	private int _annoBolla;
	private int _tipoIdentificativoBolla;
	private String _tipoBolla;
	private String _codiceDeposito;
	private int _numeroBollettario;
	private int _numeroBolla;
	private Date _dataBollaEvasa;
	private int _rigoDDT;
	private int _tipoRigoDDT;
	private String _codiceCausaleMagazzino;
	private String _codiceDepositoMagazzino;
	private String _codiceArticolo;
	private String _codiceVariante;
	private String _codiceTestoDescrizioni;
	private String _descrizione;
	private String _unitaMisura;
	private int _numeroDecimalliQuant;
	private double _quantita1;
	private double _quantita2;
	private double _quantita3;
	private double _quantita;
	private String _unitaMisura2;
	private double _quantitaUnitMisSec;
	private int _numeroDecimalliPrezzo;
	private double _prezzo;
	private double _prezzoIVA;
	private double _importoLordo;
	private double _importoLordoIVA;
	private double _sconto1;
	private double _sconto2;
	private double _sconto3;
	private double _importoNettoRigo;
	private double _importoNettoRigoIVA;
	private double _importoNetto;
	private double _importoNettoIVA;
	private double _importoCONAIAddebitatoInt;
	private double _importoCONAIAddebitatoOrig;
	private String _codiceIVAFatturazione;
	private int _nomenclaturaCombinata;
	private boolean _testStampaDisBase;
	private int _tipoOrdineEvaso;
	private int _annoOrdineEvaso;
	private int _numeroOrdineEvaso;
	private int _numeroRigoOrdineEvaso;
	private String _tipoEvasione;
	private String _descrizioneRiferimento;
	private int _numeroPrimaNotaMagazzino;
	private int _numeroMovimentoMagazzino;
	private int _tipoEsplosione;
	private int _livelloMassimoEsplosione;
	private String _libStr1;
	private String _libStr2;
	private String _libStr3;
	private double _libDbl1;
	private double _libDbl2;
	private double _libDbl3;
	private long _libLng1;
	private long _libLng2;
	private long _libLng3;
	private Date _libDat1;
	private Date _libDat2;
	private Date _libDat3;
	private boolean _testFatturazioneAuto;
}