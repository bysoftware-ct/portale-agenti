/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Ordinato}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Ordinato
 * @generated
 */
public class OrdinatoWrapper implements Ordinato, ModelWrapper<Ordinato> {
	public OrdinatoWrapper(Ordinato ordinato) {
		_ordinato = ordinato;
	}

	@Override
	public Class<?> getModelClass() {
		return Ordinato.class;
	}

	@Override
	public String getModelClassName() {
		return Ordinato.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codiceDesposito", getCodiceDesposito());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("quantitaOrdinataFornitore",
			getQuantitaOrdinataFornitore());
		attributes.put("quantitaOrdinataProduzione",
			getQuantitaOrdinataProduzione());
		attributes.put("quantitaOrdinataAltri", getQuantitaOrdinataAltri());
		attributes.put("quantitaImpegnataClienti", getQuantitaImpegnataClienti());
		attributes.put("quantitaImpegnataProduzione",
			getQuantitaImpegnataProduzione());
		attributes.put("quantitaImpegnataAltri", getQuantitaImpegnataAltri());
		attributes.put("quantitaOrdinataFornitoreUnMis2",
			getQuantitaOrdinataFornitoreUnMis2());
		attributes.put("quantitaOrdinataProduzioneUnMis2",
			getQuantitaOrdinataProduzioneUnMis2());
		attributes.put("quantitaOrdinataAltriUnMis2",
			getQuantitaOrdinataAltriUnMis2());
		attributes.put("quantitaImpegnataClientiUnMis2",
			getQuantitaImpegnataClientiUnMis2());
		attributes.put("quantitaImpegnataProduzioneUnMis2",
			getQuantitaImpegnataProduzioneUnMis2());
		attributes.put("quantitaImpegnataAltriUnMis2",
			getQuantitaImpegnataAltriUnMis2());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codiceDesposito = (String)attributes.get("codiceDesposito");

		if (codiceDesposito != null) {
			setCodiceDesposito(codiceDesposito);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		Double quantitaOrdinataFornitore = (Double)attributes.get(
				"quantitaOrdinataFornitore");

		if (quantitaOrdinataFornitore != null) {
			setQuantitaOrdinataFornitore(quantitaOrdinataFornitore);
		}

		Double quantitaOrdinataProduzione = (Double)attributes.get(
				"quantitaOrdinataProduzione");

		if (quantitaOrdinataProduzione != null) {
			setQuantitaOrdinataProduzione(quantitaOrdinataProduzione);
		}

		Double quantitaOrdinataAltri = (Double)attributes.get(
				"quantitaOrdinataAltri");

		if (quantitaOrdinataAltri != null) {
			setQuantitaOrdinataAltri(quantitaOrdinataAltri);
		}

		Double quantitaImpegnataClienti = (Double)attributes.get(
				"quantitaImpegnataClienti");

		if (quantitaImpegnataClienti != null) {
			setQuantitaImpegnataClienti(quantitaImpegnataClienti);
		}

		Double quantitaImpegnataProduzione = (Double)attributes.get(
				"quantitaImpegnataProduzione");

		if (quantitaImpegnataProduzione != null) {
			setQuantitaImpegnataProduzione(quantitaImpegnataProduzione);
		}

		Double quantitaImpegnataAltri = (Double)attributes.get(
				"quantitaImpegnataAltri");

		if (quantitaImpegnataAltri != null) {
			setQuantitaImpegnataAltri(quantitaImpegnataAltri);
		}

		Double quantitaOrdinataFornitoreUnMis2 = (Double)attributes.get(
				"quantitaOrdinataFornitoreUnMis2");

		if (quantitaOrdinataFornitoreUnMis2 != null) {
			setQuantitaOrdinataFornitoreUnMis2(quantitaOrdinataFornitoreUnMis2);
		}

		Double quantitaOrdinataProduzioneUnMis2 = (Double)attributes.get(
				"quantitaOrdinataProduzioneUnMis2");

		if (quantitaOrdinataProduzioneUnMis2 != null) {
			setQuantitaOrdinataProduzioneUnMis2(quantitaOrdinataProduzioneUnMis2);
		}

		Double quantitaOrdinataAltriUnMis2 = (Double)attributes.get(
				"quantitaOrdinataAltriUnMis2");

		if (quantitaOrdinataAltriUnMis2 != null) {
			setQuantitaOrdinataAltriUnMis2(quantitaOrdinataAltriUnMis2);
		}

		Double quantitaImpegnataClientiUnMis2 = (Double)attributes.get(
				"quantitaImpegnataClientiUnMis2");

		if (quantitaImpegnataClientiUnMis2 != null) {
			setQuantitaImpegnataClientiUnMis2(quantitaImpegnataClientiUnMis2);
		}

		Double quantitaImpegnataProduzioneUnMis2 = (Double)attributes.get(
				"quantitaImpegnataProduzioneUnMis2");

		if (quantitaImpegnataProduzioneUnMis2 != null) {
			setQuantitaImpegnataProduzioneUnMis2(quantitaImpegnataProduzioneUnMis2);
		}

		Double quantitaImpegnataAltriUnMis2 = (Double)attributes.get(
				"quantitaImpegnataAltriUnMis2");

		if (quantitaImpegnataAltriUnMis2 != null) {
			setQuantitaImpegnataAltriUnMis2(quantitaImpegnataAltriUnMis2);
		}
	}

	/**
	* Returns the primary key of this ordinato.
	*
	* @return the primary key of this ordinato
	*/
	@Override
	public it.bysoftware.ct.service.persistence.OrdinatoPK getPrimaryKey() {
		return _ordinato.getPrimaryKey();
	}

	/**
	* Sets the primary key of this ordinato.
	*
	* @param primaryKey the primary key of this ordinato
	*/
	@Override
	public void setPrimaryKey(
		it.bysoftware.ct.service.persistence.OrdinatoPK primaryKey) {
		_ordinato.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the codice desposito of this ordinato.
	*
	* @return the codice desposito of this ordinato
	*/
	@Override
	public java.lang.String getCodiceDesposito() {
		return _ordinato.getCodiceDesposito();
	}

	/**
	* Sets the codice desposito of this ordinato.
	*
	* @param codiceDesposito the codice desposito of this ordinato
	*/
	@Override
	public void setCodiceDesposito(java.lang.String codiceDesposito) {
		_ordinato.setCodiceDesposito(codiceDesposito);
	}

	/**
	* Returns the codice articolo of this ordinato.
	*
	* @return the codice articolo of this ordinato
	*/
	@Override
	public java.lang.String getCodiceArticolo() {
		return _ordinato.getCodiceArticolo();
	}

	/**
	* Sets the codice articolo of this ordinato.
	*
	* @param codiceArticolo the codice articolo of this ordinato
	*/
	@Override
	public void setCodiceArticolo(java.lang.String codiceArticolo) {
		_ordinato.setCodiceArticolo(codiceArticolo);
	}

	/**
	* Returns the codice variante of this ordinato.
	*
	* @return the codice variante of this ordinato
	*/
	@Override
	public java.lang.String getCodiceVariante() {
		return _ordinato.getCodiceVariante();
	}

	/**
	* Sets the codice variante of this ordinato.
	*
	* @param codiceVariante the codice variante of this ordinato
	*/
	@Override
	public void setCodiceVariante(java.lang.String codiceVariante) {
		_ordinato.setCodiceVariante(codiceVariante);
	}

	/**
	* Returns the quantita ordinata fornitore of this ordinato.
	*
	* @return the quantita ordinata fornitore of this ordinato
	*/
	@Override
	public double getQuantitaOrdinataFornitore() {
		return _ordinato.getQuantitaOrdinataFornitore();
	}

	/**
	* Sets the quantita ordinata fornitore of this ordinato.
	*
	* @param quantitaOrdinataFornitore the quantita ordinata fornitore of this ordinato
	*/
	@Override
	public void setQuantitaOrdinataFornitore(double quantitaOrdinataFornitore) {
		_ordinato.setQuantitaOrdinataFornitore(quantitaOrdinataFornitore);
	}

	/**
	* Returns the quantita ordinata produzione of this ordinato.
	*
	* @return the quantita ordinata produzione of this ordinato
	*/
	@Override
	public double getQuantitaOrdinataProduzione() {
		return _ordinato.getQuantitaOrdinataProduzione();
	}

	/**
	* Sets the quantita ordinata produzione of this ordinato.
	*
	* @param quantitaOrdinataProduzione the quantita ordinata produzione of this ordinato
	*/
	@Override
	public void setQuantitaOrdinataProduzione(double quantitaOrdinataProduzione) {
		_ordinato.setQuantitaOrdinataProduzione(quantitaOrdinataProduzione);
	}

	/**
	* Returns the quantita ordinata altri of this ordinato.
	*
	* @return the quantita ordinata altri of this ordinato
	*/
	@Override
	public double getQuantitaOrdinataAltri() {
		return _ordinato.getQuantitaOrdinataAltri();
	}

	/**
	* Sets the quantita ordinata altri of this ordinato.
	*
	* @param quantitaOrdinataAltri the quantita ordinata altri of this ordinato
	*/
	@Override
	public void setQuantitaOrdinataAltri(double quantitaOrdinataAltri) {
		_ordinato.setQuantitaOrdinataAltri(quantitaOrdinataAltri);
	}

	/**
	* Returns the quantita impegnata clienti of this ordinato.
	*
	* @return the quantita impegnata clienti of this ordinato
	*/
	@Override
	public double getQuantitaImpegnataClienti() {
		return _ordinato.getQuantitaImpegnataClienti();
	}

	/**
	* Sets the quantita impegnata clienti of this ordinato.
	*
	* @param quantitaImpegnataClienti the quantita impegnata clienti of this ordinato
	*/
	@Override
	public void setQuantitaImpegnataClienti(double quantitaImpegnataClienti) {
		_ordinato.setQuantitaImpegnataClienti(quantitaImpegnataClienti);
	}

	/**
	* Returns the quantita impegnata produzione of this ordinato.
	*
	* @return the quantita impegnata produzione of this ordinato
	*/
	@Override
	public double getQuantitaImpegnataProduzione() {
		return _ordinato.getQuantitaImpegnataProduzione();
	}

	/**
	* Sets the quantita impegnata produzione of this ordinato.
	*
	* @param quantitaImpegnataProduzione the quantita impegnata produzione of this ordinato
	*/
	@Override
	public void setQuantitaImpegnataProduzione(
		double quantitaImpegnataProduzione) {
		_ordinato.setQuantitaImpegnataProduzione(quantitaImpegnataProduzione);
	}

	/**
	* Returns the quantita impegnata altri of this ordinato.
	*
	* @return the quantita impegnata altri of this ordinato
	*/
	@Override
	public double getQuantitaImpegnataAltri() {
		return _ordinato.getQuantitaImpegnataAltri();
	}

	/**
	* Sets the quantita impegnata altri of this ordinato.
	*
	* @param quantitaImpegnataAltri the quantita impegnata altri of this ordinato
	*/
	@Override
	public void setQuantitaImpegnataAltri(double quantitaImpegnataAltri) {
		_ordinato.setQuantitaImpegnataAltri(quantitaImpegnataAltri);
	}

	/**
	* Returns the quantita ordinata fornitore un mis2 of this ordinato.
	*
	* @return the quantita ordinata fornitore un mis2 of this ordinato
	*/
	@Override
	public double getQuantitaOrdinataFornitoreUnMis2() {
		return _ordinato.getQuantitaOrdinataFornitoreUnMis2();
	}

	/**
	* Sets the quantita ordinata fornitore un mis2 of this ordinato.
	*
	* @param quantitaOrdinataFornitoreUnMis2 the quantita ordinata fornitore un mis2 of this ordinato
	*/
	@Override
	public void setQuantitaOrdinataFornitoreUnMis2(
		double quantitaOrdinataFornitoreUnMis2) {
		_ordinato.setQuantitaOrdinataFornitoreUnMis2(quantitaOrdinataFornitoreUnMis2);
	}

	/**
	* Returns the quantita ordinata produzione un mis2 of this ordinato.
	*
	* @return the quantita ordinata produzione un mis2 of this ordinato
	*/
	@Override
	public double getQuantitaOrdinataProduzioneUnMis2() {
		return _ordinato.getQuantitaOrdinataProduzioneUnMis2();
	}

	/**
	* Sets the quantita ordinata produzione un mis2 of this ordinato.
	*
	* @param quantitaOrdinataProduzioneUnMis2 the quantita ordinata produzione un mis2 of this ordinato
	*/
	@Override
	public void setQuantitaOrdinataProduzioneUnMis2(
		double quantitaOrdinataProduzioneUnMis2) {
		_ordinato.setQuantitaOrdinataProduzioneUnMis2(quantitaOrdinataProduzioneUnMis2);
	}

	/**
	* Returns the quantita ordinata altri un mis2 of this ordinato.
	*
	* @return the quantita ordinata altri un mis2 of this ordinato
	*/
	@Override
	public double getQuantitaOrdinataAltriUnMis2() {
		return _ordinato.getQuantitaOrdinataAltriUnMis2();
	}

	/**
	* Sets the quantita ordinata altri un mis2 of this ordinato.
	*
	* @param quantitaOrdinataAltriUnMis2 the quantita ordinata altri un mis2 of this ordinato
	*/
	@Override
	public void setQuantitaOrdinataAltriUnMis2(
		double quantitaOrdinataAltriUnMis2) {
		_ordinato.setQuantitaOrdinataAltriUnMis2(quantitaOrdinataAltriUnMis2);
	}

	/**
	* Returns the quantita impegnata clienti un mis2 of this ordinato.
	*
	* @return the quantita impegnata clienti un mis2 of this ordinato
	*/
	@Override
	public double getQuantitaImpegnataClientiUnMis2() {
		return _ordinato.getQuantitaImpegnataClientiUnMis2();
	}

	/**
	* Sets the quantita impegnata clienti un mis2 of this ordinato.
	*
	* @param quantitaImpegnataClientiUnMis2 the quantita impegnata clienti un mis2 of this ordinato
	*/
	@Override
	public void setQuantitaImpegnataClientiUnMis2(
		double quantitaImpegnataClientiUnMis2) {
		_ordinato.setQuantitaImpegnataClientiUnMis2(quantitaImpegnataClientiUnMis2);
	}

	/**
	* Returns the quantita impegnata produzione un mis2 of this ordinato.
	*
	* @return the quantita impegnata produzione un mis2 of this ordinato
	*/
	@Override
	public double getQuantitaImpegnataProduzioneUnMis2() {
		return _ordinato.getQuantitaImpegnataProduzioneUnMis2();
	}

	/**
	* Sets the quantita impegnata produzione un mis2 of this ordinato.
	*
	* @param quantitaImpegnataProduzioneUnMis2 the quantita impegnata produzione un mis2 of this ordinato
	*/
	@Override
	public void setQuantitaImpegnataProduzioneUnMis2(
		double quantitaImpegnataProduzioneUnMis2) {
		_ordinato.setQuantitaImpegnataProduzioneUnMis2(quantitaImpegnataProduzioneUnMis2);
	}

	/**
	* Returns the quantita impegnata altri un mis2 of this ordinato.
	*
	* @return the quantita impegnata altri un mis2 of this ordinato
	*/
	@Override
	public double getQuantitaImpegnataAltriUnMis2() {
		return _ordinato.getQuantitaImpegnataAltriUnMis2();
	}

	/**
	* Sets the quantita impegnata altri un mis2 of this ordinato.
	*
	* @param quantitaImpegnataAltriUnMis2 the quantita impegnata altri un mis2 of this ordinato
	*/
	@Override
	public void setQuantitaImpegnataAltriUnMis2(
		double quantitaImpegnataAltriUnMis2) {
		_ordinato.setQuantitaImpegnataAltriUnMis2(quantitaImpegnataAltriUnMis2);
	}

	@Override
	public boolean isNew() {
		return _ordinato.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_ordinato.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _ordinato.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_ordinato.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _ordinato.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _ordinato.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_ordinato.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _ordinato.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_ordinato.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_ordinato.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_ordinato.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new OrdinatoWrapper((Ordinato)_ordinato.clone());
	}

	@Override
	public int compareTo(it.bysoftware.ct.model.Ordinato ordinato) {
		return _ordinato.compareTo(ordinato);
	}

	@Override
	public int hashCode() {
		return _ordinato.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Ordinato> toCacheModel() {
		return _ordinato.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.Ordinato toEscapedModel() {
		return new OrdinatoWrapper(_ordinato.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.Ordinato toUnescapedModel() {
		return new OrdinatoWrapper(_ordinato.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _ordinato.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _ordinato.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_ordinato.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OrdinatoWrapper)) {
			return false;
		}

		OrdinatoWrapper ordinatoWrapper = (OrdinatoWrapper)obj;

		if (Validator.equals(_ordinato, ordinatoWrapper._ordinato)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Ordinato getWrappedOrdinato() {
		return _ordinato;
	}

	@Override
	public Ordinato getWrappedModel() {
		return _ordinato;
	}

	@Override
	public void resetOriginalValues() {
		_ordinato.resetOriginalValues();
	}

	private Ordinato _ordinato;
}