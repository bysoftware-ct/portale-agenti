/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.DatiCostantiClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.persistence.DatiCostantiClientiFornitoriPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class DatiCostantiClientiFornitoriClp extends BaseModelImpl<DatiCostantiClientiFornitori>
	implements DatiCostantiClientiFornitori {
	public DatiCostantiClientiFornitoriClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return DatiCostantiClientiFornitori.class;
	}

	@Override
	public String getModelClassName() {
		return DatiCostantiClientiFornitori.class.getName();
	}

	@Override
	public DatiCostantiClientiFornitoriPK getPrimaryKey() {
		return new DatiCostantiClientiFornitoriPK(_tipoSoggetto, _codiceSoggetto);
	}

	@Override
	public void setPrimaryKey(DatiCostantiClientiFornitoriPK primaryKey) {
		setTipoSoggetto(primaryKey.tipoSoggetto);
		setCodiceSoggetto(primaryKey.codiceSoggetto);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new DatiCostantiClientiFornitoriPK(_tipoSoggetto, _codiceSoggetto);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((DatiCostantiClientiFornitoriPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceSoggetto", getCodiceSoggetto());
		attributes.put("codiceSottoconto", getCodiceSottoconto());
		attributes.put("massimoScoperto", getMassimoScoperto());
		attributes.put("gestioneEstrattoConto", getGestioneEstrattoConto());
		attributes.put("codicePagamento", getCodicePagamento());
		attributes.put("codiceBancaPagFor", getCodiceBancaPagFor());
		attributes.put("codiceAgenziaPagFor", getCodiceAgenziaPagFor());
		attributes.put("codiceContropartita", getCodiceContropartita());
		attributes.put("calcoloIntMora", getCalcoloIntMora());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceSoggetto = (String)attributes.get("codiceSoggetto");

		if (codiceSoggetto != null) {
			setCodiceSoggetto(codiceSoggetto);
		}

		String codiceSottoconto = (String)attributes.get("codiceSottoconto");

		if (codiceSottoconto != null) {
			setCodiceSottoconto(codiceSottoconto);
		}

		Double massimoScoperto = (Double)attributes.get("massimoScoperto");

		if (massimoScoperto != null) {
			setMassimoScoperto(massimoScoperto);
		}

		Boolean gestioneEstrattoConto = (Boolean)attributes.get(
				"gestioneEstrattoConto");

		if (gestioneEstrattoConto != null) {
			setGestioneEstrattoConto(gestioneEstrattoConto);
		}

		String codicePagamento = (String)attributes.get("codicePagamento");

		if (codicePagamento != null) {
			setCodicePagamento(codicePagamento);
		}

		String codiceBancaPagFor = (String)attributes.get("codiceBancaPagFor");

		if (codiceBancaPagFor != null) {
			setCodiceBancaPagFor(codiceBancaPagFor);
		}

		String codiceAgenziaPagFor = (String)attributes.get(
				"codiceAgenziaPagFor");

		if (codiceAgenziaPagFor != null) {
			setCodiceAgenziaPagFor(codiceAgenziaPagFor);
		}

		String codiceContropartita = (String)attributes.get(
				"codiceContropartita");

		if (codiceContropartita != null) {
			setCodiceContropartita(codiceContropartita);
		}

		Boolean calcoloIntMora = (Boolean)attributes.get("calcoloIntMora");

		if (calcoloIntMora != null) {
			setCalcoloIntMora(calcoloIntMora);
		}
	}

	@Override
	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;

		if (_datiCostantiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiCostantiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoSoggetto", boolean.class);

				method.invoke(_datiCostantiClientiFornitoriRemoteModel,
					tipoSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceSoggetto() {
		return _codiceSoggetto;
	}

	@Override
	public void setCodiceSoggetto(String codiceSoggetto) {
		_codiceSoggetto = codiceSoggetto;

		if (_datiCostantiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiCostantiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceSoggetto",
						String.class);

				method.invoke(_datiCostantiClientiFornitoriRemoteModel,
					codiceSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceSottoconto() {
		return _codiceSottoconto;
	}

	@Override
	public void setCodiceSottoconto(String codiceSottoconto) {
		_codiceSottoconto = codiceSottoconto;

		if (_datiCostantiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiCostantiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceSottoconto",
						String.class);

				method.invoke(_datiCostantiClientiFornitoriRemoteModel,
					codiceSottoconto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getMassimoScoperto() {
		return _massimoScoperto;
	}

	@Override
	public void setMassimoScoperto(double massimoScoperto) {
		_massimoScoperto = massimoScoperto;

		if (_datiCostantiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiCostantiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setMassimoScoperto",
						double.class);

				method.invoke(_datiCostantiClientiFornitoriRemoteModel,
					massimoScoperto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getGestioneEstrattoConto() {
		return _gestioneEstrattoConto;
	}

	@Override
	public boolean isGestioneEstrattoConto() {
		return _gestioneEstrattoConto;
	}

	@Override
	public void setGestioneEstrattoConto(boolean gestioneEstrattoConto) {
		_gestioneEstrattoConto = gestioneEstrattoConto;

		if (_datiCostantiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiCostantiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setGestioneEstrattoConto",
						boolean.class);

				method.invoke(_datiCostantiClientiFornitoriRemoteModel,
					gestioneEstrattoConto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodicePagamento() {
		return _codicePagamento;
	}

	@Override
	public void setCodicePagamento(String codicePagamento) {
		_codicePagamento = codicePagamento;

		if (_datiCostantiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiCostantiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodicePagamento",
						String.class);

				method.invoke(_datiCostantiClientiFornitoriRemoteModel,
					codicePagamento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceBancaPagFor() {
		return _codiceBancaPagFor;
	}

	@Override
	public void setCodiceBancaPagFor(String codiceBancaPagFor) {
		_codiceBancaPagFor = codiceBancaPagFor;

		if (_datiCostantiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiCostantiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceBancaPagFor",
						String.class);

				method.invoke(_datiCostantiClientiFornitoriRemoteModel,
					codiceBancaPagFor);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceAgenziaPagFor() {
		return _codiceAgenziaPagFor;
	}

	@Override
	public void setCodiceAgenziaPagFor(String codiceAgenziaPagFor) {
		_codiceAgenziaPagFor = codiceAgenziaPagFor;

		if (_datiCostantiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiCostantiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceAgenziaPagFor",
						String.class);

				method.invoke(_datiCostantiClientiFornitoriRemoteModel,
					codiceAgenziaPagFor);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceContropartita() {
		return _codiceContropartita;
	}

	@Override
	public void setCodiceContropartita(String codiceContropartita) {
		_codiceContropartita = codiceContropartita;

		if (_datiCostantiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiCostantiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceContropartita",
						String.class);

				method.invoke(_datiCostantiClientiFornitoriRemoteModel,
					codiceContropartita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getCalcoloIntMora() {
		return _calcoloIntMora;
	}

	@Override
	public boolean isCalcoloIntMora() {
		return _calcoloIntMora;
	}

	@Override
	public void setCalcoloIntMora(boolean calcoloIntMora) {
		_calcoloIntMora = calcoloIntMora;

		if (_datiCostantiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiCostantiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCalcoloIntMora",
						boolean.class);

				method.invoke(_datiCostantiClientiFornitoriRemoteModel,
					calcoloIntMora);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getDatiCostantiClientiFornitoriRemoteModel() {
		return _datiCostantiClientiFornitoriRemoteModel;
	}

	public void setDatiCostantiClientiFornitoriRemoteModel(
		BaseModel<?> datiCostantiClientiFornitoriRemoteModel) {
		_datiCostantiClientiFornitoriRemoteModel = datiCostantiClientiFornitoriRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _datiCostantiClientiFornitoriRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_datiCostantiClientiFornitoriRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			DatiCostantiClientiFornitoriLocalServiceUtil.addDatiCostantiClientiFornitori(this);
		}
		else {
			DatiCostantiClientiFornitoriLocalServiceUtil.updateDatiCostantiClientiFornitori(this);
		}
	}

	@Override
	public DatiCostantiClientiFornitori toEscapedModel() {
		return (DatiCostantiClientiFornitori)ProxyUtil.newProxyInstance(DatiCostantiClientiFornitori.class.getClassLoader(),
			new Class[] { DatiCostantiClientiFornitori.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		DatiCostantiClientiFornitoriClp clone = new DatiCostantiClientiFornitoriClp();

		clone.setTipoSoggetto(getTipoSoggetto());
		clone.setCodiceSoggetto(getCodiceSoggetto());
		clone.setCodiceSottoconto(getCodiceSottoconto());
		clone.setMassimoScoperto(getMassimoScoperto());
		clone.setGestioneEstrattoConto(getGestioneEstrattoConto());
		clone.setCodicePagamento(getCodicePagamento());
		clone.setCodiceBancaPagFor(getCodiceBancaPagFor());
		clone.setCodiceAgenziaPagFor(getCodiceAgenziaPagFor());
		clone.setCodiceContropartita(getCodiceContropartita());
		clone.setCalcoloIntMora(getCalcoloIntMora());

		return clone;
	}

	@Override
	public int compareTo(
		DatiCostantiClientiFornitori datiCostantiClientiFornitori) {
		DatiCostantiClientiFornitoriPK primaryKey = datiCostantiClientiFornitori.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DatiCostantiClientiFornitoriClp)) {
			return false;
		}

		DatiCostantiClientiFornitoriClp datiCostantiClientiFornitori = (DatiCostantiClientiFornitoriClp)obj;

		DatiCostantiClientiFornitoriPK primaryKey = datiCostantiClientiFornitori.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{tipoSoggetto=");
		sb.append(getTipoSoggetto());
		sb.append(", codiceSoggetto=");
		sb.append(getCodiceSoggetto());
		sb.append(", codiceSottoconto=");
		sb.append(getCodiceSottoconto());
		sb.append(", massimoScoperto=");
		sb.append(getMassimoScoperto());
		sb.append(", gestioneEstrattoConto=");
		sb.append(getGestioneEstrattoConto());
		sb.append(", codicePagamento=");
		sb.append(getCodicePagamento());
		sb.append(", codiceBancaPagFor=");
		sb.append(getCodiceBancaPagFor());
		sb.append(", codiceAgenziaPagFor=");
		sb.append(getCodiceAgenziaPagFor());
		sb.append(", codiceContropartita=");
		sb.append(getCodiceContropartita());
		sb.append(", calcoloIntMora=");
		sb.append(getCalcoloIntMora());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(34);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.DatiCostantiClientiFornitori");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>tipoSoggetto</column-name><column-value><![CDATA[");
		sb.append(getTipoSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceSoggetto</column-name><column-value><![CDATA[");
		sb.append(getCodiceSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceSottoconto</column-name><column-value><![CDATA[");
		sb.append(getCodiceSottoconto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>massimoScoperto</column-name><column-value><![CDATA[");
		sb.append(getMassimoScoperto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>gestioneEstrattoConto</column-name><column-value><![CDATA[");
		sb.append(getGestioneEstrattoConto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codicePagamento</column-name><column-value><![CDATA[");
		sb.append(getCodicePagamento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceBancaPagFor</column-name><column-value><![CDATA[");
		sb.append(getCodiceBancaPagFor());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceAgenziaPagFor</column-name><column-value><![CDATA[");
		sb.append(getCodiceAgenziaPagFor());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceContropartita</column-name><column-value><![CDATA[");
		sb.append(getCodiceContropartita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>calcoloIntMora</column-name><column-value><![CDATA[");
		sb.append(getCalcoloIntMora());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private boolean _tipoSoggetto;
	private String _codiceSoggetto;
	private String _codiceSottoconto;
	private double _massimoScoperto;
	private boolean _gestioneEstrattoConto;
	private String _codicePagamento;
	private String _codiceBancaPagFor;
	private String _codiceAgenziaPagFor;
	private String _codiceContropartita;
	private boolean _calcoloIntMora;
	private BaseModel<?> _datiCostantiClientiFornitoriRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}