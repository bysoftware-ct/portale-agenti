/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.OrdinatoPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.OrdinatoServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.OrdinatoServiceSoap
 * @generated
 */
public class OrdinatoSoap implements Serializable {
	public static OrdinatoSoap toSoapModel(Ordinato model) {
		OrdinatoSoap soapModel = new OrdinatoSoap();

		soapModel.setCodiceDesposito(model.getCodiceDesposito());
		soapModel.setCodiceArticolo(model.getCodiceArticolo());
		soapModel.setCodiceVariante(model.getCodiceVariante());
		soapModel.setQuantitaOrdinataFornitore(model.getQuantitaOrdinataFornitore());
		soapModel.setQuantitaOrdinataProduzione(model.getQuantitaOrdinataProduzione());
		soapModel.setQuantitaOrdinataAltri(model.getQuantitaOrdinataAltri());
		soapModel.setQuantitaImpegnataClienti(model.getQuantitaImpegnataClienti());
		soapModel.setQuantitaImpegnataProduzione(model.getQuantitaImpegnataProduzione());
		soapModel.setQuantitaImpegnataAltri(model.getQuantitaImpegnataAltri());
		soapModel.setQuantitaOrdinataFornitoreUnMis2(model.getQuantitaOrdinataFornitoreUnMis2());
		soapModel.setQuantitaOrdinataProduzioneUnMis2(model.getQuantitaOrdinataProduzioneUnMis2());
		soapModel.setQuantitaOrdinataAltriUnMis2(model.getQuantitaOrdinataAltriUnMis2());
		soapModel.setQuantitaImpegnataClientiUnMis2(model.getQuantitaImpegnataClientiUnMis2());
		soapModel.setQuantitaImpegnataProduzioneUnMis2(model.getQuantitaImpegnataProduzioneUnMis2());
		soapModel.setQuantitaImpegnataAltriUnMis2(model.getQuantitaImpegnataAltriUnMis2());

		return soapModel;
	}

	public static OrdinatoSoap[] toSoapModels(Ordinato[] models) {
		OrdinatoSoap[] soapModels = new OrdinatoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static OrdinatoSoap[][] toSoapModels(Ordinato[][] models) {
		OrdinatoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new OrdinatoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new OrdinatoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static OrdinatoSoap[] toSoapModels(List<Ordinato> models) {
		List<OrdinatoSoap> soapModels = new ArrayList<OrdinatoSoap>(models.size());

		for (Ordinato model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new OrdinatoSoap[soapModels.size()]);
	}

	public OrdinatoSoap() {
	}

	public OrdinatoPK getPrimaryKey() {
		return new OrdinatoPK(_codiceDesposito, _codiceArticolo, _codiceVariante);
	}

	public void setPrimaryKey(OrdinatoPK pk) {
		setCodiceDesposito(pk.codiceDesposito);
		setCodiceArticolo(pk.codiceArticolo);
		setCodiceVariante(pk.codiceVariante);
	}

	public String getCodiceDesposito() {
		return _codiceDesposito;
	}

	public void setCodiceDesposito(String codiceDesposito) {
		_codiceDesposito = codiceDesposito;
	}

	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;
	}

	public String getCodiceVariante() {
		return _codiceVariante;
	}

	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;
	}

	public double getQuantitaOrdinataFornitore() {
		return _quantitaOrdinataFornitore;
	}

	public void setQuantitaOrdinataFornitore(double quantitaOrdinataFornitore) {
		_quantitaOrdinataFornitore = quantitaOrdinataFornitore;
	}

	public double getQuantitaOrdinataProduzione() {
		return _quantitaOrdinataProduzione;
	}

	public void setQuantitaOrdinataProduzione(double quantitaOrdinataProduzione) {
		_quantitaOrdinataProduzione = quantitaOrdinataProduzione;
	}

	public double getQuantitaOrdinataAltri() {
		return _quantitaOrdinataAltri;
	}

	public void setQuantitaOrdinataAltri(double quantitaOrdinataAltri) {
		_quantitaOrdinataAltri = quantitaOrdinataAltri;
	}

	public double getQuantitaImpegnataClienti() {
		return _quantitaImpegnataClienti;
	}

	public void setQuantitaImpegnataClienti(double quantitaImpegnataClienti) {
		_quantitaImpegnataClienti = quantitaImpegnataClienti;
	}

	public double getQuantitaImpegnataProduzione() {
		return _quantitaImpegnataProduzione;
	}

	public void setQuantitaImpegnataProduzione(
		double quantitaImpegnataProduzione) {
		_quantitaImpegnataProduzione = quantitaImpegnataProduzione;
	}

	public double getQuantitaImpegnataAltri() {
		return _quantitaImpegnataAltri;
	}

	public void setQuantitaImpegnataAltri(double quantitaImpegnataAltri) {
		_quantitaImpegnataAltri = quantitaImpegnataAltri;
	}

	public double getQuantitaOrdinataFornitoreUnMis2() {
		return _quantitaOrdinataFornitoreUnMis2;
	}

	public void setQuantitaOrdinataFornitoreUnMis2(
		double quantitaOrdinataFornitoreUnMis2) {
		_quantitaOrdinataFornitoreUnMis2 = quantitaOrdinataFornitoreUnMis2;
	}

	public double getQuantitaOrdinataProduzioneUnMis2() {
		return _quantitaOrdinataProduzioneUnMis2;
	}

	public void setQuantitaOrdinataProduzioneUnMis2(
		double quantitaOrdinataProduzioneUnMis2) {
		_quantitaOrdinataProduzioneUnMis2 = quantitaOrdinataProduzioneUnMis2;
	}

	public double getQuantitaOrdinataAltriUnMis2() {
		return _quantitaOrdinataAltriUnMis2;
	}

	public void setQuantitaOrdinataAltriUnMis2(
		double quantitaOrdinataAltriUnMis2) {
		_quantitaOrdinataAltriUnMis2 = quantitaOrdinataAltriUnMis2;
	}

	public double getQuantitaImpegnataClientiUnMis2() {
		return _quantitaImpegnataClientiUnMis2;
	}

	public void setQuantitaImpegnataClientiUnMis2(
		double quantitaImpegnataClientiUnMis2) {
		_quantitaImpegnataClientiUnMis2 = quantitaImpegnataClientiUnMis2;
	}

	public double getQuantitaImpegnataProduzioneUnMis2() {
		return _quantitaImpegnataProduzioneUnMis2;
	}

	public void setQuantitaImpegnataProduzioneUnMis2(
		double quantitaImpegnataProduzioneUnMis2) {
		_quantitaImpegnataProduzioneUnMis2 = quantitaImpegnataProduzioneUnMis2;
	}

	public double getQuantitaImpegnataAltriUnMis2() {
		return _quantitaImpegnataAltriUnMis2;
	}

	public void setQuantitaImpegnataAltriUnMis2(
		double quantitaImpegnataAltriUnMis2) {
		_quantitaImpegnataAltriUnMis2 = quantitaImpegnataAltriUnMis2;
	}

	private String _codiceDesposito;
	private String _codiceArticolo;
	private String _codiceVariante;
	private double _quantitaOrdinataFornitore;
	private double _quantitaOrdinataProduzione;
	private double _quantitaOrdinataAltri;
	private double _quantitaImpegnataClienti;
	private double _quantitaImpegnataProduzione;
	private double _quantitaImpegnataAltri;
	private double _quantitaOrdinataFornitoreUnMis2;
	private double _quantitaOrdinataProduzioneUnMis2;
	private double _quantitaOrdinataAltriUnMis2;
	private double _quantitaImpegnataClientiUnMis2;
	private double _quantitaImpegnataProduzioneUnMis2;
	private double _quantitaImpegnataAltriUnMis2;
}