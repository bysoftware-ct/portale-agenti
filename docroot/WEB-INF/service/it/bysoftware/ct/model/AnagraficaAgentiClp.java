/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.AnagraficaAgentiLocalServiceUtil;
import it.bysoftware.ct.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class AnagraficaAgentiClp extends BaseModelImpl<AnagraficaAgenti>
	implements AnagraficaAgenti {
	public AnagraficaAgentiClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return AnagraficaAgenti.class;
	}

	@Override
	public String getModelClassName() {
		return AnagraficaAgenti.class.getName();
	}

	@Override
	public String getPrimaryKey() {
		return _codiceAgente;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setCodiceAgente(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _codiceAgente;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codiceAgente", getCodiceAgente());
		attributes.put("nome", getNome());
		attributes.put("codiceZona", getCodiceZona());
		attributes.put("codiceProvvRigo", getCodiceProvvRigo());
		attributes.put("codiceProvvChiusura", getCodiceProvvChiusura());
		attributes.put("budget", getBudget());
		attributes.put("tipoLiquidazione", getTipoLiquidazione());
		attributes.put("giorniToll", getGiorniToll());
		attributes.put("tipoDivisa", getTipoDivisa());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codiceAgente = (String)attributes.get("codiceAgente");

		if (codiceAgente != null) {
			setCodiceAgente(codiceAgente);
		}

		String nome = (String)attributes.get("nome");

		if (nome != null) {
			setNome(nome);
		}

		String codiceZona = (String)attributes.get("codiceZona");

		if (codiceZona != null) {
			setCodiceZona(codiceZona);
		}

		String codiceProvvRigo = (String)attributes.get("codiceProvvRigo");

		if (codiceProvvRigo != null) {
			setCodiceProvvRigo(codiceProvvRigo);
		}

		String codiceProvvChiusura = (String)attributes.get(
				"codiceProvvChiusura");

		if (codiceProvvChiusura != null) {
			setCodiceProvvChiusura(codiceProvvChiusura);
		}

		Double budget = (Double)attributes.get("budget");

		if (budget != null) {
			setBudget(budget);
		}

		Integer tipoLiquidazione = (Integer)attributes.get("tipoLiquidazione");

		if (tipoLiquidazione != null) {
			setTipoLiquidazione(tipoLiquidazione);
		}

		Integer giorniToll = (Integer)attributes.get("giorniToll");

		if (giorniToll != null) {
			setGiorniToll(giorniToll);
		}

		Boolean tipoDivisa = (Boolean)attributes.get("tipoDivisa");

		if (tipoDivisa != null) {
			setTipoDivisa(tipoDivisa);
		}
	}

	@Override
	public String getCodiceAgente() {
		return _codiceAgente;
	}

	@Override
	public void setCodiceAgente(String codiceAgente) {
		_codiceAgente = codiceAgente;

		if (_anagraficaAgentiRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaAgentiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceAgente", String.class);

				method.invoke(_anagraficaAgentiRemoteModel, codiceAgente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNome() {
		return _nome;
	}

	@Override
	public void setNome(String nome) {
		_nome = nome;

		if (_anagraficaAgentiRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaAgentiRemoteModel.getClass();

				Method method = clazz.getMethod("setNome", String.class);

				method.invoke(_anagraficaAgentiRemoteModel, nome);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceZona() {
		return _codiceZona;
	}

	@Override
	public void setCodiceZona(String codiceZona) {
		_codiceZona = codiceZona;

		if (_anagraficaAgentiRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaAgentiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceZona", String.class);

				method.invoke(_anagraficaAgentiRemoteModel, codiceZona);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceProvvRigo() {
		return _codiceProvvRigo;
	}

	@Override
	public void setCodiceProvvRigo(String codiceProvvRigo) {
		_codiceProvvRigo = codiceProvvRigo;

		if (_anagraficaAgentiRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaAgentiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceProvvRigo",
						String.class);

				method.invoke(_anagraficaAgentiRemoteModel, codiceProvvRigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceProvvChiusura() {
		return _codiceProvvChiusura;
	}

	@Override
	public void setCodiceProvvChiusura(String codiceProvvChiusura) {
		_codiceProvvChiusura = codiceProvvChiusura;

		if (_anagraficaAgentiRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaAgentiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceProvvChiusura",
						String.class);

				method.invoke(_anagraficaAgentiRemoteModel, codiceProvvChiusura);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getBudget() {
		return _budget;
	}

	@Override
	public void setBudget(double budget) {
		_budget = budget;

		if (_anagraficaAgentiRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaAgentiRemoteModel.getClass();

				Method method = clazz.getMethod("setBudget", double.class);

				method.invoke(_anagraficaAgentiRemoteModel, budget);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoLiquidazione() {
		return _tipoLiquidazione;
	}

	@Override
	public void setTipoLiquidazione(int tipoLiquidazione) {
		_tipoLiquidazione = tipoLiquidazione;

		if (_anagraficaAgentiRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaAgentiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoLiquidazione", int.class);

				method.invoke(_anagraficaAgentiRemoteModel, tipoLiquidazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getGiorniToll() {
		return _giorniToll;
	}

	@Override
	public void setGiorniToll(int giorniToll) {
		_giorniToll = giorniToll;

		if (_anagraficaAgentiRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaAgentiRemoteModel.getClass();

				Method method = clazz.getMethod("setGiorniToll", int.class);

				method.invoke(_anagraficaAgentiRemoteModel, giorniToll);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getTipoDivisa() {
		return _tipoDivisa;
	}

	@Override
	public boolean isTipoDivisa() {
		return _tipoDivisa;
	}

	@Override
	public void setTipoDivisa(boolean tipoDivisa) {
		_tipoDivisa = tipoDivisa;

		if (_anagraficaAgentiRemoteModel != null) {
			try {
				Class<?> clazz = _anagraficaAgentiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoDivisa", boolean.class);

				method.invoke(_anagraficaAgentiRemoteModel, tipoDivisa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getAnagraficaAgentiRemoteModel() {
		return _anagraficaAgentiRemoteModel;
	}

	public void setAnagraficaAgentiRemoteModel(
		BaseModel<?> anagraficaAgentiRemoteModel) {
		_anagraficaAgentiRemoteModel = anagraficaAgentiRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _anagraficaAgentiRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_anagraficaAgentiRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			AnagraficaAgentiLocalServiceUtil.addAnagraficaAgenti(this);
		}
		else {
			AnagraficaAgentiLocalServiceUtil.updateAnagraficaAgenti(this);
		}
	}

	@Override
	public AnagraficaAgenti toEscapedModel() {
		return (AnagraficaAgenti)ProxyUtil.newProxyInstance(AnagraficaAgenti.class.getClassLoader(),
			new Class[] { AnagraficaAgenti.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		AnagraficaAgentiClp clone = new AnagraficaAgentiClp();

		clone.setCodiceAgente(getCodiceAgente());
		clone.setNome(getNome());
		clone.setCodiceZona(getCodiceZona());
		clone.setCodiceProvvRigo(getCodiceProvvRigo());
		clone.setCodiceProvvChiusura(getCodiceProvvChiusura());
		clone.setBudget(getBudget());
		clone.setTipoLiquidazione(getTipoLiquidazione());
		clone.setGiorniToll(getGiorniToll());
		clone.setTipoDivisa(getTipoDivisa());

		return clone;
	}

	@Override
	public int compareTo(AnagraficaAgenti anagraficaAgenti) {
		String primaryKey = anagraficaAgenti.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AnagraficaAgentiClp)) {
			return false;
		}

		AnagraficaAgentiClp anagraficaAgenti = (AnagraficaAgentiClp)obj;

		String primaryKey = anagraficaAgenti.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{codiceAgente=");
		sb.append(getCodiceAgente());
		sb.append(", nome=");
		sb.append(getNome());
		sb.append(", codiceZona=");
		sb.append(getCodiceZona());
		sb.append(", codiceProvvRigo=");
		sb.append(getCodiceProvvRigo());
		sb.append(", codiceProvvChiusura=");
		sb.append(getCodiceProvvChiusura());
		sb.append(", budget=");
		sb.append(getBudget());
		sb.append(", tipoLiquidazione=");
		sb.append(getTipoLiquidazione());
		sb.append(", giorniToll=");
		sb.append(getGiorniToll());
		sb.append(", tipoDivisa=");
		sb.append(getTipoDivisa());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(31);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.AnagraficaAgenti");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>codiceAgente</column-name><column-value><![CDATA[");
		sb.append(getCodiceAgente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nome</column-name><column-value><![CDATA[");
		sb.append(getNome());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceZona</column-name><column-value><![CDATA[");
		sb.append(getCodiceZona());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceProvvRigo</column-name><column-value><![CDATA[");
		sb.append(getCodiceProvvRigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceProvvChiusura</column-name><column-value><![CDATA[");
		sb.append(getCodiceProvvChiusura());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>budget</column-name><column-value><![CDATA[");
		sb.append(getBudget());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoLiquidazione</column-name><column-value><![CDATA[");
		sb.append(getTipoLiquidazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>giorniToll</column-name><column-value><![CDATA[");
		sb.append(getGiorniToll());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoDivisa</column-name><column-value><![CDATA[");
		sb.append(getTipoDivisa());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _codiceAgente;
	private String _nome;
	private String _codiceZona;
	private String _codiceProvvRigo;
	private String _codiceProvvChiusura;
	private double _budget;
	private int _tipoLiquidazione;
	private int _giorniToll;
	private boolean _tipoDivisa;
	private BaseModel<?> _anagraficaAgentiRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}