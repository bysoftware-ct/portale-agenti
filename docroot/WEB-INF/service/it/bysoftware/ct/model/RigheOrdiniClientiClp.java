/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.RigheOrdiniClientiLocalServiceUtil;
import it.bysoftware.ct.service.persistence.RigheOrdiniClientiPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class RigheOrdiniClientiClp extends BaseModelImpl<RigheOrdiniClienti>
	implements RigheOrdiniClienti {
	public RigheOrdiniClientiClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return RigheOrdiniClienti.class;
	}

	@Override
	public String getModelClassName() {
		return RigheOrdiniClienti.class.getName();
	}

	@Override
	public RigheOrdiniClientiPK getPrimaryKey() {
		return new RigheOrdiniClientiPK(_anno, _codiceAttivita, _codiceCentro,
			_codiceDeposito, _tipoOrdine, _numeroOrdine, _numeroRigo);
	}

	@Override
	public void setPrimaryKey(RigheOrdiniClientiPK primaryKey) {
		setAnno(primaryKey.anno);
		setCodiceAttivita(primaryKey.codiceAttivita);
		setCodiceCentro(primaryKey.codiceCentro);
		setCodiceDeposito(primaryKey.codiceDeposito);
		setTipoOrdine(primaryKey.tipoOrdine);
		setNumeroOrdine(primaryKey.numeroOrdine);
		setNumeroRigo(primaryKey.numeroRigo);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new RigheOrdiniClientiPK(_anno, _codiceAttivita, _codiceCentro,
			_codiceDeposito, _tipoOrdine, _numeroOrdine, _numeroRigo);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((RigheOrdiniClientiPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("anno", getAnno());
		attributes.put("codiceAttivita", getCodiceAttivita());
		attributes.put("codiceCentro", getCodiceCentro());
		attributes.put("codiceDeposito", getCodiceDeposito());
		attributes.put("tipoOrdine", getTipoOrdine());
		attributes.put("numeroOrdine", getNumeroOrdine());
		attributes.put("numeroRigo", getNumeroRigo());
		attributes.put("statoRigo", getStatoRigo());
		attributes.put("tipoRigo", getTipoRigo());
		attributes.put("codiceCausaleMagazzino", getCodiceCausaleMagazzino());
		attributes.put("codiceDepositoMov", getCodiceDepositoMov());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("descrizione", getDescrizione());
		attributes.put("codiceUnitMis", getCodiceUnitMis());
		attributes.put("decimaliQuant", getDecimaliQuant());
		attributes.put("quantita1", getQuantita1());
		attributes.put("quantita2", getQuantita2());
		attributes.put("quantita3", getQuantita3());
		attributes.put("quantita", getQuantita());
		attributes.put("codiceUnitMis2", getCodiceUnitMis2());
		attributes.put("quantitaUnitMis2", getQuantitaUnitMis2());
		attributes.put("decimaliPrezzo", getDecimaliPrezzo());
		attributes.put("prezzo", getPrezzo());
		attributes.put("importoLordo", getImportoLordo());
		attributes.put("sconto1", getSconto1());
		attributes.put("sconto2", getSconto2());
		attributes.put("sconto3", getSconto3());
		attributes.put("importoNetto", getImportoNetto());
		attributes.put("importo", getImporto());
		attributes.put("codiceIVAFatturazione", getCodiceIVAFatturazione());
		attributes.put("tipoProvviggione", getTipoProvviggione());
		attributes.put("percentualeProvvAgente", getPercentualeProvvAgente());
		attributes.put("importoProvvAgente", getImportoProvvAgente());
		attributes.put("codiceSottoconto", getCodiceSottoconto());
		attributes.put("nomenclaturaCombinata", getNomenclaturaCombinata());
		attributes.put("stampaDistBase", getStampaDistBase());
		attributes.put("codiceCliente", getCodiceCliente());
		attributes.put("riferimentoOrdineCliente", getRiferimentoOrdineCliente());
		attributes.put("dataOridine", getDataOridine());
		attributes.put("statoEvasione", getStatoEvasione());
		attributes.put("dataPrevistaConsegna", getDataPrevistaConsegna());
		attributes.put("dataRegistrazioneOrdine", getDataRegistrazioneOrdine());
		attributes.put("numeroPrimaNota", getNumeroPrimaNota());
		attributes.put("numeroProgressivo", getNumeroProgressivo());
		attributes.put("tipoCompEspl", getTipoCompEspl());
		attributes.put("maxLivCompEspl", getMaxLivCompEspl());
		attributes.put("generazioneOrdFornit", getGenerazioneOrdFornit());
		attributes.put("esercizioProOrd", getEsercizioProOrd());
		attributes.put("numPropOrdine", getNumPropOrdine());
		attributes.put("numRigoPropOrdine", getNumRigoPropOrdine());
		attributes.put("libStr1", getLibStr1());
		attributes.put("libStr2", getLibStr2());
		attributes.put("libStr3", getLibStr3());
		attributes.put("libDbl1", getLibDbl1());
		attributes.put("libDbl2", getLibDbl2());
		attributes.put("libDbl3", getLibDbl3());
		attributes.put("libDat1", getLibDat1());
		attributes.put("libDat2", getLibDat2());
		attributes.put("libDat3", getLibDat3());
		attributes.put("libLng1", getLibLng1());
		attributes.put("libLng2", getLibLng2());
		attributes.put("libLng3", getLibLng3());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer anno = (Integer)attributes.get("anno");

		if (anno != null) {
			setAnno(anno);
		}

		String codiceAttivita = (String)attributes.get("codiceAttivita");

		if (codiceAttivita != null) {
			setCodiceAttivita(codiceAttivita);
		}

		String codiceCentro = (String)attributes.get("codiceCentro");

		if (codiceCentro != null) {
			setCodiceCentro(codiceCentro);
		}

		String codiceDeposito = (String)attributes.get("codiceDeposito");

		if (codiceDeposito != null) {
			setCodiceDeposito(codiceDeposito);
		}

		Integer tipoOrdine = (Integer)attributes.get("tipoOrdine");

		if (tipoOrdine != null) {
			setTipoOrdine(tipoOrdine);
		}

		Integer numeroOrdine = (Integer)attributes.get("numeroOrdine");

		if (numeroOrdine != null) {
			setNumeroOrdine(numeroOrdine);
		}

		Integer numeroRigo = (Integer)attributes.get("numeroRigo");

		if (numeroRigo != null) {
			setNumeroRigo(numeroRigo);
		}

		Boolean statoRigo = (Boolean)attributes.get("statoRigo");

		if (statoRigo != null) {
			setStatoRigo(statoRigo);
		}

		Integer tipoRigo = (Integer)attributes.get("tipoRigo");

		if (tipoRigo != null) {
			setTipoRigo(tipoRigo);
		}

		String codiceCausaleMagazzino = (String)attributes.get(
				"codiceCausaleMagazzino");

		if (codiceCausaleMagazzino != null) {
			setCodiceCausaleMagazzino(codiceCausaleMagazzino);
		}

		String codiceDepositoMov = (String)attributes.get("codiceDepositoMov");

		if (codiceDepositoMov != null) {
			setCodiceDepositoMov(codiceDepositoMov);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		String codiceUnitMis = (String)attributes.get("codiceUnitMis");

		if (codiceUnitMis != null) {
			setCodiceUnitMis(codiceUnitMis);
		}

		Integer decimaliQuant = (Integer)attributes.get("decimaliQuant");

		if (decimaliQuant != null) {
			setDecimaliQuant(decimaliQuant);
		}

		Double quantita1 = (Double)attributes.get("quantita1");

		if (quantita1 != null) {
			setQuantita1(quantita1);
		}

		Double quantita2 = (Double)attributes.get("quantita2");

		if (quantita2 != null) {
			setQuantita2(quantita2);
		}

		Double quantita3 = (Double)attributes.get("quantita3");

		if (quantita3 != null) {
			setQuantita3(quantita3);
		}

		Double quantita = (Double)attributes.get("quantita");

		if (quantita != null) {
			setQuantita(quantita);
		}

		String codiceUnitMis2 = (String)attributes.get("codiceUnitMis2");

		if (codiceUnitMis2 != null) {
			setCodiceUnitMis2(codiceUnitMis2);
		}

		Double quantitaUnitMis2 = (Double)attributes.get("quantitaUnitMis2");

		if (quantitaUnitMis2 != null) {
			setQuantitaUnitMis2(quantitaUnitMis2);
		}

		Integer decimaliPrezzo = (Integer)attributes.get("decimaliPrezzo");

		if (decimaliPrezzo != null) {
			setDecimaliPrezzo(decimaliPrezzo);
		}

		Double prezzo = (Double)attributes.get("prezzo");

		if (prezzo != null) {
			setPrezzo(prezzo);
		}

		Double importoLordo = (Double)attributes.get("importoLordo");

		if (importoLordo != null) {
			setImportoLordo(importoLordo);
		}

		Double sconto1 = (Double)attributes.get("sconto1");

		if (sconto1 != null) {
			setSconto1(sconto1);
		}

		Double sconto2 = (Double)attributes.get("sconto2");

		if (sconto2 != null) {
			setSconto2(sconto2);
		}

		Double sconto3 = (Double)attributes.get("sconto3");

		if (sconto3 != null) {
			setSconto3(sconto3);
		}

		Double importoNetto = (Double)attributes.get("importoNetto");

		if (importoNetto != null) {
			setImportoNetto(importoNetto);
		}

		Double importo = (Double)attributes.get("importo");

		if (importo != null) {
			setImporto(importo);
		}

		String codiceIVAFatturazione = (String)attributes.get(
				"codiceIVAFatturazione");

		if (codiceIVAFatturazione != null) {
			setCodiceIVAFatturazione(codiceIVAFatturazione);
		}

		Boolean tipoProvviggione = (Boolean)attributes.get("tipoProvviggione");

		if (tipoProvviggione != null) {
			setTipoProvviggione(tipoProvviggione);
		}

		Double percentualeProvvAgente = (Double)attributes.get(
				"percentualeProvvAgente");

		if (percentualeProvvAgente != null) {
			setPercentualeProvvAgente(percentualeProvvAgente);
		}

		Double importoProvvAgente = (Double)attributes.get("importoProvvAgente");

		if (importoProvvAgente != null) {
			setImportoProvvAgente(importoProvvAgente);
		}

		String codiceSottoconto = (String)attributes.get("codiceSottoconto");

		if (codiceSottoconto != null) {
			setCodiceSottoconto(codiceSottoconto);
		}

		Integer nomenclaturaCombinata = (Integer)attributes.get(
				"nomenclaturaCombinata");

		if (nomenclaturaCombinata != null) {
			setNomenclaturaCombinata(nomenclaturaCombinata);
		}

		Boolean stampaDistBase = (Boolean)attributes.get("stampaDistBase");

		if (stampaDistBase != null) {
			setStampaDistBase(stampaDistBase);
		}

		String codiceCliente = (String)attributes.get("codiceCliente");

		if (codiceCliente != null) {
			setCodiceCliente(codiceCliente);
		}

		String riferimentoOrdineCliente = (String)attributes.get(
				"riferimentoOrdineCliente");

		if (riferimentoOrdineCliente != null) {
			setRiferimentoOrdineCliente(riferimentoOrdineCliente);
		}

		Date dataOridine = (Date)attributes.get("dataOridine");

		if (dataOridine != null) {
			setDataOridine(dataOridine);
		}

		Boolean statoEvasione = (Boolean)attributes.get("statoEvasione");

		if (statoEvasione != null) {
			setStatoEvasione(statoEvasione);
		}

		Date dataPrevistaConsegna = (Date)attributes.get("dataPrevistaConsegna");

		if (dataPrevistaConsegna != null) {
			setDataPrevistaConsegna(dataPrevistaConsegna);
		}

		Date dataRegistrazioneOrdine = (Date)attributes.get(
				"dataRegistrazioneOrdine");

		if (dataRegistrazioneOrdine != null) {
			setDataRegistrazioneOrdine(dataRegistrazioneOrdine);
		}

		Integer numeroPrimaNota = (Integer)attributes.get("numeroPrimaNota");

		if (numeroPrimaNota != null) {
			setNumeroPrimaNota(numeroPrimaNota);
		}

		Integer numeroProgressivo = (Integer)attributes.get("numeroProgressivo");

		if (numeroProgressivo != null) {
			setNumeroProgressivo(numeroProgressivo);
		}

		Integer tipoCompEspl = (Integer)attributes.get("tipoCompEspl");

		if (tipoCompEspl != null) {
			setTipoCompEspl(tipoCompEspl);
		}

		Integer maxLivCompEspl = (Integer)attributes.get("maxLivCompEspl");

		if (maxLivCompEspl != null) {
			setMaxLivCompEspl(maxLivCompEspl);
		}

		Integer generazioneOrdFornit = (Integer)attributes.get(
				"generazioneOrdFornit");

		if (generazioneOrdFornit != null) {
			setGenerazioneOrdFornit(generazioneOrdFornit);
		}

		Integer esercizioProOrd = (Integer)attributes.get("esercizioProOrd");

		if (esercizioProOrd != null) {
			setEsercizioProOrd(esercizioProOrd);
		}

		Integer numPropOrdine = (Integer)attributes.get("numPropOrdine");

		if (numPropOrdine != null) {
			setNumPropOrdine(numPropOrdine);
		}

		Integer numRigoPropOrdine = (Integer)attributes.get("numRigoPropOrdine");

		if (numRigoPropOrdine != null) {
			setNumRigoPropOrdine(numRigoPropOrdine);
		}

		String libStr1 = (String)attributes.get("libStr1");

		if (libStr1 != null) {
			setLibStr1(libStr1);
		}

		String libStr2 = (String)attributes.get("libStr2");

		if (libStr2 != null) {
			setLibStr2(libStr2);
		}

		String libStr3 = (String)attributes.get("libStr3");

		if (libStr3 != null) {
			setLibStr3(libStr3);
		}

		Double libDbl1 = (Double)attributes.get("libDbl1");

		if (libDbl1 != null) {
			setLibDbl1(libDbl1);
		}

		Double libDbl2 = (Double)attributes.get("libDbl2");

		if (libDbl2 != null) {
			setLibDbl2(libDbl2);
		}

		Double libDbl3 = (Double)attributes.get("libDbl3");

		if (libDbl3 != null) {
			setLibDbl3(libDbl3);
		}

		Date libDat1 = (Date)attributes.get("libDat1");

		if (libDat1 != null) {
			setLibDat1(libDat1);
		}

		Date libDat2 = (Date)attributes.get("libDat2");

		if (libDat2 != null) {
			setLibDat2(libDat2);
		}

		Date libDat3 = (Date)attributes.get("libDat3");

		if (libDat3 != null) {
			setLibDat3(libDat3);
		}

		Long libLng1 = (Long)attributes.get("libLng1");

		if (libLng1 != null) {
			setLibLng1(libLng1);
		}

		Long libLng2 = (Long)attributes.get("libLng2");

		if (libLng2 != null) {
			setLibLng2(libLng2);
		}

		Long libLng3 = (Long)attributes.get("libLng3");

		if (libLng3 != null) {
			setLibLng3(libLng3);
		}
	}

	@Override
	public int getAnno() {
		return _anno;
	}

	@Override
	public void setAnno(int anno) {
		_anno = anno;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setAnno", int.class);

				method.invoke(_righeOrdiniClientiRemoteModel, anno);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceAttivita() {
		return _codiceAttivita;
	}

	@Override
	public void setCodiceAttivita(String codiceAttivita) {
		_codiceAttivita = codiceAttivita;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceAttivita",
						String.class);

				method.invoke(_righeOrdiniClientiRemoteModel, codiceAttivita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCentro() {
		return _codiceCentro;
	}

	@Override
	public void setCodiceCentro(String codiceCentro) {
		_codiceCentro = codiceCentro;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCentro", String.class);

				method.invoke(_righeOrdiniClientiRemoteModel, codiceCentro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDeposito() {
		return _codiceDeposito;
	}

	@Override
	public void setCodiceDeposito(String codiceDeposito) {
		_codiceDeposito = codiceDeposito;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDeposito",
						String.class);

				method.invoke(_righeOrdiniClientiRemoteModel, codiceDeposito);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoOrdine() {
		return _tipoOrdine;
	}

	@Override
	public void setTipoOrdine(int tipoOrdine) {
		_tipoOrdine = tipoOrdine;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoOrdine", int.class);

				method.invoke(_righeOrdiniClientiRemoteModel, tipoOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroOrdine() {
		return _numeroOrdine;
	}

	@Override
	public void setNumeroOrdine(int numeroOrdine) {
		_numeroOrdine = numeroOrdine;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroOrdine", int.class);

				method.invoke(_righeOrdiniClientiRemoteModel, numeroOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroRigo() {
		return _numeroRigo;
	}

	@Override
	public void setNumeroRigo(int numeroRigo) {
		_numeroRigo = numeroRigo;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroRigo", int.class);

				method.invoke(_righeOrdiniClientiRemoteModel, numeroRigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getStatoRigo() {
		return _statoRigo;
	}

	@Override
	public boolean isStatoRigo() {
		return _statoRigo;
	}

	@Override
	public void setStatoRigo(boolean statoRigo) {
		_statoRigo = statoRigo;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setStatoRigo", boolean.class);

				method.invoke(_righeOrdiniClientiRemoteModel, statoRigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoRigo() {
		return _tipoRigo;
	}

	@Override
	public void setTipoRigo(int tipoRigo) {
		_tipoRigo = tipoRigo;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoRigo", int.class);

				method.invoke(_righeOrdiniClientiRemoteModel, tipoRigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCausaleMagazzino() {
		return _codiceCausaleMagazzino;
	}

	@Override
	public void setCodiceCausaleMagazzino(String codiceCausaleMagazzino) {
		_codiceCausaleMagazzino = codiceCausaleMagazzino;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCausaleMagazzino",
						String.class);

				method.invoke(_righeOrdiniClientiRemoteModel,
					codiceCausaleMagazzino);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDepositoMov() {
		return _codiceDepositoMov;
	}

	@Override
	public void setCodiceDepositoMov(String codiceDepositoMov) {
		_codiceDepositoMov = codiceDepositoMov;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDepositoMov",
						String.class);

				method.invoke(_righeOrdiniClientiRemoteModel, codiceDepositoMov);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	@Override
	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceArticolo",
						String.class);

				method.invoke(_righeOrdiniClientiRemoteModel, codiceArticolo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceVariante() {
		return _codiceVariante;
	}

	@Override
	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceVariante",
						String.class);

				method.invoke(_righeOrdiniClientiRemoteModel, codiceVariante);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizione() {
		return _descrizione;
	}

	@Override
	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizione", String.class);

				method.invoke(_righeOrdiniClientiRemoteModel, descrizione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceUnitMis() {
		return _codiceUnitMis;
	}

	@Override
	public void setCodiceUnitMis(String codiceUnitMis) {
		_codiceUnitMis = codiceUnitMis;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceUnitMis", String.class);

				method.invoke(_righeOrdiniClientiRemoteModel, codiceUnitMis);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getDecimaliQuant() {
		return _decimaliQuant;
	}

	@Override
	public void setDecimaliQuant(int decimaliQuant) {
		_decimaliQuant = decimaliQuant;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDecimaliQuant", int.class);

				method.invoke(_righeOrdiniClientiRemoteModel, decimaliQuant);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantita1() {
		return _quantita1;
	}

	@Override
	public void setQuantita1(double quantita1) {
		_quantita1 = quantita1;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantita1", double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, quantita1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantita2() {
		return _quantita2;
	}

	@Override
	public void setQuantita2(double quantita2) {
		_quantita2 = quantita2;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantita2", double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, quantita2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantita3() {
		return _quantita3;
	}

	@Override
	public void setQuantita3(double quantita3) {
		_quantita3 = quantita3;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantita3", double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, quantita3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantita() {
		return _quantita;
	}

	@Override
	public void setQuantita(double quantita) {
		_quantita = quantita;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantita", double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, quantita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceUnitMis2() {
		return _codiceUnitMis2;
	}

	@Override
	public void setCodiceUnitMis2(String codiceUnitMis2) {
		_codiceUnitMis2 = codiceUnitMis2;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceUnitMis2",
						String.class);

				method.invoke(_righeOrdiniClientiRemoteModel, codiceUnitMis2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getQuantitaUnitMis2() {
		return _quantitaUnitMis2;
	}

	@Override
	public void setQuantitaUnitMis2(double quantitaUnitMis2) {
		_quantitaUnitMis2 = quantitaUnitMis2;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantitaUnitMis2",
						double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, quantitaUnitMis2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getDecimaliPrezzo() {
		return _decimaliPrezzo;
	}

	@Override
	public void setDecimaliPrezzo(int decimaliPrezzo) {
		_decimaliPrezzo = decimaliPrezzo;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDecimaliPrezzo", int.class);

				method.invoke(_righeOrdiniClientiRemoteModel, decimaliPrezzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPrezzo() {
		return _prezzo;
	}

	@Override
	public void setPrezzo(double prezzo) {
		_prezzo = prezzo;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setPrezzo", double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, prezzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoLordo() {
		return _importoLordo;
	}

	@Override
	public void setImportoLordo(double importoLordo) {
		_importoLordo = importoLordo;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoLordo", double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, importoLordo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSconto1() {
		return _sconto1;
	}

	@Override
	public void setSconto1(double sconto1) {
		_sconto1 = sconto1;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setSconto1", double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, sconto1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSconto2() {
		return _sconto2;
	}

	@Override
	public void setSconto2(double sconto2) {
		_sconto2 = sconto2;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setSconto2", double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, sconto2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSconto3() {
		return _sconto3;
	}

	@Override
	public void setSconto3(double sconto3) {
		_sconto3 = sconto3;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setSconto3", double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, sconto3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoNetto() {
		return _importoNetto;
	}

	@Override
	public void setImportoNetto(double importoNetto) {
		_importoNetto = importoNetto;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoNetto", double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, importoNetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImporto() {
		return _importo;
	}

	@Override
	public void setImporto(double importo) {
		_importo = importo;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setImporto", double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, importo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVAFatturazione() {
		return _codiceIVAFatturazione;
	}

	@Override
	public void setCodiceIVAFatturazione(String codiceIVAFatturazione) {
		_codiceIVAFatturazione = codiceIVAFatturazione;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVAFatturazione",
						String.class);

				method.invoke(_righeOrdiniClientiRemoteModel,
					codiceIVAFatturazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getTipoProvviggione() {
		return _tipoProvviggione;
	}

	@Override
	public boolean isTipoProvviggione() {
		return _tipoProvviggione;
	}

	@Override
	public void setTipoProvviggione(boolean tipoProvviggione) {
		_tipoProvviggione = tipoProvviggione;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoProvviggione",
						boolean.class);

				method.invoke(_righeOrdiniClientiRemoteModel, tipoProvviggione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPercentualeProvvAgente() {
		return _percentualeProvvAgente;
	}

	@Override
	public void setPercentualeProvvAgente(double percentualeProvvAgente) {
		_percentualeProvvAgente = percentualeProvvAgente;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setPercentualeProvvAgente",
						double.class);

				method.invoke(_righeOrdiniClientiRemoteModel,
					percentualeProvvAgente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoProvvAgente() {
		return _importoProvvAgente;
	}

	@Override
	public void setImportoProvvAgente(double importoProvvAgente) {
		_importoProvvAgente = importoProvvAgente;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoProvvAgente",
						double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, importoProvvAgente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceSottoconto() {
		return _codiceSottoconto;
	}

	@Override
	public void setCodiceSottoconto(String codiceSottoconto) {
		_codiceSottoconto = codiceSottoconto;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceSottoconto",
						String.class);

				method.invoke(_righeOrdiniClientiRemoteModel, codiceSottoconto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNomenclaturaCombinata() {
		return _nomenclaturaCombinata;
	}

	@Override
	public void setNomenclaturaCombinata(int nomenclaturaCombinata) {
		_nomenclaturaCombinata = nomenclaturaCombinata;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNomenclaturaCombinata",
						int.class);

				method.invoke(_righeOrdiniClientiRemoteModel,
					nomenclaturaCombinata);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getStampaDistBase() {
		return _stampaDistBase;
	}

	@Override
	public boolean isStampaDistBase() {
		return _stampaDistBase;
	}

	@Override
	public void setStampaDistBase(boolean stampaDistBase) {
		_stampaDistBase = stampaDistBase;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setStampaDistBase",
						boolean.class);

				method.invoke(_righeOrdiniClientiRemoteModel, stampaDistBase);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCliente() {
		return _codiceCliente;
	}

	@Override
	public void setCodiceCliente(String codiceCliente) {
		_codiceCliente = codiceCliente;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCliente", String.class);

				method.invoke(_righeOrdiniClientiRemoteModel, codiceCliente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRiferimentoOrdineCliente() {
		return _riferimentoOrdineCliente;
	}

	@Override
	public void setRiferimentoOrdineCliente(String riferimentoOrdineCliente) {
		_riferimentoOrdineCliente = riferimentoOrdineCliente;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setRiferimentoOrdineCliente",
						String.class);

				method.invoke(_righeOrdiniClientiRemoteModel,
					riferimentoOrdineCliente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataOridine() {
		return _dataOridine;
	}

	@Override
	public void setDataOridine(Date dataOridine) {
		_dataOridine = dataOridine;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataOridine", Date.class);

				method.invoke(_righeOrdiniClientiRemoteModel, dataOridine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getStatoEvasione() {
		return _statoEvasione;
	}

	@Override
	public boolean isStatoEvasione() {
		return _statoEvasione;
	}

	@Override
	public void setStatoEvasione(boolean statoEvasione) {
		_statoEvasione = statoEvasione;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setStatoEvasione",
						boolean.class);

				method.invoke(_righeOrdiniClientiRemoteModel, statoEvasione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataPrevistaConsegna() {
		return _dataPrevistaConsegna;
	}

	@Override
	public void setDataPrevistaConsegna(Date dataPrevistaConsegna) {
		_dataPrevistaConsegna = dataPrevistaConsegna;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataPrevistaConsegna",
						Date.class);

				method.invoke(_righeOrdiniClientiRemoteModel,
					dataPrevistaConsegna);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataRegistrazioneOrdine() {
		return _dataRegistrazioneOrdine;
	}

	@Override
	public void setDataRegistrazioneOrdine(Date dataRegistrazioneOrdine) {
		_dataRegistrazioneOrdine = dataRegistrazioneOrdine;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataRegistrazioneOrdine",
						Date.class);

				method.invoke(_righeOrdiniClientiRemoteModel,
					dataRegistrazioneOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroPrimaNota() {
		return _numeroPrimaNota;
	}

	@Override
	public void setNumeroPrimaNota(int numeroPrimaNota) {
		_numeroPrimaNota = numeroPrimaNota;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroPrimaNota", int.class);

				method.invoke(_righeOrdiniClientiRemoteModel, numeroPrimaNota);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroProgressivo() {
		return _numeroProgressivo;
	}

	@Override
	public void setNumeroProgressivo(int numeroProgressivo) {
		_numeroProgressivo = numeroProgressivo;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroProgressivo",
						int.class);

				method.invoke(_righeOrdiniClientiRemoteModel, numeroProgressivo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoCompEspl() {
		return _tipoCompEspl;
	}

	@Override
	public void setTipoCompEspl(int tipoCompEspl) {
		_tipoCompEspl = tipoCompEspl;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoCompEspl", int.class);

				method.invoke(_righeOrdiniClientiRemoteModel, tipoCompEspl);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getMaxLivCompEspl() {
		return _maxLivCompEspl;
	}

	@Override
	public void setMaxLivCompEspl(int maxLivCompEspl) {
		_maxLivCompEspl = maxLivCompEspl;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setMaxLivCompEspl", int.class);

				method.invoke(_righeOrdiniClientiRemoteModel, maxLivCompEspl);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getGenerazioneOrdFornit() {
		return _generazioneOrdFornit;
	}

	@Override
	public void setGenerazioneOrdFornit(int generazioneOrdFornit) {
		_generazioneOrdFornit = generazioneOrdFornit;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setGenerazioneOrdFornit",
						int.class);

				method.invoke(_righeOrdiniClientiRemoteModel,
					generazioneOrdFornit);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getEsercizioProOrd() {
		return _esercizioProOrd;
	}

	@Override
	public void setEsercizioProOrd(int esercizioProOrd) {
		_esercizioProOrd = esercizioProOrd;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setEsercizioProOrd", int.class);

				method.invoke(_righeOrdiniClientiRemoteModel, esercizioProOrd);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumPropOrdine() {
		return _numPropOrdine;
	}

	@Override
	public void setNumPropOrdine(int numPropOrdine) {
		_numPropOrdine = numPropOrdine;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumPropOrdine", int.class);

				method.invoke(_righeOrdiniClientiRemoteModel, numPropOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumRigoPropOrdine() {
		return _numRigoPropOrdine;
	}

	@Override
	public void setNumRigoPropOrdine(int numRigoPropOrdine) {
		_numRigoPropOrdine = numRigoPropOrdine;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumRigoPropOrdine",
						int.class);

				method.invoke(_righeOrdiniClientiRemoteModel, numRigoPropOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr1() {
		return _libStr1;
	}

	@Override
	public void setLibStr1(String libStr1) {
		_libStr1 = libStr1;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr1", String.class);

				method.invoke(_righeOrdiniClientiRemoteModel, libStr1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr2() {
		return _libStr2;
	}

	@Override
	public void setLibStr2(String libStr2) {
		_libStr2 = libStr2;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr2", String.class);

				method.invoke(_righeOrdiniClientiRemoteModel, libStr2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr3() {
		return _libStr3;
	}

	@Override
	public void setLibStr3(String libStr3) {
		_libStr3 = libStr3;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr3", String.class);

				method.invoke(_righeOrdiniClientiRemoteModel, libStr3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl1() {
		return _libDbl1;
	}

	@Override
	public void setLibDbl1(double libDbl1) {
		_libDbl1 = libDbl1;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl1", double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, libDbl1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl2() {
		return _libDbl2;
	}

	@Override
	public void setLibDbl2(double libDbl2) {
		_libDbl2 = libDbl2;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl2", double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, libDbl2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl3() {
		return _libDbl3;
	}

	@Override
	public void setLibDbl3(double libDbl3) {
		_libDbl3 = libDbl3;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl3", double.class);

				method.invoke(_righeOrdiniClientiRemoteModel, libDbl3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat1() {
		return _libDat1;
	}

	@Override
	public void setLibDat1(Date libDat1) {
		_libDat1 = libDat1;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat1", Date.class);

				method.invoke(_righeOrdiniClientiRemoteModel, libDat1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat2() {
		return _libDat2;
	}

	@Override
	public void setLibDat2(Date libDat2) {
		_libDat2 = libDat2;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat2", Date.class);

				method.invoke(_righeOrdiniClientiRemoteModel, libDat2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat3() {
		return _libDat3;
	}

	@Override
	public void setLibDat3(Date libDat3) {
		_libDat3 = libDat3;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat3", Date.class);

				method.invoke(_righeOrdiniClientiRemoteModel, libDat3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng1() {
		return _libLng1;
	}

	@Override
	public void setLibLng1(long libLng1) {
		_libLng1 = libLng1;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng1", long.class);

				method.invoke(_righeOrdiniClientiRemoteModel, libLng1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng2() {
		return _libLng2;
	}

	@Override
	public void setLibLng2(long libLng2) {
		_libLng2 = libLng2;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng2", long.class);

				method.invoke(_righeOrdiniClientiRemoteModel, libLng2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng3() {
		return _libLng3;
	}

	@Override
	public void setLibLng3(long libLng3) {
		_libLng3 = libLng3;

		if (_righeOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _righeOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng3", long.class);

				method.invoke(_righeOrdiniClientiRemoteModel, libLng3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getRigheOrdiniClientiRemoteModel() {
		return _righeOrdiniClientiRemoteModel;
	}

	public void setRigheOrdiniClientiRemoteModel(
		BaseModel<?> righeOrdiniClientiRemoteModel) {
		_righeOrdiniClientiRemoteModel = righeOrdiniClientiRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _righeOrdiniClientiRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_righeOrdiniClientiRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			RigheOrdiniClientiLocalServiceUtil.addRigheOrdiniClienti(this);
		}
		else {
			RigheOrdiniClientiLocalServiceUtil.updateRigheOrdiniClienti(this);
		}
	}

	@Override
	public RigheOrdiniClienti toEscapedModel() {
		return (RigheOrdiniClienti)ProxyUtil.newProxyInstance(RigheOrdiniClienti.class.getClassLoader(),
			new Class[] { RigheOrdiniClienti.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		RigheOrdiniClientiClp clone = new RigheOrdiniClientiClp();

		clone.setAnno(getAnno());
		clone.setCodiceAttivita(getCodiceAttivita());
		clone.setCodiceCentro(getCodiceCentro());
		clone.setCodiceDeposito(getCodiceDeposito());
		clone.setTipoOrdine(getTipoOrdine());
		clone.setNumeroOrdine(getNumeroOrdine());
		clone.setNumeroRigo(getNumeroRigo());
		clone.setStatoRigo(getStatoRigo());
		clone.setTipoRigo(getTipoRigo());
		clone.setCodiceCausaleMagazzino(getCodiceCausaleMagazzino());
		clone.setCodiceDepositoMov(getCodiceDepositoMov());
		clone.setCodiceArticolo(getCodiceArticolo());
		clone.setCodiceVariante(getCodiceVariante());
		clone.setDescrizione(getDescrizione());
		clone.setCodiceUnitMis(getCodiceUnitMis());
		clone.setDecimaliQuant(getDecimaliQuant());
		clone.setQuantita1(getQuantita1());
		clone.setQuantita2(getQuantita2());
		clone.setQuantita3(getQuantita3());
		clone.setQuantita(getQuantita());
		clone.setCodiceUnitMis2(getCodiceUnitMis2());
		clone.setQuantitaUnitMis2(getQuantitaUnitMis2());
		clone.setDecimaliPrezzo(getDecimaliPrezzo());
		clone.setPrezzo(getPrezzo());
		clone.setImportoLordo(getImportoLordo());
		clone.setSconto1(getSconto1());
		clone.setSconto2(getSconto2());
		clone.setSconto3(getSconto3());
		clone.setImportoNetto(getImportoNetto());
		clone.setImporto(getImporto());
		clone.setCodiceIVAFatturazione(getCodiceIVAFatturazione());
		clone.setTipoProvviggione(getTipoProvviggione());
		clone.setPercentualeProvvAgente(getPercentualeProvvAgente());
		clone.setImportoProvvAgente(getImportoProvvAgente());
		clone.setCodiceSottoconto(getCodiceSottoconto());
		clone.setNomenclaturaCombinata(getNomenclaturaCombinata());
		clone.setStampaDistBase(getStampaDistBase());
		clone.setCodiceCliente(getCodiceCliente());
		clone.setRiferimentoOrdineCliente(getRiferimentoOrdineCliente());
		clone.setDataOridine(getDataOridine());
		clone.setStatoEvasione(getStatoEvasione());
		clone.setDataPrevistaConsegna(getDataPrevistaConsegna());
		clone.setDataRegistrazioneOrdine(getDataRegistrazioneOrdine());
		clone.setNumeroPrimaNota(getNumeroPrimaNota());
		clone.setNumeroProgressivo(getNumeroProgressivo());
		clone.setTipoCompEspl(getTipoCompEspl());
		clone.setMaxLivCompEspl(getMaxLivCompEspl());
		clone.setGenerazioneOrdFornit(getGenerazioneOrdFornit());
		clone.setEsercizioProOrd(getEsercizioProOrd());
		clone.setNumPropOrdine(getNumPropOrdine());
		clone.setNumRigoPropOrdine(getNumRigoPropOrdine());
		clone.setLibStr1(getLibStr1());
		clone.setLibStr2(getLibStr2());
		clone.setLibStr3(getLibStr3());
		clone.setLibDbl1(getLibDbl1());
		clone.setLibDbl2(getLibDbl2());
		clone.setLibDbl3(getLibDbl3());
		clone.setLibDat1(getLibDat1());
		clone.setLibDat2(getLibDat2());
		clone.setLibDat3(getLibDat3());
		clone.setLibLng1(getLibLng1());
		clone.setLibLng2(getLibLng2());
		clone.setLibLng3(getLibLng3());

		return clone;
	}

	@Override
	public int compareTo(RigheOrdiniClienti righeOrdiniClienti) {
		RigheOrdiniClientiPK primaryKey = righeOrdiniClienti.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof RigheOrdiniClientiClp)) {
			return false;
		}

		RigheOrdiniClientiClp righeOrdiniClienti = (RigheOrdiniClientiClp)obj;

		RigheOrdiniClientiPK primaryKey = righeOrdiniClienti.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(127);

		sb.append("{anno=");
		sb.append(getAnno());
		sb.append(", codiceAttivita=");
		sb.append(getCodiceAttivita());
		sb.append(", codiceCentro=");
		sb.append(getCodiceCentro());
		sb.append(", codiceDeposito=");
		sb.append(getCodiceDeposito());
		sb.append(", tipoOrdine=");
		sb.append(getTipoOrdine());
		sb.append(", numeroOrdine=");
		sb.append(getNumeroOrdine());
		sb.append(", numeroRigo=");
		sb.append(getNumeroRigo());
		sb.append(", statoRigo=");
		sb.append(getStatoRigo());
		sb.append(", tipoRigo=");
		sb.append(getTipoRigo());
		sb.append(", codiceCausaleMagazzino=");
		sb.append(getCodiceCausaleMagazzino());
		sb.append(", codiceDepositoMov=");
		sb.append(getCodiceDepositoMov());
		sb.append(", codiceArticolo=");
		sb.append(getCodiceArticolo());
		sb.append(", codiceVariante=");
		sb.append(getCodiceVariante());
		sb.append(", descrizione=");
		sb.append(getDescrizione());
		sb.append(", codiceUnitMis=");
		sb.append(getCodiceUnitMis());
		sb.append(", decimaliQuant=");
		sb.append(getDecimaliQuant());
		sb.append(", quantita1=");
		sb.append(getQuantita1());
		sb.append(", quantita2=");
		sb.append(getQuantita2());
		sb.append(", quantita3=");
		sb.append(getQuantita3());
		sb.append(", quantita=");
		sb.append(getQuantita());
		sb.append(", codiceUnitMis2=");
		sb.append(getCodiceUnitMis2());
		sb.append(", quantitaUnitMis2=");
		sb.append(getQuantitaUnitMis2());
		sb.append(", decimaliPrezzo=");
		sb.append(getDecimaliPrezzo());
		sb.append(", prezzo=");
		sb.append(getPrezzo());
		sb.append(", importoLordo=");
		sb.append(getImportoLordo());
		sb.append(", sconto1=");
		sb.append(getSconto1());
		sb.append(", sconto2=");
		sb.append(getSconto2());
		sb.append(", sconto3=");
		sb.append(getSconto3());
		sb.append(", importoNetto=");
		sb.append(getImportoNetto());
		sb.append(", importo=");
		sb.append(getImporto());
		sb.append(", codiceIVAFatturazione=");
		sb.append(getCodiceIVAFatturazione());
		sb.append(", tipoProvviggione=");
		sb.append(getTipoProvviggione());
		sb.append(", percentualeProvvAgente=");
		sb.append(getPercentualeProvvAgente());
		sb.append(", importoProvvAgente=");
		sb.append(getImportoProvvAgente());
		sb.append(", codiceSottoconto=");
		sb.append(getCodiceSottoconto());
		sb.append(", nomenclaturaCombinata=");
		sb.append(getNomenclaturaCombinata());
		sb.append(", stampaDistBase=");
		sb.append(getStampaDistBase());
		sb.append(", codiceCliente=");
		sb.append(getCodiceCliente());
		sb.append(", riferimentoOrdineCliente=");
		sb.append(getRiferimentoOrdineCliente());
		sb.append(", dataOridine=");
		sb.append(getDataOridine());
		sb.append(", statoEvasione=");
		sb.append(getStatoEvasione());
		sb.append(", dataPrevistaConsegna=");
		sb.append(getDataPrevistaConsegna());
		sb.append(", dataRegistrazioneOrdine=");
		sb.append(getDataRegistrazioneOrdine());
		sb.append(", numeroPrimaNota=");
		sb.append(getNumeroPrimaNota());
		sb.append(", numeroProgressivo=");
		sb.append(getNumeroProgressivo());
		sb.append(", tipoCompEspl=");
		sb.append(getTipoCompEspl());
		sb.append(", maxLivCompEspl=");
		sb.append(getMaxLivCompEspl());
		sb.append(", generazioneOrdFornit=");
		sb.append(getGenerazioneOrdFornit());
		sb.append(", esercizioProOrd=");
		sb.append(getEsercizioProOrd());
		sb.append(", numPropOrdine=");
		sb.append(getNumPropOrdine());
		sb.append(", numRigoPropOrdine=");
		sb.append(getNumRigoPropOrdine());
		sb.append(", libStr1=");
		sb.append(getLibStr1());
		sb.append(", libStr2=");
		sb.append(getLibStr2());
		sb.append(", libStr3=");
		sb.append(getLibStr3());
		sb.append(", libDbl1=");
		sb.append(getLibDbl1());
		sb.append(", libDbl2=");
		sb.append(getLibDbl2());
		sb.append(", libDbl3=");
		sb.append(getLibDbl3());
		sb.append(", libDat1=");
		sb.append(getLibDat1());
		sb.append(", libDat2=");
		sb.append(getLibDat2());
		sb.append(", libDat3=");
		sb.append(getLibDat3());
		sb.append(", libLng1=");
		sb.append(getLibLng1());
		sb.append(", libLng2=");
		sb.append(getLibLng2());
		sb.append(", libLng3=");
		sb.append(getLibLng3());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(193);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.RigheOrdiniClienti");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>anno</column-name><column-value><![CDATA[");
		sb.append(getAnno());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceAttivita</column-name><column-value><![CDATA[");
		sb.append(getCodiceAttivita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCentro</column-name><column-value><![CDATA[");
		sb.append(getCodiceCentro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDeposito</column-name><column-value><![CDATA[");
		sb.append(getCodiceDeposito());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoOrdine</column-name><column-value><![CDATA[");
		sb.append(getTipoOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroOrdine</column-name><column-value><![CDATA[");
		sb.append(getNumeroOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroRigo</column-name><column-value><![CDATA[");
		sb.append(getNumeroRigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statoRigo</column-name><column-value><![CDATA[");
		sb.append(getStatoRigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoRigo</column-name><column-value><![CDATA[");
		sb.append(getTipoRigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCausaleMagazzino</column-name><column-value><![CDATA[");
		sb.append(getCodiceCausaleMagazzino());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDepositoMov</column-name><column-value><![CDATA[");
		sb.append(getCodiceDepositoMov());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceArticolo</column-name><column-value><![CDATA[");
		sb.append(getCodiceArticolo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceVariante</column-name><column-value><![CDATA[");
		sb.append(getCodiceVariante());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizione</column-name><column-value><![CDATA[");
		sb.append(getDescrizione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceUnitMis</column-name><column-value><![CDATA[");
		sb.append(getCodiceUnitMis());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>decimaliQuant</column-name><column-value><![CDATA[");
		sb.append(getDecimaliQuant());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantita1</column-name><column-value><![CDATA[");
		sb.append(getQuantita1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantita2</column-name><column-value><![CDATA[");
		sb.append(getQuantita2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantita3</column-name><column-value><![CDATA[");
		sb.append(getQuantita3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantita</column-name><column-value><![CDATA[");
		sb.append(getQuantita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceUnitMis2</column-name><column-value><![CDATA[");
		sb.append(getCodiceUnitMis2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantitaUnitMis2</column-name><column-value><![CDATA[");
		sb.append(getQuantitaUnitMis2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>decimaliPrezzo</column-name><column-value><![CDATA[");
		sb.append(getDecimaliPrezzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>prezzo</column-name><column-value><![CDATA[");
		sb.append(getPrezzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoLordo</column-name><column-value><![CDATA[");
		sb.append(getImportoLordo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sconto1</column-name><column-value><![CDATA[");
		sb.append(getSconto1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sconto2</column-name><column-value><![CDATA[");
		sb.append(getSconto2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sconto3</column-name><column-value><![CDATA[");
		sb.append(getSconto3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoNetto</column-name><column-value><![CDATA[");
		sb.append(getImportoNetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importo</column-name><column-value><![CDATA[");
		sb.append(getImporto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVAFatturazione</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVAFatturazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoProvviggione</column-name><column-value><![CDATA[");
		sb.append(getTipoProvviggione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>percentualeProvvAgente</column-name><column-value><![CDATA[");
		sb.append(getPercentualeProvvAgente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoProvvAgente</column-name><column-value><![CDATA[");
		sb.append(getImportoProvvAgente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceSottoconto</column-name><column-value><![CDATA[");
		sb.append(getCodiceSottoconto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nomenclaturaCombinata</column-name><column-value><![CDATA[");
		sb.append(getNomenclaturaCombinata());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>stampaDistBase</column-name><column-value><![CDATA[");
		sb.append(getStampaDistBase());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCliente</column-name><column-value><![CDATA[");
		sb.append(getCodiceCliente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>riferimentoOrdineCliente</column-name><column-value><![CDATA[");
		sb.append(getRiferimentoOrdineCliente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataOridine</column-name><column-value><![CDATA[");
		sb.append(getDataOridine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statoEvasione</column-name><column-value><![CDATA[");
		sb.append(getStatoEvasione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataPrevistaConsegna</column-name><column-value><![CDATA[");
		sb.append(getDataPrevistaConsegna());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataRegistrazioneOrdine</column-name><column-value><![CDATA[");
		sb.append(getDataRegistrazioneOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroPrimaNota</column-name><column-value><![CDATA[");
		sb.append(getNumeroPrimaNota());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroProgressivo</column-name><column-value><![CDATA[");
		sb.append(getNumeroProgressivo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoCompEspl</column-name><column-value><![CDATA[");
		sb.append(getTipoCompEspl());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>maxLivCompEspl</column-name><column-value><![CDATA[");
		sb.append(getMaxLivCompEspl());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>generazioneOrdFornit</column-name><column-value><![CDATA[");
		sb.append(getGenerazioneOrdFornit());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>esercizioProOrd</column-name><column-value><![CDATA[");
		sb.append(getEsercizioProOrd());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numPropOrdine</column-name><column-value><![CDATA[");
		sb.append(getNumPropOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numRigoPropOrdine</column-name><column-value><![CDATA[");
		sb.append(getNumRigoPropOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr1</column-name><column-value><![CDATA[");
		sb.append(getLibStr1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr2</column-name><column-value><![CDATA[");
		sb.append(getLibStr2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr3</column-name><column-value><![CDATA[");
		sb.append(getLibStr3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl1</column-name><column-value><![CDATA[");
		sb.append(getLibDbl1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl2</column-name><column-value><![CDATA[");
		sb.append(getLibDbl2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl3</column-name><column-value><![CDATA[");
		sb.append(getLibDbl3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat1</column-name><column-value><![CDATA[");
		sb.append(getLibDat1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat2</column-name><column-value><![CDATA[");
		sb.append(getLibDat2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat3</column-name><column-value><![CDATA[");
		sb.append(getLibDat3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng1</column-name><column-value><![CDATA[");
		sb.append(getLibLng1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng2</column-name><column-value><![CDATA[");
		sb.append(getLibLng2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng3</column-name><column-value><![CDATA[");
		sb.append(getLibLng3());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _anno;
	private String _codiceAttivita;
	private String _codiceCentro;
	private String _codiceDeposito;
	private int _tipoOrdine;
	private int _numeroOrdine;
	private int _numeroRigo;
	private boolean _statoRigo;
	private int _tipoRigo;
	private String _codiceCausaleMagazzino;
	private String _codiceDepositoMov;
	private String _codiceArticolo;
	private String _codiceVariante;
	private String _descrizione;
	private String _codiceUnitMis;
	private int _decimaliQuant;
	private double _quantita1;
	private double _quantita2;
	private double _quantita3;
	private double _quantita;
	private String _codiceUnitMis2;
	private double _quantitaUnitMis2;
	private int _decimaliPrezzo;
	private double _prezzo;
	private double _importoLordo;
	private double _sconto1;
	private double _sconto2;
	private double _sconto3;
	private double _importoNetto;
	private double _importo;
	private String _codiceIVAFatturazione;
	private boolean _tipoProvviggione;
	private double _percentualeProvvAgente;
	private double _importoProvvAgente;
	private String _codiceSottoconto;
	private int _nomenclaturaCombinata;
	private boolean _stampaDistBase;
	private String _codiceCliente;
	private String _riferimentoOrdineCliente;
	private Date _dataOridine;
	private boolean _statoEvasione;
	private Date _dataPrevistaConsegna;
	private Date _dataRegistrazioneOrdine;
	private int _numeroPrimaNota;
	private int _numeroProgressivo;
	private int _tipoCompEspl;
	private int _maxLivCompEspl;
	private int _generazioneOrdFornit;
	private int _esercizioProOrd;
	private int _numPropOrdine;
	private int _numRigoPropOrdine;
	private String _libStr1;
	private String _libStr2;
	private String _libStr3;
	private double _libDbl1;
	private double _libDbl2;
	private double _libDbl3;
	private Date _libDat1;
	private Date _libDat2;
	private Date _libDat3;
	private long _libLng1;
	private long _libLng2;
	private long _libLng3;
	private BaseModel<?> _righeOrdiniClientiRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}