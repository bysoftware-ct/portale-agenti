/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.TestataFattureClientiLocalServiceUtil;
import it.bysoftware.ct.service.persistence.TestataFattureClientiPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class TestataFattureClientiClp extends BaseModelImpl<TestataFattureClienti>
	implements TestataFattureClienti {
	public TestataFattureClientiClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return TestataFattureClienti.class;
	}

	@Override
	public String getModelClassName() {
		return TestataFattureClienti.class.getName();
	}

	@Override
	public TestataFattureClientiPK getPrimaryKey() {
		return new TestataFattureClientiPK(_anno, _codiceAttivita,
			_codiceCentro, _numeroProtocollo);
	}

	@Override
	public void setPrimaryKey(TestataFattureClientiPK primaryKey) {
		setAnno(primaryKey.anno);
		setCodiceAttivita(primaryKey.codiceAttivita);
		setCodiceCentro(primaryKey.codiceCentro);
		setNumeroProtocollo(primaryKey.numeroProtocollo);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new TestataFattureClientiPK(_anno, _codiceAttivita,
			_codiceCentro, _numeroProtocollo);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((TestataFattureClientiPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("anno", getAnno());
		attributes.put("codiceAttivita", getCodiceAttivita());
		attributes.put("codiceCentro", getCodiceCentro());
		attributes.put("numeroProtocollo", getNumeroProtocollo());
		attributes.put("tipoDocumento", getTipoDocumento());
		attributes.put("codiceCentroContAnalitica",
			getCodiceCentroContAnalitica());
		attributes.put("idTipoDocumentoOrigine", getIdTipoDocumentoOrigine());
		attributes.put("statoFattura", getStatoFattura());
		attributes.put("dataRegistrazione", getDataRegistrazione());
		attributes.put("dataOperazione", getDataOperazione());
		attributes.put("dataAnnotazione", getDataAnnotazione());
		attributes.put("dataDocumento", getDataDocumento());
		attributes.put("numeroDocumento", getNumeroDocumento());
		attributes.put("riferimentoAScontrino", getRiferimentoAScontrino());
		attributes.put("descrizioneEstremiDocumento",
			getDescrizioneEstremiDocumento());
		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceCliente", getCodiceCliente());
		attributes.put("descrizioneAggiuntivaFattura",
			getDescrizioneAggiuntivaFattura());
		attributes.put("estremiOrdine", getEstremiOrdine());
		attributes.put("estremiBolla", getEstremiBolla());
		attributes.put("codicePagamento", getCodicePagamento());
		attributes.put("codiceAgente", getCodiceAgente());
		attributes.put("codiceGruppoAgenti", getCodiceGruppoAgenti());
		attributes.put("annotazioni", getAnnotazioni());
		attributes.put("scontoChiusura", getScontoChiusura());
		attributes.put("scontoProntaCassa", getScontoProntaCassa());
		attributes.put("pertualeSpeseTrasp", getPertualeSpeseTrasp());
		attributes.put("importoSpeseTrasp", getImportoSpeseTrasp());
		attributes.put("importoSpeseImb", getImportoSpeseImb());
		attributes.put("importoSpeseVarie", getImportoSpeseVarie());
		attributes.put("importoSpeseBancarie", getImportoSpeseBancarie());
		attributes.put("codiceIVATrasp", getCodiceIVATrasp());
		attributes.put("codiceIVAImb", getCodiceIVAImb());
		attributes.put("codiceIVAVarie", getCodiceIVAVarie());
		attributes.put("codiceIVABancarie", getCodiceIVABancarie());
		attributes.put("totaleScontiCorpo", getTotaleScontiCorpo());
		attributes.put("imponibile", getImponibile());
		attributes.put("IVA", getIVA());
		attributes.put("importoFattura", getImportoFattura());
		attributes.put("libStr1", getLibStr1());
		attributes.put("libStr2", getLibStr2());
		attributes.put("libStr3", getLibStr3());
		attributes.put("libDbl1", getLibDbl1());
		attributes.put("libDbl2", getLibDbl2());
		attributes.put("libDbl3", getLibDbl3());
		attributes.put("libLng1", getLibLng1());
		attributes.put("libLng2", getLibLng2());
		attributes.put("libLng3", getLibLng3());
		attributes.put("libDat1", getLibDat1());
		attributes.put("libDat2", getLibDat2());
		attributes.put("libDat3", getLibDat3());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer anno = (Integer)attributes.get("anno");

		if (anno != null) {
			setAnno(anno);
		}

		String codiceAttivita = (String)attributes.get("codiceAttivita");

		if (codiceAttivita != null) {
			setCodiceAttivita(codiceAttivita);
		}

		String codiceCentro = (String)attributes.get("codiceCentro");

		if (codiceCentro != null) {
			setCodiceCentro(codiceCentro);
		}

		Integer numeroProtocollo = (Integer)attributes.get("numeroProtocollo");

		if (numeroProtocollo != null) {
			setNumeroProtocollo(numeroProtocollo);
		}

		String tipoDocumento = (String)attributes.get("tipoDocumento");

		if (tipoDocumento != null) {
			setTipoDocumento(tipoDocumento);
		}

		String codiceCentroContAnalitica = (String)attributes.get(
				"codiceCentroContAnalitica");

		if (codiceCentroContAnalitica != null) {
			setCodiceCentroContAnalitica(codiceCentroContAnalitica);
		}

		Integer idTipoDocumentoOrigine = (Integer)attributes.get(
				"idTipoDocumentoOrigine");

		if (idTipoDocumentoOrigine != null) {
			setIdTipoDocumentoOrigine(idTipoDocumentoOrigine);
		}

		Boolean statoFattura = (Boolean)attributes.get("statoFattura");

		if (statoFattura != null) {
			setStatoFattura(statoFattura);
		}

		Date dataRegistrazione = (Date)attributes.get("dataRegistrazione");

		if (dataRegistrazione != null) {
			setDataRegistrazione(dataRegistrazione);
		}

		Date dataOperazione = (Date)attributes.get("dataOperazione");

		if (dataOperazione != null) {
			setDataOperazione(dataOperazione);
		}

		Date dataAnnotazione = (Date)attributes.get("dataAnnotazione");

		if (dataAnnotazione != null) {
			setDataAnnotazione(dataAnnotazione);
		}

		Date dataDocumento = (Date)attributes.get("dataDocumento");

		if (dataDocumento != null) {
			setDataDocumento(dataDocumento);
		}

		Integer numeroDocumento = (Integer)attributes.get("numeroDocumento");

		if (numeroDocumento != null) {
			setNumeroDocumento(numeroDocumento);
		}

		Integer riferimentoAScontrino = (Integer)attributes.get(
				"riferimentoAScontrino");

		if (riferimentoAScontrino != null) {
			setRiferimentoAScontrino(riferimentoAScontrino);
		}

		String descrizioneEstremiDocumento = (String)attributes.get(
				"descrizioneEstremiDocumento");

		if (descrizioneEstremiDocumento != null) {
			setDescrizioneEstremiDocumento(descrizioneEstremiDocumento);
		}

		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceCliente = (String)attributes.get("codiceCliente");

		if (codiceCliente != null) {
			setCodiceCliente(codiceCliente);
		}

		String descrizioneAggiuntivaFattura = (String)attributes.get(
				"descrizioneAggiuntivaFattura");

		if (descrizioneAggiuntivaFattura != null) {
			setDescrizioneAggiuntivaFattura(descrizioneAggiuntivaFattura);
		}

		String estremiOrdine = (String)attributes.get("estremiOrdine");

		if (estremiOrdine != null) {
			setEstremiOrdine(estremiOrdine);
		}

		String estremiBolla = (String)attributes.get("estremiBolla");

		if (estremiBolla != null) {
			setEstremiBolla(estremiBolla);
		}

		String codicePagamento = (String)attributes.get("codicePagamento");

		if (codicePagamento != null) {
			setCodicePagamento(codicePagamento);
		}

		String codiceAgente = (String)attributes.get("codiceAgente");

		if (codiceAgente != null) {
			setCodiceAgente(codiceAgente);
		}

		String codiceGruppoAgenti = (String)attributes.get("codiceGruppoAgenti");

		if (codiceGruppoAgenti != null) {
			setCodiceGruppoAgenti(codiceGruppoAgenti);
		}

		String annotazioni = (String)attributes.get("annotazioni");

		if (annotazioni != null) {
			setAnnotazioni(annotazioni);
		}

		Double scontoChiusura = (Double)attributes.get("scontoChiusura");

		if (scontoChiusura != null) {
			setScontoChiusura(scontoChiusura);
		}

		Double scontoProntaCassa = (Double)attributes.get("scontoProntaCassa");

		if (scontoProntaCassa != null) {
			setScontoProntaCassa(scontoProntaCassa);
		}

		Double pertualeSpeseTrasp = (Double)attributes.get("pertualeSpeseTrasp");

		if (pertualeSpeseTrasp != null) {
			setPertualeSpeseTrasp(pertualeSpeseTrasp);
		}

		Double importoSpeseTrasp = (Double)attributes.get("importoSpeseTrasp");

		if (importoSpeseTrasp != null) {
			setImportoSpeseTrasp(importoSpeseTrasp);
		}

		Double importoSpeseImb = (Double)attributes.get("importoSpeseImb");

		if (importoSpeseImb != null) {
			setImportoSpeseImb(importoSpeseImb);
		}

		Double importoSpeseVarie = (Double)attributes.get("importoSpeseVarie");

		if (importoSpeseVarie != null) {
			setImportoSpeseVarie(importoSpeseVarie);
		}

		Double importoSpeseBancarie = (Double)attributes.get(
				"importoSpeseBancarie");

		if (importoSpeseBancarie != null) {
			setImportoSpeseBancarie(importoSpeseBancarie);
		}

		String codiceIVATrasp = (String)attributes.get("codiceIVATrasp");

		if (codiceIVATrasp != null) {
			setCodiceIVATrasp(codiceIVATrasp);
		}

		String codiceIVAImb = (String)attributes.get("codiceIVAImb");

		if (codiceIVAImb != null) {
			setCodiceIVAImb(codiceIVAImb);
		}

		String codiceIVAVarie = (String)attributes.get("codiceIVAVarie");

		if (codiceIVAVarie != null) {
			setCodiceIVAVarie(codiceIVAVarie);
		}

		String codiceIVABancarie = (String)attributes.get("codiceIVABancarie");

		if (codiceIVABancarie != null) {
			setCodiceIVABancarie(codiceIVABancarie);
		}

		Double totaleScontiCorpo = (Double)attributes.get("totaleScontiCorpo");

		if (totaleScontiCorpo != null) {
			setTotaleScontiCorpo(totaleScontiCorpo);
		}

		Double imponibile = (Double)attributes.get("imponibile");

		if (imponibile != null) {
			setImponibile(imponibile);
		}

		Double IVA = (Double)attributes.get("IVA");

		if (IVA != null) {
			setIVA(IVA);
		}

		Double importoFattura = (Double)attributes.get("importoFattura");

		if (importoFattura != null) {
			setImportoFattura(importoFattura);
		}

		String libStr1 = (String)attributes.get("libStr1");

		if (libStr1 != null) {
			setLibStr1(libStr1);
		}

		String libStr2 = (String)attributes.get("libStr2");

		if (libStr2 != null) {
			setLibStr2(libStr2);
		}

		String libStr3 = (String)attributes.get("libStr3");

		if (libStr3 != null) {
			setLibStr3(libStr3);
		}

		Double libDbl1 = (Double)attributes.get("libDbl1");

		if (libDbl1 != null) {
			setLibDbl1(libDbl1);
		}

		Double libDbl2 = (Double)attributes.get("libDbl2");

		if (libDbl2 != null) {
			setLibDbl2(libDbl2);
		}

		Double libDbl3 = (Double)attributes.get("libDbl3");

		if (libDbl3 != null) {
			setLibDbl3(libDbl3);
		}

		Long libLng1 = (Long)attributes.get("libLng1");

		if (libLng1 != null) {
			setLibLng1(libLng1);
		}

		Long libLng2 = (Long)attributes.get("libLng2");

		if (libLng2 != null) {
			setLibLng2(libLng2);
		}

		Long libLng3 = (Long)attributes.get("libLng3");

		if (libLng3 != null) {
			setLibLng3(libLng3);
		}

		Date libDat1 = (Date)attributes.get("libDat1");

		if (libDat1 != null) {
			setLibDat1(libDat1);
		}

		Date libDat2 = (Date)attributes.get("libDat2");

		if (libDat2 != null) {
			setLibDat2(libDat2);
		}

		Date libDat3 = (Date)attributes.get("libDat3");

		if (libDat3 != null) {
			setLibDat3(libDat3);
		}
	}

	@Override
	public int getAnno() {
		return _anno;
	}

	@Override
	public void setAnno(int anno) {
		_anno = anno;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setAnno", int.class);

				method.invoke(_testataFattureClientiRemoteModel, anno);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceAttivita() {
		return _codiceAttivita;
	}

	@Override
	public void setCodiceAttivita(String codiceAttivita) {
		_codiceAttivita = codiceAttivita;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceAttivita",
						String.class);

				method.invoke(_testataFattureClientiRemoteModel, codiceAttivita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCentro() {
		return _codiceCentro;
	}

	@Override
	public void setCodiceCentro(String codiceCentro) {
		_codiceCentro = codiceCentro;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCentro", String.class);

				method.invoke(_testataFattureClientiRemoteModel, codiceCentro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroProtocollo() {
		return _numeroProtocollo;
	}

	@Override
	public void setNumeroProtocollo(int numeroProtocollo) {
		_numeroProtocollo = numeroProtocollo;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroProtocollo", int.class);

				method.invoke(_testataFattureClientiRemoteModel,
					numeroProtocollo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoDocumento() {
		return _tipoDocumento;
	}

	@Override
	public void setTipoDocumento(String tipoDocumento) {
		_tipoDocumento = tipoDocumento;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoDocumento", String.class);

				method.invoke(_testataFattureClientiRemoteModel, tipoDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCentroContAnalitica() {
		return _codiceCentroContAnalitica;
	}

	@Override
	public void setCodiceCentroContAnalitica(String codiceCentroContAnalitica) {
		_codiceCentroContAnalitica = codiceCentroContAnalitica;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCentroContAnalitica",
						String.class);

				method.invoke(_testataFattureClientiRemoteModel,
					codiceCentroContAnalitica);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getIdTipoDocumentoOrigine() {
		return _idTipoDocumentoOrigine;
	}

	@Override
	public void setIdTipoDocumentoOrigine(int idTipoDocumentoOrigine) {
		_idTipoDocumentoOrigine = idTipoDocumentoOrigine;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setIdTipoDocumentoOrigine",
						int.class);

				method.invoke(_testataFattureClientiRemoteModel,
					idTipoDocumentoOrigine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getStatoFattura() {
		return _statoFattura;
	}

	@Override
	public boolean isStatoFattura() {
		return _statoFattura;
	}

	@Override
	public void setStatoFattura(boolean statoFattura) {
		_statoFattura = statoFattura;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setStatoFattura", boolean.class);

				method.invoke(_testataFattureClientiRemoteModel, statoFattura);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataRegistrazione() {
		return _dataRegistrazione;
	}

	@Override
	public void setDataRegistrazione(Date dataRegistrazione) {
		_dataRegistrazione = dataRegistrazione;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataRegistrazione",
						Date.class);

				method.invoke(_testataFattureClientiRemoteModel,
					dataRegistrazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataOperazione() {
		return _dataOperazione;
	}

	@Override
	public void setDataOperazione(Date dataOperazione) {
		_dataOperazione = dataOperazione;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataOperazione", Date.class);

				method.invoke(_testataFattureClientiRemoteModel, dataOperazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataAnnotazione() {
		return _dataAnnotazione;
	}

	@Override
	public void setDataAnnotazione(Date dataAnnotazione) {
		_dataAnnotazione = dataAnnotazione;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataAnnotazione", Date.class);

				method.invoke(_testataFattureClientiRemoteModel, dataAnnotazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataDocumento() {
		return _dataDocumento;
	}

	@Override
	public void setDataDocumento(Date dataDocumento) {
		_dataDocumento = dataDocumento;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataDocumento", Date.class);

				method.invoke(_testataFattureClientiRemoteModel, dataDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroDocumento() {
		return _numeroDocumento;
	}

	@Override
	public void setNumeroDocumento(int numeroDocumento) {
		_numeroDocumento = numeroDocumento;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroDocumento", int.class);

				method.invoke(_testataFattureClientiRemoteModel, numeroDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getRiferimentoAScontrino() {
		return _riferimentoAScontrino;
	}

	@Override
	public void setRiferimentoAScontrino(int riferimentoAScontrino) {
		_riferimentoAScontrino = riferimentoAScontrino;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setRiferimentoAScontrino",
						int.class);

				method.invoke(_testataFattureClientiRemoteModel,
					riferimentoAScontrino);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizioneEstremiDocumento() {
		return _descrizioneEstremiDocumento;
	}

	@Override
	public void setDescrizioneEstremiDocumento(
		String descrizioneEstremiDocumento) {
		_descrizioneEstremiDocumento = descrizioneEstremiDocumento;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizioneEstremiDocumento",
						String.class);

				method.invoke(_testataFattureClientiRemoteModel,
					descrizioneEstremiDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoSoggetto", boolean.class);

				method.invoke(_testataFattureClientiRemoteModel, tipoSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCliente() {
		return _codiceCliente;
	}

	@Override
	public void setCodiceCliente(String codiceCliente) {
		_codiceCliente = codiceCliente;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCliente", String.class);

				method.invoke(_testataFattureClientiRemoteModel, codiceCliente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizioneAggiuntivaFattura() {
		return _descrizioneAggiuntivaFattura;
	}

	@Override
	public void setDescrizioneAggiuntivaFattura(
		String descrizioneAggiuntivaFattura) {
		_descrizioneAggiuntivaFattura = descrizioneAggiuntivaFattura;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizioneAggiuntivaFattura",
						String.class);

				method.invoke(_testataFattureClientiRemoteModel,
					descrizioneAggiuntivaFattura);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEstremiOrdine() {
		return _estremiOrdine;
	}

	@Override
	public void setEstremiOrdine(String estremiOrdine) {
		_estremiOrdine = estremiOrdine;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setEstremiOrdine", String.class);

				method.invoke(_testataFattureClientiRemoteModel, estremiOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEstremiBolla() {
		return _estremiBolla;
	}

	@Override
	public void setEstremiBolla(String estremiBolla) {
		_estremiBolla = estremiBolla;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setEstremiBolla", String.class);

				method.invoke(_testataFattureClientiRemoteModel, estremiBolla);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodicePagamento() {
		return _codicePagamento;
	}

	@Override
	public void setCodicePagamento(String codicePagamento) {
		_codicePagamento = codicePagamento;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodicePagamento",
						String.class);

				method.invoke(_testataFattureClientiRemoteModel, codicePagamento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceAgente() {
		return _codiceAgente;
	}

	@Override
	public void setCodiceAgente(String codiceAgente) {
		_codiceAgente = codiceAgente;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceAgente", String.class);

				method.invoke(_testataFattureClientiRemoteModel, codiceAgente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceGruppoAgenti() {
		return _codiceGruppoAgenti;
	}

	@Override
	public void setCodiceGruppoAgenti(String codiceGruppoAgenti) {
		_codiceGruppoAgenti = codiceGruppoAgenti;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceGruppoAgenti",
						String.class);

				method.invoke(_testataFattureClientiRemoteModel,
					codiceGruppoAgenti);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getAnnotazioni() {
		return _annotazioni;
	}

	@Override
	public void setAnnotazioni(String annotazioni) {
		_annotazioni = annotazioni;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setAnnotazioni", String.class);

				method.invoke(_testataFattureClientiRemoteModel, annotazioni);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getScontoChiusura() {
		return _scontoChiusura;
	}

	@Override
	public void setScontoChiusura(double scontoChiusura) {
		_scontoChiusura = scontoChiusura;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setScontoChiusura",
						double.class);

				method.invoke(_testataFattureClientiRemoteModel, scontoChiusura);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getScontoProntaCassa() {
		return _scontoProntaCassa;
	}

	@Override
	public void setScontoProntaCassa(double scontoProntaCassa) {
		_scontoProntaCassa = scontoProntaCassa;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setScontoProntaCassa",
						double.class);

				method.invoke(_testataFattureClientiRemoteModel,
					scontoProntaCassa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPertualeSpeseTrasp() {
		return _pertualeSpeseTrasp;
	}

	@Override
	public void setPertualeSpeseTrasp(double pertualeSpeseTrasp) {
		_pertualeSpeseTrasp = pertualeSpeseTrasp;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setPertualeSpeseTrasp",
						double.class);

				method.invoke(_testataFattureClientiRemoteModel,
					pertualeSpeseTrasp);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoSpeseTrasp() {
		return _importoSpeseTrasp;
	}

	@Override
	public void setImportoSpeseTrasp(double importoSpeseTrasp) {
		_importoSpeseTrasp = importoSpeseTrasp;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoSpeseTrasp",
						double.class);

				method.invoke(_testataFattureClientiRemoteModel,
					importoSpeseTrasp);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoSpeseImb() {
		return _importoSpeseImb;
	}

	@Override
	public void setImportoSpeseImb(double importoSpeseImb) {
		_importoSpeseImb = importoSpeseImb;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoSpeseImb",
						double.class);

				method.invoke(_testataFattureClientiRemoteModel, importoSpeseImb);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoSpeseVarie() {
		return _importoSpeseVarie;
	}

	@Override
	public void setImportoSpeseVarie(double importoSpeseVarie) {
		_importoSpeseVarie = importoSpeseVarie;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoSpeseVarie",
						double.class);

				method.invoke(_testataFattureClientiRemoteModel,
					importoSpeseVarie);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoSpeseBancarie() {
		return _importoSpeseBancarie;
	}

	@Override
	public void setImportoSpeseBancarie(double importoSpeseBancarie) {
		_importoSpeseBancarie = importoSpeseBancarie;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoSpeseBancarie",
						double.class);

				method.invoke(_testataFattureClientiRemoteModel,
					importoSpeseBancarie);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVATrasp() {
		return _codiceIVATrasp;
	}

	@Override
	public void setCodiceIVATrasp(String codiceIVATrasp) {
		_codiceIVATrasp = codiceIVATrasp;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVATrasp",
						String.class);

				method.invoke(_testataFattureClientiRemoteModel, codiceIVATrasp);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVAImb() {
		return _codiceIVAImb;
	}

	@Override
	public void setCodiceIVAImb(String codiceIVAImb) {
		_codiceIVAImb = codiceIVAImb;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVAImb", String.class);

				method.invoke(_testataFattureClientiRemoteModel, codiceIVAImb);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVAVarie() {
		return _codiceIVAVarie;
	}

	@Override
	public void setCodiceIVAVarie(String codiceIVAVarie) {
		_codiceIVAVarie = codiceIVAVarie;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVAVarie",
						String.class);

				method.invoke(_testataFattureClientiRemoteModel, codiceIVAVarie);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIVABancarie() {
		return _codiceIVABancarie;
	}

	@Override
	public void setCodiceIVABancarie(String codiceIVABancarie) {
		_codiceIVABancarie = codiceIVABancarie;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIVABancarie",
						String.class);

				method.invoke(_testataFattureClientiRemoteModel,
					codiceIVABancarie);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getTotaleScontiCorpo() {
		return _totaleScontiCorpo;
	}

	@Override
	public void setTotaleScontiCorpo(double totaleScontiCorpo) {
		_totaleScontiCorpo = totaleScontiCorpo;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTotaleScontiCorpo",
						double.class);

				method.invoke(_testataFattureClientiRemoteModel,
					totaleScontiCorpo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImponibile() {
		return _imponibile;
	}

	@Override
	public void setImponibile(double imponibile) {
		_imponibile = imponibile;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setImponibile", double.class);

				method.invoke(_testataFattureClientiRemoteModel, imponibile);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getIVA() {
		return _IVA;
	}

	@Override
	public void setIVA(double IVA) {
		_IVA = IVA;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setIVA", double.class);

				method.invoke(_testataFattureClientiRemoteModel, IVA);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoFattura() {
		return _importoFattura;
	}

	@Override
	public void setImportoFattura(double importoFattura) {
		_importoFattura = importoFattura;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoFattura",
						double.class);

				method.invoke(_testataFattureClientiRemoteModel, importoFattura);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr1() {
		return _libStr1;
	}

	@Override
	public void setLibStr1(String libStr1) {
		_libStr1 = libStr1;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr1", String.class);

				method.invoke(_testataFattureClientiRemoteModel, libStr1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr2() {
		return _libStr2;
	}

	@Override
	public void setLibStr2(String libStr2) {
		_libStr2 = libStr2;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr2", String.class);

				method.invoke(_testataFattureClientiRemoteModel, libStr2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr3() {
		return _libStr3;
	}

	@Override
	public void setLibStr3(String libStr3) {
		_libStr3 = libStr3;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr3", String.class);

				method.invoke(_testataFattureClientiRemoteModel, libStr3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl1() {
		return _libDbl1;
	}

	@Override
	public void setLibDbl1(double libDbl1) {
		_libDbl1 = libDbl1;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl1", double.class);

				method.invoke(_testataFattureClientiRemoteModel, libDbl1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl2() {
		return _libDbl2;
	}

	@Override
	public void setLibDbl2(double libDbl2) {
		_libDbl2 = libDbl2;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl2", double.class);

				method.invoke(_testataFattureClientiRemoteModel, libDbl2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl3() {
		return _libDbl3;
	}

	@Override
	public void setLibDbl3(double libDbl3) {
		_libDbl3 = libDbl3;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl3", double.class);

				method.invoke(_testataFattureClientiRemoteModel, libDbl3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng1() {
		return _libLng1;
	}

	@Override
	public void setLibLng1(long libLng1) {
		_libLng1 = libLng1;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng1", long.class);

				method.invoke(_testataFattureClientiRemoteModel, libLng1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng2() {
		return _libLng2;
	}

	@Override
	public void setLibLng2(long libLng2) {
		_libLng2 = libLng2;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng2", long.class);

				method.invoke(_testataFattureClientiRemoteModel, libLng2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng3() {
		return _libLng3;
	}

	@Override
	public void setLibLng3(long libLng3) {
		_libLng3 = libLng3;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng3", long.class);

				method.invoke(_testataFattureClientiRemoteModel, libLng3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat1() {
		return _libDat1;
	}

	@Override
	public void setLibDat1(Date libDat1) {
		_libDat1 = libDat1;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat1", Date.class);

				method.invoke(_testataFattureClientiRemoteModel, libDat1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat2() {
		return _libDat2;
	}

	@Override
	public void setLibDat2(Date libDat2) {
		_libDat2 = libDat2;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat2", Date.class);

				method.invoke(_testataFattureClientiRemoteModel, libDat2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat3() {
		return _libDat3;
	}

	@Override
	public void setLibDat3(Date libDat3) {
		_libDat3 = libDat3;

		if (_testataFattureClientiRemoteModel != null) {
			try {
				Class<?> clazz = _testataFattureClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat3", Date.class);

				method.invoke(_testataFattureClientiRemoteModel, libDat3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getTestataFattureClientiRemoteModel() {
		return _testataFattureClientiRemoteModel;
	}

	public void setTestataFattureClientiRemoteModel(
		BaseModel<?> testataFattureClientiRemoteModel) {
		_testataFattureClientiRemoteModel = testataFattureClientiRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _testataFattureClientiRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_testataFattureClientiRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			TestataFattureClientiLocalServiceUtil.addTestataFattureClienti(this);
		}
		else {
			TestataFattureClientiLocalServiceUtil.updateTestataFattureClienti(this);
		}
	}

	@Override
	public TestataFattureClienti toEscapedModel() {
		return (TestataFattureClienti)ProxyUtil.newProxyInstance(TestataFattureClienti.class.getClassLoader(),
			new Class[] { TestataFattureClienti.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		TestataFattureClientiClp clone = new TestataFattureClientiClp();

		clone.setAnno(getAnno());
		clone.setCodiceAttivita(getCodiceAttivita());
		clone.setCodiceCentro(getCodiceCentro());
		clone.setNumeroProtocollo(getNumeroProtocollo());
		clone.setTipoDocumento(getTipoDocumento());
		clone.setCodiceCentroContAnalitica(getCodiceCentroContAnalitica());
		clone.setIdTipoDocumentoOrigine(getIdTipoDocumentoOrigine());
		clone.setStatoFattura(getStatoFattura());
		clone.setDataRegistrazione(getDataRegistrazione());
		clone.setDataOperazione(getDataOperazione());
		clone.setDataAnnotazione(getDataAnnotazione());
		clone.setDataDocumento(getDataDocumento());
		clone.setNumeroDocumento(getNumeroDocumento());
		clone.setRiferimentoAScontrino(getRiferimentoAScontrino());
		clone.setDescrizioneEstremiDocumento(getDescrizioneEstremiDocumento());
		clone.setTipoSoggetto(getTipoSoggetto());
		clone.setCodiceCliente(getCodiceCliente());
		clone.setDescrizioneAggiuntivaFattura(getDescrizioneAggiuntivaFattura());
		clone.setEstremiOrdine(getEstremiOrdine());
		clone.setEstremiBolla(getEstremiBolla());
		clone.setCodicePagamento(getCodicePagamento());
		clone.setCodiceAgente(getCodiceAgente());
		clone.setCodiceGruppoAgenti(getCodiceGruppoAgenti());
		clone.setAnnotazioni(getAnnotazioni());
		clone.setScontoChiusura(getScontoChiusura());
		clone.setScontoProntaCassa(getScontoProntaCassa());
		clone.setPertualeSpeseTrasp(getPertualeSpeseTrasp());
		clone.setImportoSpeseTrasp(getImportoSpeseTrasp());
		clone.setImportoSpeseImb(getImportoSpeseImb());
		clone.setImportoSpeseVarie(getImportoSpeseVarie());
		clone.setImportoSpeseBancarie(getImportoSpeseBancarie());
		clone.setCodiceIVATrasp(getCodiceIVATrasp());
		clone.setCodiceIVAImb(getCodiceIVAImb());
		clone.setCodiceIVAVarie(getCodiceIVAVarie());
		clone.setCodiceIVABancarie(getCodiceIVABancarie());
		clone.setTotaleScontiCorpo(getTotaleScontiCorpo());
		clone.setImponibile(getImponibile());
		clone.setIVA(getIVA());
		clone.setImportoFattura(getImportoFattura());
		clone.setLibStr1(getLibStr1());
		clone.setLibStr2(getLibStr2());
		clone.setLibStr3(getLibStr3());
		clone.setLibDbl1(getLibDbl1());
		clone.setLibDbl2(getLibDbl2());
		clone.setLibDbl3(getLibDbl3());
		clone.setLibLng1(getLibLng1());
		clone.setLibLng2(getLibLng2());
		clone.setLibLng3(getLibLng3());
		clone.setLibDat1(getLibDat1());
		clone.setLibDat2(getLibDat2());
		clone.setLibDat3(getLibDat3());

		return clone;
	}

	@Override
	public int compareTo(TestataFattureClienti testataFattureClienti) {
		TestataFattureClientiPK primaryKey = testataFattureClienti.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TestataFattureClientiClp)) {
			return false;
		}

		TestataFattureClientiClp testataFattureClienti = (TestataFattureClientiClp)obj;

		TestataFattureClientiPK primaryKey = testataFattureClienti.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(103);

		sb.append("{anno=");
		sb.append(getAnno());
		sb.append(", codiceAttivita=");
		sb.append(getCodiceAttivita());
		sb.append(", codiceCentro=");
		sb.append(getCodiceCentro());
		sb.append(", numeroProtocollo=");
		sb.append(getNumeroProtocollo());
		sb.append(", tipoDocumento=");
		sb.append(getTipoDocumento());
		sb.append(", codiceCentroContAnalitica=");
		sb.append(getCodiceCentroContAnalitica());
		sb.append(", idTipoDocumentoOrigine=");
		sb.append(getIdTipoDocumentoOrigine());
		sb.append(", statoFattura=");
		sb.append(getStatoFattura());
		sb.append(", dataRegistrazione=");
		sb.append(getDataRegistrazione());
		sb.append(", dataOperazione=");
		sb.append(getDataOperazione());
		sb.append(", dataAnnotazione=");
		sb.append(getDataAnnotazione());
		sb.append(", dataDocumento=");
		sb.append(getDataDocumento());
		sb.append(", numeroDocumento=");
		sb.append(getNumeroDocumento());
		sb.append(", riferimentoAScontrino=");
		sb.append(getRiferimentoAScontrino());
		sb.append(", descrizioneEstremiDocumento=");
		sb.append(getDescrizioneEstremiDocumento());
		sb.append(", tipoSoggetto=");
		sb.append(getTipoSoggetto());
		sb.append(", codiceCliente=");
		sb.append(getCodiceCliente());
		sb.append(", descrizioneAggiuntivaFattura=");
		sb.append(getDescrizioneAggiuntivaFattura());
		sb.append(", estremiOrdine=");
		sb.append(getEstremiOrdine());
		sb.append(", estremiBolla=");
		sb.append(getEstremiBolla());
		sb.append(", codicePagamento=");
		sb.append(getCodicePagamento());
		sb.append(", codiceAgente=");
		sb.append(getCodiceAgente());
		sb.append(", codiceGruppoAgenti=");
		sb.append(getCodiceGruppoAgenti());
		sb.append(", annotazioni=");
		sb.append(getAnnotazioni());
		sb.append(", scontoChiusura=");
		sb.append(getScontoChiusura());
		sb.append(", scontoProntaCassa=");
		sb.append(getScontoProntaCassa());
		sb.append(", pertualeSpeseTrasp=");
		sb.append(getPertualeSpeseTrasp());
		sb.append(", importoSpeseTrasp=");
		sb.append(getImportoSpeseTrasp());
		sb.append(", importoSpeseImb=");
		sb.append(getImportoSpeseImb());
		sb.append(", importoSpeseVarie=");
		sb.append(getImportoSpeseVarie());
		sb.append(", importoSpeseBancarie=");
		sb.append(getImportoSpeseBancarie());
		sb.append(", codiceIVATrasp=");
		sb.append(getCodiceIVATrasp());
		sb.append(", codiceIVAImb=");
		sb.append(getCodiceIVAImb());
		sb.append(", codiceIVAVarie=");
		sb.append(getCodiceIVAVarie());
		sb.append(", codiceIVABancarie=");
		sb.append(getCodiceIVABancarie());
		sb.append(", totaleScontiCorpo=");
		sb.append(getTotaleScontiCorpo());
		sb.append(", imponibile=");
		sb.append(getImponibile());
		sb.append(", IVA=");
		sb.append(getIVA());
		sb.append(", importoFattura=");
		sb.append(getImportoFattura());
		sb.append(", libStr1=");
		sb.append(getLibStr1());
		sb.append(", libStr2=");
		sb.append(getLibStr2());
		sb.append(", libStr3=");
		sb.append(getLibStr3());
		sb.append(", libDbl1=");
		sb.append(getLibDbl1());
		sb.append(", libDbl2=");
		sb.append(getLibDbl2());
		sb.append(", libDbl3=");
		sb.append(getLibDbl3());
		sb.append(", libLng1=");
		sb.append(getLibLng1());
		sb.append(", libLng2=");
		sb.append(getLibLng2());
		sb.append(", libLng3=");
		sb.append(getLibLng3());
		sb.append(", libDat1=");
		sb.append(getLibDat1());
		sb.append(", libDat2=");
		sb.append(getLibDat2());
		sb.append(", libDat3=");
		sb.append(getLibDat3());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(157);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.TestataFattureClienti");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>anno</column-name><column-value><![CDATA[");
		sb.append(getAnno());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceAttivita</column-name><column-value><![CDATA[");
		sb.append(getCodiceAttivita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCentro</column-name><column-value><![CDATA[");
		sb.append(getCodiceCentro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroProtocollo</column-name><column-value><![CDATA[");
		sb.append(getNumeroProtocollo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoDocumento</column-name><column-value><![CDATA[");
		sb.append(getTipoDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCentroContAnalitica</column-name><column-value><![CDATA[");
		sb.append(getCodiceCentroContAnalitica());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idTipoDocumentoOrigine</column-name><column-value><![CDATA[");
		sb.append(getIdTipoDocumentoOrigine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statoFattura</column-name><column-value><![CDATA[");
		sb.append(getStatoFattura());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataRegistrazione</column-name><column-value><![CDATA[");
		sb.append(getDataRegistrazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataOperazione</column-name><column-value><![CDATA[");
		sb.append(getDataOperazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataAnnotazione</column-name><column-value><![CDATA[");
		sb.append(getDataAnnotazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataDocumento</column-name><column-value><![CDATA[");
		sb.append(getDataDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroDocumento</column-name><column-value><![CDATA[");
		sb.append(getNumeroDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>riferimentoAScontrino</column-name><column-value><![CDATA[");
		sb.append(getRiferimentoAScontrino());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizioneEstremiDocumento</column-name><column-value><![CDATA[");
		sb.append(getDescrizioneEstremiDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoSoggetto</column-name><column-value><![CDATA[");
		sb.append(getTipoSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCliente</column-name><column-value><![CDATA[");
		sb.append(getCodiceCliente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizioneAggiuntivaFattura</column-name><column-value><![CDATA[");
		sb.append(getDescrizioneAggiuntivaFattura());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>estremiOrdine</column-name><column-value><![CDATA[");
		sb.append(getEstremiOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>estremiBolla</column-name><column-value><![CDATA[");
		sb.append(getEstremiBolla());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codicePagamento</column-name><column-value><![CDATA[");
		sb.append(getCodicePagamento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceAgente</column-name><column-value><![CDATA[");
		sb.append(getCodiceAgente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceGruppoAgenti</column-name><column-value><![CDATA[");
		sb.append(getCodiceGruppoAgenti());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>annotazioni</column-name><column-value><![CDATA[");
		sb.append(getAnnotazioni());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>scontoChiusura</column-name><column-value><![CDATA[");
		sb.append(getScontoChiusura());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>scontoProntaCassa</column-name><column-value><![CDATA[");
		sb.append(getScontoProntaCassa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>pertualeSpeseTrasp</column-name><column-value><![CDATA[");
		sb.append(getPertualeSpeseTrasp());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoSpeseTrasp</column-name><column-value><![CDATA[");
		sb.append(getImportoSpeseTrasp());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoSpeseImb</column-name><column-value><![CDATA[");
		sb.append(getImportoSpeseImb());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoSpeseVarie</column-name><column-value><![CDATA[");
		sb.append(getImportoSpeseVarie());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoSpeseBancarie</column-name><column-value><![CDATA[");
		sb.append(getImportoSpeseBancarie());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVATrasp</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVATrasp());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVAImb</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVAImb());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVAVarie</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVAVarie());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIVABancarie</column-name><column-value><![CDATA[");
		sb.append(getCodiceIVABancarie());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>totaleScontiCorpo</column-name><column-value><![CDATA[");
		sb.append(getTotaleScontiCorpo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>imponibile</column-name><column-value><![CDATA[");
		sb.append(getImponibile());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>IVA</column-name><column-value><![CDATA[");
		sb.append(getIVA());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoFattura</column-name><column-value><![CDATA[");
		sb.append(getImportoFattura());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr1</column-name><column-value><![CDATA[");
		sb.append(getLibStr1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr2</column-name><column-value><![CDATA[");
		sb.append(getLibStr2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr3</column-name><column-value><![CDATA[");
		sb.append(getLibStr3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl1</column-name><column-value><![CDATA[");
		sb.append(getLibDbl1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl2</column-name><column-value><![CDATA[");
		sb.append(getLibDbl2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl3</column-name><column-value><![CDATA[");
		sb.append(getLibDbl3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng1</column-name><column-value><![CDATA[");
		sb.append(getLibLng1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng2</column-name><column-value><![CDATA[");
		sb.append(getLibLng2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng3</column-name><column-value><![CDATA[");
		sb.append(getLibLng3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat1</column-name><column-value><![CDATA[");
		sb.append(getLibDat1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat2</column-name><column-value><![CDATA[");
		sb.append(getLibDat2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat3</column-name><column-value><![CDATA[");
		sb.append(getLibDat3());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _anno;
	private String _codiceAttivita;
	private String _codiceCentro;
	private int _numeroProtocollo;
	private String _tipoDocumento;
	private String _codiceCentroContAnalitica;
	private int _idTipoDocumentoOrigine;
	private boolean _statoFattura;
	private Date _dataRegistrazione;
	private Date _dataOperazione;
	private Date _dataAnnotazione;
	private Date _dataDocumento;
	private int _numeroDocumento;
	private int _riferimentoAScontrino;
	private String _descrizioneEstremiDocumento;
	private boolean _tipoSoggetto;
	private String _codiceCliente;
	private String _descrizioneAggiuntivaFattura;
	private String _estremiOrdine;
	private String _estremiBolla;
	private String _codicePagamento;
	private String _codiceAgente;
	private String _codiceGruppoAgenti;
	private String _annotazioni;
	private double _scontoChiusura;
	private double _scontoProntaCassa;
	private double _pertualeSpeseTrasp;
	private double _importoSpeseTrasp;
	private double _importoSpeseImb;
	private double _importoSpeseVarie;
	private double _importoSpeseBancarie;
	private String _codiceIVATrasp;
	private String _codiceIVAImb;
	private String _codiceIVAVarie;
	private String _codiceIVABancarie;
	private double _totaleScontiCorpo;
	private double _imponibile;
	private double _IVA;
	private double _importoFattura;
	private String _libStr1;
	private String _libStr2;
	private String _libStr3;
	private double _libDbl1;
	private double _libDbl2;
	private double _libDbl3;
	private long _libLng1;
	private long _libLng2;
	private long _libLng3;
	private Date _libDat1;
	private Date _libDat2;
	private Date _libDat3;
	private BaseModel<?> _testataFattureClientiRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}