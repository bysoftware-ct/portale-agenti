/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.EvasioniOrdiniServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.EvasioniOrdiniServiceSoap
 * @generated
 */
public class EvasioniOrdiniSoap implements Serializable {
	public static EvasioniOrdiniSoap toSoapModel(EvasioniOrdini model) {
		EvasioniOrdiniSoap soapModel = new EvasioniOrdiniSoap();

		soapModel.setID(model.getID());
		soapModel.setAnno(model.getAnno());
		soapModel.setCodiceAttivita(model.getCodiceAttivita());
		soapModel.setCodiceCentro(model.getCodiceCentro());
		soapModel.setCodiceDeposito(model.getCodiceDeposito());
		soapModel.setTipoOrdine(model.getTipoOrdine());
		soapModel.setNumeroOrdine(model.getNumeroOrdine());
		soapModel.setCodiceCliente(model.getCodiceCliente());
		soapModel.setNumeroRigo(model.getNumeroRigo());
		soapModel.setCodiceArticolo(model.getCodiceArticolo());
		soapModel.setCodiceVariante(model.getCodiceVariante());
		soapModel.setDescrizione(model.getDescrizione());
		soapModel.setUnitaMisura(model.getUnitaMisura());
		soapModel.setQuantitaOrdinata(model.getQuantitaOrdinata());
		soapModel.setNumeroProgrezzivoEvasione(model.getNumeroProgrezzivoEvasione());
		soapModel.setQuantitaConsegnata(model.getQuantitaConsegnata());
		soapModel.setDataDocumentoConsegna(model.getDataDocumentoConsegna());
		soapModel.setTestAccontoSaldo(model.getTestAccontoSaldo());
		soapModel.setTipoDocumentoEvasione(model.getTipoDocumentoEvasione());
		soapModel.setNumeroDocumentoEvasione(model.getNumeroDocumentoEvasione());

		return soapModel;
	}

	public static EvasioniOrdiniSoap[] toSoapModels(EvasioniOrdini[] models) {
		EvasioniOrdiniSoap[] soapModels = new EvasioniOrdiniSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EvasioniOrdiniSoap[][] toSoapModels(EvasioniOrdini[][] models) {
		EvasioniOrdiniSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EvasioniOrdiniSoap[models.length][models[0].length];
		}
		else {
			soapModels = new EvasioniOrdiniSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EvasioniOrdiniSoap[] toSoapModels(List<EvasioniOrdini> models) {
		List<EvasioniOrdiniSoap> soapModels = new ArrayList<EvasioniOrdiniSoap>(models.size());

		for (EvasioniOrdini model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EvasioniOrdiniSoap[soapModels.size()]);
	}

	public EvasioniOrdiniSoap() {
	}

	public String getPrimaryKey() {
		return _ID;
	}

	public void setPrimaryKey(String pk) {
		setID(pk);
	}

	public String getID() {
		return _ID;
	}

	public void setID(String ID) {
		_ID = ID;
	}

	public int getAnno() {
		return _anno;
	}

	public void setAnno(int anno) {
		_anno = anno;
	}

	public String getCodiceAttivita() {
		return _codiceAttivita;
	}

	public void setCodiceAttivita(String codiceAttivita) {
		_codiceAttivita = codiceAttivita;
	}

	public String getCodiceCentro() {
		return _codiceCentro;
	}

	public void setCodiceCentro(String codiceCentro) {
		_codiceCentro = codiceCentro;
	}

	public String getCodiceDeposito() {
		return _codiceDeposito;
	}

	public void setCodiceDeposito(String codiceDeposito) {
		_codiceDeposito = codiceDeposito;
	}

	public int getTipoOrdine() {
		return _tipoOrdine;
	}

	public void setTipoOrdine(int tipoOrdine) {
		_tipoOrdine = tipoOrdine;
	}

	public int getNumeroOrdine() {
		return _numeroOrdine;
	}

	public void setNumeroOrdine(int numeroOrdine) {
		_numeroOrdine = numeroOrdine;
	}

	public String getCodiceCliente() {
		return _codiceCliente;
	}

	public void setCodiceCliente(String codiceCliente) {
		_codiceCliente = codiceCliente;
	}

	public int getNumeroRigo() {
		return _numeroRigo;
	}

	public void setNumeroRigo(int numeroRigo) {
		_numeroRigo = numeroRigo;
	}

	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;
	}

	public String getCodiceVariante() {
		return _codiceVariante;
	}

	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;
	}

	public String getDescrizione() {
		return _descrizione;
	}

	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;
	}

	public String getUnitaMisura() {
		return _unitaMisura;
	}

	public void setUnitaMisura(String unitaMisura) {
		_unitaMisura = unitaMisura;
	}

	public double getQuantitaOrdinata() {
		return _quantitaOrdinata;
	}

	public void setQuantitaOrdinata(double quantitaOrdinata) {
		_quantitaOrdinata = quantitaOrdinata;
	}

	public int getNumeroProgrezzivoEvasione() {
		return _numeroProgrezzivoEvasione;
	}

	public void setNumeroProgrezzivoEvasione(int numeroProgrezzivoEvasione) {
		_numeroProgrezzivoEvasione = numeroProgrezzivoEvasione;
	}

	public double getQuantitaConsegnata() {
		return _quantitaConsegnata;
	}

	public void setQuantitaConsegnata(double quantitaConsegnata) {
		_quantitaConsegnata = quantitaConsegnata;
	}

	public Date getDataDocumentoConsegna() {
		return _dataDocumentoConsegna;
	}

	public void setDataDocumentoConsegna(Date dataDocumentoConsegna) {
		_dataDocumentoConsegna = dataDocumentoConsegna;
	}

	public String getTestAccontoSaldo() {
		return _testAccontoSaldo;
	}

	public void setTestAccontoSaldo(String testAccontoSaldo) {
		_testAccontoSaldo = testAccontoSaldo;
	}

	public String getTipoDocumentoEvasione() {
		return _tipoDocumentoEvasione;
	}

	public void setTipoDocumentoEvasione(String tipoDocumentoEvasione) {
		_tipoDocumentoEvasione = tipoDocumentoEvasione;
	}

	public int getNumeroDocumentoEvasione() {
		return _numeroDocumentoEvasione;
	}

	public void setNumeroDocumentoEvasione(int numeroDocumentoEvasione) {
		_numeroDocumentoEvasione = numeroDocumentoEvasione;
	}

	private String _ID;
	private int _anno;
	private String _codiceAttivita;
	private String _codiceCentro;
	private String _codiceDeposito;
	private int _tipoOrdine;
	private int _numeroOrdine;
	private String _codiceCliente;
	private int _numeroRigo;
	private String _codiceArticolo;
	private String _codiceVariante;
	private String _descrizione;
	private String _unitaMisura;
	private double _quantitaOrdinata;
	private int _numeroProgrezzivoEvasione;
	private double _quantitaConsegnata;
	private Date _dataDocumentoConsegna;
	private String _testAccontoSaldo;
	private String _tipoDocumentoEvasione;
	private int _numeroDocumentoEvasione;
}