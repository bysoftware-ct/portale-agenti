/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.EstrattoContoLocalServiceUtil;
import it.bysoftware.ct.service.persistence.EstrattoContoPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class EstrattoContoClp extends BaseModelImpl<EstrattoConto>
	implements EstrattoConto {
	public EstrattoContoClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return EstrattoConto.class;
	}

	@Override
	public String getModelClassName() {
		return EstrattoConto.class.getName();
	}

	@Override
	public EstrattoContoPK getPrimaryKey() {
		return new EstrattoContoPK(_tipoSoggetto, _codiceCliente,
			_esercizioRegistrazione, _numeroPartita, _numeroScadenza);
	}

	@Override
	public void setPrimaryKey(EstrattoContoPK primaryKey) {
		setTipoSoggetto(primaryKey.tipoSoggetto);
		setCodiceCliente(primaryKey.codiceCliente);
		setEsercizioRegistrazione(primaryKey.esercizioRegistrazione);
		setNumeroPartita(primaryKey.numeroPartita);
		setNumeroScadenza(primaryKey.numeroScadenza);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new EstrattoContoPK(_tipoSoggetto, _codiceCliente,
			_esercizioRegistrazione, _numeroPartita, _numeroScadenza);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((EstrattoContoPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceCliente", getCodiceCliente());
		attributes.put("esercizioRegistrazione", getEsercizioRegistrazione());
		attributes.put("numeroPartita", getNumeroPartita());
		attributes.put("numeroScadenza", getNumeroScadenza());
		attributes.put("dataScadenza", getDataScadenza());
		attributes.put("codiceTipoPagam", getCodiceTipoPagam());
		attributes.put("stato", getStato());
		attributes.put("codiceBanca", getCodiceBanca());
		attributes.put("codiceContoCorrente", getCodiceContoCorrente());
		attributes.put("codiceAgente", getCodiceAgente());
		attributes.put("tipoCausale", getTipoCausale());
		attributes.put("esercizioDocumento", getEsercizioDocumento());
		attributes.put("protocolloDocumento", getProtocolloDocumento());
		attributes.put("codiceAttivita", getCodiceAttivita());
		attributes.put("codiceCentro", getCodiceCentro());
		attributes.put("dataRegistrazione", getDataRegistrazione());
		attributes.put("dataOperazione", getDataOperazione());
		attributes.put("dataAggiornamento", getDataAggiornamento());
		attributes.put("dataChiusura", getDataChiusura());
		attributes.put("codiceCausale", getCodiceCausale());
		attributes.put("dataDocumento", getDataDocumento());
		attributes.put("numeroDocumento", getNumeroDocumento());
		attributes.put("codiceTipoPagamOrig", getCodiceTipoPagamOrig());
		attributes.put("codiceTipoPagamScad", getCodiceTipoPagamScad());
		attributes.put("causaleEstrattoContoOrig", getCausaleEstrattoContoOrig());
		attributes.put("codiceDivisaOrig", getCodiceDivisaOrig());
		attributes.put("importoTotaleInt", getImportoTotaleInt());
		attributes.put("importoPagatoInt", getImportoPagatoInt());
		attributes.put("importoApertoInt", getImportoApertoInt());
		attributes.put("importoEspostoInt", getImportoEspostoInt());
		attributes.put("importoScadutoInt", getImportoScadutoInt());
		attributes.put("importoInsolutoInt", getImportoInsolutoInt());
		attributes.put("importoInContenziosoInt", getImportoInContenziosoInt());
		attributes.put("importoTotale", getImportoTotale());
		attributes.put("importoPagato", getImportoPagato());
		attributes.put("importoAperto", getImportoAperto());
		attributes.put("importoEsposto", getImportoEsposto());
		attributes.put("importoScaduto", getImportoScaduto());
		attributes.put("importoInsoluto", getImportoInsoluto());
		attributes.put("importoInContenzioso", getImportoInContenzioso());
		attributes.put("numeroLettereSollecito", getNumeroLettereSollecito());
		attributes.put("codiceDivisaInterna", getCodiceDivisaInterna());
		attributes.put("codiceEsercizioScadenzaConversione",
			getCodiceEsercizioScadenzaConversione());
		attributes.put("numeroPartitaScadenzaCoversione",
			getNumeroPartitaScadenzaCoversione());
		attributes.put("numeroScadenzaCoversione", getNumeroScadenzaCoversione());
		attributes.put("tipoCoversione", getTipoCoversione());
		attributes.put("codiceEsercizioScritturaConversione",
			getCodiceEsercizioScritturaConversione());
		attributes.put("numeroRegistrazioneScritturaConversione",
			getNumeroRegistrazioneScritturaConversione());
		attributes.put("dataRegistrazioneScritturaConversione",
			getDataRegistrazioneScritturaConversione());
		attributes.put("codiceDivisaIntScadenzaConversione",
			getCodiceDivisaIntScadenzaConversione());
		attributes.put("codiceDivisaScadenzaConversione",
			getCodiceDivisaScadenzaConversione());
		attributes.put("cambioScadenzaConversione",
			getCambioScadenzaConversione());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceCliente = (String)attributes.get("codiceCliente");

		if (codiceCliente != null) {
			setCodiceCliente(codiceCliente);
		}

		Integer esercizioRegistrazione = (Integer)attributes.get(
				"esercizioRegistrazione");

		if (esercizioRegistrazione != null) {
			setEsercizioRegistrazione(esercizioRegistrazione);
		}

		Integer numeroPartita = (Integer)attributes.get("numeroPartita");

		if (numeroPartita != null) {
			setNumeroPartita(numeroPartita);
		}

		Integer numeroScadenza = (Integer)attributes.get("numeroScadenza");

		if (numeroScadenza != null) {
			setNumeroScadenza(numeroScadenza);
		}

		Date dataScadenza = (Date)attributes.get("dataScadenza");

		if (dataScadenza != null) {
			setDataScadenza(dataScadenza);
		}

		Integer codiceTipoPagam = (Integer)attributes.get("codiceTipoPagam");

		if (codiceTipoPagam != null) {
			setCodiceTipoPagam(codiceTipoPagam);
		}

		Integer stato = (Integer)attributes.get("stato");

		if (stato != null) {
			setStato(stato);
		}

		String codiceBanca = (String)attributes.get("codiceBanca");

		if (codiceBanca != null) {
			setCodiceBanca(codiceBanca);
		}

		String codiceContoCorrente = (String)attributes.get(
				"codiceContoCorrente");

		if (codiceContoCorrente != null) {
			setCodiceContoCorrente(codiceContoCorrente);
		}

		String codiceAgente = (String)attributes.get("codiceAgente");

		if (codiceAgente != null) {
			setCodiceAgente(codiceAgente);
		}

		String tipoCausale = (String)attributes.get("tipoCausale");

		if (tipoCausale != null) {
			setTipoCausale(tipoCausale);
		}

		Integer esercizioDocumento = (Integer)attributes.get(
				"esercizioDocumento");

		if (esercizioDocumento != null) {
			setEsercizioDocumento(esercizioDocumento);
		}

		Integer protocolloDocumento = (Integer)attributes.get(
				"protocolloDocumento");

		if (protocolloDocumento != null) {
			setProtocolloDocumento(protocolloDocumento);
		}

		String codiceAttivita = (String)attributes.get("codiceAttivita");

		if (codiceAttivita != null) {
			setCodiceAttivita(codiceAttivita);
		}

		String codiceCentro = (String)attributes.get("codiceCentro");

		if (codiceCentro != null) {
			setCodiceCentro(codiceCentro);
		}

		Date dataRegistrazione = (Date)attributes.get("dataRegistrazione");

		if (dataRegistrazione != null) {
			setDataRegistrazione(dataRegistrazione);
		}

		Date dataOperazione = (Date)attributes.get("dataOperazione");

		if (dataOperazione != null) {
			setDataOperazione(dataOperazione);
		}

		Date dataAggiornamento = (Date)attributes.get("dataAggiornamento");

		if (dataAggiornamento != null) {
			setDataAggiornamento(dataAggiornamento);
		}

		Date dataChiusura = (Date)attributes.get("dataChiusura");

		if (dataChiusura != null) {
			setDataChiusura(dataChiusura);
		}

		String codiceCausale = (String)attributes.get("codiceCausale");

		if (codiceCausale != null) {
			setCodiceCausale(codiceCausale);
		}

		Date dataDocumento = (Date)attributes.get("dataDocumento");

		if (dataDocumento != null) {
			setDataDocumento(dataDocumento);
		}

		Integer numeroDocumento = (Integer)attributes.get("numeroDocumento");

		if (numeroDocumento != null) {
			setNumeroDocumento(numeroDocumento);
		}

		String codiceTipoPagamOrig = (String)attributes.get(
				"codiceTipoPagamOrig");

		if (codiceTipoPagamOrig != null) {
			setCodiceTipoPagamOrig(codiceTipoPagamOrig);
		}

		String codiceTipoPagamScad = (String)attributes.get(
				"codiceTipoPagamScad");

		if (codiceTipoPagamScad != null) {
			setCodiceTipoPagamScad(codiceTipoPagamScad);
		}

		String causaleEstrattoContoOrig = (String)attributes.get(
				"causaleEstrattoContoOrig");

		if (causaleEstrattoContoOrig != null) {
			setCausaleEstrattoContoOrig(causaleEstrattoContoOrig);
		}

		String codiceDivisaOrig = (String)attributes.get("codiceDivisaOrig");

		if (codiceDivisaOrig != null) {
			setCodiceDivisaOrig(codiceDivisaOrig);
		}

		Double importoTotaleInt = (Double)attributes.get("importoTotaleInt");

		if (importoTotaleInt != null) {
			setImportoTotaleInt(importoTotaleInt);
		}

		Double importoPagatoInt = (Double)attributes.get("importoPagatoInt");

		if (importoPagatoInt != null) {
			setImportoPagatoInt(importoPagatoInt);
		}

		Double importoApertoInt = (Double)attributes.get("importoApertoInt");

		if (importoApertoInt != null) {
			setImportoApertoInt(importoApertoInt);
		}

		Double importoEspostoInt = (Double)attributes.get("importoEspostoInt");

		if (importoEspostoInt != null) {
			setImportoEspostoInt(importoEspostoInt);
		}

		Double importoScadutoInt = (Double)attributes.get("importoScadutoInt");

		if (importoScadutoInt != null) {
			setImportoScadutoInt(importoScadutoInt);
		}

		Double importoInsolutoInt = (Double)attributes.get("importoInsolutoInt");

		if (importoInsolutoInt != null) {
			setImportoInsolutoInt(importoInsolutoInt);
		}

		Double importoInContenziosoInt = (Double)attributes.get(
				"importoInContenziosoInt");

		if (importoInContenziosoInt != null) {
			setImportoInContenziosoInt(importoInContenziosoInt);
		}

		Double importoTotale = (Double)attributes.get("importoTotale");

		if (importoTotale != null) {
			setImportoTotale(importoTotale);
		}

		Double importoPagato = (Double)attributes.get("importoPagato");

		if (importoPagato != null) {
			setImportoPagato(importoPagato);
		}

		Double importoAperto = (Double)attributes.get("importoAperto");

		if (importoAperto != null) {
			setImportoAperto(importoAperto);
		}

		Double importoEsposto = (Double)attributes.get("importoEsposto");

		if (importoEsposto != null) {
			setImportoEsposto(importoEsposto);
		}

		Double importoScaduto = (Double)attributes.get("importoScaduto");

		if (importoScaduto != null) {
			setImportoScaduto(importoScaduto);
		}

		Double importoInsoluto = (Double)attributes.get("importoInsoluto");

		if (importoInsoluto != null) {
			setImportoInsoluto(importoInsoluto);
		}

		Double importoInContenzioso = (Double)attributes.get(
				"importoInContenzioso");

		if (importoInContenzioso != null) {
			setImportoInContenzioso(importoInContenzioso);
		}

		Integer numeroLettereSollecito = (Integer)attributes.get(
				"numeroLettereSollecito");

		if (numeroLettereSollecito != null) {
			setNumeroLettereSollecito(numeroLettereSollecito);
		}

		String codiceDivisaInterna = (String)attributes.get(
				"codiceDivisaInterna");

		if (codiceDivisaInterna != null) {
			setCodiceDivisaInterna(codiceDivisaInterna);
		}

		Integer codiceEsercizioScadenzaConversione = (Integer)attributes.get(
				"codiceEsercizioScadenzaConversione");

		if (codiceEsercizioScadenzaConversione != null) {
			setCodiceEsercizioScadenzaConversione(codiceEsercizioScadenzaConversione);
		}

		Integer numeroPartitaScadenzaCoversione = (Integer)attributes.get(
				"numeroPartitaScadenzaCoversione");

		if (numeroPartitaScadenzaCoversione != null) {
			setNumeroPartitaScadenzaCoversione(numeroPartitaScadenzaCoversione);
		}

		Integer numeroScadenzaCoversione = (Integer)attributes.get(
				"numeroScadenzaCoversione");

		if (numeroScadenzaCoversione != null) {
			setNumeroScadenzaCoversione(numeroScadenzaCoversione);
		}

		Integer tipoCoversione = (Integer)attributes.get("tipoCoversione");

		if (tipoCoversione != null) {
			setTipoCoversione(tipoCoversione);
		}

		Integer codiceEsercizioScritturaConversione = (Integer)attributes.get(
				"codiceEsercizioScritturaConversione");

		if (codiceEsercizioScritturaConversione != null) {
			setCodiceEsercizioScritturaConversione(codiceEsercizioScritturaConversione);
		}

		Integer numeroRegistrazioneScritturaConversione = (Integer)attributes.get(
				"numeroRegistrazioneScritturaConversione");

		if (numeroRegistrazioneScritturaConversione != null) {
			setNumeroRegistrazioneScritturaConversione(numeroRegistrazioneScritturaConversione);
		}

		Date dataRegistrazioneScritturaConversione = (Date)attributes.get(
				"dataRegistrazioneScritturaConversione");

		if (dataRegistrazioneScritturaConversione != null) {
			setDataRegistrazioneScritturaConversione(dataRegistrazioneScritturaConversione);
		}

		String codiceDivisaIntScadenzaConversione = (String)attributes.get(
				"codiceDivisaIntScadenzaConversione");

		if (codiceDivisaIntScadenzaConversione != null) {
			setCodiceDivisaIntScadenzaConversione(codiceDivisaIntScadenzaConversione);
		}

		String codiceDivisaScadenzaConversione = (String)attributes.get(
				"codiceDivisaScadenzaConversione");

		if (codiceDivisaScadenzaConversione != null) {
			setCodiceDivisaScadenzaConversione(codiceDivisaScadenzaConversione);
		}

		Double cambioScadenzaConversione = (Double)attributes.get(
				"cambioScadenzaConversione");

		if (cambioScadenzaConversione != null) {
			setCambioScadenzaConversione(cambioScadenzaConversione);
		}
	}

	@Override
	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoSoggetto", boolean.class);

				method.invoke(_estrattoContoRemoteModel, tipoSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCliente() {
		return _codiceCliente;
	}

	@Override
	public void setCodiceCliente(String codiceCliente) {
		_codiceCliente = codiceCliente;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCliente", String.class);

				method.invoke(_estrattoContoRemoteModel, codiceCliente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getEsercizioRegistrazione() {
		return _esercizioRegistrazione;
	}

	@Override
	public void setEsercizioRegistrazione(int esercizioRegistrazione) {
		_esercizioRegistrazione = esercizioRegistrazione;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setEsercizioRegistrazione",
						int.class);

				method.invoke(_estrattoContoRemoteModel, esercizioRegistrazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroPartita() {
		return _numeroPartita;
	}

	@Override
	public void setNumeroPartita(int numeroPartita) {
		_numeroPartita = numeroPartita;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroPartita", int.class);

				method.invoke(_estrattoContoRemoteModel, numeroPartita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroScadenza() {
		return _numeroScadenza;
	}

	@Override
	public void setNumeroScadenza(int numeroScadenza) {
		_numeroScadenza = numeroScadenza;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroScadenza", int.class);

				method.invoke(_estrattoContoRemoteModel, numeroScadenza);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataScadenza() {
		return _dataScadenza;
	}

	@Override
	public void setDataScadenza(Date dataScadenza) {
		_dataScadenza = dataScadenza;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setDataScadenza", Date.class);

				method.invoke(_estrattoContoRemoteModel, dataScadenza);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getCodiceTipoPagam() {
		return _codiceTipoPagam;
	}

	@Override
	public void setCodiceTipoPagam(int codiceTipoPagam) {
		_codiceTipoPagam = codiceTipoPagam;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceTipoPagam", int.class);

				method.invoke(_estrattoContoRemoteModel, codiceTipoPagam);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getStato() {
		return _stato;
	}

	@Override
	public void setStato(int stato) {
		_stato = stato;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setStato", int.class);

				method.invoke(_estrattoContoRemoteModel, stato);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceBanca() {
		return _codiceBanca;
	}

	@Override
	public void setCodiceBanca(String codiceBanca) {
		_codiceBanca = codiceBanca;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceBanca", String.class);

				method.invoke(_estrattoContoRemoteModel, codiceBanca);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceContoCorrente() {
		return _codiceContoCorrente;
	}

	@Override
	public void setCodiceContoCorrente(String codiceContoCorrente) {
		_codiceContoCorrente = codiceContoCorrente;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceContoCorrente",
						String.class);

				method.invoke(_estrattoContoRemoteModel, codiceContoCorrente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceAgente() {
		return _codiceAgente;
	}

	@Override
	public void setCodiceAgente(String codiceAgente) {
		_codiceAgente = codiceAgente;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceAgente", String.class);

				method.invoke(_estrattoContoRemoteModel, codiceAgente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoCausale() {
		return _tipoCausale;
	}

	@Override
	public void setTipoCausale(String tipoCausale) {
		_tipoCausale = tipoCausale;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoCausale", String.class);

				method.invoke(_estrattoContoRemoteModel, tipoCausale);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getEsercizioDocumento() {
		return _esercizioDocumento;
	}

	@Override
	public void setEsercizioDocumento(int esercizioDocumento) {
		_esercizioDocumento = esercizioDocumento;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setEsercizioDocumento",
						int.class);

				method.invoke(_estrattoContoRemoteModel, esercizioDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getProtocolloDocumento() {
		return _protocolloDocumento;
	}

	@Override
	public void setProtocolloDocumento(int protocolloDocumento) {
		_protocolloDocumento = protocolloDocumento;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setProtocolloDocumento",
						int.class);

				method.invoke(_estrattoContoRemoteModel, protocolloDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceAttivita() {
		return _codiceAttivita;
	}

	@Override
	public void setCodiceAttivita(String codiceAttivita) {
		_codiceAttivita = codiceAttivita;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceAttivita",
						String.class);

				method.invoke(_estrattoContoRemoteModel, codiceAttivita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCentro() {
		return _codiceCentro;
	}

	@Override
	public void setCodiceCentro(String codiceCentro) {
		_codiceCentro = codiceCentro;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCentro", String.class);

				method.invoke(_estrattoContoRemoteModel, codiceCentro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataRegistrazione() {
		return _dataRegistrazione;
	}

	@Override
	public void setDataRegistrazione(Date dataRegistrazione) {
		_dataRegistrazione = dataRegistrazione;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setDataRegistrazione",
						Date.class);

				method.invoke(_estrattoContoRemoteModel, dataRegistrazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataOperazione() {
		return _dataOperazione;
	}

	@Override
	public void setDataOperazione(Date dataOperazione) {
		_dataOperazione = dataOperazione;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setDataOperazione", Date.class);

				method.invoke(_estrattoContoRemoteModel, dataOperazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataAggiornamento() {
		return _dataAggiornamento;
	}

	@Override
	public void setDataAggiornamento(Date dataAggiornamento) {
		_dataAggiornamento = dataAggiornamento;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setDataAggiornamento",
						Date.class);

				method.invoke(_estrattoContoRemoteModel, dataAggiornamento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataChiusura() {
		return _dataChiusura;
	}

	@Override
	public void setDataChiusura(Date dataChiusura) {
		_dataChiusura = dataChiusura;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setDataChiusura", Date.class);

				method.invoke(_estrattoContoRemoteModel, dataChiusura);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCausale() {
		return _codiceCausale;
	}

	@Override
	public void setCodiceCausale(String codiceCausale) {
		_codiceCausale = codiceCausale;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCausale", String.class);

				method.invoke(_estrattoContoRemoteModel, codiceCausale);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataDocumento() {
		return _dataDocumento;
	}

	@Override
	public void setDataDocumento(Date dataDocumento) {
		_dataDocumento = dataDocumento;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setDataDocumento", Date.class);

				method.invoke(_estrattoContoRemoteModel, dataDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroDocumento() {
		return _numeroDocumento;
	}

	@Override
	public void setNumeroDocumento(int numeroDocumento) {
		_numeroDocumento = numeroDocumento;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroDocumento", int.class);

				method.invoke(_estrattoContoRemoteModel, numeroDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceTipoPagamOrig() {
		return _codiceTipoPagamOrig;
	}

	@Override
	public void setCodiceTipoPagamOrig(String codiceTipoPagamOrig) {
		_codiceTipoPagamOrig = codiceTipoPagamOrig;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceTipoPagamOrig",
						String.class);

				method.invoke(_estrattoContoRemoteModel, codiceTipoPagamOrig);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceTipoPagamScad() {
		return _codiceTipoPagamScad;
	}

	@Override
	public void setCodiceTipoPagamScad(String codiceTipoPagamScad) {
		_codiceTipoPagamScad = codiceTipoPagamScad;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceTipoPagamScad",
						String.class);

				method.invoke(_estrattoContoRemoteModel, codiceTipoPagamScad);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCausaleEstrattoContoOrig() {
		return _causaleEstrattoContoOrig;
	}

	@Override
	public void setCausaleEstrattoContoOrig(String causaleEstrattoContoOrig) {
		_causaleEstrattoContoOrig = causaleEstrattoContoOrig;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCausaleEstrattoContoOrig",
						String.class);

				method.invoke(_estrattoContoRemoteModel,
					causaleEstrattoContoOrig);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDivisaOrig() {
		return _codiceDivisaOrig;
	}

	@Override
	public void setCodiceDivisaOrig(String codiceDivisaOrig) {
		_codiceDivisaOrig = codiceDivisaOrig;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDivisaOrig",
						String.class);

				method.invoke(_estrattoContoRemoteModel, codiceDivisaOrig);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoTotaleInt() {
		return _importoTotaleInt;
	}

	@Override
	public void setImportoTotaleInt(double importoTotaleInt) {
		_importoTotaleInt = importoTotaleInt;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoTotaleInt",
						double.class);

				method.invoke(_estrattoContoRemoteModel, importoTotaleInt);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoPagatoInt() {
		return _importoPagatoInt;
	}

	@Override
	public void setImportoPagatoInt(double importoPagatoInt) {
		_importoPagatoInt = importoPagatoInt;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoPagatoInt",
						double.class);

				method.invoke(_estrattoContoRemoteModel, importoPagatoInt);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoApertoInt() {
		return _importoApertoInt;
	}

	@Override
	public void setImportoApertoInt(double importoApertoInt) {
		_importoApertoInt = importoApertoInt;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoApertoInt",
						double.class);

				method.invoke(_estrattoContoRemoteModel, importoApertoInt);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoEspostoInt() {
		return _importoEspostoInt;
	}

	@Override
	public void setImportoEspostoInt(double importoEspostoInt) {
		_importoEspostoInt = importoEspostoInt;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoEspostoInt",
						double.class);

				method.invoke(_estrattoContoRemoteModel, importoEspostoInt);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoScadutoInt() {
		return _importoScadutoInt;
	}

	@Override
	public void setImportoScadutoInt(double importoScadutoInt) {
		_importoScadutoInt = importoScadutoInt;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoScadutoInt",
						double.class);

				method.invoke(_estrattoContoRemoteModel, importoScadutoInt);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoInsolutoInt() {
		return _importoInsolutoInt;
	}

	@Override
	public void setImportoInsolutoInt(double importoInsolutoInt) {
		_importoInsolutoInt = importoInsolutoInt;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoInsolutoInt",
						double.class);

				method.invoke(_estrattoContoRemoteModel, importoInsolutoInt);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoInContenziosoInt() {
		return _importoInContenziosoInt;
	}

	@Override
	public void setImportoInContenziosoInt(double importoInContenziosoInt) {
		_importoInContenziosoInt = importoInContenziosoInt;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoInContenziosoInt",
						double.class);

				method.invoke(_estrattoContoRemoteModel, importoInContenziosoInt);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoTotale() {
		return _importoTotale;
	}

	@Override
	public void setImportoTotale(double importoTotale) {
		_importoTotale = importoTotale;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoTotale", double.class);

				method.invoke(_estrattoContoRemoteModel, importoTotale);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoPagato() {
		return _importoPagato;
	}

	@Override
	public void setImportoPagato(double importoPagato) {
		_importoPagato = importoPagato;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoPagato", double.class);

				method.invoke(_estrattoContoRemoteModel, importoPagato);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoAperto() {
		return _importoAperto;
	}

	@Override
	public void setImportoAperto(double importoAperto) {
		_importoAperto = importoAperto;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoAperto", double.class);

				method.invoke(_estrattoContoRemoteModel, importoAperto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoEsposto() {
		return _importoEsposto;
	}

	@Override
	public void setImportoEsposto(double importoEsposto) {
		_importoEsposto = importoEsposto;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoEsposto",
						double.class);

				method.invoke(_estrattoContoRemoteModel, importoEsposto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoScaduto() {
		return _importoScaduto;
	}

	@Override
	public void setImportoScaduto(double importoScaduto) {
		_importoScaduto = importoScaduto;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoScaduto",
						double.class);

				method.invoke(_estrattoContoRemoteModel, importoScaduto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoInsoluto() {
		return _importoInsoluto;
	}

	@Override
	public void setImportoInsoluto(double importoInsoluto) {
		_importoInsoluto = importoInsoluto;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoInsoluto",
						double.class);

				method.invoke(_estrattoContoRemoteModel, importoInsoluto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getImportoInContenzioso() {
		return _importoInContenzioso;
	}

	@Override
	public void setImportoInContenzioso(double importoInContenzioso) {
		_importoInContenzioso = importoInContenzioso;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setImportoInContenzioso",
						double.class);

				method.invoke(_estrattoContoRemoteModel, importoInContenzioso);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroLettereSollecito() {
		return _numeroLettereSollecito;
	}

	@Override
	public void setNumeroLettereSollecito(int numeroLettereSollecito) {
		_numeroLettereSollecito = numeroLettereSollecito;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroLettereSollecito",
						int.class);

				method.invoke(_estrattoContoRemoteModel, numeroLettereSollecito);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDivisaInterna() {
		return _codiceDivisaInterna;
	}

	@Override
	public void setCodiceDivisaInterna(String codiceDivisaInterna) {
		_codiceDivisaInterna = codiceDivisaInterna;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDivisaInterna",
						String.class);

				method.invoke(_estrattoContoRemoteModel, codiceDivisaInterna);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getCodiceEsercizioScadenzaConversione() {
		return _codiceEsercizioScadenzaConversione;
	}

	@Override
	public void setCodiceEsercizioScadenzaConversione(
		int codiceEsercizioScadenzaConversione) {
		_codiceEsercizioScadenzaConversione = codiceEsercizioScadenzaConversione;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceEsercizioScadenzaConversione",
						int.class);

				method.invoke(_estrattoContoRemoteModel,
					codiceEsercizioScadenzaConversione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroPartitaScadenzaCoversione() {
		return _numeroPartitaScadenzaCoversione;
	}

	@Override
	public void setNumeroPartitaScadenzaCoversione(
		int numeroPartitaScadenzaCoversione) {
		_numeroPartitaScadenzaCoversione = numeroPartitaScadenzaCoversione;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroPartitaScadenzaCoversione",
						int.class);

				method.invoke(_estrattoContoRemoteModel,
					numeroPartitaScadenzaCoversione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroScadenzaCoversione() {
		return _numeroScadenzaCoversione;
	}

	@Override
	public void setNumeroScadenzaCoversione(int numeroScadenzaCoversione) {
		_numeroScadenzaCoversione = numeroScadenzaCoversione;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroScadenzaCoversione",
						int.class);

				method.invoke(_estrattoContoRemoteModel,
					numeroScadenzaCoversione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoCoversione() {
		return _tipoCoversione;
	}

	@Override
	public void setTipoCoversione(int tipoCoversione) {
		_tipoCoversione = tipoCoversione;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoCoversione", int.class);

				method.invoke(_estrattoContoRemoteModel, tipoCoversione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getCodiceEsercizioScritturaConversione() {
		return _codiceEsercizioScritturaConversione;
	}

	@Override
	public void setCodiceEsercizioScritturaConversione(
		int codiceEsercizioScritturaConversione) {
		_codiceEsercizioScritturaConversione = codiceEsercizioScritturaConversione;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceEsercizioScritturaConversione",
						int.class);

				method.invoke(_estrattoContoRemoteModel,
					codiceEsercizioScritturaConversione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroRegistrazioneScritturaConversione() {
		return _numeroRegistrazioneScritturaConversione;
	}

	@Override
	public void setNumeroRegistrazioneScritturaConversione(
		int numeroRegistrazioneScritturaConversione) {
		_numeroRegistrazioneScritturaConversione = numeroRegistrazioneScritturaConversione;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroRegistrazioneScritturaConversione",
						int.class);

				method.invoke(_estrattoContoRemoteModel,
					numeroRegistrazioneScritturaConversione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataRegistrazioneScritturaConversione() {
		return _dataRegistrazioneScritturaConversione;
	}

	@Override
	public void setDataRegistrazioneScritturaConversione(
		Date dataRegistrazioneScritturaConversione) {
		_dataRegistrazioneScritturaConversione = dataRegistrazioneScritturaConversione;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setDataRegistrazioneScritturaConversione",
						Date.class);

				method.invoke(_estrattoContoRemoteModel,
					dataRegistrazioneScritturaConversione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDivisaIntScadenzaConversione() {
		return _codiceDivisaIntScadenzaConversione;
	}

	@Override
	public void setCodiceDivisaIntScadenzaConversione(
		String codiceDivisaIntScadenzaConversione) {
		_codiceDivisaIntScadenzaConversione = codiceDivisaIntScadenzaConversione;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDivisaIntScadenzaConversione",
						String.class);

				method.invoke(_estrattoContoRemoteModel,
					codiceDivisaIntScadenzaConversione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDivisaScadenzaConversione() {
		return _codiceDivisaScadenzaConversione;
	}

	@Override
	public void setCodiceDivisaScadenzaConversione(
		String codiceDivisaScadenzaConversione) {
		_codiceDivisaScadenzaConversione = codiceDivisaScadenzaConversione;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDivisaScadenzaConversione",
						String.class);

				method.invoke(_estrattoContoRemoteModel,
					codiceDivisaScadenzaConversione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getCambioScadenzaConversione() {
		return _cambioScadenzaConversione;
	}

	@Override
	public void setCambioScadenzaConversione(double cambioScadenzaConversione) {
		_cambioScadenzaConversione = cambioScadenzaConversione;

		if (_estrattoContoRemoteModel != null) {
			try {
				Class<?> clazz = _estrattoContoRemoteModel.getClass();

				Method method = clazz.getMethod("setCambioScadenzaConversione",
						double.class);

				method.invoke(_estrattoContoRemoteModel,
					cambioScadenzaConversione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getEstrattoContoRemoteModel() {
		return _estrattoContoRemoteModel;
	}

	public void setEstrattoContoRemoteModel(
		BaseModel<?> estrattoContoRemoteModel) {
		_estrattoContoRemoteModel = estrattoContoRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _estrattoContoRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_estrattoContoRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			EstrattoContoLocalServiceUtil.addEstrattoConto(this);
		}
		else {
			EstrattoContoLocalServiceUtil.updateEstrattoConto(this);
		}
	}

	@Override
	public EstrattoConto toEscapedModel() {
		return (EstrattoConto)ProxyUtil.newProxyInstance(EstrattoConto.class.getClassLoader(),
			new Class[] { EstrattoConto.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		EstrattoContoClp clone = new EstrattoContoClp();

		clone.setTipoSoggetto(getTipoSoggetto());
		clone.setCodiceCliente(getCodiceCliente());
		clone.setEsercizioRegistrazione(getEsercizioRegistrazione());
		clone.setNumeroPartita(getNumeroPartita());
		clone.setNumeroScadenza(getNumeroScadenza());
		clone.setDataScadenza(getDataScadenza());
		clone.setCodiceTipoPagam(getCodiceTipoPagam());
		clone.setStato(getStato());
		clone.setCodiceBanca(getCodiceBanca());
		clone.setCodiceContoCorrente(getCodiceContoCorrente());
		clone.setCodiceAgente(getCodiceAgente());
		clone.setTipoCausale(getTipoCausale());
		clone.setEsercizioDocumento(getEsercizioDocumento());
		clone.setProtocolloDocumento(getProtocolloDocumento());
		clone.setCodiceAttivita(getCodiceAttivita());
		clone.setCodiceCentro(getCodiceCentro());
		clone.setDataRegistrazione(getDataRegistrazione());
		clone.setDataOperazione(getDataOperazione());
		clone.setDataAggiornamento(getDataAggiornamento());
		clone.setDataChiusura(getDataChiusura());
		clone.setCodiceCausale(getCodiceCausale());
		clone.setDataDocumento(getDataDocumento());
		clone.setNumeroDocumento(getNumeroDocumento());
		clone.setCodiceTipoPagamOrig(getCodiceTipoPagamOrig());
		clone.setCodiceTipoPagamScad(getCodiceTipoPagamScad());
		clone.setCausaleEstrattoContoOrig(getCausaleEstrattoContoOrig());
		clone.setCodiceDivisaOrig(getCodiceDivisaOrig());
		clone.setImportoTotaleInt(getImportoTotaleInt());
		clone.setImportoPagatoInt(getImportoPagatoInt());
		clone.setImportoApertoInt(getImportoApertoInt());
		clone.setImportoEspostoInt(getImportoEspostoInt());
		clone.setImportoScadutoInt(getImportoScadutoInt());
		clone.setImportoInsolutoInt(getImportoInsolutoInt());
		clone.setImportoInContenziosoInt(getImportoInContenziosoInt());
		clone.setImportoTotale(getImportoTotale());
		clone.setImportoPagato(getImportoPagato());
		clone.setImportoAperto(getImportoAperto());
		clone.setImportoEsposto(getImportoEsposto());
		clone.setImportoScaduto(getImportoScaduto());
		clone.setImportoInsoluto(getImportoInsoluto());
		clone.setImportoInContenzioso(getImportoInContenzioso());
		clone.setNumeroLettereSollecito(getNumeroLettereSollecito());
		clone.setCodiceDivisaInterna(getCodiceDivisaInterna());
		clone.setCodiceEsercizioScadenzaConversione(getCodiceEsercizioScadenzaConversione());
		clone.setNumeroPartitaScadenzaCoversione(getNumeroPartitaScadenzaCoversione());
		clone.setNumeroScadenzaCoversione(getNumeroScadenzaCoversione());
		clone.setTipoCoversione(getTipoCoversione());
		clone.setCodiceEsercizioScritturaConversione(getCodiceEsercizioScritturaConversione());
		clone.setNumeroRegistrazioneScritturaConversione(getNumeroRegistrazioneScritturaConversione());
		clone.setDataRegistrazioneScritturaConversione(getDataRegistrazioneScritturaConversione());
		clone.setCodiceDivisaIntScadenzaConversione(getCodiceDivisaIntScadenzaConversione());
		clone.setCodiceDivisaScadenzaConversione(getCodiceDivisaScadenzaConversione());
		clone.setCambioScadenzaConversione(getCambioScadenzaConversione());

		return clone;
	}

	@Override
	public int compareTo(EstrattoConto estrattoConto) {
		EstrattoContoPK primaryKey = estrattoConto.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EstrattoContoClp)) {
			return false;
		}

		EstrattoContoClp estrattoConto = (EstrattoContoClp)obj;

		EstrattoContoPK primaryKey = estrattoConto.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(107);

		sb.append("{tipoSoggetto=");
		sb.append(getTipoSoggetto());
		sb.append(", codiceCliente=");
		sb.append(getCodiceCliente());
		sb.append(", esercizioRegistrazione=");
		sb.append(getEsercizioRegistrazione());
		sb.append(", numeroPartita=");
		sb.append(getNumeroPartita());
		sb.append(", numeroScadenza=");
		sb.append(getNumeroScadenza());
		sb.append(", dataScadenza=");
		sb.append(getDataScadenza());
		sb.append(", codiceTipoPagam=");
		sb.append(getCodiceTipoPagam());
		sb.append(", stato=");
		sb.append(getStato());
		sb.append(", codiceBanca=");
		sb.append(getCodiceBanca());
		sb.append(", codiceContoCorrente=");
		sb.append(getCodiceContoCorrente());
		sb.append(", codiceAgente=");
		sb.append(getCodiceAgente());
		sb.append(", tipoCausale=");
		sb.append(getTipoCausale());
		sb.append(", esercizioDocumento=");
		sb.append(getEsercizioDocumento());
		sb.append(", protocolloDocumento=");
		sb.append(getProtocolloDocumento());
		sb.append(", codiceAttivita=");
		sb.append(getCodiceAttivita());
		sb.append(", codiceCentro=");
		sb.append(getCodiceCentro());
		sb.append(", dataRegistrazione=");
		sb.append(getDataRegistrazione());
		sb.append(", dataOperazione=");
		sb.append(getDataOperazione());
		sb.append(", dataAggiornamento=");
		sb.append(getDataAggiornamento());
		sb.append(", dataChiusura=");
		sb.append(getDataChiusura());
		sb.append(", codiceCausale=");
		sb.append(getCodiceCausale());
		sb.append(", dataDocumento=");
		sb.append(getDataDocumento());
		sb.append(", numeroDocumento=");
		sb.append(getNumeroDocumento());
		sb.append(", codiceTipoPagamOrig=");
		sb.append(getCodiceTipoPagamOrig());
		sb.append(", codiceTipoPagamScad=");
		sb.append(getCodiceTipoPagamScad());
		sb.append(", causaleEstrattoContoOrig=");
		sb.append(getCausaleEstrattoContoOrig());
		sb.append(", codiceDivisaOrig=");
		sb.append(getCodiceDivisaOrig());
		sb.append(", importoTotaleInt=");
		sb.append(getImportoTotaleInt());
		sb.append(", importoPagatoInt=");
		sb.append(getImportoPagatoInt());
		sb.append(", importoApertoInt=");
		sb.append(getImportoApertoInt());
		sb.append(", importoEspostoInt=");
		sb.append(getImportoEspostoInt());
		sb.append(", importoScadutoInt=");
		sb.append(getImportoScadutoInt());
		sb.append(", importoInsolutoInt=");
		sb.append(getImportoInsolutoInt());
		sb.append(", importoInContenziosoInt=");
		sb.append(getImportoInContenziosoInt());
		sb.append(", importoTotale=");
		sb.append(getImportoTotale());
		sb.append(", importoPagato=");
		sb.append(getImportoPagato());
		sb.append(", importoAperto=");
		sb.append(getImportoAperto());
		sb.append(", importoEsposto=");
		sb.append(getImportoEsposto());
		sb.append(", importoScaduto=");
		sb.append(getImportoScaduto());
		sb.append(", importoInsoluto=");
		sb.append(getImportoInsoluto());
		sb.append(", importoInContenzioso=");
		sb.append(getImportoInContenzioso());
		sb.append(", numeroLettereSollecito=");
		sb.append(getNumeroLettereSollecito());
		sb.append(", codiceDivisaInterna=");
		sb.append(getCodiceDivisaInterna());
		sb.append(", codiceEsercizioScadenzaConversione=");
		sb.append(getCodiceEsercizioScadenzaConversione());
		sb.append(", numeroPartitaScadenzaCoversione=");
		sb.append(getNumeroPartitaScadenzaCoversione());
		sb.append(", numeroScadenzaCoversione=");
		sb.append(getNumeroScadenzaCoversione());
		sb.append(", tipoCoversione=");
		sb.append(getTipoCoversione());
		sb.append(", codiceEsercizioScritturaConversione=");
		sb.append(getCodiceEsercizioScritturaConversione());
		sb.append(", numeroRegistrazioneScritturaConversione=");
		sb.append(getNumeroRegistrazioneScritturaConversione());
		sb.append(", dataRegistrazioneScritturaConversione=");
		sb.append(getDataRegistrazioneScritturaConversione());
		sb.append(", codiceDivisaIntScadenzaConversione=");
		sb.append(getCodiceDivisaIntScadenzaConversione());
		sb.append(", codiceDivisaScadenzaConversione=");
		sb.append(getCodiceDivisaScadenzaConversione());
		sb.append(", cambioScadenzaConversione=");
		sb.append(getCambioScadenzaConversione());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(163);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.EstrattoConto");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>tipoSoggetto</column-name><column-value><![CDATA[");
		sb.append(getTipoSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCliente</column-name><column-value><![CDATA[");
		sb.append(getCodiceCliente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>esercizioRegistrazione</column-name><column-value><![CDATA[");
		sb.append(getEsercizioRegistrazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroPartita</column-name><column-value><![CDATA[");
		sb.append(getNumeroPartita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroScadenza</column-name><column-value><![CDATA[");
		sb.append(getNumeroScadenza());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataScadenza</column-name><column-value><![CDATA[");
		sb.append(getDataScadenza());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceTipoPagam</column-name><column-value><![CDATA[");
		sb.append(getCodiceTipoPagam());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>stato</column-name><column-value><![CDATA[");
		sb.append(getStato());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceBanca</column-name><column-value><![CDATA[");
		sb.append(getCodiceBanca());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceContoCorrente</column-name><column-value><![CDATA[");
		sb.append(getCodiceContoCorrente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceAgente</column-name><column-value><![CDATA[");
		sb.append(getCodiceAgente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoCausale</column-name><column-value><![CDATA[");
		sb.append(getTipoCausale());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>esercizioDocumento</column-name><column-value><![CDATA[");
		sb.append(getEsercizioDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>protocolloDocumento</column-name><column-value><![CDATA[");
		sb.append(getProtocolloDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceAttivita</column-name><column-value><![CDATA[");
		sb.append(getCodiceAttivita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCentro</column-name><column-value><![CDATA[");
		sb.append(getCodiceCentro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataRegistrazione</column-name><column-value><![CDATA[");
		sb.append(getDataRegistrazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataOperazione</column-name><column-value><![CDATA[");
		sb.append(getDataOperazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataAggiornamento</column-name><column-value><![CDATA[");
		sb.append(getDataAggiornamento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataChiusura</column-name><column-value><![CDATA[");
		sb.append(getDataChiusura());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCausale</column-name><column-value><![CDATA[");
		sb.append(getCodiceCausale());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataDocumento</column-name><column-value><![CDATA[");
		sb.append(getDataDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroDocumento</column-name><column-value><![CDATA[");
		sb.append(getNumeroDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceTipoPagamOrig</column-name><column-value><![CDATA[");
		sb.append(getCodiceTipoPagamOrig());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceTipoPagamScad</column-name><column-value><![CDATA[");
		sb.append(getCodiceTipoPagamScad());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>causaleEstrattoContoOrig</column-name><column-value><![CDATA[");
		sb.append(getCausaleEstrattoContoOrig());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDivisaOrig</column-name><column-value><![CDATA[");
		sb.append(getCodiceDivisaOrig());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoTotaleInt</column-name><column-value><![CDATA[");
		sb.append(getImportoTotaleInt());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoPagatoInt</column-name><column-value><![CDATA[");
		sb.append(getImportoPagatoInt());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoApertoInt</column-name><column-value><![CDATA[");
		sb.append(getImportoApertoInt());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoEspostoInt</column-name><column-value><![CDATA[");
		sb.append(getImportoEspostoInt());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoScadutoInt</column-name><column-value><![CDATA[");
		sb.append(getImportoScadutoInt());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoInsolutoInt</column-name><column-value><![CDATA[");
		sb.append(getImportoInsolutoInt());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoInContenziosoInt</column-name><column-value><![CDATA[");
		sb.append(getImportoInContenziosoInt());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoTotale</column-name><column-value><![CDATA[");
		sb.append(getImportoTotale());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoPagato</column-name><column-value><![CDATA[");
		sb.append(getImportoPagato());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoAperto</column-name><column-value><![CDATA[");
		sb.append(getImportoAperto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoEsposto</column-name><column-value><![CDATA[");
		sb.append(getImportoEsposto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoScaduto</column-name><column-value><![CDATA[");
		sb.append(getImportoScaduto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoInsoluto</column-name><column-value><![CDATA[");
		sb.append(getImportoInsoluto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>importoInContenzioso</column-name><column-value><![CDATA[");
		sb.append(getImportoInContenzioso());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroLettereSollecito</column-name><column-value><![CDATA[");
		sb.append(getNumeroLettereSollecito());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDivisaInterna</column-name><column-value><![CDATA[");
		sb.append(getCodiceDivisaInterna());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceEsercizioScadenzaConversione</column-name><column-value><![CDATA[");
		sb.append(getCodiceEsercizioScadenzaConversione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroPartitaScadenzaCoversione</column-name><column-value><![CDATA[");
		sb.append(getNumeroPartitaScadenzaCoversione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroScadenzaCoversione</column-name><column-value><![CDATA[");
		sb.append(getNumeroScadenzaCoversione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoCoversione</column-name><column-value><![CDATA[");
		sb.append(getTipoCoversione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceEsercizioScritturaConversione</column-name><column-value><![CDATA[");
		sb.append(getCodiceEsercizioScritturaConversione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroRegistrazioneScritturaConversione</column-name><column-value><![CDATA[");
		sb.append(getNumeroRegistrazioneScritturaConversione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataRegistrazioneScritturaConversione</column-name><column-value><![CDATA[");
		sb.append(getDataRegistrazioneScritturaConversione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDivisaIntScadenzaConversione</column-name><column-value><![CDATA[");
		sb.append(getCodiceDivisaIntScadenzaConversione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDivisaScadenzaConversione</column-name><column-value><![CDATA[");
		sb.append(getCodiceDivisaScadenzaConversione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cambioScadenzaConversione</column-name><column-value><![CDATA[");
		sb.append(getCambioScadenzaConversione());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private boolean _tipoSoggetto;
	private String _codiceCliente;
	private int _esercizioRegistrazione;
	private int _numeroPartita;
	private int _numeroScadenza;
	private Date _dataScadenza;
	private int _codiceTipoPagam;
	private int _stato;
	private String _codiceBanca;
	private String _codiceContoCorrente;
	private String _codiceAgente;
	private String _tipoCausale;
	private int _esercizioDocumento;
	private int _protocolloDocumento;
	private String _codiceAttivita;
	private String _codiceCentro;
	private Date _dataRegistrazione;
	private Date _dataOperazione;
	private Date _dataAggiornamento;
	private Date _dataChiusura;
	private String _codiceCausale;
	private Date _dataDocumento;
	private int _numeroDocumento;
	private String _codiceTipoPagamOrig;
	private String _codiceTipoPagamScad;
	private String _causaleEstrattoContoOrig;
	private String _codiceDivisaOrig;
	private double _importoTotaleInt;
	private double _importoPagatoInt;
	private double _importoApertoInt;
	private double _importoEspostoInt;
	private double _importoScadutoInt;
	private double _importoInsolutoInt;
	private double _importoInContenziosoInt;
	private double _importoTotale;
	private double _importoPagato;
	private double _importoAperto;
	private double _importoEsposto;
	private double _importoScaduto;
	private double _importoInsoluto;
	private double _importoInContenzioso;
	private int _numeroLettereSollecito;
	private String _codiceDivisaInterna;
	private int _codiceEsercizioScadenzaConversione;
	private int _numeroPartitaScadenzaCoversione;
	private int _numeroScadenzaCoversione;
	private int _tipoCoversione;
	private int _codiceEsercizioScritturaConversione;
	private int _numeroRegistrazioneScritturaConversione;
	private Date _dataRegistrazioneScritturaConversione;
	private String _codiceDivisaIntScadenzaConversione;
	private String _codiceDivisaScadenzaConversione;
	private double _cambioScadenzaConversione;
	private BaseModel<?> _estrattoContoRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}