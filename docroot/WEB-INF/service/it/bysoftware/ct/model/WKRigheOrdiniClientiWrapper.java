/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link WKRigheOrdiniClienti}.
 * </p>
 *
 * @author Mario Torrisi
 * @see WKRigheOrdiniClienti
 * @generated
 */
public class WKRigheOrdiniClientiWrapper implements WKRigheOrdiniClienti,
	ModelWrapper<WKRigheOrdiniClienti> {
	public WKRigheOrdiniClientiWrapper(
		WKRigheOrdiniClienti wkRigheOrdiniClienti) {
		_wkRigheOrdiniClienti = wkRigheOrdiniClienti;
	}

	@Override
	public Class<?> getModelClass() {
		return WKRigheOrdiniClienti.class;
	}

	@Override
	public String getModelClassName() {
		return WKRigheOrdiniClienti.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("anno", getAnno());
		attributes.put("tipoOrdine", getTipoOrdine());
		attributes.put("numeroOrdine", getNumeroOrdine());
		attributes.put("numeroRigo", getNumeroRigo());
		attributes.put("statoRigo", getStatoRigo());
		attributes.put("tipoRigo", getTipoRigo());
		attributes.put("codiceDepositoMov", getCodiceDepositoMov());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("descrizione", getDescrizione());
		attributes.put("codiceUnitMis", getCodiceUnitMis());
		attributes.put("decimaliQuant", getDecimaliQuant());
		attributes.put("quantita1", getQuantita1());
		attributes.put("quantita2", getQuantita2());
		attributes.put("quantita3", getQuantita3());
		attributes.put("quantita", getQuantita());
		attributes.put("codiceUnitMis2", getCodiceUnitMis2());
		attributes.put("quantitaUnitMis2", getQuantitaUnitMis2());
		attributes.put("decimaliPrezzo", getDecimaliPrezzo());
		attributes.put("prezzo", getPrezzo());
		attributes.put("importoLordo", getImportoLordo());
		attributes.put("sconto1", getSconto1());
		attributes.put("sconto2", getSconto2());
		attributes.put("sconto3", getSconto3());
		attributes.put("importoNetto", getImportoNetto());
		attributes.put("importo", getImporto());
		attributes.put("codiceIVAFatturazione", getCodiceIVAFatturazione());
		attributes.put("codiceCliente", getCodiceCliente());
		attributes.put("riferimentoOrdineCliente", getRiferimentoOrdineCliente());
		attributes.put("dataOridine", getDataOridine());
		attributes.put("statoEvasione", getStatoEvasione());
		attributes.put("dataPrevistaConsegna", getDataPrevistaConsegna());
		attributes.put("dataRegistrazioneOrdine", getDataRegistrazioneOrdine());
		attributes.put("libStr1", getLibStr1());
		attributes.put("libStr2", getLibStr2());
		attributes.put("libStr3", getLibStr3());
		attributes.put("libDbl1", getLibDbl1());
		attributes.put("libDbl2", getLibDbl2());
		attributes.put("libDbl3", getLibDbl3());
		attributes.put("libDat1", getLibDat1());
		attributes.put("libDat2", getLibDat2());
		attributes.put("libDat3", getLibDat3());
		attributes.put("libLng1", getLibLng1());
		attributes.put("libLng2", getLibLng2());
		attributes.put("libLng3", getLibLng3());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer anno = (Integer)attributes.get("anno");

		if (anno != null) {
			setAnno(anno);
		}

		Integer tipoOrdine = (Integer)attributes.get("tipoOrdine");

		if (tipoOrdine != null) {
			setTipoOrdine(tipoOrdine);
		}

		Integer numeroOrdine = (Integer)attributes.get("numeroOrdine");

		if (numeroOrdine != null) {
			setNumeroOrdine(numeroOrdine);
		}

		Integer numeroRigo = (Integer)attributes.get("numeroRigo");

		if (numeroRigo != null) {
			setNumeroRigo(numeroRigo);
		}

		Boolean statoRigo = (Boolean)attributes.get("statoRigo");

		if (statoRigo != null) {
			setStatoRigo(statoRigo);
		}

		Integer tipoRigo = (Integer)attributes.get("tipoRigo");

		if (tipoRigo != null) {
			setTipoRigo(tipoRigo);
		}

		String codiceDepositoMov = (String)attributes.get("codiceDepositoMov");

		if (codiceDepositoMov != null) {
			setCodiceDepositoMov(codiceDepositoMov);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		String codiceUnitMis = (String)attributes.get("codiceUnitMis");

		if (codiceUnitMis != null) {
			setCodiceUnitMis(codiceUnitMis);
		}

		Integer decimaliQuant = (Integer)attributes.get("decimaliQuant");

		if (decimaliQuant != null) {
			setDecimaliQuant(decimaliQuant);
		}

		Double quantita1 = (Double)attributes.get("quantita1");

		if (quantita1 != null) {
			setQuantita1(quantita1);
		}

		Double quantita2 = (Double)attributes.get("quantita2");

		if (quantita2 != null) {
			setQuantita2(quantita2);
		}

		Double quantita3 = (Double)attributes.get("quantita3");

		if (quantita3 != null) {
			setQuantita3(quantita3);
		}

		Double quantita = (Double)attributes.get("quantita");

		if (quantita != null) {
			setQuantita(quantita);
		}

		String codiceUnitMis2 = (String)attributes.get("codiceUnitMis2");

		if (codiceUnitMis2 != null) {
			setCodiceUnitMis2(codiceUnitMis2);
		}

		Double quantitaUnitMis2 = (Double)attributes.get("quantitaUnitMis2");

		if (quantitaUnitMis2 != null) {
			setQuantitaUnitMis2(quantitaUnitMis2);
		}

		Integer decimaliPrezzo = (Integer)attributes.get("decimaliPrezzo");

		if (decimaliPrezzo != null) {
			setDecimaliPrezzo(decimaliPrezzo);
		}

		Double prezzo = (Double)attributes.get("prezzo");

		if (prezzo != null) {
			setPrezzo(prezzo);
		}

		Double importoLordo = (Double)attributes.get("importoLordo");

		if (importoLordo != null) {
			setImportoLordo(importoLordo);
		}

		Double sconto1 = (Double)attributes.get("sconto1");

		if (sconto1 != null) {
			setSconto1(sconto1);
		}

		Double sconto2 = (Double)attributes.get("sconto2");

		if (sconto2 != null) {
			setSconto2(sconto2);
		}

		Double sconto3 = (Double)attributes.get("sconto3");

		if (sconto3 != null) {
			setSconto3(sconto3);
		}

		Double importoNetto = (Double)attributes.get("importoNetto");

		if (importoNetto != null) {
			setImportoNetto(importoNetto);
		}

		Double importo = (Double)attributes.get("importo");

		if (importo != null) {
			setImporto(importo);
		}

		String codiceIVAFatturazione = (String)attributes.get(
				"codiceIVAFatturazione");

		if (codiceIVAFatturazione != null) {
			setCodiceIVAFatturazione(codiceIVAFatturazione);
		}

		String codiceCliente = (String)attributes.get("codiceCliente");

		if (codiceCliente != null) {
			setCodiceCliente(codiceCliente);
		}

		String riferimentoOrdineCliente = (String)attributes.get(
				"riferimentoOrdineCliente");

		if (riferimentoOrdineCliente != null) {
			setRiferimentoOrdineCliente(riferimentoOrdineCliente);
		}

		Date dataOridine = (Date)attributes.get("dataOridine");

		if (dataOridine != null) {
			setDataOridine(dataOridine);
		}

		Boolean statoEvasione = (Boolean)attributes.get("statoEvasione");

		if (statoEvasione != null) {
			setStatoEvasione(statoEvasione);
		}

		Date dataPrevistaConsegna = (Date)attributes.get("dataPrevistaConsegna");

		if (dataPrevistaConsegna != null) {
			setDataPrevistaConsegna(dataPrevistaConsegna);
		}

		Date dataRegistrazioneOrdine = (Date)attributes.get(
				"dataRegistrazioneOrdine");

		if (dataRegistrazioneOrdine != null) {
			setDataRegistrazioneOrdine(dataRegistrazioneOrdine);
		}

		String libStr1 = (String)attributes.get("libStr1");

		if (libStr1 != null) {
			setLibStr1(libStr1);
		}

		String libStr2 = (String)attributes.get("libStr2");

		if (libStr2 != null) {
			setLibStr2(libStr2);
		}

		String libStr3 = (String)attributes.get("libStr3");

		if (libStr3 != null) {
			setLibStr3(libStr3);
		}

		Double libDbl1 = (Double)attributes.get("libDbl1");

		if (libDbl1 != null) {
			setLibDbl1(libDbl1);
		}

		Double libDbl2 = (Double)attributes.get("libDbl2");

		if (libDbl2 != null) {
			setLibDbl2(libDbl2);
		}

		Double libDbl3 = (Double)attributes.get("libDbl3");

		if (libDbl3 != null) {
			setLibDbl3(libDbl3);
		}

		Date libDat1 = (Date)attributes.get("libDat1");

		if (libDat1 != null) {
			setLibDat1(libDat1);
		}

		Date libDat2 = (Date)attributes.get("libDat2");

		if (libDat2 != null) {
			setLibDat2(libDat2);
		}

		Date libDat3 = (Date)attributes.get("libDat3");

		if (libDat3 != null) {
			setLibDat3(libDat3);
		}

		Long libLng1 = (Long)attributes.get("libLng1");

		if (libLng1 != null) {
			setLibLng1(libLng1);
		}

		Long libLng2 = (Long)attributes.get("libLng2");

		if (libLng2 != null) {
			setLibLng2(libLng2);
		}

		Long libLng3 = (Long)attributes.get("libLng3");

		if (libLng3 != null) {
			setLibLng3(libLng3);
		}
	}

	/**
	* Returns the primary key of this w k righe ordini clienti.
	*
	* @return the primary key of this w k righe ordini clienti
	*/
	@Override
	public it.bysoftware.ct.service.persistence.WKRigheOrdiniClientiPK getPrimaryKey() {
		return _wkRigheOrdiniClienti.getPrimaryKey();
	}

	/**
	* Sets the primary key of this w k righe ordini clienti.
	*
	* @param primaryKey the primary key of this w k righe ordini clienti
	*/
	@Override
	public void setPrimaryKey(
		it.bysoftware.ct.service.persistence.WKRigheOrdiniClientiPK primaryKey) {
		_wkRigheOrdiniClienti.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the anno of this w k righe ordini clienti.
	*
	* @return the anno of this w k righe ordini clienti
	*/
	@Override
	public int getAnno() {
		return _wkRigheOrdiniClienti.getAnno();
	}

	/**
	* Sets the anno of this w k righe ordini clienti.
	*
	* @param anno the anno of this w k righe ordini clienti
	*/
	@Override
	public void setAnno(int anno) {
		_wkRigheOrdiniClienti.setAnno(anno);
	}

	/**
	* Returns the tipo ordine of this w k righe ordini clienti.
	*
	* @return the tipo ordine of this w k righe ordini clienti
	*/
	@Override
	public int getTipoOrdine() {
		return _wkRigheOrdiniClienti.getTipoOrdine();
	}

	/**
	* Sets the tipo ordine of this w k righe ordini clienti.
	*
	* @param tipoOrdine the tipo ordine of this w k righe ordini clienti
	*/
	@Override
	public void setTipoOrdine(int tipoOrdine) {
		_wkRigheOrdiniClienti.setTipoOrdine(tipoOrdine);
	}

	/**
	* Returns the numero ordine of this w k righe ordini clienti.
	*
	* @return the numero ordine of this w k righe ordini clienti
	*/
	@Override
	public int getNumeroOrdine() {
		return _wkRigheOrdiniClienti.getNumeroOrdine();
	}

	/**
	* Sets the numero ordine of this w k righe ordini clienti.
	*
	* @param numeroOrdine the numero ordine of this w k righe ordini clienti
	*/
	@Override
	public void setNumeroOrdine(int numeroOrdine) {
		_wkRigheOrdiniClienti.setNumeroOrdine(numeroOrdine);
	}

	/**
	* Returns the numero rigo of this w k righe ordini clienti.
	*
	* @return the numero rigo of this w k righe ordini clienti
	*/
	@Override
	public int getNumeroRigo() {
		return _wkRigheOrdiniClienti.getNumeroRigo();
	}

	/**
	* Sets the numero rigo of this w k righe ordini clienti.
	*
	* @param numeroRigo the numero rigo of this w k righe ordini clienti
	*/
	@Override
	public void setNumeroRigo(int numeroRigo) {
		_wkRigheOrdiniClienti.setNumeroRigo(numeroRigo);
	}

	/**
	* Returns the stato rigo of this w k righe ordini clienti.
	*
	* @return the stato rigo of this w k righe ordini clienti
	*/
	@Override
	public boolean getStatoRigo() {
		return _wkRigheOrdiniClienti.getStatoRigo();
	}

	/**
	* Returns <code>true</code> if this w k righe ordini clienti is stato rigo.
	*
	* @return <code>true</code> if this w k righe ordini clienti is stato rigo; <code>false</code> otherwise
	*/
	@Override
	public boolean isStatoRigo() {
		return _wkRigheOrdiniClienti.isStatoRigo();
	}

	/**
	* Sets whether this w k righe ordini clienti is stato rigo.
	*
	* @param statoRigo the stato rigo of this w k righe ordini clienti
	*/
	@Override
	public void setStatoRigo(boolean statoRigo) {
		_wkRigheOrdiniClienti.setStatoRigo(statoRigo);
	}

	/**
	* Returns the tipo rigo of this w k righe ordini clienti.
	*
	* @return the tipo rigo of this w k righe ordini clienti
	*/
	@Override
	public int getTipoRigo() {
		return _wkRigheOrdiniClienti.getTipoRigo();
	}

	/**
	* Sets the tipo rigo of this w k righe ordini clienti.
	*
	* @param tipoRigo the tipo rigo of this w k righe ordini clienti
	*/
	@Override
	public void setTipoRigo(int tipoRigo) {
		_wkRigheOrdiniClienti.setTipoRigo(tipoRigo);
	}

	/**
	* Returns the codice deposito mov of this w k righe ordini clienti.
	*
	* @return the codice deposito mov of this w k righe ordini clienti
	*/
	@Override
	public java.lang.String getCodiceDepositoMov() {
		return _wkRigheOrdiniClienti.getCodiceDepositoMov();
	}

	/**
	* Sets the codice deposito mov of this w k righe ordini clienti.
	*
	* @param codiceDepositoMov the codice deposito mov of this w k righe ordini clienti
	*/
	@Override
	public void setCodiceDepositoMov(java.lang.String codiceDepositoMov) {
		_wkRigheOrdiniClienti.setCodiceDepositoMov(codiceDepositoMov);
	}

	/**
	* Returns the codice articolo of this w k righe ordini clienti.
	*
	* @return the codice articolo of this w k righe ordini clienti
	*/
	@Override
	public java.lang.String getCodiceArticolo() {
		return _wkRigheOrdiniClienti.getCodiceArticolo();
	}

	/**
	* Sets the codice articolo of this w k righe ordini clienti.
	*
	* @param codiceArticolo the codice articolo of this w k righe ordini clienti
	*/
	@Override
	public void setCodiceArticolo(java.lang.String codiceArticolo) {
		_wkRigheOrdiniClienti.setCodiceArticolo(codiceArticolo);
	}

	/**
	* Returns the codice variante of this w k righe ordini clienti.
	*
	* @return the codice variante of this w k righe ordini clienti
	*/
	@Override
	public java.lang.String getCodiceVariante() {
		return _wkRigheOrdiniClienti.getCodiceVariante();
	}

	/**
	* Sets the codice variante of this w k righe ordini clienti.
	*
	* @param codiceVariante the codice variante of this w k righe ordini clienti
	*/
	@Override
	public void setCodiceVariante(java.lang.String codiceVariante) {
		_wkRigheOrdiniClienti.setCodiceVariante(codiceVariante);
	}

	/**
	* Returns the descrizione of this w k righe ordini clienti.
	*
	* @return the descrizione of this w k righe ordini clienti
	*/
	@Override
	public java.lang.String getDescrizione() {
		return _wkRigheOrdiniClienti.getDescrizione();
	}

	/**
	* Sets the descrizione of this w k righe ordini clienti.
	*
	* @param descrizione the descrizione of this w k righe ordini clienti
	*/
	@Override
	public void setDescrizione(java.lang.String descrizione) {
		_wkRigheOrdiniClienti.setDescrizione(descrizione);
	}

	/**
	* Returns the codice unit mis of this w k righe ordini clienti.
	*
	* @return the codice unit mis of this w k righe ordini clienti
	*/
	@Override
	public java.lang.String getCodiceUnitMis() {
		return _wkRigheOrdiniClienti.getCodiceUnitMis();
	}

	/**
	* Sets the codice unit mis of this w k righe ordini clienti.
	*
	* @param codiceUnitMis the codice unit mis of this w k righe ordini clienti
	*/
	@Override
	public void setCodiceUnitMis(java.lang.String codiceUnitMis) {
		_wkRigheOrdiniClienti.setCodiceUnitMis(codiceUnitMis);
	}

	/**
	* Returns the decimali quant of this w k righe ordini clienti.
	*
	* @return the decimali quant of this w k righe ordini clienti
	*/
	@Override
	public int getDecimaliQuant() {
		return _wkRigheOrdiniClienti.getDecimaliQuant();
	}

	/**
	* Sets the decimali quant of this w k righe ordini clienti.
	*
	* @param decimaliQuant the decimali quant of this w k righe ordini clienti
	*/
	@Override
	public void setDecimaliQuant(int decimaliQuant) {
		_wkRigheOrdiniClienti.setDecimaliQuant(decimaliQuant);
	}

	/**
	* Returns the quantita1 of this w k righe ordini clienti.
	*
	* @return the quantita1 of this w k righe ordini clienti
	*/
	@Override
	public double getQuantita1() {
		return _wkRigheOrdiniClienti.getQuantita1();
	}

	/**
	* Sets the quantita1 of this w k righe ordini clienti.
	*
	* @param quantita1 the quantita1 of this w k righe ordini clienti
	*/
	@Override
	public void setQuantita1(double quantita1) {
		_wkRigheOrdiniClienti.setQuantita1(quantita1);
	}

	/**
	* Returns the quantita2 of this w k righe ordini clienti.
	*
	* @return the quantita2 of this w k righe ordini clienti
	*/
	@Override
	public double getQuantita2() {
		return _wkRigheOrdiniClienti.getQuantita2();
	}

	/**
	* Sets the quantita2 of this w k righe ordini clienti.
	*
	* @param quantita2 the quantita2 of this w k righe ordini clienti
	*/
	@Override
	public void setQuantita2(double quantita2) {
		_wkRigheOrdiniClienti.setQuantita2(quantita2);
	}

	/**
	* Returns the quantita3 of this w k righe ordini clienti.
	*
	* @return the quantita3 of this w k righe ordini clienti
	*/
	@Override
	public double getQuantita3() {
		return _wkRigheOrdiniClienti.getQuantita3();
	}

	/**
	* Sets the quantita3 of this w k righe ordini clienti.
	*
	* @param quantita3 the quantita3 of this w k righe ordini clienti
	*/
	@Override
	public void setQuantita3(double quantita3) {
		_wkRigheOrdiniClienti.setQuantita3(quantita3);
	}

	/**
	* Returns the quantita of this w k righe ordini clienti.
	*
	* @return the quantita of this w k righe ordini clienti
	*/
	@Override
	public double getQuantita() {
		return _wkRigheOrdiniClienti.getQuantita();
	}

	/**
	* Sets the quantita of this w k righe ordini clienti.
	*
	* @param quantita the quantita of this w k righe ordini clienti
	*/
	@Override
	public void setQuantita(double quantita) {
		_wkRigheOrdiniClienti.setQuantita(quantita);
	}

	/**
	* Returns the codice unit mis2 of this w k righe ordini clienti.
	*
	* @return the codice unit mis2 of this w k righe ordini clienti
	*/
	@Override
	public java.lang.String getCodiceUnitMis2() {
		return _wkRigheOrdiniClienti.getCodiceUnitMis2();
	}

	/**
	* Sets the codice unit mis2 of this w k righe ordini clienti.
	*
	* @param codiceUnitMis2 the codice unit mis2 of this w k righe ordini clienti
	*/
	@Override
	public void setCodiceUnitMis2(java.lang.String codiceUnitMis2) {
		_wkRigheOrdiniClienti.setCodiceUnitMis2(codiceUnitMis2);
	}

	/**
	* Returns the quantita unit mis2 of this w k righe ordini clienti.
	*
	* @return the quantita unit mis2 of this w k righe ordini clienti
	*/
	@Override
	public double getQuantitaUnitMis2() {
		return _wkRigheOrdiniClienti.getQuantitaUnitMis2();
	}

	/**
	* Sets the quantita unit mis2 of this w k righe ordini clienti.
	*
	* @param quantitaUnitMis2 the quantita unit mis2 of this w k righe ordini clienti
	*/
	@Override
	public void setQuantitaUnitMis2(double quantitaUnitMis2) {
		_wkRigheOrdiniClienti.setQuantitaUnitMis2(quantitaUnitMis2);
	}

	/**
	* Returns the decimali prezzo of this w k righe ordini clienti.
	*
	* @return the decimali prezzo of this w k righe ordini clienti
	*/
	@Override
	public int getDecimaliPrezzo() {
		return _wkRigheOrdiniClienti.getDecimaliPrezzo();
	}

	/**
	* Sets the decimali prezzo of this w k righe ordini clienti.
	*
	* @param decimaliPrezzo the decimali prezzo of this w k righe ordini clienti
	*/
	@Override
	public void setDecimaliPrezzo(int decimaliPrezzo) {
		_wkRigheOrdiniClienti.setDecimaliPrezzo(decimaliPrezzo);
	}

	/**
	* Returns the prezzo of this w k righe ordini clienti.
	*
	* @return the prezzo of this w k righe ordini clienti
	*/
	@Override
	public double getPrezzo() {
		return _wkRigheOrdiniClienti.getPrezzo();
	}

	/**
	* Sets the prezzo of this w k righe ordini clienti.
	*
	* @param prezzo the prezzo of this w k righe ordini clienti
	*/
	@Override
	public void setPrezzo(double prezzo) {
		_wkRigheOrdiniClienti.setPrezzo(prezzo);
	}

	/**
	* Returns the importo lordo of this w k righe ordini clienti.
	*
	* @return the importo lordo of this w k righe ordini clienti
	*/
	@Override
	public double getImportoLordo() {
		return _wkRigheOrdiniClienti.getImportoLordo();
	}

	/**
	* Sets the importo lordo of this w k righe ordini clienti.
	*
	* @param importoLordo the importo lordo of this w k righe ordini clienti
	*/
	@Override
	public void setImportoLordo(double importoLordo) {
		_wkRigheOrdiniClienti.setImportoLordo(importoLordo);
	}

	/**
	* Returns the sconto1 of this w k righe ordini clienti.
	*
	* @return the sconto1 of this w k righe ordini clienti
	*/
	@Override
	public double getSconto1() {
		return _wkRigheOrdiniClienti.getSconto1();
	}

	/**
	* Sets the sconto1 of this w k righe ordini clienti.
	*
	* @param sconto1 the sconto1 of this w k righe ordini clienti
	*/
	@Override
	public void setSconto1(double sconto1) {
		_wkRigheOrdiniClienti.setSconto1(sconto1);
	}

	/**
	* Returns the sconto2 of this w k righe ordini clienti.
	*
	* @return the sconto2 of this w k righe ordini clienti
	*/
	@Override
	public double getSconto2() {
		return _wkRigheOrdiniClienti.getSconto2();
	}

	/**
	* Sets the sconto2 of this w k righe ordini clienti.
	*
	* @param sconto2 the sconto2 of this w k righe ordini clienti
	*/
	@Override
	public void setSconto2(double sconto2) {
		_wkRigheOrdiniClienti.setSconto2(sconto2);
	}

	/**
	* Returns the sconto3 of this w k righe ordini clienti.
	*
	* @return the sconto3 of this w k righe ordini clienti
	*/
	@Override
	public double getSconto3() {
		return _wkRigheOrdiniClienti.getSconto3();
	}

	/**
	* Sets the sconto3 of this w k righe ordini clienti.
	*
	* @param sconto3 the sconto3 of this w k righe ordini clienti
	*/
	@Override
	public void setSconto3(double sconto3) {
		_wkRigheOrdiniClienti.setSconto3(sconto3);
	}

	/**
	* Returns the importo netto of this w k righe ordini clienti.
	*
	* @return the importo netto of this w k righe ordini clienti
	*/
	@Override
	public double getImportoNetto() {
		return _wkRigheOrdiniClienti.getImportoNetto();
	}

	/**
	* Sets the importo netto of this w k righe ordini clienti.
	*
	* @param importoNetto the importo netto of this w k righe ordini clienti
	*/
	@Override
	public void setImportoNetto(double importoNetto) {
		_wkRigheOrdiniClienti.setImportoNetto(importoNetto);
	}

	/**
	* Returns the importo of this w k righe ordini clienti.
	*
	* @return the importo of this w k righe ordini clienti
	*/
	@Override
	public double getImporto() {
		return _wkRigheOrdiniClienti.getImporto();
	}

	/**
	* Sets the importo of this w k righe ordini clienti.
	*
	* @param importo the importo of this w k righe ordini clienti
	*/
	@Override
	public void setImporto(double importo) {
		_wkRigheOrdiniClienti.setImporto(importo);
	}

	/**
	* Returns the codice i v a fatturazione of this w k righe ordini clienti.
	*
	* @return the codice i v a fatturazione of this w k righe ordini clienti
	*/
	@Override
	public java.lang.String getCodiceIVAFatturazione() {
		return _wkRigheOrdiniClienti.getCodiceIVAFatturazione();
	}

	/**
	* Sets the codice i v a fatturazione of this w k righe ordini clienti.
	*
	* @param codiceIVAFatturazione the codice i v a fatturazione of this w k righe ordini clienti
	*/
	@Override
	public void setCodiceIVAFatturazione(java.lang.String codiceIVAFatturazione) {
		_wkRigheOrdiniClienti.setCodiceIVAFatturazione(codiceIVAFatturazione);
	}

	/**
	* Returns the codice cliente of this w k righe ordini clienti.
	*
	* @return the codice cliente of this w k righe ordini clienti
	*/
	@Override
	public java.lang.String getCodiceCliente() {
		return _wkRigheOrdiniClienti.getCodiceCliente();
	}

	/**
	* Sets the codice cliente of this w k righe ordini clienti.
	*
	* @param codiceCliente the codice cliente of this w k righe ordini clienti
	*/
	@Override
	public void setCodiceCliente(java.lang.String codiceCliente) {
		_wkRigheOrdiniClienti.setCodiceCliente(codiceCliente);
	}

	/**
	* Returns the riferimento ordine cliente of this w k righe ordini clienti.
	*
	* @return the riferimento ordine cliente of this w k righe ordini clienti
	*/
	@Override
	public java.lang.String getRiferimentoOrdineCliente() {
		return _wkRigheOrdiniClienti.getRiferimentoOrdineCliente();
	}

	/**
	* Sets the riferimento ordine cliente of this w k righe ordini clienti.
	*
	* @param riferimentoOrdineCliente the riferimento ordine cliente of this w k righe ordini clienti
	*/
	@Override
	public void setRiferimentoOrdineCliente(
		java.lang.String riferimentoOrdineCliente) {
		_wkRigheOrdiniClienti.setRiferimentoOrdineCliente(riferimentoOrdineCliente);
	}

	/**
	* Returns the data oridine of this w k righe ordini clienti.
	*
	* @return the data oridine of this w k righe ordini clienti
	*/
	@Override
	public java.util.Date getDataOridine() {
		return _wkRigheOrdiniClienti.getDataOridine();
	}

	/**
	* Sets the data oridine of this w k righe ordini clienti.
	*
	* @param dataOridine the data oridine of this w k righe ordini clienti
	*/
	@Override
	public void setDataOridine(java.util.Date dataOridine) {
		_wkRigheOrdiniClienti.setDataOridine(dataOridine);
	}

	/**
	* Returns the stato evasione of this w k righe ordini clienti.
	*
	* @return the stato evasione of this w k righe ordini clienti
	*/
	@Override
	public boolean getStatoEvasione() {
		return _wkRigheOrdiniClienti.getStatoEvasione();
	}

	/**
	* Returns <code>true</code> if this w k righe ordini clienti is stato evasione.
	*
	* @return <code>true</code> if this w k righe ordini clienti is stato evasione; <code>false</code> otherwise
	*/
	@Override
	public boolean isStatoEvasione() {
		return _wkRigheOrdiniClienti.isStatoEvasione();
	}

	/**
	* Sets whether this w k righe ordini clienti is stato evasione.
	*
	* @param statoEvasione the stato evasione of this w k righe ordini clienti
	*/
	@Override
	public void setStatoEvasione(boolean statoEvasione) {
		_wkRigheOrdiniClienti.setStatoEvasione(statoEvasione);
	}

	/**
	* Returns the data prevista consegna of this w k righe ordini clienti.
	*
	* @return the data prevista consegna of this w k righe ordini clienti
	*/
	@Override
	public java.util.Date getDataPrevistaConsegna() {
		return _wkRigheOrdiniClienti.getDataPrevistaConsegna();
	}

	/**
	* Sets the data prevista consegna of this w k righe ordini clienti.
	*
	* @param dataPrevistaConsegna the data prevista consegna of this w k righe ordini clienti
	*/
	@Override
	public void setDataPrevistaConsegna(java.util.Date dataPrevistaConsegna) {
		_wkRigheOrdiniClienti.setDataPrevistaConsegna(dataPrevistaConsegna);
	}

	/**
	* Returns the data registrazione ordine of this w k righe ordini clienti.
	*
	* @return the data registrazione ordine of this w k righe ordini clienti
	*/
	@Override
	public java.util.Date getDataRegistrazioneOrdine() {
		return _wkRigheOrdiniClienti.getDataRegistrazioneOrdine();
	}

	/**
	* Sets the data registrazione ordine of this w k righe ordini clienti.
	*
	* @param dataRegistrazioneOrdine the data registrazione ordine of this w k righe ordini clienti
	*/
	@Override
	public void setDataRegistrazioneOrdine(
		java.util.Date dataRegistrazioneOrdine) {
		_wkRigheOrdiniClienti.setDataRegistrazioneOrdine(dataRegistrazioneOrdine);
	}

	/**
	* Returns the lib str1 of this w k righe ordini clienti.
	*
	* @return the lib str1 of this w k righe ordini clienti
	*/
	@Override
	public java.lang.String getLibStr1() {
		return _wkRigheOrdiniClienti.getLibStr1();
	}

	/**
	* Sets the lib str1 of this w k righe ordini clienti.
	*
	* @param libStr1 the lib str1 of this w k righe ordini clienti
	*/
	@Override
	public void setLibStr1(java.lang.String libStr1) {
		_wkRigheOrdiniClienti.setLibStr1(libStr1);
	}

	/**
	* Returns the lib str2 of this w k righe ordini clienti.
	*
	* @return the lib str2 of this w k righe ordini clienti
	*/
	@Override
	public java.lang.String getLibStr2() {
		return _wkRigheOrdiniClienti.getLibStr2();
	}

	/**
	* Sets the lib str2 of this w k righe ordini clienti.
	*
	* @param libStr2 the lib str2 of this w k righe ordini clienti
	*/
	@Override
	public void setLibStr2(java.lang.String libStr2) {
		_wkRigheOrdiniClienti.setLibStr2(libStr2);
	}

	/**
	* Returns the lib str3 of this w k righe ordini clienti.
	*
	* @return the lib str3 of this w k righe ordini clienti
	*/
	@Override
	public java.lang.String getLibStr3() {
		return _wkRigheOrdiniClienti.getLibStr3();
	}

	/**
	* Sets the lib str3 of this w k righe ordini clienti.
	*
	* @param libStr3 the lib str3 of this w k righe ordini clienti
	*/
	@Override
	public void setLibStr3(java.lang.String libStr3) {
		_wkRigheOrdiniClienti.setLibStr3(libStr3);
	}

	/**
	* Returns the lib dbl1 of this w k righe ordini clienti.
	*
	* @return the lib dbl1 of this w k righe ordini clienti
	*/
	@Override
	public double getLibDbl1() {
		return _wkRigheOrdiniClienti.getLibDbl1();
	}

	/**
	* Sets the lib dbl1 of this w k righe ordini clienti.
	*
	* @param libDbl1 the lib dbl1 of this w k righe ordini clienti
	*/
	@Override
	public void setLibDbl1(double libDbl1) {
		_wkRigheOrdiniClienti.setLibDbl1(libDbl1);
	}

	/**
	* Returns the lib dbl2 of this w k righe ordini clienti.
	*
	* @return the lib dbl2 of this w k righe ordini clienti
	*/
	@Override
	public double getLibDbl2() {
		return _wkRigheOrdiniClienti.getLibDbl2();
	}

	/**
	* Sets the lib dbl2 of this w k righe ordini clienti.
	*
	* @param libDbl2 the lib dbl2 of this w k righe ordini clienti
	*/
	@Override
	public void setLibDbl2(double libDbl2) {
		_wkRigheOrdiniClienti.setLibDbl2(libDbl2);
	}

	/**
	* Returns the lib dbl3 of this w k righe ordini clienti.
	*
	* @return the lib dbl3 of this w k righe ordini clienti
	*/
	@Override
	public double getLibDbl3() {
		return _wkRigheOrdiniClienti.getLibDbl3();
	}

	/**
	* Sets the lib dbl3 of this w k righe ordini clienti.
	*
	* @param libDbl3 the lib dbl3 of this w k righe ordini clienti
	*/
	@Override
	public void setLibDbl3(double libDbl3) {
		_wkRigheOrdiniClienti.setLibDbl3(libDbl3);
	}

	/**
	* Returns the lib dat1 of this w k righe ordini clienti.
	*
	* @return the lib dat1 of this w k righe ordini clienti
	*/
	@Override
	public java.util.Date getLibDat1() {
		return _wkRigheOrdiniClienti.getLibDat1();
	}

	/**
	* Sets the lib dat1 of this w k righe ordini clienti.
	*
	* @param libDat1 the lib dat1 of this w k righe ordini clienti
	*/
	@Override
	public void setLibDat1(java.util.Date libDat1) {
		_wkRigheOrdiniClienti.setLibDat1(libDat1);
	}

	/**
	* Returns the lib dat2 of this w k righe ordini clienti.
	*
	* @return the lib dat2 of this w k righe ordini clienti
	*/
	@Override
	public java.util.Date getLibDat2() {
		return _wkRigheOrdiniClienti.getLibDat2();
	}

	/**
	* Sets the lib dat2 of this w k righe ordini clienti.
	*
	* @param libDat2 the lib dat2 of this w k righe ordini clienti
	*/
	@Override
	public void setLibDat2(java.util.Date libDat2) {
		_wkRigheOrdiniClienti.setLibDat2(libDat2);
	}

	/**
	* Returns the lib dat3 of this w k righe ordini clienti.
	*
	* @return the lib dat3 of this w k righe ordini clienti
	*/
	@Override
	public java.util.Date getLibDat3() {
		return _wkRigheOrdiniClienti.getLibDat3();
	}

	/**
	* Sets the lib dat3 of this w k righe ordini clienti.
	*
	* @param libDat3 the lib dat3 of this w k righe ordini clienti
	*/
	@Override
	public void setLibDat3(java.util.Date libDat3) {
		_wkRigheOrdiniClienti.setLibDat3(libDat3);
	}

	/**
	* Returns the lib lng1 of this w k righe ordini clienti.
	*
	* @return the lib lng1 of this w k righe ordini clienti
	*/
	@Override
	public long getLibLng1() {
		return _wkRigheOrdiniClienti.getLibLng1();
	}

	/**
	* Sets the lib lng1 of this w k righe ordini clienti.
	*
	* @param libLng1 the lib lng1 of this w k righe ordini clienti
	*/
	@Override
	public void setLibLng1(long libLng1) {
		_wkRigheOrdiniClienti.setLibLng1(libLng1);
	}

	/**
	* Returns the lib lng2 of this w k righe ordini clienti.
	*
	* @return the lib lng2 of this w k righe ordini clienti
	*/
	@Override
	public long getLibLng2() {
		return _wkRigheOrdiniClienti.getLibLng2();
	}

	/**
	* Sets the lib lng2 of this w k righe ordini clienti.
	*
	* @param libLng2 the lib lng2 of this w k righe ordini clienti
	*/
	@Override
	public void setLibLng2(long libLng2) {
		_wkRigheOrdiniClienti.setLibLng2(libLng2);
	}

	/**
	* Returns the lib lng3 of this w k righe ordini clienti.
	*
	* @return the lib lng3 of this w k righe ordini clienti
	*/
	@Override
	public long getLibLng3() {
		return _wkRigheOrdiniClienti.getLibLng3();
	}

	/**
	* Sets the lib lng3 of this w k righe ordini clienti.
	*
	* @param libLng3 the lib lng3 of this w k righe ordini clienti
	*/
	@Override
	public void setLibLng3(long libLng3) {
		_wkRigheOrdiniClienti.setLibLng3(libLng3);
	}

	@Override
	public boolean isNew() {
		return _wkRigheOrdiniClienti.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_wkRigheOrdiniClienti.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _wkRigheOrdiniClienti.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_wkRigheOrdiniClienti.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _wkRigheOrdiniClienti.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _wkRigheOrdiniClienti.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_wkRigheOrdiniClienti.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _wkRigheOrdiniClienti.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_wkRigheOrdiniClienti.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_wkRigheOrdiniClienti.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_wkRigheOrdiniClienti.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new WKRigheOrdiniClientiWrapper((WKRigheOrdiniClienti)_wkRigheOrdiniClienti.clone());
	}

	@Override
	public int compareTo(
		it.bysoftware.ct.model.WKRigheOrdiniClienti wkRigheOrdiniClienti) {
		return _wkRigheOrdiniClienti.compareTo(wkRigheOrdiniClienti);
	}

	@Override
	public int hashCode() {
		return _wkRigheOrdiniClienti.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.WKRigheOrdiniClienti> toCacheModel() {
		return _wkRigheOrdiniClienti.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.WKRigheOrdiniClienti toEscapedModel() {
		return new WKRigheOrdiniClientiWrapper(_wkRigheOrdiniClienti.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.WKRigheOrdiniClienti toUnescapedModel() {
		return new WKRigheOrdiniClientiWrapper(_wkRigheOrdiniClienti.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _wkRigheOrdiniClienti.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _wkRigheOrdiniClienti.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_wkRigheOrdiniClienti.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof WKRigheOrdiniClientiWrapper)) {
			return false;
		}

		WKRigheOrdiniClientiWrapper wkRigheOrdiniClientiWrapper = (WKRigheOrdiniClientiWrapper)obj;

		if (Validator.equals(_wkRigheOrdiniClienti,
					wkRigheOrdiniClientiWrapper._wkRigheOrdiniClienti)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public WKRigheOrdiniClienti getWrappedWKRigheOrdiniClienti() {
		return _wkRigheOrdiniClienti;
	}

	@Override
	public WKRigheOrdiniClienti getWrappedModel() {
		return _wkRigheOrdiniClienti;
	}

	@Override
	public void resetOriginalValues() {
		_wkRigheOrdiniClienti.resetOriginalValues();
	}

	private WKRigheOrdiniClienti _wkRigheOrdiniClienti;
}