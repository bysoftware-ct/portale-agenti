/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.MovimentiMagazzinoServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.MovimentiMagazzinoServiceSoap
 * @generated
 */
public class MovimentiMagazzinoSoap implements Serializable {
	public static MovimentiMagazzinoSoap toSoapModel(MovimentiMagazzino model) {
		MovimentiMagazzinoSoap soapModel = new MovimentiMagazzinoSoap();

		soapModel.setID(model.getID());
		soapModel.setAnno(model.getAnno());
		soapModel.setCodiceArticolo(model.getCodiceArticolo());
		soapModel.setCodiceVariante(model.getCodiceVariante());
		soapModel.setDescrizione(model.getDescrizione());
		soapModel.setUnitaMisura(model.getUnitaMisura());
		soapModel.setQuantita(model.getQuantita());
		soapModel.setTestCaricoScarico(model.getTestCaricoScarico());
		soapModel.setDataRegistrazione(model.getDataRegistrazione());
		soapModel.setTipoSoggetto(model.getTipoSoggetto());
		soapModel.setCodiceSoggetto(model.getCodiceSoggetto());
		soapModel.setRagioneSociale(model.getRagioneSociale());
		soapModel.setEstremiDocumenti(model.getEstremiDocumenti());
		soapModel.setDataDocumento(model.getDataDocumento());
		soapModel.setNumeroDocumento(model.getNumeroDocumento());
		soapModel.setTipoDocumento(model.getTipoDocumento());
		soapModel.setSoloValore(model.getSoloValore());

		return soapModel;
	}

	public static MovimentiMagazzinoSoap[] toSoapModels(
		MovimentiMagazzino[] models) {
		MovimentiMagazzinoSoap[] soapModels = new MovimentiMagazzinoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static MovimentiMagazzinoSoap[][] toSoapModels(
		MovimentiMagazzino[][] models) {
		MovimentiMagazzinoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new MovimentiMagazzinoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new MovimentiMagazzinoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static MovimentiMagazzinoSoap[] toSoapModels(
		List<MovimentiMagazzino> models) {
		List<MovimentiMagazzinoSoap> soapModels = new ArrayList<MovimentiMagazzinoSoap>(models.size());

		for (MovimentiMagazzino model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new MovimentiMagazzinoSoap[soapModels.size()]);
	}

	public MovimentiMagazzinoSoap() {
	}

	public String getPrimaryKey() {
		return _ID;
	}

	public void setPrimaryKey(String pk) {
		setID(pk);
	}

	public String getID() {
		return _ID;
	}

	public void setID(String ID) {
		_ID = ID;
	}

	public int getAnno() {
		return _anno;
	}

	public void setAnno(int anno) {
		_anno = anno;
	}

	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;
	}

	public String getCodiceVariante() {
		return _codiceVariante;
	}

	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;
	}

	public String getDescrizione() {
		return _descrizione;
	}

	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;
	}

	public String getUnitaMisura() {
		return _unitaMisura;
	}

	public void setUnitaMisura(String unitaMisura) {
		_unitaMisura = unitaMisura;
	}

	public Double getQuantita() {
		return _quantita;
	}

	public void setQuantita(Double quantita) {
		_quantita = quantita;
	}

	public int getTestCaricoScarico() {
		return _testCaricoScarico;
	}

	public void setTestCaricoScarico(int testCaricoScarico) {
		_testCaricoScarico = testCaricoScarico;
	}

	public Date getDataRegistrazione() {
		return _dataRegistrazione;
	}

	public void setDataRegistrazione(Date dataRegistrazione) {
		_dataRegistrazione = dataRegistrazione;
	}

	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;
	}

	public String getCodiceSoggetto() {
		return _codiceSoggetto;
	}

	public void setCodiceSoggetto(String codiceSoggetto) {
		_codiceSoggetto = codiceSoggetto;
	}

	public String getRagioneSociale() {
		return _ragioneSociale;
	}

	public void setRagioneSociale(String ragioneSociale) {
		_ragioneSociale = ragioneSociale;
	}

	public String getEstremiDocumenti() {
		return _estremiDocumenti;
	}

	public void setEstremiDocumenti(String estremiDocumenti) {
		_estremiDocumenti = estremiDocumenti;
	}

	public Date getDataDocumento() {
		return _dataDocumento;
	}

	public void setDataDocumento(Date dataDocumento) {
		_dataDocumento = dataDocumento;
	}

	public int getNumeroDocumento() {
		return _numeroDocumento;
	}

	public void setNumeroDocumento(int numeroDocumento) {
		_numeroDocumento = numeroDocumento;
	}

	public String getTipoDocumento() {
		return _tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		_tipoDocumento = tipoDocumento;
	}

	public int getSoloValore() {
		return _soloValore;
	}

	public void setSoloValore(int soloValore) {
		_soloValore = soloValore;
	}

	private String _ID;
	private int _anno;
	private String _codiceArticolo;
	private String _codiceVariante;
	private String _descrizione;
	private String _unitaMisura;
	private Double _quantita;
	private int _testCaricoScarico;
	private Date _dataRegistrazione;
	private boolean _tipoSoggetto;
	private String _codiceSoggetto;
	private String _ragioneSociale;
	private String _estremiDocumenti;
	private Date _dataDocumento;
	private int _numeroDocumento;
	private String _tipoDocumento;
	private int _soloValore;
}