/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.TestataFattureClientiPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.TestataFattureClientiServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.TestataFattureClientiServiceSoap
 * @generated
 */
public class TestataFattureClientiSoap implements Serializable {
	public static TestataFattureClientiSoap toSoapModel(
		TestataFattureClienti model) {
		TestataFattureClientiSoap soapModel = new TestataFattureClientiSoap();

		soapModel.setAnno(model.getAnno());
		soapModel.setCodiceAttivita(model.getCodiceAttivita());
		soapModel.setCodiceCentro(model.getCodiceCentro());
		soapModel.setNumeroProtocollo(model.getNumeroProtocollo());
		soapModel.setTipoDocumento(model.getTipoDocumento());
		soapModel.setCodiceCentroContAnalitica(model.getCodiceCentroContAnalitica());
		soapModel.setIdTipoDocumentoOrigine(model.getIdTipoDocumentoOrigine());
		soapModel.setStatoFattura(model.getStatoFattura());
		soapModel.setDataRegistrazione(model.getDataRegistrazione());
		soapModel.setDataOperazione(model.getDataOperazione());
		soapModel.setDataAnnotazione(model.getDataAnnotazione());
		soapModel.setDataDocumento(model.getDataDocumento());
		soapModel.setNumeroDocumento(model.getNumeroDocumento());
		soapModel.setRiferimentoAScontrino(model.getRiferimentoAScontrino());
		soapModel.setDescrizioneEstremiDocumento(model.getDescrizioneEstremiDocumento());
		soapModel.setTipoSoggetto(model.getTipoSoggetto());
		soapModel.setCodiceCliente(model.getCodiceCliente());
		soapModel.setDescrizioneAggiuntivaFattura(model.getDescrizioneAggiuntivaFattura());
		soapModel.setEstremiOrdine(model.getEstremiOrdine());
		soapModel.setEstremiBolla(model.getEstremiBolla());
		soapModel.setCodicePagamento(model.getCodicePagamento());
		soapModel.setCodiceAgente(model.getCodiceAgente());
		soapModel.setCodiceGruppoAgenti(model.getCodiceGruppoAgenti());
		soapModel.setAnnotazioni(model.getAnnotazioni());
		soapModel.setScontoChiusura(model.getScontoChiusura());
		soapModel.setScontoProntaCassa(model.getScontoProntaCassa());
		soapModel.setPertualeSpeseTrasp(model.getPertualeSpeseTrasp());
		soapModel.setImportoSpeseTrasp(model.getImportoSpeseTrasp());
		soapModel.setImportoSpeseImb(model.getImportoSpeseImb());
		soapModel.setImportoSpeseVarie(model.getImportoSpeseVarie());
		soapModel.setImportoSpeseBancarie(model.getImportoSpeseBancarie());
		soapModel.setCodiceIVATrasp(model.getCodiceIVATrasp());
		soapModel.setCodiceIVAImb(model.getCodiceIVAImb());
		soapModel.setCodiceIVAVarie(model.getCodiceIVAVarie());
		soapModel.setCodiceIVABancarie(model.getCodiceIVABancarie());
		soapModel.setTotaleScontiCorpo(model.getTotaleScontiCorpo());
		soapModel.setImponibile(model.getImponibile());
		soapModel.setIVA(model.getIVA());
		soapModel.setImportoFattura(model.getImportoFattura());
		soapModel.setLibStr1(model.getLibStr1());
		soapModel.setLibStr2(model.getLibStr2());
		soapModel.setLibStr3(model.getLibStr3());
		soapModel.setLibDbl1(model.getLibDbl1());
		soapModel.setLibDbl2(model.getLibDbl2());
		soapModel.setLibDbl3(model.getLibDbl3());
		soapModel.setLibLng1(model.getLibLng1());
		soapModel.setLibLng2(model.getLibLng2());
		soapModel.setLibLng3(model.getLibLng3());
		soapModel.setLibDat1(model.getLibDat1());
		soapModel.setLibDat2(model.getLibDat2());
		soapModel.setLibDat3(model.getLibDat3());

		return soapModel;
	}

	public static TestataFattureClientiSoap[] toSoapModels(
		TestataFattureClienti[] models) {
		TestataFattureClientiSoap[] soapModels = new TestataFattureClientiSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TestataFattureClientiSoap[][] toSoapModels(
		TestataFattureClienti[][] models) {
		TestataFattureClientiSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TestataFattureClientiSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TestataFattureClientiSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TestataFattureClientiSoap[] toSoapModels(
		List<TestataFattureClienti> models) {
		List<TestataFattureClientiSoap> soapModels = new ArrayList<TestataFattureClientiSoap>(models.size());

		for (TestataFattureClienti model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TestataFattureClientiSoap[soapModels.size()]);
	}

	public TestataFattureClientiSoap() {
	}

	public TestataFattureClientiPK getPrimaryKey() {
		return new TestataFattureClientiPK(_anno, _codiceAttivita,
			_codiceCentro, _numeroProtocollo);
	}

	public void setPrimaryKey(TestataFattureClientiPK pk) {
		setAnno(pk.anno);
		setCodiceAttivita(pk.codiceAttivita);
		setCodiceCentro(pk.codiceCentro);
		setNumeroProtocollo(pk.numeroProtocollo);
	}

	public int getAnno() {
		return _anno;
	}

	public void setAnno(int anno) {
		_anno = anno;
	}

	public String getCodiceAttivita() {
		return _codiceAttivita;
	}

	public void setCodiceAttivita(String codiceAttivita) {
		_codiceAttivita = codiceAttivita;
	}

	public String getCodiceCentro() {
		return _codiceCentro;
	}

	public void setCodiceCentro(String codiceCentro) {
		_codiceCentro = codiceCentro;
	}

	public int getNumeroProtocollo() {
		return _numeroProtocollo;
	}

	public void setNumeroProtocollo(int numeroProtocollo) {
		_numeroProtocollo = numeroProtocollo;
	}

	public String getTipoDocumento() {
		return _tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		_tipoDocumento = tipoDocumento;
	}

	public String getCodiceCentroContAnalitica() {
		return _codiceCentroContAnalitica;
	}

	public void setCodiceCentroContAnalitica(String codiceCentroContAnalitica) {
		_codiceCentroContAnalitica = codiceCentroContAnalitica;
	}

	public int getIdTipoDocumentoOrigine() {
		return _idTipoDocumentoOrigine;
	}

	public void setIdTipoDocumentoOrigine(int idTipoDocumentoOrigine) {
		_idTipoDocumentoOrigine = idTipoDocumentoOrigine;
	}

	public boolean getStatoFattura() {
		return _statoFattura;
	}

	public boolean isStatoFattura() {
		return _statoFattura;
	}

	public void setStatoFattura(boolean statoFattura) {
		_statoFattura = statoFattura;
	}

	public Date getDataRegistrazione() {
		return _dataRegistrazione;
	}

	public void setDataRegistrazione(Date dataRegistrazione) {
		_dataRegistrazione = dataRegistrazione;
	}

	public Date getDataOperazione() {
		return _dataOperazione;
	}

	public void setDataOperazione(Date dataOperazione) {
		_dataOperazione = dataOperazione;
	}

	public Date getDataAnnotazione() {
		return _dataAnnotazione;
	}

	public void setDataAnnotazione(Date dataAnnotazione) {
		_dataAnnotazione = dataAnnotazione;
	}

	public Date getDataDocumento() {
		return _dataDocumento;
	}

	public void setDataDocumento(Date dataDocumento) {
		_dataDocumento = dataDocumento;
	}

	public int getNumeroDocumento() {
		return _numeroDocumento;
	}

	public void setNumeroDocumento(int numeroDocumento) {
		_numeroDocumento = numeroDocumento;
	}

	public int getRiferimentoAScontrino() {
		return _riferimentoAScontrino;
	}

	public void setRiferimentoAScontrino(int riferimentoAScontrino) {
		_riferimentoAScontrino = riferimentoAScontrino;
	}

	public String getDescrizioneEstremiDocumento() {
		return _descrizioneEstremiDocumento;
	}

	public void setDescrizioneEstremiDocumento(
		String descrizioneEstremiDocumento) {
		_descrizioneEstremiDocumento = descrizioneEstremiDocumento;
	}

	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;
	}

	public String getCodiceCliente() {
		return _codiceCliente;
	}

	public void setCodiceCliente(String codiceCliente) {
		_codiceCliente = codiceCliente;
	}

	public String getDescrizioneAggiuntivaFattura() {
		return _descrizioneAggiuntivaFattura;
	}

	public void setDescrizioneAggiuntivaFattura(
		String descrizioneAggiuntivaFattura) {
		_descrizioneAggiuntivaFattura = descrizioneAggiuntivaFattura;
	}

	public String getEstremiOrdine() {
		return _estremiOrdine;
	}

	public void setEstremiOrdine(String estremiOrdine) {
		_estremiOrdine = estremiOrdine;
	}

	public String getEstremiBolla() {
		return _estremiBolla;
	}

	public void setEstremiBolla(String estremiBolla) {
		_estremiBolla = estremiBolla;
	}

	public String getCodicePagamento() {
		return _codicePagamento;
	}

	public void setCodicePagamento(String codicePagamento) {
		_codicePagamento = codicePagamento;
	}

	public String getCodiceAgente() {
		return _codiceAgente;
	}

	public void setCodiceAgente(String codiceAgente) {
		_codiceAgente = codiceAgente;
	}

	public String getCodiceGruppoAgenti() {
		return _codiceGruppoAgenti;
	}

	public void setCodiceGruppoAgenti(String codiceGruppoAgenti) {
		_codiceGruppoAgenti = codiceGruppoAgenti;
	}

	public String getAnnotazioni() {
		return _annotazioni;
	}

	public void setAnnotazioni(String annotazioni) {
		_annotazioni = annotazioni;
	}

	public double getScontoChiusura() {
		return _scontoChiusura;
	}

	public void setScontoChiusura(double scontoChiusura) {
		_scontoChiusura = scontoChiusura;
	}

	public double getScontoProntaCassa() {
		return _scontoProntaCassa;
	}

	public void setScontoProntaCassa(double scontoProntaCassa) {
		_scontoProntaCassa = scontoProntaCassa;
	}

	public double getPertualeSpeseTrasp() {
		return _pertualeSpeseTrasp;
	}

	public void setPertualeSpeseTrasp(double pertualeSpeseTrasp) {
		_pertualeSpeseTrasp = pertualeSpeseTrasp;
	}

	public double getImportoSpeseTrasp() {
		return _importoSpeseTrasp;
	}

	public void setImportoSpeseTrasp(double importoSpeseTrasp) {
		_importoSpeseTrasp = importoSpeseTrasp;
	}

	public double getImportoSpeseImb() {
		return _importoSpeseImb;
	}

	public void setImportoSpeseImb(double importoSpeseImb) {
		_importoSpeseImb = importoSpeseImb;
	}

	public double getImportoSpeseVarie() {
		return _importoSpeseVarie;
	}

	public void setImportoSpeseVarie(double importoSpeseVarie) {
		_importoSpeseVarie = importoSpeseVarie;
	}

	public double getImportoSpeseBancarie() {
		return _importoSpeseBancarie;
	}

	public void setImportoSpeseBancarie(double importoSpeseBancarie) {
		_importoSpeseBancarie = importoSpeseBancarie;
	}

	public String getCodiceIVATrasp() {
		return _codiceIVATrasp;
	}

	public void setCodiceIVATrasp(String codiceIVATrasp) {
		_codiceIVATrasp = codiceIVATrasp;
	}

	public String getCodiceIVAImb() {
		return _codiceIVAImb;
	}

	public void setCodiceIVAImb(String codiceIVAImb) {
		_codiceIVAImb = codiceIVAImb;
	}

	public String getCodiceIVAVarie() {
		return _codiceIVAVarie;
	}

	public void setCodiceIVAVarie(String codiceIVAVarie) {
		_codiceIVAVarie = codiceIVAVarie;
	}

	public String getCodiceIVABancarie() {
		return _codiceIVABancarie;
	}

	public void setCodiceIVABancarie(String codiceIVABancarie) {
		_codiceIVABancarie = codiceIVABancarie;
	}

	public double getTotaleScontiCorpo() {
		return _totaleScontiCorpo;
	}

	public void setTotaleScontiCorpo(double totaleScontiCorpo) {
		_totaleScontiCorpo = totaleScontiCorpo;
	}

	public double getImponibile() {
		return _imponibile;
	}

	public void setImponibile(double imponibile) {
		_imponibile = imponibile;
	}

	public double getIVA() {
		return _IVA;
	}

	public void setIVA(double IVA) {
		_IVA = IVA;
	}

	public double getImportoFattura() {
		return _importoFattura;
	}

	public void setImportoFattura(double importoFattura) {
		_importoFattura = importoFattura;
	}

	public String getLibStr1() {
		return _libStr1;
	}

	public void setLibStr1(String libStr1) {
		_libStr1 = libStr1;
	}

	public String getLibStr2() {
		return _libStr2;
	}

	public void setLibStr2(String libStr2) {
		_libStr2 = libStr2;
	}

	public String getLibStr3() {
		return _libStr3;
	}

	public void setLibStr3(String libStr3) {
		_libStr3 = libStr3;
	}

	public double getLibDbl1() {
		return _libDbl1;
	}

	public void setLibDbl1(double libDbl1) {
		_libDbl1 = libDbl1;
	}

	public double getLibDbl2() {
		return _libDbl2;
	}

	public void setLibDbl2(double libDbl2) {
		_libDbl2 = libDbl2;
	}

	public double getLibDbl3() {
		return _libDbl3;
	}

	public void setLibDbl3(double libDbl3) {
		_libDbl3 = libDbl3;
	}

	public long getLibLng1() {
		return _libLng1;
	}

	public void setLibLng1(long libLng1) {
		_libLng1 = libLng1;
	}

	public long getLibLng2() {
		return _libLng2;
	}

	public void setLibLng2(long libLng2) {
		_libLng2 = libLng2;
	}

	public long getLibLng3() {
		return _libLng3;
	}

	public void setLibLng3(long libLng3) {
		_libLng3 = libLng3;
	}

	public Date getLibDat1() {
		return _libDat1;
	}

	public void setLibDat1(Date libDat1) {
		_libDat1 = libDat1;
	}

	public Date getLibDat2() {
		return _libDat2;
	}

	public void setLibDat2(Date libDat2) {
		_libDat2 = libDat2;
	}

	public Date getLibDat3() {
		return _libDat3;
	}

	public void setLibDat3(Date libDat3) {
		_libDat3 = libDat3;
	}

	private int _anno;
	private String _codiceAttivita;
	private String _codiceCentro;
	private int _numeroProtocollo;
	private String _tipoDocumento;
	private String _codiceCentroContAnalitica;
	private int _idTipoDocumentoOrigine;
	private boolean _statoFattura;
	private Date _dataRegistrazione;
	private Date _dataOperazione;
	private Date _dataAnnotazione;
	private Date _dataDocumento;
	private int _numeroDocumento;
	private int _riferimentoAScontrino;
	private String _descrizioneEstremiDocumento;
	private boolean _tipoSoggetto;
	private String _codiceCliente;
	private String _descrizioneAggiuntivaFattura;
	private String _estremiOrdine;
	private String _estremiBolla;
	private String _codicePagamento;
	private String _codiceAgente;
	private String _codiceGruppoAgenti;
	private String _annotazioni;
	private double _scontoChiusura;
	private double _scontoProntaCassa;
	private double _pertualeSpeseTrasp;
	private double _importoSpeseTrasp;
	private double _importoSpeseImb;
	private double _importoSpeseVarie;
	private double _importoSpeseBancarie;
	private String _codiceIVATrasp;
	private String _codiceIVAImb;
	private String _codiceIVAVarie;
	private String _codiceIVABancarie;
	private double _totaleScontiCorpo;
	private double _imponibile;
	private double _IVA;
	private double _importoFattura;
	private String _libStr1;
	private String _libStr2;
	private String _libStr3;
	private double _libDbl1;
	private double _libDbl2;
	private double _libDbl3;
	private long _libLng1;
	private long _libLng2;
	private long _libLng3;
	private Date _libDat1;
	private Date _libDat2;
	private Date _libDat3;
}