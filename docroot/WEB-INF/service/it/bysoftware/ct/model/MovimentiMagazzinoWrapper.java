/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link MovimentiMagazzino}.
 * </p>
 *
 * @author Mario Torrisi
 * @see MovimentiMagazzino
 * @generated
 */
public class MovimentiMagazzinoWrapper implements MovimentiMagazzino,
	ModelWrapper<MovimentiMagazzino> {
	public MovimentiMagazzinoWrapper(MovimentiMagazzino movimentiMagazzino) {
		_movimentiMagazzino = movimentiMagazzino;
	}

	@Override
	public Class<?> getModelClass() {
		return MovimentiMagazzino.class;
	}

	@Override
	public String getModelClassName() {
		return MovimentiMagazzino.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ID", getID());
		attributes.put("anno", getAnno());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("descrizione", getDescrizione());
		attributes.put("unitaMisura", getUnitaMisura());
		attributes.put("quantita", getQuantita());
		attributes.put("testCaricoScarico", getTestCaricoScarico());
		attributes.put("dataRegistrazione", getDataRegistrazione());
		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceSoggetto", getCodiceSoggetto());
		attributes.put("ragioneSociale", getRagioneSociale());
		attributes.put("estremiDocumenti", getEstremiDocumenti());
		attributes.put("dataDocumento", getDataDocumento());
		attributes.put("numeroDocumento", getNumeroDocumento());
		attributes.put("tipoDocumento", getTipoDocumento());
		attributes.put("soloValore", getSoloValore());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String ID = (String)attributes.get("ID");

		if (ID != null) {
			setID(ID);
		}

		Integer anno = (Integer)attributes.get("anno");

		if (anno != null) {
			setAnno(anno);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		String unitaMisura = (String)attributes.get("unitaMisura");

		if (unitaMisura != null) {
			setUnitaMisura(unitaMisura);
		}

		Double quantita = (Double)attributes.get("quantita");

		if (quantita != null) {
			setQuantita(quantita);
		}

		Integer testCaricoScarico = (Integer)attributes.get("testCaricoScarico");

		if (testCaricoScarico != null) {
			setTestCaricoScarico(testCaricoScarico);
		}

		Date dataRegistrazione = (Date)attributes.get("dataRegistrazione");

		if (dataRegistrazione != null) {
			setDataRegistrazione(dataRegistrazione);
		}

		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceSoggetto = (String)attributes.get("codiceSoggetto");

		if (codiceSoggetto != null) {
			setCodiceSoggetto(codiceSoggetto);
		}

		String ragioneSociale = (String)attributes.get("ragioneSociale");

		if (ragioneSociale != null) {
			setRagioneSociale(ragioneSociale);
		}

		String estremiDocumenti = (String)attributes.get("estremiDocumenti");

		if (estremiDocumenti != null) {
			setEstremiDocumenti(estremiDocumenti);
		}

		Date dataDocumento = (Date)attributes.get("dataDocumento");

		if (dataDocumento != null) {
			setDataDocumento(dataDocumento);
		}

		Integer numeroDocumento = (Integer)attributes.get("numeroDocumento");

		if (numeroDocumento != null) {
			setNumeroDocumento(numeroDocumento);
		}

		String tipoDocumento = (String)attributes.get("tipoDocumento");

		if (tipoDocumento != null) {
			setTipoDocumento(tipoDocumento);
		}

		Integer soloValore = (Integer)attributes.get("soloValore");

		if (soloValore != null) {
			setSoloValore(soloValore);
		}
	}

	/**
	* Returns the primary key of this movimenti magazzino.
	*
	* @return the primary key of this movimenti magazzino
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _movimentiMagazzino.getPrimaryKey();
	}

	/**
	* Sets the primary key of this movimenti magazzino.
	*
	* @param primaryKey the primary key of this movimenti magazzino
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_movimentiMagazzino.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the i d of this movimenti magazzino.
	*
	* @return the i d of this movimenti magazzino
	*/
	@Override
	public java.lang.String getID() {
		return _movimentiMagazzino.getID();
	}

	/**
	* Sets the i d of this movimenti magazzino.
	*
	* @param ID the i d of this movimenti magazzino
	*/
	@Override
	public void setID(java.lang.String ID) {
		_movimentiMagazzino.setID(ID);
	}

	/**
	* Returns the anno of this movimenti magazzino.
	*
	* @return the anno of this movimenti magazzino
	*/
	@Override
	public int getAnno() {
		return _movimentiMagazzino.getAnno();
	}

	/**
	* Sets the anno of this movimenti magazzino.
	*
	* @param anno the anno of this movimenti magazzino
	*/
	@Override
	public void setAnno(int anno) {
		_movimentiMagazzino.setAnno(anno);
	}

	/**
	* Returns the codice articolo of this movimenti magazzino.
	*
	* @return the codice articolo of this movimenti magazzino
	*/
	@Override
	public java.lang.String getCodiceArticolo() {
		return _movimentiMagazzino.getCodiceArticolo();
	}

	/**
	* Sets the codice articolo of this movimenti magazzino.
	*
	* @param codiceArticolo the codice articolo of this movimenti magazzino
	*/
	@Override
	public void setCodiceArticolo(java.lang.String codiceArticolo) {
		_movimentiMagazzino.setCodiceArticolo(codiceArticolo);
	}

	/**
	* Returns the codice variante of this movimenti magazzino.
	*
	* @return the codice variante of this movimenti magazzino
	*/
	@Override
	public java.lang.String getCodiceVariante() {
		return _movimentiMagazzino.getCodiceVariante();
	}

	/**
	* Sets the codice variante of this movimenti magazzino.
	*
	* @param codiceVariante the codice variante of this movimenti magazzino
	*/
	@Override
	public void setCodiceVariante(java.lang.String codiceVariante) {
		_movimentiMagazzino.setCodiceVariante(codiceVariante);
	}

	/**
	* Returns the descrizione of this movimenti magazzino.
	*
	* @return the descrizione of this movimenti magazzino
	*/
	@Override
	public java.lang.String getDescrizione() {
		return _movimentiMagazzino.getDescrizione();
	}

	/**
	* Sets the descrizione of this movimenti magazzino.
	*
	* @param descrizione the descrizione of this movimenti magazzino
	*/
	@Override
	public void setDescrizione(java.lang.String descrizione) {
		_movimentiMagazzino.setDescrizione(descrizione);
	}

	/**
	* Returns the unita misura of this movimenti magazzino.
	*
	* @return the unita misura of this movimenti magazzino
	*/
	@Override
	public java.lang.String getUnitaMisura() {
		return _movimentiMagazzino.getUnitaMisura();
	}

	/**
	* Sets the unita misura of this movimenti magazzino.
	*
	* @param unitaMisura the unita misura of this movimenti magazzino
	*/
	@Override
	public void setUnitaMisura(java.lang.String unitaMisura) {
		_movimentiMagazzino.setUnitaMisura(unitaMisura);
	}

	/**
	* Returns the quantita of this movimenti magazzino.
	*
	* @return the quantita of this movimenti magazzino
	*/
	@Override
	public java.lang.Double getQuantita() {
		return _movimentiMagazzino.getQuantita();
	}

	/**
	* Sets the quantita of this movimenti magazzino.
	*
	* @param quantita the quantita of this movimenti magazzino
	*/
	@Override
	public void setQuantita(java.lang.Double quantita) {
		_movimentiMagazzino.setQuantita(quantita);
	}

	/**
	* Returns the test carico scarico of this movimenti magazzino.
	*
	* @return the test carico scarico of this movimenti magazzino
	*/
	@Override
	public int getTestCaricoScarico() {
		return _movimentiMagazzino.getTestCaricoScarico();
	}

	/**
	* Sets the test carico scarico of this movimenti magazzino.
	*
	* @param testCaricoScarico the test carico scarico of this movimenti magazzino
	*/
	@Override
	public void setTestCaricoScarico(int testCaricoScarico) {
		_movimentiMagazzino.setTestCaricoScarico(testCaricoScarico);
	}

	/**
	* Returns the data registrazione of this movimenti magazzino.
	*
	* @return the data registrazione of this movimenti magazzino
	*/
	@Override
	public java.util.Date getDataRegistrazione() {
		return _movimentiMagazzino.getDataRegistrazione();
	}

	/**
	* Sets the data registrazione of this movimenti magazzino.
	*
	* @param dataRegistrazione the data registrazione of this movimenti magazzino
	*/
	@Override
	public void setDataRegistrazione(java.util.Date dataRegistrazione) {
		_movimentiMagazzino.setDataRegistrazione(dataRegistrazione);
	}

	/**
	* Returns the tipo soggetto of this movimenti magazzino.
	*
	* @return the tipo soggetto of this movimenti magazzino
	*/
	@Override
	public boolean getTipoSoggetto() {
		return _movimentiMagazzino.getTipoSoggetto();
	}

	/**
	* Returns <code>true</code> if this movimenti magazzino is tipo soggetto.
	*
	* @return <code>true</code> if this movimenti magazzino is tipo soggetto; <code>false</code> otherwise
	*/
	@Override
	public boolean isTipoSoggetto() {
		return _movimentiMagazzino.isTipoSoggetto();
	}

	/**
	* Sets whether this movimenti magazzino is tipo soggetto.
	*
	* @param tipoSoggetto the tipo soggetto of this movimenti magazzino
	*/
	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_movimentiMagazzino.setTipoSoggetto(tipoSoggetto);
	}

	/**
	* Returns the codice soggetto of this movimenti magazzino.
	*
	* @return the codice soggetto of this movimenti magazzino
	*/
	@Override
	public java.lang.String getCodiceSoggetto() {
		return _movimentiMagazzino.getCodiceSoggetto();
	}

	/**
	* Sets the codice soggetto of this movimenti magazzino.
	*
	* @param codiceSoggetto the codice soggetto of this movimenti magazzino
	*/
	@Override
	public void setCodiceSoggetto(java.lang.String codiceSoggetto) {
		_movimentiMagazzino.setCodiceSoggetto(codiceSoggetto);
	}

	/**
	* Returns the ragione sociale of this movimenti magazzino.
	*
	* @return the ragione sociale of this movimenti magazzino
	*/
	@Override
	public java.lang.String getRagioneSociale() {
		return _movimentiMagazzino.getRagioneSociale();
	}

	/**
	* Sets the ragione sociale of this movimenti magazzino.
	*
	* @param ragioneSociale the ragione sociale of this movimenti magazzino
	*/
	@Override
	public void setRagioneSociale(java.lang.String ragioneSociale) {
		_movimentiMagazzino.setRagioneSociale(ragioneSociale);
	}

	/**
	* Returns the estremi documenti of this movimenti magazzino.
	*
	* @return the estremi documenti of this movimenti magazzino
	*/
	@Override
	public java.lang.String getEstremiDocumenti() {
		return _movimentiMagazzino.getEstremiDocumenti();
	}

	/**
	* Sets the estremi documenti of this movimenti magazzino.
	*
	* @param estremiDocumenti the estremi documenti of this movimenti magazzino
	*/
	@Override
	public void setEstremiDocumenti(java.lang.String estremiDocumenti) {
		_movimentiMagazzino.setEstremiDocumenti(estremiDocumenti);
	}

	/**
	* Returns the data documento of this movimenti magazzino.
	*
	* @return the data documento of this movimenti magazzino
	*/
	@Override
	public java.util.Date getDataDocumento() {
		return _movimentiMagazzino.getDataDocumento();
	}

	/**
	* Sets the data documento of this movimenti magazzino.
	*
	* @param dataDocumento the data documento of this movimenti magazzino
	*/
	@Override
	public void setDataDocumento(java.util.Date dataDocumento) {
		_movimentiMagazzino.setDataDocumento(dataDocumento);
	}

	/**
	* Returns the numero documento of this movimenti magazzino.
	*
	* @return the numero documento of this movimenti magazzino
	*/
	@Override
	public int getNumeroDocumento() {
		return _movimentiMagazzino.getNumeroDocumento();
	}

	/**
	* Sets the numero documento of this movimenti magazzino.
	*
	* @param numeroDocumento the numero documento of this movimenti magazzino
	*/
	@Override
	public void setNumeroDocumento(int numeroDocumento) {
		_movimentiMagazzino.setNumeroDocumento(numeroDocumento);
	}

	/**
	* Returns the tipo documento of this movimenti magazzino.
	*
	* @return the tipo documento of this movimenti magazzino
	*/
	@Override
	public java.lang.String getTipoDocumento() {
		return _movimentiMagazzino.getTipoDocumento();
	}

	/**
	* Sets the tipo documento of this movimenti magazzino.
	*
	* @param tipoDocumento the tipo documento of this movimenti magazzino
	*/
	@Override
	public void setTipoDocumento(java.lang.String tipoDocumento) {
		_movimentiMagazzino.setTipoDocumento(tipoDocumento);
	}

	/**
	* Returns the solo valore of this movimenti magazzino.
	*
	* @return the solo valore of this movimenti magazzino
	*/
	@Override
	public int getSoloValore() {
		return _movimentiMagazzino.getSoloValore();
	}

	/**
	* Sets the solo valore of this movimenti magazzino.
	*
	* @param soloValore the solo valore of this movimenti magazzino
	*/
	@Override
	public void setSoloValore(int soloValore) {
		_movimentiMagazzino.setSoloValore(soloValore);
	}

	@Override
	public boolean isNew() {
		return _movimentiMagazzino.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_movimentiMagazzino.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _movimentiMagazzino.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_movimentiMagazzino.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _movimentiMagazzino.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _movimentiMagazzino.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_movimentiMagazzino.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _movimentiMagazzino.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_movimentiMagazzino.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_movimentiMagazzino.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_movimentiMagazzino.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new MovimentiMagazzinoWrapper((MovimentiMagazzino)_movimentiMagazzino.clone());
	}

	@Override
	public int compareTo(
		it.bysoftware.ct.model.MovimentiMagazzino movimentiMagazzino) {
		return _movimentiMagazzino.compareTo(movimentiMagazzino);
	}

	@Override
	public int hashCode() {
		return _movimentiMagazzino.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.MovimentiMagazzino> toCacheModel() {
		return _movimentiMagazzino.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.MovimentiMagazzino toEscapedModel() {
		return new MovimentiMagazzinoWrapper(_movimentiMagazzino.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.MovimentiMagazzino toUnescapedModel() {
		return new MovimentiMagazzinoWrapper(_movimentiMagazzino.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _movimentiMagazzino.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _movimentiMagazzino.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_movimentiMagazzino.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MovimentiMagazzinoWrapper)) {
			return false;
		}

		MovimentiMagazzinoWrapper movimentiMagazzinoWrapper = (MovimentiMagazzinoWrapper)obj;

		if (Validator.equals(_movimentiMagazzino,
					movimentiMagazzinoWrapper._movimentiMagazzino)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public MovimentiMagazzino getWrappedMovimentiMagazzino() {
		return _movimentiMagazzino;
	}

	@Override
	public MovimentiMagazzino getWrappedModel() {
		return _movimentiMagazzino;
	}

	@Override
	public void resetOriginalValues() {
		_movimentiMagazzino.resetOriginalValues();
	}

	private MovimentiMagazzino _movimentiMagazzino;
}