/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link EvasioniOrdini}.
 * </p>
 *
 * @author Mario Torrisi
 * @see EvasioniOrdini
 * @generated
 */
public class EvasioniOrdiniWrapper implements EvasioniOrdini,
	ModelWrapper<EvasioniOrdini> {
	public EvasioniOrdiniWrapper(EvasioniOrdini evasioniOrdini) {
		_evasioniOrdini = evasioniOrdini;
	}

	@Override
	public Class<?> getModelClass() {
		return EvasioniOrdini.class;
	}

	@Override
	public String getModelClassName() {
		return EvasioniOrdini.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ID", getID());
		attributes.put("anno", getAnno());
		attributes.put("codiceAttivita", getCodiceAttivita());
		attributes.put("codiceCentro", getCodiceCentro());
		attributes.put("codiceDeposito", getCodiceDeposito());
		attributes.put("tipoOrdine", getTipoOrdine());
		attributes.put("numeroOrdine", getNumeroOrdine());
		attributes.put("codiceCliente", getCodiceCliente());
		attributes.put("numeroRigo", getNumeroRigo());
		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("codiceVariante", getCodiceVariante());
		attributes.put("descrizione", getDescrizione());
		attributes.put("unitaMisura", getUnitaMisura());
		attributes.put("quantitaOrdinata", getQuantitaOrdinata());
		attributes.put("numeroProgrezzivoEvasione",
			getNumeroProgrezzivoEvasione());
		attributes.put("quantitaConsegnata", getQuantitaConsegnata());
		attributes.put("dataDocumentoConsegna", getDataDocumentoConsegna());
		attributes.put("testAccontoSaldo", getTestAccontoSaldo());
		attributes.put("tipoDocumentoEvasione", getTipoDocumentoEvasione());
		attributes.put("numeroDocumentoEvasione", getNumeroDocumentoEvasione());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String ID = (String)attributes.get("ID");

		if (ID != null) {
			setID(ID);
		}

		Integer anno = (Integer)attributes.get("anno");

		if (anno != null) {
			setAnno(anno);
		}

		String codiceAttivita = (String)attributes.get("codiceAttivita");

		if (codiceAttivita != null) {
			setCodiceAttivita(codiceAttivita);
		}

		String codiceCentro = (String)attributes.get("codiceCentro");

		if (codiceCentro != null) {
			setCodiceCentro(codiceCentro);
		}

		String codiceDeposito = (String)attributes.get("codiceDeposito");

		if (codiceDeposito != null) {
			setCodiceDeposito(codiceDeposito);
		}

		Integer tipoOrdine = (Integer)attributes.get("tipoOrdine");

		if (tipoOrdine != null) {
			setTipoOrdine(tipoOrdine);
		}

		Integer numeroOrdine = (Integer)attributes.get("numeroOrdine");

		if (numeroOrdine != null) {
			setNumeroOrdine(numeroOrdine);
		}

		String codiceCliente = (String)attributes.get("codiceCliente");

		if (codiceCliente != null) {
			setCodiceCliente(codiceCliente);
		}

		Integer numeroRigo = (Integer)attributes.get("numeroRigo");

		if (numeroRigo != null) {
			setNumeroRigo(numeroRigo);
		}

		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String codiceVariante = (String)attributes.get("codiceVariante");

		if (codiceVariante != null) {
			setCodiceVariante(codiceVariante);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		String unitaMisura = (String)attributes.get("unitaMisura");

		if (unitaMisura != null) {
			setUnitaMisura(unitaMisura);
		}

		Double quantitaOrdinata = (Double)attributes.get("quantitaOrdinata");

		if (quantitaOrdinata != null) {
			setQuantitaOrdinata(quantitaOrdinata);
		}

		Integer numeroProgrezzivoEvasione = (Integer)attributes.get(
				"numeroProgrezzivoEvasione");

		if (numeroProgrezzivoEvasione != null) {
			setNumeroProgrezzivoEvasione(numeroProgrezzivoEvasione);
		}

		Double quantitaConsegnata = (Double)attributes.get("quantitaConsegnata");

		if (quantitaConsegnata != null) {
			setQuantitaConsegnata(quantitaConsegnata);
		}

		Date dataDocumentoConsegna = (Date)attributes.get(
				"dataDocumentoConsegna");

		if (dataDocumentoConsegna != null) {
			setDataDocumentoConsegna(dataDocumentoConsegna);
		}

		String testAccontoSaldo = (String)attributes.get("testAccontoSaldo");

		if (testAccontoSaldo != null) {
			setTestAccontoSaldo(testAccontoSaldo);
		}

		String tipoDocumentoEvasione = (String)attributes.get(
				"tipoDocumentoEvasione");

		if (tipoDocumentoEvasione != null) {
			setTipoDocumentoEvasione(tipoDocumentoEvasione);
		}

		Integer numeroDocumentoEvasione = (Integer)attributes.get(
				"numeroDocumentoEvasione");

		if (numeroDocumentoEvasione != null) {
			setNumeroDocumentoEvasione(numeroDocumentoEvasione);
		}
	}

	/**
	* Returns the primary key of this evasioni ordini.
	*
	* @return the primary key of this evasioni ordini
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _evasioniOrdini.getPrimaryKey();
	}

	/**
	* Sets the primary key of this evasioni ordini.
	*
	* @param primaryKey the primary key of this evasioni ordini
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_evasioniOrdini.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the i d of this evasioni ordini.
	*
	* @return the i d of this evasioni ordini
	*/
	@Override
	public java.lang.String getID() {
		return _evasioniOrdini.getID();
	}

	/**
	* Sets the i d of this evasioni ordini.
	*
	* @param ID the i d of this evasioni ordini
	*/
	@Override
	public void setID(java.lang.String ID) {
		_evasioniOrdini.setID(ID);
	}

	/**
	* Returns the anno of this evasioni ordini.
	*
	* @return the anno of this evasioni ordini
	*/
	@Override
	public int getAnno() {
		return _evasioniOrdini.getAnno();
	}

	/**
	* Sets the anno of this evasioni ordini.
	*
	* @param anno the anno of this evasioni ordini
	*/
	@Override
	public void setAnno(int anno) {
		_evasioniOrdini.setAnno(anno);
	}

	/**
	* Returns the codice attivita of this evasioni ordini.
	*
	* @return the codice attivita of this evasioni ordini
	*/
	@Override
	public java.lang.String getCodiceAttivita() {
		return _evasioniOrdini.getCodiceAttivita();
	}

	/**
	* Sets the codice attivita of this evasioni ordini.
	*
	* @param codiceAttivita the codice attivita of this evasioni ordini
	*/
	@Override
	public void setCodiceAttivita(java.lang.String codiceAttivita) {
		_evasioniOrdini.setCodiceAttivita(codiceAttivita);
	}

	/**
	* Returns the codice centro of this evasioni ordini.
	*
	* @return the codice centro of this evasioni ordini
	*/
	@Override
	public java.lang.String getCodiceCentro() {
		return _evasioniOrdini.getCodiceCentro();
	}

	/**
	* Sets the codice centro of this evasioni ordini.
	*
	* @param codiceCentro the codice centro of this evasioni ordini
	*/
	@Override
	public void setCodiceCentro(java.lang.String codiceCentro) {
		_evasioniOrdini.setCodiceCentro(codiceCentro);
	}

	/**
	* Returns the codice deposito of this evasioni ordini.
	*
	* @return the codice deposito of this evasioni ordini
	*/
	@Override
	public java.lang.String getCodiceDeposito() {
		return _evasioniOrdini.getCodiceDeposito();
	}

	/**
	* Sets the codice deposito of this evasioni ordini.
	*
	* @param codiceDeposito the codice deposito of this evasioni ordini
	*/
	@Override
	public void setCodiceDeposito(java.lang.String codiceDeposito) {
		_evasioniOrdini.setCodiceDeposito(codiceDeposito);
	}

	/**
	* Returns the tipo ordine of this evasioni ordini.
	*
	* @return the tipo ordine of this evasioni ordini
	*/
	@Override
	public int getTipoOrdine() {
		return _evasioniOrdini.getTipoOrdine();
	}

	/**
	* Sets the tipo ordine of this evasioni ordini.
	*
	* @param tipoOrdine the tipo ordine of this evasioni ordini
	*/
	@Override
	public void setTipoOrdine(int tipoOrdine) {
		_evasioniOrdini.setTipoOrdine(tipoOrdine);
	}

	/**
	* Returns the numero ordine of this evasioni ordini.
	*
	* @return the numero ordine of this evasioni ordini
	*/
	@Override
	public int getNumeroOrdine() {
		return _evasioniOrdini.getNumeroOrdine();
	}

	/**
	* Sets the numero ordine of this evasioni ordini.
	*
	* @param numeroOrdine the numero ordine of this evasioni ordini
	*/
	@Override
	public void setNumeroOrdine(int numeroOrdine) {
		_evasioniOrdini.setNumeroOrdine(numeroOrdine);
	}

	/**
	* Returns the codice cliente of this evasioni ordini.
	*
	* @return the codice cliente of this evasioni ordini
	*/
	@Override
	public java.lang.String getCodiceCliente() {
		return _evasioniOrdini.getCodiceCliente();
	}

	/**
	* Sets the codice cliente of this evasioni ordini.
	*
	* @param codiceCliente the codice cliente of this evasioni ordini
	*/
	@Override
	public void setCodiceCliente(java.lang.String codiceCliente) {
		_evasioniOrdini.setCodiceCliente(codiceCliente);
	}

	/**
	* Returns the numero rigo of this evasioni ordini.
	*
	* @return the numero rigo of this evasioni ordini
	*/
	@Override
	public int getNumeroRigo() {
		return _evasioniOrdini.getNumeroRigo();
	}

	/**
	* Sets the numero rigo of this evasioni ordini.
	*
	* @param numeroRigo the numero rigo of this evasioni ordini
	*/
	@Override
	public void setNumeroRigo(int numeroRigo) {
		_evasioniOrdini.setNumeroRigo(numeroRigo);
	}

	/**
	* Returns the codice articolo of this evasioni ordini.
	*
	* @return the codice articolo of this evasioni ordini
	*/
	@Override
	public java.lang.String getCodiceArticolo() {
		return _evasioniOrdini.getCodiceArticolo();
	}

	/**
	* Sets the codice articolo of this evasioni ordini.
	*
	* @param codiceArticolo the codice articolo of this evasioni ordini
	*/
	@Override
	public void setCodiceArticolo(java.lang.String codiceArticolo) {
		_evasioniOrdini.setCodiceArticolo(codiceArticolo);
	}

	/**
	* Returns the codice variante of this evasioni ordini.
	*
	* @return the codice variante of this evasioni ordini
	*/
	@Override
	public java.lang.String getCodiceVariante() {
		return _evasioniOrdini.getCodiceVariante();
	}

	/**
	* Sets the codice variante of this evasioni ordini.
	*
	* @param codiceVariante the codice variante of this evasioni ordini
	*/
	@Override
	public void setCodiceVariante(java.lang.String codiceVariante) {
		_evasioniOrdini.setCodiceVariante(codiceVariante);
	}

	/**
	* Returns the descrizione of this evasioni ordini.
	*
	* @return the descrizione of this evasioni ordini
	*/
	@Override
	public java.lang.String getDescrizione() {
		return _evasioniOrdini.getDescrizione();
	}

	/**
	* Sets the descrizione of this evasioni ordini.
	*
	* @param descrizione the descrizione of this evasioni ordini
	*/
	@Override
	public void setDescrizione(java.lang.String descrizione) {
		_evasioniOrdini.setDescrizione(descrizione);
	}

	/**
	* Returns the unita misura of this evasioni ordini.
	*
	* @return the unita misura of this evasioni ordini
	*/
	@Override
	public java.lang.String getUnitaMisura() {
		return _evasioniOrdini.getUnitaMisura();
	}

	/**
	* Sets the unita misura of this evasioni ordini.
	*
	* @param unitaMisura the unita misura of this evasioni ordini
	*/
	@Override
	public void setUnitaMisura(java.lang.String unitaMisura) {
		_evasioniOrdini.setUnitaMisura(unitaMisura);
	}

	/**
	* Returns the quantita ordinata of this evasioni ordini.
	*
	* @return the quantita ordinata of this evasioni ordini
	*/
	@Override
	public double getQuantitaOrdinata() {
		return _evasioniOrdini.getQuantitaOrdinata();
	}

	/**
	* Sets the quantita ordinata of this evasioni ordini.
	*
	* @param quantitaOrdinata the quantita ordinata of this evasioni ordini
	*/
	@Override
	public void setQuantitaOrdinata(double quantitaOrdinata) {
		_evasioniOrdini.setQuantitaOrdinata(quantitaOrdinata);
	}

	/**
	* Returns the numero progrezzivo evasione of this evasioni ordini.
	*
	* @return the numero progrezzivo evasione of this evasioni ordini
	*/
	@Override
	public int getNumeroProgrezzivoEvasione() {
		return _evasioniOrdini.getNumeroProgrezzivoEvasione();
	}

	/**
	* Sets the numero progrezzivo evasione of this evasioni ordini.
	*
	* @param numeroProgrezzivoEvasione the numero progrezzivo evasione of this evasioni ordini
	*/
	@Override
	public void setNumeroProgrezzivoEvasione(int numeroProgrezzivoEvasione) {
		_evasioniOrdini.setNumeroProgrezzivoEvasione(numeroProgrezzivoEvasione);
	}

	/**
	* Returns the quantita consegnata of this evasioni ordini.
	*
	* @return the quantita consegnata of this evasioni ordini
	*/
	@Override
	public double getQuantitaConsegnata() {
		return _evasioniOrdini.getQuantitaConsegnata();
	}

	/**
	* Sets the quantita consegnata of this evasioni ordini.
	*
	* @param quantitaConsegnata the quantita consegnata of this evasioni ordini
	*/
	@Override
	public void setQuantitaConsegnata(double quantitaConsegnata) {
		_evasioniOrdini.setQuantitaConsegnata(quantitaConsegnata);
	}

	/**
	* Returns the data documento consegna of this evasioni ordini.
	*
	* @return the data documento consegna of this evasioni ordini
	*/
	@Override
	public java.util.Date getDataDocumentoConsegna() {
		return _evasioniOrdini.getDataDocumentoConsegna();
	}

	/**
	* Sets the data documento consegna of this evasioni ordini.
	*
	* @param dataDocumentoConsegna the data documento consegna of this evasioni ordini
	*/
	@Override
	public void setDataDocumentoConsegna(java.util.Date dataDocumentoConsegna) {
		_evasioniOrdini.setDataDocumentoConsegna(dataDocumentoConsegna);
	}

	/**
	* Returns the test acconto saldo of this evasioni ordini.
	*
	* @return the test acconto saldo of this evasioni ordini
	*/
	@Override
	public java.lang.String getTestAccontoSaldo() {
		return _evasioniOrdini.getTestAccontoSaldo();
	}

	/**
	* Sets the test acconto saldo of this evasioni ordini.
	*
	* @param testAccontoSaldo the test acconto saldo of this evasioni ordini
	*/
	@Override
	public void setTestAccontoSaldo(java.lang.String testAccontoSaldo) {
		_evasioniOrdini.setTestAccontoSaldo(testAccontoSaldo);
	}

	/**
	* Returns the tipo documento evasione of this evasioni ordini.
	*
	* @return the tipo documento evasione of this evasioni ordini
	*/
	@Override
	public java.lang.String getTipoDocumentoEvasione() {
		return _evasioniOrdini.getTipoDocumentoEvasione();
	}

	/**
	* Sets the tipo documento evasione of this evasioni ordini.
	*
	* @param tipoDocumentoEvasione the tipo documento evasione of this evasioni ordini
	*/
	@Override
	public void setTipoDocumentoEvasione(java.lang.String tipoDocumentoEvasione) {
		_evasioniOrdini.setTipoDocumentoEvasione(tipoDocumentoEvasione);
	}

	/**
	* Returns the numero documento evasione of this evasioni ordini.
	*
	* @return the numero documento evasione of this evasioni ordini
	*/
	@Override
	public int getNumeroDocumentoEvasione() {
		return _evasioniOrdini.getNumeroDocumentoEvasione();
	}

	/**
	* Sets the numero documento evasione of this evasioni ordini.
	*
	* @param numeroDocumentoEvasione the numero documento evasione of this evasioni ordini
	*/
	@Override
	public void setNumeroDocumentoEvasione(int numeroDocumentoEvasione) {
		_evasioniOrdini.setNumeroDocumentoEvasione(numeroDocumentoEvasione);
	}

	@Override
	public boolean isNew() {
		return _evasioniOrdini.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_evasioniOrdini.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _evasioniOrdini.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_evasioniOrdini.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _evasioniOrdini.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _evasioniOrdini.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_evasioniOrdini.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _evasioniOrdini.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_evasioniOrdini.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_evasioniOrdini.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_evasioniOrdini.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new EvasioniOrdiniWrapper((EvasioniOrdini)_evasioniOrdini.clone());
	}

	@Override
	public int compareTo(it.bysoftware.ct.model.EvasioniOrdini evasioniOrdini) {
		return _evasioniOrdini.compareTo(evasioniOrdini);
	}

	@Override
	public int hashCode() {
		return _evasioniOrdini.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.EvasioniOrdini> toCacheModel() {
		return _evasioniOrdini.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.EvasioniOrdini toEscapedModel() {
		return new EvasioniOrdiniWrapper(_evasioniOrdini.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.EvasioniOrdini toUnescapedModel() {
		return new EvasioniOrdiniWrapper(_evasioniOrdini.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _evasioniOrdini.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _evasioniOrdini.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_evasioniOrdini.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EvasioniOrdiniWrapper)) {
			return false;
		}

		EvasioniOrdiniWrapper evasioniOrdiniWrapper = (EvasioniOrdiniWrapper)obj;

		if (Validator.equals(_evasioniOrdini,
					evasioniOrdiniWrapper._evasioniOrdini)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public EvasioniOrdini getWrappedEvasioniOrdini() {
		return _evasioniOrdini;
	}

	@Override
	public EvasioniOrdini getWrappedModel() {
		return _evasioniOrdini;
	}

	@Override
	public void resetOriginalValues() {
		_evasioniOrdini.resetOriginalValues();
	}

	private EvasioniOrdini _evasioniOrdini;
}