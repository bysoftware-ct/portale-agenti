/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link TestataFattureClienti}.
 * </p>
 *
 * @author Mario Torrisi
 * @see TestataFattureClienti
 * @generated
 */
public class TestataFattureClientiWrapper implements TestataFattureClienti,
	ModelWrapper<TestataFattureClienti> {
	public TestataFattureClientiWrapper(
		TestataFattureClienti testataFattureClienti) {
		_testataFattureClienti = testataFattureClienti;
	}

	@Override
	public Class<?> getModelClass() {
		return TestataFattureClienti.class;
	}

	@Override
	public String getModelClassName() {
		return TestataFattureClienti.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("anno", getAnno());
		attributes.put("codiceAttivita", getCodiceAttivita());
		attributes.put("codiceCentro", getCodiceCentro());
		attributes.put("numeroProtocollo", getNumeroProtocollo());
		attributes.put("tipoDocumento", getTipoDocumento());
		attributes.put("codiceCentroContAnalitica",
			getCodiceCentroContAnalitica());
		attributes.put("idTipoDocumentoOrigine", getIdTipoDocumentoOrigine());
		attributes.put("statoFattura", getStatoFattura());
		attributes.put("dataRegistrazione", getDataRegistrazione());
		attributes.put("dataOperazione", getDataOperazione());
		attributes.put("dataAnnotazione", getDataAnnotazione());
		attributes.put("dataDocumento", getDataDocumento());
		attributes.put("numeroDocumento", getNumeroDocumento());
		attributes.put("riferimentoAScontrino", getRiferimentoAScontrino());
		attributes.put("descrizioneEstremiDocumento",
			getDescrizioneEstremiDocumento());
		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceCliente", getCodiceCliente());
		attributes.put("descrizioneAggiuntivaFattura",
			getDescrizioneAggiuntivaFattura());
		attributes.put("estremiOrdine", getEstremiOrdine());
		attributes.put("estremiBolla", getEstremiBolla());
		attributes.put("codicePagamento", getCodicePagamento());
		attributes.put("codiceAgente", getCodiceAgente());
		attributes.put("codiceGruppoAgenti", getCodiceGruppoAgenti());
		attributes.put("annotazioni", getAnnotazioni());
		attributes.put("scontoChiusura", getScontoChiusura());
		attributes.put("scontoProntaCassa", getScontoProntaCassa());
		attributes.put("pertualeSpeseTrasp", getPertualeSpeseTrasp());
		attributes.put("importoSpeseTrasp", getImportoSpeseTrasp());
		attributes.put("importoSpeseImb", getImportoSpeseImb());
		attributes.put("importoSpeseVarie", getImportoSpeseVarie());
		attributes.put("importoSpeseBancarie", getImportoSpeseBancarie());
		attributes.put("codiceIVATrasp", getCodiceIVATrasp());
		attributes.put("codiceIVAImb", getCodiceIVAImb());
		attributes.put("codiceIVAVarie", getCodiceIVAVarie());
		attributes.put("codiceIVABancarie", getCodiceIVABancarie());
		attributes.put("totaleScontiCorpo", getTotaleScontiCorpo());
		attributes.put("imponibile", getImponibile());
		attributes.put("IVA", getIVA());
		attributes.put("importoFattura", getImportoFattura());
		attributes.put("libStr1", getLibStr1());
		attributes.put("libStr2", getLibStr2());
		attributes.put("libStr3", getLibStr3());
		attributes.put("libDbl1", getLibDbl1());
		attributes.put("libDbl2", getLibDbl2());
		attributes.put("libDbl3", getLibDbl3());
		attributes.put("libLng1", getLibLng1());
		attributes.put("libLng2", getLibLng2());
		attributes.put("libLng3", getLibLng3());
		attributes.put("libDat1", getLibDat1());
		attributes.put("libDat2", getLibDat2());
		attributes.put("libDat3", getLibDat3());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer anno = (Integer)attributes.get("anno");

		if (anno != null) {
			setAnno(anno);
		}

		String codiceAttivita = (String)attributes.get("codiceAttivita");

		if (codiceAttivita != null) {
			setCodiceAttivita(codiceAttivita);
		}

		String codiceCentro = (String)attributes.get("codiceCentro");

		if (codiceCentro != null) {
			setCodiceCentro(codiceCentro);
		}

		Integer numeroProtocollo = (Integer)attributes.get("numeroProtocollo");

		if (numeroProtocollo != null) {
			setNumeroProtocollo(numeroProtocollo);
		}

		String tipoDocumento = (String)attributes.get("tipoDocumento");

		if (tipoDocumento != null) {
			setTipoDocumento(tipoDocumento);
		}

		String codiceCentroContAnalitica = (String)attributes.get(
				"codiceCentroContAnalitica");

		if (codiceCentroContAnalitica != null) {
			setCodiceCentroContAnalitica(codiceCentroContAnalitica);
		}

		Integer idTipoDocumentoOrigine = (Integer)attributes.get(
				"idTipoDocumentoOrigine");

		if (idTipoDocumentoOrigine != null) {
			setIdTipoDocumentoOrigine(idTipoDocumentoOrigine);
		}

		Boolean statoFattura = (Boolean)attributes.get("statoFattura");

		if (statoFattura != null) {
			setStatoFattura(statoFattura);
		}

		Date dataRegistrazione = (Date)attributes.get("dataRegistrazione");

		if (dataRegistrazione != null) {
			setDataRegistrazione(dataRegistrazione);
		}

		Date dataOperazione = (Date)attributes.get("dataOperazione");

		if (dataOperazione != null) {
			setDataOperazione(dataOperazione);
		}

		Date dataAnnotazione = (Date)attributes.get("dataAnnotazione");

		if (dataAnnotazione != null) {
			setDataAnnotazione(dataAnnotazione);
		}

		Date dataDocumento = (Date)attributes.get("dataDocumento");

		if (dataDocumento != null) {
			setDataDocumento(dataDocumento);
		}

		Integer numeroDocumento = (Integer)attributes.get("numeroDocumento");

		if (numeroDocumento != null) {
			setNumeroDocumento(numeroDocumento);
		}

		Integer riferimentoAScontrino = (Integer)attributes.get(
				"riferimentoAScontrino");

		if (riferimentoAScontrino != null) {
			setRiferimentoAScontrino(riferimentoAScontrino);
		}

		String descrizioneEstremiDocumento = (String)attributes.get(
				"descrizioneEstremiDocumento");

		if (descrizioneEstremiDocumento != null) {
			setDescrizioneEstremiDocumento(descrizioneEstremiDocumento);
		}

		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceCliente = (String)attributes.get("codiceCliente");

		if (codiceCliente != null) {
			setCodiceCliente(codiceCliente);
		}

		String descrizioneAggiuntivaFattura = (String)attributes.get(
				"descrizioneAggiuntivaFattura");

		if (descrizioneAggiuntivaFattura != null) {
			setDescrizioneAggiuntivaFattura(descrizioneAggiuntivaFattura);
		}

		String estremiOrdine = (String)attributes.get("estremiOrdine");

		if (estremiOrdine != null) {
			setEstremiOrdine(estremiOrdine);
		}

		String estremiBolla = (String)attributes.get("estremiBolla");

		if (estremiBolla != null) {
			setEstremiBolla(estremiBolla);
		}

		String codicePagamento = (String)attributes.get("codicePagamento");

		if (codicePagamento != null) {
			setCodicePagamento(codicePagamento);
		}

		String codiceAgente = (String)attributes.get("codiceAgente");

		if (codiceAgente != null) {
			setCodiceAgente(codiceAgente);
		}

		String codiceGruppoAgenti = (String)attributes.get("codiceGruppoAgenti");

		if (codiceGruppoAgenti != null) {
			setCodiceGruppoAgenti(codiceGruppoAgenti);
		}

		String annotazioni = (String)attributes.get("annotazioni");

		if (annotazioni != null) {
			setAnnotazioni(annotazioni);
		}

		Double scontoChiusura = (Double)attributes.get("scontoChiusura");

		if (scontoChiusura != null) {
			setScontoChiusura(scontoChiusura);
		}

		Double scontoProntaCassa = (Double)attributes.get("scontoProntaCassa");

		if (scontoProntaCassa != null) {
			setScontoProntaCassa(scontoProntaCassa);
		}

		Double pertualeSpeseTrasp = (Double)attributes.get("pertualeSpeseTrasp");

		if (pertualeSpeseTrasp != null) {
			setPertualeSpeseTrasp(pertualeSpeseTrasp);
		}

		Double importoSpeseTrasp = (Double)attributes.get("importoSpeseTrasp");

		if (importoSpeseTrasp != null) {
			setImportoSpeseTrasp(importoSpeseTrasp);
		}

		Double importoSpeseImb = (Double)attributes.get("importoSpeseImb");

		if (importoSpeseImb != null) {
			setImportoSpeseImb(importoSpeseImb);
		}

		Double importoSpeseVarie = (Double)attributes.get("importoSpeseVarie");

		if (importoSpeseVarie != null) {
			setImportoSpeseVarie(importoSpeseVarie);
		}

		Double importoSpeseBancarie = (Double)attributes.get(
				"importoSpeseBancarie");

		if (importoSpeseBancarie != null) {
			setImportoSpeseBancarie(importoSpeseBancarie);
		}

		String codiceIVATrasp = (String)attributes.get("codiceIVATrasp");

		if (codiceIVATrasp != null) {
			setCodiceIVATrasp(codiceIVATrasp);
		}

		String codiceIVAImb = (String)attributes.get("codiceIVAImb");

		if (codiceIVAImb != null) {
			setCodiceIVAImb(codiceIVAImb);
		}

		String codiceIVAVarie = (String)attributes.get("codiceIVAVarie");

		if (codiceIVAVarie != null) {
			setCodiceIVAVarie(codiceIVAVarie);
		}

		String codiceIVABancarie = (String)attributes.get("codiceIVABancarie");

		if (codiceIVABancarie != null) {
			setCodiceIVABancarie(codiceIVABancarie);
		}

		Double totaleScontiCorpo = (Double)attributes.get("totaleScontiCorpo");

		if (totaleScontiCorpo != null) {
			setTotaleScontiCorpo(totaleScontiCorpo);
		}

		Double imponibile = (Double)attributes.get("imponibile");

		if (imponibile != null) {
			setImponibile(imponibile);
		}

		Double IVA = (Double)attributes.get("IVA");

		if (IVA != null) {
			setIVA(IVA);
		}

		Double importoFattura = (Double)attributes.get("importoFattura");

		if (importoFattura != null) {
			setImportoFattura(importoFattura);
		}

		String libStr1 = (String)attributes.get("libStr1");

		if (libStr1 != null) {
			setLibStr1(libStr1);
		}

		String libStr2 = (String)attributes.get("libStr2");

		if (libStr2 != null) {
			setLibStr2(libStr2);
		}

		String libStr3 = (String)attributes.get("libStr3");

		if (libStr3 != null) {
			setLibStr3(libStr3);
		}

		Double libDbl1 = (Double)attributes.get("libDbl1");

		if (libDbl1 != null) {
			setLibDbl1(libDbl1);
		}

		Double libDbl2 = (Double)attributes.get("libDbl2");

		if (libDbl2 != null) {
			setLibDbl2(libDbl2);
		}

		Double libDbl3 = (Double)attributes.get("libDbl3");

		if (libDbl3 != null) {
			setLibDbl3(libDbl3);
		}

		Long libLng1 = (Long)attributes.get("libLng1");

		if (libLng1 != null) {
			setLibLng1(libLng1);
		}

		Long libLng2 = (Long)attributes.get("libLng2");

		if (libLng2 != null) {
			setLibLng2(libLng2);
		}

		Long libLng3 = (Long)attributes.get("libLng3");

		if (libLng3 != null) {
			setLibLng3(libLng3);
		}

		Date libDat1 = (Date)attributes.get("libDat1");

		if (libDat1 != null) {
			setLibDat1(libDat1);
		}

		Date libDat2 = (Date)attributes.get("libDat2");

		if (libDat2 != null) {
			setLibDat2(libDat2);
		}

		Date libDat3 = (Date)attributes.get("libDat3");

		if (libDat3 != null) {
			setLibDat3(libDat3);
		}
	}

	/**
	* Returns the primary key of this testata fatture clienti.
	*
	* @return the primary key of this testata fatture clienti
	*/
	@Override
	public it.bysoftware.ct.service.persistence.TestataFattureClientiPK getPrimaryKey() {
		return _testataFattureClienti.getPrimaryKey();
	}

	/**
	* Sets the primary key of this testata fatture clienti.
	*
	* @param primaryKey the primary key of this testata fatture clienti
	*/
	@Override
	public void setPrimaryKey(
		it.bysoftware.ct.service.persistence.TestataFattureClientiPK primaryKey) {
		_testataFattureClienti.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the anno of this testata fatture clienti.
	*
	* @return the anno of this testata fatture clienti
	*/
	@Override
	public int getAnno() {
		return _testataFattureClienti.getAnno();
	}

	/**
	* Sets the anno of this testata fatture clienti.
	*
	* @param anno the anno of this testata fatture clienti
	*/
	@Override
	public void setAnno(int anno) {
		_testataFattureClienti.setAnno(anno);
	}

	/**
	* Returns the codice attivita of this testata fatture clienti.
	*
	* @return the codice attivita of this testata fatture clienti
	*/
	@Override
	public java.lang.String getCodiceAttivita() {
		return _testataFattureClienti.getCodiceAttivita();
	}

	/**
	* Sets the codice attivita of this testata fatture clienti.
	*
	* @param codiceAttivita the codice attivita of this testata fatture clienti
	*/
	@Override
	public void setCodiceAttivita(java.lang.String codiceAttivita) {
		_testataFattureClienti.setCodiceAttivita(codiceAttivita);
	}

	/**
	* Returns the codice centro of this testata fatture clienti.
	*
	* @return the codice centro of this testata fatture clienti
	*/
	@Override
	public java.lang.String getCodiceCentro() {
		return _testataFattureClienti.getCodiceCentro();
	}

	/**
	* Sets the codice centro of this testata fatture clienti.
	*
	* @param codiceCentro the codice centro of this testata fatture clienti
	*/
	@Override
	public void setCodiceCentro(java.lang.String codiceCentro) {
		_testataFattureClienti.setCodiceCentro(codiceCentro);
	}

	/**
	* Returns the numero protocollo of this testata fatture clienti.
	*
	* @return the numero protocollo of this testata fatture clienti
	*/
	@Override
	public int getNumeroProtocollo() {
		return _testataFattureClienti.getNumeroProtocollo();
	}

	/**
	* Sets the numero protocollo of this testata fatture clienti.
	*
	* @param numeroProtocollo the numero protocollo of this testata fatture clienti
	*/
	@Override
	public void setNumeroProtocollo(int numeroProtocollo) {
		_testataFattureClienti.setNumeroProtocollo(numeroProtocollo);
	}

	/**
	* Returns the tipo documento of this testata fatture clienti.
	*
	* @return the tipo documento of this testata fatture clienti
	*/
	@Override
	public java.lang.String getTipoDocumento() {
		return _testataFattureClienti.getTipoDocumento();
	}

	/**
	* Sets the tipo documento of this testata fatture clienti.
	*
	* @param tipoDocumento the tipo documento of this testata fatture clienti
	*/
	@Override
	public void setTipoDocumento(java.lang.String tipoDocumento) {
		_testataFattureClienti.setTipoDocumento(tipoDocumento);
	}

	/**
	* Returns the codice centro cont analitica of this testata fatture clienti.
	*
	* @return the codice centro cont analitica of this testata fatture clienti
	*/
	@Override
	public java.lang.String getCodiceCentroContAnalitica() {
		return _testataFattureClienti.getCodiceCentroContAnalitica();
	}

	/**
	* Sets the codice centro cont analitica of this testata fatture clienti.
	*
	* @param codiceCentroContAnalitica the codice centro cont analitica of this testata fatture clienti
	*/
	@Override
	public void setCodiceCentroContAnalitica(
		java.lang.String codiceCentroContAnalitica) {
		_testataFattureClienti.setCodiceCentroContAnalitica(codiceCentroContAnalitica);
	}

	/**
	* Returns the id tipo documento origine of this testata fatture clienti.
	*
	* @return the id tipo documento origine of this testata fatture clienti
	*/
	@Override
	public int getIdTipoDocumentoOrigine() {
		return _testataFattureClienti.getIdTipoDocumentoOrigine();
	}

	/**
	* Sets the id tipo documento origine of this testata fatture clienti.
	*
	* @param idTipoDocumentoOrigine the id tipo documento origine of this testata fatture clienti
	*/
	@Override
	public void setIdTipoDocumentoOrigine(int idTipoDocumentoOrigine) {
		_testataFattureClienti.setIdTipoDocumentoOrigine(idTipoDocumentoOrigine);
	}

	/**
	* Returns the stato fattura of this testata fatture clienti.
	*
	* @return the stato fattura of this testata fatture clienti
	*/
	@Override
	public boolean getStatoFattura() {
		return _testataFattureClienti.getStatoFattura();
	}

	/**
	* Returns <code>true</code> if this testata fatture clienti is stato fattura.
	*
	* @return <code>true</code> if this testata fatture clienti is stato fattura; <code>false</code> otherwise
	*/
	@Override
	public boolean isStatoFattura() {
		return _testataFattureClienti.isStatoFattura();
	}

	/**
	* Sets whether this testata fatture clienti is stato fattura.
	*
	* @param statoFattura the stato fattura of this testata fatture clienti
	*/
	@Override
	public void setStatoFattura(boolean statoFattura) {
		_testataFattureClienti.setStatoFattura(statoFattura);
	}

	/**
	* Returns the data registrazione of this testata fatture clienti.
	*
	* @return the data registrazione of this testata fatture clienti
	*/
	@Override
	public java.util.Date getDataRegistrazione() {
		return _testataFattureClienti.getDataRegistrazione();
	}

	/**
	* Sets the data registrazione of this testata fatture clienti.
	*
	* @param dataRegistrazione the data registrazione of this testata fatture clienti
	*/
	@Override
	public void setDataRegistrazione(java.util.Date dataRegistrazione) {
		_testataFattureClienti.setDataRegistrazione(dataRegistrazione);
	}

	/**
	* Returns the data operazione of this testata fatture clienti.
	*
	* @return the data operazione of this testata fatture clienti
	*/
	@Override
	public java.util.Date getDataOperazione() {
		return _testataFattureClienti.getDataOperazione();
	}

	/**
	* Sets the data operazione of this testata fatture clienti.
	*
	* @param dataOperazione the data operazione of this testata fatture clienti
	*/
	@Override
	public void setDataOperazione(java.util.Date dataOperazione) {
		_testataFattureClienti.setDataOperazione(dataOperazione);
	}

	/**
	* Returns the data annotazione of this testata fatture clienti.
	*
	* @return the data annotazione of this testata fatture clienti
	*/
	@Override
	public java.util.Date getDataAnnotazione() {
		return _testataFattureClienti.getDataAnnotazione();
	}

	/**
	* Sets the data annotazione of this testata fatture clienti.
	*
	* @param dataAnnotazione the data annotazione of this testata fatture clienti
	*/
	@Override
	public void setDataAnnotazione(java.util.Date dataAnnotazione) {
		_testataFattureClienti.setDataAnnotazione(dataAnnotazione);
	}

	/**
	* Returns the data documento of this testata fatture clienti.
	*
	* @return the data documento of this testata fatture clienti
	*/
	@Override
	public java.util.Date getDataDocumento() {
		return _testataFattureClienti.getDataDocumento();
	}

	/**
	* Sets the data documento of this testata fatture clienti.
	*
	* @param dataDocumento the data documento of this testata fatture clienti
	*/
	@Override
	public void setDataDocumento(java.util.Date dataDocumento) {
		_testataFattureClienti.setDataDocumento(dataDocumento);
	}

	/**
	* Returns the numero documento of this testata fatture clienti.
	*
	* @return the numero documento of this testata fatture clienti
	*/
	@Override
	public int getNumeroDocumento() {
		return _testataFattureClienti.getNumeroDocumento();
	}

	/**
	* Sets the numero documento of this testata fatture clienti.
	*
	* @param numeroDocumento the numero documento of this testata fatture clienti
	*/
	@Override
	public void setNumeroDocumento(int numeroDocumento) {
		_testataFattureClienti.setNumeroDocumento(numeroDocumento);
	}

	/**
	* Returns the riferimento a scontrino of this testata fatture clienti.
	*
	* @return the riferimento a scontrino of this testata fatture clienti
	*/
	@Override
	public int getRiferimentoAScontrino() {
		return _testataFattureClienti.getRiferimentoAScontrino();
	}

	/**
	* Sets the riferimento a scontrino of this testata fatture clienti.
	*
	* @param riferimentoAScontrino the riferimento a scontrino of this testata fatture clienti
	*/
	@Override
	public void setRiferimentoAScontrino(int riferimentoAScontrino) {
		_testataFattureClienti.setRiferimentoAScontrino(riferimentoAScontrino);
	}

	/**
	* Returns the descrizione estremi documento of this testata fatture clienti.
	*
	* @return the descrizione estremi documento of this testata fatture clienti
	*/
	@Override
	public java.lang.String getDescrizioneEstremiDocumento() {
		return _testataFattureClienti.getDescrizioneEstremiDocumento();
	}

	/**
	* Sets the descrizione estremi documento of this testata fatture clienti.
	*
	* @param descrizioneEstremiDocumento the descrizione estremi documento of this testata fatture clienti
	*/
	@Override
	public void setDescrizioneEstremiDocumento(
		java.lang.String descrizioneEstremiDocumento) {
		_testataFattureClienti.setDescrizioneEstremiDocumento(descrizioneEstremiDocumento);
	}

	/**
	* Returns the tipo soggetto of this testata fatture clienti.
	*
	* @return the tipo soggetto of this testata fatture clienti
	*/
	@Override
	public boolean getTipoSoggetto() {
		return _testataFattureClienti.getTipoSoggetto();
	}

	/**
	* Returns <code>true</code> if this testata fatture clienti is tipo soggetto.
	*
	* @return <code>true</code> if this testata fatture clienti is tipo soggetto; <code>false</code> otherwise
	*/
	@Override
	public boolean isTipoSoggetto() {
		return _testataFattureClienti.isTipoSoggetto();
	}

	/**
	* Sets whether this testata fatture clienti is tipo soggetto.
	*
	* @param tipoSoggetto the tipo soggetto of this testata fatture clienti
	*/
	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_testataFattureClienti.setTipoSoggetto(tipoSoggetto);
	}

	/**
	* Returns the codice cliente of this testata fatture clienti.
	*
	* @return the codice cliente of this testata fatture clienti
	*/
	@Override
	public java.lang.String getCodiceCliente() {
		return _testataFattureClienti.getCodiceCliente();
	}

	/**
	* Sets the codice cliente of this testata fatture clienti.
	*
	* @param codiceCliente the codice cliente of this testata fatture clienti
	*/
	@Override
	public void setCodiceCliente(java.lang.String codiceCliente) {
		_testataFattureClienti.setCodiceCliente(codiceCliente);
	}

	/**
	* Returns the descrizione aggiuntiva fattura of this testata fatture clienti.
	*
	* @return the descrizione aggiuntiva fattura of this testata fatture clienti
	*/
	@Override
	public java.lang.String getDescrizioneAggiuntivaFattura() {
		return _testataFattureClienti.getDescrizioneAggiuntivaFattura();
	}

	/**
	* Sets the descrizione aggiuntiva fattura of this testata fatture clienti.
	*
	* @param descrizioneAggiuntivaFattura the descrizione aggiuntiva fattura of this testata fatture clienti
	*/
	@Override
	public void setDescrizioneAggiuntivaFattura(
		java.lang.String descrizioneAggiuntivaFattura) {
		_testataFattureClienti.setDescrizioneAggiuntivaFattura(descrizioneAggiuntivaFattura);
	}

	/**
	* Returns the estremi ordine of this testata fatture clienti.
	*
	* @return the estremi ordine of this testata fatture clienti
	*/
	@Override
	public java.lang.String getEstremiOrdine() {
		return _testataFattureClienti.getEstremiOrdine();
	}

	/**
	* Sets the estremi ordine of this testata fatture clienti.
	*
	* @param estremiOrdine the estremi ordine of this testata fatture clienti
	*/
	@Override
	public void setEstremiOrdine(java.lang.String estremiOrdine) {
		_testataFattureClienti.setEstremiOrdine(estremiOrdine);
	}

	/**
	* Returns the estremi bolla of this testata fatture clienti.
	*
	* @return the estremi bolla of this testata fatture clienti
	*/
	@Override
	public java.lang.String getEstremiBolla() {
		return _testataFattureClienti.getEstremiBolla();
	}

	/**
	* Sets the estremi bolla of this testata fatture clienti.
	*
	* @param estremiBolla the estremi bolla of this testata fatture clienti
	*/
	@Override
	public void setEstremiBolla(java.lang.String estremiBolla) {
		_testataFattureClienti.setEstremiBolla(estremiBolla);
	}

	/**
	* Returns the codice pagamento of this testata fatture clienti.
	*
	* @return the codice pagamento of this testata fatture clienti
	*/
	@Override
	public java.lang.String getCodicePagamento() {
		return _testataFattureClienti.getCodicePagamento();
	}

	/**
	* Sets the codice pagamento of this testata fatture clienti.
	*
	* @param codicePagamento the codice pagamento of this testata fatture clienti
	*/
	@Override
	public void setCodicePagamento(java.lang.String codicePagamento) {
		_testataFattureClienti.setCodicePagamento(codicePagamento);
	}

	/**
	* Returns the codice agente of this testata fatture clienti.
	*
	* @return the codice agente of this testata fatture clienti
	*/
	@Override
	public java.lang.String getCodiceAgente() {
		return _testataFattureClienti.getCodiceAgente();
	}

	/**
	* Sets the codice agente of this testata fatture clienti.
	*
	* @param codiceAgente the codice agente of this testata fatture clienti
	*/
	@Override
	public void setCodiceAgente(java.lang.String codiceAgente) {
		_testataFattureClienti.setCodiceAgente(codiceAgente);
	}

	/**
	* Returns the codice gruppo agenti of this testata fatture clienti.
	*
	* @return the codice gruppo agenti of this testata fatture clienti
	*/
	@Override
	public java.lang.String getCodiceGruppoAgenti() {
		return _testataFattureClienti.getCodiceGruppoAgenti();
	}

	/**
	* Sets the codice gruppo agenti of this testata fatture clienti.
	*
	* @param codiceGruppoAgenti the codice gruppo agenti of this testata fatture clienti
	*/
	@Override
	public void setCodiceGruppoAgenti(java.lang.String codiceGruppoAgenti) {
		_testataFattureClienti.setCodiceGruppoAgenti(codiceGruppoAgenti);
	}

	/**
	* Returns the annotazioni of this testata fatture clienti.
	*
	* @return the annotazioni of this testata fatture clienti
	*/
	@Override
	public java.lang.String getAnnotazioni() {
		return _testataFattureClienti.getAnnotazioni();
	}

	/**
	* Sets the annotazioni of this testata fatture clienti.
	*
	* @param annotazioni the annotazioni of this testata fatture clienti
	*/
	@Override
	public void setAnnotazioni(java.lang.String annotazioni) {
		_testataFattureClienti.setAnnotazioni(annotazioni);
	}

	/**
	* Returns the sconto chiusura of this testata fatture clienti.
	*
	* @return the sconto chiusura of this testata fatture clienti
	*/
	@Override
	public double getScontoChiusura() {
		return _testataFattureClienti.getScontoChiusura();
	}

	/**
	* Sets the sconto chiusura of this testata fatture clienti.
	*
	* @param scontoChiusura the sconto chiusura of this testata fatture clienti
	*/
	@Override
	public void setScontoChiusura(double scontoChiusura) {
		_testataFattureClienti.setScontoChiusura(scontoChiusura);
	}

	/**
	* Returns the sconto pronta cassa of this testata fatture clienti.
	*
	* @return the sconto pronta cassa of this testata fatture clienti
	*/
	@Override
	public double getScontoProntaCassa() {
		return _testataFattureClienti.getScontoProntaCassa();
	}

	/**
	* Sets the sconto pronta cassa of this testata fatture clienti.
	*
	* @param scontoProntaCassa the sconto pronta cassa of this testata fatture clienti
	*/
	@Override
	public void setScontoProntaCassa(double scontoProntaCassa) {
		_testataFattureClienti.setScontoProntaCassa(scontoProntaCassa);
	}

	/**
	* Returns the pertuale spese trasp of this testata fatture clienti.
	*
	* @return the pertuale spese trasp of this testata fatture clienti
	*/
	@Override
	public double getPertualeSpeseTrasp() {
		return _testataFattureClienti.getPertualeSpeseTrasp();
	}

	/**
	* Sets the pertuale spese trasp of this testata fatture clienti.
	*
	* @param pertualeSpeseTrasp the pertuale spese trasp of this testata fatture clienti
	*/
	@Override
	public void setPertualeSpeseTrasp(double pertualeSpeseTrasp) {
		_testataFattureClienti.setPertualeSpeseTrasp(pertualeSpeseTrasp);
	}

	/**
	* Returns the importo spese trasp of this testata fatture clienti.
	*
	* @return the importo spese trasp of this testata fatture clienti
	*/
	@Override
	public double getImportoSpeseTrasp() {
		return _testataFattureClienti.getImportoSpeseTrasp();
	}

	/**
	* Sets the importo spese trasp of this testata fatture clienti.
	*
	* @param importoSpeseTrasp the importo spese trasp of this testata fatture clienti
	*/
	@Override
	public void setImportoSpeseTrasp(double importoSpeseTrasp) {
		_testataFattureClienti.setImportoSpeseTrasp(importoSpeseTrasp);
	}

	/**
	* Returns the importo spese imb of this testata fatture clienti.
	*
	* @return the importo spese imb of this testata fatture clienti
	*/
	@Override
	public double getImportoSpeseImb() {
		return _testataFattureClienti.getImportoSpeseImb();
	}

	/**
	* Sets the importo spese imb of this testata fatture clienti.
	*
	* @param importoSpeseImb the importo spese imb of this testata fatture clienti
	*/
	@Override
	public void setImportoSpeseImb(double importoSpeseImb) {
		_testataFattureClienti.setImportoSpeseImb(importoSpeseImb);
	}

	/**
	* Returns the importo spese varie of this testata fatture clienti.
	*
	* @return the importo spese varie of this testata fatture clienti
	*/
	@Override
	public double getImportoSpeseVarie() {
		return _testataFattureClienti.getImportoSpeseVarie();
	}

	/**
	* Sets the importo spese varie of this testata fatture clienti.
	*
	* @param importoSpeseVarie the importo spese varie of this testata fatture clienti
	*/
	@Override
	public void setImportoSpeseVarie(double importoSpeseVarie) {
		_testataFattureClienti.setImportoSpeseVarie(importoSpeseVarie);
	}

	/**
	* Returns the importo spese bancarie of this testata fatture clienti.
	*
	* @return the importo spese bancarie of this testata fatture clienti
	*/
	@Override
	public double getImportoSpeseBancarie() {
		return _testataFattureClienti.getImportoSpeseBancarie();
	}

	/**
	* Sets the importo spese bancarie of this testata fatture clienti.
	*
	* @param importoSpeseBancarie the importo spese bancarie of this testata fatture clienti
	*/
	@Override
	public void setImportoSpeseBancarie(double importoSpeseBancarie) {
		_testataFattureClienti.setImportoSpeseBancarie(importoSpeseBancarie);
	}

	/**
	* Returns the codice i v a trasp of this testata fatture clienti.
	*
	* @return the codice i v a trasp of this testata fatture clienti
	*/
	@Override
	public java.lang.String getCodiceIVATrasp() {
		return _testataFattureClienti.getCodiceIVATrasp();
	}

	/**
	* Sets the codice i v a trasp of this testata fatture clienti.
	*
	* @param codiceIVATrasp the codice i v a trasp of this testata fatture clienti
	*/
	@Override
	public void setCodiceIVATrasp(java.lang.String codiceIVATrasp) {
		_testataFattureClienti.setCodiceIVATrasp(codiceIVATrasp);
	}

	/**
	* Returns the codice i v a imb of this testata fatture clienti.
	*
	* @return the codice i v a imb of this testata fatture clienti
	*/
	@Override
	public java.lang.String getCodiceIVAImb() {
		return _testataFattureClienti.getCodiceIVAImb();
	}

	/**
	* Sets the codice i v a imb of this testata fatture clienti.
	*
	* @param codiceIVAImb the codice i v a imb of this testata fatture clienti
	*/
	@Override
	public void setCodiceIVAImb(java.lang.String codiceIVAImb) {
		_testataFattureClienti.setCodiceIVAImb(codiceIVAImb);
	}

	/**
	* Returns the codice i v a varie of this testata fatture clienti.
	*
	* @return the codice i v a varie of this testata fatture clienti
	*/
	@Override
	public java.lang.String getCodiceIVAVarie() {
		return _testataFattureClienti.getCodiceIVAVarie();
	}

	/**
	* Sets the codice i v a varie of this testata fatture clienti.
	*
	* @param codiceIVAVarie the codice i v a varie of this testata fatture clienti
	*/
	@Override
	public void setCodiceIVAVarie(java.lang.String codiceIVAVarie) {
		_testataFattureClienti.setCodiceIVAVarie(codiceIVAVarie);
	}

	/**
	* Returns the codice i v a bancarie of this testata fatture clienti.
	*
	* @return the codice i v a bancarie of this testata fatture clienti
	*/
	@Override
	public java.lang.String getCodiceIVABancarie() {
		return _testataFattureClienti.getCodiceIVABancarie();
	}

	/**
	* Sets the codice i v a bancarie of this testata fatture clienti.
	*
	* @param codiceIVABancarie the codice i v a bancarie of this testata fatture clienti
	*/
	@Override
	public void setCodiceIVABancarie(java.lang.String codiceIVABancarie) {
		_testataFattureClienti.setCodiceIVABancarie(codiceIVABancarie);
	}

	/**
	* Returns the totale sconti corpo of this testata fatture clienti.
	*
	* @return the totale sconti corpo of this testata fatture clienti
	*/
	@Override
	public double getTotaleScontiCorpo() {
		return _testataFattureClienti.getTotaleScontiCorpo();
	}

	/**
	* Sets the totale sconti corpo of this testata fatture clienti.
	*
	* @param totaleScontiCorpo the totale sconti corpo of this testata fatture clienti
	*/
	@Override
	public void setTotaleScontiCorpo(double totaleScontiCorpo) {
		_testataFattureClienti.setTotaleScontiCorpo(totaleScontiCorpo);
	}

	/**
	* Returns the imponibile of this testata fatture clienti.
	*
	* @return the imponibile of this testata fatture clienti
	*/
	@Override
	public double getImponibile() {
		return _testataFattureClienti.getImponibile();
	}

	/**
	* Sets the imponibile of this testata fatture clienti.
	*
	* @param imponibile the imponibile of this testata fatture clienti
	*/
	@Override
	public void setImponibile(double imponibile) {
		_testataFattureClienti.setImponibile(imponibile);
	}

	/**
	* Returns the i v a of this testata fatture clienti.
	*
	* @return the i v a of this testata fatture clienti
	*/
	@Override
	public double getIVA() {
		return _testataFattureClienti.getIVA();
	}

	/**
	* Sets the i v a of this testata fatture clienti.
	*
	* @param IVA the i v a of this testata fatture clienti
	*/
	@Override
	public void setIVA(double IVA) {
		_testataFattureClienti.setIVA(IVA);
	}

	/**
	* Returns the importo fattura of this testata fatture clienti.
	*
	* @return the importo fattura of this testata fatture clienti
	*/
	@Override
	public double getImportoFattura() {
		return _testataFattureClienti.getImportoFattura();
	}

	/**
	* Sets the importo fattura of this testata fatture clienti.
	*
	* @param importoFattura the importo fattura of this testata fatture clienti
	*/
	@Override
	public void setImportoFattura(double importoFattura) {
		_testataFattureClienti.setImportoFattura(importoFattura);
	}

	/**
	* Returns the lib str1 of this testata fatture clienti.
	*
	* @return the lib str1 of this testata fatture clienti
	*/
	@Override
	public java.lang.String getLibStr1() {
		return _testataFattureClienti.getLibStr1();
	}

	/**
	* Sets the lib str1 of this testata fatture clienti.
	*
	* @param libStr1 the lib str1 of this testata fatture clienti
	*/
	@Override
	public void setLibStr1(java.lang.String libStr1) {
		_testataFattureClienti.setLibStr1(libStr1);
	}

	/**
	* Returns the lib str2 of this testata fatture clienti.
	*
	* @return the lib str2 of this testata fatture clienti
	*/
	@Override
	public java.lang.String getLibStr2() {
		return _testataFattureClienti.getLibStr2();
	}

	/**
	* Sets the lib str2 of this testata fatture clienti.
	*
	* @param libStr2 the lib str2 of this testata fatture clienti
	*/
	@Override
	public void setLibStr2(java.lang.String libStr2) {
		_testataFattureClienti.setLibStr2(libStr2);
	}

	/**
	* Returns the lib str3 of this testata fatture clienti.
	*
	* @return the lib str3 of this testata fatture clienti
	*/
	@Override
	public java.lang.String getLibStr3() {
		return _testataFattureClienti.getLibStr3();
	}

	/**
	* Sets the lib str3 of this testata fatture clienti.
	*
	* @param libStr3 the lib str3 of this testata fatture clienti
	*/
	@Override
	public void setLibStr3(java.lang.String libStr3) {
		_testataFattureClienti.setLibStr3(libStr3);
	}

	/**
	* Returns the lib dbl1 of this testata fatture clienti.
	*
	* @return the lib dbl1 of this testata fatture clienti
	*/
	@Override
	public double getLibDbl1() {
		return _testataFattureClienti.getLibDbl1();
	}

	/**
	* Sets the lib dbl1 of this testata fatture clienti.
	*
	* @param libDbl1 the lib dbl1 of this testata fatture clienti
	*/
	@Override
	public void setLibDbl1(double libDbl1) {
		_testataFattureClienti.setLibDbl1(libDbl1);
	}

	/**
	* Returns the lib dbl2 of this testata fatture clienti.
	*
	* @return the lib dbl2 of this testata fatture clienti
	*/
	@Override
	public double getLibDbl2() {
		return _testataFattureClienti.getLibDbl2();
	}

	/**
	* Sets the lib dbl2 of this testata fatture clienti.
	*
	* @param libDbl2 the lib dbl2 of this testata fatture clienti
	*/
	@Override
	public void setLibDbl2(double libDbl2) {
		_testataFattureClienti.setLibDbl2(libDbl2);
	}

	/**
	* Returns the lib dbl3 of this testata fatture clienti.
	*
	* @return the lib dbl3 of this testata fatture clienti
	*/
	@Override
	public double getLibDbl3() {
		return _testataFattureClienti.getLibDbl3();
	}

	/**
	* Sets the lib dbl3 of this testata fatture clienti.
	*
	* @param libDbl3 the lib dbl3 of this testata fatture clienti
	*/
	@Override
	public void setLibDbl3(double libDbl3) {
		_testataFattureClienti.setLibDbl3(libDbl3);
	}

	/**
	* Returns the lib lng1 of this testata fatture clienti.
	*
	* @return the lib lng1 of this testata fatture clienti
	*/
	@Override
	public long getLibLng1() {
		return _testataFattureClienti.getLibLng1();
	}

	/**
	* Sets the lib lng1 of this testata fatture clienti.
	*
	* @param libLng1 the lib lng1 of this testata fatture clienti
	*/
	@Override
	public void setLibLng1(long libLng1) {
		_testataFattureClienti.setLibLng1(libLng1);
	}

	/**
	* Returns the lib lng2 of this testata fatture clienti.
	*
	* @return the lib lng2 of this testata fatture clienti
	*/
	@Override
	public long getLibLng2() {
		return _testataFattureClienti.getLibLng2();
	}

	/**
	* Sets the lib lng2 of this testata fatture clienti.
	*
	* @param libLng2 the lib lng2 of this testata fatture clienti
	*/
	@Override
	public void setLibLng2(long libLng2) {
		_testataFattureClienti.setLibLng2(libLng2);
	}

	/**
	* Returns the lib lng3 of this testata fatture clienti.
	*
	* @return the lib lng3 of this testata fatture clienti
	*/
	@Override
	public long getLibLng3() {
		return _testataFattureClienti.getLibLng3();
	}

	/**
	* Sets the lib lng3 of this testata fatture clienti.
	*
	* @param libLng3 the lib lng3 of this testata fatture clienti
	*/
	@Override
	public void setLibLng3(long libLng3) {
		_testataFattureClienti.setLibLng3(libLng3);
	}

	/**
	* Returns the lib dat1 of this testata fatture clienti.
	*
	* @return the lib dat1 of this testata fatture clienti
	*/
	@Override
	public java.util.Date getLibDat1() {
		return _testataFattureClienti.getLibDat1();
	}

	/**
	* Sets the lib dat1 of this testata fatture clienti.
	*
	* @param libDat1 the lib dat1 of this testata fatture clienti
	*/
	@Override
	public void setLibDat1(java.util.Date libDat1) {
		_testataFattureClienti.setLibDat1(libDat1);
	}

	/**
	* Returns the lib dat2 of this testata fatture clienti.
	*
	* @return the lib dat2 of this testata fatture clienti
	*/
	@Override
	public java.util.Date getLibDat2() {
		return _testataFattureClienti.getLibDat2();
	}

	/**
	* Sets the lib dat2 of this testata fatture clienti.
	*
	* @param libDat2 the lib dat2 of this testata fatture clienti
	*/
	@Override
	public void setLibDat2(java.util.Date libDat2) {
		_testataFattureClienti.setLibDat2(libDat2);
	}

	/**
	* Returns the lib dat3 of this testata fatture clienti.
	*
	* @return the lib dat3 of this testata fatture clienti
	*/
	@Override
	public java.util.Date getLibDat3() {
		return _testataFattureClienti.getLibDat3();
	}

	/**
	* Sets the lib dat3 of this testata fatture clienti.
	*
	* @param libDat3 the lib dat3 of this testata fatture clienti
	*/
	@Override
	public void setLibDat3(java.util.Date libDat3) {
		_testataFattureClienti.setLibDat3(libDat3);
	}

	@Override
	public boolean isNew() {
		return _testataFattureClienti.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_testataFattureClienti.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _testataFattureClienti.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_testataFattureClienti.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _testataFattureClienti.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _testataFattureClienti.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_testataFattureClienti.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _testataFattureClienti.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_testataFattureClienti.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_testataFattureClienti.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_testataFattureClienti.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new TestataFattureClientiWrapper((TestataFattureClienti)_testataFattureClienti.clone());
	}

	@Override
	public int compareTo(
		it.bysoftware.ct.model.TestataFattureClienti testataFattureClienti) {
		return _testataFattureClienti.compareTo(testataFattureClienti);
	}

	@Override
	public int hashCode() {
		return _testataFattureClienti.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.TestataFattureClienti> toCacheModel() {
		return _testataFattureClienti.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.TestataFattureClienti toEscapedModel() {
		return new TestataFattureClientiWrapper(_testataFattureClienti.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.TestataFattureClienti toUnescapedModel() {
		return new TestataFattureClientiWrapper(_testataFattureClienti.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _testataFattureClienti.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _testataFattureClienti.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_testataFattureClienti.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TestataFattureClientiWrapper)) {
			return false;
		}

		TestataFattureClientiWrapper testataFattureClientiWrapper = (TestataFattureClientiWrapper)obj;

		if (Validator.equals(_testataFattureClienti,
					testataFattureClientiWrapper._testataFattureClienti)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public TestataFattureClienti getWrappedTestataFattureClienti() {
		return _testataFattureClienti;
	}

	@Override
	public TestataFattureClienti getWrappedModel() {
		return _testataFattureClienti;
	}

	@Override
	public void resetOriginalValues() {
		_testataFattureClienti.resetOriginalValues();
	}

	private TestataFattureClienti _testataFattureClienti;
}