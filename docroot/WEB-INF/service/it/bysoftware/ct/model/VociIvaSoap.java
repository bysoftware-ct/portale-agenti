/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.VociIvaServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.VociIvaServiceSoap
 * @generated
 */
public class VociIvaSoap implements Serializable {
	public static VociIvaSoap toSoapModel(VociIva model) {
		VociIvaSoap soapModel = new VociIvaSoap();

		soapModel.setCodiceIva(model.getCodiceIva());
		soapModel.setDescrizione(model.getDescrizione());
		soapModel.setAliquota(model.getAliquota());
		soapModel.setAliquotaVandita(model.getAliquotaVandita());
		soapModel.setAliquotaScorporo(model.getAliquotaScorporo());
		soapModel.setAliquotaUlterioreDetr(model.getAliquotaUlterioreDetr());
		soapModel.setTipoVoceIva(model.getTipoVoceIva());
		soapModel.setGestioneOmaggio(model.getGestioneOmaggio());
		soapModel.setCodIvaSpeseOmaggio(model.getCodIvaSpeseOmaggio());
		soapModel.setDescrizioneDocuemto(model.getDescrizioneDocuemto());

		return soapModel;
	}

	public static VociIvaSoap[] toSoapModels(VociIva[] models) {
		VociIvaSoap[] soapModels = new VociIvaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static VociIvaSoap[][] toSoapModels(VociIva[][] models) {
		VociIvaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new VociIvaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new VociIvaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static VociIvaSoap[] toSoapModels(List<VociIva> models) {
		List<VociIvaSoap> soapModels = new ArrayList<VociIvaSoap>(models.size());

		for (VociIva model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new VociIvaSoap[soapModels.size()]);
	}

	public VociIvaSoap() {
	}

	public String getPrimaryKey() {
		return _codiceIva;
	}

	public void setPrimaryKey(String pk) {
		setCodiceIva(pk);
	}

	public String getCodiceIva() {
		return _codiceIva;
	}

	public void setCodiceIva(String codiceIva) {
		_codiceIva = codiceIva;
	}

	public String getDescrizione() {
		return _descrizione;
	}

	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;
	}

	public double getAliquota() {
		return _aliquota;
	}

	public void setAliquota(double aliquota) {
		_aliquota = aliquota;
	}

	public double getAliquotaVandita() {
		return _aliquotaVandita;
	}

	public void setAliquotaVandita(double aliquotaVandita) {
		_aliquotaVandita = aliquotaVandita;
	}

	public double getAliquotaScorporo() {
		return _aliquotaScorporo;
	}

	public void setAliquotaScorporo(double aliquotaScorporo) {
		_aliquotaScorporo = aliquotaScorporo;
	}

	public double getAliquotaUlterioreDetr() {
		return _aliquotaUlterioreDetr;
	}

	public void setAliquotaUlterioreDetr(double aliquotaUlterioreDetr) {
		_aliquotaUlterioreDetr = aliquotaUlterioreDetr;
	}

	public int getTipoVoceIva() {
		return _tipoVoceIva;
	}

	public void setTipoVoceIva(int tipoVoceIva) {
		_tipoVoceIva = tipoVoceIva;
	}

	public int getGestioneOmaggio() {
		return _gestioneOmaggio;
	}

	public void setGestioneOmaggio(int gestioneOmaggio) {
		_gestioneOmaggio = gestioneOmaggio;
	}

	public String getCodIvaSpeseOmaggio() {
		return _codIvaSpeseOmaggio;
	}

	public void setCodIvaSpeseOmaggio(String codIvaSpeseOmaggio) {
		_codIvaSpeseOmaggio = codIvaSpeseOmaggio;
	}

	public String getDescrizioneDocuemto() {
		return _descrizioneDocuemto;
	}

	public void setDescrizioneDocuemto(String descrizioneDocuemto) {
		_descrizioneDocuemto = descrizioneDocuemto;
	}

	private String _codiceIva;
	private String _descrizione;
	private double _aliquota;
	private double _aliquotaVandita;
	private double _aliquotaScorporo;
	private double _aliquotaUlterioreDetr;
	private int _tipoVoceIva;
	private int _gestioneOmaggio;
	private String _codIvaSpeseOmaggio;
	private String _descrizioneDocuemto;
}