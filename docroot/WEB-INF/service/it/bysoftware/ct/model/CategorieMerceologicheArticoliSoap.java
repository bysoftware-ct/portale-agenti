/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.CategorieMerceologicheArticoliServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.CategorieMerceologicheArticoliServiceSoap
 * @generated
 */
public class CategorieMerceologicheArticoliSoap implements Serializable {
	public static CategorieMerceologicheArticoliSoap toSoapModel(
		CategorieMerceologicheArticoli model) {
		CategorieMerceologicheArticoliSoap soapModel = new CategorieMerceologicheArticoliSoap();

		soapModel.setCodiceCategoria(model.getCodiceCategoria());
		soapModel.setDescrizione(model.getDescrizione());

		return soapModel;
	}

	public static CategorieMerceologicheArticoliSoap[] toSoapModels(
		CategorieMerceologicheArticoli[] models) {
		CategorieMerceologicheArticoliSoap[] soapModels = new CategorieMerceologicheArticoliSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CategorieMerceologicheArticoliSoap[][] toSoapModels(
		CategorieMerceologicheArticoli[][] models) {
		CategorieMerceologicheArticoliSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CategorieMerceologicheArticoliSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CategorieMerceologicheArticoliSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CategorieMerceologicheArticoliSoap[] toSoapModels(
		List<CategorieMerceologicheArticoli> models) {
		List<CategorieMerceologicheArticoliSoap> soapModels = new ArrayList<CategorieMerceologicheArticoliSoap>(models.size());

		for (CategorieMerceologicheArticoli model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CategorieMerceologicheArticoliSoap[soapModels.size()]);
	}

	public CategorieMerceologicheArticoliSoap() {
	}

	public String getPrimaryKey() {
		return _codiceCategoria;
	}

	public void setPrimaryKey(String pk) {
		setCodiceCategoria(pk);
	}

	public String getCodiceCategoria() {
		return _codiceCategoria;
	}

	public void setCodiceCategoria(String codiceCategoria) {
		_codiceCategoria = codiceCategoria;
	}

	public String getDescrizione() {
		return _descrizione;
	}

	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;
	}

	private String _codiceCategoria;
	private String _descrizione;
}