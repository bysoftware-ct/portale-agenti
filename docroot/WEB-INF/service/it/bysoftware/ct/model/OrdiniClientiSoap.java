/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.OrdiniClientiPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.OrdiniClientiServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.OrdiniClientiServiceSoap
 * @generated
 */
public class OrdiniClientiSoap implements Serializable {
	public static OrdiniClientiSoap toSoapModel(OrdiniClienti model) {
		OrdiniClientiSoap soapModel = new OrdiniClientiSoap();

		soapModel.setAnno(model.getAnno());
		soapModel.setCodiceAttivita(model.getCodiceAttivita());
		soapModel.setCodiceCentro(model.getCodiceCentro());
		soapModel.setCodiceDeposito(model.getCodiceDeposito());
		soapModel.setTipoOrdine(model.getTipoOrdine());
		soapModel.setNumeroOrdine(model.getNumeroOrdine());
		soapModel.setTipoDocumento(model.getTipoDocumento());
		soapModel.setCodiceContAnalitica(model.getCodiceContAnalitica());
		soapModel.setIdTipoDocumento(model.getIdTipoDocumento());
		soapModel.setStatoOrdine(model.getStatoOrdine());
		soapModel.setDescrEstremiDoc(model.getDescrEstremiDoc());
		soapModel.setTipoSoggetto(model.getTipoSoggetto());
		soapModel.setCodiceCliente(model.getCodiceCliente());
		soapModel.setNaturaTransazione(model.getNaturaTransazione());
		soapModel.setCodiceEsenzione(model.getCodiceEsenzione());
		soapModel.setCodiceDivisa(model.getCodiceDivisa());
		soapModel.setValoreCambio(model.getValoreCambio());
		soapModel.setDataValoreCambio(model.getDataValoreCambio());
		soapModel.setCodicePianoPag(model.getCodicePianoPag());
		soapModel.setInizioCalcoloPag(model.getInizioCalcoloPag());
		soapModel.setPercentualeScontoMaggiorazione(model.getPercentualeScontoMaggiorazione());
		soapModel.setPercentualeScontoProntaCassa(model.getPercentualeScontoProntaCassa());
		soapModel.setPercentualeProvvChiusura(model.getPercentualeProvvChiusura());
		soapModel.setDataDocumento(model.getDataDocumento());
		soapModel.setDataRegistrazione(model.getDataRegistrazione());
		soapModel.setCausaleEstrattoConto(model.getCausaleEstrattoConto());
		soapModel.setDataPrimaRata(model.getDataPrimaRata());
		soapModel.setDataUltimaRata(model.getDataUltimaRata());
		soapModel.setCodiceBanca(model.getCodiceBanca());
		soapModel.setCodiceAgenzia(model.getCodiceAgenzia());
		soapModel.setCodiceAgente(model.getCodiceAgente());
		soapModel.setCodiceGruppoAgenti(model.getCodiceGruppoAgenti());
		soapModel.setCodiceZona(model.getCodiceZona());
		soapModel.setCodiceSpedizione(model.getCodiceSpedizione());
		soapModel.setCodicePorto(model.getCodicePorto());
		soapModel.setCodiceDestinatario(model.getCodiceDestinatario());
		soapModel.setCodiceListino(model.getCodiceListino());
		soapModel.setCodiceLingua(model.getCodiceLingua());
		soapModel.setNumeroDecPrezzo(model.getNumeroDecPrezzo());
		soapModel.setNote(model.getNote());
		soapModel.setPercentualeSpeseTrasp(model.getPercentualeSpeseTrasp());
		soapModel.setSpeseTrasporto(model.getSpeseTrasporto());
		soapModel.setSpeseImballaggio(model.getSpeseImballaggio());
		soapModel.setSpeseVarie(model.getSpeseVarie());
		soapModel.setSpeseBanca(model.getSpeseBanca());
		soapModel.setCuraTrasporto(model.getCuraTrasporto());
		soapModel.setCausaleTrasporto(model.getCausaleTrasporto());
		soapModel.setAspettoEstriore(model.getAspettoEstriore());
		soapModel.setVettore1(model.getVettore1());
		soapModel.setVettore2(model.getVettore2());
		soapModel.setVettore3(model.getVettore3());
		soapModel.setNumeroColli(model.getNumeroColli());
		soapModel.setPesoLordo(model.getPesoLordo());
		soapModel.setPesoNetto(model.getPesoNetto());
		soapModel.setVolume(model.getVolume());
		soapModel.setNumeroCopie(model.getNumeroCopie());
		soapModel.setNumeroCopieStampate(model.getNumeroCopieStampate());
		soapModel.setInviatoEmail(model.getInviatoEmail());
		soapModel.setNomePDF(model.getNomePDF());
		soapModel.setRiferimentoOrdine(model.getRiferimentoOrdine());
		soapModel.setDataConferma(model.getDataConferma());
		soapModel.setConfermaStampata(model.getConfermaStampata());
		soapModel.setTotaleOrdine(model.getTotaleOrdine());
		soapModel.setCodiceIVATrasp(model.getCodiceIVATrasp());
		soapModel.setCodiceIVAImballo(model.getCodiceIVAImballo());
		soapModel.setCodiceIVAVarie(model.getCodiceIVAVarie());
		soapModel.setCodiceIVABanca(model.getCodiceIVABanca());
		soapModel.setLibStr1(model.getLibStr1());
		soapModel.setLibStr2(model.getLibStr2());
		soapModel.setLibStr3(model.getLibStr3());
		soapModel.setLibDbl1(model.getLibDbl1());
		soapModel.setLibDbl2(model.getLibDbl2());
		soapModel.setLibDbl3(model.getLibDbl3());
		soapModel.setLibDat1(model.getLibDat1());
		soapModel.setLibDat2(model.getLibDat2());
		soapModel.setLibDat3(model.getLibDat3());
		soapModel.setLibLng1(model.getLibLng1());
		soapModel.setLibLng2(model.getLibLng2());
		soapModel.setLibLng3(model.getLibLng3());

		return soapModel;
	}

	public static OrdiniClientiSoap[] toSoapModels(OrdiniClienti[] models) {
		OrdiniClientiSoap[] soapModels = new OrdiniClientiSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static OrdiniClientiSoap[][] toSoapModels(OrdiniClienti[][] models) {
		OrdiniClientiSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new OrdiniClientiSoap[models.length][models[0].length];
		}
		else {
			soapModels = new OrdiniClientiSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static OrdiniClientiSoap[] toSoapModels(List<OrdiniClienti> models) {
		List<OrdiniClientiSoap> soapModels = new ArrayList<OrdiniClientiSoap>(models.size());

		for (OrdiniClienti model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new OrdiniClientiSoap[soapModels.size()]);
	}

	public OrdiniClientiSoap() {
	}

	public OrdiniClientiPK getPrimaryKey() {
		return new OrdiniClientiPK(_anno, _codiceAttivita, _codiceCentro,
			_codiceDeposito, _tipoOrdine, _numeroOrdine);
	}

	public void setPrimaryKey(OrdiniClientiPK pk) {
		setAnno(pk.anno);
		setCodiceAttivita(pk.codiceAttivita);
		setCodiceCentro(pk.codiceCentro);
		setCodiceDeposito(pk.codiceDeposito);
		setTipoOrdine(pk.tipoOrdine);
		setNumeroOrdine(pk.numeroOrdine);
	}

	public int getAnno() {
		return _anno;
	}

	public void setAnno(int anno) {
		_anno = anno;
	}

	public String getCodiceAttivita() {
		return _codiceAttivita;
	}

	public void setCodiceAttivita(String codiceAttivita) {
		_codiceAttivita = codiceAttivita;
	}

	public String getCodiceCentro() {
		return _codiceCentro;
	}

	public void setCodiceCentro(String codiceCentro) {
		_codiceCentro = codiceCentro;
	}

	public String getCodiceDeposito() {
		return _codiceDeposito;
	}

	public void setCodiceDeposito(String codiceDeposito) {
		_codiceDeposito = codiceDeposito;
	}

	public int getTipoOrdine() {
		return _tipoOrdine;
	}

	public void setTipoOrdine(int tipoOrdine) {
		_tipoOrdine = tipoOrdine;
	}

	public int getNumeroOrdine() {
		return _numeroOrdine;
	}

	public void setNumeroOrdine(int numeroOrdine) {
		_numeroOrdine = numeroOrdine;
	}

	public String getTipoDocumento() {
		return _tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		_tipoDocumento = tipoDocumento;
	}

	public String getCodiceContAnalitica() {
		return _codiceContAnalitica;
	}

	public void setCodiceContAnalitica(String codiceContAnalitica) {
		_codiceContAnalitica = codiceContAnalitica;
	}

	public int getIdTipoDocumento() {
		return _idTipoDocumento;
	}

	public void setIdTipoDocumento(int idTipoDocumento) {
		_idTipoDocumento = idTipoDocumento;
	}

	public boolean getStatoOrdine() {
		return _statoOrdine;
	}

	public boolean isStatoOrdine() {
		return _statoOrdine;
	}

	public void setStatoOrdine(boolean statoOrdine) {
		_statoOrdine = statoOrdine;
	}

	public String getDescrEstremiDoc() {
		return _descrEstremiDoc;
	}

	public void setDescrEstremiDoc(String descrEstremiDoc) {
		_descrEstremiDoc = descrEstremiDoc;
	}

	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;
	}

	public String getCodiceCliente() {
		return _codiceCliente;
	}

	public void setCodiceCliente(String codiceCliente) {
		_codiceCliente = codiceCliente;
	}

	public String getNaturaTransazione() {
		return _naturaTransazione;
	}

	public void setNaturaTransazione(String naturaTransazione) {
		_naturaTransazione = naturaTransazione;
	}

	public String getCodiceEsenzione() {
		return _codiceEsenzione;
	}

	public void setCodiceEsenzione(String codiceEsenzione) {
		_codiceEsenzione = codiceEsenzione;
	}

	public String getCodiceDivisa() {
		return _codiceDivisa;
	}

	public void setCodiceDivisa(String codiceDivisa) {
		_codiceDivisa = codiceDivisa;
	}

	public double getValoreCambio() {
		return _valoreCambio;
	}

	public void setValoreCambio(double valoreCambio) {
		_valoreCambio = valoreCambio;
	}

	public Date getDataValoreCambio() {
		return _dataValoreCambio;
	}

	public void setDataValoreCambio(Date dataValoreCambio) {
		_dataValoreCambio = dataValoreCambio;
	}

	public String getCodicePianoPag() {
		return _codicePianoPag;
	}

	public void setCodicePianoPag(String codicePianoPag) {
		_codicePianoPag = codicePianoPag;
	}

	public Date getInizioCalcoloPag() {
		return _inizioCalcoloPag;
	}

	public void setInizioCalcoloPag(Date inizioCalcoloPag) {
		_inizioCalcoloPag = inizioCalcoloPag;
	}

	public double getPercentualeScontoMaggiorazione() {
		return _percentualeScontoMaggiorazione;
	}

	public void setPercentualeScontoMaggiorazione(
		double percentualeScontoMaggiorazione) {
		_percentualeScontoMaggiorazione = percentualeScontoMaggiorazione;
	}

	public double getPercentualeScontoProntaCassa() {
		return _percentualeScontoProntaCassa;
	}

	public void setPercentualeScontoProntaCassa(
		double percentualeScontoProntaCassa) {
		_percentualeScontoProntaCassa = percentualeScontoProntaCassa;
	}

	public double getPercentualeProvvChiusura() {
		return _percentualeProvvChiusura;
	}

	public void setPercentualeProvvChiusura(double percentualeProvvChiusura) {
		_percentualeProvvChiusura = percentualeProvvChiusura;
	}

	public Date getDataDocumento() {
		return _dataDocumento;
	}

	public void setDataDocumento(Date dataDocumento) {
		_dataDocumento = dataDocumento;
	}

	public Date getDataRegistrazione() {
		return _dataRegistrazione;
	}

	public void setDataRegistrazione(Date dataRegistrazione) {
		_dataRegistrazione = dataRegistrazione;
	}

	public String getCausaleEstrattoConto() {
		return _causaleEstrattoConto;
	}

	public void setCausaleEstrattoConto(String causaleEstrattoConto) {
		_causaleEstrattoConto = causaleEstrattoConto;
	}

	public Date getDataPrimaRata() {
		return _dataPrimaRata;
	}

	public void setDataPrimaRata(Date dataPrimaRata) {
		_dataPrimaRata = dataPrimaRata;
	}

	public Date getDataUltimaRata() {
		return _dataUltimaRata;
	}

	public void setDataUltimaRata(Date dataUltimaRata) {
		_dataUltimaRata = dataUltimaRata;
	}

	public String getCodiceBanca() {
		return _codiceBanca;
	}

	public void setCodiceBanca(String codiceBanca) {
		_codiceBanca = codiceBanca;
	}

	public String getCodiceAgenzia() {
		return _codiceAgenzia;
	}

	public void setCodiceAgenzia(String codiceAgenzia) {
		_codiceAgenzia = codiceAgenzia;
	}

	public String getCodiceAgente() {
		return _codiceAgente;
	}

	public void setCodiceAgente(String codiceAgente) {
		_codiceAgente = codiceAgente;
	}

	public String getCodiceGruppoAgenti() {
		return _codiceGruppoAgenti;
	}

	public void setCodiceGruppoAgenti(String codiceGruppoAgenti) {
		_codiceGruppoAgenti = codiceGruppoAgenti;
	}

	public String getCodiceZona() {
		return _codiceZona;
	}

	public void setCodiceZona(String codiceZona) {
		_codiceZona = codiceZona;
	}

	public String getCodiceSpedizione() {
		return _codiceSpedizione;
	}

	public void setCodiceSpedizione(String codiceSpedizione) {
		_codiceSpedizione = codiceSpedizione;
	}

	public String getCodicePorto() {
		return _codicePorto;
	}

	public void setCodicePorto(String codicePorto) {
		_codicePorto = codicePorto;
	}

	public String getCodiceDestinatario() {
		return _codiceDestinatario;
	}

	public void setCodiceDestinatario(String codiceDestinatario) {
		_codiceDestinatario = codiceDestinatario;
	}

	public String getCodiceListino() {
		return _codiceListino;
	}

	public void setCodiceListino(String codiceListino) {
		_codiceListino = codiceListino;
	}

	public String getCodiceLingua() {
		return _codiceLingua;
	}

	public void setCodiceLingua(String codiceLingua) {
		_codiceLingua = codiceLingua;
	}

	public int getNumeroDecPrezzo() {
		return _numeroDecPrezzo;
	}

	public void setNumeroDecPrezzo(int numeroDecPrezzo) {
		_numeroDecPrezzo = numeroDecPrezzo;
	}

	public String getNote() {
		return _note;
	}

	public void setNote(String note) {
		_note = note;
	}

	public double getPercentualeSpeseTrasp() {
		return _percentualeSpeseTrasp;
	}

	public void setPercentualeSpeseTrasp(double percentualeSpeseTrasp) {
		_percentualeSpeseTrasp = percentualeSpeseTrasp;
	}

	public double getSpeseTrasporto() {
		return _speseTrasporto;
	}

	public void setSpeseTrasporto(double speseTrasporto) {
		_speseTrasporto = speseTrasporto;
	}

	public double getSpeseImballaggio() {
		return _speseImballaggio;
	}

	public void setSpeseImballaggio(double speseImballaggio) {
		_speseImballaggio = speseImballaggio;
	}

	public double getSpeseVarie() {
		return _speseVarie;
	}

	public void setSpeseVarie(double speseVarie) {
		_speseVarie = speseVarie;
	}

	public double getSpeseBanca() {
		return _speseBanca;
	}

	public void setSpeseBanca(double speseBanca) {
		_speseBanca = speseBanca;
	}

	public String getCuraTrasporto() {
		return _curaTrasporto;
	}

	public void setCuraTrasporto(String curaTrasporto) {
		_curaTrasporto = curaTrasporto;
	}

	public String getCausaleTrasporto() {
		return _causaleTrasporto;
	}

	public void setCausaleTrasporto(String causaleTrasporto) {
		_causaleTrasporto = causaleTrasporto;
	}

	public String getAspettoEstriore() {
		return _aspettoEstriore;
	}

	public void setAspettoEstriore(String aspettoEstriore) {
		_aspettoEstriore = aspettoEstriore;
	}

	public String getVettore1() {
		return _vettore1;
	}

	public void setVettore1(String vettore1) {
		_vettore1 = vettore1;
	}

	public String getVettore2() {
		return _vettore2;
	}

	public void setVettore2(String vettore2) {
		_vettore2 = vettore2;
	}

	public String getVettore3() {
		return _vettore3;
	}

	public void setVettore3(String vettore3) {
		_vettore3 = vettore3;
	}

	public int getNumeroColli() {
		return _numeroColli;
	}

	public void setNumeroColli(int numeroColli) {
		_numeroColli = numeroColli;
	}

	public double getPesoLordo() {
		return _pesoLordo;
	}

	public void setPesoLordo(double pesoLordo) {
		_pesoLordo = pesoLordo;
	}

	public double getPesoNetto() {
		return _pesoNetto;
	}

	public void setPesoNetto(double pesoNetto) {
		_pesoNetto = pesoNetto;
	}

	public double getVolume() {
		return _volume;
	}

	public void setVolume(double volume) {
		_volume = volume;
	}

	public int getNumeroCopie() {
		return _numeroCopie;
	}

	public void setNumeroCopie(int numeroCopie) {
		_numeroCopie = numeroCopie;
	}

	public int getNumeroCopieStampate() {
		return _numeroCopieStampate;
	}

	public void setNumeroCopieStampate(int numeroCopieStampate) {
		_numeroCopieStampate = numeroCopieStampate;
	}

	public boolean getInviatoEmail() {
		return _inviatoEmail;
	}

	public boolean isInviatoEmail() {
		return _inviatoEmail;
	}

	public void setInviatoEmail(boolean inviatoEmail) {
		_inviatoEmail = inviatoEmail;
	}

	public String getNomePDF() {
		return _nomePDF;
	}

	public void setNomePDF(String nomePDF) {
		_nomePDF = nomePDF;
	}

	public String getRiferimentoOrdine() {
		return _riferimentoOrdine;
	}

	public void setRiferimentoOrdine(String riferimentoOrdine) {
		_riferimentoOrdine = riferimentoOrdine;
	}

	public Date getDataConferma() {
		return _dataConferma;
	}

	public void setDataConferma(Date dataConferma) {
		_dataConferma = dataConferma;
	}

	public boolean getConfermaStampata() {
		return _confermaStampata;
	}

	public boolean isConfermaStampata() {
		return _confermaStampata;
	}

	public void setConfermaStampata(boolean confermaStampata) {
		_confermaStampata = confermaStampata;
	}

	public double getTotaleOrdine() {
		return _totaleOrdine;
	}

	public void setTotaleOrdine(double totaleOrdine) {
		_totaleOrdine = totaleOrdine;
	}

	public String getCodiceIVATrasp() {
		return _codiceIVATrasp;
	}

	public void setCodiceIVATrasp(String codiceIVATrasp) {
		_codiceIVATrasp = codiceIVATrasp;
	}

	public String getCodiceIVAImballo() {
		return _codiceIVAImballo;
	}

	public void setCodiceIVAImballo(String codiceIVAImballo) {
		_codiceIVAImballo = codiceIVAImballo;
	}

	public String getCodiceIVAVarie() {
		return _codiceIVAVarie;
	}

	public void setCodiceIVAVarie(String codiceIVAVarie) {
		_codiceIVAVarie = codiceIVAVarie;
	}

	public String getCodiceIVABanca() {
		return _codiceIVABanca;
	}

	public void setCodiceIVABanca(String codiceIVABanca) {
		_codiceIVABanca = codiceIVABanca;
	}

	public String getLibStr1() {
		return _libStr1;
	}

	public void setLibStr1(String libStr1) {
		_libStr1 = libStr1;
	}

	public String getLibStr2() {
		return _libStr2;
	}

	public void setLibStr2(String libStr2) {
		_libStr2 = libStr2;
	}

	public String getLibStr3() {
		return _libStr3;
	}

	public void setLibStr3(String libStr3) {
		_libStr3 = libStr3;
	}

	public double getLibDbl1() {
		return _libDbl1;
	}

	public void setLibDbl1(double libDbl1) {
		_libDbl1 = libDbl1;
	}

	public double getLibDbl2() {
		return _libDbl2;
	}

	public void setLibDbl2(double libDbl2) {
		_libDbl2 = libDbl2;
	}

	public double getLibDbl3() {
		return _libDbl3;
	}

	public void setLibDbl3(double libDbl3) {
		_libDbl3 = libDbl3;
	}

	public Date getLibDat1() {
		return _libDat1;
	}

	public void setLibDat1(Date libDat1) {
		_libDat1 = libDat1;
	}

	public Date getLibDat2() {
		return _libDat2;
	}

	public void setLibDat2(Date libDat2) {
		_libDat2 = libDat2;
	}

	public Date getLibDat3() {
		return _libDat3;
	}

	public void setLibDat3(Date libDat3) {
		_libDat3 = libDat3;
	}

	public long getLibLng1() {
		return _libLng1;
	}

	public void setLibLng1(long libLng1) {
		_libLng1 = libLng1;
	}

	public long getLibLng2() {
		return _libLng2;
	}

	public void setLibLng2(long libLng2) {
		_libLng2 = libLng2;
	}

	public long getLibLng3() {
		return _libLng3;
	}

	public void setLibLng3(long libLng3) {
		_libLng3 = libLng3;
	}

	private int _anno;
	private String _codiceAttivita;
	private String _codiceCentro;
	private String _codiceDeposito;
	private int _tipoOrdine;
	private int _numeroOrdine;
	private String _tipoDocumento;
	private String _codiceContAnalitica;
	private int _idTipoDocumento;
	private boolean _statoOrdine;
	private String _descrEstremiDoc;
	private boolean _tipoSoggetto;
	private String _codiceCliente;
	private String _naturaTransazione;
	private String _codiceEsenzione;
	private String _codiceDivisa;
	private double _valoreCambio;
	private Date _dataValoreCambio;
	private String _codicePianoPag;
	private Date _inizioCalcoloPag;
	private double _percentualeScontoMaggiorazione;
	private double _percentualeScontoProntaCassa;
	private double _percentualeProvvChiusura;
	private Date _dataDocumento;
	private Date _dataRegistrazione;
	private String _causaleEstrattoConto;
	private Date _dataPrimaRata;
	private Date _dataUltimaRata;
	private String _codiceBanca;
	private String _codiceAgenzia;
	private String _codiceAgente;
	private String _codiceGruppoAgenti;
	private String _codiceZona;
	private String _codiceSpedizione;
	private String _codicePorto;
	private String _codiceDestinatario;
	private String _codiceListino;
	private String _codiceLingua;
	private int _numeroDecPrezzo;
	private String _note;
	private double _percentualeSpeseTrasp;
	private double _speseTrasporto;
	private double _speseImballaggio;
	private double _speseVarie;
	private double _speseBanca;
	private String _curaTrasporto;
	private String _causaleTrasporto;
	private String _aspettoEstriore;
	private String _vettore1;
	private String _vettore2;
	private String _vettore3;
	private int _numeroColli;
	private double _pesoLordo;
	private double _pesoNetto;
	private double _volume;
	private int _numeroCopie;
	private int _numeroCopieStampate;
	private boolean _inviatoEmail;
	private String _nomePDF;
	private String _riferimentoOrdine;
	private Date _dataConferma;
	private boolean _confermaStampata;
	private double _totaleOrdine;
	private String _codiceIVATrasp;
	private String _codiceIVAImballo;
	private String _codiceIVAVarie;
	private String _codiceIVABanca;
	private String _libStr1;
	private String _libStr2;
	private String _libStr3;
	private double _libDbl1;
	private double _libDbl2;
	private double _libDbl3;
	private Date _libDat1;
	private Date _libDat2;
	private Date _libDat3;
	private long _libLng1;
	private long _libLng2;
	private long _libLng3;
}