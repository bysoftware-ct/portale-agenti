/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.ScontiArticoliLocalServiceUtil;
import it.bysoftware.ct.service.persistence.ScontiArticoliPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class ScontiArticoliClp extends BaseModelImpl<ScontiArticoli>
	implements ScontiArticoli {
	public ScontiArticoliClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ScontiArticoli.class;
	}

	@Override
	public String getModelClassName() {
		return ScontiArticoli.class.getName();
	}

	@Override
	public ScontiArticoliPK getPrimaryKey() {
		return new ScontiArticoliPK(_codScontoCliente, _codScontoArticolo);
	}

	@Override
	public void setPrimaryKey(ScontiArticoliPK primaryKey) {
		setCodScontoCliente(primaryKey.codScontoCliente);
		setCodScontoArticolo(primaryKey.codScontoArticolo);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new ScontiArticoliPK(_codScontoCliente, _codScontoArticolo);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((ScontiArticoliPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codScontoCliente", getCodScontoCliente());
		attributes.put("codScontoArticolo", getCodScontoArticolo());
		attributes.put("sconto", getSconto());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codScontoCliente = (String)attributes.get("codScontoCliente");

		if (codScontoCliente != null) {
			setCodScontoCliente(codScontoCliente);
		}

		String codScontoArticolo = (String)attributes.get("codScontoArticolo");

		if (codScontoArticolo != null) {
			setCodScontoArticolo(codScontoArticolo);
		}

		Double sconto = (Double)attributes.get("sconto");

		if (sconto != null) {
			setSconto(sconto);
		}
	}

	@Override
	public String getCodScontoCliente() {
		return _codScontoCliente;
	}

	@Override
	public void setCodScontoCliente(String codScontoCliente) {
		_codScontoCliente = codScontoCliente;

		if (_scontiArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _scontiArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodScontoCliente",
						String.class);

				method.invoke(_scontiArticoliRemoteModel, codScontoCliente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodScontoArticolo() {
		return _codScontoArticolo;
	}

	@Override
	public void setCodScontoArticolo(String codScontoArticolo) {
		_codScontoArticolo = codScontoArticolo;

		if (_scontiArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _scontiArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setCodScontoArticolo",
						String.class);

				method.invoke(_scontiArticoliRemoteModel, codScontoArticolo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getSconto() {
		return _sconto;
	}

	@Override
	public void setSconto(double sconto) {
		_sconto = sconto;

		if (_scontiArticoliRemoteModel != null) {
			try {
				Class<?> clazz = _scontiArticoliRemoteModel.getClass();

				Method method = clazz.getMethod("setSconto", double.class);

				method.invoke(_scontiArticoliRemoteModel, sconto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getScontiArticoliRemoteModel() {
		return _scontiArticoliRemoteModel;
	}

	public void setScontiArticoliRemoteModel(
		BaseModel<?> scontiArticoliRemoteModel) {
		_scontiArticoliRemoteModel = scontiArticoliRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _scontiArticoliRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_scontiArticoliRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ScontiArticoliLocalServiceUtil.addScontiArticoli(this);
		}
		else {
			ScontiArticoliLocalServiceUtil.updateScontiArticoli(this);
		}
	}

	@Override
	public ScontiArticoli toEscapedModel() {
		return (ScontiArticoli)ProxyUtil.newProxyInstance(ScontiArticoli.class.getClassLoader(),
			new Class[] { ScontiArticoli.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ScontiArticoliClp clone = new ScontiArticoliClp();

		clone.setCodScontoCliente(getCodScontoCliente());
		clone.setCodScontoArticolo(getCodScontoArticolo());
		clone.setSconto(getSconto());

		return clone;
	}

	@Override
	public int compareTo(ScontiArticoli scontiArticoli) {
		ScontiArticoliPK primaryKey = scontiArticoli.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ScontiArticoliClp)) {
			return false;
		}

		ScontiArticoliClp scontiArticoli = (ScontiArticoliClp)obj;

		ScontiArticoliPK primaryKey = scontiArticoli.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{codScontoCliente=");
		sb.append(getCodScontoCliente());
		sb.append(", codScontoArticolo=");
		sb.append(getCodScontoArticolo());
		sb.append(", sconto=");
		sb.append(getSconto());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.ScontiArticoli");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>codScontoCliente</column-name><column-value><![CDATA[");
		sb.append(getCodScontoCliente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codScontoArticolo</column-name><column-value><![CDATA[");
		sb.append(getCodScontoArticolo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sconto</column-name><column-value><![CDATA[");
		sb.append(getSconto());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _codScontoCliente;
	private String _codScontoArticolo;
	private double _sconto;
	private BaseModel<?> _scontiArticoliRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}