/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.WKOrdiniClientiLocalServiceUtil;
import it.bysoftware.ct.service.persistence.WKOrdiniClientiPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class WKOrdiniClientiClp extends BaseModelImpl<WKOrdiniClienti>
	implements WKOrdiniClienti {
	public WKOrdiniClientiClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return WKOrdiniClienti.class;
	}

	@Override
	public String getModelClassName() {
		return WKOrdiniClienti.class.getName();
	}

	@Override
	public WKOrdiniClientiPK getPrimaryKey() {
		return new WKOrdiniClientiPK(_anno, _tipoOrdine, _numeroOrdine);
	}

	@Override
	public void setPrimaryKey(WKOrdiniClientiPK primaryKey) {
		setAnno(primaryKey.anno);
		setTipoOrdine(primaryKey.tipoOrdine);
		setNumeroOrdine(primaryKey.numeroOrdine);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new WKOrdiniClientiPK(_anno, _tipoOrdine, _numeroOrdine);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((WKOrdiniClientiPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("anno", getAnno());
		attributes.put("tipoOrdine", getTipoOrdine());
		attributes.put("numeroOrdine", getNumeroOrdine());
		attributes.put("tipoDocumento", getTipoDocumento());
		attributes.put("idTipoDocumento", getIdTipoDocumento());
		attributes.put("statoOrdine", getStatoOrdine());
		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceCliente", getCodiceCliente());
		attributes.put("codiceEsenzione", getCodiceEsenzione());
		attributes.put("codiceDivisa", getCodiceDivisa());
		attributes.put("valoreCambio", getValoreCambio());
		attributes.put("dataValoreCambio", getDataValoreCambio());
		attributes.put("codicePianoPag", getCodicePianoPag());
		attributes.put("inizioCalcoloPag", getInizioCalcoloPag());
		attributes.put("percentualeScontoMaggiorazione",
			getPercentualeScontoMaggiorazione());
		attributes.put("percentualeScontoProntaCassa",
			getPercentualeScontoProntaCassa());
		attributes.put("dataDocumento", getDataDocumento());
		attributes.put("dataRegistrazione", getDataRegistrazione());
		attributes.put("causaleEstrattoConto", getCausaleEstrattoConto());
		attributes.put("codiceAgente", getCodiceAgente());
		attributes.put("codiceGruppoAgenti", getCodiceGruppoAgenti());
		attributes.put("codiceZona", getCodiceZona());
		attributes.put("codiceDestinatario", getCodiceDestinatario());
		attributes.put("codiceListino", getCodiceListino());
		attributes.put("numeroDecPrezzo", getNumeroDecPrezzo());
		attributes.put("note", getNote());
		attributes.put("inviatoEmail", getInviatoEmail());
		attributes.put("nomePDF", getNomePDF());
		attributes.put("riferimentoOrdine", getRiferimentoOrdine());
		attributes.put("dataConferma", getDataConferma());
		attributes.put("confermaStampata", getConfermaStampata());
		attributes.put("totaleOrdine", getTotaleOrdine());
		attributes.put("libStr1", getLibStr1());
		attributes.put("libStr2", getLibStr2());
		attributes.put("libStr3", getLibStr3());
		attributes.put("libDbl1", getLibDbl1());
		attributes.put("libDbl2", getLibDbl2());
		attributes.put("libDbl3", getLibDbl3());
		attributes.put("libDat1", getLibDat1());
		attributes.put("libDat2", getLibDat2());
		attributes.put("libDat3", getLibDat3());
		attributes.put("libLng1", getLibLng1());
		attributes.put("libLng2", getLibLng2());
		attributes.put("libLng3", getLibLng3());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer anno = (Integer)attributes.get("anno");

		if (anno != null) {
			setAnno(anno);
		}

		Integer tipoOrdine = (Integer)attributes.get("tipoOrdine");

		if (tipoOrdine != null) {
			setTipoOrdine(tipoOrdine);
		}

		Integer numeroOrdine = (Integer)attributes.get("numeroOrdine");

		if (numeroOrdine != null) {
			setNumeroOrdine(numeroOrdine);
		}

		String tipoDocumento = (String)attributes.get("tipoDocumento");

		if (tipoDocumento != null) {
			setTipoDocumento(tipoDocumento);
		}

		Integer idTipoDocumento = (Integer)attributes.get("idTipoDocumento");

		if (idTipoDocumento != null) {
			setIdTipoDocumento(idTipoDocumento);
		}

		Boolean statoOrdine = (Boolean)attributes.get("statoOrdine");

		if (statoOrdine != null) {
			setStatoOrdine(statoOrdine);
		}

		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceCliente = (String)attributes.get("codiceCliente");

		if (codiceCliente != null) {
			setCodiceCliente(codiceCliente);
		}

		String codiceEsenzione = (String)attributes.get("codiceEsenzione");

		if (codiceEsenzione != null) {
			setCodiceEsenzione(codiceEsenzione);
		}

		String codiceDivisa = (String)attributes.get("codiceDivisa");

		if (codiceDivisa != null) {
			setCodiceDivisa(codiceDivisa);
		}

		Double valoreCambio = (Double)attributes.get("valoreCambio");

		if (valoreCambio != null) {
			setValoreCambio(valoreCambio);
		}

		Date dataValoreCambio = (Date)attributes.get("dataValoreCambio");

		if (dataValoreCambio != null) {
			setDataValoreCambio(dataValoreCambio);
		}

		String codicePianoPag = (String)attributes.get("codicePianoPag");

		if (codicePianoPag != null) {
			setCodicePianoPag(codicePianoPag);
		}

		Date inizioCalcoloPag = (Date)attributes.get("inizioCalcoloPag");

		if (inizioCalcoloPag != null) {
			setInizioCalcoloPag(inizioCalcoloPag);
		}

		Double percentualeScontoMaggiorazione = (Double)attributes.get(
				"percentualeScontoMaggiorazione");

		if (percentualeScontoMaggiorazione != null) {
			setPercentualeScontoMaggiorazione(percentualeScontoMaggiorazione);
		}

		Double percentualeScontoProntaCassa = (Double)attributes.get(
				"percentualeScontoProntaCassa");

		if (percentualeScontoProntaCassa != null) {
			setPercentualeScontoProntaCassa(percentualeScontoProntaCassa);
		}

		Date dataDocumento = (Date)attributes.get("dataDocumento");

		if (dataDocumento != null) {
			setDataDocumento(dataDocumento);
		}

		Date dataRegistrazione = (Date)attributes.get("dataRegistrazione");

		if (dataRegistrazione != null) {
			setDataRegistrazione(dataRegistrazione);
		}

		String causaleEstrattoConto = (String)attributes.get(
				"causaleEstrattoConto");

		if (causaleEstrattoConto != null) {
			setCausaleEstrattoConto(causaleEstrattoConto);
		}

		String codiceAgente = (String)attributes.get("codiceAgente");

		if (codiceAgente != null) {
			setCodiceAgente(codiceAgente);
		}

		String codiceGruppoAgenti = (String)attributes.get("codiceGruppoAgenti");

		if (codiceGruppoAgenti != null) {
			setCodiceGruppoAgenti(codiceGruppoAgenti);
		}

		String codiceZona = (String)attributes.get("codiceZona");

		if (codiceZona != null) {
			setCodiceZona(codiceZona);
		}

		String codiceDestinatario = (String)attributes.get("codiceDestinatario");

		if (codiceDestinatario != null) {
			setCodiceDestinatario(codiceDestinatario);
		}

		String codiceListino = (String)attributes.get("codiceListino");

		if (codiceListino != null) {
			setCodiceListino(codiceListino);
		}

		Integer numeroDecPrezzo = (Integer)attributes.get("numeroDecPrezzo");

		if (numeroDecPrezzo != null) {
			setNumeroDecPrezzo(numeroDecPrezzo);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}

		Boolean inviatoEmail = (Boolean)attributes.get("inviatoEmail");

		if (inviatoEmail != null) {
			setInviatoEmail(inviatoEmail);
		}

		String nomePDF = (String)attributes.get("nomePDF");

		if (nomePDF != null) {
			setNomePDF(nomePDF);
		}

		String riferimentoOrdine = (String)attributes.get("riferimentoOrdine");

		if (riferimentoOrdine != null) {
			setRiferimentoOrdine(riferimentoOrdine);
		}

		Date dataConferma = (Date)attributes.get("dataConferma");

		if (dataConferma != null) {
			setDataConferma(dataConferma);
		}

		Boolean confermaStampata = (Boolean)attributes.get("confermaStampata");

		if (confermaStampata != null) {
			setConfermaStampata(confermaStampata);
		}

		Double totaleOrdine = (Double)attributes.get("totaleOrdine");

		if (totaleOrdine != null) {
			setTotaleOrdine(totaleOrdine);
		}

		String libStr1 = (String)attributes.get("libStr1");

		if (libStr1 != null) {
			setLibStr1(libStr1);
		}

		String libStr2 = (String)attributes.get("libStr2");

		if (libStr2 != null) {
			setLibStr2(libStr2);
		}

		String libStr3 = (String)attributes.get("libStr3");

		if (libStr3 != null) {
			setLibStr3(libStr3);
		}

		Double libDbl1 = (Double)attributes.get("libDbl1");

		if (libDbl1 != null) {
			setLibDbl1(libDbl1);
		}

		Double libDbl2 = (Double)attributes.get("libDbl2");

		if (libDbl2 != null) {
			setLibDbl2(libDbl2);
		}

		Double libDbl3 = (Double)attributes.get("libDbl3");

		if (libDbl3 != null) {
			setLibDbl3(libDbl3);
		}

		Date libDat1 = (Date)attributes.get("libDat1");

		if (libDat1 != null) {
			setLibDat1(libDat1);
		}

		Date libDat2 = (Date)attributes.get("libDat2");

		if (libDat2 != null) {
			setLibDat2(libDat2);
		}

		Date libDat3 = (Date)attributes.get("libDat3");

		if (libDat3 != null) {
			setLibDat3(libDat3);
		}

		Long libLng1 = (Long)attributes.get("libLng1");

		if (libLng1 != null) {
			setLibLng1(libLng1);
		}

		Long libLng2 = (Long)attributes.get("libLng2");

		if (libLng2 != null) {
			setLibLng2(libLng2);
		}

		Long libLng3 = (Long)attributes.get("libLng3");

		if (libLng3 != null) {
			setLibLng3(libLng3);
		}
	}

	@Override
	public int getAnno() {
		return _anno;
	}

	@Override
	public void setAnno(int anno) {
		_anno = anno;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setAnno", int.class);

				method.invoke(_wkOrdiniClientiRemoteModel, anno);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTipoOrdine() {
		return _tipoOrdine;
	}

	@Override
	public void setTipoOrdine(int tipoOrdine) {
		_tipoOrdine = tipoOrdine;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoOrdine", int.class);

				method.invoke(_wkOrdiniClientiRemoteModel, tipoOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroOrdine() {
		return _numeroOrdine;
	}

	@Override
	public void setNumeroOrdine(int numeroOrdine) {
		_numeroOrdine = numeroOrdine;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroOrdine", int.class);

				method.invoke(_wkOrdiniClientiRemoteModel, numeroOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoDocumento() {
		return _tipoDocumento;
	}

	@Override
	public void setTipoDocumento(String tipoDocumento) {
		_tipoDocumento = tipoDocumento;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoDocumento", String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, tipoDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getIdTipoDocumento() {
		return _idTipoDocumento;
	}

	@Override
	public void setIdTipoDocumento(int idTipoDocumento) {
		_idTipoDocumento = idTipoDocumento;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setIdTipoDocumento", int.class);

				method.invoke(_wkOrdiniClientiRemoteModel, idTipoDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getStatoOrdine() {
		return _statoOrdine;
	}

	@Override
	public boolean isStatoOrdine() {
		return _statoOrdine;
	}

	@Override
	public void setStatoOrdine(boolean statoOrdine) {
		_statoOrdine = statoOrdine;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setStatoOrdine", boolean.class);

				method.invoke(_wkOrdiniClientiRemoteModel, statoOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoSoggetto", boolean.class);

				method.invoke(_wkOrdiniClientiRemoteModel, tipoSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCliente() {
		return _codiceCliente;
	}

	@Override
	public void setCodiceCliente(String codiceCliente) {
		_codiceCliente = codiceCliente;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCliente", String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, codiceCliente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceEsenzione() {
		return _codiceEsenzione;
	}

	@Override
	public void setCodiceEsenzione(String codiceEsenzione) {
		_codiceEsenzione = codiceEsenzione;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceEsenzione",
						String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, codiceEsenzione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDivisa() {
		return _codiceDivisa;
	}

	@Override
	public void setCodiceDivisa(String codiceDivisa) {
		_codiceDivisa = codiceDivisa;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDivisa", String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, codiceDivisa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getValoreCambio() {
		return _valoreCambio;
	}

	@Override
	public void setValoreCambio(double valoreCambio) {
		_valoreCambio = valoreCambio;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setValoreCambio", double.class);

				method.invoke(_wkOrdiniClientiRemoteModel, valoreCambio);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataValoreCambio() {
		return _dataValoreCambio;
	}

	@Override
	public void setDataValoreCambio(Date dataValoreCambio) {
		_dataValoreCambio = dataValoreCambio;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataValoreCambio",
						Date.class);

				method.invoke(_wkOrdiniClientiRemoteModel, dataValoreCambio);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodicePianoPag() {
		return _codicePianoPag;
	}

	@Override
	public void setCodicePianoPag(String codicePianoPag) {
		_codicePianoPag = codicePianoPag;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodicePianoPag",
						String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, codicePianoPag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getInizioCalcoloPag() {
		return _inizioCalcoloPag;
	}

	@Override
	public void setInizioCalcoloPag(Date inizioCalcoloPag) {
		_inizioCalcoloPag = inizioCalcoloPag;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setInizioCalcoloPag",
						Date.class);

				method.invoke(_wkOrdiniClientiRemoteModel, inizioCalcoloPag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPercentualeScontoMaggiorazione() {
		return _percentualeScontoMaggiorazione;
	}

	@Override
	public void setPercentualeScontoMaggiorazione(
		double percentualeScontoMaggiorazione) {
		_percentualeScontoMaggiorazione = percentualeScontoMaggiorazione;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setPercentualeScontoMaggiorazione",
						double.class);

				method.invoke(_wkOrdiniClientiRemoteModel,
					percentualeScontoMaggiorazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPercentualeScontoProntaCassa() {
		return _percentualeScontoProntaCassa;
	}

	@Override
	public void setPercentualeScontoProntaCassa(
		double percentualeScontoProntaCassa) {
		_percentualeScontoProntaCassa = percentualeScontoProntaCassa;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setPercentualeScontoProntaCassa",
						double.class);

				method.invoke(_wkOrdiniClientiRemoteModel,
					percentualeScontoProntaCassa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataDocumento() {
		return _dataDocumento;
	}

	@Override
	public void setDataDocumento(Date dataDocumento) {
		_dataDocumento = dataDocumento;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataDocumento", Date.class);

				method.invoke(_wkOrdiniClientiRemoteModel, dataDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataRegistrazione() {
		return _dataRegistrazione;
	}

	@Override
	public void setDataRegistrazione(Date dataRegistrazione) {
		_dataRegistrazione = dataRegistrazione;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataRegistrazione",
						Date.class);

				method.invoke(_wkOrdiniClientiRemoteModel, dataRegistrazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCausaleEstrattoConto() {
		return _causaleEstrattoConto;
	}

	@Override
	public void setCausaleEstrattoConto(String causaleEstrattoConto) {
		_causaleEstrattoConto = causaleEstrattoConto;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCausaleEstrattoConto",
						String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, causaleEstrattoConto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceAgente() {
		return _codiceAgente;
	}

	@Override
	public void setCodiceAgente(String codiceAgente) {
		_codiceAgente = codiceAgente;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceAgente", String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, codiceAgente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceGruppoAgenti() {
		return _codiceGruppoAgenti;
	}

	@Override
	public void setCodiceGruppoAgenti(String codiceGruppoAgenti) {
		_codiceGruppoAgenti = codiceGruppoAgenti;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceGruppoAgenti",
						String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, codiceGruppoAgenti);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceZona() {
		return _codiceZona;
	}

	@Override
	public void setCodiceZona(String codiceZona) {
		_codiceZona = codiceZona;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceZona", String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, codiceZona);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDestinatario() {
		return _codiceDestinatario;
	}

	@Override
	public void setCodiceDestinatario(String codiceDestinatario) {
		_codiceDestinatario = codiceDestinatario;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDestinatario",
						String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, codiceDestinatario);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceListino() {
		return _codiceListino;
	}

	@Override
	public void setCodiceListino(String codiceListino) {
		_codiceListino = codiceListino;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceListino", String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, codiceListino);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumeroDecPrezzo() {
		return _numeroDecPrezzo;
	}

	@Override
	public void setNumeroDecPrezzo(int numeroDecPrezzo) {
		_numeroDecPrezzo = numeroDecPrezzo;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroDecPrezzo", int.class);

				method.invoke(_wkOrdiniClientiRemoteModel, numeroDecPrezzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNote() {
		return _note;
	}

	@Override
	public void setNote(String note) {
		_note = note;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNote", String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, note);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getInviatoEmail() {
		return _inviatoEmail;
	}

	@Override
	public boolean isInviatoEmail() {
		return _inviatoEmail;
	}

	@Override
	public void setInviatoEmail(boolean inviatoEmail) {
		_inviatoEmail = inviatoEmail;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setInviatoEmail", boolean.class);

				method.invoke(_wkOrdiniClientiRemoteModel, inviatoEmail);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNomePDF() {
		return _nomePDF;
	}

	@Override
	public void setNomePDF(String nomePDF) {
		_nomePDF = nomePDF;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setNomePDF", String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, nomePDF);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRiferimentoOrdine() {
		return _riferimentoOrdine;
	}

	@Override
	public void setRiferimentoOrdine(String riferimentoOrdine) {
		_riferimentoOrdine = riferimentoOrdine;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setRiferimentoOrdine",
						String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, riferimentoOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDataConferma() {
		return _dataConferma;
	}

	@Override
	public void setDataConferma(Date dataConferma) {
		_dataConferma = dataConferma;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setDataConferma", Date.class);

				method.invoke(_wkOrdiniClientiRemoteModel, dataConferma);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getConfermaStampata() {
		return _confermaStampata;
	}

	@Override
	public boolean isConfermaStampata() {
		return _confermaStampata;
	}

	@Override
	public void setConfermaStampata(boolean confermaStampata) {
		_confermaStampata = confermaStampata;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setConfermaStampata",
						boolean.class);

				method.invoke(_wkOrdiniClientiRemoteModel, confermaStampata);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getTotaleOrdine() {
		return _totaleOrdine;
	}

	@Override
	public void setTotaleOrdine(double totaleOrdine) {
		_totaleOrdine = totaleOrdine;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setTotaleOrdine", double.class);

				method.invoke(_wkOrdiniClientiRemoteModel, totaleOrdine);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr1() {
		return _libStr1;
	}

	@Override
	public void setLibStr1(String libStr1) {
		_libStr1 = libStr1;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr1", String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, libStr1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr2() {
		return _libStr2;
	}

	@Override
	public void setLibStr2(String libStr2) {
		_libStr2 = libStr2;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr2", String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, libStr2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr3() {
		return _libStr3;
	}

	@Override
	public void setLibStr3(String libStr3) {
		_libStr3 = libStr3;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr3", String.class);

				method.invoke(_wkOrdiniClientiRemoteModel, libStr3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl1() {
		return _libDbl1;
	}

	@Override
	public void setLibDbl1(double libDbl1) {
		_libDbl1 = libDbl1;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl1", double.class);

				method.invoke(_wkOrdiniClientiRemoteModel, libDbl1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl2() {
		return _libDbl2;
	}

	@Override
	public void setLibDbl2(double libDbl2) {
		_libDbl2 = libDbl2;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl2", double.class);

				method.invoke(_wkOrdiniClientiRemoteModel, libDbl2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl3() {
		return _libDbl3;
	}

	@Override
	public void setLibDbl3(double libDbl3) {
		_libDbl3 = libDbl3;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl3", double.class);

				method.invoke(_wkOrdiniClientiRemoteModel, libDbl3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat1() {
		return _libDat1;
	}

	@Override
	public void setLibDat1(Date libDat1) {
		_libDat1 = libDat1;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat1", Date.class);

				method.invoke(_wkOrdiniClientiRemoteModel, libDat1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat2() {
		return _libDat2;
	}

	@Override
	public void setLibDat2(Date libDat2) {
		_libDat2 = libDat2;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat2", Date.class);

				method.invoke(_wkOrdiniClientiRemoteModel, libDat2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat3() {
		return _libDat3;
	}

	@Override
	public void setLibDat3(Date libDat3) {
		_libDat3 = libDat3;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat3", Date.class);

				method.invoke(_wkOrdiniClientiRemoteModel, libDat3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng1() {
		return _libLng1;
	}

	@Override
	public void setLibLng1(long libLng1) {
		_libLng1 = libLng1;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng1", long.class);

				method.invoke(_wkOrdiniClientiRemoteModel, libLng1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng2() {
		return _libLng2;
	}

	@Override
	public void setLibLng2(long libLng2) {
		_libLng2 = libLng2;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng2", long.class);

				method.invoke(_wkOrdiniClientiRemoteModel, libLng2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng3() {
		return _libLng3;
	}

	@Override
	public void setLibLng3(long libLng3) {
		_libLng3 = libLng3;

		if (_wkOrdiniClientiRemoteModel != null) {
			try {
				Class<?> clazz = _wkOrdiniClientiRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng3", long.class);

				method.invoke(_wkOrdiniClientiRemoteModel, libLng3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getWKOrdiniClientiRemoteModel() {
		return _wkOrdiniClientiRemoteModel;
	}

	public void setWKOrdiniClientiRemoteModel(
		BaseModel<?> wkOrdiniClientiRemoteModel) {
		_wkOrdiniClientiRemoteModel = wkOrdiniClientiRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _wkOrdiniClientiRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_wkOrdiniClientiRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			WKOrdiniClientiLocalServiceUtil.addWKOrdiniClienti(this);
		}
		else {
			WKOrdiniClientiLocalServiceUtil.updateWKOrdiniClienti(this);
		}
	}

	@Override
	public WKOrdiniClienti toEscapedModel() {
		return (WKOrdiniClienti)ProxyUtil.newProxyInstance(WKOrdiniClienti.class.getClassLoader(),
			new Class[] { WKOrdiniClienti.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		WKOrdiniClientiClp clone = new WKOrdiniClientiClp();

		clone.setAnno(getAnno());
		clone.setTipoOrdine(getTipoOrdine());
		clone.setNumeroOrdine(getNumeroOrdine());
		clone.setTipoDocumento(getTipoDocumento());
		clone.setIdTipoDocumento(getIdTipoDocumento());
		clone.setStatoOrdine(getStatoOrdine());
		clone.setTipoSoggetto(getTipoSoggetto());
		clone.setCodiceCliente(getCodiceCliente());
		clone.setCodiceEsenzione(getCodiceEsenzione());
		clone.setCodiceDivisa(getCodiceDivisa());
		clone.setValoreCambio(getValoreCambio());
		clone.setDataValoreCambio(getDataValoreCambio());
		clone.setCodicePianoPag(getCodicePianoPag());
		clone.setInizioCalcoloPag(getInizioCalcoloPag());
		clone.setPercentualeScontoMaggiorazione(getPercentualeScontoMaggiorazione());
		clone.setPercentualeScontoProntaCassa(getPercentualeScontoProntaCassa());
		clone.setDataDocumento(getDataDocumento());
		clone.setDataRegistrazione(getDataRegistrazione());
		clone.setCausaleEstrattoConto(getCausaleEstrattoConto());
		clone.setCodiceAgente(getCodiceAgente());
		clone.setCodiceGruppoAgenti(getCodiceGruppoAgenti());
		clone.setCodiceZona(getCodiceZona());
		clone.setCodiceDestinatario(getCodiceDestinatario());
		clone.setCodiceListino(getCodiceListino());
		clone.setNumeroDecPrezzo(getNumeroDecPrezzo());
		clone.setNote(getNote());
		clone.setInviatoEmail(getInviatoEmail());
		clone.setNomePDF(getNomePDF());
		clone.setRiferimentoOrdine(getRiferimentoOrdine());
		clone.setDataConferma(getDataConferma());
		clone.setConfermaStampata(getConfermaStampata());
		clone.setTotaleOrdine(getTotaleOrdine());
		clone.setLibStr1(getLibStr1());
		clone.setLibStr2(getLibStr2());
		clone.setLibStr3(getLibStr3());
		clone.setLibDbl1(getLibDbl1());
		clone.setLibDbl2(getLibDbl2());
		clone.setLibDbl3(getLibDbl3());
		clone.setLibDat1(getLibDat1());
		clone.setLibDat2(getLibDat2());
		clone.setLibDat3(getLibDat3());
		clone.setLibLng1(getLibLng1());
		clone.setLibLng2(getLibLng2());
		clone.setLibLng3(getLibLng3());

		return clone;
	}

	@Override
	public int compareTo(WKOrdiniClienti wkOrdiniClienti) {
		int value = 0;

		if (getAnno() < wkOrdiniClienti.getAnno()) {
			value = -1;
		}
		else if (getAnno() > wkOrdiniClienti.getAnno()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		if (getNumeroOrdine() < wkOrdiniClienti.getNumeroOrdine()) {
			value = -1;
		}
		else if (getNumeroOrdine() > wkOrdiniClienti.getNumeroOrdine()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof WKOrdiniClientiClp)) {
			return false;
		}

		WKOrdiniClientiClp wkOrdiniClienti = (WKOrdiniClientiClp)obj;

		WKOrdiniClientiPK primaryKey = wkOrdiniClienti.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(89);

		sb.append("{anno=");
		sb.append(getAnno());
		sb.append(", tipoOrdine=");
		sb.append(getTipoOrdine());
		sb.append(", numeroOrdine=");
		sb.append(getNumeroOrdine());
		sb.append(", tipoDocumento=");
		sb.append(getTipoDocumento());
		sb.append(", idTipoDocumento=");
		sb.append(getIdTipoDocumento());
		sb.append(", statoOrdine=");
		sb.append(getStatoOrdine());
		sb.append(", tipoSoggetto=");
		sb.append(getTipoSoggetto());
		sb.append(", codiceCliente=");
		sb.append(getCodiceCliente());
		sb.append(", codiceEsenzione=");
		sb.append(getCodiceEsenzione());
		sb.append(", codiceDivisa=");
		sb.append(getCodiceDivisa());
		sb.append(", valoreCambio=");
		sb.append(getValoreCambio());
		sb.append(", dataValoreCambio=");
		sb.append(getDataValoreCambio());
		sb.append(", codicePianoPag=");
		sb.append(getCodicePianoPag());
		sb.append(", inizioCalcoloPag=");
		sb.append(getInizioCalcoloPag());
		sb.append(", percentualeScontoMaggiorazione=");
		sb.append(getPercentualeScontoMaggiorazione());
		sb.append(", percentualeScontoProntaCassa=");
		sb.append(getPercentualeScontoProntaCassa());
		sb.append(", dataDocumento=");
		sb.append(getDataDocumento());
		sb.append(", dataRegistrazione=");
		sb.append(getDataRegistrazione());
		sb.append(", causaleEstrattoConto=");
		sb.append(getCausaleEstrattoConto());
		sb.append(", codiceAgente=");
		sb.append(getCodiceAgente());
		sb.append(", codiceGruppoAgenti=");
		sb.append(getCodiceGruppoAgenti());
		sb.append(", codiceZona=");
		sb.append(getCodiceZona());
		sb.append(", codiceDestinatario=");
		sb.append(getCodiceDestinatario());
		sb.append(", codiceListino=");
		sb.append(getCodiceListino());
		sb.append(", numeroDecPrezzo=");
		sb.append(getNumeroDecPrezzo());
		sb.append(", note=");
		sb.append(getNote());
		sb.append(", inviatoEmail=");
		sb.append(getInviatoEmail());
		sb.append(", nomePDF=");
		sb.append(getNomePDF());
		sb.append(", riferimentoOrdine=");
		sb.append(getRiferimentoOrdine());
		sb.append(", dataConferma=");
		sb.append(getDataConferma());
		sb.append(", confermaStampata=");
		sb.append(getConfermaStampata());
		sb.append(", totaleOrdine=");
		sb.append(getTotaleOrdine());
		sb.append(", libStr1=");
		sb.append(getLibStr1());
		sb.append(", libStr2=");
		sb.append(getLibStr2());
		sb.append(", libStr3=");
		sb.append(getLibStr3());
		sb.append(", libDbl1=");
		sb.append(getLibDbl1());
		sb.append(", libDbl2=");
		sb.append(getLibDbl2());
		sb.append(", libDbl3=");
		sb.append(getLibDbl3());
		sb.append(", libDat1=");
		sb.append(getLibDat1());
		sb.append(", libDat2=");
		sb.append(getLibDat2());
		sb.append(", libDat3=");
		sb.append(getLibDat3());
		sb.append(", libLng1=");
		sb.append(getLibLng1());
		sb.append(", libLng2=");
		sb.append(getLibLng2());
		sb.append(", libLng3=");
		sb.append(getLibLng3());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(136);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.WKOrdiniClienti");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>anno</column-name><column-value><![CDATA[");
		sb.append(getAnno());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoOrdine</column-name><column-value><![CDATA[");
		sb.append(getTipoOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroOrdine</column-name><column-value><![CDATA[");
		sb.append(getNumeroOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoDocumento</column-name><column-value><![CDATA[");
		sb.append(getTipoDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idTipoDocumento</column-name><column-value><![CDATA[");
		sb.append(getIdTipoDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statoOrdine</column-name><column-value><![CDATA[");
		sb.append(getStatoOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoSoggetto</column-name><column-value><![CDATA[");
		sb.append(getTipoSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCliente</column-name><column-value><![CDATA[");
		sb.append(getCodiceCliente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceEsenzione</column-name><column-value><![CDATA[");
		sb.append(getCodiceEsenzione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDivisa</column-name><column-value><![CDATA[");
		sb.append(getCodiceDivisa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>valoreCambio</column-name><column-value><![CDATA[");
		sb.append(getValoreCambio());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataValoreCambio</column-name><column-value><![CDATA[");
		sb.append(getDataValoreCambio());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codicePianoPag</column-name><column-value><![CDATA[");
		sb.append(getCodicePianoPag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>inizioCalcoloPag</column-name><column-value><![CDATA[");
		sb.append(getInizioCalcoloPag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>percentualeScontoMaggiorazione</column-name><column-value><![CDATA[");
		sb.append(getPercentualeScontoMaggiorazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>percentualeScontoProntaCassa</column-name><column-value><![CDATA[");
		sb.append(getPercentualeScontoProntaCassa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataDocumento</column-name><column-value><![CDATA[");
		sb.append(getDataDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataRegistrazione</column-name><column-value><![CDATA[");
		sb.append(getDataRegistrazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>causaleEstrattoConto</column-name><column-value><![CDATA[");
		sb.append(getCausaleEstrattoConto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceAgente</column-name><column-value><![CDATA[");
		sb.append(getCodiceAgente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceGruppoAgenti</column-name><column-value><![CDATA[");
		sb.append(getCodiceGruppoAgenti());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceZona</column-name><column-value><![CDATA[");
		sb.append(getCodiceZona());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDestinatario</column-name><column-value><![CDATA[");
		sb.append(getCodiceDestinatario());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceListino</column-name><column-value><![CDATA[");
		sb.append(getCodiceListino());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroDecPrezzo</column-name><column-value><![CDATA[");
		sb.append(getNumeroDecPrezzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>note</column-name><column-value><![CDATA[");
		sb.append(getNote());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>inviatoEmail</column-name><column-value><![CDATA[");
		sb.append(getInviatoEmail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nomePDF</column-name><column-value><![CDATA[");
		sb.append(getNomePDF());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>riferimentoOrdine</column-name><column-value><![CDATA[");
		sb.append(getRiferimentoOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataConferma</column-name><column-value><![CDATA[");
		sb.append(getDataConferma());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>confermaStampata</column-name><column-value><![CDATA[");
		sb.append(getConfermaStampata());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>totaleOrdine</column-name><column-value><![CDATA[");
		sb.append(getTotaleOrdine());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr1</column-name><column-value><![CDATA[");
		sb.append(getLibStr1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr2</column-name><column-value><![CDATA[");
		sb.append(getLibStr2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr3</column-name><column-value><![CDATA[");
		sb.append(getLibStr3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl1</column-name><column-value><![CDATA[");
		sb.append(getLibDbl1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl2</column-name><column-value><![CDATA[");
		sb.append(getLibDbl2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl3</column-name><column-value><![CDATA[");
		sb.append(getLibDbl3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat1</column-name><column-value><![CDATA[");
		sb.append(getLibDat1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat2</column-name><column-value><![CDATA[");
		sb.append(getLibDat2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat3</column-name><column-value><![CDATA[");
		sb.append(getLibDat3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng1</column-name><column-value><![CDATA[");
		sb.append(getLibLng1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng2</column-name><column-value><![CDATA[");
		sb.append(getLibLng2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng3</column-name><column-value><![CDATA[");
		sb.append(getLibLng3());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _anno;
	private int _tipoOrdine;
	private int _numeroOrdine;
	private String _tipoDocumento;
	private int _idTipoDocumento;
	private boolean _statoOrdine;
	private boolean _tipoSoggetto;
	private String _codiceCliente;
	private String _codiceEsenzione;
	private String _codiceDivisa;
	private double _valoreCambio;
	private Date _dataValoreCambio;
	private String _codicePianoPag;
	private Date _inizioCalcoloPag;
	private double _percentualeScontoMaggiorazione;
	private double _percentualeScontoProntaCassa;
	private Date _dataDocumento;
	private Date _dataRegistrazione;
	private String _causaleEstrattoConto;
	private String _codiceAgente;
	private String _codiceGruppoAgenti;
	private String _codiceZona;
	private String _codiceDestinatario;
	private String _codiceListino;
	private int _numeroDecPrezzo;
	private String _note;
	private boolean _inviatoEmail;
	private String _nomePDF;
	private String _riferimentoOrdine;
	private Date _dataConferma;
	private boolean _confermaStampata;
	private double _totaleOrdine;
	private String _libStr1;
	private String _libStr2;
	private String _libStr3;
	private double _libDbl1;
	private double _libDbl2;
	private double _libDbl3;
	private Date _libDat1;
	private Date _libDat2;
	private Date _libDat3;
	private long _libLng1;
	private long _libLng2;
	private long _libLng3;
	private BaseModel<?> _wkOrdiniClientiRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}