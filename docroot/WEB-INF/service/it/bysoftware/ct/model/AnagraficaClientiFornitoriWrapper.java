/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AnagraficaClientiFornitori}.
 * </p>
 *
 * @author Mario Torrisi
 * @see AnagraficaClientiFornitori
 * @generated
 */
public class AnagraficaClientiFornitoriWrapper
	implements AnagraficaClientiFornitori,
		ModelWrapper<AnagraficaClientiFornitori> {
	public AnagraficaClientiFornitoriWrapper(
		AnagraficaClientiFornitori anagraficaClientiFornitori) {
		_anagraficaClientiFornitori = anagraficaClientiFornitori;
	}

	@Override
	public Class<?> getModelClass() {
		return AnagraficaClientiFornitori.class;
	}

	@Override
	public String getModelClassName() {
		return AnagraficaClientiFornitori.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codiceSoggetto", getCodiceSoggetto());
		attributes.put("ragioneSociale", getRagioneSociale());
		attributes.put("ragioneSocialeAggiuntiva", getRagioneSocialeAggiuntiva());
		attributes.put("indirizzo", getIndirizzo());
		attributes.put("comune", getComune());
		attributes.put("CAP", getCAP());
		attributes.put("siglaProvincia", getSiglaProvincia());
		attributes.put("siglaStato", getSiglaStato());
		attributes.put("partitaIVA", getPartitaIVA());
		attributes.put("codiceFiscale", getCodiceFiscale());
		attributes.put("note", getNote());
		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceMnemonico", getCodiceMnemonico());
		attributes.put("tipoSollecito", getTipoSollecito());
		attributes.put("attivoEC", getAttivoEC());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codiceSoggetto = (String)attributes.get("codiceSoggetto");

		if (codiceSoggetto != null) {
			setCodiceSoggetto(codiceSoggetto);
		}

		String ragioneSociale = (String)attributes.get("ragioneSociale");

		if (ragioneSociale != null) {
			setRagioneSociale(ragioneSociale);
		}

		String ragioneSocialeAggiuntiva = (String)attributes.get(
				"ragioneSocialeAggiuntiva");

		if (ragioneSocialeAggiuntiva != null) {
			setRagioneSocialeAggiuntiva(ragioneSocialeAggiuntiva);
		}

		String indirizzo = (String)attributes.get("indirizzo");

		if (indirizzo != null) {
			setIndirizzo(indirizzo);
		}

		String comune = (String)attributes.get("comune");

		if (comune != null) {
			setComune(comune);
		}

		String CAP = (String)attributes.get("CAP");

		if (CAP != null) {
			setCAP(CAP);
		}

		String siglaProvincia = (String)attributes.get("siglaProvincia");

		if (siglaProvincia != null) {
			setSiglaProvincia(siglaProvincia);
		}

		String siglaStato = (String)attributes.get("siglaStato");

		if (siglaStato != null) {
			setSiglaStato(siglaStato);
		}

		String partitaIVA = (String)attributes.get("partitaIVA");

		if (partitaIVA != null) {
			setPartitaIVA(partitaIVA);
		}

		String codiceFiscale = (String)attributes.get("codiceFiscale");

		if (codiceFiscale != null) {
			setCodiceFiscale(codiceFiscale);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}

		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceMnemonico = (String)attributes.get("codiceMnemonico");

		if (codiceMnemonico != null) {
			setCodiceMnemonico(codiceMnemonico);
		}

		Integer tipoSollecito = (Integer)attributes.get("tipoSollecito");

		if (tipoSollecito != null) {
			setTipoSollecito(tipoSollecito);
		}

		Boolean attivoEC = (Boolean)attributes.get("attivoEC");

		if (attivoEC != null) {
			setAttivoEC(attivoEC);
		}
	}

	/**
	* Returns the primary key of this anagrafica clienti fornitori.
	*
	* @return the primary key of this anagrafica clienti fornitori
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _anagraficaClientiFornitori.getPrimaryKey();
	}

	/**
	* Sets the primary key of this anagrafica clienti fornitori.
	*
	* @param primaryKey the primary key of this anagrafica clienti fornitori
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_anagraficaClientiFornitori.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the codice soggetto of this anagrafica clienti fornitori.
	*
	* @return the codice soggetto of this anagrafica clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceSoggetto() {
		return _anagraficaClientiFornitori.getCodiceSoggetto();
	}

	/**
	* Sets the codice soggetto of this anagrafica clienti fornitori.
	*
	* @param codiceSoggetto the codice soggetto of this anagrafica clienti fornitori
	*/
	@Override
	public void setCodiceSoggetto(java.lang.String codiceSoggetto) {
		_anagraficaClientiFornitori.setCodiceSoggetto(codiceSoggetto);
	}

	/**
	* Returns the ragione sociale of this anagrafica clienti fornitori.
	*
	* @return the ragione sociale of this anagrafica clienti fornitori
	*/
	@Override
	public java.lang.String getRagioneSociale() {
		return _anagraficaClientiFornitori.getRagioneSociale();
	}

	/**
	* Sets the ragione sociale of this anagrafica clienti fornitori.
	*
	* @param ragioneSociale the ragione sociale of this anagrafica clienti fornitori
	*/
	@Override
	public void setRagioneSociale(java.lang.String ragioneSociale) {
		_anagraficaClientiFornitori.setRagioneSociale(ragioneSociale);
	}

	/**
	* Returns the ragione sociale aggiuntiva of this anagrafica clienti fornitori.
	*
	* @return the ragione sociale aggiuntiva of this anagrafica clienti fornitori
	*/
	@Override
	public java.lang.String getRagioneSocialeAggiuntiva() {
		return _anagraficaClientiFornitori.getRagioneSocialeAggiuntiva();
	}

	/**
	* Sets the ragione sociale aggiuntiva of this anagrafica clienti fornitori.
	*
	* @param ragioneSocialeAggiuntiva the ragione sociale aggiuntiva of this anagrafica clienti fornitori
	*/
	@Override
	public void setRagioneSocialeAggiuntiva(
		java.lang.String ragioneSocialeAggiuntiva) {
		_anagraficaClientiFornitori.setRagioneSocialeAggiuntiva(ragioneSocialeAggiuntiva);
	}

	/**
	* Returns the indirizzo of this anagrafica clienti fornitori.
	*
	* @return the indirizzo of this anagrafica clienti fornitori
	*/
	@Override
	public java.lang.String getIndirizzo() {
		return _anagraficaClientiFornitori.getIndirizzo();
	}

	/**
	* Sets the indirizzo of this anagrafica clienti fornitori.
	*
	* @param indirizzo the indirizzo of this anagrafica clienti fornitori
	*/
	@Override
	public void setIndirizzo(java.lang.String indirizzo) {
		_anagraficaClientiFornitori.setIndirizzo(indirizzo);
	}

	/**
	* Returns the comune of this anagrafica clienti fornitori.
	*
	* @return the comune of this anagrafica clienti fornitori
	*/
	@Override
	public java.lang.String getComune() {
		return _anagraficaClientiFornitori.getComune();
	}

	/**
	* Sets the comune of this anagrafica clienti fornitori.
	*
	* @param comune the comune of this anagrafica clienti fornitori
	*/
	@Override
	public void setComune(java.lang.String comune) {
		_anagraficaClientiFornitori.setComune(comune);
	}

	/**
	* Returns the c a p of this anagrafica clienti fornitori.
	*
	* @return the c a p of this anagrafica clienti fornitori
	*/
	@Override
	public java.lang.String getCAP() {
		return _anagraficaClientiFornitori.getCAP();
	}

	/**
	* Sets the c a p of this anagrafica clienti fornitori.
	*
	* @param CAP the c a p of this anagrafica clienti fornitori
	*/
	@Override
	public void setCAP(java.lang.String CAP) {
		_anagraficaClientiFornitori.setCAP(CAP);
	}

	/**
	* Returns the sigla provincia of this anagrafica clienti fornitori.
	*
	* @return the sigla provincia of this anagrafica clienti fornitori
	*/
	@Override
	public java.lang.String getSiglaProvincia() {
		return _anagraficaClientiFornitori.getSiglaProvincia();
	}

	/**
	* Sets the sigla provincia of this anagrafica clienti fornitori.
	*
	* @param siglaProvincia the sigla provincia of this anagrafica clienti fornitori
	*/
	@Override
	public void setSiglaProvincia(java.lang.String siglaProvincia) {
		_anagraficaClientiFornitori.setSiglaProvincia(siglaProvincia);
	}

	/**
	* Returns the sigla stato of this anagrafica clienti fornitori.
	*
	* @return the sigla stato of this anagrafica clienti fornitori
	*/
	@Override
	public java.lang.String getSiglaStato() {
		return _anagraficaClientiFornitori.getSiglaStato();
	}

	/**
	* Sets the sigla stato of this anagrafica clienti fornitori.
	*
	* @param siglaStato the sigla stato of this anagrafica clienti fornitori
	*/
	@Override
	public void setSiglaStato(java.lang.String siglaStato) {
		_anagraficaClientiFornitori.setSiglaStato(siglaStato);
	}

	/**
	* Returns the partita i v a of this anagrafica clienti fornitori.
	*
	* @return the partita i v a of this anagrafica clienti fornitori
	*/
	@Override
	public java.lang.String getPartitaIVA() {
		return _anagraficaClientiFornitori.getPartitaIVA();
	}

	/**
	* Sets the partita i v a of this anagrafica clienti fornitori.
	*
	* @param partitaIVA the partita i v a of this anagrafica clienti fornitori
	*/
	@Override
	public void setPartitaIVA(java.lang.String partitaIVA) {
		_anagraficaClientiFornitori.setPartitaIVA(partitaIVA);
	}

	/**
	* Returns the codice fiscale of this anagrafica clienti fornitori.
	*
	* @return the codice fiscale of this anagrafica clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceFiscale() {
		return _anagraficaClientiFornitori.getCodiceFiscale();
	}

	/**
	* Sets the codice fiscale of this anagrafica clienti fornitori.
	*
	* @param codiceFiscale the codice fiscale of this anagrafica clienti fornitori
	*/
	@Override
	public void setCodiceFiscale(java.lang.String codiceFiscale) {
		_anagraficaClientiFornitori.setCodiceFiscale(codiceFiscale);
	}

	/**
	* Returns the note of this anagrafica clienti fornitori.
	*
	* @return the note of this anagrafica clienti fornitori
	*/
	@Override
	public java.lang.String getNote() {
		return _anagraficaClientiFornitori.getNote();
	}

	/**
	* Sets the note of this anagrafica clienti fornitori.
	*
	* @param note the note of this anagrafica clienti fornitori
	*/
	@Override
	public void setNote(java.lang.String note) {
		_anagraficaClientiFornitori.setNote(note);
	}

	/**
	* Returns the tipo soggetto of this anagrafica clienti fornitori.
	*
	* @return the tipo soggetto of this anagrafica clienti fornitori
	*/
	@Override
	public boolean getTipoSoggetto() {
		return _anagraficaClientiFornitori.getTipoSoggetto();
	}

	/**
	* Returns <code>true</code> if this anagrafica clienti fornitori is tipo soggetto.
	*
	* @return <code>true</code> if this anagrafica clienti fornitori is tipo soggetto; <code>false</code> otherwise
	*/
	@Override
	public boolean isTipoSoggetto() {
		return _anagraficaClientiFornitori.isTipoSoggetto();
	}

	/**
	* Sets whether this anagrafica clienti fornitori is tipo soggetto.
	*
	* @param tipoSoggetto the tipo soggetto of this anagrafica clienti fornitori
	*/
	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_anagraficaClientiFornitori.setTipoSoggetto(tipoSoggetto);
	}

	/**
	* Returns the codice mnemonico of this anagrafica clienti fornitori.
	*
	* @return the codice mnemonico of this anagrafica clienti fornitori
	*/
	@Override
	public java.lang.String getCodiceMnemonico() {
		return _anagraficaClientiFornitori.getCodiceMnemonico();
	}

	/**
	* Sets the codice mnemonico of this anagrafica clienti fornitori.
	*
	* @param codiceMnemonico the codice mnemonico of this anagrafica clienti fornitori
	*/
	@Override
	public void setCodiceMnemonico(java.lang.String codiceMnemonico) {
		_anagraficaClientiFornitori.setCodiceMnemonico(codiceMnemonico);
	}

	/**
	* Returns the tipo sollecito of this anagrafica clienti fornitori.
	*
	* @return the tipo sollecito of this anagrafica clienti fornitori
	*/
	@Override
	public int getTipoSollecito() {
		return _anagraficaClientiFornitori.getTipoSollecito();
	}

	/**
	* Sets the tipo sollecito of this anagrafica clienti fornitori.
	*
	* @param tipoSollecito the tipo sollecito of this anagrafica clienti fornitori
	*/
	@Override
	public void setTipoSollecito(int tipoSollecito) {
		_anagraficaClientiFornitori.setTipoSollecito(tipoSollecito);
	}

	/**
	* Returns the attivo e c of this anagrafica clienti fornitori.
	*
	* @return the attivo e c of this anagrafica clienti fornitori
	*/
	@Override
	public boolean getAttivoEC() {
		return _anagraficaClientiFornitori.getAttivoEC();
	}

	/**
	* Returns <code>true</code> if this anagrafica clienti fornitori is attivo e c.
	*
	* @return <code>true</code> if this anagrafica clienti fornitori is attivo e c; <code>false</code> otherwise
	*/
	@Override
	public boolean isAttivoEC() {
		return _anagraficaClientiFornitori.isAttivoEC();
	}

	/**
	* Sets whether this anagrafica clienti fornitori is attivo e c.
	*
	* @param attivoEC the attivo e c of this anagrafica clienti fornitori
	*/
	@Override
	public void setAttivoEC(boolean attivoEC) {
		_anagraficaClientiFornitori.setAttivoEC(attivoEC);
	}

	@Override
	public boolean isNew() {
		return _anagraficaClientiFornitori.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_anagraficaClientiFornitori.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _anagraficaClientiFornitori.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_anagraficaClientiFornitori.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _anagraficaClientiFornitori.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _anagraficaClientiFornitori.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_anagraficaClientiFornitori.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _anagraficaClientiFornitori.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_anagraficaClientiFornitori.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_anagraficaClientiFornitori.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_anagraficaClientiFornitori.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AnagraficaClientiFornitoriWrapper((AnagraficaClientiFornitori)_anagraficaClientiFornitori.clone());
	}

	@Override
	public int compareTo(
		it.bysoftware.ct.model.AnagraficaClientiFornitori anagraficaClientiFornitori) {
		return _anagraficaClientiFornitori.compareTo(anagraficaClientiFornitori);
	}

	@Override
	public int hashCode() {
		return _anagraficaClientiFornitori.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.AnagraficaClientiFornitori> toCacheModel() {
		return _anagraficaClientiFornitori.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.AnagraficaClientiFornitori toEscapedModel() {
		return new AnagraficaClientiFornitoriWrapper(_anagraficaClientiFornitori.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.AnagraficaClientiFornitori toUnescapedModel() {
		return new AnagraficaClientiFornitoriWrapper(_anagraficaClientiFornitori.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _anagraficaClientiFornitori.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _anagraficaClientiFornitori.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_anagraficaClientiFornitori.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AnagraficaClientiFornitoriWrapper)) {
			return false;
		}

		AnagraficaClientiFornitoriWrapper anagraficaClientiFornitoriWrapper = (AnagraficaClientiFornitoriWrapper)obj;

		if (Validator.equals(_anagraficaClientiFornitori,
					anagraficaClientiFornitoriWrapper._anagraficaClientiFornitori)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public AnagraficaClientiFornitori getWrappedAnagraficaClientiFornitori() {
		return _anagraficaClientiFornitori;
	}

	@Override
	public AnagraficaClientiFornitori getWrappedModel() {
		return _anagraficaClientiFornitori;
	}

	@Override
	public void resetOriginalValues() {
		_anagraficaClientiFornitori.resetOriginalValues();
	}

	private AnagraficaClientiFornitori _anagraficaClientiFornitori;
}