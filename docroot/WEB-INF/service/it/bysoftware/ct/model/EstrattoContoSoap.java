/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.EstrattoContoPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.EstrattoContoServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.EstrattoContoServiceSoap
 * @generated
 */
public class EstrattoContoSoap implements Serializable {
	public static EstrattoContoSoap toSoapModel(EstrattoConto model) {
		EstrattoContoSoap soapModel = new EstrattoContoSoap();

		soapModel.setTipoSoggetto(model.getTipoSoggetto());
		soapModel.setCodiceCliente(model.getCodiceCliente());
		soapModel.setEsercizioRegistrazione(model.getEsercizioRegistrazione());
		soapModel.setNumeroPartita(model.getNumeroPartita());
		soapModel.setNumeroScadenza(model.getNumeroScadenza());
		soapModel.setDataScadenza(model.getDataScadenza());
		soapModel.setCodiceTipoPagam(model.getCodiceTipoPagam());
		soapModel.setStato(model.getStato());
		soapModel.setCodiceBanca(model.getCodiceBanca());
		soapModel.setCodiceContoCorrente(model.getCodiceContoCorrente());
		soapModel.setCodiceAgente(model.getCodiceAgente());
		soapModel.setTipoCausale(model.getTipoCausale());
		soapModel.setEsercizioDocumento(model.getEsercizioDocumento());
		soapModel.setProtocolloDocumento(model.getProtocolloDocumento());
		soapModel.setCodiceAttivita(model.getCodiceAttivita());
		soapModel.setCodiceCentro(model.getCodiceCentro());
		soapModel.setDataRegistrazione(model.getDataRegistrazione());
		soapModel.setDataOperazione(model.getDataOperazione());
		soapModel.setDataAggiornamento(model.getDataAggiornamento());
		soapModel.setDataChiusura(model.getDataChiusura());
		soapModel.setCodiceCausale(model.getCodiceCausale());
		soapModel.setDataDocumento(model.getDataDocumento());
		soapModel.setNumeroDocumento(model.getNumeroDocumento());
		soapModel.setCodiceTipoPagamOrig(model.getCodiceTipoPagamOrig());
		soapModel.setCodiceTipoPagamScad(model.getCodiceTipoPagamScad());
		soapModel.setCausaleEstrattoContoOrig(model.getCausaleEstrattoContoOrig());
		soapModel.setCodiceDivisaOrig(model.getCodiceDivisaOrig());
		soapModel.setImportoTotaleInt(model.getImportoTotaleInt());
		soapModel.setImportoPagatoInt(model.getImportoPagatoInt());
		soapModel.setImportoApertoInt(model.getImportoApertoInt());
		soapModel.setImportoEspostoInt(model.getImportoEspostoInt());
		soapModel.setImportoScadutoInt(model.getImportoScadutoInt());
		soapModel.setImportoInsolutoInt(model.getImportoInsolutoInt());
		soapModel.setImportoInContenziosoInt(model.getImportoInContenziosoInt());
		soapModel.setImportoTotale(model.getImportoTotale());
		soapModel.setImportoPagato(model.getImportoPagato());
		soapModel.setImportoAperto(model.getImportoAperto());
		soapModel.setImportoEsposto(model.getImportoEsposto());
		soapModel.setImportoScaduto(model.getImportoScaduto());
		soapModel.setImportoInsoluto(model.getImportoInsoluto());
		soapModel.setImportoInContenzioso(model.getImportoInContenzioso());
		soapModel.setNumeroLettereSollecito(model.getNumeroLettereSollecito());
		soapModel.setCodiceDivisaInterna(model.getCodiceDivisaInterna());
		soapModel.setCodiceEsercizioScadenzaConversione(model.getCodiceEsercizioScadenzaConversione());
		soapModel.setNumeroPartitaScadenzaCoversione(model.getNumeroPartitaScadenzaCoversione());
		soapModel.setNumeroScadenzaCoversione(model.getNumeroScadenzaCoversione());
		soapModel.setTipoCoversione(model.getTipoCoversione());
		soapModel.setCodiceEsercizioScritturaConversione(model.getCodiceEsercizioScritturaConversione());
		soapModel.setNumeroRegistrazioneScritturaConversione(model.getNumeroRegistrazioneScritturaConversione());
		soapModel.setDataRegistrazioneScritturaConversione(model.getDataRegistrazioneScritturaConversione());
		soapModel.setCodiceDivisaIntScadenzaConversione(model.getCodiceDivisaIntScadenzaConversione());
		soapModel.setCodiceDivisaScadenzaConversione(model.getCodiceDivisaScadenzaConversione());
		soapModel.setCambioScadenzaConversione(model.getCambioScadenzaConversione());

		return soapModel;
	}

	public static EstrattoContoSoap[] toSoapModels(EstrattoConto[] models) {
		EstrattoContoSoap[] soapModels = new EstrattoContoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EstrattoContoSoap[][] toSoapModels(EstrattoConto[][] models) {
		EstrattoContoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EstrattoContoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new EstrattoContoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EstrattoContoSoap[] toSoapModels(List<EstrattoConto> models) {
		List<EstrattoContoSoap> soapModels = new ArrayList<EstrattoContoSoap>(models.size());

		for (EstrattoConto model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EstrattoContoSoap[soapModels.size()]);
	}

	public EstrattoContoSoap() {
	}

	public EstrattoContoPK getPrimaryKey() {
		return new EstrattoContoPK(_tipoSoggetto, _codiceCliente,
			_esercizioRegistrazione, _numeroPartita, _numeroScadenza);
	}

	public void setPrimaryKey(EstrattoContoPK pk) {
		setTipoSoggetto(pk.tipoSoggetto);
		setCodiceCliente(pk.codiceCliente);
		setEsercizioRegistrazione(pk.esercizioRegistrazione);
		setNumeroPartita(pk.numeroPartita);
		setNumeroScadenza(pk.numeroScadenza);
	}

	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;
	}

	public String getCodiceCliente() {
		return _codiceCliente;
	}

	public void setCodiceCliente(String codiceCliente) {
		_codiceCliente = codiceCliente;
	}

	public int getEsercizioRegistrazione() {
		return _esercizioRegistrazione;
	}

	public void setEsercizioRegistrazione(int esercizioRegistrazione) {
		_esercizioRegistrazione = esercizioRegistrazione;
	}

	public int getNumeroPartita() {
		return _numeroPartita;
	}

	public void setNumeroPartita(int numeroPartita) {
		_numeroPartita = numeroPartita;
	}

	public int getNumeroScadenza() {
		return _numeroScadenza;
	}

	public void setNumeroScadenza(int numeroScadenza) {
		_numeroScadenza = numeroScadenza;
	}

	public Date getDataScadenza() {
		return _dataScadenza;
	}

	public void setDataScadenza(Date dataScadenza) {
		_dataScadenza = dataScadenza;
	}

	public int getCodiceTipoPagam() {
		return _codiceTipoPagam;
	}

	public void setCodiceTipoPagam(int codiceTipoPagam) {
		_codiceTipoPagam = codiceTipoPagam;
	}

	public int getStato() {
		return _stato;
	}

	public void setStato(int stato) {
		_stato = stato;
	}

	public String getCodiceBanca() {
		return _codiceBanca;
	}

	public void setCodiceBanca(String codiceBanca) {
		_codiceBanca = codiceBanca;
	}

	public String getCodiceContoCorrente() {
		return _codiceContoCorrente;
	}

	public void setCodiceContoCorrente(String codiceContoCorrente) {
		_codiceContoCorrente = codiceContoCorrente;
	}

	public String getCodiceAgente() {
		return _codiceAgente;
	}

	public void setCodiceAgente(String codiceAgente) {
		_codiceAgente = codiceAgente;
	}

	public String getTipoCausale() {
		return _tipoCausale;
	}

	public void setTipoCausale(String tipoCausale) {
		_tipoCausale = tipoCausale;
	}

	public int getEsercizioDocumento() {
		return _esercizioDocumento;
	}

	public void setEsercizioDocumento(int esercizioDocumento) {
		_esercizioDocumento = esercizioDocumento;
	}

	public int getProtocolloDocumento() {
		return _protocolloDocumento;
	}

	public void setProtocolloDocumento(int protocolloDocumento) {
		_protocolloDocumento = protocolloDocumento;
	}

	public String getCodiceAttivita() {
		return _codiceAttivita;
	}

	public void setCodiceAttivita(String codiceAttivita) {
		_codiceAttivita = codiceAttivita;
	}

	public String getCodiceCentro() {
		return _codiceCentro;
	}

	public void setCodiceCentro(String codiceCentro) {
		_codiceCentro = codiceCentro;
	}

	public Date getDataRegistrazione() {
		return _dataRegistrazione;
	}

	public void setDataRegistrazione(Date dataRegistrazione) {
		_dataRegistrazione = dataRegistrazione;
	}

	public Date getDataOperazione() {
		return _dataOperazione;
	}

	public void setDataOperazione(Date dataOperazione) {
		_dataOperazione = dataOperazione;
	}

	public Date getDataAggiornamento() {
		return _dataAggiornamento;
	}

	public void setDataAggiornamento(Date dataAggiornamento) {
		_dataAggiornamento = dataAggiornamento;
	}

	public Date getDataChiusura() {
		return _dataChiusura;
	}

	public void setDataChiusura(Date dataChiusura) {
		_dataChiusura = dataChiusura;
	}

	public String getCodiceCausale() {
		return _codiceCausale;
	}

	public void setCodiceCausale(String codiceCausale) {
		_codiceCausale = codiceCausale;
	}

	public Date getDataDocumento() {
		return _dataDocumento;
	}

	public void setDataDocumento(Date dataDocumento) {
		_dataDocumento = dataDocumento;
	}

	public int getNumeroDocumento() {
		return _numeroDocumento;
	}

	public void setNumeroDocumento(int numeroDocumento) {
		_numeroDocumento = numeroDocumento;
	}

	public String getCodiceTipoPagamOrig() {
		return _codiceTipoPagamOrig;
	}

	public void setCodiceTipoPagamOrig(String codiceTipoPagamOrig) {
		_codiceTipoPagamOrig = codiceTipoPagamOrig;
	}

	public String getCodiceTipoPagamScad() {
		return _codiceTipoPagamScad;
	}

	public void setCodiceTipoPagamScad(String codiceTipoPagamScad) {
		_codiceTipoPagamScad = codiceTipoPagamScad;
	}

	public String getCausaleEstrattoContoOrig() {
		return _causaleEstrattoContoOrig;
	}

	public void setCausaleEstrattoContoOrig(String causaleEstrattoContoOrig) {
		_causaleEstrattoContoOrig = causaleEstrattoContoOrig;
	}

	public String getCodiceDivisaOrig() {
		return _codiceDivisaOrig;
	}

	public void setCodiceDivisaOrig(String codiceDivisaOrig) {
		_codiceDivisaOrig = codiceDivisaOrig;
	}

	public double getImportoTotaleInt() {
		return _importoTotaleInt;
	}

	public void setImportoTotaleInt(double importoTotaleInt) {
		_importoTotaleInt = importoTotaleInt;
	}

	public double getImportoPagatoInt() {
		return _importoPagatoInt;
	}

	public void setImportoPagatoInt(double importoPagatoInt) {
		_importoPagatoInt = importoPagatoInt;
	}

	public double getImportoApertoInt() {
		return _importoApertoInt;
	}

	public void setImportoApertoInt(double importoApertoInt) {
		_importoApertoInt = importoApertoInt;
	}

	public double getImportoEspostoInt() {
		return _importoEspostoInt;
	}

	public void setImportoEspostoInt(double importoEspostoInt) {
		_importoEspostoInt = importoEspostoInt;
	}

	public double getImportoScadutoInt() {
		return _importoScadutoInt;
	}

	public void setImportoScadutoInt(double importoScadutoInt) {
		_importoScadutoInt = importoScadutoInt;
	}

	public double getImportoInsolutoInt() {
		return _importoInsolutoInt;
	}

	public void setImportoInsolutoInt(double importoInsolutoInt) {
		_importoInsolutoInt = importoInsolutoInt;
	}

	public double getImportoInContenziosoInt() {
		return _importoInContenziosoInt;
	}

	public void setImportoInContenziosoInt(double importoInContenziosoInt) {
		_importoInContenziosoInt = importoInContenziosoInt;
	}

	public double getImportoTotale() {
		return _importoTotale;
	}

	public void setImportoTotale(double importoTotale) {
		_importoTotale = importoTotale;
	}

	public double getImportoPagato() {
		return _importoPagato;
	}

	public void setImportoPagato(double importoPagato) {
		_importoPagato = importoPagato;
	}

	public double getImportoAperto() {
		return _importoAperto;
	}

	public void setImportoAperto(double importoAperto) {
		_importoAperto = importoAperto;
	}

	public double getImportoEsposto() {
		return _importoEsposto;
	}

	public void setImportoEsposto(double importoEsposto) {
		_importoEsposto = importoEsposto;
	}

	public double getImportoScaduto() {
		return _importoScaduto;
	}

	public void setImportoScaduto(double importoScaduto) {
		_importoScaduto = importoScaduto;
	}

	public double getImportoInsoluto() {
		return _importoInsoluto;
	}

	public void setImportoInsoluto(double importoInsoluto) {
		_importoInsoluto = importoInsoluto;
	}

	public double getImportoInContenzioso() {
		return _importoInContenzioso;
	}

	public void setImportoInContenzioso(double importoInContenzioso) {
		_importoInContenzioso = importoInContenzioso;
	}

	public int getNumeroLettereSollecito() {
		return _numeroLettereSollecito;
	}

	public void setNumeroLettereSollecito(int numeroLettereSollecito) {
		_numeroLettereSollecito = numeroLettereSollecito;
	}

	public String getCodiceDivisaInterna() {
		return _codiceDivisaInterna;
	}

	public void setCodiceDivisaInterna(String codiceDivisaInterna) {
		_codiceDivisaInterna = codiceDivisaInterna;
	}

	public int getCodiceEsercizioScadenzaConversione() {
		return _codiceEsercizioScadenzaConversione;
	}

	public void setCodiceEsercizioScadenzaConversione(
		int codiceEsercizioScadenzaConversione) {
		_codiceEsercizioScadenzaConversione = codiceEsercizioScadenzaConversione;
	}

	public int getNumeroPartitaScadenzaCoversione() {
		return _numeroPartitaScadenzaCoversione;
	}

	public void setNumeroPartitaScadenzaCoversione(
		int numeroPartitaScadenzaCoversione) {
		_numeroPartitaScadenzaCoversione = numeroPartitaScadenzaCoversione;
	}

	public int getNumeroScadenzaCoversione() {
		return _numeroScadenzaCoversione;
	}

	public void setNumeroScadenzaCoversione(int numeroScadenzaCoversione) {
		_numeroScadenzaCoversione = numeroScadenzaCoversione;
	}

	public int getTipoCoversione() {
		return _tipoCoversione;
	}

	public void setTipoCoversione(int tipoCoversione) {
		_tipoCoversione = tipoCoversione;
	}

	public int getCodiceEsercizioScritturaConversione() {
		return _codiceEsercizioScritturaConversione;
	}

	public void setCodiceEsercizioScritturaConversione(
		int codiceEsercizioScritturaConversione) {
		_codiceEsercizioScritturaConversione = codiceEsercizioScritturaConversione;
	}

	public int getNumeroRegistrazioneScritturaConversione() {
		return _numeroRegistrazioneScritturaConversione;
	}

	public void setNumeroRegistrazioneScritturaConversione(
		int numeroRegistrazioneScritturaConversione) {
		_numeroRegistrazioneScritturaConversione = numeroRegistrazioneScritturaConversione;
	}

	public Date getDataRegistrazioneScritturaConversione() {
		return _dataRegistrazioneScritturaConversione;
	}

	public void setDataRegistrazioneScritturaConversione(
		Date dataRegistrazioneScritturaConversione) {
		_dataRegistrazioneScritturaConversione = dataRegistrazioneScritturaConversione;
	}

	public String getCodiceDivisaIntScadenzaConversione() {
		return _codiceDivisaIntScadenzaConversione;
	}

	public void setCodiceDivisaIntScadenzaConversione(
		String codiceDivisaIntScadenzaConversione) {
		_codiceDivisaIntScadenzaConversione = codiceDivisaIntScadenzaConversione;
	}

	public String getCodiceDivisaScadenzaConversione() {
		return _codiceDivisaScadenzaConversione;
	}

	public void setCodiceDivisaScadenzaConversione(
		String codiceDivisaScadenzaConversione) {
		_codiceDivisaScadenzaConversione = codiceDivisaScadenzaConversione;
	}

	public double getCambioScadenzaConversione() {
		return _cambioScadenzaConversione;
	}

	public void setCambioScadenzaConversione(double cambioScadenzaConversione) {
		_cambioScadenzaConversione = cambioScadenzaConversione;
	}

	private boolean _tipoSoggetto;
	private String _codiceCliente;
	private int _esercizioRegistrazione;
	private int _numeroPartita;
	private int _numeroScadenza;
	private Date _dataScadenza;
	private int _codiceTipoPagam;
	private int _stato;
	private String _codiceBanca;
	private String _codiceContoCorrente;
	private String _codiceAgente;
	private String _tipoCausale;
	private int _esercizioDocumento;
	private int _protocolloDocumento;
	private String _codiceAttivita;
	private String _codiceCentro;
	private Date _dataRegistrazione;
	private Date _dataOperazione;
	private Date _dataAggiornamento;
	private Date _dataChiusura;
	private String _codiceCausale;
	private Date _dataDocumento;
	private int _numeroDocumento;
	private String _codiceTipoPagamOrig;
	private String _codiceTipoPagamScad;
	private String _causaleEstrattoContoOrig;
	private String _codiceDivisaOrig;
	private double _importoTotaleInt;
	private double _importoPagatoInt;
	private double _importoApertoInt;
	private double _importoEspostoInt;
	private double _importoScadutoInt;
	private double _importoInsolutoInt;
	private double _importoInContenziosoInt;
	private double _importoTotale;
	private double _importoPagato;
	private double _importoAperto;
	private double _importoEsposto;
	private double _importoScaduto;
	private double _importoInsoluto;
	private double _importoInContenzioso;
	private int _numeroLettereSollecito;
	private String _codiceDivisaInterna;
	private int _codiceEsercizioScadenzaConversione;
	private int _numeroPartitaScadenzaCoversione;
	private int _numeroScadenzaCoversione;
	private int _tipoCoversione;
	private int _codiceEsercizioScritturaConversione;
	private int _numeroRegistrazioneScritturaConversione;
	private Date _dataRegistrazioneScritturaConversione;
	private String _codiceDivisaIntScadenzaConversione;
	private String _codiceDivisaScadenzaConversione;
	private double _cambioScadenzaConversione;
}