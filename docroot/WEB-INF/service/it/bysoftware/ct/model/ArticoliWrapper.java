/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Articoli}.
 * </p>
 *
 * @author Mario Torrisi
 * @see Articoli
 * @generated
 */
public class ArticoliWrapper implements Articoli, ModelWrapper<Articoli> {
	public ArticoliWrapper(Articoli articoli) {
		_articoli = articoli;
	}

	@Override
	public Class<?> getModelClass() {
		return Articoli.class;
	}

	@Override
	public String getModelClassName() {
		return Articoli.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codiceArticolo", getCodiceArticolo());
		attributes.put("descrizione", getDescrizione());
		attributes.put("descrizioneAggiuntiva", getDescrizioneAggiuntiva());
		attributes.put("descrizioneBreveFiscale", getDescrizioneBreveFiscale());
		attributes.put("pathImmagine", getPathImmagine());
		attributes.put("unitaMisura", getUnitaMisura());
		attributes.put("categoriaMerceologica", getCategoriaMerceologica());
		attributes.put("categoriaFiscale", getCategoriaFiscale());
		attributes.put("codiceIVA", getCodiceIVA());
		attributes.put("codiceProvvigioni", getCodiceProvvigioni());
		attributes.put("codiceSconto1", getCodiceSconto1());
		attributes.put("codiceSconto2", getCodiceSconto2());
		attributes.put("categoriaInventario", getCategoriaInventario());
		attributes.put("prezzo1", getPrezzo1());
		attributes.put("prezzo2", getPrezzo2());
		attributes.put("prezzo3", getPrezzo3());
		attributes.put("codiceIVAPrezzo1", getCodiceIVAPrezzo1());
		attributes.put("codiceIVAPrezzo2", getCodiceIVAPrezzo2());
		attributes.put("codiceIVAPrezzo3", getCodiceIVAPrezzo3());
		attributes.put("numeroDecimaliPrezzo", getNumeroDecimaliPrezzo());
		attributes.put("numeroDecimaliQuantita", getNumeroDecimaliQuantita());
		attributes.put("numeroMesiPerinvenduto", getNumeroMesiPerinvenduto());
		attributes.put("unitaMisura2", getUnitaMisura2());
		attributes.put("fattoreMoltiplicativo", getFattoreMoltiplicativo());
		attributes.put("fattoreDivisione", getFattoreDivisione());
		attributes.put("ragruppamentoLibero", getRagruppamentoLibero());
		attributes.put("tipoLivelloDistintaBase", getTipoLivelloDistintaBase());
		attributes.put("tipoCostoStatistico", getTipoCostoStatistico());
		attributes.put("tipoGestioneArticolo", getTipoGestioneArticolo());
		attributes.put("tipoApprovvigionamentoArticolo",
			getTipoApprovvigionamentoArticolo());
		attributes.put("nomenclaturaCombinata", getNomenclaturaCombinata());
		attributes.put("descrizioneEstesa", getDescrizioneEstesa());
		attributes.put("generazioneMovimenti", getGenerazioneMovimenti());
		attributes.put("tipoEsplosioneDistintaBase",
			getTipoEsplosioneDistintaBase());
		attributes.put("livelloMaxEsplosioneDistintaBase",
			getLivelloMaxEsplosioneDistintaBase());
		attributes.put("valorizzazioneMagazzino", getValorizzazioneMagazzino());
		attributes.put("abilitatoEC", getAbilitatoEC());
		attributes.put("categoriaEC", getCategoriaEC());
		attributes.put("quantitaMinimaEC", getQuantitaMinimaEC());
		attributes.put("quantitaDefaultEC", getQuantitaDefaultEC());
		attributes.put("codiceSchedaTecnicaEC", getCodiceSchedaTecnicaEC());
		attributes.put("giorniPrevistaConsegna", getGiorniPrevistaConsegna());
		attributes.put("gestioneLotti", getGestioneLotti());
		attributes.put("creazioneLotti", getCreazioneLotti());
		attributes.put("prefissoCodiceLottoAutomatico",
			getPrefissoCodiceLottoAutomatico());
		attributes.put("numeroCifreProgressivo", getNumeroCifreProgressivo());
		attributes.put("scaricoAutomaticoLotti", getScaricoAutomaticoLotti());
		attributes.put("giorniInizioValiditaLotto",
			getGiorniInizioValiditaLotto());
		attributes.put("giorniValiditaLotto", getGiorniValiditaLotto());
		attributes.put("giorniPreavvisoScadenzaLotto",
			getGiorniPreavvisoScadenzaLotto());
		attributes.put("fattoreMoltQuantRiservata",
			getFattoreMoltQuantRiservata());
		attributes.put("fattoreDivQuantRiservata", getFattoreDivQuantRiservata());
		attributes.put("obsoleto", getObsoleto());
		attributes.put("comeImballo", getComeImballo());
		attributes.put("fattoreMoltCalcoloImballo",
			getFattoreMoltCalcoloImballo());
		attributes.put("fattoreDivCalcoloImballo", getFattoreDivCalcoloImballo());
		attributes.put("pesoLordo", getPesoLordo());
		attributes.put("tara", getTara());
		attributes.put("calcoloVolume", getCalcoloVolume());
		attributes.put("lunghezza", getLunghezza());
		attributes.put("larghezza", getLarghezza());
		attributes.put("profondita", getProfondita());
		attributes.put("codiceImballo", getCodiceImballo());
		attributes.put("fattoreMoltUnitMisSupp", getFattoreMoltUnitMisSupp());
		attributes.put("fattoreDivUnitMisSupp", getFattoreDivUnitMisSupp());
		attributes.put("stampaetichette", getStampaetichette());
		attributes.put("gestioneVarianti", getGestioneVarianti());
		attributes.put("gestioneVariantiDistintaBase",
			getGestioneVariantiDistintaBase());
		attributes.put("liberoString1", getLiberoString1());
		attributes.put("liberoString2", getLiberoString2());
		attributes.put("liberoString3", getLiberoString3());
		attributes.put("liberoString4", getLiberoString4());
		attributes.put("liberoString5", getLiberoString5());
		attributes.put("liberoDate1", getLiberoDate1());
		attributes.put("liberoDate2", getLiberoDate2());
		attributes.put("liberoDate3", getLiberoDate3());
		attributes.put("liberoDate4", getLiberoDate4());
		attributes.put("liberoDate5", getLiberoDate5());
		attributes.put("liberoLong1", getLiberoLong1());
		attributes.put("liberoLong2", getLiberoLong2());
		attributes.put("liberoLong3", getLiberoLong3());
		attributes.put("liberoLong4", getLiberoLong4());
		attributes.put("liberoLong5", getLiberoLong5());
		attributes.put("liberoDouble1", getLiberoDouble1());
		attributes.put("liberoDouble2", getLiberoDouble2());
		attributes.put("liberoDouble3", getLiberoDouble3());
		attributes.put("liberoDouble4", getLiberoDouble4());
		attributes.put("liberoDouble5", getLiberoDouble5());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String codiceArticolo = (String)attributes.get("codiceArticolo");

		if (codiceArticolo != null) {
			setCodiceArticolo(codiceArticolo);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		String descrizioneAggiuntiva = (String)attributes.get(
				"descrizioneAggiuntiva");

		if (descrizioneAggiuntiva != null) {
			setDescrizioneAggiuntiva(descrizioneAggiuntiva);
		}

		String descrizioneBreveFiscale = (String)attributes.get(
				"descrizioneBreveFiscale");

		if (descrizioneBreveFiscale != null) {
			setDescrizioneBreveFiscale(descrizioneBreveFiscale);
		}

		String pathImmagine = (String)attributes.get("pathImmagine");

		if (pathImmagine != null) {
			setPathImmagine(pathImmagine);
		}

		String unitaMisura = (String)attributes.get("unitaMisura");

		if (unitaMisura != null) {
			setUnitaMisura(unitaMisura);
		}

		String categoriaMerceologica = (String)attributes.get(
				"categoriaMerceologica");

		if (categoriaMerceologica != null) {
			setCategoriaMerceologica(categoriaMerceologica);
		}

		String categoriaFiscale = (String)attributes.get("categoriaFiscale");

		if (categoriaFiscale != null) {
			setCategoriaFiscale(categoriaFiscale);
		}

		String codiceIVA = (String)attributes.get("codiceIVA");

		if (codiceIVA != null) {
			setCodiceIVA(codiceIVA);
		}

		String codiceProvvigioni = (String)attributes.get("codiceProvvigioni");

		if (codiceProvvigioni != null) {
			setCodiceProvvigioni(codiceProvvigioni);
		}

		String codiceSconto1 = (String)attributes.get("codiceSconto1");

		if (codiceSconto1 != null) {
			setCodiceSconto1(codiceSconto1);
		}

		String codiceSconto2 = (String)attributes.get("codiceSconto2");

		if (codiceSconto2 != null) {
			setCodiceSconto2(codiceSconto2);
		}

		String categoriaInventario = (String)attributes.get(
				"categoriaInventario");

		if (categoriaInventario != null) {
			setCategoriaInventario(categoriaInventario);
		}

		Double prezzo1 = (Double)attributes.get("prezzo1");

		if (prezzo1 != null) {
			setPrezzo1(prezzo1);
		}

		Double prezzo2 = (Double)attributes.get("prezzo2");

		if (prezzo2 != null) {
			setPrezzo2(prezzo2);
		}

		Double prezzo3 = (Double)attributes.get("prezzo3");

		if (prezzo3 != null) {
			setPrezzo3(prezzo3);
		}

		String codiceIVAPrezzo1 = (String)attributes.get("codiceIVAPrezzo1");

		if (codiceIVAPrezzo1 != null) {
			setCodiceIVAPrezzo1(codiceIVAPrezzo1);
		}

		String codiceIVAPrezzo2 = (String)attributes.get("codiceIVAPrezzo2");

		if (codiceIVAPrezzo2 != null) {
			setCodiceIVAPrezzo2(codiceIVAPrezzo2);
		}

		String codiceIVAPrezzo3 = (String)attributes.get("codiceIVAPrezzo3");

		if (codiceIVAPrezzo3 != null) {
			setCodiceIVAPrezzo3(codiceIVAPrezzo3);
		}

		Integer numeroDecimaliPrezzo = (Integer)attributes.get(
				"numeroDecimaliPrezzo");

		if (numeroDecimaliPrezzo != null) {
			setNumeroDecimaliPrezzo(numeroDecimaliPrezzo);
		}

		Integer numeroDecimaliQuantita = (Integer)attributes.get(
				"numeroDecimaliQuantita");

		if (numeroDecimaliQuantita != null) {
			setNumeroDecimaliQuantita(numeroDecimaliQuantita);
		}

		Integer numeroMesiPerinvenduto = (Integer)attributes.get(
				"numeroMesiPerinvenduto");

		if (numeroMesiPerinvenduto != null) {
			setNumeroMesiPerinvenduto(numeroMesiPerinvenduto);
		}

		String unitaMisura2 = (String)attributes.get("unitaMisura2");

		if (unitaMisura2 != null) {
			setUnitaMisura2(unitaMisura2);
		}

		Double fattoreMoltiplicativo = (Double)attributes.get(
				"fattoreMoltiplicativo");

		if (fattoreMoltiplicativo != null) {
			setFattoreMoltiplicativo(fattoreMoltiplicativo);
		}

		Double fattoreDivisione = (Double)attributes.get("fattoreDivisione");

		if (fattoreDivisione != null) {
			setFattoreDivisione(fattoreDivisione);
		}

		String ragruppamentoLibero = (String)attributes.get(
				"ragruppamentoLibero");

		if (ragruppamentoLibero != null) {
			setRagruppamentoLibero(ragruppamentoLibero);
		}

		Integer tipoLivelloDistintaBase = (Integer)attributes.get(
				"tipoLivelloDistintaBase");

		if (tipoLivelloDistintaBase != null) {
			setTipoLivelloDistintaBase(tipoLivelloDistintaBase);
		}

		Integer tipoCostoStatistico = (Integer)attributes.get(
				"tipoCostoStatistico");

		if (tipoCostoStatistico != null) {
			setTipoCostoStatistico(tipoCostoStatistico);
		}

		Integer tipoGestioneArticolo = (Integer)attributes.get(
				"tipoGestioneArticolo");

		if (tipoGestioneArticolo != null) {
			setTipoGestioneArticolo(tipoGestioneArticolo);
		}

		Integer tipoApprovvigionamentoArticolo = (Integer)attributes.get(
				"tipoApprovvigionamentoArticolo");

		if (tipoApprovvigionamentoArticolo != null) {
			setTipoApprovvigionamentoArticolo(tipoApprovvigionamentoArticolo);
		}

		Double nomenclaturaCombinata = (Double)attributes.get(
				"nomenclaturaCombinata");

		if (nomenclaturaCombinata != null) {
			setNomenclaturaCombinata(nomenclaturaCombinata);
		}

		String descrizioneEstesa = (String)attributes.get("descrizioneEstesa");

		if (descrizioneEstesa != null) {
			setDescrizioneEstesa(descrizioneEstesa);
		}

		Boolean generazioneMovimenti = (Boolean)attributes.get(
				"generazioneMovimenti");

		if (generazioneMovimenti != null) {
			setGenerazioneMovimenti(generazioneMovimenti);
		}

		Integer tipoEsplosioneDistintaBase = (Integer)attributes.get(
				"tipoEsplosioneDistintaBase");

		if (tipoEsplosioneDistintaBase != null) {
			setTipoEsplosioneDistintaBase(tipoEsplosioneDistintaBase);
		}

		Integer livelloMaxEsplosioneDistintaBase = (Integer)attributes.get(
				"livelloMaxEsplosioneDistintaBase");

		if (livelloMaxEsplosioneDistintaBase != null) {
			setLivelloMaxEsplosioneDistintaBase(livelloMaxEsplosioneDistintaBase);
		}

		Boolean valorizzazioneMagazzino = (Boolean)attributes.get(
				"valorizzazioneMagazzino");

		if (valorizzazioneMagazzino != null) {
			setValorizzazioneMagazzino(valorizzazioneMagazzino);
		}

		Boolean abilitatoEC = (Boolean)attributes.get("abilitatoEC");

		if (abilitatoEC != null) {
			setAbilitatoEC(abilitatoEC);
		}

		String categoriaEC = (String)attributes.get("categoriaEC");

		if (categoriaEC != null) {
			setCategoriaEC(categoriaEC);
		}

		Double quantitaMinimaEC = (Double)attributes.get("quantitaMinimaEC");

		if (quantitaMinimaEC != null) {
			setQuantitaMinimaEC(quantitaMinimaEC);
		}

		Double quantitaDefaultEC = (Double)attributes.get("quantitaDefaultEC");

		if (quantitaDefaultEC != null) {
			setQuantitaDefaultEC(quantitaDefaultEC);
		}

		String codiceSchedaTecnicaEC = (String)attributes.get(
				"codiceSchedaTecnicaEC");

		if (codiceSchedaTecnicaEC != null) {
			setCodiceSchedaTecnicaEC(codiceSchedaTecnicaEC);
		}

		Integer giorniPrevistaConsegna = (Integer)attributes.get(
				"giorniPrevistaConsegna");

		if (giorniPrevistaConsegna != null) {
			setGiorniPrevistaConsegna(giorniPrevistaConsegna);
		}

		Boolean gestioneLotti = (Boolean)attributes.get("gestioneLotti");

		if (gestioneLotti != null) {
			setGestioneLotti(gestioneLotti);
		}

		Boolean creazioneLotti = (Boolean)attributes.get("creazioneLotti");

		if (creazioneLotti != null) {
			setCreazioneLotti(creazioneLotti);
		}

		String prefissoCodiceLottoAutomatico = (String)attributes.get(
				"prefissoCodiceLottoAutomatico");

		if (prefissoCodiceLottoAutomatico != null) {
			setPrefissoCodiceLottoAutomatico(prefissoCodiceLottoAutomatico);
		}

		Integer numeroCifreProgressivo = (Integer)attributes.get(
				"numeroCifreProgressivo");

		if (numeroCifreProgressivo != null) {
			setNumeroCifreProgressivo(numeroCifreProgressivo);
		}

		Boolean scaricoAutomaticoLotti = (Boolean)attributes.get(
				"scaricoAutomaticoLotti");

		if (scaricoAutomaticoLotti != null) {
			setScaricoAutomaticoLotti(scaricoAutomaticoLotti);
		}

		Integer giorniInizioValiditaLotto = (Integer)attributes.get(
				"giorniInizioValiditaLotto");

		if (giorniInizioValiditaLotto != null) {
			setGiorniInizioValiditaLotto(giorniInizioValiditaLotto);
		}

		Integer giorniValiditaLotto = (Integer)attributes.get(
				"giorniValiditaLotto");

		if (giorniValiditaLotto != null) {
			setGiorniValiditaLotto(giorniValiditaLotto);
		}

		Integer giorniPreavvisoScadenzaLotto = (Integer)attributes.get(
				"giorniPreavvisoScadenzaLotto");

		if (giorniPreavvisoScadenzaLotto != null) {
			setGiorniPreavvisoScadenzaLotto(giorniPreavvisoScadenzaLotto);
		}

		Double fattoreMoltQuantRiservata = (Double)attributes.get(
				"fattoreMoltQuantRiservata");

		if (fattoreMoltQuantRiservata != null) {
			setFattoreMoltQuantRiservata(fattoreMoltQuantRiservata);
		}

		Double fattoreDivQuantRiservata = (Double)attributes.get(
				"fattoreDivQuantRiservata");

		if (fattoreDivQuantRiservata != null) {
			setFattoreDivQuantRiservata(fattoreDivQuantRiservata);
		}

		Boolean obsoleto = (Boolean)attributes.get("obsoleto");

		if (obsoleto != null) {
			setObsoleto(obsoleto);
		}

		Boolean comeImballo = (Boolean)attributes.get("comeImballo");

		if (comeImballo != null) {
			setComeImballo(comeImballo);
		}

		Double fattoreMoltCalcoloImballo = (Double)attributes.get(
				"fattoreMoltCalcoloImballo");

		if (fattoreMoltCalcoloImballo != null) {
			setFattoreMoltCalcoloImballo(fattoreMoltCalcoloImballo);
		}

		Double fattoreDivCalcoloImballo = (Double)attributes.get(
				"fattoreDivCalcoloImballo");

		if (fattoreDivCalcoloImballo != null) {
			setFattoreDivCalcoloImballo(fattoreDivCalcoloImballo);
		}

		Double pesoLordo = (Double)attributes.get("pesoLordo");

		if (pesoLordo != null) {
			setPesoLordo(pesoLordo);
		}

		Double tara = (Double)attributes.get("tara");

		if (tara != null) {
			setTara(tara);
		}

		Integer calcoloVolume = (Integer)attributes.get("calcoloVolume");

		if (calcoloVolume != null) {
			setCalcoloVolume(calcoloVolume);
		}

		Double lunghezza = (Double)attributes.get("lunghezza");

		if (lunghezza != null) {
			setLunghezza(lunghezza);
		}

		Double larghezza = (Double)attributes.get("larghezza");

		if (larghezza != null) {
			setLarghezza(larghezza);
		}

		Double profondita = (Double)attributes.get("profondita");

		if (profondita != null) {
			setProfondita(profondita);
		}

		String codiceImballo = (String)attributes.get("codiceImballo");

		if (codiceImballo != null) {
			setCodiceImballo(codiceImballo);
		}

		Double fattoreMoltUnitMisSupp = (Double)attributes.get(
				"fattoreMoltUnitMisSupp");

		if (fattoreMoltUnitMisSupp != null) {
			setFattoreMoltUnitMisSupp(fattoreMoltUnitMisSupp);
		}

		Double fattoreDivUnitMisSupp = (Double)attributes.get(
				"fattoreDivUnitMisSupp");

		if (fattoreDivUnitMisSupp != null) {
			setFattoreDivUnitMisSupp(fattoreDivUnitMisSupp);
		}

		Integer stampaetichette = (Integer)attributes.get("stampaetichette");

		if (stampaetichette != null) {
			setStampaetichette(stampaetichette);
		}

		Boolean gestioneVarianti = (Boolean)attributes.get("gestioneVarianti");

		if (gestioneVarianti != null) {
			setGestioneVarianti(gestioneVarianti);
		}

		Boolean gestioneVariantiDistintaBase = (Boolean)attributes.get(
				"gestioneVariantiDistintaBase");

		if (gestioneVariantiDistintaBase != null) {
			setGestioneVariantiDistintaBase(gestioneVariantiDistintaBase);
		}

		String liberoString1 = (String)attributes.get("liberoString1");

		if (liberoString1 != null) {
			setLiberoString1(liberoString1);
		}

		String liberoString2 = (String)attributes.get("liberoString2");

		if (liberoString2 != null) {
			setLiberoString2(liberoString2);
		}

		String liberoString3 = (String)attributes.get("liberoString3");

		if (liberoString3 != null) {
			setLiberoString3(liberoString3);
		}

		String liberoString4 = (String)attributes.get("liberoString4");

		if (liberoString4 != null) {
			setLiberoString4(liberoString4);
		}

		String liberoString5 = (String)attributes.get("liberoString5");

		if (liberoString5 != null) {
			setLiberoString5(liberoString5);
		}

		Date liberoDate1 = (Date)attributes.get("liberoDate1");

		if (liberoDate1 != null) {
			setLiberoDate1(liberoDate1);
		}

		Date liberoDate2 = (Date)attributes.get("liberoDate2");

		if (liberoDate2 != null) {
			setLiberoDate2(liberoDate2);
		}

		Date liberoDate3 = (Date)attributes.get("liberoDate3");

		if (liberoDate3 != null) {
			setLiberoDate3(liberoDate3);
		}

		Date liberoDate4 = (Date)attributes.get("liberoDate4");

		if (liberoDate4 != null) {
			setLiberoDate4(liberoDate4);
		}

		Date liberoDate5 = (Date)attributes.get("liberoDate5");

		if (liberoDate5 != null) {
			setLiberoDate5(liberoDate5);
		}

		Long liberoLong1 = (Long)attributes.get("liberoLong1");

		if (liberoLong1 != null) {
			setLiberoLong1(liberoLong1);
		}

		Long liberoLong2 = (Long)attributes.get("liberoLong2");

		if (liberoLong2 != null) {
			setLiberoLong2(liberoLong2);
		}

		Long liberoLong3 = (Long)attributes.get("liberoLong3");

		if (liberoLong3 != null) {
			setLiberoLong3(liberoLong3);
		}

		Long liberoLong4 = (Long)attributes.get("liberoLong4");

		if (liberoLong4 != null) {
			setLiberoLong4(liberoLong4);
		}

		Long liberoLong5 = (Long)attributes.get("liberoLong5");

		if (liberoLong5 != null) {
			setLiberoLong5(liberoLong5);
		}

		Double liberoDouble1 = (Double)attributes.get("liberoDouble1");

		if (liberoDouble1 != null) {
			setLiberoDouble1(liberoDouble1);
		}

		Double liberoDouble2 = (Double)attributes.get("liberoDouble2");

		if (liberoDouble2 != null) {
			setLiberoDouble2(liberoDouble2);
		}

		Double liberoDouble3 = (Double)attributes.get("liberoDouble3");

		if (liberoDouble3 != null) {
			setLiberoDouble3(liberoDouble3);
		}

		Double liberoDouble4 = (Double)attributes.get("liberoDouble4");

		if (liberoDouble4 != null) {
			setLiberoDouble4(liberoDouble4);
		}

		Double liberoDouble5 = (Double)attributes.get("liberoDouble5");

		if (liberoDouble5 != null) {
			setLiberoDouble5(liberoDouble5);
		}
	}

	/**
	* Returns the primary key of this articoli.
	*
	* @return the primary key of this articoli
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _articoli.getPrimaryKey();
	}

	/**
	* Sets the primary key of this articoli.
	*
	* @param primaryKey the primary key of this articoli
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_articoli.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the codice articolo of this articoli.
	*
	* @return the codice articolo of this articoli
	*/
	@Override
	public java.lang.String getCodiceArticolo() {
		return _articoli.getCodiceArticolo();
	}

	/**
	* Sets the codice articolo of this articoli.
	*
	* @param codiceArticolo the codice articolo of this articoli
	*/
	@Override
	public void setCodiceArticolo(java.lang.String codiceArticolo) {
		_articoli.setCodiceArticolo(codiceArticolo);
	}

	/**
	* Returns the descrizione of this articoli.
	*
	* @return the descrizione of this articoli
	*/
	@Override
	public java.lang.String getDescrizione() {
		return _articoli.getDescrizione();
	}

	/**
	* Sets the descrizione of this articoli.
	*
	* @param descrizione the descrizione of this articoli
	*/
	@Override
	public void setDescrizione(java.lang.String descrizione) {
		_articoli.setDescrizione(descrizione);
	}

	/**
	* Returns the descrizione aggiuntiva of this articoli.
	*
	* @return the descrizione aggiuntiva of this articoli
	*/
	@Override
	public java.lang.String getDescrizioneAggiuntiva() {
		return _articoli.getDescrizioneAggiuntiva();
	}

	/**
	* Sets the descrizione aggiuntiva of this articoli.
	*
	* @param descrizioneAggiuntiva the descrizione aggiuntiva of this articoli
	*/
	@Override
	public void setDescrizioneAggiuntiva(java.lang.String descrizioneAggiuntiva) {
		_articoli.setDescrizioneAggiuntiva(descrizioneAggiuntiva);
	}

	/**
	* Returns the descrizione breve fiscale of this articoli.
	*
	* @return the descrizione breve fiscale of this articoli
	*/
	@Override
	public java.lang.String getDescrizioneBreveFiscale() {
		return _articoli.getDescrizioneBreveFiscale();
	}

	/**
	* Sets the descrizione breve fiscale of this articoli.
	*
	* @param descrizioneBreveFiscale the descrizione breve fiscale of this articoli
	*/
	@Override
	public void setDescrizioneBreveFiscale(
		java.lang.String descrizioneBreveFiscale) {
		_articoli.setDescrizioneBreveFiscale(descrizioneBreveFiscale);
	}

	/**
	* Returns the path immagine of this articoli.
	*
	* @return the path immagine of this articoli
	*/
	@Override
	public java.lang.String getPathImmagine() {
		return _articoli.getPathImmagine();
	}

	/**
	* Sets the path immagine of this articoli.
	*
	* @param pathImmagine the path immagine of this articoli
	*/
	@Override
	public void setPathImmagine(java.lang.String pathImmagine) {
		_articoli.setPathImmagine(pathImmagine);
	}

	/**
	* Returns the unita misura of this articoli.
	*
	* @return the unita misura of this articoli
	*/
	@Override
	public java.lang.String getUnitaMisura() {
		return _articoli.getUnitaMisura();
	}

	/**
	* Sets the unita misura of this articoli.
	*
	* @param unitaMisura the unita misura of this articoli
	*/
	@Override
	public void setUnitaMisura(java.lang.String unitaMisura) {
		_articoli.setUnitaMisura(unitaMisura);
	}

	/**
	* Returns the categoria merceologica of this articoli.
	*
	* @return the categoria merceologica of this articoli
	*/
	@Override
	public java.lang.String getCategoriaMerceologica() {
		return _articoli.getCategoriaMerceologica();
	}

	/**
	* Sets the categoria merceologica of this articoli.
	*
	* @param categoriaMerceologica the categoria merceologica of this articoli
	*/
	@Override
	public void setCategoriaMerceologica(java.lang.String categoriaMerceologica) {
		_articoli.setCategoriaMerceologica(categoriaMerceologica);
	}

	/**
	* Returns the categoria fiscale of this articoli.
	*
	* @return the categoria fiscale of this articoli
	*/
	@Override
	public java.lang.String getCategoriaFiscale() {
		return _articoli.getCategoriaFiscale();
	}

	/**
	* Sets the categoria fiscale of this articoli.
	*
	* @param categoriaFiscale the categoria fiscale of this articoli
	*/
	@Override
	public void setCategoriaFiscale(java.lang.String categoriaFiscale) {
		_articoli.setCategoriaFiscale(categoriaFiscale);
	}

	/**
	* Returns the codice i v a of this articoli.
	*
	* @return the codice i v a of this articoli
	*/
	@Override
	public java.lang.String getCodiceIVA() {
		return _articoli.getCodiceIVA();
	}

	/**
	* Sets the codice i v a of this articoli.
	*
	* @param codiceIVA the codice i v a of this articoli
	*/
	@Override
	public void setCodiceIVA(java.lang.String codiceIVA) {
		_articoli.setCodiceIVA(codiceIVA);
	}

	/**
	* Returns the codice provvigioni of this articoli.
	*
	* @return the codice provvigioni of this articoli
	*/
	@Override
	public java.lang.String getCodiceProvvigioni() {
		return _articoli.getCodiceProvvigioni();
	}

	/**
	* Sets the codice provvigioni of this articoli.
	*
	* @param codiceProvvigioni the codice provvigioni of this articoli
	*/
	@Override
	public void setCodiceProvvigioni(java.lang.String codiceProvvigioni) {
		_articoli.setCodiceProvvigioni(codiceProvvigioni);
	}

	/**
	* Returns the codice sconto1 of this articoli.
	*
	* @return the codice sconto1 of this articoli
	*/
	@Override
	public java.lang.String getCodiceSconto1() {
		return _articoli.getCodiceSconto1();
	}

	/**
	* Sets the codice sconto1 of this articoli.
	*
	* @param codiceSconto1 the codice sconto1 of this articoli
	*/
	@Override
	public void setCodiceSconto1(java.lang.String codiceSconto1) {
		_articoli.setCodiceSconto1(codiceSconto1);
	}

	/**
	* Returns the codice sconto2 of this articoli.
	*
	* @return the codice sconto2 of this articoli
	*/
	@Override
	public java.lang.String getCodiceSconto2() {
		return _articoli.getCodiceSconto2();
	}

	/**
	* Sets the codice sconto2 of this articoli.
	*
	* @param codiceSconto2 the codice sconto2 of this articoli
	*/
	@Override
	public void setCodiceSconto2(java.lang.String codiceSconto2) {
		_articoli.setCodiceSconto2(codiceSconto2);
	}

	/**
	* Returns the categoria inventario of this articoli.
	*
	* @return the categoria inventario of this articoli
	*/
	@Override
	public java.lang.String getCategoriaInventario() {
		return _articoli.getCategoriaInventario();
	}

	/**
	* Sets the categoria inventario of this articoli.
	*
	* @param categoriaInventario the categoria inventario of this articoli
	*/
	@Override
	public void setCategoriaInventario(java.lang.String categoriaInventario) {
		_articoli.setCategoriaInventario(categoriaInventario);
	}

	/**
	* Returns the prezzo1 of this articoli.
	*
	* @return the prezzo1 of this articoli
	*/
	@Override
	public double getPrezzo1() {
		return _articoli.getPrezzo1();
	}

	/**
	* Sets the prezzo1 of this articoli.
	*
	* @param prezzo1 the prezzo1 of this articoli
	*/
	@Override
	public void setPrezzo1(double prezzo1) {
		_articoli.setPrezzo1(prezzo1);
	}

	/**
	* Returns the prezzo2 of this articoli.
	*
	* @return the prezzo2 of this articoli
	*/
	@Override
	public double getPrezzo2() {
		return _articoli.getPrezzo2();
	}

	/**
	* Sets the prezzo2 of this articoli.
	*
	* @param prezzo2 the prezzo2 of this articoli
	*/
	@Override
	public void setPrezzo2(double prezzo2) {
		_articoli.setPrezzo2(prezzo2);
	}

	/**
	* Returns the prezzo3 of this articoli.
	*
	* @return the prezzo3 of this articoli
	*/
	@Override
	public double getPrezzo3() {
		return _articoli.getPrezzo3();
	}

	/**
	* Sets the prezzo3 of this articoli.
	*
	* @param prezzo3 the prezzo3 of this articoli
	*/
	@Override
	public void setPrezzo3(double prezzo3) {
		_articoli.setPrezzo3(prezzo3);
	}

	/**
	* Returns the codice i v a prezzo1 of this articoli.
	*
	* @return the codice i v a prezzo1 of this articoli
	*/
	@Override
	public java.lang.String getCodiceIVAPrezzo1() {
		return _articoli.getCodiceIVAPrezzo1();
	}

	/**
	* Sets the codice i v a prezzo1 of this articoli.
	*
	* @param codiceIVAPrezzo1 the codice i v a prezzo1 of this articoli
	*/
	@Override
	public void setCodiceIVAPrezzo1(java.lang.String codiceIVAPrezzo1) {
		_articoli.setCodiceIVAPrezzo1(codiceIVAPrezzo1);
	}

	/**
	* Returns the codice i v a prezzo2 of this articoli.
	*
	* @return the codice i v a prezzo2 of this articoli
	*/
	@Override
	public java.lang.String getCodiceIVAPrezzo2() {
		return _articoli.getCodiceIVAPrezzo2();
	}

	/**
	* Sets the codice i v a prezzo2 of this articoli.
	*
	* @param codiceIVAPrezzo2 the codice i v a prezzo2 of this articoli
	*/
	@Override
	public void setCodiceIVAPrezzo2(java.lang.String codiceIVAPrezzo2) {
		_articoli.setCodiceIVAPrezzo2(codiceIVAPrezzo2);
	}

	/**
	* Returns the codice i v a prezzo3 of this articoli.
	*
	* @return the codice i v a prezzo3 of this articoli
	*/
	@Override
	public java.lang.String getCodiceIVAPrezzo3() {
		return _articoli.getCodiceIVAPrezzo3();
	}

	/**
	* Sets the codice i v a prezzo3 of this articoli.
	*
	* @param codiceIVAPrezzo3 the codice i v a prezzo3 of this articoli
	*/
	@Override
	public void setCodiceIVAPrezzo3(java.lang.String codiceIVAPrezzo3) {
		_articoli.setCodiceIVAPrezzo3(codiceIVAPrezzo3);
	}

	/**
	* Returns the numero decimali prezzo of this articoli.
	*
	* @return the numero decimali prezzo of this articoli
	*/
	@Override
	public int getNumeroDecimaliPrezzo() {
		return _articoli.getNumeroDecimaliPrezzo();
	}

	/**
	* Sets the numero decimali prezzo of this articoli.
	*
	* @param numeroDecimaliPrezzo the numero decimali prezzo of this articoli
	*/
	@Override
	public void setNumeroDecimaliPrezzo(int numeroDecimaliPrezzo) {
		_articoli.setNumeroDecimaliPrezzo(numeroDecimaliPrezzo);
	}

	/**
	* Returns the numero decimali quantita of this articoli.
	*
	* @return the numero decimali quantita of this articoli
	*/
	@Override
	public int getNumeroDecimaliQuantita() {
		return _articoli.getNumeroDecimaliQuantita();
	}

	/**
	* Sets the numero decimali quantita of this articoli.
	*
	* @param numeroDecimaliQuantita the numero decimali quantita of this articoli
	*/
	@Override
	public void setNumeroDecimaliQuantita(int numeroDecimaliQuantita) {
		_articoli.setNumeroDecimaliQuantita(numeroDecimaliQuantita);
	}

	/**
	* Returns the numero mesi perinvenduto of this articoli.
	*
	* @return the numero mesi perinvenduto of this articoli
	*/
	@Override
	public int getNumeroMesiPerinvenduto() {
		return _articoli.getNumeroMesiPerinvenduto();
	}

	/**
	* Sets the numero mesi perinvenduto of this articoli.
	*
	* @param numeroMesiPerinvenduto the numero mesi perinvenduto of this articoli
	*/
	@Override
	public void setNumeroMesiPerinvenduto(int numeroMesiPerinvenduto) {
		_articoli.setNumeroMesiPerinvenduto(numeroMesiPerinvenduto);
	}

	/**
	* Returns the unita misura2 of this articoli.
	*
	* @return the unita misura2 of this articoli
	*/
	@Override
	public java.lang.String getUnitaMisura2() {
		return _articoli.getUnitaMisura2();
	}

	/**
	* Sets the unita misura2 of this articoli.
	*
	* @param unitaMisura2 the unita misura2 of this articoli
	*/
	@Override
	public void setUnitaMisura2(java.lang.String unitaMisura2) {
		_articoli.setUnitaMisura2(unitaMisura2);
	}

	/**
	* Returns the fattore moltiplicativo of this articoli.
	*
	* @return the fattore moltiplicativo of this articoli
	*/
	@Override
	public double getFattoreMoltiplicativo() {
		return _articoli.getFattoreMoltiplicativo();
	}

	/**
	* Sets the fattore moltiplicativo of this articoli.
	*
	* @param fattoreMoltiplicativo the fattore moltiplicativo of this articoli
	*/
	@Override
	public void setFattoreMoltiplicativo(double fattoreMoltiplicativo) {
		_articoli.setFattoreMoltiplicativo(fattoreMoltiplicativo);
	}

	/**
	* Returns the fattore divisione of this articoli.
	*
	* @return the fattore divisione of this articoli
	*/
	@Override
	public double getFattoreDivisione() {
		return _articoli.getFattoreDivisione();
	}

	/**
	* Sets the fattore divisione of this articoli.
	*
	* @param fattoreDivisione the fattore divisione of this articoli
	*/
	@Override
	public void setFattoreDivisione(double fattoreDivisione) {
		_articoli.setFattoreDivisione(fattoreDivisione);
	}

	/**
	* Returns the ragruppamento libero of this articoli.
	*
	* @return the ragruppamento libero of this articoli
	*/
	@Override
	public java.lang.String getRagruppamentoLibero() {
		return _articoli.getRagruppamentoLibero();
	}

	/**
	* Sets the ragruppamento libero of this articoli.
	*
	* @param ragruppamentoLibero the ragruppamento libero of this articoli
	*/
	@Override
	public void setRagruppamentoLibero(java.lang.String ragruppamentoLibero) {
		_articoli.setRagruppamentoLibero(ragruppamentoLibero);
	}

	/**
	* Returns the tipo livello distinta base of this articoli.
	*
	* @return the tipo livello distinta base of this articoli
	*/
	@Override
	public int getTipoLivelloDistintaBase() {
		return _articoli.getTipoLivelloDistintaBase();
	}

	/**
	* Sets the tipo livello distinta base of this articoli.
	*
	* @param tipoLivelloDistintaBase the tipo livello distinta base of this articoli
	*/
	@Override
	public void setTipoLivelloDistintaBase(int tipoLivelloDistintaBase) {
		_articoli.setTipoLivelloDistintaBase(tipoLivelloDistintaBase);
	}

	/**
	* Returns the tipo costo statistico of this articoli.
	*
	* @return the tipo costo statistico of this articoli
	*/
	@Override
	public int getTipoCostoStatistico() {
		return _articoli.getTipoCostoStatistico();
	}

	/**
	* Sets the tipo costo statistico of this articoli.
	*
	* @param tipoCostoStatistico the tipo costo statistico of this articoli
	*/
	@Override
	public void setTipoCostoStatistico(int tipoCostoStatistico) {
		_articoli.setTipoCostoStatistico(tipoCostoStatistico);
	}

	/**
	* Returns the tipo gestione articolo of this articoli.
	*
	* @return the tipo gestione articolo of this articoli
	*/
	@Override
	public int getTipoGestioneArticolo() {
		return _articoli.getTipoGestioneArticolo();
	}

	/**
	* Sets the tipo gestione articolo of this articoli.
	*
	* @param tipoGestioneArticolo the tipo gestione articolo of this articoli
	*/
	@Override
	public void setTipoGestioneArticolo(int tipoGestioneArticolo) {
		_articoli.setTipoGestioneArticolo(tipoGestioneArticolo);
	}

	/**
	* Returns the tipo approvvigionamento articolo of this articoli.
	*
	* @return the tipo approvvigionamento articolo of this articoli
	*/
	@Override
	public int getTipoApprovvigionamentoArticolo() {
		return _articoli.getTipoApprovvigionamentoArticolo();
	}

	/**
	* Sets the tipo approvvigionamento articolo of this articoli.
	*
	* @param tipoApprovvigionamentoArticolo the tipo approvvigionamento articolo of this articoli
	*/
	@Override
	public void setTipoApprovvigionamentoArticolo(
		int tipoApprovvigionamentoArticolo) {
		_articoli.setTipoApprovvigionamentoArticolo(tipoApprovvigionamentoArticolo);
	}

	/**
	* Returns the nomenclatura combinata of this articoli.
	*
	* @return the nomenclatura combinata of this articoli
	*/
	@Override
	public double getNomenclaturaCombinata() {
		return _articoli.getNomenclaturaCombinata();
	}

	/**
	* Sets the nomenclatura combinata of this articoli.
	*
	* @param nomenclaturaCombinata the nomenclatura combinata of this articoli
	*/
	@Override
	public void setNomenclaturaCombinata(double nomenclaturaCombinata) {
		_articoli.setNomenclaturaCombinata(nomenclaturaCombinata);
	}

	/**
	* Returns the descrizione estesa of this articoli.
	*
	* @return the descrizione estesa of this articoli
	*/
	@Override
	public java.lang.String getDescrizioneEstesa() {
		return _articoli.getDescrizioneEstesa();
	}

	/**
	* Sets the descrizione estesa of this articoli.
	*
	* @param descrizioneEstesa the descrizione estesa of this articoli
	*/
	@Override
	public void setDescrizioneEstesa(java.lang.String descrizioneEstesa) {
		_articoli.setDescrizioneEstesa(descrizioneEstesa);
	}

	/**
	* Returns the generazione movimenti of this articoli.
	*
	* @return the generazione movimenti of this articoli
	*/
	@Override
	public boolean getGenerazioneMovimenti() {
		return _articoli.getGenerazioneMovimenti();
	}

	/**
	* Returns <code>true</code> if this articoli is generazione movimenti.
	*
	* @return <code>true</code> if this articoli is generazione movimenti; <code>false</code> otherwise
	*/
	@Override
	public boolean isGenerazioneMovimenti() {
		return _articoli.isGenerazioneMovimenti();
	}

	/**
	* Sets whether this articoli is generazione movimenti.
	*
	* @param generazioneMovimenti the generazione movimenti of this articoli
	*/
	@Override
	public void setGenerazioneMovimenti(boolean generazioneMovimenti) {
		_articoli.setGenerazioneMovimenti(generazioneMovimenti);
	}

	/**
	* Returns the tipo esplosione distinta base of this articoli.
	*
	* @return the tipo esplosione distinta base of this articoli
	*/
	@Override
	public int getTipoEsplosioneDistintaBase() {
		return _articoli.getTipoEsplosioneDistintaBase();
	}

	/**
	* Sets the tipo esplosione distinta base of this articoli.
	*
	* @param tipoEsplosioneDistintaBase the tipo esplosione distinta base of this articoli
	*/
	@Override
	public void setTipoEsplosioneDistintaBase(int tipoEsplosioneDistintaBase) {
		_articoli.setTipoEsplosioneDistintaBase(tipoEsplosioneDistintaBase);
	}

	/**
	* Returns the livello max esplosione distinta base of this articoli.
	*
	* @return the livello max esplosione distinta base of this articoli
	*/
	@Override
	public int getLivelloMaxEsplosioneDistintaBase() {
		return _articoli.getLivelloMaxEsplosioneDistintaBase();
	}

	/**
	* Sets the livello max esplosione distinta base of this articoli.
	*
	* @param livelloMaxEsplosioneDistintaBase the livello max esplosione distinta base of this articoli
	*/
	@Override
	public void setLivelloMaxEsplosioneDistintaBase(
		int livelloMaxEsplosioneDistintaBase) {
		_articoli.setLivelloMaxEsplosioneDistintaBase(livelloMaxEsplosioneDistintaBase);
	}

	/**
	* Returns the valorizzazione magazzino of this articoli.
	*
	* @return the valorizzazione magazzino of this articoli
	*/
	@Override
	public boolean getValorizzazioneMagazzino() {
		return _articoli.getValorizzazioneMagazzino();
	}

	/**
	* Returns <code>true</code> if this articoli is valorizzazione magazzino.
	*
	* @return <code>true</code> if this articoli is valorizzazione magazzino; <code>false</code> otherwise
	*/
	@Override
	public boolean isValorizzazioneMagazzino() {
		return _articoli.isValorizzazioneMagazzino();
	}

	/**
	* Sets whether this articoli is valorizzazione magazzino.
	*
	* @param valorizzazioneMagazzino the valorizzazione magazzino of this articoli
	*/
	@Override
	public void setValorizzazioneMagazzino(boolean valorizzazioneMagazzino) {
		_articoli.setValorizzazioneMagazzino(valorizzazioneMagazzino);
	}

	/**
	* Returns the abilitato e c of this articoli.
	*
	* @return the abilitato e c of this articoli
	*/
	@Override
	public boolean getAbilitatoEC() {
		return _articoli.getAbilitatoEC();
	}

	/**
	* Returns <code>true</code> if this articoli is abilitato e c.
	*
	* @return <code>true</code> if this articoli is abilitato e c; <code>false</code> otherwise
	*/
	@Override
	public boolean isAbilitatoEC() {
		return _articoli.isAbilitatoEC();
	}

	/**
	* Sets whether this articoli is abilitato e c.
	*
	* @param abilitatoEC the abilitato e c of this articoli
	*/
	@Override
	public void setAbilitatoEC(boolean abilitatoEC) {
		_articoli.setAbilitatoEC(abilitatoEC);
	}

	/**
	* Returns the categoria e c of this articoli.
	*
	* @return the categoria e c of this articoli
	*/
	@Override
	public java.lang.String getCategoriaEC() {
		return _articoli.getCategoriaEC();
	}

	/**
	* Sets the categoria e c of this articoli.
	*
	* @param categoriaEC the categoria e c of this articoli
	*/
	@Override
	public void setCategoriaEC(java.lang.String categoriaEC) {
		_articoli.setCategoriaEC(categoriaEC);
	}

	/**
	* Returns the quantita minima e c of this articoli.
	*
	* @return the quantita minima e c of this articoli
	*/
	@Override
	public double getQuantitaMinimaEC() {
		return _articoli.getQuantitaMinimaEC();
	}

	/**
	* Sets the quantita minima e c of this articoli.
	*
	* @param quantitaMinimaEC the quantita minima e c of this articoli
	*/
	@Override
	public void setQuantitaMinimaEC(double quantitaMinimaEC) {
		_articoli.setQuantitaMinimaEC(quantitaMinimaEC);
	}

	/**
	* Returns the quantita default e c of this articoli.
	*
	* @return the quantita default e c of this articoli
	*/
	@Override
	public double getQuantitaDefaultEC() {
		return _articoli.getQuantitaDefaultEC();
	}

	/**
	* Sets the quantita default e c of this articoli.
	*
	* @param quantitaDefaultEC the quantita default e c of this articoli
	*/
	@Override
	public void setQuantitaDefaultEC(double quantitaDefaultEC) {
		_articoli.setQuantitaDefaultEC(quantitaDefaultEC);
	}

	/**
	* Returns the codice scheda tecnica e c of this articoli.
	*
	* @return the codice scheda tecnica e c of this articoli
	*/
	@Override
	public java.lang.String getCodiceSchedaTecnicaEC() {
		return _articoli.getCodiceSchedaTecnicaEC();
	}

	/**
	* Sets the codice scheda tecnica e c of this articoli.
	*
	* @param codiceSchedaTecnicaEC the codice scheda tecnica e c of this articoli
	*/
	@Override
	public void setCodiceSchedaTecnicaEC(java.lang.String codiceSchedaTecnicaEC) {
		_articoli.setCodiceSchedaTecnicaEC(codiceSchedaTecnicaEC);
	}

	/**
	* Returns the giorni prevista consegna of this articoli.
	*
	* @return the giorni prevista consegna of this articoli
	*/
	@Override
	public int getGiorniPrevistaConsegna() {
		return _articoli.getGiorniPrevistaConsegna();
	}

	/**
	* Sets the giorni prevista consegna of this articoli.
	*
	* @param giorniPrevistaConsegna the giorni prevista consegna of this articoli
	*/
	@Override
	public void setGiorniPrevistaConsegna(int giorniPrevistaConsegna) {
		_articoli.setGiorniPrevistaConsegna(giorniPrevistaConsegna);
	}

	/**
	* Returns the gestione lotti of this articoli.
	*
	* @return the gestione lotti of this articoli
	*/
	@Override
	public boolean getGestioneLotti() {
		return _articoli.getGestioneLotti();
	}

	/**
	* Returns <code>true</code> if this articoli is gestione lotti.
	*
	* @return <code>true</code> if this articoli is gestione lotti; <code>false</code> otherwise
	*/
	@Override
	public boolean isGestioneLotti() {
		return _articoli.isGestioneLotti();
	}

	/**
	* Sets whether this articoli is gestione lotti.
	*
	* @param gestioneLotti the gestione lotti of this articoli
	*/
	@Override
	public void setGestioneLotti(boolean gestioneLotti) {
		_articoli.setGestioneLotti(gestioneLotti);
	}

	/**
	* Returns the creazione lotti of this articoli.
	*
	* @return the creazione lotti of this articoli
	*/
	@Override
	public boolean getCreazioneLotti() {
		return _articoli.getCreazioneLotti();
	}

	/**
	* Returns <code>true</code> if this articoli is creazione lotti.
	*
	* @return <code>true</code> if this articoli is creazione lotti; <code>false</code> otherwise
	*/
	@Override
	public boolean isCreazioneLotti() {
		return _articoli.isCreazioneLotti();
	}

	/**
	* Sets whether this articoli is creazione lotti.
	*
	* @param creazioneLotti the creazione lotti of this articoli
	*/
	@Override
	public void setCreazioneLotti(boolean creazioneLotti) {
		_articoli.setCreazioneLotti(creazioneLotti);
	}

	/**
	* Returns the prefisso codice lotto automatico of this articoli.
	*
	* @return the prefisso codice lotto automatico of this articoli
	*/
	@Override
	public java.lang.String getPrefissoCodiceLottoAutomatico() {
		return _articoli.getPrefissoCodiceLottoAutomatico();
	}

	/**
	* Sets the prefisso codice lotto automatico of this articoli.
	*
	* @param prefissoCodiceLottoAutomatico the prefisso codice lotto automatico of this articoli
	*/
	@Override
	public void setPrefissoCodiceLottoAutomatico(
		java.lang.String prefissoCodiceLottoAutomatico) {
		_articoli.setPrefissoCodiceLottoAutomatico(prefissoCodiceLottoAutomatico);
	}

	/**
	* Returns the numero cifre progressivo of this articoli.
	*
	* @return the numero cifre progressivo of this articoli
	*/
	@Override
	public int getNumeroCifreProgressivo() {
		return _articoli.getNumeroCifreProgressivo();
	}

	/**
	* Sets the numero cifre progressivo of this articoli.
	*
	* @param numeroCifreProgressivo the numero cifre progressivo of this articoli
	*/
	@Override
	public void setNumeroCifreProgressivo(int numeroCifreProgressivo) {
		_articoli.setNumeroCifreProgressivo(numeroCifreProgressivo);
	}

	/**
	* Returns the scarico automatico lotti of this articoli.
	*
	* @return the scarico automatico lotti of this articoli
	*/
	@Override
	public boolean getScaricoAutomaticoLotti() {
		return _articoli.getScaricoAutomaticoLotti();
	}

	/**
	* Returns <code>true</code> if this articoli is scarico automatico lotti.
	*
	* @return <code>true</code> if this articoli is scarico automatico lotti; <code>false</code> otherwise
	*/
	@Override
	public boolean isScaricoAutomaticoLotti() {
		return _articoli.isScaricoAutomaticoLotti();
	}

	/**
	* Sets whether this articoli is scarico automatico lotti.
	*
	* @param scaricoAutomaticoLotti the scarico automatico lotti of this articoli
	*/
	@Override
	public void setScaricoAutomaticoLotti(boolean scaricoAutomaticoLotti) {
		_articoli.setScaricoAutomaticoLotti(scaricoAutomaticoLotti);
	}

	/**
	* Returns the giorni inizio validita lotto of this articoli.
	*
	* @return the giorni inizio validita lotto of this articoli
	*/
	@Override
	public int getGiorniInizioValiditaLotto() {
		return _articoli.getGiorniInizioValiditaLotto();
	}

	/**
	* Sets the giorni inizio validita lotto of this articoli.
	*
	* @param giorniInizioValiditaLotto the giorni inizio validita lotto of this articoli
	*/
	@Override
	public void setGiorniInizioValiditaLotto(int giorniInizioValiditaLotto) {
		_articoli.setGiorniInizioValiditaLotto(giorniInizioValiditaLotto);
	}

	/**
	* Returns the giorni validita lotto of this articoli.
	*
	* @return the giorni validita lotto of this articoli
	*/
	@Override
	public int getGiorniValiditaLotto() {
		return _articoli.getGiorniValiditaLotto();
	}

	/**
	* Sets the giorni validita lotto of this articoli.
	*
	* @param giorniValiditaLotto the giorni validita lotto of this articoli
	*/
	@Override
	public void setGiorniValiditaLotto(int giorniValiditaLotto) {
		_articoli.setGiorniValiditaLotto(giorniValiditaLotto);
	}

	/**
	* Returns the giorni preavviso scadenza lotto of this articoli.
	*
	* @return the giorni preavviso scadenza lotto of this articoli
	*/
	@Override
	public int getGiorniPreavvisoScadenzaLotto() {
		return _articoli.getGiorniPreavvisoScadenzaLotto();
	}

	/**
	* Sets the giorni preavviso scadenza lotto of this articoli.
	*
	* @param giorniPreavvisoScadenzaLotto the giorni preavviso scadenza lotto of this articoli
	*/
	@Override
	public void setGiorniPreavvisoScadenzaLotto(
		int giorniPreavvisoScadenzaLotto) {
		_articoli.setGiorniPreavvisoScadenzaLotto(giorniPreavvisoScadenzaLotto);
	}

	/**
	* Returns the fattore molt quant riservata of this articoli.
	*
	* @return the fattore molt quant riservata of this articoli
	*/
	@Override
	public double getFattoreMoltQuantRiservata() {
		return _articoli.getFattoreMoltQuantRiservata();
	}

	/**
	* Sets the fattore molt quant riservata of this articoli.
	*
	* @param fattoreMoltQuantRiservata the fattore molt quant riservata of this articoli
	*/
	@Override
	public void setFattoreMoltQuantRiservata(double fattoreMoltQuantRiservata) {
		_articoli.setFattoreMoltQuantRiservata(fattoreMoltQuantRiservata);
	}

	/**
	* Returns the fattore div quant riservata of this articoli.
	*
	* @return the fattore div quant riservata of this articoli
	*/
	@Override
	public double getFattoreDivQuantRiservata() {
		return _articoli.getFattoreDivQuantRiservata();
	}

	/**
	* Sets the fattore div quant riservata of this articoli.
	*
	* @param fattoreDivQuantRiservata the fattore div quant riservata of this articoli
	*/
	@Override
	public void setFattoreDivQuantRiservata(double fattoreDivQuantRiservata) {
		_articoli.setFattoreDivQuantRiservata(fattoreDivQuantRiservata);
	}

	/**
	* Returns the obsoleto of this articoli.
	*
	* @return the obsoleto of this articoli
	*/
	@Override
	public boolean getObsoleto() {
		return _articoli.getObsoleto();
	}

	/**
	* Returns <code>true</code> if this articoli is obsoleto.
	*
	* @return <code>true</code> if this articoli is obsoleto; <code>false</code> otherwise
	*/
	@Override
	public boolean isObsoleto() {
		return _articoli.isObsoleto();
	}

	/**
	* Sets whether this articoli is obsoleto.
	*
	* @param obsoleto the obsoleto of this articoli
	*/
	@Override
	public void setObsoleto(boolean obsoleto) {
		_articoli.setObsoleto(obsoleto);
	}

	/**
	* Returns the come imballo of this articoli.
	*
	* @return the come imballo of this articoli
	*/
	@Override
	public boolean getComeImballo() {
		return _articoli.getComeImballo();
	}

	/**
	* Returns <code>true</code> if this articoli is come imballo.
	*
	* @return <code>true</code> if this articoli is come imballo; <code>false</code> otherwise
	*/
	@Override
	public boolean isComeImballo() {
		return _articoli.isComeImballo();
	}

	/**
	* Sets whether this articoli is come imballo.
	*
	* @param comeImballo the come imballo of this articoli
	*/
	@Override
	public void setComeImballo(boolean comeImballo) {
		_articoli.setComeImballo(comeImballo);
	}

	/**
	* Returns the fattore molt calcolo imballo of this articoli.
	*
	* @return the fattore molt calcolo imballo of this articoli
	*/
	@Override
	public double getFattoreMoltCalcoloImballo() {
		return _articoli.getFattoreMoltCalcoloImballo();
	}

	/**
	* Sets the fattore molt calcolo imballo of this articoli.
	*
	* @param fattoreMoltCalcoloImballo the fattore molt calcolo imballo of this articoli
	*/
	@Override
	public void setFattoreMoltCalcoloImballo(double fattoreMoltCalcoloImballo) {
		_articoli.setFattoreMoltCalcoloImballo(fattoreMoltCalcoloImballo);
	}

	/**
	* Returns the fattore div calcolo imballo of this articoli.
	*
	* @return the fattore div calcolo imballo of this articoli
	*/
	@Override
	public double getFattoreDivCalcoloImballo() {
		return _articoli.getFattoreDivCalcoloImballo();
	}

	/**
	* Sets the fattore div calcolo imballo of this articoli.
	*
	* @param fattoreDivCalcoloImballo the fattore div calcolo imballo of this articoli
	*/
	@Override
	public void setFattoreDivCalcoloImballo(double fattoreDivCalcoloImballo) {
		_articoli.setFattoreDivCalcoloImballo(fattoreDivCalcoloImballo);
	}

	/**
	* Returns the peso lordo of this articoli.
	*
	* @return the peso lordo of this articoli
	*/
	@Override
	public double getPesoLordo() {
		return _articoli.getPesoLordo();
	}

	/**
	* Sets the peso lordo of this articoli.
	*
	* @param pesoLordo the peso lordo of this articoli
	*/
	@Override
	public void setPesoLordo(double pesoLordo) {
		_articoli.setPesoLordo(pesoLordo);
	}

	/**
	* Returns the tara of this articoli.
	*
	* @return the tara of this articoli
	*/
	@Override
	public double getTara() {
		return _articoli.getTara();
	}

	/**
	* Sets the tara of this articoli.
	*
	* @param tara the tara of this articoli
	*/
	@Override
	public void setTara(double tara) {
		_articoli.setTara(tara);
	}

	/**
	* Returns the calcolo volume of this articoli.
	*
	* @return the calcolo volume of this articoli
	*/
	@Override
	public int getCalcoloVolume() {
		return _articoli.getCalcoloVolume();
	}

	/**
	* Sets the calcolo volume of this articoli.
	*
	* @param calcoloVolume the calcolo volume of this articoli
	*/
	@Override
	public void setCalcoloVolume(int calcoloVolume) {
		_articoli.setCalcoloVolume(calcoloVolume);
	}

	/**
	* Returns the lunghezza of this articoli.
	*
	* @return the lunghezza of this articoli
	*/
	@Override
	public double getLunghezza() {
		return _articoli.getLunghezza();
	}

	/**
	* Sets the lunghezza of this articoli.
	*
	* @param lunghezza the lunghezza of this articoli
	*/
	@Override
	public void setLunghezza(double lunghezza) {
		_articoli.setLunghezza(lunghezza);
	}

	/**
	* Returns the larghezza of this articoli.
	*
	* @return the larghezza of this articoli
	*/
	@Override
	public double getLarghezza() {
		return _articoli.getLarghezza();
	}

	/**
	* Sets the larghezza of this articoli.
	*
	* @param larghezza the larghezza of this articoli
	*/
	@Override
	public void setLarghezza(double larghezza) {
		_articoli.setLarghezza(larghezza);
	}

	/**
	* Returns the profondita of this articoli.
	*
	* @return the profondita of this articoli
	*/
	@Override
	public double getProfondita() {
		return _articoli.getProfondita();
	}

	/**
	* Sets the profondita of this articoli.
	*
	* @param profondita the profondita of this articoli
	*/
	@Override
	public void setProfondita(double profondita) {
		_articoli.setProfondita(profondita);
	}

	/**
	* Returns the codice imballo of this articoli.
	*
	* @return the codice imballo of this articoli
	*/
	@Override
	public java.lang.String getCodiceImballo() {
		return _articoli.getCodiceImballo();
	}

	/**
	* Sets the codice imballo of this articoli.
	*
	* @param codiceImballo the codice imballo of this articoli
	*/
	@Override
	public void setCodiceImballo(java.lang.String codiceImballo) {
		_articoli.setCodiceImballo(codiceImballo);
	}

	/**
	* Returns the fattore molt unit mis supp of this articoli.
	*
	* @return the fattore molt unit mis supp of this articoli
	*/
	@Override
	public double getFattoreMoltUnitMisSupp() {
		return _articoli.getFattoreMoltUnitMisSupp();
	}

	/**
	* Sets the fattore molt unit mis supp of this articoli.
	*
	* @param fattoreMoltUnitMisSupp the fattore molt unit mis supp of this articoli
	*/
	@Override
	public void setFattoreMoltUnitMisSupp(double fattoreMoltUnitMisSupp) {
		_articoli.setFattoreMoltUnitMisSupp(fattoreMoltUnitMisSupp);
	}

	/**
	* Returns the fattore div unit mis supp of this articoli.
	*
	* @return the fattore div unit mis supp of this articoli
	*/
	@Override
	public double getFattoreDivUnitMisSupp() {
		return _articoli.getFattoreDivUnitMisSupp();
	}

	/**
	* Sets the fattore div unit mis supp of this articoli.
	*
	* @param fattoreDivUnitMisSupp the fattore div unit mis supp of this articoli
	*/
	@Override
	public void setFattoreDivUnitMisSupp(double fattoreDivUnitMisSupp) {
		_articoli.setFattoreDivUnitMisSupp(fattoreDivUnitMisSupp);
	}

	/**
	* Returns the stampaetichette of this articoli.
	*
	* @return the stampaetichette of this articoli
	*/
	@Override
	public int getStampaetichette() {
		return _articoli.getStampaetichette();
	}

	/**
	* Sets the stampaetichette of this articoli.
	*
	* @param stampaetichette the stampaetichette of this articoli
	*/
	@Override
	public void setStampaetichette(int stampaetichette) {
		_articoli.setStampaetichette(stampaetichette);
	}

	/**
	* Returns the gestione varianti of this articoli.
	*
	* @return the gestione varianti of this articoli
	*/
	@Override
	public boolean getGestioneVarianti() {
		return _articoli.getGestioneVarianti();
	}

	/**
	* Returns <code>true</code> if this articoli is gestione varianti.
	*
	* @return <code>true</code> if this articoli is gestione varianti; <code>false</code> otherwise
	*/
	@Override
	public boolean isGestioneVarianti() {
		return _articoli.isGestioneVarianti();
	}

	/**
	* Sets whether this articoli is gestione varianti.
	*
	* @param gestioneVarianti the gestione varianti of this articoli
	*/
	@Override
	public void setGestioneVarianti(boolean gestioneVarianti) {
		_articoli.setGestioneVarianti(gestioneVarianti);
	}

	/**
	* Returns the gestione varianti distinta base of this articoli.
	*
	* @return the gestione varianti distinta base of this articoli
	*/
	@Override
	public boolean getGestioneVariantiDistintaBase() {
		return _articoli.getGestioneVariantiDistintaBase();
	}

	/**
	* Returns <code>true</code> if this articoli is gestione varianti distinta base.
	*
	* @return <code>true</code> if this articoli is gestione varianti distinta base; <code>false</code> otherwise
	*/
	@Override
	public boolean isGestioneVariantiDistintaBase() {
		return _articoli.isGestioneVariantiDistintaBase();
	}

	/**
	* Sets whether this articoli is gestione varianti distinta base.
	*
	* @param gestioneVariantiDistintaBase the gestione varianti distinta base of this articoli
	*/
	@Override
	public void setGestioneVariantiDistintaBase(
		boolean gestioneVariantiDistintaBase) {
		_articoli.setGestioneVariantiDistintaBase(gestioneVariantiDistintaBase);
	}

	/**
	* Returns the libero string1 of this articoli.
	*
	* @return the libero string1 of this articoli
	*/
	@Override
	public java.lang.String getLiberoString1() {
		return _articoli.getLiberoString1();
	}

	/**
	* Sets the libero string1 of this articoli.
	*
	* @param liberoString1 the libero string1 of this articoli
	*/
	@Override
	public void setLiberoString1(java.lang.String liberoString1) {
		_articoli.setLiberoString1(liberoString1);
	}

	/**
	* Returns the libero string2 of this articoli.
	*
	* @return the libero string2 of this articoli
	*/
	@Override
	public java.lang.String getLiberoString2() {
		return _articoli.getLiberoString2();
	}

	/**
	* Sets the libero string2 of this articoli.
	*
	* @param liberoString2 the libero string2 of this articoli
	*/
	@Override
	public void setLiberoString2(java.lang.String liberoString2) {
		_articoli.setLiberoString2(liberoString2);
	}

	/**
	* Returns the libero string3 of this articoli.
	*
	* @return the libero string3 of this articoli
	*/
	@Override
	public java.lang.String getLiberoString3() {
		return _articoli.getLiberoString3();
	}

	/**
	* Sets the libero string3 of this articoli.
	*
	* @param liberoString3 the libero string3 of this articoli
	*/
	@Override
	public void setLiberoString3(java.lang.String liberoString3) {
		_articoli.setLiberoString3(liberoString3);
	}

	/**
	* Returns the libero string4 of this articoli.
	*
	* @return the libero string4 of this articoli
	*/
	@Override
	public java.lang.String getLiberoString4() {
		return _articoli.getLiberoString4();
	}

	/**
	* Sets the libero string4 of this articoli.
	*
	* @param liberoString4 the libero string4 of this articoli
	*/
	@Override
	public void setLiberoString4(java.lang.String liberoString4) {
		_articoli.setLiberoString4(liberoString4);
	}

	/**
	* Returns the libero string5 of this articoli.
	*
	* @return the libero string5 of this articoli
	*/
	@Override
	public java.lang.String getLiberoString5() {
		return _articoli.getLiberoString5();
	}

	/**
	* Sets the libero string5 of this articoli.
	*
	* @param liberoString5 the libero string5 of this articoli
	*/
	@Override
	public void setLiberoString5(java.lang.String liberoString5) {
		_articoli.setLiberoString5(liberoString5);
	}

	/**
	* Returns the libero date1 of this articoli.
	*
	* @return the libero date1 of this articoli
	*/
	@Override
	public java.util.Date getLiberoDate1() {
		return _articoli.getLiberoDate1();
	}

	/**
	* Sets the libero date1 of this articoli.
	*
	* @param liberoDate1 the libero date1 of this articoli
	*/
	@Override
	public void setLiberoDate1(java.util.Date liberoDate1) {
		_articoli.setLiberoDate1(liberoDate1);
	}

	/**
	* Returns the libero date2 of this articoli.
	*
	* @return the libero date2 of this articoli
	*/
	@Override
	public java.util.Date getLiberoDate2() {
		return _articoli.getLiberoDate2();
	}

	/**
	* Sets the libero date2 of this articoli.
	*
	* @param liberoDate2 the libero date2 of this articoli
	*/
	@Override
	public void setLiberoDate2(java.util.Date liberoDate2) {
		_articoli.setLiberoDate2(liberoDate2);
	}

	/**
	* Returns the libero date3 of this articoli.
	*
	* @return the libero date3 of this articoli
	*/
	@Override
	public java.util.Date getLiberoDate3() {
		return _articoli.getLiberoDate3();
	}

	/**
	* Sets the libero date3 of this articoli.
	*
	* @param liberoDate3 the libero date3 of this articoli
	*/
	@Override
	public void setLiberoDate3(java.util.Date liberoDate3) {
		_articoli.setLiberoDate3(liberoDate3);
	}

	/**
	* Returns the libero date4 of this articoli.
	*
	* @return the libero date4 of this articoli
	*/
	@Override
	public java.util.Date getLiberoDate4() {
		return _articoli.getLiberoDate4();
	}

	/**
	* Sets the libero date4 of this articoli.
	*
	* @param liberoDate4 the libero date4 of this articoli
	*/
	@Override
	public void setLiberoDate4(java.util.Date liberoDate4) {
		_articoli.setLiberoDate4(liberoDate4);
	}

	/**
	* Returns the libero date5 of this articoli.
	*
	* @return the libero date5 of this articoli
	*/
	@Override
	public java.util.Date getLiberoDate5() {
		return _articoli.getLiberoDate5();
	}

	/**
	* Sets the libero date5 of this articoli.
	*
	* @param liberoDate5 the libero date5 of this articoli
	*/
	@Override
	public void setLiberoDate5(java.util.Date liberoDate5) {
		_articoli.setLiberoDate5(liberoDate5);
	}

	/**
	* Returns the libero long1 of this articoli.
	*
	* @return the libero long1 of this articoli
	*/
	@Override
	public long getLiberoLong1() {
		return _articoli.getLiberoLong1();
	}

	/**
	* Sets the libero long1 of this articoli.
	*
	* @param liberoLong1 the libero long1 of this articoli
	*/
	@Override
	public void setLiberoLong1(long liberoLong1) {
		_articoli.setLiberoLong1(liberoLong1);
	}

	/**
	* Returns the libero long2 of this articoli.
	*
	* @return the libero long2 of this articoli
	*/
	@Override
	public long getLiberoLong2() {
		return _articoli.getLiberoLong2();
	}

	/**
	* Sets the libero long2 of this articoli.
	*
	* @param liberoLong2 the libero long2 of this articoli
	*/
	@Override
	public void setLiberoLong2(long liberoLong2) {
		_articoli.setLiberoLong2(liberoLong2);
	}

	/**
	* Returns the libero long3 of this articoli.
	*
	* @return the libero long3 of this articoli
	*/
	@Override
	public long getLiberoLong3() {
		return _articoli.getLiberoLong3();
	}

	/**
	* Sets the libero long3 of this articoli.
	*
	* @param liberoLong3 the libero long3 of this articoli
	*/
	@Override
	public void setLiberoLong3(long liberoLong3) {
		_articoli.setLiberoLong3(liberoLong3);
	}

	/**
	* Returns the libero long4 of this articoli.
	*
	* @return the libero long4 of this articoli
	*/
	@Override
	public long getLiberoLong4() {
		return _articoli.getLiberoLong4();
	}

	/**
	* Sets the libero long4 of this articoli.
	*
	* @param liberoLong4 the libero long4 of this articoli
	*/
	@Override
	public void setLiberoLong4(long liberoLong4) {
		_articoli.setLiberoLong4(liberoLong4);
	}

	/**
	* Returns the libero long5 of this articoli.
	*
	* @return the libero long5 of this articoli
	*/
	@Override
	public long getLiberoLong5() {
		return _articoli.getLiberoLong5();
	}

	/**
	* Sets the libero long5 of this articoli.
	*
	* @param liberoLong5 the libero long5 of this articoli
	*/
	@Override
	public void setLiberoLong5(long liberoLong5) {
		_articoli.setLiberoLong5(liberoLong5);
	}

	/**
	* Returns the libero double1 of this articoli.
	*
	* @return the libero double1 of this articoli
	*/
	@Override
	public double getLiberoDouble1() {
		return _articoli.getLiberoDouble1();
	}

	/**
	* Sets the libero double1 of this articoli.
	*
	* @param liberoDouble1 the libero double1 of this articoli
	*/
	@Override
	public void setLiberoDouble1(double liberoDouble1) {
		_articoli.setLiberoDouble1(liberoDouble1);
	}

	/**
	* Returns the libero double2 of this articoli.
	*
	* @return the libero double2 of this articoli
	*/
	@Override
	public double getLiberoDouble2() {
		return _articoli.getLiberoDouble2();
	}

	/**
	* Sets the libero double2 of this articoli.
	*
	* @param liberoDouble2 the libero double2 of this articoli
	*/
	@Override
	public void setLiberoDouble2(double liberoDouble2) {
		_articoli.setLiberoDouble2(liberoDouble2);
	}

	/**
	* Returns the libero double3 of this articoli.
	*
	* @return the libero double3 of this articoli
	*/
	@Override
	public double getLiberoDouble3() {
		return _articoli.getLiberoDouble3();
	}

	/**
	* Sets the libero double3 of this articoli.
	*
	* @param liberoDouble3 the libero double3 of this articoli
	*/
	@Override
	public void setLiberoDouble3(double liberoDouble3) {
		_articoli.setLiberoDouble3(liberoDouble3);
	}

	/**
	* Returns the libero double4 of this articoli.
	*
	* @return the libero double4 of this articoli
	*/
	@Override
	public double getLiberoDouble4() {
		return _articoli.getLiberoDouble4();
	}

	/**
	* Sets the libero double4 of this articoli.
	*
	* @param liberoDouble4 the libero double4 of this articoli
	*/
	@Override
	public void setLiberoDouble4(double liberoDouble4) {
		_articoli.setLiberoDouble4(liberoDouble4);
	}

	/**
	* Returns the libero double5 of this articoli.
	*
	* @return the libero double5 of this articoli
	*/
	@Override
	public double getLiberoDouble5() {
		return _articoli.getLiberoDouble5();
	}

	/**
	* Sets the libero double5 of this articoli.
	*
	* @param liberoDouble5 the libero double5 of this articoli
	*/
	@Override
	public void setLiberoDouble5(double liberoDouble5) {
		_articoli.setLiberoDouble5(liberoDouble5);
	}

	@Override
	public boolean isNew() {
		return _articoli.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_articoli.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _articoli.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_articoli.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _articoli.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _articoli.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_articoli.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _articoli.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_articoli.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_articoli.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_articoli.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ArticoliWrapper((Articoli)_articoli.clone());
	}

	@Override
	public int compareTo(it.bysoftware.ct.model.Articoli articoli) {
		return _articoli.compareTo(articoli);
	}

	@Override
	public int hashCode() {
		return _articoli.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.Articoli> toCacheModel() {
		return _articoli.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.Articoli toEscapedModel() {
		return new ArticoliWrapper(_articoli.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.Articoli toUnescapedModel() {
		return new ArticoliWrapper(_articoli.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _articoli.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _articoli.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_articoli.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ArticoliWrapper)) {
			return false;
		}

		ArticoliWrapper articoliWrapper = (ArticoliWrapper)obj;

		if (Validator.equals(_articoli, articoliWrapper._articoli)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Articoli getWrappedArticoli() {
		return _articoli;
	}

	@Override
	public Articoli getWrappedModel() {
		return _articoli;
	}

	@Override
	public void resetOriginalValues() {
		_articoli.resetOriginalValues();
	}

	private Articoli _articoli;
}