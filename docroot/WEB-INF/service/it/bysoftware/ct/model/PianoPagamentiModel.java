/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

/**
 * The base model interface for the PianoPagamenti service. Represents a row in the &quot;PAGAME&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link it.bysoftware.ct.model.impl.PianoPagamentiModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link it.bysoftware.ct.model.impl.PianoPagamentiImpl}.
 * </p>
 *
 * @author Mario Torrisi
 * @see PianoPagamenti
 * @see it.bysoftware.ct.model.impl.PianoPagamentiImpl
 * @see it.bysoftware.ct.model.impl.PianoPagamentiModelImpl
 * @generated
 */
public interface PianoPagamentiModel extends BaseModel<PianoPagamenti> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a piano pagamenti model instance should use the {@link PianoPagamenti} interface instead.
	 */

	/**
	 * Returns the primary key of this piano pagamenti.
	 *
	 * @return the primary key of this piano pagamenti
	 */
	public String getPrimaryKey();

	/**
	 * Sets the primary key of this piano pagamenti.
	 *
	 * @param primaryKey the primary key of this piano pagamenti
	 */
	public void setPrimaryKey(String primaryKey);

	/**
	 * Returns the codice piano pagamento of this piano pagamenti.
	 *
	 * @return the codice piano pagamento of this piano pagamenti
	 */
	@AutoEscape
	public String getCodicePianoPagamento();

	/**
	 * Sets the codice piano pagamento of this piano pagamenti.
	 *
	 * @param codicePianoPagamento the codice piano pagamento of this piano pagamenti
	 */
	public void setCodicePianoPagamento(String codicePianoPagamento);

	/**
	 * Returns the descrizione of this piano pagamenti.
	 *
	 * @return the descrizione of this piano pagamenti
	 */
	@AutoEscape
	public String getDescrizione();

	/**
	 * Sets the descrizione of this piano pagamenti.
	 *
	 * @param descrizione the descrizione of this piano pagamenti
	 */
	public void setDescrizione(String descrizione);

	/**
	 * Returns the primo mese escluso of this piano pagamenti.
	 *
	 * @return the primo mese escluso of this piano pagamenti
	 */
	public int getPrimoMeseEscluso();

	/**
	 * Sets the primo mese escluso of this piano pagamenti.
	 *
	 * @param primoMeseEscluso the primo mese escluso of this piano pagamenti
	 */
	public void setPrimoMeseEscluso(int primoMeseEscluso);

	/**
	 * Returns the giorno primo mese succ of this piano pagamenti.
	 *
	 * @return the giorno primo mese succ of this piano pagamenti
	 */
	public int getGiornoPrimoMeseSucc();

	/**
	 * Sets the giorno primo mese succ of this piano pagamenti.
	 *
	 * @param giornoPrimoMeseSucc the giorno primo mese succ of this piano pagamenti
	 */
	public void setGiornoPrimoMeseSucc(int giornoPrimoMeseSucc);

	/**
	 * Returns the giorno secondo mese succ of this piano pagamenti.
	 *
	 * @return the giorno secondo mese succ of this piano pagamenti
	 */
	public int getGiornoSecondoMeseSucc();

	/**
	 * Sets the giorno secondo mese succ of this piano pagamenti.
	 *
	 * @param giornoSecondoMeseSucc the giorno secondo mese succ of this piano pagamenti
	 */
	public void setGiornoSecondoMeseSucc(int giornoSecondoMeseSucc);

	/**
	 * Returns the secondo mese escluso of this piano pagamenti.
	 *
	 * @return the secondo mese escluso of this piano pagamenti
	 */
	public int getSecondoMeseEscluso();

	/**
	 * Sets the secondo mese escluso of this piano pagamenti.
	 *
	 * @param secondoMeseEscluso the secondo mese escluso of this piano pagamenti
	 */
	public void setSecondoMeseEscluso(int secondoMeseEscluso);

	/**
	 * Returns the inizio fatt da bolla of this piano pagamenti.
	 *
	 * @return the inizio fatt da bolla of this piano pagamenti
	 */
	public boolean getInizioFattDaBolla();

	/**
	 * Returns <code>true</code> if this piano pagamenti is inizio fatt da bolla.
	 *
	 * @return <code>true</code> if this piano pagamenti is inizio fatt da bolla; <code>false</code> otherwise
	 */
	public boolean isInizioFattDaBolla();

	/**
	 * Sets whether this piano pagamenti is inizio fatt da bolla.
	 *
	 * @param inizioFattDaBolla the inizio fatt da bolla of this piano pagamenti
	 */
	public void setInizioFattDaBolla(boolean inizioFattDaBolla);

	/**
	 * Returns the perc sc chiusura of this piano pagamenti.
	 *
	 * @return the perc sc chiusura of this piano pagamenti
	 */
	public double getPercScChiusura();

	/**
	 * Sets the perc sc chiusura of this piano pagamenti.
	 *
	 * @param percScChiusura the perc sc chiusura of this piano pagamenti
	 */
	public void setPercScChiusura(double percScChiusura);

	/**
	 * Returns the perc sc cassa of this piano pagamenti.
	 *
	 * @return the perc sc cassa of this piano pagamenti
	 */
	public double getPercScCassa();

	/**
	 * Sets the perc sc cassa of this piano pagamenti.
	 *
	 * @param percScCassa the perc sc cassa of this piano pagamenti
	 */
	public void setPercScCassa(double percScCassa);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(it.bysoftware.ct.model.PianoPagamenti pianoPagamenti);

	@Override
	public int hashCode();

	@Override
	public CacheModel<it.bysoftware.ct.model.PianoPagamenti> toCacheModel();

	@Override
	public it.bysoftware.ct.model.PianoPagamenti toEscapedModel();

	@Override
	public it.bysoftware.ct.model.PianoPagamenti toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}