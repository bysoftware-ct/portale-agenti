/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.ListaMovimentiMagazzinoServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.ListaMovimentiMagazzinoServiceSoap
 * @generated
 */
public class ListaMovimentiMagazzinoSoap implements Serializable {
	public static ListaMovimentiMagazzinoSoap toSoapModel(
		ListaMovimentiMagazzino model) {
		ListaMovimentiMagazzinoSoap soapModel = new ListaMovimentiMagazzinoSoap();

		soapModel.setID(model.getID());
		soapModel.setCodiceArticolo(model.getCodiceArticolo());
		soapModel.setCodiceVariante(model.getCodiceVariante());
		soapModel.setDescrizione(model.getDescrizione());
		soapModel.setQuantita(model.getQuantita());
		soapModel.setTestCaricoScarico(model.getTestCaricoScarico());
		soapModel.setSoloValore(model.getSoloValore());

		return soapModel;
	}

	public static ListaMovimentiMagazzinoSoap[] toSoapModels(
		ListaMovimentiMagazzino[] models) {
		ListaMovimentiMagazzinoSoap[] soapModels = new ListaMovimentiMagazzinoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ListaMovimentiMagazzinoSoap[][] toSoapModels(
		ListaMovimentiMagazzino[][] models) {
		ListaMovimentiMagazzinoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ListaMovimentiMagazzinoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ListaMovimentiMagazzinoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ListaMovimentiMagazzinoSoap[] toSoapModels(
		List<ListaMovimentiMagazzino> models) {
		List<ListaMovimentiMagazzinoSoap> soapModels = new ArrayList<ListaMovimentiMagazzinoSoap>(models.size());

		for (ListaMovimentiMagazzino model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ListaMovimentiMagazzinoSoap[soapModels.size()]);
	}

	public ListaMovimentiMagazzinoSoap() {
	}

	public String getPrimaryKey() {
		return _ID;
	}

	public void setPrimaryKey(String pk) {
		setID(pk);
	}

	public String getID() {
		return _ID;
	}

	public void setID(String ID) {
		_ID = ID;
	}

	public String getCodiceArticolo() {
		return _codiceArticolo;
	}

	public void setCodiceArticolo(String codiceArticolo) {
		_codiceArticolo = codiceArticolo;
	}

	public String getCodiceVariante() {
		return _codiceVariante;
	}

	public void setCodiceVariante(String codiceVariante) {
		_codiceVariante = codiceVariante;
	}

	public String getDescrizione() {
		return _descrizione;
	}

	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;
	}

	public String getQuantita() {
		return _quantita;
	}

	public void setQuantita(String quantita) {
		_quantita = quantita;
	}

	public int getTestCaricoScarico() {
		return _testCaricoScarico;
	}

	public void setTestCaricoScarico(int testCaricoScarico) {
		_testCaricoScarico = testCaricoScarico;
	}

	public int getSoloValore() {
		return _soloValore;
	}

	public void setSoloValore(int soloValore) {
		_soloValore = soloValore;
	}

	private String _ID;
	private String _codiceArticolo;
	private String _codiceVariante;
	private String _descrizione;
	private String _quantita;
	private int _testCaricoScarico;
	private int _soloValore;
}