/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.bysoftware.ct.service.ClpSerializer;
import it.bysoftware.ct.service.DatiClientiFornitoriLocalServiceUtil;
import it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Torrisi
 */
public class DatiClientiFornitoriClp extends BaseModelImpl<DatiClientiFornitori>
	implements DatiClientiFornitori {
	public DatiClientiFornitoriClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return DatiClientiFornitori.class;
	}

	@Override
	public String getModelClassName() {
		return DatiClientiFornitori.class.getName();
	}

	@Override
	public DatiClientiFornitoriPK getPrimaryKey() {
		return new DatiClientiFornitoriPK(_tipoSoggetto, _codiceSoggetto);
	}

	@Override
	public void setPrimaryKey(DatiClientiFornitoriPK primaryKey) {
		setTipoSoggetto(primaryKey.tipoSoggetto);
		setCodiceSoggetto(primaryKey.codiceSoggetto);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new DatiClientiFornitoriPK(_tipoSoggetto, _codiceSoggetto);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((DatiClientiFornitoriPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceSoggetto", getCodiceSoggetto());
		attributes.put("codiceBanca", getCodiceBanca());
		attributes.put("codiceAgenzia", getCodiceAgenzia());
		attributes.put("codiceIdBanca", getCodiceIdBanca());
		attributes.put("codicePaese", getCodicePaese());
		attributes.put("codiceCINInt", getCodiceCINInt());
		attributes.put("codiceCINNaz", getCodiceCINNaz());
		attributes.put("numeroContoCorrente", getNumeroContoCorrente());
		attributes.put("IBAN", getIBAN());
		attributes.put("categoriaEconomica", getCategoriaEconomica());
		attributes.put("codiceEsenzioneIVA", getCodiceEsenzioneIVA());
		attributes.put("codiceDivisa", getCodiceDivisa());
		attributes.put("codicePagamento", getCodicePagamento());
		attributes.put("codiceZona", getCodiceZona());
		attributes.put("codiceAgente", getCodiceAgente());
		attributes.put("codiceGruppoAgente", getCodiceGruppoAgente());
		attributes.put("codiceCatProvv", getCodiceCatProvv());
		attributes.put("codiceSpedizione", getCodiceSpedizione());
		attributes.put("codicePorto", getCodicePorto());
		attributes.put("codiceVettore", getCodiceVettore());
		attributes.put("codiceDestDiversa", getCodiceDestDiversa());
		attributes.put("codiceListinoPrezzi", getCodiceListinoPrezzi());
		attributes.put("codiceScontoTotale", getCodiceScontoTotale());
		attributes.put("codiceScontoPreIVA", getCodiceScontoPreIVA());
		attributes.put("scontoCat1", getScontoCat1());
		attributes.put("scontoCat2", getScontoCat2());
		attributes.put("codiceLingua", getCodiceLingua());
		attributes.put("codiceLinguaEC", getCodiceLinguaEC());
		attributes.put("addebitoBolli", getAddebitoBolli());
		attributes.put("addebitoSpeseBanca", getAddebitoSpeseBanca());
		attributes.put("ragruppaBolle", getRagruppaBolle());
		attributes.put("sospensioneIVA", getSospensioneIVA());
		attributes.put("ragruppaOrdini", getRagruppaOrdini());
		attributes.put("ragruppaXDestinazione", getRagruppaXDestinazione());
		attributes.put("ragruppaXPorto", getRagruppaXPorto());
		attributes.put("percSpeseTrasporto", getPercSpeseTrasporto());
		attributes.put("prezzoDaProporre", getPrezzoDaProporre());
		attributes.put("sogettoFatturazione", getSogettoFatturazione());
		attributes.put("codiceRaggEffetti", getCodiceRaggEffetti());
		attributes.put("riportoRiferimenti", getRiportoRiferimenti());
		attributes.put("stampaPrezzo", getStampaPrezzo());
		attributes.put("bloccato", getBloccato());
		attributes.put("motivazione", getMotivazione());
		attributes.put("livelloBlocco", getLivelloBlocco());
		attributes.put("addebitoCONAI", getAddebitoCONAI());
		attributes.put("libStr1", getLibStr1());
		attributes.put("libStr2", getLibStr2());
		attributes.put("libStr3", getLibStr3());
		attributes.put("libStr4", getLibStr4());
		attributes.put("libStr5", getLibStr5());
		attributes.put("libDat1", getLibDat1());
		attributes.put("libDat2", getLibDat2());
		attributes.put("libDat3", getLibDat3());
		attributes.put("libDat4", getLibDat4());
		attributes.put("libDat5", getLibDat5());
		attributes.put("libLng1", getLibLng1());
		attributes.put("libLng2", getLibLng2());
		attributes.put("libLng3", getLibLng3());
		attributes.put("libLng4", getLibLng4());
		attributes.put("libLng5", getLibLng5());
		attributes.put("libDbl1", getLibDbl1());
		attributes.put("libDbl2", getLibDbl2());
		attributes.put("libDbl3", getLibDbl3());
		attributes.put("libDbl4", getLibDbl4());
		attributes.put("libDbl5", getLibDbl5());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceSoggetto = (String)attributes.get("codiceSoggetto");

		if (codiceSoggetto != null) {
			setCodiceSoggetto(codiceSoggetto);
		}

		String codiceBanca = (String)attributes.get("codiceBanca");

		if (codiceBanca != null) {
			setCodiceBanca(codiceBanca);
		}

		String codiceAgenzia = (String)attributes.get("codiceAgenzia");

		if (codiceAgenzia != null) {
			setCodiceAgenzia(codiceAgenzia);
		}

		String codiceIdBanca = (String)attributes.get("codiceIdBanca");

		if (codiceIdBanca != null) {
			setCodiceIdBanca(codiceIdBanca);
		}

		String codicePaese = (String)attributes.get("codicePaese");

		if (codicePaese != null) {
			setCodicePaese(codicePaese);
		}

		String codiceCINInt = (String)attributes.get("codiceCINInt");

		if (codiceCINInt != null) {
			setCodiceCINInt(codiceCINInt);
		}

		String codiceCINNaz = (String)attributes.get("codiceCINNaz");

		if (codiceCINNaz != null) {
			setCodiceCINNaz(codiceCINNaz);
		}

		String numeroContoCorrente = (String)attributes.get(
				"numeroContoCorrente");

		if (numeroContoCorrente != null) {
			setNumeroContoCorrente(numeroContoCorrente);
		}

		String IBAN = (String)attributes.get("IBAN");

		if (IBAN != null) {
			setIBAN(IBAN);
		}

		String categoriaEconomica = (String)attributes.get("categoriaEconomica");

		if (categoriaEconomica != null) {
			setCategoriaEconomica(categoriaEconomica);
		}

		String codiceEsenzioneIVA = (String)attributes.get("codiceEsenzioneIVA");

		if (codiceEsenzioneIVA != null) {
			setCodiceEsenzioneIVA(codiceEsenzioneIVA);
		}

		String codiceDivisa = (String)attributes.get("codiceDivisa");

		if (codiceDivisa != null) {
			setCodiceDivisa(codiceDivisa);
		}

		String codicePagamento = (String)attributes.get("codicePagamento");

		if (codicePagamento != null) {
			setCodicePagamento(codicePagamento);
		}

		String codiceZona = (String)attributes.get("codiceZona");

		if (codiceZona != null) {
			setCodiceZona(codiceZona);
		}

		String codiceAgente = (String)attributes.get("codiceAgente");

		if (codiceAgente != null) {
			setCodiceAgente(codiceAgente);
		}

		String codiceGruppoAgente = (String)attributes.get("codiceGruppoAgente");

		if (codiceGruppoAgente != null) {
			setCodiceGruppoAgente(codiceGruppoAgente);
		}

		String codiceCatProvv = (String)attributes.get("codiceCatProvv");

		if (codiceCatProvv != null) {
			setCodiceCatProvv(codiceCatProvv);
		}

		String codiceSpedizione = (String)attributes.get("codiceSpedizione");

		if (codiceSpedizione != null) {
			setCodiceSpedizione(codiceSpedizione);
		}

		String codicePorto = (String)attributes.get("codicePorto");

		if (codicePorto != null) {
			setCodicePorto(codicePorto);
		}

		String codiceVettore = (String)attributes.get("codiceVettore");

		if (codiceVettore != null) {
			setCodiceVettore(codiceVettore);
		}

		String codiceDestDiversa = (String)attributes.get("codiceDestDiversa");

		if (codiceDestDiversa != null) {
			setCodiceDestDiversa(codiceDestDiversa);
		}

		String codiceListinoPrezzi = (String)attributes.get(
				"codiceListinoPrezzi");

		if (codiceListinoPrezzi != null) {
			setCodiceListinoPrezzi(codiceListinoPrezzi);
		}

		String codiceScontoTotale = (String)attributes.get("codiceScontoTotale");

		if (codiceScontoTotale != null) {
			setCodiceScontoTotale(codiceScontoTotale);
		}

		String codiceScontoPreIVA = (String)attributes.get("codiceScontoPreIVA");

		if (codiceScontoPreIVA != null) {
			setCodiceScontoPreIVA(codiceScontoPreIVA);
		}

		String scontoCat1 = (String)attributes.get("scontoCat1");

		if (scontoCat1 != null) {
			setScontoCat1(scontoCat1);
		}

		String scontoCat2 = (String)attributes.get("scontoCat2");

		if (scontoCat2 != null) {
			setScontoCat2(scontoCat2);
		}

		String codiceLingua = (String)attributes.get("codiceLingua");

		if (codiceLingua != null) {
			setCodiceLingua(codiceLingua);
		}

		String codiceLinguaEC = (String)attributes.get("codiceLinguaEC");

		if (codiceLinguaEC != null) {
			setCodiceLinguaEC(codiceLinguaEC);
		}

		Boolean addebitoBolli = (Boolean)attributes.get("addebitoBolli");

		if (addebitoBolli != null) {
			setAddebitoBolli(addebitoBolli);
		}

		Boolean addebitoSpeseBanca = (Boolean)attributes.get(
				"addebitoSpeseBanca");

		if (addebitoSpeseBanca != null) {
			setAddebitoSpeseBanca(addebitoSpeseBanca);
		}

		Boolean ragruppaBolle = (Boolean)attributes.get("ragruppaBolle");

		if (ragruppaBolle != null) {
			setRagruppaBolle(ragruppaBolle);
		}

		Boolean sospensioneIVA = (Boolean)attributes.get("sospensioneIVA");

		if (sospensioneIVA != null) {
			setSospensioneIVA(sospensioneIVA);
		}

		Integer ragruppaOrdini = (Integer)attributes.get("ragruppaOrdini");

		if (ragruppaOrdini != null) {
			setRagruppaOrdini(ragruppaOrdini);
		}

		Boolean ragruppaXDestinazione = (Boolean)attributes.get(
				"ragruppaXDestinazione");

		if (ragruppaXDestinazione != null) {
			setRagruppaXDestinazione(ragruppaXDestinazione);
		}

		Boolean ragruppaXPorto = (Boolean)attributes.get("ragruppaXPorto");

		if (ragruppaXPorto != null) {
			setRagruppaXPorto(ragruppaXPorto);
		}

		Double percSpeseTrasporto = (Double)attributes.get("percSpeseTrasporto");

		if (percSpeseTrasporto != null) {
			setPercSpeseTrasporto(percSpeseTrasporto);
		}

		Integer prezzoDaProporre = (Integer)attributes.get("prezzoDaProporre");

		if (prezzoDaProporre != null) {
			setPrezzoDaProporre(prezzoDaProporre);
		}

		String sogettoFatturazione = (String)attributes.get(
				"sogettoFatturazione");

		if (sogettoFatturazione != null) {
			setSogettoFatturazione(sogettoFatturazione);
		}

		String codiceRaggEffetti = (String)attributes.get("codiceRaggEffetti");

		if (codiceRaggEffetti != null) {
			setCodiceRaggEffetti(codiceRaggEffetti);
		}

		Integer riportoRiferimenti = (Integer)attributes.get(
				"riportoRiferimenti");

		if (riportoRiferimenti != null) {
			setRiportoRiferimenti(riportoRiferimenti);
		}

		Boolean stampaPrezzo = (Boolean)attributes.get("stampaPrezzo");

		if (stampaPrezzo != null) {
			setStampaPrezzo(stampaPrezzo);
		}

		Integer bloccato = (Integer)attributes.get("bloccato");

		if (bloccato != null) {
			setBloccato(bloccato);
		}

		String motivazione = (String)attributes.get("motivazione");

		if (motivazione != null) {
			setMotivazione(motivazione);
		}

		Integer livelloBlocco = (Integer)attributes.get("livelloBlocco");

		if (livelloBlocco != null) {
			setLivelloBlocco(livelloBlocco);
		}

		Boolean addebitoCONAI = (Boolean)attributes.get("addebitoCONAI");

		if (addebitoCONAI != null) {
			setAddebitoCONAI(addebitoCONAI);
		}

		String libStr1 = (String)attributes.get("libStr1");

		if (libStr1 != null) {
			setLibStr1(libStr1);
		}

		String libStr2 = (String)attributes.get("libStr2");

		if (libStr2 != null) {
			setLibStr2(libStr2);
		}

		String libStr3 = (String)attributes.get("libStr3");

		if (libStr3 != null) {
			setLibStr3(libStr3);
		}

		String libStr4 = (String)attributes.get("libStr4");

		if (libStr4 != null) {
			setLibStr4(libStr4);
		}

		String libStr5 = (String)attributes.get("libStr5");

		if (libStr5 != null) {
			setLibStr5(libStr5);
		}

		Date libDat1 = (Date)attributes.get("libDat1");

		if (libDat1 != null) {
			setLibDat1(libDat1);
		}

		Date libDat2 = (Date)attributes.get("libDat2");

		if (libDat2 != null) {
			setLibDat2(libDat2);
		}

		Date libDat3 = (Date)attributes.get("libDat3");

		if (libDat3 != null) {
			setLibDat3(libDat3);
		}

		Date libDat4 = (Date)attributes.get("libDat4");

		if (libDat4 != null) {
			setLibDat4(libDat4);
		}

		Date libDat5 = (Date)attributes.get("libDat5");

		if (libDat5 != null) {
			setLibDat5(libDat5);
		}

		Long libLng1 = (Long)attributes.get("libLng1");

		if (libLng1 != null) {
			setLibLng1(libLng1);
		}

		Long libLng2 = (Long)attributes.get("libLng2");

		if (libLng2 != null) {
			setLibLng2(libLng2);
		}

		Long libLng3 = (Long)attributes.get("libLng3");

		if (libLng3 != null) {
			setLibLng3(libLng3);
		}

		Long libLng4 = (Long)attributes.get("libLng4");

		if (libLng4 != null) {
			setLibLng4(libLng4);
		}

		Long libLng5 = (Long)attributes.get("libLng5");

		if (libLng5 != null) {
			setLibLng5(libLng5);
		}

		Double libDbl1 = (Double)attributes.get("libDbl1");

		if (libDbl1 != null) {
			setLibDbl1(libDbl1);
		}

		Double libDbl2 = (Double)attributes.get("libDbl2");

		if (libDbl2 != null) {
			setLibDbl2(libDbl2);
		}

		Double libDbl3 = (Double)attributes.get("libDbl3");

		if (libDbl3 != null) {
			setLibDbl3(libDbl3);
		}

		Double libDbl4 = (Double)attributes.get("libDbl4");

		if (libDbl4 != null) {
			setLibDbl4(libDbl4);
		}

		Double libDbl5 = (Double)attributes.get("libDbl5");

		if (libDbl5 != null) {
			setLibDbl5(libDbl5);
		}
	}

	@Override
	public boolean getTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public boolean isTipoSoggetto() {
		return _tipoSoggetto;
	}

	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_tipoSoggetto = tipoSoggetto;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoSoggetto", boolean.class);

				method.invoke(_datiClientiFornitoriRemoteModel, tipoSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceSoggetto() {
		return _codiceSoggetto;
	}

	@Override
	public void setCodiceSoggetto(String codiceSoggetto) {
		_codiceSoggetto = codiceSoggetto;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceSoggetto",
						String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codiceSoggetto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceBanca() {
		return _codiceBanca;
	}

	@Override
	public void setCodiceBanca(String codiceBanca) {
		_codiceBanca = codiceBanca;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceBanca", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codiceBanca);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceAgenzia() {
		return _codiceAgenzia;
	}

	@Override
	public void setCodiceAgenzia(String codiceAgenzia) {
		_codiceAgenzia = codiceAgenzia;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceAgenzia", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codiceAgenzia);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceIdBanca() {
		return _codiceIdBanca;
	}

	@Override
	public void setCodiceIdBanca(String codiceIdBanca) {
		_codiceIdBanca = codiceIdBanca;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceIdBanca", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codiceIdBanca);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodicePaese() {
		return _codicePaese;
	}

	@Override
	public void setCodicePaese(String codicePaese) {
		_codicePaese = codicePaese;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodicePaese", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codicePaese);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCINInt() {
		return _codiceCINInt;
	}

	@Override
	public void setCodiceCINInt(String codiceCINInt) {
		_codiceCINInt = codiceCINInt;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCINInt", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codiceCINInt);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCINNaz() {
		return _codiceCINNaz;
	}

	@Override
	public void setCodiceCINNaz(String codiceCINNaz) {
		_codiceCINNaz = codiceCINNaz;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCINNaz", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codiceCINNaz);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNumeroContoCorrente() {
		return _numeroContoCorrente;
	}

	@Override
	public void setNumeroContoCorrente(String numeroContoCorrente) {
		_numeroContoCorrente = numeroContoCorrente;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setNumeroContoCorrente",
						String.class);

				method.invoke(_datiClientiFornitoriRemoteModel,
					numeroContoCorrente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIBAN() {
		return _IBAN;
	}

	@Override
	public void setIBAN(String IBAN) {
		_IBAN = IBAN;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setIBAN", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, IBAN);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCategoriaEconomica() {
		return _categoriaEconomica;
	}

	@Override
	public void setCategoriaEconomica(String categoriaEconomica) {
		_categoriaEconomica = categoriaEconomica;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCategoriaEconomica",
						String.class);

				method.invoke(_datiClientiFornitoriRemoteModel,
					categoriaEconomica);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceEsenzioneIVA() {
		return _codiceEsenzioneIVA;
	}

	@Override
	public void setCodiceEsenzioneIVA(String codiceEsenzioneIVA) {
		_codiceEsenzioneIVA = codiceEsenzioneIVA;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceEsenzioneIVA",
						String.class);

				method.invoke(_datiClientiFornitoriRemoteModel,
					codiceEsenzioneIVA);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDivisa() {
		return _codiceDivisa;
	}

	@Override
	public void setCodiceDivisa(String codiceDivisa) {
		_codiceDivisa = codiceDivisa;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDivisa", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codiceDivisa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodicePagamento() {
		return _codicePagamento;
	}

	@Override
	public void setCodicePagamento(String codicePagamento) {
		_codicePagamento = codicePagamento;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodicePagamento",
						String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codicePagamento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceZona() {
		return _codiceZona;
	}

	@Override
	public void setCodiceZona(String codiceZona) {
		_codiceZona = codiceZona;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceZona", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codiceZona);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceAgente() {
		return _codiceAgente;
	}

	@Override
	public void setCodiceAgente(String codiceAgente) {
		_codiceAgente = codiceAgente;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceAgente", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codiceAgente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceGruppoAgente() {
		return _codiceGruppoAgente;
	}

	@Override
	public void setCodiceGruppoAgente(String codiceGruppoAgente) {
		_codiceGruppoAgente = codiceGruppoAgente;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceGruppoAgente",
						String.class);

				method.invoke(_datiClientiFornitoriRemoteModel,
					codiceGruppoAgente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceCatProvv() {
		return _codiceCatProvv;
	}

	@Override
	public void setCodiceCatProvv(String codiceCatProvv) {
		_codiceCatProvv = codiceCatProvv;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceCatProvv",
						String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codiceCatProvv);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceSpedizione() {
		return _codiceSpedizione;
	}

	@Override
	public void setCodiceSpedizione(String codiceSpedizione) {
		_codiceSpedizione = codiceSpedizione;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceSpedizione",
						String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codiceSpedizione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodicePorto() {
		return _codicePorto;
	}

	@Override
	public void setCodicePorto(String codicePorto) {
		_codicePorto = codicePorto;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodicePorto", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codicePorto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceVettore() {
		return _codiceVettore;
	}

	@Override
	public void setCodiceVettore(String codiceVettore) {
		_codiceVettore = codiceVettore;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceVettore", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codiceVettore);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceDestDiversa() {
		return _codiceDestDiversa;
	}

	@Override
	public void setCodiceDestDiversa(String codiceDestDiversa) {
		_codiceDestDiversa = codiceDestDiversa;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceDestDiversa",
						String.class);

				method.invoke(_datiClientiFornitoriRemoteModel,
					codiceDestDiversa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceListinoPrezzi() {
		return _codiceListinoPrezzi;
	}

	@Override
	public void setCodiceListinoPrezzi(String codiceListinoPrezzi) {
		_codiceListinoPrezzi = codiceListinoPrezzi;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceListinoPrezzi",
						String.class);

				method.invoke(_datiClientiFornitoriRemoteModel,
					codiceListinoPrezzi);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceScontoTotale() {
		return _codiceScontoTotale;
	}

	@Override
	public void setCodiceScontoTotale(String codiceScontoTotale) {
		_codiceScontoTotale = codiceScontoTotale;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceScontoTotale",
						String.class);

				method.invoke(_datiClientiFornitoriRemoteModel,
					codiceScontoTotale);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceScontoPreIVA() {
		return _codiceScontoPreIVA;
	}

	@Override
	public void setCodiceScontoPreIVA(String codiceScontoPreIVA) {
		_codiceScontoPreIVA = codiceScontoPreIVA;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceScontoPreIVA",
						String.class);

				method.invoke(_datiClientiFornitoriRemoteModel,
					codiceScontoPreIVA);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getScontoCat1() {
		return _scontoCat1;
	}

	@Override
	public void setScontoCat1(String scontoCat1) {
		_scontoCat1 = scontoCat1;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setScontoCat1", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, scontoCat1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getScontoCat2() {
		return _scontoCat2;
	}

	@Override
	public void setScontoCat2(String scontoCat2) {
		_scontoCat2 = scontoCat2;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setScontoCat2", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, scontoCat2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceLingua() {
		return _codiceLingua;
	}

	@Override
	public void setCodiceLingua(String codiceLingua) {
		_codiceLingua = codiceLingua;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceLingua", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codiceLingua);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceLinguaEC() {
		return _codiceLinguaEC;
	}

	@Override
	public void setCodiceLinguaEC(String codiceLinguaEC) {
		_codiceLinguaEC = codiceLinguaEC;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceLinguaEC",
						String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, codiceLinguaEC);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getAddebitoBolli() {
		return _addebitoBolli;
	}

	@Override
	public boolean isAddebitoBolli() {
		return _addebitoBolli;
	}

	@Override
	public void setAddebitoBolli(boolean addebitoBolli) {
		_addebitoBolli = addebitoBolli;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setAddebitoBolli",
						boolean.class);

				method.invoke(_datiClientiFornitoriRemoteModel, addebitoBolli);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getAddebitoSpeseBanca() {
		return _addebitoSpeseBanca;
	}

	@Override
	public boolean isAddebitoSpeseBanca() {
		return _addebitoSpeseBanca;
	}

	@Override
	public void setAddebitoSpeseBanca(boolean addebitoSpeseBanca) {
		_addebitoSpeseBanca = addebitoSpeseBanca;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setAddebitoSpeseBanca",
						boolean.class);

				method.invoke(_datiClientiFornitoriRemoteModel,
					addebitoSpeseBanca);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getRagruppaBolle() {
		return _ragruppaBolle;
	}

	@Override
	public boolean isRagruppaBolle() {
		return _ragruppaBolle;
	}

	@Override
	public void setRagruppaBolle(boolean ragruppaBolle) {
		_ragruppaBolle = ragruppaBolle;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setRagruppaBolle",
						boolean.class);

				method.invoke(_datiClientiFornitoriRemoteModel, ragruppaBolle);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getSospensioneIVA() {
		return _sospensioneIVA;
	}

	@Override
	public boolean isSospensioneIVA() {
		return _sospensioneIVA;
	}

	@Override
	public void setSospensioneIVA(boolean sospensioneIVA) {
		_sospensioneIVA = sospensioneIVA;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setSospensioneIVA",
						boolean.class);

				method.invoke(_datiClientiFornitoriRemoteModel, sospensioneIVA);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getRagruppaOrdini() {
		return _ragruppaOrdini;
	}

	@Override
	public void setRagruppaOrdini(int ragruppaOrdini) {
		_ragruppaOrdini = ragruppaOrdini;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setRagruppaOrdini", int.class);

				method.invoke(_datiClientiFornitoriRemoteModel, ragruppaOrdini);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getRagruppaXDestinazione() {
		return _ragruppaXDestinazione;
	}

	@Override
	public boolean isRagruppaXDestinazione() {
		return _ragruppaXDestinazione;
	}

	@Override
	public void setRagruppaXDestinazione(boolean ragruppaXDestinazione) {
		_ragruppaXDestinazione = ragruppaXDestinazione;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setRagruppaXDestinazione",
						boolean.class);

				method.invoke(_datiClientiFornitoriRemoteModel,
					ragruppaXDestinazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getRagruppaXPorto() {
		return _ragruppaXPorto;
	}

	@Override
	public boolean isRagruppaXPorto() {
		return _ragruppaXPorto;
	}

	@Override
	public void setRagruppaXPorto(boolean ragruppaXPorto) {
		_ragruppaXPorto = ragruppaXPorto;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setRagruppaXPorto",
						boolean.class);

				method.invoke(_datiClientiFornitoriRemoteModel, ragruppaXPorto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPercSpeseTrasporto() {
		return _percSpeseTrasporto;
	}

	@Override
	public void setPercSpeseTrasporto(double percSpeseTrasporto) {
		_percSpeseTrasporto = percSpeseTrasporto;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setPercSpeseTrasporto",
						double.class);

				method.invoke(_datiClientiFornitoriRemoteModel,
					percSpeseTrasporto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getPrezzoDaProporre() {
		return _prezzoDaProporre;
	}

	@Override
	public void setPrezzoDaProporre(int prezzoDaProporre) {
		_prezzoDaProporre = prezzoDaProporre;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setPrezzoDaProporre", int.class);

				method.invoke(_datiClientiFornitoriRemoteModel, prezzoDaProporre);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getSogettoFatturazione() {
		return _sogettoFatturazione;
	}

	@Override
	public void setSogettoFatturazione(String sogettoFatturazione) {
		_sogettoFatturazione = sogettoFatturazione;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setSogettoFatturazione",
						String.class);

				method.invoke(_datiClientiFornitoriRemoteModel,
					sogettoFatturazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodiceRaggEffetti() {
		return _codiceRaggEffetti;
	}

	@Override
	public void setCodiceRaggEffetti(String codiceRaggEffetti) {
		_codiceRaggEffetti = codiceRaggEffetti;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setCodiceRaggEffetti",
						String.class);

				method.invoke(_datiClientiFornitoriRemoteModel,
					codiceRaggEffetti);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getRiportoRiferimenti() {
		return _riportoRiferimenti;
	}

	@Override
	public void setRiportoRiferimenti(int riportoRiferimenti) {
		_riportoRiferimenti = riportoRiferimenti;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setRiportoRiferimenti",
						int.class);

				method.invoke(_datiClientiFornitoriRemoteModel,
					riportoRiferimenti);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getStampaPrezzo() {
		return _stampaPrezzo;
	}

	@Override
	public boolean isStampaPrezzo() {
		return _stampaPrezzo;
	}

	@Override
	public void setStampaPrezzo(boolean stampaPrezzo) {
		_stampaPrezzo = stampaPrezzo;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setStampaPrezzo", boolean.class);

				method.invoke(_datiClientiFornitoriRemoteModel, stampaPrezzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getBloccato() {
		return _bloccato;
	}

	@Override
	public void setBloccato(int bloccato) {
		_bloccato = bloccato;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setBloccato", int.class);

				method.invoke(_datiClientiFornitoriRemoteModel, bloccato);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMotivazione() {
		return _motivazione;
	}

	@Override
	public void setMotivazione(String motivazione) {
		_motivazione = motivazione;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setMotivazione", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, motivazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getLivelloBlocco() {
		return _livelloBlocco;
	}

	@Override
	public void setLivelloBlocco(int livelloBlocco) {
		_livelloBlocco = livelloBlocco;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLivelloBlocco", int.class);

				method.invoke(_datiClientiFornitoriRemoteModel, livelloBlocco);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getAddebitoCONAI() {
		return _addebitoCONAI;
	}

	@Override
	public boolean isAddebitoCONAI() {
		return _addebitoCONAI;
	}

	@Override
	public void setAddebitoCONAI(boolean addebitoCONAI) {
		_addebitoCONAI = addebitoCONAI;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setAddebitoCONAI",
						boolean.class);

				method.invoke(_datiClientiFornitoriRemoteModel, addebitoCONAI);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr1() {
		return _libStr1;
	}

	@Override
	public void setLibStr1(String libStr1) {
		_libStr1 = libStr1;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr1", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libStr1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr2() {
		return _libStr2;
	}

	@Override
	public void setLibStr2(String libStr2) {
		_libStr2 = libStr2;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr2", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libStr2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr3() {
		return _libStr3;
	}

	@Override
	public void setLibStr3(String libStr3) {
		_libStr3 = libStr3;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr3", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libStr3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr4() {
		return _libStr4;
	}

	@Override
	public void setLibStr4(String libStr4) {
		_libStr4 = libStr4;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr4", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libStr4);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLibStr5() {
		return _libStr5;
	}

	@Override
	public void setLibStr5(String libStr5) {
		_libStr5 = libStr5;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibStr5", String.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libStr5);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat1() {
		return _libDat1;
	}

	@Override
	public void setLibDat1(Date libDat1) {
		_libDat1 = libDat1;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat1", Date.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libDat1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat2() {
		return _libDat2;
	}

	@Override
	public void setLibDat2(Date libDat2) {
		_libDat2 = libDat2;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat2", Date.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libDat2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat3() {
		return _libDat3;
	}

	@Override
	public void setLibDat3(Date libDat3) {
		_libDat3 = libDat3;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat3", Date.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libDat3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat4() {
		return _libDat4;
	}

	@Override
	public void setLibDat4(Date libDat4) {
		_libDat4 = libDat4;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat4", Date.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libDat4);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getLibDat5() {
		return _libDat5;
	}

	@Override
	public void setLibDat5(Date libDat5) {
		_libDat5 = libDat5;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDat5", Date.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libDat5);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng1() {
		return _libLng1;
	}

	@Override
	public void setLibLng1(long libLng1) {
		_libLng1 = libLng1;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng1", long.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libLng1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng2() {
		return _libLng2;
	}

	@Override
	public void setLibLng2(long libLng2) {
		_libLng2 = libLng2;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng2", long.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libLng2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng3() {
		return _libLng3;
	}

	@Override
	public void setLibLng3(long libLng3) {
		_libLng3 = libLng3;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng3", long.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libLng3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng4() {
		return _libLng4;
	}

	@Override
	public void setLibLng4(long libLng4) {
		_libLng4 = libLng4;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng4", long.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libLng4);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLibLng5() {
		return _libLng5;
	}

	@Override
	public void setLibLng5(long libLng5) {
		_libLng5 = libLng5;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibLng5", long.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libLng5);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl1() {
		return _libDbl1;
	}

	@Override
	public void setLibDbl1(double libDbl1) {
		_libDbl1 = libDbl1;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl1", double.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libDbl1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl2() {
		return _libDbl2;
	}

	@Override
	public void setLibDbl2(double libDbl2) {
		_libDbl2 = libDbl2;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl2", double.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libDbl2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl3() {
		return _libDbl3;
	}

	@Override
	public void setLibDbl3(double libDbl3) {
		_libDbl3 = libDbl3;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl3", double.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libDbl3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl4() {
		return _libDbl4;
	}

	@Override
	public void setLibDbl4(double libDbl4) {
		_libDbl4 = libDbl4;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl4", double.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libDbl4);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getLibDbl5() {
		return _libDbl5;
	}

	@Override
	public void setLibDbl5(double libDbl5) {
		_libDbl5 = libDbl5;

		if (_datiClientiFornitoriRemoteModel != null) {
			try {
				Class<?> clazz = _datiClientiFornitoriRemoteModel.getClass();

				Method method = clazz.getMethod("setLibDbl5", double.class);

				method.invoke(_datiClientiFornitoriRemoteModel, libDbl5);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getDatiClientiFornitoriRemoteModel() {
		return _datiClientiFornitoriRemoteModel;
	}

	public void setDatiClientiFornitoriRemoteModel(
		BaseModel<?> datiClientiFornitoriRemoteModel) {
		_datiClientiFornitoriRemoteModel = datiClientiFornitoriRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _datiClientiFornitoriRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_datiClientiFornitoriRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			DatiClientiFornitoriLocalServiceUtil.addDatiClientiFornitori(this);
		}
		else {
			DatiClientiFornitoriLocalServiceUtil.updateDatiClientiFornitori(this);
		}
	}

	@Override
	public DatiClientiFornitori toEscapedModel() {
		return (DatiClientiFornitori)ProxyUtil.newProxyInstance(DatiClientiFornitori.class.getClassLoader(),
			new Class[] { DatiClientiFornitori.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		DatiClientiFornitoriClp clone = new DatiClientiFornitoriClp();

		clone.setTipoSoggetto(getTipoSoggetto());
		clone.setCodiceSoggetto(getCodiceSoggetto());
		clone.setCodiceBanca(getCodiceBanca());
		clone.setCodiceAgenzia(getCodiceAgenzia());
		clone.setCodiceIdBanca(getCodiceIdBanca());
		clone.setCodicePaese(getCodicePaese());
		clone.setCodiceCINInt(getCodiceCINInt());
		clone.setCodiceCINNaz(getCodiceCINNaz());
		clone.setNumeroContoCorrente(getNumeroContoCorrente());
		clone.setIBAN(getIBAN());
		clone.setCategoriaEconomica(getCategoriaEconomica());
		clone.setCodiceEsenzioneIVA(getCodiceEsenzioneIVA());
		clone.setCodiceDivisa(getCodiceDivisa());
		clone.setCodicePagamento(getCodicePagamento());
		clone.setCodiceZona(getCodiceZona());
		clone.setCodiceAgente(getCodiceAgente());
		clone.setCodiceGruppoAgente(getCodiceGruppoAgente());
		clone.setCodiceCatProvv(getCodiceCatProvv());
		clone.setCodiceSpedizione(getCodiceSpedizione());
		clone.setCodicePorto(getCodicePorto());
		clone.setCodiceVettore(getCodiceVettore());
		clone.setCodiceDestDiversa(getCodiceDestDiversa());
		clone.setCodiceListinoPrezzi(getCodiceListinoPrezzi());
		clone.setCodiceScontoTotale(getCodiceScontoTotale());
		clone.setCodiceScontoPreIVA(getCodiceScontoPreIVA());
		clone.setScontoCat1(getScontoCat1());
		clone.setScontoCat2(getScontoCat2());
		clone.setCodiceLingua(getCodiceLingua());
		clone.setCodiceLinguaEC(getCodiceLinguaEC());
		clone.setAddebitoBolli(getAddebitoBolli());
		clone.setAddebitoSpeseBanca(getAddebitoSpeseBanca());
		clone.setRagruppaBolle(getRagruppaBolle());
		clone.setSospensioneIVA(getSospensioneIVA());
		clone.setRagruppaOrdini(getRagruppaOrdini());
		clone.setRagruppaXDestinazione(getRagruppaXDestinazione());
		clone.setRagruppaXPorto(getRagruppaXPorto());
		clone.setPercSpeseTrasporto(getPercSpeseTrasporto());
		clone.setPrezzoDaProporre(getPrezzoDaProporre());
		clone.setSogettoFatturazione(getSogettoFatturazione());
		clone.setCodiceRaggEffetti(getCodiceRaggEffetti());
		clone.setRiportoRiferimenti(getRiportoRiferimenti());
		clone.setStampaPrezzo(getStampaPrezzo());
		clone.setBloccato(getBloccato());
		clone.setMotivazione(getMotivazione());
		clone.setLivelloBlocco(getLivelloBlocco());
		clone.setAddebitoCONAI(getAddebitoCONAI());
		clone.setLibStr1(getLibStr1());
		clone.setLibStr2(getLibStr2());
		clone.setLibStr3(getLibStr3());
		clone.setLibStr4(getLibStr4());
		clone.setLibStr5(getLibStr5());
		clone.setLibDat1(getLibDat1());
		clone.setLibDat2(getLibDat2());
		clone.setLibDat3(getLibDat3());
		clone.setLibDat4(getLibDat4());
		clone.setLibDat5(getLibDat5());
		clone.setLibLng1(getLibLng1());
		clone.setLibLng2(getLibLng2());
		clone.setLibLng3(getLibLng3());
		clone.setLibLng4(getLibLng4());
		clone.setLibLng5(getLibLng5());
		clone.setLibDbl1(getLibDbl1());
		clone.setLibDbl2(getLibDbl2());
		clone.setLibDbl3(getLibDbl3());
		clone.setLibDbl4(getLibDbl4());
		clone.setLibDbl5(getLibDbl5());

		return clone;
	}

	@Override
	public int compareTo(DatiClientiFornitori datiClientiFornitori) {
		DatiClientiFornitoriPK primaryKey = datiClientiFornitori.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DatiClientiFornitoriClp)) {
			return false;
		}

		DatiClientiFornitoriClp datiClientiFornitori = (DatiClientiFornitoriClp)obj;

		DatiClientiFornitoriPK primaryKey = datiClientiFornitori.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(133);

		sb.append("{tipoSoggetto=");
		sb.append(getTipoSoggetto());
		sb.append(", codiceSoggetto=");
		sb.append(getCodiceSoggetto());
		sb.append(", codiceBanca=");
		sb.append(getCodiceBanca());
		sb.append(", codiceAgenzia=");
		sb.append(getCodiceAgenzia());
		sb.append(", codiceIdBanca=");
		sb.append(getCodiceIdBanca());
		sb.append(", codicePaese=");
		sb.append(getCodicePaese());
		sb.append(", codiceCINInt=");
		sb.append(getCodiceCINInt());
		sb.append(", codiceCINNaz=");
		sb.append(getCodiceCINNaz());
		sb.append(", numeroContoCorrente=");
		sb.append(getNumeroContoCorrente());
		sb.append(", IBAN=");
		sb.append(getIBAN());
		sb.append(", categoriaEconomica=");
		sb.append(getCategoriaEconomica());
		sb.append(", codiceEsenzioneIVA=");
		sb.append(getCodiceEsenzioneIVA());
		sb.append(", codiceDivisa=");
		sb.append(getCodiceDivisa());
		sb.append(", codicePagamento=");
		sb.append(getCodicePagamento());
		sb.append(", codiceZona=");
		sb.append(getCodiceZona());
		sb.append(", codiceAgente=");
		sb.append(getCodiceAgente());
		sb.append(", codiceGruppoAgente=");
		sb.append(getCodiceGruppoAgente());
		sb.append(", codiceCatProvv=");
		sb.append(getCodiceCatProvv());
		sb.append(", codiceSpedizione=");
		sb.append(getCodiceSpedizione());
		sb.append(", codicePorto=");
		sb.append(getCodicePorto());
		sb.append(", codiceVettore=");
		sb.append(getCodiceVettore());
		sb.append(", codiceDestDiversa=");
		sb.append(getCodiceDestDiversa());
		sb.append(", codiceListinoPrezzi=");
		sb.append(getCodiceListinoPrezzi());
		sb.append(", codiceScontoTotale=");
		sb.append(getCodiceScontoTotale());
		sb.append(", codiceScontoPreIVA=");
		sb.append(getCodiceScontoPreIVA());
		sb.append(", scontoCat1=");
		sb.append(getScontoCat1());
		sb.append(", scontoCat2=");
		sb.append(getScontoCat2());
		sb.append(", codiceLingua=");
		sb.append(getCodiceLingua());
		sb.append(", codiceLinguaEC=");
		sb.append(getCodiceLinguaEC());
		sb.append(", addebitoBolli=");
		sb.append(getAddebitoBolli());
		sb.append(", addebitoSpeseBanca=");
		sb.append(getAddebitoSpeseBanca());
		sb.append(", ragruppaBolle=");
		sb.append(getRagruppaBolle());
		sb.append(", sospensioneIVA=");
		sb.append(getSospensioneIVA());
		sb.append(", ragruppaOrdini=");
		sb.append(getRagruppaOrdini());
		sb.append(", ragruppaXDestinazione=");
		sb.append(getRagruppaXDestinazione());
		sb.append(", ragruppaXPorto=");
		sb.append(getRagruppaXPorto());
		sb.append(", percSpeseTrasporto=");
		sb.append(getPercSpeseTrasporto());
		sb.append(", prezzoDaProporre=");
		sb.append(getPrezzoDaProporre());
		sb.append(", sogettoFatturazione=");
		sb.append(getSogettoFatturazione());
		sb.append(", codiceRaggEffetti=");
		sb.append(getCodiceRaggEffetti());
		sb.append(", riportoRiferimenti=");
		sb.append(getRiportoRiferimenti());
		sb.append(", stampaPrezzo=");
		sb.append(getStampaPrezzo());
		sb.append(", bloccato=");
		sb.append(getBloccato());
		sb.append(", motivazione=");
		sb.append(getMotivazione());
		sb.append(", livelloBlocco=");
		sb.append(getLivelloBlocco());
		sb.append(", addebitoCONAI=");
		sb.append(getAddebitoCONAI());
		sb.append(", libStr1=");
		sb.append(getLibStr1());
		sb.append(", libStr2=");
		sb.append(getLibStr2());
		sb.append(", libStr3=");
		sb.append(getLibStr3());
		sb.append(", libStr4=");
		sb.append(getLibStr4());
		sb.append(", libStr5=");
		sb.append(getLibStr5());
		sb.append(", libDat1=");
		sb.append(getLibDat1());
		sb.append(", libDat2=");
		sb.append(getLibDat2());
		sb.append(", libDat3=");
		sb.append(getLibDat3());
		sb.append(", libDat4=");
		sb.append(getLibDat4());
		sb.append(", libDat5=");
		sb.append(getLibDat5());
		sb.append(", libLng1=");
		sb.append(getLibLng1());
		sb.append(", libLng2=");
		sb.append(getLibLng2());
		sb.append(", libLng3=");
		sb.append(getLibLng3());
		sb.append(", libLng4=");
		sb.append(getLibLng4());
		sb.append(", libLng5=");
		sb.append(getLibLng5());
		sb.append(", libDbl1=");
		sb.append(getLibDbl1());
		sb.append(", libDbl2=");
		sb.append(getLibDbl2());
		sb.append(", libDbl3=");
		sb.append(getLibDbl3());
		sb.append(", libDbl4=");
		sb.append(getLibDbl4());
		sb.append(", libDbl5=");
		sb.append(getLibDbl5());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(202);

		sb.append("<model><model-name>");
		sb.append("it.bysoftware.ct.model.DatiClientiFornitori");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>tipoSoggetto</column-name><column-value><![CDATA[");
		sb.append(getTipoSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceSoggetto</column-name><column-value><![CDATA[");
		sb.append(getCodiceSoggetto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceBanca</column-name><column-value><![CDATA[");
		sb.append(getCodiceBanca());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceAgenzia</column-name><column-value><![CDATA[");
		sb.append(getCodiceAgenzia());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceIdBanca</column-name><column-value><![CDATA[");
		sb.append(getCodiceIdBanca());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codicePaese</column-name><column-value><![CDATA[");
		sb.append(getCodicePaese());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCINInt</column-name><column-value><![CDATA[");
		sb.append(getCodiceCINInt());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCINNaz</column-name><column-value><![CDATA[");
		sb.append(getCodiceCINNaz());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numeroContoCorrente</column-name><column-value><![CDATA[");
		sb.append(getNumeroContoCorrente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>IBAN</column-name><column-value><![CDATA[");
		sb.append(getIBAN());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>categoriaEconomica</column-name><column-value><![CDATA[");
		sb.append(getCategoriaEconomica());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceEsenzioneIVA</column-name><column-value><![CDATA[");
		sb.append(getCodiceEsenzioneIVA());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDivisa</column-name><column-value><![CDATA[");
		sb.append(getCodiceDivisa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codicePagamento</column-name><column-value><![CDATA[");
		sb.append(getCodicePagamento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceZona</column-name><column-value><![CDATA[");
		sb.append(getCodiceZona());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceAgente</column-name><column-value><![CDATA[");
		sb.append(getCodiceAgente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceGruppoAgente</column-name><column-value><![CDATA[");
		sb.append(getCodiceGruppoAgente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceCatProvv</column-name><column-value><![CDATA[");
		sb.append(getCodiceCatProvv());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceSpedizione</column-name><column-value><![CDATA[");
		sb.append(getCodiceSpedizione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codicePorto</column-name><column-value><![CDATA[");
		sb.append(getCodicePorto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceVettore</column-name><column-value><![CDATA[");
		sb.append(getCodiceVettore());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceDestDiversa</column-name><column-value><![CDATA[");
		sb.append(getCodiceDestDiversa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceListinoPrezzi</column-name><column-value><![CDATA[");
		sb.append(getCodiceListinoPrezzi());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceScontoTotale</column-name><column-value><![CDATA[");
		sb.append(getCodiceScontoTotale());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceScontoPreIVA</column-name><column-value><![CDATA[");
		sb.append(getCodiceScontoPreIVA());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>scontoCat1</column-name><column-value><![CDATA[");
		sb.append(getScontoCat1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>scontoCat2</column-name><column-value><![CDATA[");
		sb.append(getScontoCat2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceLingua</column-name><column-value><![CDATA[");
		sb.append(getCodiceLingua());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceLinguaEC</column-name><column-value><![CDATA[");
		sb.append(getCodiceLinguaEC());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>addebitoBolli</column-name><column-value><![CDATA[");
		sb.append(getAddebitoBolli());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>addebitoSpeseBanca</column-name><column-value><![CDATA[");
		sb.append(getAddebitoSpeseBanca());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ragruppaBolle</column-name><column-value><![CDATA[");
		sb.append(getRagruppaBolle());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sospensioneIVA</column-name><column-value><![CDATA[");
		sb.append(getSospensioneIVA());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ragruppaOrdini</column-name><column-value><![CDATA[");
		sb.append(getRagruppaOrdini());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ragruppaXDestinazione</column-name><column-value><![CDATA[");
		sb.append(getRagruppaXDestinazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ragruppaXPorto</column-name><column-value><![CDATA[");
		sb.append(getRagruppaXPorto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>percSpeseTrasporto</column-name><column-value><![CDATA[");
		sb.append(getPercSpeseTrasporto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>prezzoDaProporre</column-name><column-value><![CDATA[");
		sb.append(getPrezzoDaProporre());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sogettoFatturazione</column-name><column-value><![CDATA[");
		sb.append(getSogettoFatturazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codiceRaggEffetti</column-name><column-value><![CDATA[");
		sb.append(getCodiceRaggEffetti());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>riportoRiferimenti</column-name><column-value><![CDATA[");
		sb.append(getRiportoRiferimenti());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>stampaPrezzo</column-name><column-value><![CDATA[");
		sb.append(getStampaPrezzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>bloccato</column-name><column-value><![CDATA[");
		sb.append(getBloccato());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>motivazione</column-name><column-value><![CDATA[");
		sb.append(getMotivazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>livelloBlocco</column-name><column-value><![CDATA[");
		sb.append(getLivelloBlocco());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>addebitoCONAI</column-name><column-value><![CDATA[");
		sb.append(getAddebitoCONAI());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr1</column-name><column-value><![CDATA[");
		sb.append(getLibStr1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr2</column-name><column-value><![CDATA[");
		sb.append(getLibStr2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr3</column-name><column-value><![CDATA[");
		sb.append(getLibStr3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr4</column-name><column-value><![CDATA[");
		sb.append(getLibStr4());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libStr5</column-name><column-value><![CDATA[");
		sb.append(getLibStr5());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat1</column-name><column-value><![CDATA[");
		sb.append(getLibDat1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat2</column-name><column-value><![CDATA[");
		sb.append(getLibDat2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat3</column-name><column-value><![CDATA[");
		sb.append(getLibDat3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat4</column-name><column-value><![CDATA[");
		sb.append(getLibDat4());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDat5</column-name><column-value><![CDATA[");
		sb.append(getLibDat5());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng1</column-name><column-value><![CDATA[");
		sb.append(getLibLng1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng2</column-name><column-value><![CDATA[");
		sb.append(getLibLng2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng3</column-name><column-value><![CDATA[");
		sb.append(getLibLng3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng4</column-name><column-value><![CDATA[");
		sb.append(getLibLng4());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libLng5</column-name><column-value><![CDATA[");
		sb.append(getLibLng5());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl1</column-name><column-value><![CDATA[");
		sb.append(getLibDbl1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl2</column-name><column-value><![CDATA[");
		sb.append(getLibDbl2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl3</column-name><column-value><![CDATA[");
		sb.append(getLibDbl3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl4</column-name><column-value><![CDATA[");
		sb.append(getLibDbl4());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>libDbl5</column-name><column-value><![CDATA[");
		sb.append(getLibDbl5());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private boolean _tipoSoggetto;
	private String _codiceSoggetto;
	private String _codiceBanca;
	private String _codiceAgenzia;
	private String _codiceIdBanca;
	private String _codicePaese;
	private String _codiceCINInt;
	private String _codiceCINNaz;
	private String _numeroContoCorrente;
	private String _IBAN;
	private String _categoriaEconomica;
	private String _codiceEsenzioneIVA;
	private String _codiceDivisa;
	private String _codicePagamento;
	private String _codiceZona;
	private String _codiceAgente;
	private String _codiceGruppoAgente;
	private String _codiceCatProvv;
	private String _codiceSpedizione;
	private String _codicePorto;
	private String _codiceVettore;
	private String _codiceDestDiversa;
	private String _codiceListinoPrezzi;
	private String _codiceScontoTotale;
	private String _codiceScontoPreIVA;
	private String _scontoCat1;
	private String _scontoCat2;
	private String _codiceLingua;
	private String _codiceLinguaEC;
	private boolean _addebitoBolli;
	private boolean _addebitoSpeseBanca;
	private boolean _ragruppaBolle;
	private boolean _sospensioneIVA;
	private int _ragruppaOrdini;
	private boolean _ragruppaXDestinazione;
	private boolean _ragruppaXPorto;
	private double _percSpeseTrasporto;
	private int _prezzoDaProporre;
	private String _sogettoFatturazione;
	private String _codiceRaggEffetti;
	private int _riportoRiferimenti;
	private boolean _stampaPrezzo;
	private int _bloccato;
	private String _motivazione;
	private int _livelloBlocco;
	private boolean _addebitoCONAI;
	private String _libStr1;
	private String _libStr2;
	private String _libStr3;
	private String _libStr4;
	private String _libStr5;
	private Date _libDat1;
	private Date _libDat2;
	private Date _libDat3;
	private Date _libDat4;
	private Date _libDat5;
	private long _libLng1;
	private long _libLng2;
	private long _libLng3;
	private long _libLng4;
	private long _libLng5;
	private double _libDbl1;
	private double _libDbl2;
	private double _libDbl3;
	private double _libDbl4;
	private double _libDbl5;
	private BaseModel<?> _datiClientiFornitoriRemoteModel;
	private Class<?> _clpSerializerClass = it.bysoftware.ct.service.ClpSerializer.class;
}