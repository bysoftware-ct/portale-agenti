/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import it.bysoftware.ct.service.persistence.ScontiArticoliPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.bysoftware.ct.service.http.ScontiArticoliServiceSoap}.
 *
 * @author Mario Torrisi
 * @see it.bysoftware.ct.service.http.ScontiArticoliServiceSoap
 * @generated
 */
public class ScontiArticoliSoap implements Serializable {
	public static ScontiArticoliSoap toSoapModel(ScontiArticoli model) {
		ScontiArticoliSoap soapModel = new ScontiArticoliSoap();

		soapModel.setCodScontoCliente(model.getCodScontoCliente());
		soapModel.setCodScontoArticolo(model.getCodScontoArticolo());
		soapModel.setSconto(model.getSconto());

		return soapModel;
	}

	public static ScontiArticoliSoap[] toSoapModels(ScontiArticoli[] models) {
		ScontiArticoliSoap[] soapModels = new ScontiArticoliSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ScontiArticoliSoap[][] toSoapModels(ScontiArticoli[][] models) {
		ScontiArticoliSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ScontiArticoliSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ScontiArticoliSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ScontiArticoliSoap[] toSoapModels(List<ScontiArticoli> models) {
		List<ScontiArticoliSoap> soapModels = new ArrayList<ScontiArticoliSoap>(models.size());

		for (ScontiArticoli model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ScontiArticoliSoap[soapModels.size()]);
	}

	public ScontiArticoliSoap() {
	}

	public ScontiArticoliPK getPrimaryKey() {
		return new ScontiArticoliPK(_codScontoCliente, _codScontoArticolo);
	}

	public void setPrimaryKey(ScontiArticoliPK pk) {
		setCodScontoCliente(pk.codScontoCliente);
		setCodScontoArticolo(pk.codScontoArticolo);
	}

	public String getCodScontoCliente() {
		return _codScontoCliente;
	}

	public void setCodScontoCliente(String codScontoCliente) {
		_codScontoCliente = codScontoCliente;
	}

	public String getCodScontoArticolo() {
		return _codScontoArticolo;
	}

	public void setCodScontoArticolo(String codScontoArticolo) {
		_codScontoArticolo = codScontoArticolo;
	}

	public double getSconto() {
		return _sconto;
	}

	public void setSconto(double sconto) {
		_sconto = sconto;
	}

	private String _codScontoCliente;
	private String _codScontoArticolo;
	private double _sconto;
}