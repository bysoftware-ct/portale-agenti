/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.bysoftware.ct.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link EstrattoConto}.
 * </p>
 *
 * @author Mario Torrisi
 * @see EstrattoConto
 * @generated
 */
public class EstrattoContoWrapper implements EstrattoConto,
	ModelWrapper<EstrattoConto> {
	public EstrattoContoWrapper(EstrattoConto estrattoConto) {
		_estrattoConto = estrattoConto;
	}

	@Override
	public Class<?> getModelClass() {
		return EstrattoConto.class;
	}

	@Override
	public String getModelClassName() {
		return EstrattoConto.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("tipoSoggetto", getTipoSoggetto());
		attributes.put("codiceCliente", getCodiceCliente());
		attributes.put("esercizioRegistrazione", getEsercizioRegistrazione());
		attributes.put("numeroPartita", getNumeroPartita());
		attributes.put("numeroScadenza", getNumeroScadenza());
		attributes.put("dataScadenza", getDataScadenza());
		attributes.put("codiceTipoPagam", getCodiceTipoPagam());
		attributes.put("stato", getStato());
		attributes.put("codiceBanca", getCodiceBanca());
		attributes.put("codiceContoCorrente", getCodiceContoCorrente());
		attributes.put("codiceAgente", getCodiceAgente());
		attributes.put("tipoCausale", getTipoCausale());
		attributes.put("esercizioDocumento", getEsercizioDocumento());
		attributes.put("protocolloDocumento", getProtocolloDocumento());
		attributes.put("codiceAttivita", getCodiceAttivita());
		attributes.put("codiceCentro", getCodiceCentro());
		attributes.put("dataRegistrazione", getDataRegistrazione());
		attributes.put("dataOperazione", getDataOperazione());
		attributes.put("dataAggiornamento", getDataAggiornamento());
		attributes.put("dataChiusura", getDataChiusura());
		attributes.put("codiceCausale", getCodiceCausale());
		attributes.put("dataDocumento", getDataDocumento());
		attributes.put("numeroDocumento", getNumeroDocumento());
		attributes.put("codiceTipoPagamOrig", getCodiceTipoPagamOrig());
		attributes.put("codiceTipoPagamScad", getCodiceTipoPagamScad());
		attributes.put("causaleEstrattoContoOrig", getCausaleEstrattoContoOrig());
		attributes.put("codiceDivisaOrig", getCodiceDivisaOrig());
		attributes.put("importoTotaleInt", getImportoTotaleInt());
		attributes.put("importoPagatoInt", getImportoPagatoInt());
		attributes.put("importoApertoInt", getImportoApertoInt());
		attributes.put("importoEspostoInt", getImportoEspostoInt());
		attributes.put("importoScadutoInt", getImportoScadutoInt());
		attributes.put("importoInsolutoInt", getImportoInsolutoInt());
		attributes.put("importoInContenziosoInt", getImportoInContenziosoInt());
		attributes.put("importoTotale", getImportoTotale());
		attributes.put("importoPagato", getImportoPagato());
		attributes.put("importoAperto", getImportoAperto());
		attributes.put("importoEsposto", getImportoEsposto());
		attributes.put("importoScaduto", getImportoScaduto());
		attributes.put("importoInsoluto", getImportoInsoluto());
		attributes.put("importoInContenzioso", getImportoInContenzioso());
		attributes.put("numeroLettereSollecito", getNumeroLettereSollecito());
		attributes.put("codiceDivisaInterna", getCodiceDivisaInterna());
		attributes.put("codiceEsercizioScadenzaConversione",
			getCodiceEsercizioScadenzaConversione());
		attributes.put("numeroPartitaScadenzaCoversione",
			getNumeroPartitaScadenzaCoversione());
		attributes.put("numeroScadenzaCoversione", getNumeroScadenzaCoversione());
		attributes.put("tipoCoversione", getTipoCoversione());
		attributes.put("codiceEsercizioScritturaConversione",
			getCodiceEsercizioScritturaConversione());
		attributes.put("numeroRegistrazioneScritturaConversione",
			getNumeroRegistrazioneScritturaConversione());
		attributes.put("dataRegistrazioneScritturaConversione",
			getDataRegistrazioneScritturaConversione());
		attributes.put("codiceDivisaIntScadenzaConversione",
			getCodiceDivisaIntScadenzaConversione());
		attributes.put("codiceDivisaScadenzaConversione",
			getCodiceDivisaScadenzaConversione());
		attributes.put("cambioScadenzaConversione",
			getCambioScadenzaConversione());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Boolean tipoSoggetto = (Boolean)attributes.get("tipoSoggetto");

		if (tipoSoggetto != null) {
			setTipoSoggetto(tipoSoggetto);
		}

		String codiceCliente = (String)attributes.get("codiceCliente");

		if (codiceCliente != null) {
			setCodiceCliente(codiceCliente);
		}

		Integer esercizioRegistrazione = (Integer)attributes.get(
				"esercizioRegistrazione");

		if (esercizioRegistrazione != null) {
			setEsercizioRegistrazione(esercizioRegistrazione);
		}

		Integer numeroPartita = (Integer)attributes.get("numeroPartita");

		if (numeroPartita != null) {
			setNumeroPartita(numeroPartita);
		}

		Integer numeroScadenza = (Integer)attributes.get("numeroScadenza");

		if (numeroScadenza != null) {
			setNumeroScadenza(numeroScadenza);
		}

		Date dataScadenza = (Date)attributes.get("dataScadenza");

		if (dataScadenza != null) {
			setDataScadenza(dataScadenza);
		}

		Integer codiceTipoPagam = (Integer)attributes.get("codiceTipoPagam");

		if (codiceTipoPagam != null) {
			setCodiceTipoPagam(codiceTipoPagam);
		}

		Integer stato = (Integer)attributes.get("stato");

		if (stato != null) {
			setStato(stato);
		}

		String codiceBanca = (String)attributes.get("codiceBanca");

		if (codiceBanca != null) {
			setCodiceBanca(codiceBanca);
		}

		String codiceContoCorrente = (String)attributes.get(
				"codiceContoCorrente");

		if (codiceContoCorrente != null) {
			setCodiceContoCorrente(codiceContoCorrente);
		}

		String codiceAgente = (String)attributes.get("codiceAgente");

		if (codiceAgente != null) {
			setCodiceAgente(codiceAgente);
		}

		String tipoCausale = (String)attributes.get("tipoCausale");

		if (tipoCausale != null) {
			setTipoCausale(tipoCausale);
		}

		Integer esercizioDocumento = (Integer)attributes.get(
				"esercizioDocumento");

		if (esercizioDocumento != null) {
			setEsercizioDocumento(esercizioDocumento);
		}

		Integer protocolloDocumento = (Integer)attributes.get(
				"protocolloDocumento");

		if (protocolloDocumento != null) {
			setProtocolloDocumento(protocolloDocumento);
		}

		String codiceAttivita = (String)attributes.get("codiceAttivita");

		if (codiceAttivita != null) {
			setCodiceAttivita(codiceAttivita);
		}

		String codiceCentro = (String)attributes.get("codiceCentro");

		if (codiceCentro != null) {
			setCodiceCentro(codiceCentro);
		}

		Date dataRegistrazione = (Date)attributes.get("dataRegistrazione");

		if (dataRegistrazione != null) {
			setDataRegistrazione(dataRegistrazione);
		}

		Date dataOperazione = (Date)attributes.get("dataOperazione");

		if (dataOperazione != null) {
			setDataOperazione(dataOperazione);
		}

		Date dataAggiornamento = (Date)attributes.get("dataAggiornamento");

		if (dataAggiornamento != null) {
			setDataAggiornamento(dataAggiornamento);
		}

		Date dataChiusura = (Date)attributes.get("dataChiusura");

		if (dataChiusura != null) {
			setDataChiusura(dataChiusura);
		}

		String codiceCausale = (String)attributes.get("codiceCausale");

		if (codiceCausale != null) {
			setCodiceCausale(codiceCausale);
		}

		Date dataDocumento = (Date)attributes.get("dataDocumento");

		if (dataDocumento != null) {
			setDataDocumento(dataDocumento);
		}

		Integer numeroDocumento = (Integer)attributes.get("numeroDocumento");

		if (numeroDocumento != null) {
			setNumeroDocumento(numeroDocumento);
		}

		String codiceTipoPagamOrig = (String)attributes.get(
				"codiceTipoPagamOrig");

		if (codiceTipoPagamOrig != null) {
			setCodiceTipoPagamOrig(codiceTipoPagamOrig);
		}

		String codiceTipoPagamScad = (String)attributes.get(
				"codiceTipoPagamScad");

		if (codiceTipoPagamScad != null) {
			setCodiceTipoPagamScad(codiceTipoPagamScad);
		}

		String causaleEstrattoContoOrig = (String)attributes.get(
				"causaleEstrattoContoOrig");

		if (causaleEstrattoContoOrig != null) {
			setCausaleEstrattoContoOrig(causaleEstrattoContoOrig);
		}

		String codiceDivisaOrig = (String)attributes.get("codiceDivisaOrig");

		if (codiceDivisaOrig != null) {
			setCodiceDivisaOrig(codiceDivisaOrig);
		}

		Double importoTotaleInt = (Double)attributes.get("importoTotaleInt");

		if (importoTotaleInt != null) {
			setImportoTotaleInt(importoTotaleInt);
		}

		Double importoPagatoInt = (Double)attributes.get("importoPagatoInt");

		if (importoPagatoInt != null) {
			setImportoPagatoInt(importoPagatoInt);
		}

		Double importoApertoInt = (Double)attributes.get("importoApertoInt");

		if (importoApertoInt != null) {
			setImportoApertoInt(importoApertoInt);
		}

		Double importoEspostoInt = (Double)attributes.get("importoEspostoInt");

		if (importoEspostoInt != null) {
			setImportoEspostoInt(importoEspostoInt);
		}

		Double importoScadutoInt = (Double)attributes.get("importoScadutoInt");

		if (importoScadutoInt != null) {
			setImportoScadutoInt(importoScadutoInt);
		}

		Double importoInsolutoInt = (Double)attributes.get("importoInsolutoInt");

		if (importoInsolutoInt != null) {
			setImportoInsolutoInt(importoInsolutoInt);
		}

		Double importoInContenziosoInt = (Double)attributes.get(
				"importoInContenziosoInt");

		if (importoInContenziosoInt != null) {
			setImportoInContenziosoInt(importoInContenziosoInt);
		}

		Double importoTotale = (Double)attributes.get("importoTotale");

		if (importoTotale != null) {
			setImportoTotale(importoTotale);
		}

		Double importoPagato = (Double)attributes.get("importoPagato");

		if (importoPagato != null) {
			setImportoPagato(importoPagato);
		}

		Double importoAperto = (Double)attributes.get("importoAperto");

		if (importoAperto != null) {
			setImportoAperto(importoAperto);
		}

		Double importoEsposto = (Double)attributes.get("importoEsposto");

		if (importoEsposto != null) {
			setImportoEsposto(importoEsposto);
		}

		Double importoScaduto = (Double)attributes.get("importoScaduto");

		if (importoScaduto != null) {
			setImportoScaduto(importoScaduto);
		}

		Double importoInsoluto = (Double)attributes.get("importoInsoluto");

		if (importoInsoluto != null) {
			setImportoInsoluto(importoInsoluto);
		}

		Double importoInContenzioso = (Double)attributes.get(
				"importoInContenzioso");

		if (importoInContenzioso != null) {
			setImportoInContenzioso(importoInContenzioso);
		}

		Integer numeroLettereSollecito = (Integer)attributes.get(
				"numeroLettereSollecito");

		if (numeroLettereSollecito != null) {
			setNumeroLettereSollecito(numeroLettereSollecito);
		}

		String codiceDivisaInterna = (String)attributes.get(
				"codiceDivisaInterna");

		if (codiceDivisaInterna != null) {
			setCodiceDivisaInterna(codiceDivisaInterna);
		}

		Integer codiceEsercizioScadenzaConversione = (Integer)attributes.get(
				"codiceEsercizioScadenzaConversione");

		if (codiceEsercizioScadenzaConversione != null) {
			setCodiceEsercizioScadenzaConversione(codiceEsercizioScadenzaConversione);
		}

		Integer numeroPartitaScadenzaCoversione = (Integer)attributes.get(
				"numeroPartitaScadenzaCoversione");

		if (numeroPartitaScadenzaCoversione != null) {
			setNumeroPartitaScadenzaCoversione(numeroPartitaScadenzaCoversione);
		}

		Integer numeroScadenzaCoversione = (Integer)attributes.get(
				"numeroScadenzaCoversione");

		if (numeroScadenzaCoversione != null) {
			setNumeroScadenzaCoversione(numeroScadenzaCoversione);
		}

		Integer tipoCoversione = (Integer)attributes.get("tipoCoversione");

		if (tipoCoversione != null) {
			setTipoCoversione(tipoCoversione);
		}

		Integer codiceEsercizioScritturaConversione = (Integer)attributes.get(
				"codiceEsercizioScritturaConversione");

		if (codiceEsercizioScritturaConversione != null) {
			setCodiceEsercizioScritturaConversione(codiceEsercizioScritturaConversione);
		}

		Integer numeroRegistrazioneScritturaConversione = (Integer)attributes.get(
				"numeroRegistrazioneScritturaConversione");

		if (numeroRegistrazioneScritturaConversione != null) {
			setNumeroRegistrazioneScritturaConversione(numeroRegistrazioneScritturaConversione);
		}

		Date dataRegistrazioneScritturaConversione = (Date)attributes.get(
				"dataRegistrazioneScritturaConversione");

		if (dataRegistrazioneScritturaConversione != null) {
			setDataRegistrazioneScritturaConversione(dataRegistrazioneScritturaConversione);
		}

		String codiceDivisaIntScadenzaConversione = (String)attributes.get(
				"codiceDivisaIntScadenzaConversione");

		if (codiceDivisaIntScadenzaConversione != null) {
			setCodiceDivisaIntScadenzaConversione(codiceDivisaIntScadenzaConversione);
		}

		String codiceDivisaScadenzaConversione = (String)attributes.get(
				"codiceDivisaScadenzaConversione");

		if (codiceDivisaScadenzaConversione != null) {
			setCodiceDivisaScadenzaConversione(codiceDivisaScadenzaConversione);
		}

		Double cambioScadenzaConversione = (Double)attributes.get(
				"cambioScadenzaConversione");

		if (cambioScadenzaConversione != null) {
			setCambioScadenzaConversione(cambioScadenzaConversione);
		}
	}

	/**
	* Returns the primary key of this estratto conto.
	*
	* @return the primary key of this estratto conto
	*/
	@Override
	public it.bysoftware.ct.service.persistence.EstrattoContoPK getPrimaryKey() {
		return _estrattoConto.getPrimaryKey();
	}

	/**
	* Sets the primary key of this estratto conto.
	*
	* @param primaryKey the primary key of this estratto conto
	*/
	@Override
	public void setPrimaryKey(
		it.bysoftware.ct.service.persistence.EstrattoContoPK primaryKey) {
		_estrattoConto.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the tipo soggetto of this estratto conto.
	*
	* @return the tipo soggetto of this estratto conto
	*/
	@Override
	public boolean getTipoSoggetto() {
		return _estrattoConto.getTipoSoggetto();
	}

	/**
	* Returns <code>true</code> if this estratto conto is tipo soggetto.
	*
	* @return <code>true</code> if this estratto conto is tipo soggetto; <code>false</code> otherwise
	*/
	@Override
	public boolean isTipoSoggetto() {
		return _estrattoConto.isTipoSoggetto();
	}

	/**
	* Sets whether this estratto conto is tipo soggetto.
	*
	* @param tipoSoggetto the tipo soggetto of this estratto conto
	*/
	@Override
	public void setTipoSoggetto(boolean tipoSoggetto) {
		_estrattoConto.setTipoSoggetto(tipoSoggetto);
	}

	/**
	* Returns the codice cliente of this estratto conto.
	*
	* @return the codice cliente of this estratto conto
	*/
	@Override
	public java.lang.String getCodiceCliente() {
		return _estrattoConto.getCodiceCliente();
	}

	/**
	* Sets the codice cliente of this estratto conto.
	*
	* @param codiceCliente the codice cliente of this estratto conto
	*/
	@Override
	public void setCodiceCliente(java.lang.String codiceCliente) {
		_estrattoConto.setCodiceCliente(codiceCliente);
	}

	/**
	* Returns the esercizio registrazione of this estratto conto.
	*
	* @return the esercizio registrazione of this estratto conto
	*/
	@Override
	public int getEsercizioRegistrazione() {
		return _estrattoConto.getEsercizioRegistrazione();
	}

	/**
	* Sets the esercizio registrazione of this estratto conto.
	*
	* @param esercizioRegistrazione the esercizio registrazione of this estratto conto
	*/
	@Override
	public void setEsercizioRegistrazione(int esercizioRegistrazione) {
		_estrattoConto.setEsercizioRegistrazione(esercizioRegistrazione);
	}

	/**
	* Returns the numero partita of this estratto conto.
	*
	* @return the numero partita of this estratto conto
	*/
	@Override
	public int getNumeroPartita() {
		return _estrattoConto.getNumeroPartita();
	}

	/**
	* Sets the numero partita of this estratto conto.
	*
	* @param numeroPartita the numero partita of this estratto conto
	*/
	@Override
	public void setNumeroPartita(int numeroPartita) {
		_estrattoConto.setNumeroPartita(numeroPartita);
	}

	/**
	* Returns the numero scadenza of this estratto conto.
	*
	* @return the numero scadenza of this estratto conto
	*/
	@Override
	public int getNumeroScadenza() {
		return _estrattoConto.getNumeroScadenza();
	}

	/**
	* Sets the numero scadenza of this estratto conto.
	*
	* @param numeroScadenza the numero scadenza of this estratto conto
	*/
	@Override
	public void setNumeroScadenza(int numeroScadenza) {
		_estrattoConto.setNumeroScadenza(numeroScadenza);
	}

	/**
	* Returns the data scadenza of this estratto conto.
	*
	* @return the data scadenza of this estratto conto
	*/
	@Override
	public java.util.Date getDataScadenza() {
		return _estrattoConto.getDataScadenza();
	}

	/**
	* Sets the data scadenza of this estratto conto.
	*
	* @param dataScadenza the data scadenza of this estratto conto
	*/
	@Override
	public void setDataScadenza(java.util.Date dataScadenza) {
		_estrattoConto.setDataScadenza(dataScadenza);
	}

	/**
	* Returns the codice tipo pagam of this estratto conto.
	*
	* @return the codice tipo pagam of this estratto conto
	*/
	@Override
	public int getCodiceTipoPagam() {
		return _estrattoConto.getCodiceTipoPagam();
	}

	/**
	* Sets the codice tipo pagam of this estratto conto.
	*
	* @param codiceTipoPagam the codice tipo pagam of this estratto conto
	*/
	@Override
	public void setCodiceTipoPagam(int codiceTipoPagam) {
		_estrattoConto.setCodiceTipoPagam(codiceTipoPagam);
	}

	/**
	* Returns the stato of this estratto conto.
	*
	* @return the stato of this estratto conto
	*/
	@Override
	public int getStato() {
		return _estrattoConto.getStato();
	}

	/**
	* Sets the stato of this estratto conto.
	*
	* @param stato the stato of this estratto conto
	*/
	@Override
	public void setStato(int stato) {
		_estrattoConto.setStato(stato);
	}

	/**
	* Returns the codice banca of this estratto conto.
	*
	* @return the codice banca of this estratto conto
	*/
	@Override
	public java.lang.String getCodiceBanca() {
		return _estrattoConto.getCodiceBanca();
	}

	/**
	* Sets the codice banca of this estratto conto.
	*
	* @param codiceBanca the codice banca of this estratto conto
	*/
	@Override
	public void setCodiceBanca(java.lang.String codiceBanca) {
		_estrattoConto.setCodiceBanca(codiceBanca);
	}

	/**
	* Returns the codice conto corrente of this estratto conto.
	*
	* @return the codice conto corrente of this estratto conto
	*/
	@Override
	public java.lang.String getCodiceContoCorrente() {
		return _estrattoConto.getCodiceContoCorrente();
	}

	/**
	* Sets the codice conto corrente of this estratto conto.
	*
	* @param codiceContoCorrente the codice conto corrente of this estratto conto
	*/
	@Override
	public void setCodiceContoCorrente(java.lang.String codiceContoCorrente) {
		_estrattoConto.setCodiceContoCorrente(codiceContoCorrente);
	}

	/**
	* Returns the codice agente of this estratto conto.
	*
	* @return the codice agente of this estratto conto
	*/
	@Override
	public java.lang.String getCodiceAgente() {
		return _estrattoConto.getCodiceAgente();
	}

	/**
	* Sets the codice agente of this estratto conto.
	*
	* @param codiceAgente the codice agente of this estratto conto
	*/
	@Override
	public void setCodiceAgente(java.lang.String codiceAgente) {
		_estrattoConto.setCodiceAgente(codiceAgente);
	}

	/**
	* Returns the tipo causale of this estratto conto.
	*
	* @return the tipo causale of this estratto conto
	*/
	@Override
	public java.lang.String getTipoCausale() {
		return _estrattoConto.getTipoCausale();
	}

	/**
	* Sets the tipo causale of this estratto conto.
	*
	* @param tipoCausale the tipo causale of this estratto conto
	*/
	@Override
	public void setTipoCausale(java.lang.String tipoCausale) {
		_estrattoConto.setTipoCausale(tipoCausale);
	}

	/**
	* Returns the esercizio documento of this estratto conto.
	*
	* @return the esercizio documento of this estratto conto
	*/
	@Override
	public int getEsercizioDocumento() {
		return _estrattoConto.getEsercizioDocumento();
	}

	/**
	* Sets the esercizio documento of this estratto conto.
	*
	* @param esercizioDocumento the esercizio documento of this estratto conto
	*/
	@Override
	public void setEsercizioDocumento(int esercizioDocumento) {
		_estrattoConto.setEsercizioDocumento(esercizioDocumento);
	}

	/**
	* Returns the protocollo documento of this estratto conto.
	*
	* @return the protocollo documento of this estratto conto
	*/
	@Override
	public int getProtocolloDocumento() {
		return _estrattoConto.getProtocolloDocumento();
	}

	/**
	* Sets the protocollo documento of this estratto conto.
	*
	* @param protocolloDocumento the protocollo documento of this estratto conto
	*/
	@Override
	public void setProtocolloDocumento(int protocolloDocumento) {
		_estrattoConto.setProtocolloDocumento(protocolloDocumento);
	}

	/**
	* Returns the codice attivita of this estratto conto.
	*
	* @return the codice attivita of this estratto conto
	*/
	@Override
	public java.lang.String getCodiceAttivita() {
		return _estrattoConto.getCodiceAttivita();
	}

	/**
	* Sets the codice attivita of this estratto conto.
	*
	* @param codiceAttivita the codice attivita of this estratto conto
	*/
	@Override
	public void setCodiceAttivita(java.lang.String codiceAttivita) {
		_estrattoConto.setCodiceAttivita(codiceAttivita);
	}

	/**
	* Returns the codice centro of this estratto conto.
	*
	* @return the codice centro of this estratto conto
	*/
	@Override
	public java.lang.String getCodiceCentro() {
		return _estrattoConto.getCodiceCentro();
	}

	/**
	* Sets the codice centro of this estratto conto.
	*
	* @param codiceCentro the codice centro of this estratto conto
	*/
	@Override
	public void setCodiceCentro(java.lang.String codiceCentro) {
		_estrattoConto.setCodiceCentro(codiceCentro);
	}

	/**
	* Returns the data registrazione of this estratto conto.
	*
	* @return the data registrazione of this estratto conto
	*/
	@Override
	public java.util.Date getDataRegistrazione() {
		return _estrattoConto.getDataRegistrazione();
	}

	/**
	* Sets the data registrazione of this estratto conto.
	*
	* @param dataRegistrazione the data registrazione of this estratto conto
	*/
	@Override
	public void setDataRegistrazione(java.util.Date dataRegistrazione) {
		_estrattoConto.setDataRegistrazione(dataRegistrazione);
	}

	/**
	* Returns the data operazione of this estratto conto.
	*
	* @return the data operazione of this estratto conto
	*/
	@Override
	public java.util.Date getDataOperazione() {
		return _estrattoConto.getDataOperazione();
	}

	/**
	* Sets the data operazione of this estratto conto.
	*
	* @param dataOperazione the data operazione of this estratto conto
	*/
	@Override
	public void setDataOperazione(java.util.Date dataOperazione) {
		_estrattoConto.setDataOperazione(dataOperazione);
	}

	/**
	* Returns the data aggiornamento of this estratto conto.
	*
	* @return the data aggiornamento of this estratto conto
	*/
	@Override
	public java.util.Date getDataAggiornamento() {
		return _estrattoConto.getDataAggiornamento();
	}

	/**
	* Sets the data aggiornamento of this estratto conto.
	*
	* @param dataAggiornamento the data aggiornamento of this estratto conto
	*/
	@Override
	public void setDataAggiornamento(java.util.Date dataAggiornamento) {
		_estrattoConto.setDataAggiornamento(dataAggiornamento);
	}

	/**
	* Returns the data chiusura of this estratto conto.
	*
	* @return the data chiusura of this estratto conto
	*/
	@Override
	public java.util.Date getDataChiusura() {
		return _estrattoConto.getDataChiusura();
	}

	/**
	* Sets the data chiusura of this estratto conto.
	*
	* @param dataChiusura the data chiusura of this estratto conto
	*/
	@Override
	public void setDataChiusura(java.util.Date dataChiusura) {
		_estrattoConto.setDataChiusura(dataChiusura);
	}

	/**
	* Returns the codice causale of this estratto conto.
	*
	* @return the codice causale of this estratto conto
	*/
	@Override
	public java.lang.String getCodiceCausale() {
		return _estrattoConto.getCodiceCausale();
	}

	/**
	* Sets the codice causale of this estratto conto.
	*
	* @param codiceCausale the codice causale of this estratto conto
	*/
	@Override
	public void setCodiceCausale(java.lang.String codiceCausale) {
		_estrattoConto.setCodiceCausale(codiceCausale);
	}

	/**
	* Returns the data documento of this estratto conto.
	*
	* @return the data documento of this estratto conto
	*/
	@Override
	public java.util.Date getDataDocumento() {
		return _estrattoConto.getDataDocumento();
	}

	/**
	* Sets the data documento of this estratto conto.
	*
	* @param dataDocumento the data documento of this estratto conto
	*/
	@Override
	public void setDataDocumento(java.util.Date dataDocumento) {
		_estrattoConto.setDataDocumento(dataDocumento);
	}

	/**
	* Returns the numero documento of this estratto conto.
	*
	* @return the numero documento of this estratto conto
	*/
	@Override
	public int getNumeroDocumento() {
		return _estrattoConto.getNumeroDocumento();
	}

	/**
	* Sets the numero documento of this estratto conto.
	*
	* @param numeroDocumento the numero documento of this estratto conto
	*/
	@Override
	public void setNumeroDocumento(int numeroDocumento) {
		_estrattoConto.setNumeroDocumento(numeroDocumento);
	}

	/**
	* Returns the codice tipo pagam orig of this estratto conto.
	*
	* @return the codice tipo pagam orig of this estratto conto
	*/
	@Override
	public java.lang.String getCodiceTipoPagamOrig() {
		return _estrattoConto.getCodiceTipoPagamOrig();
	}

	/**
	* Sets the codice tipo pagam orig of this estratto conto.
	*
	* @param codiceTipoPagamOrig the codice tipo pagam orig of this estratto conto
	*/
	@Override
	public void setCodiceTipoPagamOrig(java.lang.String codiceTipoPagamOrig) {
		_estrattoConto.setCodiceTipoPagamOrig(codiceTipoPagamOrig);
	}

	/**
	* Returns the codice tipo pagam scad of this estratto conto.
	*
	* @return the codice tipo pagam scad of this estratto conto
	*/
	@Override
	public java.lang.String getCodiceTipoPagamScad() {
		return _estrattoConto.getCodiceTipoPagamScad();
	}

	/**
	* Sets the codice tipo pagam scad of this estratto conto.
	*
	* @param codiceTipoPagamScad the codice tipo pagam scad of this estratto conto
	*/
	@Override
	public void setCodiceTipoPagamScad(java.lang.String codiceTipoPagamScad) {
		_estrattoConto.setCodiceTipoPagamScad(codiceTipoPagamScad);
	}

	/**
	* Returns the causale estratto conto orig of this estratto conto.
	*
	* @return the causale estratto conto orig of this estratto conto
	*/
	@Override
	public java.lang.String getCausaleEstrattoContoOrig() {
		return _estrattoConto.getCausaleEstrattoContoOrig();
	}

	/**
	* Sets the causale estratto conto orig of this estratto conto.
	*
	* @param causaleEstrattoContoOrig the causale estratto conto orig of this estratto conto
	*/
	@Override
	public void setCausaleEstrattoContoOrig(
		java.lang.String causaleEstrattoContoOrig) {
		_estrattoConto.setCausaleEstrattoContoOrig(causaleEstrattoContoOrig);
	}

	/**
	* Returns the codice divisa orig of this estratto conto.
	*
	* @return the codice divisa orig of this estratto conto
	*/
	@Override
	public java.lang.String getCodiceDivisaOrig() {
		return _estrattoConto.getCodiceDivisaOrig();
	}

	/**
	* Sets the codice divisa orig of this estratto conto.
	*
	* @param codiceDivisaOrig the codice divisa orig of this estratto conto
	*/
	@Override
	public void setCodiceDivisaOrig(java.lang.String codiceDivisaOrig) {
		_estrattoConto.setCodiceDivisaOrig(codiceDivisaOrig);
	}

	/**
	* Returns the importo totale int of this estratto conto.
	*
	* @return the importo totale int of this estratto conto
	*/
	@Override
	public double getImportoTotaleInt() {
		return _estrattoConto.getImportoTotaleInt();
	}

	/**
	* Sets the importo totale int of this estratto conto.
	*
	* @param importoTotaleInt the importo totale int of this estratto conto
	*/
	@Override
	public void setImportoTotaleInt(double importoTotaleInt) {
		_estrattoConto.setImportoTotaleInt(importoTotaleInt);
	}

	/**
	* Returns the importo pagato int of this estratto conto.
	*
	* @return the importo pagato int of this estratto conto
	*/
	@Override
	public double getImportoPagatoInt() {
		return _estrattoConto.getImportoPagatoInt();
	}

	/**
	* Sets the importo pagato int of this estratto conto.
	*
	* @param importoPagatoInt the importo pagato int of this estratto conto
	*/
	@Override
	public void setImportoPagatoInt(double importoPagatoInt) {
		_estrattoConto.setImportoPagatoInt(importoPagatoInt);
	}

	/**
	* Returns the importo aperto int of this estratto conto.
	*
	* @return the importo aperto int of this estratto conto
	*/
	@Override
	public double getImportoApertoInt() {
		return _estrattoConto.getImportoApertoInt();
	}

	/**
	* Sets the importo aperto int of this estratto conto.
	*
	* @param importoApertoInt the importo aperto int of this estratto conto
	*/
	@Override
	public void setImportoApertoInt(double importoApertoInt) {
		_estrattoConto.setImportoApertoInt(importoApertoInt);
	}

	/**
	* Returns the importo esposto int of this estratto conto.
	*
	* @return the importo esposto int of this estratto conto
	*/
	@Override
	public double getImportoEspostoInt() {
		return _estrattoConto.getImportoEspostoInt();
	}

	/**
	* Sets the importo esposto int of this estratto conto.
	*
	* @param importoEspostoInt the importo esposto int of this estratto conto
	*/
	@Override
	public void setImportoEspostoInt(double importoEspostoInt) {
		_estrattoConto.setImportoEspostoInt(importoEspostoInt);
	}

	/**
	* Returns the importo scaduto int of this estratto conto.
	*
	* @return the importo scaduto int of this estratto conto
	*/
	@Override
	public double getImportoScadutoInt() {
		return _estrattoConto.getImportoScadutoInt();
	}

	/**
	* Sets the importo scaduto int of this estratto conto.
	*
	* @param importoScadutoInt the importo scaduto int of this estratto conto
	*/
	@Override
	public void setImportoScadutoInt(double importoScadutoInt) {
		_estrattoConto.setImportoScadutoInt(importoScadutoInt);
	}

	/**
	* Returns the importo insoluto int of this estratto conto.
	*
	* @return the importo insoluto int of this estratto conto
	*/
	@Override
	public double getImportoInsolutoInt() {
		return _estrattoConto.getImportoInsolutoInt();
	}

	/**
	* Sets the importo insoluto int of this estratto conto.
	*
	* @param importoInsolutoInt the importo insoluto int of this estratto conto
	*/
	@Override
	public void setImportoInsolutoInt(double importoInsolutoInt) {
		_estrattoConto.setImportoInsolutoInt(importoInsolutoInt);
	}

	/**
	* Returns the importo in contenzioso int of this estratto conto.
	*
	* @return the importo in contenzioso int of this estratto conto
	*/
	@Override
	public double getImportoInContenziosoInt() {
		return _estrattoConto.getImportoInContenziosoInt();
	}

	/**
	* Sets the importo in contenzioso int of this estratto conto.
	*
	* @param importoInContenziosoInt the importo in contenzioso int of this estratto conto
	*/
	@Override
	public void setImportoInContenziosoInt(double importoInContenziosoInt) {
		_estrattoConto.setImportoInContenziosoInt(importoInContenziosoInt);
	}

	/**
	* Returns the importo totale of this estratto conto.
	*
	* @return the importo totale of this estratto conto
	*/
	@Override
	public double getImportoTotale() {
		return _estrattoConto.getImportoTotale();
	}

	/**
	* Sets the importo totale of this estratto conto.
	*
	* @param importoTotale the importo totale of this estratto conto
	*/
	@Override
	public void setImportoTotale(double importoTotale) {
		_estrattoConto.setImportoTotale(importoTotale);
	}

	/**
	* Returns the importo pagato of this estratto conto.
	*
	* @return the importo pagato of this estratto conto
	*/
	@Override
	public double getImportoPagato() {
		return _estrattoConto.getImportoPagato();
	}

	/**
	* Sets the importo pagato of this estratto conto.
	*
	* @param importoPagato the importo pagato of this estratto conto
	*/
	@Override
	public void setImportoPagato(double importoPagato) {
		_estrattoConto.setImportoPagato(importoPagato);
	}

	/**
	* Returns the importo aperto of this estratto conto.
	*
	* @return the importo aperto of this estratto conto
	*/
	@Override
	public double getImportoAperto() {
		return _estrattoConto.getImportoAperto();
	}

	/**
	* Sets the importo aperto of this estratto conto.
	*
	* @param importoAperto the importo aperto of this estratto conto
	*/
	@Override
	public void setImportoAperto(double importoAperto) {
		_estrattoConto.setImportoAperto(importoAperto);
	}

	/**
	* Returns the importo esposto of this estratto conto.
	*
	* @return the importo esposto of this estratto conto
	*/
	@Override
	public double getImportoEsposto() {
		return _estrattoConto.getImportoEsposto();
	}

	/**
	* Sets the importo esposto of this estratto conto.
	*
	* @param importoEsposto the importo esposto of this estratto conto
	*/
	@Override
	public void setImportoEsposto(double importoEsposto) {
		_estrattoConto.setImportoEsposto(importoEsposto);
	}

	/**
	* Returns the importo scaduto of this estratto conto.
	*
	* @return the importo scaduto of this estratto conto
	*/
	@Override
	public double getImportoScaduto() {
		return _estrattoConto.getImportoScaduto();
	}

	/**
	* Sets the importo scaduto of this estratto conto.
	*
	* @param importoScaduto the importo scaduto of this estratto conto
	*/
	@Override
	public void setImportoScaduto(double importoScaduto) {
		_estrattoConto.setImportoScaduto(importoScaduto);
	}

	/**
	* Returns the importo insoluto of this estratto conto.
	*
	* @return the importo insoluto of this estratto conto
	*/
	@Override
	public double getImportoInsoluto() {
		return _estrattoConto.getImportoInsoluto();
	}

	/**
	* Sets the importo insoluto of this estratto conto.
	*
	* @param importoInsoluto the importo insoluto of this estratto conto
	*/
	@Override
	public void setImportoInsoluto(double importoInsoluto) {
		_estrattoConto.setImportoInsoluto(importoInsoluto);
	}

	/**
	* Returns the importo in contenzioso of this estratto conto.
	*
	* @return the importo in contenzioso of this estratto conto
	*/
	@Override
	public double getImportoInContenzioso() {
		return _estrattoConto.getImportoInContenzioso();
	}

	/**
	* Sets the importo in contenzioso of this estratto conto.
	*
	* @param importoInContenzioso the importo in contenzioso of this estratto conto
	*/
	@Override
	public void setImportoInContenzioso(double importoInContenzioso) {
		_estrattoConto.setImportoInContenzioso(importoInContenzioso);
	}

	/**
	* Returns the numero lettere sollecito of this estratto conto.
	*
	* @return the numero lettere sollecito of this estratto conto
	*/
	@Override
	public int getNumeroLettereSollecito() {
		return _estrattoConto.getNumeroLettereSollecito();
	}

	/**
	* Sets the numero lettere sollecito of this estratto conto.
	*
	* @param numeroLettereSollecito the numero lettere sollecito of this estratto conto
	*/
	@Override
	public void setNumeroLettereSollecito(int numeroLettereSollecito) {
		_estrattoConto.setNumeroLettereSollecito(numeroLettereSollecito);
	}

	/**
	* Returns the codice divisa interna of this estratto conto.
	*
	* @return the codice divisa interna of this estratto conto
	*/
	@Override
	public java.lang.String getCodiceDivisaInterna() {
		return _estrattoConto.getCodiceDivisaInterna();
	}

	/**
	* Sets the codice divisa interna of this estratto conto.
	*
	* @param codiceDivisaInterna the codice divisa interna of this estratto conto
	*/
	@Override
	public void setCodiceDivisaInterna(java.lang.String codiceDivisaInterna) {
		_estrattoConto.setCodiceDivisaInterna(codiceDivisaInterna);
	}

	/**
	* Returns the codice esercizio scadenza conversione of this estratto conto.
	*
	* @return the codice esercizio scadenza conversione of this estratto conto
	*/
	@Override
	public int getCodiceEsercizioScadenzaConversione() {
		return _estrattoConto.getCodiceEsercizioScadenzaConversione();
	}

	/**
	* Sets the codice esercizio scadenza conversione of this estratto conto.
	*
	* @param codiceEsercizioScadenzaConversione the codice esercizio scadenza conversione of this estratto conto
	*/
	@Override
	public void setCodiceEsercizioScadenzaConversione(
		int codiceEsercizioScadenzaConversione) {
		_estrattoConto.setCodiceEsercizioScadenzaConversione(codiceEsercizioScadenzaConversione);
	}

	/**
	* Returns the numero partita scadenza coversione of this estratto conto.
	*
	* @return the numero partita scadenza coversione of this estratto conto
	*/
	@Override
	public int getNumeroPartitaScadenzaCoversione() {
		return _estrattoConto.getNumeroPartitaScadenzaCoversione();
	}

	/**
	* Sets the numero partita scadenza coversione of this estratto conto.
	*
	* @param numeroPartitaScadenzaCoversione the numero partita scadenza coversione of this estratto conto
	*/
	@Override
	public void setNumeroPartitaScadenzaCoversione(
		int numeroPartitaScadenzaCoversione) {
		_estrattoConto.setNumeroPartitaScadenzaCoversione(numeroPartitaScadenzaCoversione);
	}

	/**
	* Returns the numero scadenza coversione of this estratto conto.
	*
	* @return the numero scadenza coversione of this estratto conto
	*/
	@Override
	public int getNumeroScadenzaCoversione() {
		return _estrattoConto.getNumeroScadenzaCoversione();
	}

	/**
	* Sets the numero scadenza coversione of this estratto conto.
	*
	* @param numeroScadenzaCoversione the numero scadenza coversione of this estratto conto
	*/
	@Override
	public void setNumeroScadenzaCoversione(int numeroScadenzaCoversione) {
		_estrattoConto.setNumeroScadenzaCoversione(numeroScadenzaCoversione);
	}

	/**
	* Returns the tipo coversione of this estratto conto.
	*
	* @return the tipo coversione of this estratto conto
	*/
	@Override
	public int getTipoCoversione() {
		return _estrattoConto.getTipoCoversione();
	}

	/**
	* Sets the tipo coversione of this estratto conto.
	*
	* @param tipoCoversione the tipo coversione of this estratto conto
	*/
	@Override
	public void setTipoCoversione(int tipoCoversione) {
		_estrattoConto.setTipoCoversione(tipoCoversione);
	}

	/**
	* Returns the codice esercizio scrittura conversione of this estratto conto.
	*
	* @return the codice esercizio scrittura conversione of this estratto conto
	*/
	@Override
	public int getCodiceEsercizioScritturaConversione() {
		return _estrattoConto.getCodiceEsercizioScritturaConversione();
	}

	/**
	* Sets the codice esercizio scrittura conversione of this estratto conto.
	*
	* @param codiceEsercizioScritturaConversione the codice esercizio scrittura conversione of this estratto conto
	*/
	@Override
	public void setCodiceEsercizioScritturaConversione(
		int codiceEsercizioScritturaConversione) {
		_estrattoConto.setCodiceEsercizioScritturaConversione(codiceEsercizioScritturaConversione);
	}

	/**
	* Returns the numero registrazione scrittura conversione of this estratto conto.
	*
	* @return the numero registrazione scrittura conversione of this estratto conto
	*/
	@Override
	public int getNumeroRegistrazioneScritturaConversione() {
		return _estrattoConto.getNumeroRegistrazioneScritturaConversione();
	}

	/**
	* Sets the numero registrazione scrittura conversione of this estratto conto.
	*
	* @param numeroRegistrazioneScritturaConversione the numero registrazione scrittura conversione of this estratto conto
	*/
	@Override
	public void setNumeroRegistrazioneScritturaConversione(
		int numeroRegistrazioneScritturaConversione) {
		_estrattoConto.setNumeroRegistrazioneScritturaConversione(numeroRegistrazioneScritturaConversione);
	}

	/**
	* Returns the data registrazione scrittura conversione of this estratto conto.
	*
	* @return the data registrazione scrittura conversione of this estratto conto
	*/
	@Override
	public java.util.Date getDataRegistrazioneScritturaConversione() {
		return _estrattoConto.getDataRegistrazioneScritturaConversione();
	}

	/**
	* Sets the data registrazione scrittura conversione of this estratto conto.
	*
	* @param dataRegistrazioneScritturaConversione the data registrazione scrittura conversione of this estratto conto
	*/
	@Override
	public void setDataRegistrazioneScritturaConversione(
		java.util.Date dataRegistrazioneScritturaConversione) {
		_estrattoConto.setDataRegistrazioneScritturaConversione(dataRegistrazioneScritturaConversione);
	}

	/**
	* Returns the codice divisa int scadenza conversione of this estratto conto.
	*
	* @return the codice divisa int scadenza conversione of this estratto conto
	*/
	@Override
	public java.lang.String getCodiceDivisaIntScadenzaConversione() {
		return _estrattoConto.getCodiceDivisaIntScadenzaConversione();
	}

	/**
	* Sets the codice divisa int scadenza conversione of this estratto conto.
	*
	* @param codiceDivisaIntScadenzaConversione the codice divisa int scadenza conversione of this estratto conto
	*/
	@Override
	public void setCodiceDivisaIntScadenzaConversione(
		java.lang.String codiceDivisaIntScadenzaConversione) {
		_estrattoConto.setCodiceDivisaIntScadenzaConversione(codiceDivisaIntScadenzaConversione);
	}

	/**
	* Returns the codice divisa scadenza conversione of this estratto conto.
	*
	* @return the codice divisa scadenza conversione of this estratto conto
	*/
	@Override
	public java.lang.String getCodiceDivisaScadenzaConversione() {
		return _estrattoConto.getCodiceDivisaScadenzaConversione();
	}

	/**
	* Sets the codice divisa scadenza conversione of this estratto conto.
	*
	* @param codiceDivisaScadenzaConversione the codice divisa scadenza conversione of this estratto conto
	*/
	@Override
	public void setCodiceDivisaScadenzaConversione(
		java.lang.String codiceDivisaScadenzaConversione) {
		_estrattoConto.setCodiceDivisaScadenzaConversione(codiceDivisaScadenzaConversione);
	}

	/**
	* Returns the cambio scadenza conversione of this estratto conto.
	*
	* @return the cambio scadenza conversione of this estratto conto
	*/
	@Override
	public double getCambioScadenzaConversione() {
		return _estrattoConto.getCambioScadenzaConversione();
	}

	/**
	* Sets the cambio scadenza conversione of this estratto conto.
	*
	* @param cambioScadenzaConversione the cambio scadenza conversione of this estratto conto
	*/
	@Override
	public void setCambioScadenzaConversione(double cambioScadenzaConversione) {
		_estrattoConto.setCambioScadenzaConversione(cambioScadenzaConversione);
	}

	@Override
	public boolean isNew() {
		return _estrattoConto.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_estrattoConto.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _estrattoConto.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_estrattoConto.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _estrattoConto.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _estrattoConto.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_estrattoConto.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _estrattoConto.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_estrattoConto.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_estrattoConto.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_estrattoConto.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new EstrattoContoWrapper((EstrattoConto)_estrattoConto.clone());
	}

	@Override
	public int compareTo(it.bysoftware.ct.model.EstrattoConto estrattoConto) {
		return _estrattoConto.compareTo(estrattoConto);
	}

	@Override
	public int hashCode() {
		return _estrattoConto.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.bysoftware.ct.model.EstrattoConto> toCacheModel() {
		return _estrattoConto.toCacheModel();
	}

	@Override
	public it.bysoftware.ct.model.EstrattoConto toEscapedModel() {
		return new EstrattoContoWrapper(_estrattoConto.toEscapedModel());
	}

	@Override
	public it.bysoftware.ct.model.EstrattoConto toUnescapedModel() {
		return new EstrattoContoWrapper(_estrattoConto.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _estrattoConto.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _estrattoConto.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_estrattoConto.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EstrattoContoWrapper)) {
			return false;
		}

		EstrattoContoWrapper estrattoContoWrapper = (EstrattoContoWrapper)obj;

		if (Validator.equals(_estrattoConto, estrattoContoWrapper._estrattoConto)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public EstrattoConto getWrappedEstrattoConto() {
		return _estrattoConto;
	}

	@Override
	public EstrattoConto getWrappedModel() {
		return _estrattoConto;
	}

	@Override
	public void resetOriginalValues() {
		_estrattoConto.resetOriginalValues();
	}

	private EstrattoConto _estrattoConto;
}