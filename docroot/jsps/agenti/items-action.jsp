<%@page import="it.bysoftware.ct.agenti.ResourceId"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="it.bysoftware.ct.model.Articoli"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@include file="../init.jsp"%>
<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    Articoli articolo = (Articoli) row.getObject();
    boolean handle = Boolean.parseBoolean(row.getParameter("handle")
            .toString());
    boolean editing = Boolean.parseBoolean(row.getParameter("editing")
            .toString());
    String codiceSoggetto = (String) row.getParameter("codiceSoggetto");
    String backUrl = (String) row.getParameter("backUrl");
%>

<liferay-ui:icon-menu>
	<c:if test="<%=!articolo.getLiberoString5().equals("")%>">
		<liferay-portlet:renderURL varImpl="sheetURL">
			<portlet:param name="mvcPath"
				value="/jsps/schedetecniche/edit-item-sheet.jsp" />
			<portlet:param name="itemSheetId"
				value="<%=articolo.getLiberoString5()%>" />
		</liferay-portlet:renderURL>
		<liferay-ui:icon image="page_template" url="${sheetURL}"
			target="_blank" message="item-sheet" />
	</c:if>
	<liferay-portlet:renderURL var="itemURL">
		<liferay-portlet:param name="backUrl" value="<%=backUrl%>" />
		<liferay-portlet:param name="codiceSoggetto"
			value="<%=codiceSoggetto%>" />
		<liferay-portlet:param name="codiceArticolo"
			value="<%=articolo.getCodiceArticolo()%>" />
		<liferay-portlet:param name="jspPage"
			value="/jsps/agenti/item_view.jsp" />
	</liferay-portlet:renderURL>
	<liferay-ui:icon image="search" url="${itemURL}" label="true"
		message="view" />
	<c:if test="<%=handle%>">
		<liferay-portlet:renderURL var="handleItemURL">
			<liferay-portlet:param name="backUrl" value="<%=backUrl%>" />
			<liferay-portlet:param name="codiceArticolo"
				value="<%=articolo.getCodiceArticolo()%>" />
			<liferay-portlet:param name="jspPage"
				value="/jsps/agenti/handle-item.jsp" />
		</liferay-portlet:renderURL>
		<liferay-ui:icon image="view_tasks" url="${handleItemURL}"
			label="true" message="handle-item" />
	</c:if>
</liferay-ui:icon-menu>