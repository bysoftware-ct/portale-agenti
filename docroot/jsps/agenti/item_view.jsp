<%@page import="it.bysoftware.ct.service.OrdinatoLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.impl.OrdinatoLocalServiceImpl"%>
<%@page import="it.bysoftware.ct.model.Ordinato"%>
<%@page import="java.math.RoundingMode"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.bysoftware.ct.service.UltimiPrezziArticoliLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.impl.UltimiPrezziArticoliLocalServiceImpl"%>
<%@page import="it.bysoftware.ct.model.UltimiPrezziArticoli"%>
<%@page import="java.util.List"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page
	import="it.bysoftware.ct.service.ListiniPrezziArticoliLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.ListiniPrezziArticoli"%>
<%@page import="it.bysoftware.ct.service.persistence.ScontiArticoliPK"%>
<%@page import="it.bysoftware.ct.service.ScontiArticoliLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.ScontiArticoliLocalService"%>
<%@page import="it.bysoftware.ct.model.ScontiArticoli"%>
<%@page import="it.bysoftware.ct.service.VociIvaLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.VociIva"%>
<%@page import="it.bysoftware.ct.service.ArticoliLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Articoli"%>
<%@page
	import="it.bysoftware.ct.service.DatiClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.DatiClientiFornitori"%>
<%@include file="../init.jsp"%>

<%
    String backUrl = ParamUtil.getString(renderRequest, "backUrl");
    String codiceSoggetto = ParamUtil.getString(renderRequest,
		    "codiceSoggetto");
    String codiceArticolo = ParamUtil.getString(renderRequest,
		    "codiceArticolo");
    DatiClientiFornitori datiCliente = null;
    Articoli articolo = null;

    boolean error = false;
    boolean scontoDaANAART = true;
    double prezzo = 0.0;
    double sconto1 = 0.0;
    double sconto2 = 0.0;
    double sconto3 = 0.0;
    double aliquotaIva = 0.0;
    String codiceIva = "";
    List<UltimiPrezziArticoli> ultimeCondizioni = null;
    List<UltimiPrezziArticoli> ultimeCondizioniFornitore = null;
    if (codiceSoggetto != null && codiceArticolo != null
    	&& !codiceSoggetto.isEmpty() && !codiceArticolo.isEmpty()){
		datiCliente = DatiClientiFornitoriLocalServiceUtil.getDatiCliente(codiceSoggetto);
		articolo = ArticoliLocalServiceUtil.getArticoli(codiceArticolo);
		if (datiCliente.getPrezzoDaProporre() == 0) {
			ListiniPrezziArticoli listinoDedicato =
				ListiniPrezziArticoliLocalServiceUtil.getListinoDedicato(articolo.
					getCodiceArticolo(), "", codiceSoggetto);
			ListiniPrezziArticoli listinoGenerico =
				ListiniPrezziArticoliLocalServiceUtil.getListinoGenerico(articolo.getCodiceArticolo(), "");
			if (listinoDedicato != null){
			    if (!listinoDedicato.getCodiceIVA().equals("")) {
					VociIva iva = VociIvaLocalServiceUtil.fetchVociIva(listinoDedicato.getCodiceIVA());
					prezzo = listinoDedicato.getPrezzoNettoIVA() / (1+ (iva.getAliquota()/100));
			    } else {
					prezzo = listinoDedicato.getPrezzoNettoIVA();
			    }
			    if (listinoDedicato.getUtilizzoSconti()) {
					scontoDaANAART = false;
					sconto1 = listinoDedicato.getScontoListino1();
					sconto2 = listinoDedicato.getScontoListino2();
					sconto3 = listinoDedicato.getScontoListino3();
			    } 
			} else if (listinoGenerico != null){
			    if (!listinoGenerico.getCodiceIVA().equals("")) {
					VociIva iva = VociIvaLocalServiceUtil.fetchVociIva(listinoGenerico.getCodiceIVA());
					prezzo = listinoGenerico.getPrezzoNettoIVA() / (1+ (iva.getAliquota()/100));
			    } else {
					prezzo = listinoGenerico.getPrezzoNettoIVA();
			    }
			    if (listinoGenerico.getUtilizzoSconti()) {
					scontoDaANAART = false;
					sconto1 = listinoGenerico.getScontoListino1();
					sconto2 = listinoGenerico.getScontoListino2();
					sconto3 = listinoGenerico.getScontoListino3();
			    }
			}
		} else if (datiCliente.getPrezzoDaProporre() == 1) {
		    if (!articolo.getCodiceIVAPrezzo1().equals("")) {
				VociIva iva = VociIvaLocalServiceUtil.fetchVociIva(articolo.getCodiceIVAPrezzo1());
				prezzo = articolo.getPrezzo1() / (1 + (iva.getAliquota() / 100));
		    } else {
				prezzo = articolo.getPrezzo1();
		    }
		} else if (datiCliente.getPrezzoDaProporre() == 2) {
		    if (!articolo.getCodiceIVAPrezzo2().equals("")) {
				VociIva iva = VociIvaLocalServiceUtil.fetchVociIva(articolo.getCodiceIVAPrezzo2());
				prezzo = articolo.getPrezzo2() / (1 + (iva.getAliquota() / 100));
    		} else {
				prezzo = articolo.getPrezzo2();
    		}
		} else {
		    if (!articolo.getCodiceIVAPrezzo3().equals("")) {
				VociIva iva = VociIvaLocalServiceUtil.fetchVociIva(articolo.getCodiceIVAPrezzo3());
				prezzo = articolo.getPrezzo3() / (1 + (iva.getAliquota() / 100));
    		} else {
				prezzo = articolo.getPrezzo3();
    		}
		}
		if (scontoDaANAART) {
		    ScontiArticoli sc1 = ScontiArticoliLocalServiceUtil.fetchScontiArticoli(
			    new ScontiArticoliPK(datiCliente.getScontoCat1(), articolo.getCodiceSconto1())); 
		    ScontiArticoli sc2 = ScontiArticoliLocalServiceUtil.fetchScontiArticoli(
			    new ScontiArticoliPK(datiCliente.getScontoCat2(), articolo.getCodiceSconto2()));
		    sconto1 = (sc1 != null) ? sc1.getSconto() : 0.0;
		    sconto2 = (sc2 != null) ? sc2.getSconto() : 0.0;
		}
		VociIva iva = VociIvaLocalServiceUtil.fetchVociIva(articolo.getCodiceIVA());
		aliquotaIva = iva.getAliquota(); 
		codiceIva = iva.getCodiceIva();
		ultimeCondizioni = UltimiPrezziArticoliLocalServiceUtil.getUltimeCondizioniACliente(
			datiCliente.getCodiceSoggetto(), articolo.getCodiceArticolo(), "");
		ultimeCondizioniFornitore = UltimiPrezziArticoliLocalServiceUtil.getUltimeCondizioniDaFornitore(
			articolo.getCodiceArticolo(), "");	
    } else {
		error = true;
    }
    
    Ordinato ordinato = OrdinatoLocalServiceUtil.getQuantitaOridnata(codiceArticolo, "");
%>

<c:choose>
	<c:when test="<%=!error%>">
        <liferay-ui:header backURL="<%=backUrl %>" title="item" />
		<aui:layout>
			<aui:column columnWidth="90" first="true">
				<aui:input name="codiceArticolo" label="code" inlineField="true"
					value="<%=articolo.getCodiceArticolo()%>" />
				<aui:input name="descrizione" label="description"
					cssClass="input-xxlarge" inlineField="true"
					value="<%=articolo.getDescrizione()%>" />
				<c:if test="<%= !articolo.getLiberoString5().equals("") %>">
				    <liferay-portlet:renderURL varImpl="sheetURL">
                        <portlet:param name="mvcPath" value="/jsps/schedetecniche/edit-item-sheet.jsp" />
                        <portlet:param name="itemSheetId"
                            value="<%=articolo.getLiberoString5()%>" />
                    </liferay-portlet:renderURL>
				    <aui:a href="<%=sheetURL.toString() %>" target="_blank">Scheda Tecnica</aui:a>
				</c:if>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="50" first="true">
				<aui:fieldset label="current-price">
					<aui:input name="prezzo" label="price" cssClass="input-small"
						inlineField="true" value="<%=prezzo%>" />
					<aui:input name="sconto1" label="discount1" cssClass="input-small"
						inlineField="true" value="<%=sconto1%>" />
					<aui:input name="sconto2" label="discount2" cssClass="input-small"
						inlineField="true" value="<%=sconto2%>" />
					<aui:input name="sconto3" label="discount3" cssClass="input-small"
						inlineField="true" value="<%=sconto3%>" />
					<aui:input name="iva" label="IVA" cssClass="input-small"
						inlineField="true" value="<%=aliquotaIva%>" />
					<aui:input name="codiceIva" label="" type="hidden"
					   value="<%=articolo.getCodiceIVA()%>" />
					<aui:button name="selectBTN" type="button" value="select" />
				</aui:fieldset>
			</aui:column>
			<aui:column columnWidth="50" last="true">
				<liferay-portlet:renderURL varImpl="iteratorURL">
					<portlet:param name="mvcPath" value="/jsps/agenti/item_view.jsp" />
				</liferay-portlet:renderURL>
				<aui:fieldset label="last-price">
					<liferay-ui:search-container delta="20"
						emptyResultsMessage="no-last-condition-available"
						iteratorURL="<%= iteratorURL %>">
		 				<liferay-ui:search-container-results>
		 					<%
		 						results = ListUtil.subList(ultimeCondizioni,
									searchContainer.getStart(),
									searchContainer.getEnd());
								total = ultimeCondizioni.size();
								pageContext.setAttribute("results", results);
								pageContext.setAttribute("total", total);
		 					%>
						</liferay-ui:search-container-results>
						<liferay-ui:search-container-row
							className="it.bysoftware.ct.model.UltimiPrezziArticoli"
							modelVar="ultimaCondizione" >
							<liferay-ui:search-container-column-text property="prezzo"
								name="Prezzo" />
							<liferay-ui:search-container-column-text property="sconto1"
								name="discount1" />
							<liferay-ui:search-container-column-text property="sconto2"
								name="discount2" />
							<liferay-ui:search-container-column-text property="sconto3"
								name="discount3"/>
							<%
				    			SimpleDateFormat sdf = new SimpleDateFormat();
								sdf.applyLocalizedPattern("dd/MM/yyyy");
								String date = sdf.format(ultimaCondizione.getDataDocumento());
							%>
							<liferay-ui:search-container-column-text name="date"
								value="<%= date %>"/>
							<liferay-ui:search-container-column-button name="Seleziona"
								href="set('${ultimaCondizione.prezzo}|${ultimaCondizione.sconto1}|${ultimaCondizione.sconto2}|${ultimaCondizione.sconto3}')" />
						</liferay-ui:search-container-row>
						<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
							paginate="false" />
					</liferay-ui:search-container>
				</aui:fieldset>
			</aui:column>
		</aui:layout>
		<liferay-ui:panel-container extended="true">
			<liferay-ui:panel collapsible="true" defaultState="collapsed" title="supplier-section">
				<aui:layout>
					<aui:column columnWidth="50" first="true">
						<liferay-portlet:renderURL varImpl="iteratorURL">
							<portlet:param name="mvcPath" value="/jsps/agenti/item_view.jsp" />
						</liferay-portlet:renderURL>
						<aui:fieldset label="last-price-supplier">
							<liferay-ui:search-container delta="20"
								emptyResultsMessage="no-last-condition-available"
								iteratorURL="<%= iteratorURL %>">
				 				<liferay-ui:search-container-results>
				 					<%
				 						results = ListUtil.subList(ultimeCondizioniFornitore,
											searchContainer.getStart(),
											searchContainer.getEnd());
										total = ultimeCondizioni.size();
										pageContext.setAttribute("results", results);
										pageContext.setAttribute("total", total);
				 					%>
								</liferay-ui:search-container-results>
								<liferay-ui:search-container-row
									className="it.bysoftware.ct.model.UltimiPrezziArticoli"
									modelVar="ultimaCondizioneFornitore" >
									<liferay-ui:search-container-column-text property="prezzo"
										name="Prezzo" />
									<liferay-ui:search-container-column-text property="sconto1"
										name="discount1" />
									<liferay-ui:search-container-column-text property="sconto2"
										name="discount2" />
									<liferay-ui:search-container-column-text property="sconto3"
										name="discount3"/>
									<%
						    			SimpleDateFormat sdf = new SimpleDateFormat();
										sdf.applyLocalizedPattern("dd/MM/yyyy");
										String date =
											sdf.format(ultimaCondizioneFornitore.getDataDocumento());
										 
										double prezzo1 = ultimaCondizioneFornitore.getPrezzo() * (1 + (ultimaCondizioneFornitore.getSconto1() / 100));
										double prezzo2 = prezzo1 * (1 + (ultimaCondizioneFornitore.getSconto2() / 100));
										double prezzoNetto = prezzo2 * (1 + (ultimaCondizioneFornitore.getSconto3() / 100));
										DecimalFormat decForm = new DecimalFormat("#.###", new DecimalFormatSymbols());
		                                decForm.setRoundingMode(RoundingMode.CEILING);
									%>
									<liferay-ui:search-container-column-text name="date"
										value="<%= date %>"/>
									<liferay-ui:search-container-column-text name="price"
										value="<%= decForm.format(prezzoNetto)%>"/>
								</liferay-ui:search-container-row>
								<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
									paginate="false" />
							</liferay-ui:search-container>
						</aui:fieldset>
					</aui:column>
					<aui:column columnWidth="50" last="true">
					   <aui:fieldset label="supplier-ordered">
					       <aui:input name="ordered" label="ordered"
					           value="<%= (ordinato != null) ? ordinato.getQuantitaOrdinataFornitore() : ""%>"/>
				       </aui:fieldset>
					</aui:column>
				</aui:layout>
			</liferay-ui:panel>
		</liferay-ui:panel-container>
	</c:when>
	<c:otherwise>
		<div class="portlet-msg-info">
			<liferay-ui:message key="problem" />
		</div>
	</c:otherwise>
</c:choose>

<aui:script>
var codiceArticolo = '<%=articolo.getCodiceArticolo()%>';
var descrizione = '<%=articolo.getDescrizione()%>';
var unitaMisura = '<%=articolo.getUnitaMisura()%>';
var prezzo = '<%=prezzo%>';
var sconto1 = '<%=sconto1%>';
var sconto2 = '<%=sconto2%>';
var sconto3 = '<%=sconto3%>';
var aliquotaIVA = '<%=aliquotaIva%>';
var codiceIVA = '<%=codiceIva%>';

AUI().use('node', function (A) {
    A.one('#<portlet:namespace/>selectBTN').on('click', function () {
    	var data = codiceArticolo + '|' + descrizione + '|' + unitaMisura + '|'
			+ prezzo + '|' + sconto1 + '|' + sconto2 + '|' + sconto3 + '|'
			+ aliquotaIVA + '|' + codiceIVA;
		//console.log("Codice Articolo: " + data);
		Liferay.Util.getOpener().closePopup(data,
			'<portlet:namespace/>itemDialog');
    });
});
</aui:script>

<script type="text/javascript"> 
	function set(data) {
		data = codiceArticolo + '|' + descrizione + '|' + unitaMisura + '|'
		+ data + '|' + aliquotaIVA + '|' + codiceIVA;
		console.log(data);
		Liferay.Util.getOpener().closePopup(data,
			'<portlet:namespace/>itemDialog');
	}
</script>
