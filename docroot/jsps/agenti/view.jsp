<%@page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@page import="it.bysoftware.ct.service.AnagraficaAgentiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficaAgenti"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.service.ClassNameLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoValue"%>
<%@page
	import="com.liferay.portlet.expando.service.
	ExpandoValueLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficaClientiFornitori"%>
<%@page import="it.bysoftware.ct.model.DatiClientiFornitori"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page
	import="it.bysoftware.ct.service.
	AnagraficaClientiFornitoriLocalServiceUtil"%>
<%@page
	import="it.bysoftware.ct.service.DatiClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@include file="../init.jsp"%>

<%
	String comune = ParamUtil.getString(renderRequest, "comune", null);
	String ragSociale = ParamUtil.getString(renderRequest,
			"ragioneSociale", null);
	String codiceSoggetto = ParamUtil.getString(renderRequest,
	        "codiceSoggetto", null);
	
	ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
			user.getCompanyId(),
			ClassNameLocalServiceUtil.getClassNameId(User.class),
			"CUSTOM_FIELDS", Constants.CODICE_AGENTE, user.getUserId());
	String codiceAgente = value.getData();

	List<AnagraficaClientiFornitori> clienti =
		AnagraficaClientiFornitoriLocalServiceUtil.getClientiByCodiceAgente(
			codiceAgente, comune, ragSociale, codiceSoggetto);
%>

<liferay-portlet:renderURL varImpl="iteratorURL">
	<portlet:param name="mvcPath" value="/jsps/agenti/view.jsp" />
	<portlet:param name="comune" value="<%= codiceSoggetto %>"/>
	<portlet:param name="comune" value="<%= comune %>"/>
	<portlet:param name="ragioneSociale" value="<%= ragSociale %>"/>
</liferay-portlet:renderURL>

<liferay-portlet:renderURL varImpl="searchURL">
	<portlet:param name="mvcPath" value="/jsps/agenti/view.jsp" />
</liferay-portlet:renderURL>

<aui:form action="<%=searchURL%>" method="get" name="fm">
	<liferay-portlet:renderURLParams varImpl="searchURL" />

	<div class="search-form">
		<span class="aui-search-bar">
		  <aui:input inlineField="true" label="Cod. Cliente" name="codiceSoggetto"
                size="30" title="search-entries" type="text" /> 
		  <aui:input inlineField="true" label="Rag. Sociale" name="ragioneSociale"
				size="30" title="search-entries" type="text" />
		  <aui:input inlineField="true" label="Comune" name="comune" size="30"
				title="search-entries" type="text" /> 
		  <aui:button type="submit" value="search" />
		</span>
	</div>
</aui:form>

<liferay-ui:search-container delta="20"
	emptyResultsMessage="no-client" iteratorURL="<%=iteratorURL%>">
 	<liferay-ui:search-container-results>
 		<%
 			results = ListUtil.subList(clienti,
					searchContainer.getStart(),
					searchContainer.getEnd());
			total = clienti.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
 		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row
		className="it.bysoftware.ct.model.AnagraficaClientiFornitori"
		modelVar="cliente">
		<liferay-ui:search-container-column-text property="codiceSoggetto"
			name="Codice" />
		<liferay-ui:search-container-column-text property="ragioneSociale"
			name="Rag. Sociale" />
		<liferay-ui:search-container-column-text property="comune"
			name="Comune" />
		<liferay-ui:search-container-column-jsp align="right"
			path="/jsps/agenti/client-action.jsp"/>
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
		paginate="true" />
</liferay-ui:search-container>