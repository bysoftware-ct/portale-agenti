<%@page import="it.bysoftware.ct.agenti.ResourceId"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Date"%>
<%@page import="com.liferay.portal.kernel.util.UnicodeFormatter"%>
<%@page import="it.bysoftware.ct.model.AnagraficaClientiFornitori"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficaClientiFornitoriLocalServiceUtil"%>
<%@include file="../init.jsp"%>

<%
    String codiceSoggetto = ParamUtil.getString(renderRequest,
            "codiceSoggetto", null);
    AnagraficaClientiFornitori cliente = null;
    if (codiceSoggetto != null) {
        cliente = AnagraficaClientiFornitoriLocalServiceUtil
           .getAnagraficaClientiFornitori(codiceSoggetto);
    }
    
    Date date = new Date(new java.util.Date().getTime());

    SimpleDateFormat sdf = new SimpleDateFormat();
    sdf.applyLocalizedPattern("dd/MM/yyyy");
    String header = sdf.format(date) + " - " + cliente.getRagioneSociale();
%>
<liferay-ui:header title="fill-note-document" />

<aui:fieldset label="client-info">
	<aui:input name="ragSociale" label="customer"
		value="<%=cliente.getRagioneSociale()%>" cssClass="input-xxlarge"
		inlineField="true" />
	<%
	    String indirizzo = cliente.getIndirizzo() + " - "
			        + cliente.getCAP() + " " + cliente.getComune() + " ("
			        + cliente.getSiglaProvincia() + ") - "
			        + cliente.getSiglaStato();
	%>
	<aui:input name="indirizzo" label="address" value="<%=indirizzo%>"
		cssClass="input-xxlarge" inlineField="true" />
</aui:fieldset>
<portlet:resourceURL var="saveNoteURL" 
        id="<%=ResourceId.saveNote.name() %>"/>        
<aui:field-wrapper label="note-document">
	<liferay-ui:input-editor name="descriptionEditor"
		toolbarSet="simple" initMethod="initEditor" width="100%" />
	<aui:button id="btnUpload" name="Save" value="save" type="submit" />
</aui:field-wrapper>
<div class="yui3-skin-sam">
    <div id="modal"></div>
</div>
<aui:script>
	function <portlet:namespace />initEditor() {
		return '<%= UnicodeFormatter.toString(header)%>';
	}

	AUI().use("aui-base", "aui-io-request", "aui-modal", function(A) {
		var modal = new A.Modal({
            bodyContent : '<div class="loading-animation"/>',
            centered : true,
            headerContent : '<h3>Loading...</h3>',
            modal : true,
            render : '#modal',
            close: false,
            width : 450
        }).render();
		modal.hide();
	    if(A.one('#<portlet:namespace />Save') !== null) {
	        A.one('#<portlet:namespace />Save').on('click', function(event) {
	        	modal.show()
	        	var clientCode = '<%= codiceSoggetto %>';
	        	var descriptionEditor = window.<portlet:namespace />descriptionEditor.getHTML();
	        	A.io.request('${saveNoteURL}', {
	        		method: 'post',
	        		data: { 
	        			<portlet:namespace/>clientCode: clientCode,
	        			<portlet:namespace/>descriptionEditor: descriptionEditor
	        		}, 
	        		on: {
	        			success: function() { 
	        				modal.hide();
	        				var data = JSON.parse(this.get('responseData'));
	                        console.log(data);
	        				if (data.code === 0) {
	        					alert("Salvataggio e notifica del documento "
	        							 + "completati con successo!");
	        				} else {
	        					alert("Si e\' verificato un errore nel salvataggio"
                                        + " o nella notifica del documento.\n\n"
                                        + data.message);
	        				}
	        			},
	        			failure: function () {
	                        modal.hide();
	                        alert("Si e' verificato nel salvataggio del documento.");
	                    }
	        		}
	        	});
	        });
	    }
	});
</aui:script>