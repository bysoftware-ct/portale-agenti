<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="it.bysoftware.ct.service.EvasioniOrdiniLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.EvasioniOrdini"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>

<%
    int anno = ParamUtil.getInteger(renderRequest, "anno");
    String codiceAttivita = ParamUtil.getString(renderRequest,
		    "codiceAttivita", "");
    String codiceCentro = ParamUtil.getString(renderRequest,
		    "codiceCentro", "");
    String codiceDeposito = ParamUtil.getString(renderRequest,
		    "codiceDeposito", "");
    int tipoOrdine = ParamUtil.getInteger(renderRequest, "tipoOrdine");
    int numeroOrdine = ParamUtil.getInteger(renderRequest,
		    "numeroOrdine");
    String codiceCliente = ParamUtil.getString(renderRequest,
		    "codiceCliente", "");

    List<EvasioniOrdini> list = EvasioniOrdiniLocalServiceUtil
		    .getEvasioniByCodiceCliente(codiceCliente, anno,
			    codiceAttivita, codiceCentro, codiceDeposito,
			    tipoOrdine, numeroOrdine);
%>

<liferay-portlet:renderURL varImpl="iteratorURL">
	<portlet:param name="mvcPath" value="/jsps/agenti/order-rows-state.jsp" />
	<portlet:param name="anno" value="<%=String.valueOf(anno)%>" />
	<portlet:param name="codiceAttivita" value="<%=codiceAttivita%>" />
	<portlet:param name="codiceCentro" value="<%=codiceCentro%>" />
	<portlet:param name="codiceDeposito" value="<%=codiceDeposito%>" />
	<portlet:param name="tipoOrdine"
		value="<%=String.valueOf(tipoOrdine)%>" />
	<portlet:param name="numeroOrdine"
		value="<%=String.valueOf(numeroOrdine)%>" />
	<portlet:param name="codiceCliente" value="<%=codiceCliente%>" />
</liferay-portlet:renderURL>

<liferay-ui:header title="order-detail" />
<liferay-ui:search-container delta="1000"
	emptyResultsMessage="no-articoli" iteratorURL="<%=iteratorURL%>">
	<liferay-ui:search-container-results>
		<%
		    results = ListUtil.subList(list,
					    searchContainer.getStart(),
					    searchContainer.getEnd());
				    total = list.size();
				    pageContext.setAttribute("results", results);
				    pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	<%
	    Map<String, ArrayList<Double>> residui = new HashMap<String, ArrayList<Double>>();
	%>
	<liferay-ui:search-container-row
		className="it.bysoftware.ct.model.EvasioniOrdini" modelVar="evasione">
		<%
		    String nDoc = "";
		    if (evasione.getNumeroDocumentoEvasione() != 0) {
			   nDoc = String.valueOf(evasione.getNumeroDocumentoEvasione());
		    }

			String typeDoc = "";
			if (evasione.getTipoDocumentoEvasione() != null) {
			    typeDoc = evasione.getTipoDocumentoEvasione();
			}
		%>
		<liferay-ui:search-container-column-text value="<%=nDoc%>" name="N." />
		<liferay-ui:search-container-column-text value="<%=typeDoc%>"
			name="Documento" />
		<%
		    SimpleDateFormat sdf = new SimpleDateFormat();
		    sdf.applyLocalizedPattern("dd/MM/yyyy");
		    String dataDoc = "";
		    if (evasione.getDataDocumentoConsegna() != null) {
				dataDoc = sdf.format(evasione.getDataDocumentoConsegna());
		    }
		    String codArt = "";
		    String codVar = "";
		    String descri = "";
		    String uniMis = "";
		    String qntOrd = "";
		    double residuo = 0;
		    ArrayList<Double> quantita = null;
		    if (residui.keySet().contains(evasione.getCodiceArticolo() + "_" + evasione.getCodiceVariante())) {
				quantita = residui.get(evasione.getCodiceArticolo()	+ "_" + evasione.getCodiceVariante());
				residuo = quantita.get(quantita.size() - 1)	- evasione.getQuantitaConsegnata();
			} else {
				quantita = new ArrayList<Double>();
				residuo = evasione.getQuantitaOrdinata() - evasione.getQuantitaConsegnata();
				residui.put(evasione.getCodiceArticolo() + "_" + evasione.getCodiceVariante(), quantita);
				codArt = evasione.getCodiceArticolo();
				codVar = evasione.getCodiceVariante();
				descri = evasione.getDescrizione();
				uniMis = evasione.getUnitaMisura();
				qntOrd = String.valueOf(evasione.getQuantitaOrdinata());
		    }
		    quantita.add(residuo);
		%>
		<liferay-ui:search-container-column-text value="<%=dataDoc%>"
			name="Data Evasione" />
		<liferay-ui:search-container-column-text value="<%=codArt%>"
			name="Articolo" />
		<liferay-ui:search-container-column-text value="<%=descri%>"
			name="Descrizione" />
		<liferay-ui:search-container-column-text value="<%=uniMis%>"
			name="Un. Mis." />
		<liferay-ui:search-container-column-text value="<%=qntOrd%>"
			name="Q. Ord." />
		<liferay-ui:search-container-column-text property="quantitaConsegnata"
			name="Q. Con." />

		<liferay-ui:search-container-column-text
			value="<%=String.valueOf(residuo)%>" name="Residuo" />
		<%
		    String value = "";
				    if ("S".equals(evasione.getTestAccontoSaldo())) {
					value = "SALDO";
				    } else if ("A".equals(evasione.getTestAccontoSaldo())) {
					value = "ACCONTO";
				    }
		%>
		<liferay-ui:search-container-column-text value="<%=value%>" name="" />
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
		paginate="false" />
</liferay-ui:search-container>
