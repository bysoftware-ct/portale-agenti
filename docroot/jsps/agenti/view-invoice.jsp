<%@page import="java.sql.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.NumberFormat"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficaClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficaClientiFornitori"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page
	import="it.bysoftware.ct.service.RigheFattureVeditaLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.RigheFattureVedita"%>
<%@page import="java.util.List"%>
<%@page
	import="it.bysoftware.ct.service.persistence.TestataFattureClientiPK"%>
<%@page
	import="it.bysoftware.ct.service.TestataFattureClientiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.TestataFattureClienti"%>
<%@include file="../init.jsp"%>

<%
    int esercizio = ParamUtil.getInteger(renderRequest,
            "esercizioDocumento");
    String codiceAttivita = ParamUtil.getString(renderRequest,
            "codiceAttivita");
    String codiceCentro = ParamUtil.getString(renderRequest,
            "codiceCentro");
    int numeroDocumento = ParamUtil.getInteger(renderRequest,
            "numeroDocumento");
    String codiceCliente = ParamUtil.getString(renderRequest,
            "codiceCliente");
    long dataLong = ParamUtil.getLong(renderRequest, "dataDocumento");
    
    AnagraficaClientiFornitori cliente = AnagraficaClientiFornitoriLocalServiceUtil
            .getAnagraficaClientiFornitori(codiceCliente);
    TestataFattureClienti testataFattura = TestataFattureClientiLocalServiceUtil
            .getFattureByAnnoAttivitaDocumento(esercizio, codiceAttivita,
                    codiceCentro, numeroDocumento, new Date(dataLong), false,
                    codiceCliente);
    List<RigheFattureVedita> righe = new ArrayList<RigheFattureVedita>();
    SimpleDateFormat sdf = new SimpleDateFormat();
    
    String indirizzo = "";
    if (testataFattura != null) {
        sdf.applyLocalizedPattern("dd/MM/yyyy");
        righe = RigheFattureVeditaLocalServiceUtil
                .getRigheFatturaByTestata(testataFattura);
        indirizzo = cliente.getIndirizzo() + " - "
                + cliente.getCAP() + " " + cliente.getComune()
                + " (" + cliente.getSiglaProvincia() + ") - "
                + cliente.getSiglaStato();
    }
    NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
    String backURL = ParamUtil.getString(renderRequest, "backURL");
%>

<liferay-ui:header title="view-invoice" backURL="<%=backURL %>"/>

<c:choose>
	<c:when test="<%=testataFattura != null %>">
		<aui:fieldset label="invoice-header">
		    <aui:input name="number" label="number"
		        value="<%=testataFattura.getNumeroDocumento()%>" disabled="true"
		        cssClass="input-small" inlineField="true" />
		    <aui:input name="invoiceDate" label="date"
		        value="<%=sdf.format(testataFattura.getDataDocumento())%>" disabled="true"
		        inlineField="true" />
			<aui:input name="ragSociale" label="customer" disabled="true"
			    value="<%=cliente.getRagioneSociale()%>" cssClass="input-xxlarge"
			    inlineField="true" />
			<aui:input name="indirizzo" label="address" value="<%=indirizzo%>"
			    disabled="true" cssClass="input-xxlarge" inlineField="true" />
		</aui:fieldset>
		<liferay-ui:search-container delta="200" emptyResultsMessage="no-invoice" >
			<liferay-ui:search-container-results>
				<%
				    results = ListUtil.subList(righe,
				                    searchContainer.getStart(),
				                    searchContainer.getEnd());
				            total = righe.size();
				            pageContext.setAttribute("results", results);
				            pageContext.setAttribute("total", total);
				%>
			</liferay-ui:search-container-results>
			<liferay-ui:search-container-row className="RigheFattureVedita"
				modelVar="rigo">
				<liferay-ui:search-container-column-text property="codiceArticolo"
					name="code" />
				<liferay-ui:search-container-column-text property="descrizione"
					name="description" />
				<liferay-ui:search-container-column-text property="unitaMisura"
					name="unit-mis" />
				<liferay-ui:search-container-column-text property="quantita"
					name="quantity" />
				<liferay-ui:search-container-column-text
					value="<%=fmt.format(rigo.getPrezzo())%>" name="price" />
				<%
				    String sconto = "";
		            if (rigo.getSconto1() != 0) {
		                sconto += sconto + rigo.getSconto1() + "%";
		            }
		            if (rigo.getSconto2() != 0) {
		                sconto += sconto + rigo.getSconto2() + "%";
		            }
		            if (rigo.getSconto3() != 0) {
		                sconto += sconto + rigo.getSconto3() + "%";
		            }
				%>
				<liferay-ui:search-container-column-text value="<%=sconto%>"
					name="discount" />
				<liferay-ui:search-container-column-text
					value="<%=fmt.format(rigo.getImportoNettoRigo())%>" name="import" />
				<liferay-ui:search-container-column-text
					value="<%=rigo.getCodiceIVAFatturazione()%>" name="VAT-code" />
			</liferay-ui:search-container-row>
			<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
				paginate="false" />
		</liferay-ui:search-container>
		<aui:input name="grossAmount" label="gross-amount"
		        value="<%=fmt.format(testataFattura.getImponibile())%>" disabled="true"
		        inlineField="true" />
		<aui:input name="VAT" label="VAT"
		        value="<%=fmt.format(testataFattura.getIVA())%>" disabled="true"
		        inlineField="true" />
		<aui:input name="amount" label="total" 
		        value="<%=fmt.format(testataFattura.getImportoFattura())%>" disabled="true"
		        inlineField="true" />
    </c:when>
    <c:otherwise>
        <liferay-ui:message key="no-invoice-found" />
    </c:otherwise>
</c:choose>