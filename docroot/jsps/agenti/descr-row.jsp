<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@include file="../init.jsp"%>

<aui:input name="descrRow" type="textarea" label="add-descr-row" cssClass="input-xxlarge" />
<aui:input id="selectBtn" name="selectBtn" type="submit" label=""
	value="<%=LanguageUtil.get(themeDisplay.getLocale(), "confirm")%>" />

<aui:script>
	AUI().use('aui-base', function(A) {
		A.one('#<portlet:namespace/>selectBtn').on('click', function() {
			var description = A.one(
					'#<portlet:namespace/>descrRow').val();
			Liferay.Util.getOpener().closePopup(description,
					'<portlet:namespace/>descrRowDialog');
		});
	});
</aui:script>
	

