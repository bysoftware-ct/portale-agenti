<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="java.math.RoundingMode"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="it.bysoftware.ct.service.VociIvaLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.VociIva"%>
<%@page import="com.liferay.portal.kernel.json.JSONArray"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="it.bysoftware.ct.agenti.ResourceId"%>
<%@page import="it.bysoftware.ct.model.AnagraficaClientiFornitori"%>
<%@page import="it.bysoftware.ct.model.DatiClientiFornitori"%>
<%@page import="it.bysoftware.ct.model.PianoPagamenti"%>
<%@page import="it.bysoftware.ct.model.WKRigheOrdiniClienti"%>
<%@page import="it.bysoftware.ct.model.WKOrdiniClienti"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficaClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.DatiClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.PianoPagamentiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.WKRigheOrdiniClientiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.persistence.WKOrdiniClientiPK"%>
<%@page import="it.bysoftware.ct.service.WKOrdiniClientiLocalServiceUtil"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>

<%
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    String codiceSoggetto = ParamUtil.getString(renderRequest,
		    "codiceSoggetto", null);
    boolean noObsolete = ParamUtil.getBoolean(renderRequest, "noObsolete", true);
    AnagraficaClientiFornitori cliente = null;
    PianoPagamenti pagamentoCliente = null;
    WKOrdiniClienti ordine = null;
    JSONArray jsonArr = null;
    boolean editFlag = ParamUtil.getBoolean(renderRequest, "editFlag", false);
    boolean saveEnabledFlag = ParamUtil.getBoolean(renderRequest, "saveEnabledFlag", true);
    Date orderDate = Calendar.getInstance().getTime();
    Date deliveryDate = null;
    String email = "";
    DatiClientiFornitori datiCliente = null;
    if (codiceSoggetto != null) {
		cliente = AnagraficaClientiFornitoriLocalServiceUtil
			.getAnagraficaClientiFornitori(codiceSoggetto);
		datiCliente = DatiClientiFornitoriLocalServiceUtil.
			getDatiCliente(cliente.getCodiceSoggetto());
		pagamentoCliente = PianoPagamentiLocalServiceUtil.fetchPianoPagamenti(
			datiCliente.getCodicePagamento());
		if (editFlag) {
		    int anno = ParamUtil.getInteger(renderRequest, "anno", -1);
		    int tipoOrdine = ParamUtil.getInteger(renderRequest, "tipoOrdine", -1);
		    int numeroOrdine = ParamUtil.getInteger(renderRequest, "numeroOrdine", -1);
		    if (anno != -1 && tipoOrdine != -1 && numeroOrdine != -1){
		    	ordine = WKOrdiniClientiLocalServiceUtil.getWKOrdiniClienti(
		    		new WKOrdiniClientiPK(anno, tipoOrdine, numeroOrdine));
		    	List<WKRigheOrdiniClienti> righeOrdine =
		    		WKRigheOrdiniClientiLocalServiceUtil.
		    		getByAnnoTipoNumeroOrdine(anno, tipoOrdine, numeroOrdine);
		    	
		    	jsonArr = JSONFactoryUtil.createJSONArray();
		    	for (WKRigheOrdiniClienti rigo : righeOrdine) {

		            JSONObject json = JSONFactoryUtil.createJSONObject();
		            json.put("codiceArticolo", rigo.getCodiceArticolo());
		            json.put("descrizione", rigo.getDescrizione());
		            json.put("unitaMisura", rigo.getCodiceUnitMis());
		            json.put("quantita", rigo.getQuantita());
		            json.put("prezzo", rigo.getPrezzo());
		            json.put("sconto1", -1 * rigo.getSconto1());
		            json.put("sconto2", -1 * rigo.getSconto2());
		            json.put("sconto3", -1 * rigo.getSconto3());
		            json.put("codiceIVA", rigo.getCodiceIVAFatturazione());

		            double importoLordo = rigo.getPrezzo() * rigo.getQuantita();
		            double importo1 = importoLordo
		            	+ ((importoLordo * rigo.getSconto1()) / 100);
		            double importo2 = importo1 + ((importo1 * rigo.getSconto2()) / 100);
		            double importoNetto = importo2 + ((importo2 * rigo.getSconto3()) / 100);
		            VociIva iva = VociIvaLocalServiceUtil.fetchVociIva(rigo.getCodiceIVAFatturazione());
		            double aliquotaIva = (iva != null) ? iva.getAliquota() : 0;
					double importoIvato =  importoNetto * (1 + (aliquotaIva / 100));
					
					DecimalFormat decForm = new DecimalFormat("#.###", new DecimalFormatSymbols());
				    decForm.setRoundingMode(RoundingMode.CEILING); 
					json.put("importoLordo", decForm.format(importoLordo));
					DecimalFormat decForm1 = new DecimalFormat("#.##", new DecimalFormatSymbols());
					decForm1.setRoundingMode(RoundingMode.CEILING);
		            json.put("importoNetto", decForm1.format(importoNetto));
		            json.put("aliquotaIva", aliquotaIva);
		            json.put("importoIvato", decForm.format(importoIvato));
		            json.put("tipoRigo", rigo.getTipoRigo());
		            jsonArr.put(json);
		            orderDate = ordine.getDataDocumento();
		            deliveryDate = rigo.getDataPrevistaConsegna();
		            pagamentoCliente = PianoPagamentiLocalServiceUtil.fetchPianoPagamenti(ordine.getCodicePianoPag());
		        }
		    }
		}
    }
    int countPag = PianoPagamentiLocalServiceUtil.getPianoPagamentisCount();
    List<PianoPagamenti> pagamenti =
	    PianoPagamentiLocalServiceUtil.getPianoPagamentis(0, countPag);

    if (deliveryDate == null) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(orderDate);
        calendar.add(Calendar.DAY_OF_YEAR, 7);
        deliveryDate = calendar.getTime();
    }
    
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

%>
<liferay-portlet:renderURL var="itemURL"
	windowState="<%=LiferayWindowState.POP_UP.toString()%>">
	<liferay-portlet:param name="editing" value="true" />
	<liferay-portlet:param name="handle" value="false" />
	<liferay-portlet:param name="codiceSoggetto" value="<%=codiceSoggetto %>"/>
	<liferay-portlet:param name="mvcPath"
		value="/jsps/agenti/items_search.jsp" />
</liferay-portlet:renderURL>
<liferay-portlet:renderURL var="descrRowURL"
    windowState="<%=LiferayWindowState.POP_UP.toString()%>">
    <liferay-portlet:param name="mvcPath"
        value="/jsps/agenti/descr-row.jsp" />
</liferay-portlet:renderURL>
<liferay-portlet:renderURL var="priceURL"
	windowState="<%=LiferayWindowState.POP_UP.toString()%>">
	<liferay-portlet:param name="codiceSoggetto" value="<%= codiceSoggetto %>"/>
	<liferay-portlet:param name="mvcPath"
		value="/jsps/agenti/item_view.jsp" />
</liferay-portlet:renderURL>
<liferay-portlet:renderURL var="backURL" >
	<liferay-portlet:param name="codiceSoggetto" value="<%=codiceSoggetto %>"/>
	<liferay-portlet:param name="mvcPath"
		value="/jsps/agenti/orders.jsp" />
</liferay-portlet:renderURL>
<liferay-ui:header backURL="<%=backURL %>"
    title="<%=(!saveEnabledFlag) ? "view-order" : ((!editFlag) ? "new-order" : "edit-order" )%>" />
<aui:fieldset label="fill-order">
	<portlet:resourceURL var="saveOrderURL" 
		id="<%=ResourceId.saveOrder.name() %>" />
	<portlet:resourceURL var="printOrderURL" 
        id="<%=ResourceId.printOrder.name() %>" />
    <portlet:resourceURL var="sendOrderURL" 
         id="<%=ResourceId.sendOrder.name() %>" >
         <portlet:param name="origEmail" value="<%=email %>"/>
    </portlet:resourceURL>
    
    <c:if test="<%=datiCliente.getBloccato() == Constants.BLOCKED_ALERT %>">
         <div class="portlet-msg-alert">
             Il soggetto risulta essere bloccato con avviso.
         </div>
    </c:if>
    <c:if test="<%=datiCliente.getBloccato() == Constants.BLOCKED %>">
         <div class="portlet-msg-error">
             Il soggetto risulta essere bloccato.
         </div>
    </c:if>
    <c:if test="<%=datiCliente.getBloccato() == Constants.OBSOLETE %>">
         <div class="portlet-msg-info">
             Il soggetto risulta essere obsoleto.
         </div>
    </c:if>
    
	<aui:nav-bar>
		<aui:nav>
		    <c:if test="<%=saveEnabledFlag %>">
			     <aui:nav-item id="saveBtn" iconCssClass="icon-save" label="save"
				    selected='<%="save".equals(toolbarItem)%>' />
			</c:if>
			<aui:nav-item id="printBtn" iconCssClass="icon-print" label="print"
				selected='<%="print".equals(toolbarItem)%>' />
			<aui:nav-item id="sendBtn" iconCssClass="icon-envelope" label="send"
                selected='<%="send".equals(toolbarItem)%>' />
		</aui:nav>
	</aui:nav-bar>
	<aui:layout>
		<aui:column columnWidth="70"
			cssClass="detail-column detail-column-first">
			<aui:input id="codiceClienteTxt" type="text" name="codCliente"
				label="code" cssClass="input-small" disabled="true"
				inlineField="true" value="<%=cliente.getCodiceSoggetto()%>" />
			<aui:input id="clienteTxt" type="text" name="cliente" label="customer"
				cssClass="input-xxlarge" inlineField="true"
				value="<%=cliente.getRagioneSociale()%>" />
			<%
			    String indirizzo = cliente.getIndirizzo() + " - "
					+ cliente.getCAP() + " " + cliente.getComune()
					+ " (" + cliente.getSiglaProvincia() + ") - "
					+ cliente.getSiglaStato();
			%>
			<aui:input id="destinazioneTxt" type="text" name="destinazione"
				label="address" cssClass="input-xxlarge" value="<%=indirizzo%>"
				inlineField="true" />
			<aui:input id="codicePagam" type="hidden" name="codicePagam" inlineField="true"
				label="" value="<%=pagamentoCliente != null ? pagamentoCliente.getCodicePianoPagamento() : "CON" %>"/>
			<aui:select name="tipoPagam" id="tipoPagam" label="Tipo Pagamento"
				inlineField="true">
				<%
				    for (int i = 0; i < countPag; i++) {
					    PianoPagamenti pagamento = pagamenti.get(i);
				%>
				<aui:option label="<%=pagamento.getDescrizione()%>"
					value="<%=pagamento.getCodicePianoPagamento()%>"
					selected="<%=pagamento.getCodicePianoPagamento().
						equals(pagamentoCliente != null ? pagamentoCliente.getCodicePianoPagamento() : "CON") %>" />
				<%
				    }
				%>
			</aui:select>
			<aui:input id="emailTxt" type="email" name="email" label="email"
                inlineField="true" value="<%=email%>" />
			<aui:input id="orderDate" type="text" name="dataOrdine"
				label="order-date" inlineField="true"
				value="<%=sdf.format(orderDate)%>" />
			<aui:input id="deliveryDate" type="text" name="dataConsegna"
				label="delivery-date" inlineField="true"
				value="<%=sdf.format(deliveryDate)%>" />
		</aui:column>
		<aui:column columnWidth="20" cssClass="test" last="true">
			<aui:input type="text" name="nDoc" label="N. Documento" 
				disabled="true"	style="width: 90%" value="<%=(editFlag) ? ordine.getNumeroOrdine() : "" %>"/>
		</aui:column>
	</aui:layout>
	<portlet:resourceURL id="<%=ResourceId.autoComplete.toString() %>" var="getItems" />
	<portlet:resourceURL id="<%=ResourceId.loadItemInfo.toString() %>" var="getItemInfo" />
	<c:if test="<%=saveEnabledFlag %>">
		<aui:nav-bar>
			<aui:nav>
				<aui:nav-item id="searchBtn" iconCssClass="icon-search" label="advanced-search"
					selected='<%="add".equals(toolbarItem)%>' />
				<aui:nav-item id="addDescrBtn" iconCssClass="icon-edit" label="add-descr-row"
                    selected='<%="add-descr-row".equals(toolbarItem)%>' />
				<aui:nav-item id="deleteBtn" iconCssClass="icon-trash" label="delete"
					selected='<%="delete".equals(toolbarItem)%>' />
			</aui:nav>
		</aui:nav-bar>
		<aui:fieldset>
		  <aui:layout  cssClass="test test-padd" label="">
		      <aui:column first="true" columnWidth="45">
			      <aui:input id="itemCode" name="itemCode" label="item" cssClass="input-xlarge" inlineField="true" 
	                helpMessage="item-code-help" />
	              <aui:input id="noObsolete" name="noObsolete" label="no-obsolete" type="checkbox" value="<%=noObsolete %>"
	                inlineField="true" helpMessage="no-obsolete-help"/>
	              <aui:input id="itemDescription" name="itemDescription" disabled="true"
	                inlineField="true" label="description" />
                  <aui:input id="itemUnMeasurement" name="itemUnMeasurement" disabled="true"
                    inlineField="true" cssClass="input-small" label="Un. Mis." />
	              <aui:input id="itemQuantity" name="itemQuantity" label="quantity"
	                cssClass="input-small" inlineField="true"/>
              </aui:column>
              <aui:column last="true" columnWidth="45">
	              <aui:input id="itemPrice" name="itemPrice" disabled="true" type="hidden"
	                cssClass="input-small" />
	              <aui:input id="itemVATCode" name="itemVATCode" type="hidden"
                    cssClass="input-small" />
                  <aui:input id="itemPriceTXT" name="itemPriceTXT" disabled="true"
                    inlineField="true" cssClass="input-large" label="price" />
                  <aui:input id="itemVAT" name="itemVAT" disabled="true" inlineField="true"
                    cssClass="input-small" label="VAT" />
	              <aui:input id="itemDiscount1" name="itemDiscount1" disabled="true"
	                cssClass="input-small" label="discount1" inlineField="true" />
	              <aui:input id="itemDiscount2" name="itemDiscount2" disabled="true"
	                cssClass="input-small" label="discount2" inlineField="true" />
	              <aui:input id="itemDiscount3" name="itemDiscount3" disabled="true"
	                cssClass="input-small" label="discount3" inlineField="true"/>
	              <liferay-portlet:renderURL varImpl="sheetURL">
                    <portlet:param name="mvcPath" value="/jsps/schedetecniche/edit-item-sheet.jsp" />
                  </liferay-portlet:renderURL>
                  <aui:a id="itemSheetLink" href="<%=sheetURL.toString() %>" cssClass="hide"
                      target="_blank"> 
                    Scheda Tecnica
                  </aui:a>
              </aui:column>
          </aui:layout>
        </aui:fieldset>
	</c:if>

	<div id="myDataTable"  style="overflow-y:Auto;height:300px;width:100%;overflow-x:hidden"></div>
</aui:fieldset>

<div id="modal" ></div>

<aui:script>
	AUI().use('aui-base', function(A) {
	    A.one("#<portlet:namespace/>tipoPagam").on('change',function() {
	        var selectedValue = A.one('#<portlet:namespace/>tipoPagam').get(
	                                'value');
	        console.log(selectedValue);
	        A.one('#<portlet:namespace/>codicePagam').set('value',
	            selectedValue);
	                        
	    });
	});

	AUI().use('autocomplete-list', 'aui-base', 'aui-io-request',
			'autocomplete-filters',	'autocomplete-highlighters', function(A) {
		if (saveEnabledFlag === 'true') {
			  var testData;
			  var itemCode = new A.AutoCompleteList({
				  allowBrowserAutocomplete : 'true',
				  activateFirstItem : 'true',
				  inputNode : '#<portlet:namespace />itemCode',
				  resultTextLocator : function (result) {
					  return result.codiceArticolo + " --> " + result.descrizione
				  }, 
				  render : 'true',
				  resultHighlighter : 'phraseMatch',
				  resultFilters : [ 'startsWith', 'phraseMatch' ],
				  source : function() {
					  var inputValue = A.one("#<portlet:namespace />itemCode").get('value');
					  var noObsoleteValue = A.one("#<portlet:namespace />noObsolete").get('value');
					  var myAjaxRequest = A.io.request('${getItems}',
							  {
						        dataType : 'json',
								method : 'POST',
								data : {
									<portlet:namespace />itemCode : inputValue,
									<portlet:namespace />noObsolete : noObsoleteValue
								},
								autoLoad : false,
								sync : false,
								on : {
									success : function() {
										var data = this.get('responseData');
										testData = data;
								    }
								}
							}
					  );
					  myAjaxRequest.start();
					  return testData;
				 },
			});
			itemCode.on('select', function(e) {
				var selected_node = e.itemNode, selected_data = e.result;
			});
		}
	});

	AUI().use('aui-datepicker', function(A) {
		var orderDatePicker = new A.DatePicker({
			trigger : '#<portlet:namespace />orderDate',
			mask : '%d/%m/%Y',
			popover : {
				position : 'top',
				toolbars : {
					header : [ {
						icon : 'icon-trash',
						label : 'Cancella',
						on : {
							click : function() {
								orderDate.clearSelection();
							}
						}
					} ]
				},
				zIndex : 1
			},
			on : {
				selectionChange : function(event) {
					console.log(event.newSelection);
				}
			},
			after : {
				selectionChange : function(event) {
					var myDate = A.DataType.Date.addDays(new
							Date(orderDatePicker.getSelectedDates()) , +7);
					var day = myDate.getDate();
					var month = myDate.getMonth() + 1;
					var year = myDate.getFullYear();
					
					var myDateString = ('0' + myDate.getDate()).slice(-2) + '/'
		             + ('0' + (myDate.getMonth()+1)).slice(-2) + '/'
		             + myDate.getFullYear();
					A.one('#<portlet:namespace />deliveryDate').set('value',
							myDateString);
                }
			}
		});
		new A.DatePicker({
			trigger : '#<portlet:namespace />deliveryDate',
			mask : '%d/%m/%Y',
			popover : {
				position : 'top',
				toolbars : {
					header : [ {
						icon : 'icon-trash',
						label : 'Cancella',
						on : {
							click : function() {
								orderDate.clearSelection();
							}
						}
					} ]
				},
				zIndex : 1
			},
			on : {
				selectionChange : function(event) {
					console.log(event.newSelection);
				}
			}
		});
	});

	var dataTable;
	var recordSelected;
	AUI().use('aui-datatable', 'aui-datatype', 'datatable-sort', 'datatable-mutable',
		function(A) {
		var remoteData =<%= jsonArr%>;
		var numberEditor = new A.TextCellEditor({
			inputFormatter: A.DataType.String.evaluate,
			validator: {
				rules: {
					value: {
						number: true,
                        required: true
                    }
                }
            }
        });
		var nestedCols = [ {
			className: 'yui3-high',
			key : 'codiceArticolo',
			label : 'Codice'
		}, {
			key : 'descrizione',
			label : 'Descrizione'
		}, {
			key : 'unitaMisura',
			label : 'Un. Mis.'
		}, {
			editor: numberEditor,
			key : 'quantita',
			label : 'Quant.'
		}, {
			editor: numberEditor,
			key : 'prezzo',
			label : 'Pr. Unit.'
		}, {
			key : 'importoLordo',
			label : 'Imp. Lordo'
		}, {
			editor: numberEditor,
			key : 'sconto1',
			label : 'Sconto 1'
		}, {
			editor: numberEditor,
			key : 'sconto2',
			label : 'Sconto 2'
		}, {
			editor: numberEditor,
			key : 'sconto3',
			label : 'Sconto 3'
		}, {
			key : 'importoNetto',
			label : 'Imp. Netto'
		}, {
			editor: numberEditor,
			key : 'aliquotaIva',
			label : 'IVA'
		}, {
			className: 'yui3-hide',
            key : 'codiceIva',
            label : 'Codice IVA'
        }, {
			key : 'importoIvato',
			label : 'Totale'
		}, {
			className: 'yui3-hide',
            key : 'tipoRigo'
        } ];

		dataTable = new A.DataTable({
			columns : nestedCols,
			data : remoteData,
			editEvent : 'click',
			plugins : [ {
				cfg : {
					highlightRange : false
				},
				fn : A.Plugin.DataTableHighlight
			} ]
		}).render('#myDataTable');

		dataTable.get('boundingBox').unselectable();
		dataTable.addAttr("selectedRow", {
			value : null
		});
		dataTable.delegate('dblclick', function(e) {
			console.log(e.currentTarget._node.cells[0].innerText);
			console.log(e.currentTarget);
			recordSelected = dataTable.getRecord(e.currentTarget);
			if (e.currentTarget._node.cells[13].innerText === '0') {
				Liferay.Util.openWindow({
					dialog : {
						centered : true,
						modal : true
					},
					id : '<portlet:namespace/>itemDialog',
					title : 'Seleziona Prodotto',
					uri : '<%=itemURL%>'
				});
			} else {
				Liferay.Util.openWindow({
                    dialog : {
                    	width : '480px',
                    	height : '240px',
                        centered : true,
                        modal : true
                    },
                    id : '<portlet:namespace/>descrRowDialog',
                    title : 'Rigo Descrittivo',
                    uri : '<%= descrRowURL %>?rigo=e.currentTarget._node.cells[1].innerText'
                });
			}

		}, 'tr', dataTable);
		dataTable.delegate('click', function (e) { //handler for price column
			recordSelected = dataTable.getRecord(e.currentTarget);
			if (dataTable.getActiveColumn().key === 'codiceArticolo') {
	            Liferay.Util.openWindow({
	                dialog: {
	                    centered: true,
	                    modal: true
	                },
	                id: '<portlet:namespace/>itemDialog',
	                title: 'Seleziona Prezzo',
	                uri: '<%=priceURL%>&<portlet:namespace/>codiceArticolo='
	                	+ e.currentTarget._node.cells[0].innerText
	            });	
			}
        }, 'tr', dataTable);
		
		dataTable.after('*:quantitaChange', function (e) {
            calcolaImportoLordo(dataTable.getActiveRecord().getAttrs());
        });
		dataTable.after('*:prezzoChange', function (e) {
			calcolaImportoLordo(dataTable.getActiveRecord().getAttrs());
        });
		dataTable.after('*:importoLordoChange', function (e) {
			calcolaImportoNetto(dataTable.getActiveRecord().getAttrs());
        });
		dataTable.after('*:sconto1Change', function (e) {
			calcolaImportoNetto(dataTable.getActiveRecord().getAttrs());
        });
		dataTable.after('*:sconto2Change', function (e) {
			calcolaImportoNetto(dataTable.getActiveRecord().getAttrs());
        });
		dataTable.after('*:sconto3Change', function (e) {
			calcolaImportoNetto(dataTable.getActiveRecord().getAttrs());
        });
		dataTable.after('*:importoNettoChange', function (e) {
			calcolaImportoIvato(dataTable.getActiveRecord().getAttrs());
        });
	});

	var saveEnabledFlag = '<%=saveEnabledFlag%>';
	AUI().use('aui-base', 'datatype-number', function(A) {
		if (saveEnabledFlag === 'true') {
			A.one("#<portlet:namespace/>saveBtn").on('click', function() {
				var stringOrderDate = A.one('#<portlet:namespace/>orderDate').get('value');
	            var orderDateSplitted = stringOrderDate.split("/");
	            var orderDate = new Date(orderDateSplitted[2], orderDateSplitted[1], orderDateSplitted[0]);
	
	            var stringDeliveryDate =  A.one('#<portlet:namespace/>deliveryDate').get('value')
	            var deliveryDateSplitted = stringDeliveryDate.split("/");
	            var deliveryDate = new Date(deliveryDateSplitted[2], deliveryDateSplitted[1], deliveryDateSplitted[0]);
	
	
	            if (deliveryDate >= orderDate) {
	                var rows = [];
	                var ok = true;
	                for (var i = 0; i < dataTable.data.size(); i++) {
	                    rows[i] = dataTable.data.item(i).toJSON();
	                    if (rows[i].tipoRigo === 0 && (rows[i].quantita === "" || rows[i].quantita === 0)) {
	                    	ok = false;
	                    	alert("Specficare la quantita' ordinata al rigo: " + (i + 1) + ".");
	                    	break;
	                    }
	                }
	
	                if (ok) {
		                if (rows.length !== 0) {
		                    sendData(rows);
		                } else {
		                    alert("Attenzione inserire almeno un rigo nell'ordine.");
		                }
	                }
	            } else {
	                alert("Attenzione inserire una data di consegna maggiore o "
	                		+ " uguale alla data dell'ordine.");
	            }
			});
			A.one("#<portlet:namespace/>searchBtn").on('click', function() {
	            recordSelected = undefined;
	            Liferay.Util.openWindow({
	                dialog : {
	                    centered : true,
	                    modal : true
	                },
	                id : '<portlet:namespace/>itemDialog',
	                title : 'Cerca Prodotto',
	                uri : '<%= itemURL %>'
	            });
	        });
			A.one("#<portlet:namespace/>addDescrBtn").on('click', function() {
                recordSelected = undefined;
                Liferay.Util.openWindow({
                    dialog : {
                    	width : '480px',
                        height : '240px',
                        centered : true,
                        modal : true
                    },
                    id : '<portlet:namespace/>descrRowDialog',
                    title : 'Rigo Descrittivo',
                    uri : '<%= descrRowURL %>'
                });
            });
	        A.one("#<portlet:namespace/>deleteBtn").on('click', function() {
	            if (recordSelected) {
	                var result = confirm("Confermi l'eliminazione del rigo selezionato");
	                if (result) {
	                    dataTable.removeRow(recordSelected);
	                    recordSelected = undefined;
	                }
	            }
	        });
		}
		A.one("#<portlet:namespace/>printBtn").on('click', function() {
			AUI().use('aui-base', 'aui-io-request', function (A) {
				var numeroOrdine = A.one('#<portlet:namespace/>nDoc').get('value');
				var codiceCliente = A.one('#<portlet:namespace/>codiceClienteTxt').get('value');
				if (numeroOrdine !== ''){
					var win = window.open('${printOrderURL}' +
							'&<portlet:namespace />numeroOrdine=' +
							numeroOrdine + '&<portlet:namespace />codiceCliente='
							+ codiceCliente, '_blank');
	                win.focus();
				} else {
					alert('Ordine non ancora salvato, procedere al' +
							'salvataggio prima di porter stampare il documento.');
				}
			});
		});
		A.one("#<portlet:namespace/>sendBtn").on('click', function() {
            AUI().use('aui-base', 'aui-io-request', function (A) {
                var origEmail = '<%= email %>';
                var email = A.one('#<portlet:namespace/>emailTxt').get('value');
                var orderNumber = A.one('#<portlet:namespace />nDoc').get('value');
                var customer = A.one('#<portlet:namespace />clienteTxt').get('value');
                if (orderNumber !== '') {
	                if (origEmail !== '' || email !== '') {
	                	modal.show();
	                	A.io.request('${sendOrderURL}', {
	                		method: 'POST',
	                        data: {
	                            <portlet:namespace/>email: email,
	                            <portlet:namespace/>origEmail: origEmail,
	                            <portlet:namespace/>orderNumber: orderNumber,
	                            <portlet:namespace/>customer: customer
	                        },
	                        on: {
	                            success: function () {
	                            	var data = JSON.parse(this.get('responseData'));
	                                console.log(data);
	                            	modal.hide();
	                            	if (data.code === 0) {
	                            		alert("Copia dell'ordine inviata correttamente");
	                                } else {
	                                	msg = "Attenzione, invio non riuscito.\n";
	                                	if (data.code === 6) {
	                                		alert(msg + "Copia ordine non trovata, "
	                                				+ " procedere alla stampa dell'ordine.");
	                                	}
	                                }
	                            },
		                        failure: function () {
		                            modal.hide();
		                            alert("Si e' verificato nell'invio della copia " 
		                            		+ "dell'ordine al cliente.");
		                        }
	                        }
	                	});
	                } else {
	                	alert("Nessun indirizzo disponibile.\nInserire un indirizzo"
	                			+ " email valido per il cliente.");
	                }
                } else {
                	alert("Salvare e stampare l'ordine prima di procedere all'invio.");
                }
            });
        });
		A.one("#<portlet:namespace/>itemCode").on('click', function(){
			A.one('#<portlet:namespace />itemQuantity').set('value', '');
		});
		A.one("#<portlet:namespace/>itemCode").on('keydown', function(event) {
			var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode === 9 || keycode === 13){
            	if(A.one('#<portlet:namespace />itemCode').get('value').split(" --> ").length > 1) {
            		A.one('#<portlet:namespace />itemCode').set('value',
            			    A.one('#<portlet:namespace />itemCode').get('value').split(" --> ")[0]);
            		var itemCode =
                        A.one('#<portlet:namespace />itemCode').get('value');
                    var codiceCliente =
                        A.one('#<portlet:namespace />codiceClienteTxt').get('value');
                    loadItemInfo(codiceCliente, itemCode);
            	}
            }
        });
		A.one("#<portlet:namespace/>itemQuantity").on('click', function(event) {
			if(A.one('#<portlet:namespace />itemCode').get('value').split(" --> ").length > 1) {
                A.one('#<portlet:namespace />itemCode').set('value',
                        A.one('#<portlet:namespace />itemCode').get('value').split(" --> ")[0]);
                var itemCode =
                    A.one('#<portlet:namespace />itemCode').get('value');
                var codiceCliente =
                    A.one('#<portlet:namespace />codiceClienteTxt').get('value');
                loadItemInfo(codiceCliente, itemCode);
            }
		});
		A.one("#<portlet:namespace/>itemQuantity").on('keydown', function(event) {
			var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode === 13) {
            	var impLordo = A.one('#<portlet:namespace/>itemPrice').get('value') * 
            	   A.one('#<portlet:namespace/>itemQuantity').get('value');
            	var sc1 = parseFloat(A.one('#<portlet:namespace/>itemDiscount1').get('value'));
                var sc2 = parseFloat(A.one('#<portlet:namespace/>itemDiscount2').get('value'));
                var sc3 = parseFloat(A.one('#<portlet:namespace/>itemDiscount3').get('value'));
            	var imp1 = impLordo * (1 - (sc1 / 100));
            	var imp2 = imp1 * (1 - (sc2 / 100));
            	var impNetto = imp2 * (1 - (sc3 / 100));
            	var impIvato = impNetto * (1 + (A.one('#<portlet:namespace/>itemVAT').get('value') / 100));
            	dataTable.addRow({
                    codiceArticolo : A.one('#<portlet:namespace/>itemCode').get('value'),
                    descrizione : A.one('#<portlet:namespace/>itemDescription').get('value'),
                    unitaMisura : A.one('#<portlet:namespace/>itemUnMeasurement').get('value'),
                    prezzo : A.Number.format( A.one('#<portlet:namespace/>itemPrice').get('value'), {
                        decimalSeparator: '.',
                        decimalPlaces: 3,
                    }),
                    sconto1 : A.one('#<portlet:namespace/>itemDiscount1').get('value'),
                    sconto2 : A.one('#<portlet:namespace/>itemDiscount2').get('value'),
                    sconto3 : A.one('#<portlet:namespace/>itemDiscount3').get('value'),
                    aliquotaIva : A.one('#<portlet:namespace/>itemVAT').get('value'),
                    codiceIva : A.one('#<portlet:namespace/>itemVATCode').get('value'),
                    quantita : A.one('#<portlet:namespace/>itemQuantity').get('value'),
                    importoLordo : A.Number.format(impLordo,{
                        decimalSeparator: '.',
                        decimalPlaces: 2,
                    }),
                    importoNetto : A.Number.format(impNetto,{
                        decimalSeparator: '.',
                        decimalPlaces: 2,
                    }),
                    importoIvato : A.Number.format(impIvato,{
                        decimalSeparator: '.',
                        decimalPlaces: 2,
                    }),
                    tipoRigo : 0
                }, {
                    sync : true
                });
            	A.one('#<portlet:namespace/>itemCode').set('value', '');
            	A.one('#<portlet:namespace/>itemDescription').set('value','');
                A.one('#<portlet:namespace/>itemUnMeasurement').set('value','');
                A.one('#<portlet:namespace/>itemPriceTXT').set('value','');
                A.one('#<portlet:namespace/>itemPrice').set('value','');
                A.one('#<portlet:namespace/>itemVAT').set('value','');
                A.one('#<portlet:namespace/>itemVATCode').set('value','');
                A.one('#<portlet:namespace/>itemDiscount1').set('value', '');
                A.one('#<portlet:namespace/>itemDiscount3').set('value','');
                A.one('#<portlet:namespace/>itemQuantity').set('value','');
                A.one('#<portlet:namespace/>itemCode').focus();
            }
		});
	});
	var modal;
	AUI().use('aui-modal', function (A) {
		modal = new A.Modal({
			bodyContent : '<div class="loading-animation"/>',
			centered : true,
			headerContent : '<h3>Loading...</h3>',
			modal : true,
			render : '#modal',
			close: false,
			width : 450
		}).render();
		modal.hide();
	});
	
	Liferay.provide(window, 'closePopup', function(data, dialogId) {

		var dialog = Liferay.Util.Window.getById(dialogId);
		dialog.hide();
		console.log(data);
		switch (dialogId) {
			case '<portlet:namespace/>itemDialog':
				setItem(data);
				break;
			case '<portlet:namespace/>descrRowDialog':
	            setDescriptiveRow(data);
	            break;
		}
	});

	function setItem(data) {
		var tmp = data.split('|');

		console.log(recordSelected);
		if (recordSelected) {
			recordSelected.setAttrs({
				codiceArticolo : tmp[0],
				descrizione : tmp[1],
				unitaMisura : tmp[2],
				prezzo : tmp[3],
				sconto1 : tmp[4],
				sconto2 : tmp[5],
				sconto3 : tmp[6],
				aliquotaIva : tmp[7],
				codiceIva : tmp[8],
				tipoRigo : 0
			});
			recordSelected = undefined;
		} else {
			if (tmp.length > 2) {
				dataTable.addRow({
	                codiceArticolo : tmp[0],
	                descrizione : tmp[1],
	                unitaMisura : tmp[2],
	                prezzo : tmp[3],
	                sconto1 : tmp[4],
	                sconto2 : tmp[5],
	                sconto3 : tmp[6],
	                aliquotaIva : tmp[7],
	                codiceIva : tmp[8],
	                tipoRigo : 0
	            }, {
	                sync : true
	            });
			} else {
				AUI().use('aui-base', function(A){
				    A.one('#<portlet:namespace />itemCode').set('value', tmp[0]);
				    var codSog = A.one('#<portlet:namespace />codiceClienteTxt').get('value');
				    loadItemInfo(codSog, tmp[0]);
				});
			}
		}
	}

	function loadItemInfo(codiceCliente, itemCode) {
		modal.show()
		AUI().use('aui-base', 'aui-io-request', function (A) {
	        A.io.request('${getItemInfo}', {
	            method: 'POST',
	            data: {
	                <portlet:namespace/>itemCode: itemCode,
	                <portlet:namespace/>customerCode: codiceCliente
	            },
	            on: {
	                success: function () {
	                    var data = JSON.parse(this.get('responseData'));
	                    console.log(data);
	                    A.one('#<portlet:namespace/>itemDescription').set('value',
	                            data.descrizione);
	                    A.one('#<portlet:namespace/>itemUnMeasurement').set('value',
	                            data.unitaMisura);
	                    A.one('#<portlet:namespace/>itemPriceTXT').set('value',
	                            A.Number.format(data.prezzo,{
	                                prefix: '\u20AC',
	                                decimalSeparator: '.',
	                                decimalPlaces: 2,
	                            }));
	                    A.one('#<portlet:namespace/>itemPrice').set('value',
	                            data.prezzo);
	                    A.one('#<portlet:namespace/>itemVAT').set('value',
	                            data.aliquotaIva);
	                    A.one('#<portlet:namespace/>itemVATCode').set('value',
	                            data.codiceIva);
	                    A.one('#<portlet:namespace/>itemDiscount1').set('value',
	                            data.sconto1);
	                    A.one('#<portlet:namespace/>itemDiscount2').set('value',
	                            data.sconto2);
	                    A.one('#<portlet:namespace/>itemDiscount3').set('value',
	                            data.sconto3);
	                    if (data.itemSheetId != "") {
	                    	A.one('#<portlet:namespace />itemSheetLink').removeClass('hide');
	                    	A.one('#<portlet:namespace />itemSheetLink').set('href',
	                    			A.one('#<portlet:namespace />itemSheetLink').get('href')
	                    			+ '?<portlet:namespace/>itemSheetId=' + data.itemSheetId 
	                    			+ '&<portlet:namespace/>itemCode=' + itemCode);
	                    } else {
	                    	A.one('#<portlet:namespace />itemSheetLink').addClass('hide');
	                    }
	                    modal.hide();
	                    document.getElementById('<portlet:namespace/>itemQuantity').focus();
	                },
	                failure: function () {
	                    modal.hide();
	                    alert("Si e' verificato nel recupero delle informazioni del prodotto.");
	                }
	            }
	        });
		});
	}
	
	function setDescriptiveRow(data){
		console.log(recordSelected);
        if (recordSelected) {
            recordSelected.setAttrs({
                descrizione : data
            });
            recordSelected = undefined;
        } else {
            dataTable.addRow({
                descrizione : data,
                tipoRigo : 2
            }, {
                sync : true
            });
        } 
	}
	
	function calcolaImportoLordo(recordData) {
		var impLordo = recordData.quantita * recordData.prezzo;
		recordSelected.setAttrs({importoLordo: impLordo.toFixed(3)});
	}
	
	function calcolaImportoNetto(recordData) {
		var sconto1 = (!isNaN(recordData.sconto1)) ? recordData.sconto1 : 0;
        var sconto2 = (!isNaN(recordData.sconto2)) ? recordData.sconto2 : 0;
        var sconto3 = (!isNaN(recordData.sconto3)) ? recordData.sconto3 : 0;
        
        var importo1 = recordData.importoLordo
        	- ((recordData.importoLordo * sconto1) / 100);
        var importo2 = importo1 - ((importo1 * sconto2) / 100);
        var importoNetto = importo2 - ((importo2 * sconto3) / 100);
        
        recordSelected.setAttrs({importoNetto: importoNetto.toFixed(2)});
	}
	
	function calcolaImportoIvato(recordData) {
		var aliquotaIva = (!isNaN(recordData.aliquotaIva)) ? recordData.aliquotaIva : 0;
		var importoIvato =  recordData.importoNetto * (1 + (aliquotaIva / 100));
		
		recordSelected.setAttrs({importoIvato: importoIvato.toFixed(3)});
	}
	
	function sendData(rows) {
		AUI().use('aui-base', 'aui-io-request', function (A) {

			var numeroOrdine =
            	A.one('#<portlet:namespace />nDoc').get('value');
            var codiceCliente =
            	A.one('#<portlet:namespace />codiceClienteTxt').get('value');
            var codicePagam =
            	A.one('#<portlet:namespace />codicePagam').get('value');
            var orderDate =
            	A.one('#<portlet:namespace />orderDate').get('value');
            var deliveryDate =
            	A.one('#<portlet:namespace />deliveryDate').get('value');
            
//            	console.log('${saveOrderURL}' + queryString);
			modal.show();
           	A.io.request('${saveOrderURL}', {
           		method: 'POST',
           		data: {
           			<portlet:namespace/>numeroOrdine: numeroOrdine,
           			<portlet:namespace/>codiceCliente: codiceCliente,
           			<portlet:namespace/>codicePagam: codicePagam,
           			<portlet:namespace/>orderDate: orderDate,
           			<portlet:namespace/>deliveryDate: deliveryDate,
           			<portlet:namespace/>rows: JSON.stringify(rows)
           		},
           		on: {
                    success: function () {
                        var data = JSON.parse(this.get('responseData'));
                        console.log(data);
                        modal.hide();
                        if (data.code === 0) {
                            alert("Salvataggio effettuato con successo.");
                            var doc = JSON.parse(data.message);
                            A.one('#<portlet:namespace/>nDoc').set('value',
                            		doc.numeroOrdine);
                        } else {
                        	switch (data.code) {
                        	    case 6:
                                alert("Attenzione, salvataggio non " + 
                                        "riuscito.\n" + data.id);
                                break;
                        		case 7:
                        			alert("Attenzione, salvataggio non " + 
                        					"riuscito.\n" + data.id);
                        			break;
                        	}
                        }
                    },
                    failure: function () {
                    	alert("Si e' verificato un errore nel salvataggio dell'ordine.");
                    	modal.hide();
                    }
           		}
           	});
		});
	}
	
</aui:script>