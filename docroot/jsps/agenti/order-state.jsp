<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.bysoftware.ct.model.EvasioniOrdini"%>
<%@page import="it.bysoftware.ct.model.OrdiniClienti"%>
<%@page import="it.bysoftware.ct.service.EvasioniOrdiniLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.OrdiniClientiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.service.RigheOrdiniClientiLocalServiceUtil"%>
<%@page import="java.sql.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>

<%
    boolean evasi = ParamUtil.getBoolean(renderRequest, "evasi", false);
    String from = ParamUtil.getString(renderRequest, "from", null);
    String to = ParamUtil.getString(renderRequest, "to", null);
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    Date dateFrom = null;
    if (from != null && !from.isEmpty()) {
		java.util.Date parsed = format.parse(from);
		dateFrom = new Date(parsed.getTime());
    }

    Date dateTo = new Date(new java.util.Date().getTime());
    if (to != null && !to.isEmpty()) {
		java.util.Date parsed = format.parse(to);
		dateTo = new Date(parsed.getTime());
    }
    
    String note = ParamUtil.getString(renderRequest, "note", "");
    String codiceCliente = ParamUtil.getString(renderRequest,
		    "codiceCliente", "");

    int end = ParamUtil.getInteger(renderRequest, "end", -1);
    List<OrdiniClienti> ordini;

    if (end == -1) {
		end = OrdiniClientiLocalServiceUtil.getOrdiniClientisCount();
    }

    if (note != null && !note.isEmpty()) {
		ordini = OrdiniClientiLocalServiceUtil
			.getOrdiniByCodiceClienteNumPortale(codiceCliente,
				evasi, note, 0, end, dateFrom, dateTo, null);
    } else {
		ordini = OrdiniClientiLocalServiceUtil
			.getOrdiniByCodiceCliente(codiceCliente, evasi, 0, end,
				 dateFrom, dateTo, null);
    }
%>
<liferay-portlet:renderURL varImpl="backURL">
	<portlet:param name="mvcPath" value="/jsps/agenti/orders.jsp" />
	<portlet:param name="codiceSoggetto" value="<%=codiceCliente%>" />
</liferay-portlet:renderURL>

<liferay-ui:header title="orders" />

<liferay-portlet:renderURL varImpl="iteratorURL">
	<portlet:param name="mvcPath" value="/jsps/agenti/order-state.jsp" />
	<portlet:param name="note" value="<%=note%>" />
	<portlet:param name="codiceCliente" value="<%=codiceCliente%>" />
</liferay-portlet:renderURL>
<liferay-portlet:renderURL varImpl="searchURL">
	<portlet:param name="mvcPath" value="/jsps/agenti/order-state.jsp" />
	<portlet:param name="note" value="<%=note%>" />
	<portlet:param name="codiceCliente" value="<%=codiceCliente%>" />
</liferay-portlet:renderURL>
<aui:form action="<%=searchURL%>" method="get" name="fm">
	<liferay-portlet:renderURLParams varImpl="searchURL" />
	<div class="search-form">
		<span class="aui-search-bar" title="search-entries"> <aui:input
				label="processed-orders" name="evasi" title="search-orders"
				type="checkbox" checked="<%=evasi%>" /> <aui:input
				inlineField="<%=true%>" label="from" name="from" size="30"
				title="search-entries" type="text" /> <aui:input
				inlineField="<%=true%>" label="to" name="to" size="30"
				value="<%=format.format(dateTo)%>" title="search-entries"
				type="text" /> <aui:button type="submit" value="search" />
		</span>
	</div>
</aui:form>

<liferay-ui:search-container delta="20"
	emptyResultsMessage="no-order" iteratorURL="<%=iteratorURL%>">
	<liferay-ui:search-container-results>
		<%
		    results = ListUtil.subList(ordini,
					    searchContainer.getStart(),
					    searchContainer.getEnd());
				    total = ordini.size();
				    pageContext.setAttribute("results", results);
				    pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row
		className="it.bysoftware.ct.model.OrdiniClienti" modelVar="ordine">
		<liferay-ui:search-container-column-text property="numeroOrdine"
			name="N." />
		<%
		    SimpleDateFormat sdf = new SimpleDateFormat();
				    sdf.applyLocalizedPattern("dd/MM/yyyy");
				    String dataDoc = sdf.format(ordine.getDataDocumento());
				    String dataReg = "";
				    if (ordine.getDataRegistrazione() != null) {
					    dataReg = sdf.format(ordine.getDataRegistrazione());
				    }
		%>

		<liferay-ui:search-container-column-text value="<%=dataDoc%>"
			name="Data Ordine" />
		<liferay-ui:search-container-column-text value="<%=dataReg%>"
			name="Data Registrazione" />
		<c:if test="<%=evasi%>">
		    <%
		        boolean state = RigheOrdiniClientiLocalServiceUtil.checkRigheOrdineByTestata(
                ordine.getAnno(), ordine.getCodiceAttivita(),
                ordine.getCodiceCentro(), ordine.getCodiceDeposito(),
                ordine.getTipoOrdine(), ordine.getNumeroOrdine());
            %>
			<liferay-ui:search-container-column-text name="state"
			     value="<%= (state) ? "APERTO" : "EVASO" %>" />
		</c:if>
		<liferay-ui:search-container-column-jsp align="right"
			path="/jsps/agenti/order-state-action.jsp" />
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
		paginate="true" />
</liferay-ui:search-container>

<aui:script>
	AUI().use('aui-datepicker', function(A) {
		new A.DatePicker({
			trigger : '#<portlet:namespace />from',
			mask : '%d/%m/%Y',
			popover : {
				position : 'top',
				toolbars : {
					header : [ {
						icon : 'icon-trash',
						label : 'Cancella',
						on : {
							click : function() {
								from.clearSelection();
							}
						}
					} ]
				},
				zIndex : 1
			},
			on : {
				selectionChange : function(event) {
					console.log(event.newSelection);
				}
			}
		});
		new A.DatePicker({
			trigger : '#<portlet:namespace />to',
			mask : '%d/%m/%Y',
			popover : {
				position : 'top',
				toolbars : {
					header : [ {
						icon : 'icon-trash',
						label : 'Cancella',
						on : {
							click : function() {
								to.clearSelection();
							}
						}
					} ]
				},
				zIndex : 1
			},
			on : {
				selectionChange : function(event) {
					console.log(event.newSelection);
				}
			}
		});
	});

	
</aui:script>