<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.bysoftware.ct.model.EstrattoConto"%>
<%@page import="it.bysoftware.ct.service.EstrattoContoLocalServiceUtil"%>
<%@page import="java.sql.Date"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>

<%
    Calendar c = Calendar.getInstance();

    boolean chiuse = ParamUtil
		    .getBoolean(renderRequest, "chiuse", false);
    String from = ParamUtil.getString(renderRequest, "from", null);
    String to = ParamUtil.getString(renderRequest, "to", null);
    String codiceSoggetto = ParamUtil.getString(renderRequest,
		    "codiceSoggetto", null);
    
    Date dateFrom = null;
    if (from != null && !from.isEmpty()) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date parsed = format.parse(from);
		dateFrom = new Date(parsed.getTime());
    }
    
    Date dateTo = new Date(new java.util.Date().getTime());
    if (to != null && !to.isEmpty()) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        java.util.Date parsed = format.parse(to);
        dateTo = new Date(parsed.getTime());
    }
    
    int end = EstrattoContoLocalServiceUtil.getEstrattoContosCount();
    List<EstrattoConto> partite = EstrattoContoLocalServiceUtil
		    .getPartiteCliente(codiceSoggetto, chiuse, dateFrom, dateTo, 0, end,
			    null);
    double importoTotale = EstrattoContoLocalServiceUtil.getTotalePartiteAperte(
            codiceSoggetto, chiuse, dateFrom, dateTo, 0, end);

    NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
    SimpleDateFormat sdf = new SimpleDateFormat();
    sdf.applyLocalizedPattern("dd/MM/yyyy");
    
%>

<liferay-portlet:renderURL var="currentURL" >
    <portlet:param name="chiuse" value="<%=String.valueOf(chiuse) %>" />
    <portlet:param name="from" value="<%= from %>" />
    <portlet:param name="to" value="<%= to %>" />
    <portlet:param name="codiceSoggetto" value="<%=codiceSoggetto %>" />
    <portlet:param name="mvcPath" value="/jsps/agenti/entries.jsp" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="backURL" >
    <portlet:param name="mvcPath" value="/jsps/agenti/view.jsp" />
</liferay-portlet:renderURL>
<liferay-ui:header backURL="<%=backURL %>" title="entries" />

<liferay-portlet:renderURL varImpl="iteratorURL">
	<portlet:param name="mvcPath" value="/jsps/agenti/entries.jsp" />
	<portlet:param name="chiuse" value="<%=String.valueOf(chiuse) %>" />
	<portlet:param name="from" value="<%=from%>" />
	<portlet:param name="to" value="<%=to%>" />
	<portlet:param name="codiceSoggetto" value="<%=codiceSoggetto%>" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL varImpl="searchURL">
    <portlet:param name="codiceSoggetto" value="<%=codiceSoggetto%>" />
    <portlet:param name="mvcPath" value="/jsps/agenti/entries.jsp" />
</liferay-portlet:renderURL>
<aui:form action="<%=searchURL%>" method="get" name="fm">
    <liferay-portlet:renderURLParams varImpl="searchURL" />
    <div class="search-form">
        <span class="aui-search-bar" title="search-entries"> <aui:input
                label="closed-entries" name="chiuse"
                title="search-entries" type="checkbox" checked="<%= chiuse %>" /> <aui:input
                inlineField="<%=true%>" label="from" name="from" size="30"
                title="search-entries" type="text" /> <aui:input
                inlineField="<%=true%>" label="to" name="to" size="30" value="<%=sdf.format(dateTo) %>"
                title="search-entries" type="text" /> 
                <aui:button type="submit" value="search" />
        </span>
    </div>
</aui:form>

<liferay-ui:search-container delta="10"
	emptyResultsMessage="no-entries" iteratorURL="<%=iteratorURL%>">
	<liferay-ui:search-container-results>
		<%
		    results = ListUtil.subList(partite,
					    searchContainer.getStart(),
					    searchContainer.getEnd());
				    total = partite.size();
				    pageContext.setAttribute("results", results);
				    pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row
		className="it.bysoftware.ct.model.EstrattoConto" modelVar="partita">
		<liferay-ui:search-container-column-text property="numeroDocumento"
			name="N.Fattura" />
		<%
		    String s = "";
		    double importoFattura = partita.getImportoTotale();
		    double importoPagato = partita.getImportoPagato();
		    double importoPartita = partita.getImportoAperto();
            switch (partita.getStato()) {
            case 1:
                s = "APERTA";
                break;
            case 2:
                s = "ESPOSTA";
                break;
            case 3:
                s = "SCADUTA";
                break;
            case 4:
                s = "INSOLUTA";
                break;
            case 5:
                s = "IN CONTENZIOSO";
                break;
            case 6:
                s = "CHIUSA";
                break;
            case 7:
                s = "SOSTITUITA";
                break;
            }
            
            String date = sdf.format(partita.getDataDocumento());
            String expirationDate = sdf.format(partita.getDataScadenza());
		%>
		<liferay-ui:search-container-column-text name="date"
            value="<%=date%>" />
		<liferay-ui:search-container-column-text name="Imp. fattura"
            value="<%=fmt.format(importoFattura)%>" />
        <liferay-ui:search-container-column-text name="Imp. pagato"
            value="<%=fmt.format(importoPagato)%>" />
        <liferay-ui:search-container-column-text name="Imp. da saldare"
            value="<%=fmt.format(importoPartita)%>" />
        <liferay-ui:search-container-column-text name="expiration-date"
            value="<%=expirationDate%>" />
		<liferay-ui:search-container-column-text name="state" value="<%=s%>" />
		<liferay-ui:search-container-row-parameter name="backURL"
		    value="<%=currentURL %>" />
		<liferay-ui:search-container-column-jsp align="right"
            path="/jsps/agenti/entries-action.jsp"/>
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
		paginate="true" />
</liferay-ui:search-container>
<aui:input name="total-entries" value="<%= fmt.format(importoTotale) %>" disabled="true" />

<aui:script>
    AUI().use('aui-datepicker', function(A) {
        var from = new A.DatePicker({
            trigger : '#<portlet:namespace />from',
            mask : '%d/%m/%Y',
            popover : {
                position : 'top',
                toolbars : {
                    header : [ {
                        icon : 'icon-trash',
                        label : 'Cancella',
                        on : {
                            click : function() {
                                from.clearSelection();
                            }
                        }
                    } ]
                },
                zIndex : 1
            },
            on : {
                selectionChange : function(event) {
                    console.log(event.newSelection);
                }
            }
        });
        var to = new A.DatePicker({
            trigger : '#<portlet:namespace />to',
            mask : '%d/%m/%Y',
            popover : {
                position : 'top',
                toolbars : {
                    header : [ {
                        icon : 'icon-trash',
                        label : 'Cancella',
                        on : {
                            click : function() {
                                to.clearSelection();
                            }
                        }
                    } ]
                },
                zIndex : 1
            },
            on : {
                selectionChange : function(event) {
                    console.log(event.newSelection);
                }
            }
        });
    });
 </aui:script>