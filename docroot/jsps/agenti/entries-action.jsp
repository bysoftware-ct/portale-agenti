<%@page import="it.bysoftware.ct.model.EstrattoConto"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@include file="../init.jsp"%>
<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    EstrattoConto estrattoConto = (EstrattoConto) row
            .getObject();
    String backUrl = (String) row.getParameter("backURL");
%>

<liferay-ui:icon-menu>
    <liferay-portlet:renderURL var="viewInvoice">
    <liferay-portlet:param name="esercizioDocumento"
            value="<%=String.valueOf(estrattoConto.getEsercizioDocumento())%>" />
        <liferay-portlet:param name="codiceAttivita"
            value="<%=estrattoConto.getCodiceAttivita()%>" />
        <liferay-portlet:param name="codiceCentro"
            value="<%=estrattoConto.getCodiceCentro()%>" />
        <liferay-portlet:param name="codiceCliente"
            value="<%=estrattoConto.getCodiceCliente()%>" />
            <liferay-portlet:param name="dataDocumento"
            value="<%=String.valueOf(estrattoConto.getDataDocumento().getTime())%>" />
        <liferay-portlet:param name="backURL" value="<%=backUrl %>"/>
        <liferay-portlet:param name="numeroDocumento"
            value="<%=String.valueOf(estrattoConto.getNumeroDocumento())%>" />
        <liferay-portlet:param name="jspPage" value="/jsps/agenti/view-invoice.jsp" />
    </liferay-portlet:renderURL>
    <liferay-ui:icon image="view" url="${viewInvoice}" label="true"
        message="view-invoice" />
</liferay-ui:icon-menu>