<%@page import="it.bysoftware.ct.model.AnagraficaClientiFornitori"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@include file="../init.jsp"%>
<%
    ResultRow row = (ResultRow) request
		    .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    AnagraficaClientiFornitori cliente = (AnagraficaClientiFornitori) row
		    .getObject();
%>

<liferay-ui:icon-menu>
	<liferay-portlet:renderURL var="order">
		<liferay-portlet:param name="codiceSoggetto"
			value="<%=String.valueOf(cliente.getCodiceSoggetto())%>" />
		<liferay-portlet:param name="jspPage" value="/jsps/agenti/orders.jsp" />
	</liferay-portlet:renderURL>
	<liferay-ui:icon image="edit" url="${order}" label="true"
		message="orders" target="_blank" />
	<liferay-portlet:renderURL var="note">
        <liferay-portlet:param name="codiceSoggetto"
            value="<%=String.valueOf(cliente.getCodiceSoggetto())%>" />
        <liferay-portlet:param name="jspPage" value="/jsps/agenti/note.jsp" />
    </liferay-portlet:renderURL>
    <liferay-ui:icon image="edit" url="${note}" label="true"
        message="note-document" target="_blank" />
	<liferay-portlet:renderURL var="items">
		<liferay-portlet:param name="codiceSoggetto"
			value="<%=String.valueOf(cliente.getCodiceSoggetto())%>" />
		<liferay-portlet:param name="handle" value="true" />
		<liferay-portlet:param name="jspPage" value="/jsps/agenti/items_search.jsp" />
	</liferay-portlet:renderURL>
	<liferay-ui:icon image="edit" url="${items}" label="true"
		message="items" />
	<liferay-portlet:renderURL var="state">
        <liferay-portlet:param name="codiceCliente"
            value="<%=String.valueOf(cliente.getCodiceSoggetto())%>" />
        <liferay-portlet:param name="jspPage" value="/jsps/agenti/order-state.jsp" />
    </liferay-portlet:renderURL>
	<liferay-ui:icon image="submit" url="${state}" label="true"
        message="orders-list" />
	<liferay-portlet:renderURL var="entries">
        <liferay-portlet:param name="codiceSoggetto"
            value="<%=String.valueOf(cliente.getCodiceSoggetto())%>" />
        <liferay-portlet:param name="jspPage" value="/jsps/agenti/entries.jsp" />
    </liferay-portlet:renderURL>
    <liferay-ui:icon image="search" url="${entries}" label="true"
        message="entries" />
</liferay-ui:icon-menu>