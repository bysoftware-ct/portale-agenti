<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@page import="it.bysoftware.ct.model.WKOrdiniClienti"%>
<%@include file="../init.jsp"%>
<%
    ResultRow row = (ResultRow) request
		    .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    WKOrdiniClienti ordine = (WKOrdiniClienti) row.getObject();
    String codiceSoggetto = (String) row.getParameter("codiceSoggetto");
%>

<liferay-ui:icon-menu>
	<liferay-portlet:renderURL var="editOrderURL">
		<liferay-portlet:param name="saveEnabledFlag"
			value="<%=String.valueOf(!ordine.getStatoOrdine())%>" />
		<liferay-portlet:param name="editFlag"
			value="<%=String.valueOf(true)%>" />
		<liferay-portlet:param name="codiceSoggetto"
			value="<%=codiceSoggetto%>" />
		<liferay-portlet:param name="anno"
			value="<%=String.valueOf(ordine.getAnno())%>" />
		<liferay-portlet:param name="tipoOrdine"
			value="<%=String.valueOf(ordine.getTipoOrdine())%>" />
		<liferay-portlet:param name="numeroOrdine"
			value="<%=String.valueOf(ordine.getNumeroOrdine())%>" />
		<liferay-portlet:param name="jspPage"
			value="/jsps/agenti/edit-order.jsp" />
	</liferay-portlet:renderURL>
	<c:choose>
		<c:when test="<%=!ordine.getStatoOrdine()%>">
			<liferay-ui:icon image="edit" url="${editOrderURL}" label="true"
				message="edit" />
		</c:when>
		<c:otherwise>
			<liferay-portlet:renderURL var="state">
				<liferay-portlet:param name="codiceCliente"
					value="<%=codiceSoggetto%>" />
				<liferay-portlet:param name="note"
					value="<%=ordine.getCodiceAgente() + "/"
				    + ordine.getNumeroOrdine() %>" />
				<liferay-portlet:param name="jspPage"
					value="/jsps/agenti/order-state.jsp" />
			</liferay-portlet:renderURL>
			<liferay-ui:icon image="submit" url="${state}" label="true"
				message="order-state" />
			<liferay-ui:icon image="search" url="${editOrderURL}" label="true"
				message="view" />
		</c:otherwise>
	</c:choose>

	<liferay-portlet:actionURL var="deleteOrder" name="deleteOrder">
		<liferay-portlet:param name="anno"
			value="<%=String.valueOf(ordine.getAnno())%>" />
		<liferay-portlet:param name="tipoOrdine"
			value="<%=String.valueOf(ordine.getTipoOrdine())%>" />
		<liferay-portlet:param name="numeroOrdine"
			value="<%=String.valueOf(ordine.getNumeroOrdine())%>" />
	</liferay-portlet:actionURL>
	<c:if test="<%=!ordine.getStatoOrdine()%>">
		<liferay-ui:icon image="delete"
			useDialog="Sei sicuro di voler eliminare l'ordine N:
				<%= String.valueOf(ordine.getNumeroOrdine())%> del
				<%= ordine.getDataDocumento()%>.\nL'operazione non puo' essere annulata."
			url="${deleteOrder}" />
	</c:if>
	<liferay-portlet:resourceURL var="printOrder" id="printOrder">
	<liferay-portlet:param name="codiceCliente"
            value="<%=codiceSoggetto%>" />
		<liferay-portlet:param name="anno"
			value="<%=String.valueOf(ordine.getAnno())%>" />
		<liferay-portlet:param name="tipoOrdine"
			value="<%=String.valueOf(ordine.getTipoOrdine())%>" />
		<liferay-portlet:param name="numeroOrdine"
			value="<%=String.valueOf(ordine.getNumeroOrdine())%>" />
	</liferay-portlet:resourceURL>
	<liferay-ui:icon image="print" url="${printOrder}" />
</liferay-ui:icon-menu>