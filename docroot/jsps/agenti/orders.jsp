<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="it.bysoftware.ct.service.persistence.DatiClientiFornitoriPK"%>
<%@page import="it.bysoftware.ct.service.DatiClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.DatiClientiFornitori"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.Collections"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficaClientiFornitoriLocalServiceUtil"%>
<%@page
	import="it.bysoftware.ct.service.WKOrdiniClientiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficaClientiFornitori"%>
<%@page import="it.bysoftware.ct.model.WKOrdiniClienti"%>
<%@page import="it.bysoftware.ct.utils.Utils"%>
<%@page import="java.sql.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>

<%
    String toolbarItem = ParamUtil.getString(renderRequest,
		    "toolbarItem", "view-all");
    String codiceSoggetto = ParamUtil.getString(renderRequest,
		    "codiceSoggetto", null);
    String from = ParamUtil.getString(renderRequest, "from", null);
    String to = ParamUtil.getString(renderRequest, "to", null);
    
    Date dateFrom = null;
    if (from != null && !from.isEmpty()) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        java.util.Date parsed = format.parse(from);
        dateFrom = new Date(parsed.getTime());
    }
    
    Date dateTo =  new Date(new java.util.Date().getTime());
    if (to != null && !to.isEmpty()) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        java.util.Date parsed = format.parse(to);
        dateTo = new Date(parsed.getTime());
    }

    AnagraficaClientiFornitori cliente = null;
    List<WKOrdiniClienti> ordini = null;
    if (codiceSoggetto != null) {
		cliente = AnagraficaClientiFornitoriLocalServiceUtil
			.getAnagraficaClientiFornitori(codiceSoggetto);
// 		ordini = WKOrdiniClientiLocalServiceUtil
// 			.getOrdiniByCodiceCliente(cliente.getCodiceSoggetto());
        int end = WKOrdiniClientiLocalServiceUtil.getWKOrdiniClientisCount();
		ordini = WKOrdiniClientiLocalServiceUtil.searchOrdini(null,
			cliente.getCodiceSoggetto(), dateFrom, dateTo, 0, end, null);
		
    }
%>

<liferay-ui:header title="orders" />

<liferay-portlet:renderURL var="newOrderURL">
	<liferay-portlet:param name="codiceSoggetto"
		value="<%=cliente.getCodiceSoggetto()%>" />
	<liferay-portlet:param name="jspPage"
		value="/jsps/agenti/edit-order.jsp" />
</liferay-portlet:renderURL>

<c:choose>
	<c:when test="<%=cliente != null%>">
		<liferay-ui:error key="error-deleting-order"
			message="error-deleting-order" />
		<aui:fieldset label="client-info">
		    <%
		    DatiClientiFornitori dati = DatiClientiFornitoriLocalServiceUtil
                    .fetchDatiClientiFornitori(new DatiClientiFornitoriPK(
                            false, cliente.getCodiceSoggetto()));
		    
		    %>
		    <c:if test="<%=dati.getBloccato() == Constants.BLOCKED_ALERT %>">
                 <div class="portlet-msg-alert">
                     Il soggetto risulta essere bloccato con avviso.
                 </div>
            </c:if>
            <c:if test="<%=dati.getBloccato() == Constants.BLOCKED %>">
                 <div class="portlet-msg-error">
                     Il soggetto risulta essere bloccato.
                 </div>
            </c:if>
            <c:if test="<%=dati.getBloccato() == Constants.OBSOLETE %>">
                 <div class="portlet-msg-info">
                     Il soggetto risulta essere obsoleto.
                 </div>
            </c:if>
			<aui:input name="ragSociale" label="customer"
				value="<%=cliente.getRagioneSociale()%>" cssClass="input-xxlarge"
				inlineField="true" />
			<%
			    String indirizzo = cliente.getIndirizzo() + " - "
					+ cliente.getCAP() + " " + cliente.getComune()
					+ " (" + cliente.getSiglaProvincia() + ") - "
					+ cliente.getSiglaStato();
			%>
			<aui:input name="indirizzo" label="address" value="<%=indirizzo%>"
				cssClass="input-xxlarge" inlineField="true" />
			<c:if test="<%=dati.getBloccato() != 0 %>">
			     <aui:input name="reasonTxt" type="textarea" label="reason"
			         cssClass="input-xxlarge" value="<%= dati.getMotivazione() %>"/>
			</c:if>
		</aui:fieldset>
		<liferay-portlet:renderURL varImpl="searchURL">
			<portlet:param name="codiceSoggetto" value="<%=codiceSoggetto%>" />
			<portlet:param name="mvcPath" value="/jsps/agenti/orders.jsp" />
		</liferay-portlet:renderURL>
		<aui:fieldset label="orders">
			<aui:form action="<%=searchURL%>" method="get" name="fm">
	            <liferay-portlet:renderURLParams varImpl="searchURL" />
	            <div class="search-form">
	                <span class="aui-search-bar" title="search-document"> <aui:input
	                        id="from" inlineField="true" label="from" name="from" size="30"
	                        title="search-document" type="text" /> <aui:input id="to"
	                        inlineField="true" label="to" name="to" size="30"
	                        title="search-document" type="text" /> <aui:button type="submit"
	                        value="search" />
	                </span>
	            </div>
	        </aui:form>
			<aui:field-wrapper>
				<div class="btn-toolbar">
					<div class="btn-group">
						<button id="btnNew" class="btn">
							<i class="icon-plus"></i>Nuovo
						</button>
					</div>
				</div>
			</aui:field-wrapper>
			<liferay-portlet:renderURL varImpl="iteratorURL">
				<portlet:param name="mvcPath" value="/jsps/agenti/orders.jsp" />
				<portlet:param name="codiceSoggetto" value="<%=codiceSoggetto%>" />
			</liferay-portlet:renderURL>

			<liferay-ui:search-container delta="20"
				emptyResultsMessage="no-order" iteratorURL="<%=iteratorURL%>">
				<liferay-ui:search-container-results>
					<%
					    results = ListUtil.subList(ordini,
							searchContainer.getStart(),
							searchContainer.getEnd());
						total = ordini.size();
						pageContext.setAttribute("results", results);
						pageContext.setAttribute("total", total);
					%>
				</liferay-ui:search-container-results>
				<liferay-ui:search-container-row className="WKOrdiniClienti"
					modelVar="ordine">
					<%
					    SimpleDateFormat sdf = new SimpleDateFormat();
						sdf.applyLocalizedPattern("dd/MM/yyyy");
						String dataDoc = sdf.format(ordine.getDataDocumento());
						String dataReg = "";
						if (ordine.getDataRegistrazione() != null) {
						    dataReg = sdf.format(ordine.getDataRegistrazione()); 
						}
					%>
					<liferay-ui:search-container-column-text property="numeroOrdine"
						name="number" />
					<liferay-ui:search-container-column-text name="Data documento"
						value="<%=dataDoc%>" />
					<liferay-ui:search-container-column-text name="Data registrazione"
						value="<%=dataReg%>" />
					<liferay-ui:search-container-column-text name="Stato">
						<%
						    String icon = "/portale-agenti-portlet/icons/";
						    if (ordine.getStatoOrdine()) {
								icon += "update.png";
						    } else {
								icon += "not-updated.png";
						    }
						%>
						<liferay-ui:icon image="updated" src="<%=icon%>" />
					</liferay-ui:search-container-column-text>
					<liferay-ui:search-container-row-parameter name="codiceSoggetto"
						value="<%=codiceSoggetto%>" />
					<liferay-ui:search-container-column-jsp align="right"
						path="/jsps/agenti/order-action.jsp" />
				</liferay-ui:search-container-row>
				<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
					paginate="true" />
			</liferay-ui:search-container>
		</aui:fieldset>
	</c:when>
	<c:otherwise>
		<div class="portlet-msg-info">
			<liferay-ui:message key="no-client" />
		</div>
	</c:otherwise>
</c:choose>

<aui:script>
	AUI().use('aui-datepicker', function(A) {
		var from = new A.DatePicker({
			trigger : '#<portlet:namespace />from',
			mask : '%d/%m/%Y',
			popover : {
				position : 'top',
				toolbars : {
					header : [ {
						icon : 'icon-trash',
						label : 'Cancella',
						on : {
							click : function() {
								from.clearSelection();
							}
						}
					} ]
				},
				zIndex : 1
			},
			on : {
				selectionChange : function(event) {
					console.log(event.newSelection);
				}
			}
		});
		var to = new A.DatePicker({
			trigger : '#<portlet:namespace />to',
			mask : '%d/%m/%Y',
			popover : {
				position : 'top',
				toolbars : {
					header : [ {
						icon : 'icon-trash',
						label : 'Cancella',
						on : {
							click : function() {
								to.clearSelection();
							}
						}
					} ]
				},
				zIndex : 1
			},
			on : {
				selectionChange : function(event) {
					console.log(event.newSelection);
				}
			}
		});
	});

	AUI().use('node', function(A) {
		A.one('#btnNew').on('click', function() {
			window.location.href = '<%=newOrderURL.toString()%>';
		});
	});
</aui:script>
	

