<%@page import="it.bysoftware.ct.model.OrdiniClienti"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@include file="../init.jsp"%>

<%
    ResultRow row = (ResultRow) request
		    .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    OrdiniClienti ordine = (OrdiniClienti) row.getObject();
%>

<liferay-ui:icon-menu>
	<liferay-portlet:renderURL var="order">
		<liferay-portlet:param name="anno"
			value="<%=String.valueOf(ordine.getAnno())%>" />
		<liferay-portlet:param name="codiceAttivita"
			value="<%=ordine.getCodiceAttivita()%>" />
		<liferay-portlet:param name="codiceCentro"
			value="<%=ordine.getCodiceCentro()%>" />
		<liferay-portlet:param name="codiceDeposito"
			value="<%=ordine.getCodiceDeposito()%>" />
		<liferay-portlet:param name="tipoOrdine"
			value="<%=String.valueOf(ordine.getTipoOrdine())%>" />
		<liferay-portlet:param name="numeroOrdine"
			value="<%=String.valueOf(ordine.getNumeroOrdine())%>" />
		<liferay-portlet:param name="codiceCliente"
            value="<%=ordine.getCodiceCliente()%>" />
		<liferay-portlet:param name="jspPage"
			value="/jsps/agenti/order-rows-state.jsp" />
	</liferay-portlet:renderURL>
	<liferay-ui:icon image="edit" url="${order}" label="true"
		message="order-detail" />
</liferay-ui:icon-menu>