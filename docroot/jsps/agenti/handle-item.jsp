<%@page import="it.bysoftware.ct.service.AnagraficaClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficaClientiFornitori"%>
<%@page import="it.bysoftware.ct.service.DatiClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.DatiClientiFornitori"%>
<%@page import="java.sql.Date"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.math.RoundingMode"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="it.bysoftware.ct.service.ArticoliLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.Articoli"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page
	import="it.bysoftware.ct.service.MovimentiMagazzinoLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.MovimentiMagazzino"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>

<%
    String backURL = ParamUtil.getString(renderRequest, "backUrl");
    int testScarico = ParamUtil.getInteger(renderRequest,
		    "testScarico", -1);
    String from = ParamUtil.getString(renderRequest, "from", null);
    String to = ParamUtil.getString(renderRequest, "to", null);

    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    Date dateFrom = null;
    if (from != null && !from.isEmpty()) {
		java.util.Date parsed = format.parse(from);
		dateFrom = new Date(parsed.getTime());
    }

    Date dateTo = new Date(new java.util.Date().getTime());
    if (to != null && !to.isEmpty()) {
		java.util.Date parsed = format.parse(to);
		dateTo = new Date(parsed.getTime());
    }

    String codiceArticolo = ParamUtil.getString(renderRequest,
		    "codiceArticolo", "");
    Articoli articolo = ArticoliLocalServiceUtil
		    .getArticoli(codiceArticolo);
    List<MovimentiMagazzino> movimenti = MovimentiMagazzinoLocalServiceUtil
		    .getMovimentiMagazzino(codiceArticolo, "", testScarico, dateFrom, dateTo);

    DecimalFormat decForm = new DecimalFormat("#.##",
		    new DecimalFormatSymbols());
    decForm.setRoundingMode(RoundingMode.CEILING);
%>
<liferay-ui:header backURL="<%=backURL%>" title="list-handle-item" />

<aui:button-row cssClass="test test-padd">
	<aui:input name="articolo" label="item" disabled="true"
		value="<%=articolo.getCodiceArticolo()%>"
		cssClass="input-large" inlineField="true" />
	<aui:input name="descrizione" label="description" disabled="true"
		value="<%=articolo.getDescrizione()%>" cssClass="input-xxlarge"
		inlineField="true" />
</aui:button-row>

<liferay-portlet:renderURL varImpl="searchURL">
	<portlet:param name="mvcPath" value="/jsps/agenti/handle-item.jsp" />
	<%-- 	<portlet:param name="testScarico" value="<%=testScarico%>" /> --%>
	<portlet:param name="codiceArticolo" value="<%=codiceArticolo%>" />
</liferay-portlet:renderURL>
<aui:form action="<%=searchURL%>" method="get" name="fm">
	<liferay-portlet:renderURLParams varImpl="searchURL" />
	<aui:button-row>
		<aui:select name="testScarico" id="testScarico" label="handle-type"
			inlineField="true">
			<aui:option label="all" value="-1" selected="<%= testScarico == -1 %>" />
			<aui:option label="load" value="1" selected="<%= testScarico == 1 %>" />
			<aui:option label="unload" value="2" selected="<%= testScarico == 2 %>" />
		</aui:select>
		<aui:input inlineField="<%=true%>" label="from" name="from" size="30"
			title="search-entries" type="text" />
		<aui:input inlineField="<%=true%>" label="to" name="to" size="30"
			value="<%=format.format(dateTo)%>" title="search-entries" type="text" />
		<aui:button type="submit" value="search" />
	</aui:button-row>
</aui:form>

<liferay-portlet:renderURL varImpl="iteratorURL">
	<portlet:param name="mvcPath" value="/jsps/agenti/handle-item.jsp" />
	<portlet:param name="codiceArticolo" value="<%=codiceArticolo%>" />
</liferay-portlet:renderURL>

<liferay-ui:search-container delta="20" emptyResultsMessage="no-handle"
	iteratorURL="<%=iteratorURL%>">
	<liferay-ui:search-container-results>
		<%
		    results = ListUtil.subList(movimenti,
							    searchContainer.getStart(),
							    searchContainer.getEnd());
						    total = movimenti.size();

						    pageContext.setAttribute("results", results);
						    pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row
		className="it.bysoftware.ct.model.MovimentiMagazzino"
		modelVar="movimento">
		<liferay-ui:search-container-column-text property="ragioneSociale"
			name="Soggetto" />
		<%
		    DatiClientiFornitori dati = DatiClientiFornitoriLocalServiceUtil
			    .getDatiCliente(movimento.getCodiceSoggetto());
		%>
		<liferay-ui:search-container-column-text name="Tipo Sogg."
		      value="<%=(dati.getTipoSoggetto()) ? "FORNITORE" : "CLIENTE" %>"/>
		<%
		    SimpleDateFormat sdf = new SimpleDateFormat();
		    sdf.applyLocalizedPattern("dd/MM/yyyy");
		    String date = sdf.format(movimento.getDataRegistrazione());
		%>
		<liferay-ui:search-container-column-text name="Data registrazione"
			value="<%=date%>" />
		<liferay-ui:search-container-column-text name="Tipo movimento"
			align="center">
			<%
			    String icon = "/portale-agenti-portlet/icons/";
				String msg = "";
				switch (movimento.getTestCaricoScarico()) {
				case 1:
				    msg = "Carico";
				    icon += "enter.png";
				    break;
				case 2:
				    msg = "Scarico";
				    icon += "exit.png";
				    break;
				}
			%>
			<liferay-ui:icon image="<%=msg%>" src="<%=icon%>" />
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text property="unitaMisura"
			name="Un. Mis." />
		<%
		    String quant = "";
		    if (movimento.getTestCaricoScarico() == 1) {
			    quant = decForm.format(movimento.getQuantita());
		    } else {
			    quant = decForm.format(-1 * movimento.getQuantita());
		    }
		%>
		<liferay-ui:search-container-column-text value="<%=quant%>"
			name="Quant." />
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
		paginate="true" />
</liferay-ui:search-container>

<aui:script>
	AUI().use('aui-datepicker', function(A) {
		new A.DatePicker({
			trigger : '#<portlet:namespace />from',
			mask : '%d/%m/%Y',
			popover : {
				position : 'top',
				toolbars : {
					header : [ {
						icon : 'icon-trash',
						label : 'Cancella',
						on : {
							click : function() {
								orderDate.clearSelection();
							}
						}
					} ]
				},
				zIndex : 1
			},
			on : {
				selectionChange : function(event) {
					console.log(event.newSelection);
				}
			}
		});
		new A.DatePicker({
			trigger : '#<portlet:namespace />to',
			mask : '%d/%m/%Y',
			popover : {
				position : 'top',
				toolbars : {
					header : [ {
						icon : 'icon-trash',
						label : 'Cancella',
						on : {
							click : function() {
								orderDate.clearSelection();
							}
						}
					} ]
				},
				zIndex : 1
			},
			on : {
				selectionChange : function(event) {
					console.log(event.newSelection);
				}
			}
		});
	});

	
</aui:script>