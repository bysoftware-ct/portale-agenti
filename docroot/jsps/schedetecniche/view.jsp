<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.bysoftware.ct.model.Articoli"%>
<%@page import="it.bysoftware.ct.model.CategorieMerceologicheArticoli"%>
<%@page import="it.bysoftware.ct.service.ArticoliLocalServiceUtil"%>
<%@page	import="it.bysoftware.ct.service.
    CategorieMerceologicheArticoliLocalServiceUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>

<%
	String codiceArticolo = ParamUtil.getString(renderRequest, "codiceArticolo",
	    null);
	String descrizione = ParamUtil.getString(renderRequest,
	        "descrizione", null);
	String categoriaMerceologica = ParamUtil.getString(renderRequest,
	        "categoriaMerceologica", null);
	boolean noObsolete = ParamUtil.getBoolean(renderRequest, "noObsolete", true);
	int countCategorie = CategorieMerceologicheArticoliLocalServiceUtil
	        .getCategorieMerceologicheArticolisCount();
	
	List<CategorieMerceologicheArticoli> categorie = null;
	if (categoriaMerceologica == null || !categoriaMerceologica.isEmpty()) {
	    if (countCategorie > 0) {
	        categorie = CategorieMerceologicheArticoliLocalServiceUtil
	            .getCategorieMerceologicheArticolis(0,
	                countCategorie);
	    }
	} else {
	    categorie = CategorieMerceologicheArticoliLocalServiceUtil
	        .getCategorieMerceologicheArticolis(0, countCategorie);
	}

    int end = ArticoliLocalServiceUtil.getArticolisCount();
    List<Articoli> articoli = ArticoliLocalServiceUtil.searchArticoliByCodice(
                codiceArticolo, descrizione, categoriaMerceologica, noObsolete, true, 0,
                end, null);
%>
<liferay-portlet:renderURL varImpl="searchURL">
	<portlet:param name="mvcPath" value="/jsps/schedetecniche/view.jsp" />
</liferay-portlet:renderURL>
<aui:form action="<%=searchURL%>" method="get" name="fm">
	<liferay-portlet:renderURLParams varImpl="searchURL" />
	<div class="search-form">
		<span class="aui-search-bar" title="search-entries">
		    <aui:input name="noObsolete" label="no-obsolete" type="checkbox"
                checked="<%=noObsolete %>" />
		    <aui:select
				name="categoria" id="categoria" label="category" inlineField="true">
				<aui:option label="all" value="all" selected="true" />
				<%
                    for (int i = 0; i < categorie.size(); i++) {
                        CategorieMerceologicheArticoli categoria = categorie
                            .get(i);
                %>
				<aui:option label="<%=categoria.getDescrizione()%>"
					value="<%=categoria.getCodiceCategoria()%>" />
				<%
                    }
                %>
			</aui:select> <aui:input name="categoriaMerceologica" title="search-entries"
				cssClass="input-small" type="text" readonly="true" label="Cod. cat."
				inlineField="true" value="<%=categoriaMerceologica%>" /> <aui:input
				inlineField="true" label="item" name="codiceArticolo" size="30"
				title="search-entries" type="text">
			</aui:input> <aui:input inlineField="true" label="description" name="descrizione"
				size="30" title="search-entries" type="text">
			</aui:input> <aui:button inlineField="true" type="submit" value="search" />
		</span>
	</div>
</aui:form>

<liferay-portlet:renderURL varImpl="iteratorURL">
	<portlet:param name="mvcPath" value="/jsps/schedetecniche/view.jsp" />
	<portlet:param name="codiceArticolo" value="<%= codiceArticolo %>" />
	<portlet:param name="descrizione" value="<%= descrizione %>" />
	<portlet:param name="categoriaMerceologica"
		value="<%=categoriaMerceologica %>" />
	<portlet:param name="noObsolete" value="<%= String.valueOf(noObsolete) %>"/>
</liferay-portlet:renderURL>

<liferay-ui:search-container delta="20"
	emptyResultsMessage="no-articoli" iteratorURL="<%=iteratorURL%>">
	<c:choose>
		<c:when test="<%= articoli == null%>">
			java.lang.Integer deprecatedTotal = (java.lang.Integer) pageContext.getAttribute("deprecatedTotal");
		</c:when>
		<c:otherwise>
			<liferay-ui:search-container-results>
				<%
				    results = ListUtil.subList(articoli,
				                searchContainer.getStart(),
				                searchContainer.getEnd());
				            total = articoli.size();
				            
				            pageContext.setAttribute("results", results);
				            pageContext.setAttribute("total", total);
				%>
			</liferay-ui:search-container-results>
		</c:otherwise>
	</c:choose>
	<liferay-ui:search-container-row className="Articoli"
		modelVar="articolo">
		<liferay-ui:search-container-column-text property="codiceArticolo"
			name="Codice" />
		<liferay-ui:search-container-column-text property="descrizione"
			name="Descrizione" />
		<%
		    CategorieMerceologicheArticoli c = CategorieMerceologicheArticoliLocalServiceUtil
		          .getCategorieMerceologicheArticoli(articolo.getCategoriaMerceologica());
		%>
		<liferay-ui:search-container-column-text
			value="<%=c.getDescrizione()%>" name="Categoria" />
		<liferay-ui:search-container-column-jsp align="right"
                     path="/jsps/schedetecniche/items-sheet-action.jsp"/>
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
		paginate="true" />
</liferay-ui:search-container>

<aui:script>
	AUI().use(
			'aui-base',
			function(A) {
				A.one("#<portlet:namespace/>categoria").on(
						'change',
						function() {
							var selectedValue = A.one(
									'#<portlet:namespace/>categoria').get(
									'value');
							console.log(selectedValue);
							A.one('#<portlet:namespace/>categoriaMerceologica')
									.set('value', selectedValue);

						});
			});
</aui:script>



<%!private static Log _log = LogFactoryUtil
			.getLog("docroot.jsps.agenti.item_view_jsp");%>

<script type="text/javascript">
	function set(data) {
		Liferay.Util.getOpener().closePopup(data,
				'<portlet:namespace/>itemDialog');
	}
</script>