<%@page import="com.liferay.portlet.documentlibrary.util.DLUtil"%>
<%@page
	import="com.liferay.portlet.documentlibrary.util.PDFProcessorUtil"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileVersion"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileEntry"%>
<%@page
	import="com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil"%>
<%@page
	import="com.liferay.portlet.documentlibrary.service.DLAppHelperLocalServiceUtil"%>
<%@ include file="../init.jsp"%>

<%
    boolean edit = ParamUtil.getBoolean(renderRequest, "edit", false);
    long fileEntryId = ParamUtil.getLong(renderRequest, "itemSheetId", -1);
    String itemCode = ParamUtil.getString(renderRequest, "itemCode", "");
    
%>

<liferay-portlet:renderURL varImpl="viewSheetURL">
    <portlet:param name="mvcPath"
        value="/jsps/schedetecniche/edit-item-sheet.jsp" />
    <portlet:param name="edit" value="<%=String.valueOf(edit) %>" />
    <portlet:param name="itemSheetId"
        value="<%=String.valueOf(fileEntryId)%>" />
</liferay-portlet:renderURL>

<aui:layout>
    <c:if test="<%= edit %>">
		<aui:column columnWidth="40">
			<liferay-portlet:actionURL name="upload" var="uploadFileURL" >
			 <portlet:param name="itemCode" value="<%=itemCode %>"/>
			 <portlet:param name="edit" value="<%=String.valueOf(edit) %>"/>
			</liferay-portlet:actionURL>
			<aui:form action="<%=uploadFileURL%>" enctype="multipart/form-data"
				method="post">
				<aui:fieldset label="load-sheet">
					<liferay-ui:error key="empty-file" message="empty-sheet" />
					<liferay-ui:error key="disk-space" message="disk-space" />
					<aui:layout>
						<aui:column columnWidth="100" first="true">
						    <aui:input name="origId" type="hidden" value="<%=fileEntryId%>" />
							<aui:input type="file" name="fileupload" inlineField="true"
								label="file">
								<aui:validator name="acceptFiles">'pdf'</aui:validator>
								<aui:validator name="required" />
							</aui:input>
							<aui:button id="btnUpload" name="Save" value="Upload"
								type="submit" />
						</aui:column>
					</aui:layout>
				</aui:fieldset>
			</aui:form>
		</aui:column>
	</c:if>
	<aui:column columnWidth="<%= edit ? 60 : 100 %>">
	    <%
	        int previewFileCount = 0;
            String previewFileURL = null;
            String[] previewFileURLs = null;
            String videoThumbnailURL = null;
            String previewQueryString = null;
	    %>
		<c:choose>
			<c:when test="<%=fileEntryId != -1%>">
				<%
				    FileEntry fileEntry = DLAppLocalServiceUtil
							.getFileEntry(fileEntryId);
					FileVersion fileVersion = fileEntry.getFileVersion();
					boolean hasPDFImages = PDFProcessorUtil.hasImages(fileVersion);
				%>
				<c:choose>
					<c:when test="<%=hasPDFImages%>">
						<%
				            previewFileCount = PDFProcessorUtil
			                        .getPreviewFileCount(fileVersion);
			                previewQueryString = "&previewFileIndex=";
			                previewFileURL = DLUtil.getPreviewURL(fileEntry, fileVersion,
			                        themeDisplay, previewQueryString);
						%>
						<div class="lfr-preview-file"
							id="<portlet:namespace />previewFile">
							<div class="lfr-preview-file-content"
								id="<portlet:namespace />previewFileContent">
								<div class="lfr-preview-file-image-current-column">
									<div class="lfr-preview-file-image-container">
										<img class="lfr-preview-file-image-current"
											id="<portlet:namespace />previewFileImage"
											src="<%=previewFileURL + "1"%>" />
									</div>
									<span class="lfr-preview-file-actions aui-helper-hidden"
										id="<portlet:namespace />previewFileActions"> <span
										class="lfr-preview-file-toolbar"
										id="<portlet:namespace />previewToolbar"></span> <span
										class="lfr-preview-file-info"> <span
											class="lfr-preview-file-index"
											id="<portlet:namespace />previewFileIndex">1</span> of <span
											class="lfr-preview-file-count"><%=previewFileCount%></span>
									</span>
									</span>
								</div>

								<div class="lfr-preview-file-images"
									id="<portlet:namespace />previewImagesContent">
									<div class="lfr-preview-file-images-content"></div>
								</div>
							</div>
						</div>
						<aui:script use="aui-base,liferay-preview">
							new Liferay.Preview(
									{
										actionContent : '#<portlet:namespace />previewFileActions',
										baseImageURL : '<%= previewFileURL %>',
										boundingBox : '#<portlet:namespace />previewFile',
										contentBox : '#<portlet:namespace />previewFileContent',
										currentPreviewImage : '#<portlet:namespace />previewFileImage',
										imageListContent : '#<portlet:namespace />previewImagesContent',
										maxIndex : <%= previewFileCount %>,
										previewFileIndexNode : '#<portlet:namespace />previewFileIndex',
										toolbar : '#<portlet:namespace />previewToolbar'
									}).render();
						</aui:script>
					</c:when>
					<c:otherwise>
						<h2>Anteprima non disponibile.</h2>
						<button id="btnRefresh" class="btn">
						  <i class="icon-refresh"></i> Aggiorna
                        </button>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<h2>Anteprima non disponibile.</h2>
                <button id="btnRefresh" class="btn">
                  <i class="icon-refresh"></i> Aggiorna
                </button>
			</c:otherwise>			
		</c:choose>
	</aui:column>
</aui:layout>

<div class="yui3-skin-sam">
    <div id="modal"></div>
</div>

<aui:script>

AUI().use("aui-base", "liferay-util-list-fields", function(A) {
	if(A.one('#<portlet:namespace />Save') !== null) {
		A.one('#<portlet:namespace />Save').on('click', function(event) {
			AUI().use("aui-modal", function(A) {
		        var modal = new A.Modal({
		            bodyContent : '<div class="loading-animation"/>',
		            centered : true,
		            headerContent : '<h3>Loading...</h3>',
		            modal : true,
		            render : '#modal',
		            close: false,
		            width : 450
		        }).render();
		        modal.after("render", function() {
		            submitForm(document.<portlet:namespace/>fm);
		        });
			});
		});
	}
});

AUI().use('node', function(A) {
	if (A.one('#btnRefresh') !== null) {
	    A.one('#btnRefresh').on('click', function() {
	        window.location.href = '<%=viewSheetURL%>';
	    });
	}
});
</aui:script>