<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="it.bysoftware.ct.agenti.ResourceId"%>
<%@page import="it.bysoftware.ct.model.Articoli"%>
<%@include file="../init.jsp"%>
<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    Articoli articolo = (Articoli) row.getObject();
%>

<liferay-ui:icon-menu>
	<c:choose>
		<c:when test="<%=!articolo.getLiberoString5().isEmpty()%>">
			<liferay-portlet:renderURL varImpl="viewSheetURL">
				<portlet:param name="mvcPath"
					value="/jsps/schedetecniche/edit-item-sheet.jsp" />
				<portlet:param name="edit" value="false" />
				<portlet:param name="itemSheetId"
					value="<%=articolo.getLiberoString5()%>" />
			</liferay-portlet:renderURL>
			<liferay-ui:icon image="page_template" url="${viewSheetURL}"
				target="_blank" message="view" />
			<liferay-portlet:renderURL varImpl="editSheetURL">
				<portlet:param name="mvcPath"
					value="/jsps/schedetecniche/edit-item-sheet.jsp" />
				<portlet:param name="edit" value="true" />
				<portlet:param name="itemCode"
                    value="<%=articolo.getCodiceArticolo()%>" />
				<portlet:param name="itemSheetId"
					value="<%=articolo.getLiberoString5()%>" />
			</liferay-portlet:renderURL>
			<liferay-ui:icon image="edit" url="${editSheetURL}"/>
			<liferay-portlet:actionURL var="delete" name="deleteSheet">
				<portlet:param name="itemCode"
					value="<%=articolo.getCodiceArticolo()%>" />
			</liferay-portlet:actionURL>
			<liferay-ui:icon-delete image="trash" url="${deleteSheet}"
			    message="delete"/>
		</c:when>
		<c:otherwise>
			<liferay-portlet:renderURL varImpl="addSheetURL">
				<portlet:param name="mvcPath"
					value="/jsps/schedetecniche/edit-item-sheet.jsp" />
				<portlet:param name="edit" value="true"/>
				<portlet:param name="itemCode"
                    value="<%=articolo.getCodiceArticolo()%>" />
			</liferay-portlet:renderURL>
			<liferay-ui:icon image="add" url="${addSheetURL}" message="add" />
		</c:otherwise>
	</c:choose>
</liferay-ui:icon-menu>