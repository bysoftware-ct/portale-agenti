<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@page import="it.bysoftware.ct.model.WKOrdiniClienti"%>
<%@include file="../init.jsp"%>
<%
    ResultRow row = (ResultRow) request
		    .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    WKOrdiniClienti ordine = (WKOrdiniClienti) row.getObject();
%>

<liferay-ui:icon-menu>
	<liferay-portlet:resourceURL var="download" id="exportOrder">
		<liferay-portlet:param name="anno"
			value="<%=String.valueOf(ordine.getAnno())%>" />
		<liferay-portlet:param name="tipoOrdine"
			value="<%=String.valueOf(ordine.getTipoOrdine())%>" />
		<liferay-portlet:param name="numeroOrdine"
			value="<%=String.valueOf(ordine.getNumeroOrdine())%>" />
	</liferay-portlet:resourceURL>
	<liferay-ui:icon image="download" label="download" url="${download}" />
</liferay-ui:icon-menu>