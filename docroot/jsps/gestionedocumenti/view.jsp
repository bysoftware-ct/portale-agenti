<%@page import="java.sql.Date"%>
<%@page	import="it.bysoftware.ct.service
    .AnagraficaClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficaClientiFornitori"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficaAgenti"%>
<%@page import="it.bysoftware.ct.model.WKOrdiniClienti"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficaAgentiLocalServiceUtil"%>
<%@page
	import="it.bysoftware.ct.service.WKOrdiniClientiLocalServiceUtil"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>

<%
    boolean active1 = ParamUtil.getBoolean(renderRequest, "active1", true);
    String customer = ParamUtil.getString(renderRequest, "customer", null);
    String from = ParamUtil.getString(renderRequest, "from", null);
	String to = ParamUtil.getString(renderRequest, "to", null);
	
    List<WKOrdiniClienti> ordini = WKOrdiniClientiLocalServiceUtil
		.getOrdiniByStatoOrdine(false);
    List<WKOrdiniClienti> ordiniAcquisiti;
    
    Date dateFrom = null;
    if (from != null && !from.isEmpty()) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        java.util.Date parsed = format.parse(from);
        dateFrom = new Date(parsed.getTime());
    }
    
    Date dateTo = null;
    if (to != null && !to.isEmpty()) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        java.util.Date parsed = format.parse(to);
        dateTo = new Date(parsed.getTime());
    }
    
    if ((customer != null && !customer.isEmpty()) || dateFrom != null
	    || dateTo != null) {
	   int end = WKOrdiniClientiLocalServiceUtil
		   .getOrdiniClientiAcquisitiCount();
       ordiniAcquisiti = WKOrdiniClientiLocalServiceUtil.searchOrdini(
	       true, customer, dateFrom, dateTo, 0, end, null);
    } else {
	   ordiniAcquisiti = WKOrdiniClientiLocalServiceUtil
		   .getOrdiniByStatoOrdine(true);
    }
    
%>

<div id="myTab">

	<ul class="nav nav-tabs">
	<c:choose>
	   <c:when test="<%= active1 %>">
	       <li class="active">
	           <a href="#tab-1">Ordini da acquisire</a>
           </li>
           <li>
               <a href="#tab-2">Ordini acquisiti</a>
           </li>
	   </c:when>
	   <c:otherwise>
	       <li>
               <a href="#tab-1">Ordini da acquisire</a>
           </li>
           <li class="active">
               <a href="#tab-2">Ordini acquisiti</a>
           </li>
	   </c:otherwise>
	</c:choose>
		
	</ul>

	<div class="tab-content">
		<div id="tab-1" class="tab-pane">
			<portlet:resourceURL var="download" id="download" />
			<liferay-portlet:renderURL varImpl="iteratorURL">
                <portlet:param name="active1" value="true"/>
			    <portlet:param name="mvcPath"
			         value="/jsps/gestionedocumenti/view.jsp" />
			</liferay-portlet:renderURL>
			<liferay-ui:search-container delta="20" 
                emptyResultsMessage="no-new-order"
                    iteratorURL="<%=iteratorURL%>">
				<liferay-ui:search-container-results>
					<%
					    results = ListUtil.subList(ordini,
						    searchContainer.getStart(),
						    searchContainer.getEnd());
					    total = ordini.size();
					    pageContext.setAttribute("results", results);
					    pageContext.setAttribute("total", total);
					%>
				</liferay-ui:search-container-results>
				<liferay-ui:search-container-row className="WKOrdiniClienti"
					modelVar="ordine">
					<%
					    SimpleDateFormat sdf = new SimpleDateFormat();
					    sdf.applyLocalizedPattern("dd/MM/yyyy");
					    String dataDoc = sdf.format(ordine.getDataDocumento());
					    String dataReg = "";
					    if (ordine.getDataRegistrazione() != null) {
						dataReg = sdf.format(ordine.getDataRegistrazione());
					    }
					    AnagraficaClientiFornitori cliente =
						    AnagraficaClientiFornitoriLocalServiceUtil
						    .getAnagraficaClientiFornitori(ordine
							    .getCodiceCliente());
					    AnagraficaAgenti agente =
						    AnagraficaAgentiLocalServiceUtil
						    .getAnagraficaAgenti(ordine.getCodiceAgente());
					%>
					<liferay-ui:search-container-column-text
					   property="numeroOrdine" name="number" />
					<liferay-ui:search-container-column-text name="Cliente"
						value="<%=cliente.getRagioneSociale()%>" />
					<liferay-ui:search-container-column-text
					   name="Data documento" value="<%=dataDoc%>" />
					<liferay-ui:search-container-column-text
					   name="Data registrazione" value="<%=dataReg%>" />
					<liferay-ui:search-container-column-text
					   name="Agente" value="<%=agente.getNome()%>" />
					<liferay-ui:search-container-column-text name="Stato">
						<%
						    String icon = "/portale-agenti-portlet/icons/";
							if (ordine.getStatoOrdine()) {
							    icon += "update.png";
							} else {
							    icon += "not-updated.png";
							}
						%>
						<liferay-ui:icon image="state" src="<%=icon%>" />
					</liferay-ui:search-container-column-text>
					<liferay-ui:search-container-column-jsp
						path="/jsps/gestionedocumenti/document-action.jsp" />
				</liferay-ui:search-container-row>
				<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
					paginate="true" />
			</liferay-ui:search-container>
		</div>
		<div id="tab-2">
		  <portlet:resourceURL var="download" id="download" />
            <liferay-portlet:renderURL varImpl="iterator2URL">
                <portlet:param name="mvcPath"
                    value="/jsps/gestionedocumenti/view.jsp" />
                <portlet:param name="active1" value="false"/>
            </liferay-portlet:renderURL>
            <liferay-portlet:renderURL varImpl="searchURL">
<%-- 			    <portlet:param name="codiceSoggetto" value="<%=codiceSoggetto%>" /> --%>
			    <portlet:param name="mvcPath" value="/jsps/gestionedocumenti/view.jsp" />
			    <portlet:param name="active1" value="false"/>
			</liferay-portlet:renderURL>
			<aui:form action="<%=searchURL%>" method="get" name="fm">
			    <liferay-portlet:renderURLParams varImpl="searchURL" />
			    <div class="search-form">
			        <span class="aui-search-bar" title="search-document">
			             <aui:input label="customer" name="customer" size="30"
			                 title="search-document" type="text" />
			             <aui:input id="from" inlineField="true" label="from" name="from"
			                 size="30" title="search-document" type="text" />
		                 <aui:input id="to" inlineField="true" label="to" name="to"
		                     size="30" title="search-document" type="text" /> 
		                 <aui:button type="submit" value="search" />
			        </span>
			    </div>
			</aui:form>
            <liferay-ui:search-container delta="20" 
                emptyResultsMessage="no-new-order" iteratorURL="<%=iterator2URL%>">
                <liferay-ui:search-container-results>
                    <%
                        results = ListUtil.subList(ordiniAcquisiti,
                            searchContainer.getStart(),
                            searchContainer.getEnd());
                        total = ordini.size();
                        pageContext.setAttribute("results", results);
                        pageContext.setAttribute("total", total);
                    %>
                </liferay-ui:search-container-results>
                <liferay-ui:search-container-row className="WKOrdiniClienti"
                    modelVar="ordine">
                    <%
                        SimpleDateFormat sdf = new SimpleDateFormat();
                        sdf.applyLocalizedPattern("dd/MM/yyyy");
                        String dataDoc = sdf.format(ordine.getDataDocumento());
                        String dataReg = "";
                        if (ordine.getDataRegistrazione() != null) {
                            dataReg = sdf.format(ordine.getDataRegistrazione());
                        }
                        AnagraficaClientiFornitori cliente =
                        	AnagraficaClientiFornitoriLocalServiceUtil
                            .getAnagraficaClientiFornitori(ordine
                                .getCodiceCliente());
                        AnagraficaAgenti agente = AnagraficaAgentiLocalServiceUtil
                            .getAnagraficaAgenti(ordine.getCodiceAgente());
                    %>
                    <liferay-ui:search-container-column-text
                        property="numeroOrdine" name="number" />
                    <liferay-ui:search-container-column-text
                        name="Cliente" value="<%=cliente.getRagioneSociale()%>" />
                    <liferay-ui:search-container-column-text
                        name="Data documento" value="<%=dataDoc%>" />
                    <liferay-ui:search-container-column-text
                        name="Data registrazione" value="<%=dataReg%>" />
                    <liferay-ui:search-container-column-text name="Agente"
                        value="<%=agente.getNome()%>" />
                    <liferay-ui:search-container-column-text name="Stato">
                        <%
                            String icon = "/portale-agenti-portlet/icons/";
                            if (ordine.getStatoOrdine()) {
                                icon += "update.png";
                            } else {
                                icon += "not-updated.png";
                            }
                        %>
                        <liferay-ui:icon image="state" src="<%=icon%>" />
                    </liferay-ui:search-container-column-text>
                    <liferay-ui:search-container-column-jsp
                        path="/jsps/gestionedocumenti/document-action.jsp" />
                </liferay-ui:search-container-row>
                <liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
                    paginate="true" />
            </liferay-ui:search-container>
		</div>
	</div>
</div>

<aui:script>
	AUI().use('aui-datepicker', function(A) {
		var from = new A.DatePicker({
			trigger : '#<portlet:namespace />from',
			mask : '%d/%m/%Y',
			popover : {
				position : 'top',
				toolbars : {
					header : [ {
						icon : 'icon-trash',
						label : 'Cancella',
						on : {
							click : function() {
								from.clearSelection();
							}
						}
					} ]
				},
				zIndex : 1
			},
			on : {
				selectionChange : function(event) {
					console.log(event.newSelection);
				}
			}
		});
		var to = new A.DatePicker({
			trigger : '#<portlet:namespace />to',
			mask : '%d/%m/%Y',
			popover : {
				position : 'top',
				toolbars : {
					header : [ {
						icon : 'icon-trash',
						label : 'Cancella',
						on : {
							click : function() {
								to.clearSelection();
							}
						}
					} ]
				},
				zIndex : 1
			},
			on : {
				selectionChange : function(event) {
					console.log(event.newSelection);
				}
			}
		});
	});
	AUI().use('aui-tabview', function(A) {
		new A.TabView({
			srcNode : '#myTab'
		}).render();
	});
</aui:script>