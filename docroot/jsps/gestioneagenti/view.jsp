<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.ArrayUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.model.Role"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.service.ClassNameLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.RoleServiceUtil"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoValue"%>
<%@page
    import="com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficaAgenti"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficaAgentiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>

<%
    String toolbarItem = ParamUtil.getString(renderRequest, "toolbarItem", "view-all");
    int count = UserLocalServiceUtil.getCompanyUsersCount(user
		    .getCompanyId());
    List<User> users = UserLocalServiceUtil.getCompanyUsers(
		    user.getCompanyId(), 0, count);
    List<User> activeAgents = new ArrayList<User>();
    List<User> deactivatedAgents = new ArrayList<User>();
    Role role = RoleServiceUtil.getRole(user.getCompanyId(), "agente");
    for (User u : users) {
		long[] rolesIds = u.getRoleIds();
		if (ArrayUtil.contains(rolesIds, role.getRoleId())) {
		    if(u.isActive()) {
			    activeAgents.add(u);
		    } else {
			    deactivatedAgents.add(u);
		    }
		}
    }
%>

<fieldset title="list-agents">
	<aui:nav-bar>
		<aui:nav>
            <aui:nav-item id="newBtn" iconCssClass="icon-plus" label="new"
                selected='<%="new".equals(toolbarItem)%>' />
		</aui:nav>
	</aui:nav-bar>
	<div id="myTab">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab-1">Attivi</a></li>
			<li><a href="#tab-2">Disattivati</a></li>
		</ul>

		<div class="tab-content">
			<div id="tab-1" class="tab-pane">
			    <liferay-portlet:renderURL varImpl="addAgentURL">
                    <portlet:param name="mvcPath" value="/jsps/gestioneagenti/edit-agent.jsp" />
                </liferay-portlet:renderURL>
				<liferay-portlet:renderURL varImpl="iteratorURL">
					<portlet:param name="mvcPath" value="/jsps/gestioneagenti/view.jsp" />
				</liferay-portlet:renderURL>

				<liferay-ui:search-container delta="20"
                    emptyResultsMessage="no-agent" iteratorURL="<%=iteratorURL%>">
					<liferay-ui:search-container-results>
						<%
						    results = ListUtil.subList(activeAgents,
									    searchContainer.getStart(),
									    searchContainer.getEnd());
						    total = activeAgents.size();
						    pageContext.setAttribute("results", results);
						    pageContext.setAttribute("total", total);
						%>
					</liferay-ui:search-container-results>
					<liferay-ui:search-container-row className="User" modelVar="activeAgent">
						<%
						    ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
							    activeAgent.getCompanyId(), ClassNameLocalServiceUtil
								    .getClassNameId(User.class),
							    "CUSTOM_FIELDS", Constants.CODICE_AGENTE, activeAgent
								    .getUserId());
						    String codiceAgente = value.getData();
						%>
						<liferay-ui:search-container-column-text value="<%=codiceAgente%>"
							name="Codice" />
						<liferay-ui:search-container-row-parameter name="codiceAgente"
							value="<%=codiceAgente%>" />
						<liferay-ui:search-container-column-text property="fullName"
							name="Nome" />
						<liferay-ui:search-container-column-jsp align="right"
							path="/jsps/gestioneagenti/agent-action.jsp" />
					</liferay-ui:search-container-row>

					<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
						paginate="true" />
				</liferay-ui:search-container>
			</div>
			<div id="tab-2">
			     <liferay-ui:search-container delta="20"
                    emptyResultsMessage="no-agent" iteratorURL="<%=iteratorURL%>">
                    <liferay-ui:search-container-results>
                        <%
                            results = ListUtil.subList(deactivatedAgents,
                                        searchContainer.getStart(),
                                        searchContainer.getEnd());
                            total = deactivatedAgents.size();
                            pageContext.setAttribute("results", results);
                            pageContext.setAttribute("total", total);
                        %>
                    </liferay-ui:search-container-results>
                    <liferay-ui:search-container-row className="User" modelVar="deactivatedAgent">
                        <%
                            ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
                        	    deactivatedAgent.getCompanyId(), ClassNameLocalServiceUtil
                                    .getClassNameId(User.class),
                                "CUSTOM_FIELDS", Constants.CODICE_AGENTE, deactivatedAgent
                                    .getUserId());
                            String codiceAgente = value.getData();
                        %>
                        <liferay-ui:search-container-column-text value="<%=codiceAgente%>"
                            name="Codice" />
                        <liferay-ui:search-container-row-parameter name="codiceAgente"
                            value="<%=codiceAgente%>" />
                        <liferay-ui:search-container-column-text property="fullName"
                            name="Nome" />
                        <liferay-ui:search-container-column-jsp align="right"
                            path="/jsps/gestioneagenti/agent-action.jsp" />
                    </liferay-ui:search-container-row>

                    <liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
                        paginate="true" />
                </liferay-ui:search-container>
			</div>
		</div>
	</div>
</fieldset>

<aui:script>
	AUI().use('aui-tabview', function(A) {
		new A.TabView({
			srcNode : '#myTab'
		}).render();
	});
	
	AUI().use('aui-base', function(A) {
		A.one("#<portlet:namespace/>newBtn").on('click', function() {
        	window.location.href = '${addAgentURL}';
	    });
    });
</aui:script>
