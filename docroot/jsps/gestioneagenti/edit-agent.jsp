<%@page
	import="it.bysoftware.ct.service.AnagraficaAgentiLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficaAgenti"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.service.ClassNameLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoValue"%>
<%@page
	import="com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.utils.Constants"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>

<%
    String codiceAgente =  ParamUtil.getString(renderRequest, "codiceAgente",
    "");
    long userId = ParamUtil.getLong(renderRequest, "id", -1);
    User agent = null;

    AnagraficaAgenti a = null;
    if (userId != -1) {
		agent = UserLocalServiceUtil.getUser(userId);
		a = AnagraficaAgentiLocalServiceUtil.getAnagraficaAgenti(codiceAgente);
    }
%>
<liferay-portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/jsps/gestioneagenti/view.jsp" />
</liferay-portlet:renderURL>
<liferay-ui:header backURL="<%=backURL%>" title="agent" />

<liferay-portlet:renderURL var="selectCodeURL"
	windowState="<%=LiferayWindowState.POP_UP.toString()%>">
	<portlet:param name="mvcPath"
		value="/jsps/gestioneagenti/search-agent.jsp" />
	<portlet:param name="codiceAgente" value="<%=codiceAgente%>" />
	<portlet:param name="id" value="<%=String.valueOf(userId)%>" />
</liferay-portlet:renderURL>

<liferay-portlet:actionURL name="saveAgent" var="saveAgent" />

<aui:form id="aForm" name="aForm" action="<%=saveAgent%>" method="post">
	<aui:layout>
		<aui:column columnWidth="50" first="true">
			<aui:input id="id" name="id" readonly="true"
				value="<%=(userId != -1) ? userId : ""%>" />
			<aui:row>
				<aui:input id="agentCode" name="agentCode" label="agent-code"
					inlineField="true" value="<%=codiceAgente%>">
					<aui:validator name="required" />
				</aui:input>
				<aui:button id="selectCode" value="select" />
			</aui:row>
			<aui:input id="firstName" name="firstName" type="text"
				value="<%=(userId != -1) ? agent.getFirstName() : ""%>">
				<aui:validator name="required" />
			</aui:input>
			<aui:input id="lastName" name="lastName" type="text"
				value="<%=(userId != -1) ? agent.getLastName() : ""%>">
				<aui:validator name="required" />
			</aui:input>
		</aui:column>
		<aui:column columnWidth="50" last="true">
			<aui:input id="email" name="email" type="text"
				disabled="<%=userId != -1%>"
				value="<%=(userId != -1) ? agent.getEmailAddress() : ""%>">
				<aui:validator name="required" />
				<aui:validator name="email" />
			</aui:input>
			<aui:input name="password" type="password"
				value="<%=(userId != -1) ? agent
				.getPasswordEncrypted() : ""%>">
				<aui:validator name="required" />
				<aui:validator name="minLength">4</aui:validator>
				<aui:validator name="maxLength">10</aui:validator>
			</aui:input>
			<aui:input id="folder" name="folder" helpMessage="folder-name"
				disabled="<%=userId != -1%>"
				value="<%=(userId != -1) ? agent.getScreenName() : ""%>">
				<aui:validator name="required" />
			</aui:input>
		</aui:column>
	</aui:layout>
	<liferay-ui:panel-container extended="true">
		<liferay-ui:panel collapsible="true" defaultState="collapsed"
			title="details">
			<aui:layout>
				<aui:column columnWidth="50" first="true">
					<aui:input name="codiceZona" type="text" label="cod-zona"
						value="<%=(a != null) ? a.getCodiceZona() : ""%>" />
					<aui:input name="codiceProvvRigo" type="text" label="cod-prov-rig"
						value="<%=(a != null) ? a.getCodiceProvvRigo() : ""%>" />
					<aui:input name="codiceProvvChiusura" type="text"
						label="cod-prov-chi"
						value="<%=(a != null) ? a.getCodiceProvvChiusura() : ""%>" />
					<aui:input name="budget" type="text" label="budget"
						value="<%=(a != null) ? a.getBudget() : ""%>" />
				</aui:column>
				<aui:column columnWidth="50" last="true">
					<aui:input name="tipoLiquidazione" type="text" label="tip-liq"
						value="<%=(a != null) ? a.getTipoLiquidazione() : ""%>" />
					<aui:input name="giorniToll" type="text" label="gio-toll"
						value="<%=(a != null) ? a.getGiorniToll() : ""%>" />
					<aui:input name="tipoDivisa" type="checkbox" label="tip-div"
						checked="<%=(a != null) ? a.getTipoDivisa() : false%>" />
				</aui:column>
			</aui:layout>
		</liferay-ui:panel>
	</liferay-ui:panel-container>
	<aui:button type="submit" value="submit" />
</aui:form>

<aui:script>
	AUI().use(
			'aui-base',
			function(A) {
				A.one("#selectCode").on('click', function() {
					// 			window.location.href = '${selectCodeURL}';
					Liferay.Util.openWindow({
						dialog : {
							centered : true,
							modal : true
						},
						id : '<portlet:namespace/>itemDialog',
						title : 'Seleziona Agente',
						uri : '<%=selectCodeURL%>'
					});
				});

				Liferay.provide(window, 'closePopup', function(data, dialogId) {

					var dialog = Liferay.Util.Window.getById(dialogId);
					dialog.hide();
					console.log(data);
					var tmp = data.split("|");
					var codice = tmp[0];
					var nome = tmp[1].split(" ")[0];
					var cognome = tmp[1].split(" ")[1];
					A.one('#<portlet:namespace/>agentCode')
							.set('value', codice);
					A.one('#<portlet:namespace/>firstName').set('value', nome);
					A.one('#<portlet:namespace/>lastName')
							.set('value', cognome);
				});

				A.one("#<portlet:namespace/>email").on(
						'change',
						function() {
							var email = A.one('#<portlet:namespace/>email')
									.get('value');
							A.one('#<portlet:namespace/>folder').set('value',
									email.split("@")[0]);
						});
			});
</aui:script>
	



