<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficaAgentiLocalServiceUtil"%>
<%@page
	import="it.bysoftware.ct.service.AnagraficaClientiFornitoriLocalServiceUtil"%>
<%@page import="it.bysoftware.ct.model.AnagraficaAgenti"%>
<%@page import="java.util.List"%>
<%@include file="../init.jsp"%>

<%
    long userId = ParamUtil.getLong(renderRequest, "userId", -1);
    String codiceAgente = ParamUtil.getString(renderRequest,
		    "codiceAgente", "");
    int end = AnagraficaAgentiLocalServiceUtil
		    .getAnagraficaAgentisCount();
    List<AnagraficaAgenti> agenti = AnagraficaAgentiLocalServiceUtil
		    .getAnagraficaAgentis(0, end);
%>

<%-- <liferay-portlet:renderURL var="backURL"> --%>
<%-- 	<portlet:param name="mvcPath" --%>
<%-- 		value="/jsps/gestioneagenti/edit-agent.jsp" /> --%>
<%-- 	<portlet:param name="codiceAgente" value="<%=codiceAgente%>" /> --%>
<%-- 	<portlet:param name="userId" value="<%=String.valueOf(userId)%>" /> --%>
<%-- </liferay-portlet:renderURL> --%>
<%-- <liferay-ui:header backURL="<%=backURL%>" title="seach-agent" /> --%>

<liferay-portlet:renderURL varImpl="iteratorURL">
	<portlet:param name="mvcPath"
		value="/jsps/gestioneagenti/search-agent.jsp" />
</liferay-portlet:renderURL>

<liferay-ui:search-container delta="20"
    emptyResultsMessage="no-articoli" iteratorURL="<%=iteratorURL%>">
	<liferay-ui:search-container-results>
		<%
		    results = ListUtil.subList(agenti,
					    searchContainer.getStart(),
					    searchContainer.getEnd());
				    total = agenti.size();
				    pageContext.setAttribute("results", results);
				    pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row
		className="it.bysoftware.ct.model.AnagraficaAgenti" modelVar="agente">
		<liferay-ui:search-container-column-text property="codiceAgente"
			name="Codice" />
		<liferay-ui:search-container-column-text property="nome" name="Nome" />
		<liferay-ui:search-container-column-text property="codiceZona"
			name="Codice zona" />
		<liferay-ui:search-container-column-button name="Seleziona"
                                href="set('${agente.codiceAgente}|${agente.nome}')" />
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
		paginate="true" />
</liferay-ui:search-container>

<script type="text/javascript"> 
    function set(data) {
        console.log(data);
        Liferay.Util.getOpener().closePopup(data,
            '<portlet:namespace/>itemDialog');
    }
</script>