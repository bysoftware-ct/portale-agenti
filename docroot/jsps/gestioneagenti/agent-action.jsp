<%@page import="com.liferay.portal.model.User"%>
<%@page import="it.bysoftware.ct.model.AnagraficaAgenti"%>
<%@include file="../init.jsp"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%
    ResultRow row = (ResultRow) request
		    .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    User agent = (User) row.getObject();
    String codiceAgente = (String) row.getParameter("codiceAgente");
%>


<liferay-ui:icon-menu>

	<liferay-portlet:actionURL var="toggleAgent" name="toggleAgent">
		<liferay-portlet:param name="userId"
			value="<%=String.valueOf(agent.getUserId())%>" />
	</liferay-portlet:actionURL>
	<c:if test="<%=agent.isActive()%>">
		<liferay-ui:icon image="deactivate" url="${toggleAgent}" />

	</c:if>
	<c:if test="<%=!agent.isActive()%>">
		<liferay-ui:icon image="activate" url="${toggleAgent}" />
	</c:if>
	<liferay-portlet:renderURL var="editAgent">
	<liferay-portlet:param name="codiceAgente"
            value="<%=codiceAgente%>" />
		<liferay-portlet:param name="id"
			value="<%=String.valueOf(agent.getUserId())%>" />
		<liferay-portlet:param name="mvcPath"
			value="/jsps/gestioneagenti/edit-agent.jsp" />
	</liferay-portlet:renderURL>
	<liferay-ui:icon image="edit" url="${editAgent}" label="true" />

</liferay-ui:icon-menu>